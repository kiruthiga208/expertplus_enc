<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
	

	

Route::get('/', 'HomeController@index');
Route::get('/preview', 'HomeController@getPreview');
Route::get('/home/notpage', 'HomeController@getNotpage');

Route::post('/home/contact', 'HomeController@postContact');

Route::resource('/home', 'HomeController');


//Route::resource('/user', 'UserController');
Route::get('/user/forgot', 'UserController@getForgot');
Route::get('/user/login', 'UserController@getLogin');
Route::get('login', 'UserController@getLogin');
Route::get('/user/register', 'UserController@getRegister');
Route::post('/user/signin', 'UserController@postSignin');
Route::get('/user/socialize/{slug?}', 'UserController@getSocialize');
Route::get('/user/social/{slug?}', 'UserController@getSocial');
Route::post('/user/create', 'UserController@postCreate');
Route::get('profile/{username}', 'UserController@getProfiles');
Route::get('/user/logout', 'UserController@getLogout');
Route::get('/user/course', 'UserController@getMycourse');
Route::get('/user/dangerzone', 'UserController@getDangerzone');
Route::post('/user/ajax-courses', 'UserController@postAjaxCourses');
Route::post('/user/dangerzone', 'UserController@postDangerzone');
Route::post('/user/withdraw-request', 'UserController@postWithdrawRequest');
Route::post('/user/request', 'UserController@postRequest');
Route::post('/user/featured', 'UserController@postFeatured');

Route::post('/user/checkcoursecount','UserController@checkbusinessusercourse');
Route::get('/user/instructors', 'UserController@getInstructors');

Route::delete('/user/image','UserController@deleteImage');

Route::resource('/blogs', 'BlogController');

Route::get('/course/coursenameexits/{coursename?}','CourseController@getCoursenameexits');
Route::get('/course/coursecertificate/{id?}/{slug?}','CourseController@getCoursecertificate');
Route::get('/course/instructors/{id?}','CourseController@getInstructors');
Route::get('/course/allusers/{id?}','CourseController@getAllusers');


Route::post('/course/checkcoursepassword','CourseController@postCheckcoursepassword');
Route::post('/course/unpublish','CourseController@postUnpublish');
Route::post('/course/addusers','CourseController@postAddusers');
Route::post('/course/deleteinviteusers','CourseController@postDeleteinviteusers');
Route::post('/course/updateannoncement','CourseController@postUpdateannoncement');
Route::post('/course/savefeedbackreply','CourseController@postSavefeedbackreply');
Route::post('/course/createcourse','CourseController@postCreatecourse');
Route::post('/course/submitreview','CourseController@postSubmitreview');
Route::post('/course/updatecourse','CourseController@postUpdatecourse');
Route::post('/course/survey','CourseController@postSurvey');
Route::post('/course/image','CourseController@postImage');
Route::post('/course/testvideo/{id}','CourseController@postTestvideo');
Route::post('/course/uploadvideo','CourseController@postUploadvideo');
Route::post('/course/addinstructors','CourseController@postAddinstructors');
Route::post('/course/updateinstructors','CourseController@postUpdateinstructors');
Route::post('/course/subcategory','CourseController@postSubcategory');
Route::post('/course/insertlecturecomments','CourseController@postInsertlecturecomments');
Route::post('/course/updatelecturecomments','CourseController@postUpdatelecturecomments');
Route::post('/course/insertlecturereply','CourseController@postInsertlecturereply');
Route::post('/course/removelecturecomments','CourseController@postRemovelecturecomments');
Route::post('/course/adddiscussions','CourseController@postAdddiscussions');
Route::post('/course/searchdiscussion','CourseController@postSearchdiscussion');
Route::post('/course/insertannouncement','CourseController@postInsertannouncement');
Route::post('/course/removeannoucement','CourseController@postRemoveannoucement');
Route::post('/course/wishlist','CourseController@postWishlist');
Route::post('/course/updatecoursestatus','CourseController@postUpdatecoursestatus');
Route::post('/course/takenotes','CourseController@postTakenotes');
Route::post('/course/ratingform','CourseController@postRatingform');
Route::post('/course/reviewrating','CourseController@postReviewrating');
Route::post('/course/deletereview','CourseController@postDeletereview');
Route::post('/course/quizsubmit','CourseController@postQuizsubmit');
Route::post('/course/ajax-replies','CourseController@postAjaxReplies');
Route::post('/course/updatelecturereply','CourseController@postUpdatelecturereply');
Route::post('/course/removelecturereply','CourseController@postRemovelecturereply');
Route::post('/course/goonline','CourseController@postGoonline');
Route::post('/course/updateinsstatus','CourseController@postUpdateinsstatus');
Route::post('/course/deleteinstructors','CourseController@postDeleteinstructors');


Route::delete('/course/image','CourseController@deleteImage');
Route::delete('/course/testvideo','CourseController@deleteTestvideo');
Route::delete('/course/uploadvideo','CourseController@deleteUploadvideo');

Route::get('course/youtubelectureupload/{id?}/{vid?}', 'CourseController@getYoutubelectureupload');
Route::get('course/coupon', 'CourseController@getCoupon');
Route::get('course/startonline/{id?}', 'CourseController@getStartonline');
Route::get('course/logoutbbb/{id?}', 'CourseController@getLogoutbbb');
Route::get('course/logbbb/{id?}/{guid?}', 'CourseController@getLogbbb');


Route::delete('/course/coupon/{id?}','CourseController@deleteCoupon');

Route::resource('course', 'CourseController');
Route::get('course', 'CourseController@getIndex');
Route::get('course/image/{img?}/{size?}', 'CourseController@getImage');
Route::get('course/youtubeupload/{tube}/{id}', 'CourseController@getYoutubeupload');
Route::get('course/youtubepriupload/{tube}/{id}', 'CourseController@getYoutubepriupload');


Route::get('courseview/{id}/{slug?}', 'CourseController@getDetails')->where(array('id' => '\d+'));
Route::get('course-preview/{id}/{slug?}', 'CourseController@getCoursePreview')->where(array('id' => '\d+'));


Route::get('category/{category}', 'CategoryController@getIndex');
Route::get('search', 'SearchController@getIndex');


Route::post('/settings/store', 'SettingsController@postStore');
Route::resource('/settings', 'SettingsController');


Route::get('featured', 'FeaturedController@getIndex');
Route::get('topFree', 'FeaturedController@topFree');
Route::get('freeCourses', 'FeaturedController@freeCourses');
Route::get('topRated', 'FeaturedController@TopRated');
Route::get('topPaid', 'FeaturedController@TopPaid');
Route::get('mostViewed', 'FeaturedController@MostViewed');
Route::get('latestcourse', 'FeaturedController@lastestCourses');
Route::get('takencourse', 'FeaturedController@takenCourses');

/* user - forum */
Route::get('user-forum', 'ForumController@index');
Route::get('user-forum/index', 'ForumController@getIndex');
Route::get('forum/show/{id}', 'ForumController@getShow');
Route::get('forum/update/{id?}', 'ForumController@getUpdate');
Route::get('topic/{slug}', 'ForumController@getTopic');
Route::get('user-forum/{id}', 'ForumController@getForum');
Route::post('forum/insert', 'ForumController@postInsert');
Route::post('forum/delete', 'ForumController@postDelete');

Route::get('forum/update/{id?}', 'ForumController@getUpdate');
Route::post('user-forum/insertt', 'ForumController@postInsertt');
Route::post('user-forum/foruminsert', 'ForumController@postForuminsert');
Route::post('user-forum/forumupdate', 'ForumController@postForumupdate');
Route::get('user-forum/delete/{query}', 'ForumController@getForumdelete');
Route::get('topic/user-forum/delete/{query}', 'ForumController@getForumdelete');
Route::post('user-forum/comments', 'ForumController@postCommentsupdate');
Route::get('user-forum/comments/{id}', 'ForumController@getCommentdestory');
Route::post('user-forum/enable', 'ForumController@getEnable');

Route::get('forum/show/{id?}', 'ForumController@getShow');
Route::get('forum/update/{id?}', 'ForumController@getUpdate');
Route::post('forum/delete', 'ForumController@postDelete');
Route::post('/forum/multisearch','ForumController@postMultisearch');
Route::post('/forum/filter','ForumController@postFilter');
Route::post('/forum/insert','ForumController@postInsert');
Route::post('/forum/forumsave','ForumController@postForumsave');

Route::get('/forum/download','ForumController@getDownload');

Route::resource('forum', 'ForumController');

Route::get('blog/removecomm/{id}/{slug}', 'BlogController@getRemovecomm');
Route::get('blogs/read/{id?}', 'BlogController@getRead');
Route::get('blogs/category/{id?}', 'BlogController@getCategory');

Route::post('blogs/savecomment/{id?}', 'BlogController@postSavecomment');
Route::post('blogs/updatecomm', 'BlogController@postUpdatecomm');

Route::get('subscribe-course/{id}/{slug?}', 'CourseController@CourseSubscribe')->where(array('id' => '\d+'));


Route::get('courselisting/feedback/{id}', 'CourselistingController@getFeedback');
Route::get('courselisting/testvideo', 'CourselistingController@getTestvideo');
Route::post('courselisting/productidentifier', 'CourselistingController@postProductidentifier');
Route::post('forum/forumsave', 'ForumController@postForumsave');

Route::get('courselisting/removefeedback/{id?}/{feedid?}', 'CourselistingController@getRemovefeedback');

Route::get('user-forum/index', 'ForumController@getIndex');
Route::post('courselisting/savefeedback/{id}', 'CourselistingController@postSavefeedback');
Route::post('/courselisting/multisearch','CourselistingController@postMultisearch');
Route::post('/courselisting/filter','CourselistingController@postFilter');
Route::post('/courselisting/delete','CourselistingController@postDelete');


Route::resource('courselisting', 'CourselistingController');
Route::get('coursesurvey/show/{id?}','CoursesurveyController@getShow');
Route::post('coursesurvey/filter','CoursesurveyController@postFilter');
Route::post('coursesurvey/multisearch','CoursesurveyController@postMultisearch');
Route::resource('coursesurvey', 'CoursesurveyController');

Route::get('course/coursecertificate/{cid}/{uid}','CourseController@getCertificate');
Route::post('course/videourl','CourseController@postVideourl');

Route::get('payment-form/{cid}/{uid}','PaymentController@getPaymentapi');
Route::get('payment-form/{cid}/{uid}/{amt}','PaymentController@getPaymentapi');
Route::get('payment/successmob/{tid}/{cid}','PaymentController@getSuccessmob');
Route::get('payment/failuremob/{tid}/{cid}','PaymentController@getFailuremob');
Route::get('payment/success','PaymentController@getSuccess');
Route::get('payment/failure','PaymentController@getFailure');
Route::get('payment/cancelmembership','PaymentController@getCancelmembership');

Route::get('payment/walletform', 'PaymentController@getWalletForm');
Route::post('payment/walletform', 'PaymentController@postWalletForm');

Route::get('payment/form/{type?}','PaymentController@getForm');
Route::get('payment/paypalsuccess','PaymentController@getPaypalsuccess');
Route::get('payment/paypalfailure','PaymentController@getPaypalfailure');

Route::post('payment/courseform','PaymentController@postCourseform');
Route::post('payment/form','PaymentController@postForm');
Route::post('payment/stripe','PaymentController@postStripe');
Route::post('payment/stripemembership','PaymentController@postStripemembership');
Route::post('payment/paypalform','PaymentController@postPaypalform');

Route::resource('/payment', 'PaymentController');

Route::get('download-resourcemob/{name}', 'CourseController@getDownloadresourcemob');
Route::get('download-notesmob/{lid}/{uid}', 'CourseController@getDownloadnotesmob');
Route::get('category/{category}/{subcategory?}', 'CategoryController@getIndex');

Route::get('invoice/update/{id?}', 'InvoiceController@getUpdate');
Route::get('invoice/show/{id?}', 'InvoiceController@getShow');
Route::get('/invoice/download','InvoiceController@getDownload');

Route::post('invoice/save/{id?}', 'InvoiceController@postSave');
Route::post('invoice/multisearch', 'InvoiceController@postMultisearch');
Route::post('invoice/filter', 'InvoiceController@postFilter');
Route::post('invoice/delete', 'InvoiceController@postDelete');


Route::resource('invoice', 'InvoiceController');

include('pageroutes.php');
include('moduleroutes.php');
include('droidroutes.php');
include('iosroutes.php');


Route::get('transaction/update/{id?}', 'TransactionController@getUpdate');
Route::get('/transaction/download','TransactionController@getDownload');
Route::get('/transaction/show/{id?}','TransactionController@getShow');


Route::post('transaction/save/{id?}', 'TransactionController@postSave');
Route::post('transaction/delete', 'TransactionController@postDelete');
Route::post('transaction/multisearch', 'TransactionController@postMultisearch');
Route::post('transaction/filter', 'TransactionController@postFilter');

Route::resource('transaction', 'TransactionController');


Route::get('withdrawrequests/update/{id?}', 'WithdrawrequestsController@getUpdate');
Route::get('withdrawrequests/delete/{id?}', 'WithdrawrequestsController@getDelete');
Route::get('withdrawrequests/download', 'WithdrawrequestsController@getDownload');
Route::post('withdrawrequests/save/{id?}', 'WithdrawrequestsController@postSave');
Route::post('withdrawrequests/multisearch', 'WithdrawrequestsController@postMultisearch');
Route::post('withdrawrequests/filter', 'WithdrawrequestsController@postFilter');
Route::post('withdrawrequests/delete', 'WithdrawrequestsController@postDelete');

Route::resource('withdrawrequests', 'WithdrawrequestsController');

Route::post('forumcomments/multisearch', 'ForumcommentsController@postMultisearch');
Route::post('forumcomments/filter', 'ForumcommentsController@postFilter');
Route::get('forumcomments/show/{id}','ForumcommentsController@getShow');
Route::resource('forumcomments', 'ForumcommentsController');


Route::get('forumcategory/update/{id?}', 'ForumcategoryController@getUpdate');
Route::get('forumcategory/show/{id?}', 'ForumcategoryController@getShow');
Route::post('forumcategory/category', 'ForumcategoryController@postCategory');
Route::post('forumcategory/delete', 'ForumcategoryController@postDelete');
Route::post('forumcategory/multisearch', 'ForumcategoryController@postMultisearch');
Route::post('forumcategory/filter', 'ForumcategoryController@postFilter');
Route::post('forumcategory/categoryupdate', 'ForumcategoryController@postCategoryupdate');

Route::resource('forumcategory', 'ForumcategoryController');

Route::get('user/credits', 'UserController@getCredits');
Route::get('user/requests', 'UserController@getRequests');
Route::get('user/profile', 'UserController@getProfile');
Route::get('user/photo', 'UserController@getPhoto');
Route::get('user/account', 'UserController@getAccount');
Route::get('user/notification', 'UserController@getNotification');
Route::get('user/dangerzone', 'UserController@getDangerzone');
Route::get('user/mycourse', 'UserController@getMycourse');
Route::get('user/learning', 'UserController@getLearning');
Route::get('user/wishlist', 'UserController@getWishlist');
Route::get('user/certificate', 'UserController@getCertificate');

Route::get('user/premium-instrutors', 'UserController@getPremiumInstructors');
Route::post('user/approval', 'UserController@postApproval');

Route::get('user/wallet', 'UserController@getWallet');
Route::get('user/walletpoint', 'UserController@getWalletpoint');
Route::get('user/walletpointupdate/{id?}','UserController@getWalletPointUpdateForm');
Route::post('user/walletpointupdate/{id?}', 'UserController@postWalletPointUpdate');

Route::get('user/certificateview', 'UserController@getCertificateview');
Route::get('user/instructorinfo/{edit?}', 'UserController@getInstructorinfo');
Route::get('user/image/{image_hash?}/{size?}', 'UserController@getImage');
Route::get('/user/usermembership/{id?}','UserController@getUsermembership');
Route::get('/user/membership/{id?}','UserController@getMembership');
Route::get('user/activation/{id?}/{size?}', 'UserController@getActivation');
Route::get('user/reset/{id?}/{size?}', 'UserController@getReset');

Route::get('coupon/show/{id?}', 'CouponController@getShow');

Route::post('coupon/multisearch', 'CouponController@postMultisearch');
Route::post('coupon/filter', 'CouponController@postFilter');
Route::post('coupon/delete', 'CouponController@postDelete');

Route::resource('coupon', 'CouponController');

Route::get('admincoupon/update/{id?}', 'AdmincouponController@getUpdate');
Route::get('admincoupon/show/{id?}', 'AdmincouponController@getShow');
Route::post('admincoupon/save/{id?}', 'AdmincouponController@postSave');
Route::post('admincoupon/delete', 'AdmincouponController@postDelete');
Route::post('admincoupon/multisearch', 'AdmincouponController@postMultisearch');
Route::post('admincoupon/filter', 'AdmincouponController@postFilter');

Route::resource('admincoupon', 'AdmincouponController');

Route::post('/user/saveprofile', 'UserController@postSaveprofile');
Route::post('/user/account', 'UserController@postAccount');
Route::post('/user/notification', 'UserController@postNotification');
Route::post('/user/instructorinfo/{info?}', 'UserController@postInstructorinfo');
Route::post('/user/certificate', 'UserController@postCertificate');
Route::post('/user/getskills', 'UserController@postGetskills');
Route::post('/user/image', 'UserController@postImage');
Route::post('/user/photo', 'UserController@postPhoto');


Route::get('questionanswer/view', 'QuestionanswerController@getView');
Route::get('questionanswer/save', 'QuestionanswerController@postSave');
Route::get('questionanswer/update/{id?}', 'QuestionanswerController@getUpdate');
Route::get('questionanswer/show/{id?}/{ans?}', 'QuestionanswerController@getShow');
Route::get('questionanswer/downloadresource/{id?}', 'QuestionanswerController@getDownloadresource');
Route::get('questionanswer/deletequestion/{id?}', 'QuestionanswerController@getDeletequestion');
Route::get('questionanswer/search' , 'QuestionanswerController@getSearch');

Route::post('questionanswer/filter', 'QuestionanswerController@postFilter');
Route::post('questionanswer/upload', 'QuestionanswerController@postUpload');
Route::post('questionanswer/save/{id?}', 'QuestionanswerController@postSave');
Route::post('questionanswer/delete', 'QuestionanswerController@postDelete');
Route::post('questionanswer/answer/{id?}', 'QuestionanswerController@postAnswer');
Route::post('questionanswer/docdelete', 'QuestionanswerController@postDocdelete');


Route::resource('questionanswer', 'QuestionanswerController');

Route::get('customcourserequest/view', 'CustomcourserequestController@getView');
Route::get('customcourserequest/payments', 'CustomcourserequestController@getPayments');
Route::get('customcourserequest/completerequest/{id?}', 'CustomcourserequestController@getCompleterequest');
Route::get('customcourserequest/complete/{id?}', 'CustomcourserequestController@getComplete');

Route::get('customcourserequest/update/{id?}', 'CustomcourserequestController@getUpdate');
Route::get('customcourserequest/show/{id?}', 'CustomcourserequestController@getShow');
Route::get('customcourserequest/downloadresource/{name?}', 'CustomcourserequestController@getDownloadresource');


Route::post('customcourserequest/messagesend', 'CustomcourserequestController@postMessagesend');
Route::post('customcourserequest/messageslist', 'CustomcourserequestController@postMessageslist');
Route::post('customcourserequest/apply', 'CustomcourserequestController@postApply');
Route::post('customcourserequest/filter', 'CustomcourserequestController@postFilter');
Route::post('customcourserequest/delete/{id?}', 'CustomcourserequestController@postDelete');
Route::post('customcourserequest/save/{id?}', 'CustomcourserequestController@postSave');
Route::post('customcourserequest/upload', 'CustomcourserequestController@postUpload');
Route::post('customcourserequest/docdelete/{id?}', 'CustomcourserequestController@postDocdelete');
Route::get('customcourserequest/delete/{id?}', 'CustomcourserequestController@getDelete');
Route::get('customcourserequest/award/{id?}', 'CustomcourserequestController@getAward');

Route::post('customcourserequest/instructors', 'CustomcourserequestController@postInstructors');
Route::post('customcourserequest/courseshare', 'CustomcourserequestController@postCourseshare');
Route::post('customcourserequest/cancel', 'CustomcourserequestController@postCancel');
Route::post('customcourserequest/finishdispute', 'CustomcourserequestController@postFinishdispute');
Route::post('customcourserequest/proposalprice', 'CustomcourserequestController@postProposalprice');



Route::resource('customcourserequest', 'CustomcourserequestController');


Route::get('banner/update/{id?}', 'BannerController@getUpdate');
Route::get('banner/show/{id?}', 'BannerController@getShow');

Route::post('banner/save', 'BannerController@postSave');
Route::post('banner/delete','BannerController@postDelete');
Route::post('/banner/filter','BannerController@postFilter');
Route::post('/banner/multisearch','BannerController@postMultisearch');

Route::delete('/banner/delete/{id?}','BannerController@deleteBanner');

Route::resource('banner', 'BannerController');

Route::get('advertisement/update/{id?}', 'AdvertisementController@getUpdate');
Route::get('advertisement/show/{id?}', 'AdvertisementController@getShow');
Route::get('advertisement/download', 'AdvertisementController@getDownload');

Route::post('advertisement/save/{id?}', 'AdvertisementController@postSave');
Route::post('advertisement/multisearch', 'AdvertisementController@postMultisearch');
Route::post('advertisement/filter', 'AdvertisementController@postFilter');
Route::post('advertisement/delete', 'AdvertisementController@postDelete');

Route::resource('advertisement', 'AdvertisementController');

Route::get('currency/update/{id?}', 'CurrencyController@getUpdate');
Route::get('currency/show/{id?}', 'CurrencyController@getShow');

Route::post('currency/save/{id?}', 'CurrencyController@postSave');
Route::post('currency/multisearch', 'CurrencyController@postMultisearch');
Route::post('currency/filter', 'CurrencyController@postFilter');
Route::post('currency/delete', 'CurrencyController@postDelete');

Route::resource('currency', 'CurrencyController');


Route::get('clientbanner/update/{id?}', 'ClientbannerController@getUpdate');
Route::get('clientbanner/show/{id?}', 'ClientbannerController@getShow');

Route::post('clientbanner/save/{id?}', 'ClientbannerController@postSave');
Route::post('clientbanner/delete', 'ClientbannerController@postDelete');
Route::post('clientbanner/multisearch','ClientbannerController@postMultisearch');
Route::post('clientbanner/filter','ClientbannerController@postFilter');

Route::resource('clientbanner', 'ClientbannerController');

 Route::post('/dashboard/ajax-course', 'DashboardController@postAjaxCourse');
 Route::resource('/dashboard', 'DashboardController');

Route::post('/bank/cashondelivery', 'BankController@postCashondelivery');

Route::resource('bank', 'BankController');

Route::post('ccavenue/ccavenuerequest', 'CcavenueController@postCcavenuerequest');
Route::post('ccavenue/save/{id?}', 'CcavenueController@postSave');
Route::post('ccavenue/ccavenueresponse', 'CcavenueController@postCcavenueresponse');
Route::post('ccavenue/delete', 'CcavenueController@postDelete');

Route::resource('ccavenue', 'CcavenueController');

Route::get('membership/update/{id?}', 'MembershipController@getUpdate');
Route::get('membership/show/{id?}', 'MembershipController@getShow');
Route::get('/membership/download','MembershipController@getDownload');
Route::get('/membership/courseselect','MembershipController@getCourseselect');
Route::get('/membership/courselist/{id?}','MembershipController@getCourselist');

Route::post('membership/save/{id?}', 'MembershipController@postSave');
Route::post('membership/delete/{id?}', 'MembershipController@postDelete');
Route::post('membership/addacourse', 'MembershipController@postAddacourse');
Route::post('membership/remcourse', 'MembershipController@postRemcourse');
Route::post('membership/filter', 'MembershipController@postFilter');
Route::post('membership/couponcheck', 'MembershipController@postCouponcheck');
Route::post('membership/indsummary','MembershipController@postIndsummary');
Route::post('membership/indstatus','MembershipController@postIndstatus');


Route::resource('membership', 'MembershipController');

//business
Route::get('businessplan/students', 'BusinessplanController@getStudents');
Route::get('businessplan/courses', 'BusinessplanController@getCourses');
Route::get('businessplan/allcourses', 'BusinessplanController@getAllcourses');
Route::get('businessplan/assignuserbysubscription', 'BusinessplanController@getAssignuserbysubscription');
Route::get('businessplan/allstudents', 'BusinessplanController@getAllstudents');
Route::get('businessplan/studentform', 'BusinessplanController@getStudentform');
Route::get('businessplan/update/{id?}', 'BusinessplanController@getUpdate');
Route::get('businessplan/show/{id?}', 'BusinessplanController@getShow');
Route::get('businessplan/download', 'BusinessplanController@getDownload');

Route::post('businessplan/save/{id?}', 'BusinessplanController@postSave');
Route::post('businessplan/multisearch', 'BusinessplanController@postMultisearch');
Route::post('businessplan/filter', 'BusinessplanController@postFilter');
Route::post('businessplan/delete', 'BusinessplanController@postDelete');
Route::get('businessplan/users', 'BusinessplanController@getUsers');
Route::get('businessplan/studentsum/{id?}','BusinessplanController@getStudentsum');
Route::post('businessplan/createstudent/{id?}','BusinessplanController@postCreatestudent');
Route::post('businessplan/assignusersbysubs','BusinessplanController@postAssignusersbysubs');
Route::get('businessplan/studentcourselist/{id?}','BusinessplanController@getstudentcourselist');
Route::resource('businessplan', 'BusinessplanController');


Route::group(['middleware' => 'auth' , 'middleware' => 'web'], function()
{
    



	Route::resource('config', 'ConfigController');
	Route::get('/core/users/download','Core\UsersController@getDownload');	
	Route::get('/core/users/blast','Core\UsersController@getBlast');
	Route::get('/core/users/show/{id?}','Core\UsersController@getShow');
	Route::get('/core/users/update/{id?}','Core\UsersController@getUpdate');
	Route::get('/core/users/comboselect','Core\UsersController@getComboselect');

	Route::delete('/core/users/usersimage','Core\UsersController@deleteUsersimage');

	Route::post('/core/users/doblast','Core\UsersController@postDoblast');
	Route::post('/core/users/save/{id?}','Core\UsersController@postSave');
	Route::post('/core/users/delete','Core\UsersController@postDelete');

	Route::resource('core/users','Core\UsersController');
    Route::post('/core/users/filter','Core\UsersController@postFilter');

    Route::get('/core/logs/download','Core\LogsController@getDownload');
    Route::post('/core/logs/delete','Core\LogsController@postDelete');
    Route::post('/core/logs/filter','Core\LogsController@postFilter');
    Route::resource('core/logs','Core\LogsController');

    Route::get('core/pages/update/{id?}','Core\PagesController@getUpdate');
    Route::get('core/pages/show/{id?}','Core\PagesController@getShow');
    Route::get('core/pages/download','Core\PagesController@getDownload');

    Route::post('core/pages/filter','Core\PagesController@postFilter');
    Route::post('core/pages/save/{id?}','Core\PagesController@postSave');
    Route::post('core/pages/delete','Core\PagesController@postDelete');

    Route::resource('core/pages','Core\PagesController');

    Route::resource('core/groups','Core\GroupsController');

    
    
    Route::post('bsetec/menu/saveorder/{id?}','bsetec\MenuController@postSaveorder');
    Route::post('bsetec/menu/save/{id?}','bsetec\MenuController@postSave');
    Route::get('bsetec/menu/{id?}','bsetec\MenuController@getIndex');
    Route::resource('bsetec/menu','bsetec\MenuController');
    Route::get('bsetec/menu/destroy/{id?}','bsetec\MenuController@getDestroy');

    
    Route::get('bsetec/config/security','bsetec\ConfigController@getSecurity');
    Route::get('bsetec/config/theme','bsetec\ConfigController@getTheme');
    Route::get('bsetec/config/translation','bsetec\ConfigController@getTranslation');
    Route::get('bsetec/config/email','bsetec\ConfigController@getEmail');
    Route::get('bsetec/config/payment','bsetec\ConfigController@getPayment');
    Route::get('bsetec/config/log','bsetec\ConfigController@getLog');
    Route::get('bsetec/config/customsettings','bsetec\ConfigController@getCustomsettings');
    Route::get('bsetec/config/addtranslation','bsetec\ConfigController@getAddtranslation');
    Route::get('bsetec/config/removetranslation/{folder}','bsetec\ConfigController@getRemovetranslation');
    Route::get('bsetec/config/clearlog','bsetec\ConfigController@getClearlog');
    Route::get('bsetec/config/change/{theme}','bsetec\ConfigController@getChange');
    Route::get('bsetec/config/mailsettings','bsetec\ConfigController@getMailsettings');
    Route::get('bsetec/config/coursesettings','bsetec\ConfigController@getCoursesettings');
    Route::get('bsetec/config/checkcoursesettings','bsetec\ConfigController@getCheckcoursesettings');
    Route::get('bsetec/config/awssettings','bsetec\ConfigController@getAwssettings');
    Route::get('bsetec/config/membership','bsetec\ConfigController@getMembership');
    Route::get('bsetec/config/walletpoint','bsetec\ConfigController@getWalletpoint');
    Route::post('bsetec/config/walletpoint','bsetec\ConfigController@postWalletpoint');



    Route::post('bsetec/config/save','bsetec\ConfigController@postSave');
    Route::post('bsetec/config/email','bsetec\ConfigController@postEmail');
    Route::post('bsetec/config/login','bsetec\ConfigController@postLogin');
    Route::post('bsetec/config/addtranslation','bsetec\ConfigController@postAddtranslation');
    Route::post('bsetec/config/payment','bsetec\ConfigController@postPayment');
    Route::post('bsetec/config/updatemailsettings','bsetec\ConfigController@postUpdatemailsettings');
    Route::post('bsetec/config/sendemail','bsetec\ConfigController@postSendemail');
    Route::post('bsetec/config/updatcoursesettings','bsetec\ConfigController@postUpdatcoursesettings');
    Route::post('bsetec/config/savetranslation','bsetec\ConfigController@postSavetranslation');
    Route::post('bsetec/config/customsettings','bsetec\ConfigController@postCustomsettings');
    Route::post('bsetec/config/membership','bsetec\ConfigController@postMembership');
    Route::post('bsetec/config/awssave','bsetec\ConfigController@postAwssave');




    Route::resource('bsetec/config','bsetec\ConfigController');


    Route::get('bsetec/module/permission/{id}','bsetec\ModuleController@getPermission');
    Route::get('bsetec/module/build/{id}','bsetec\ModuleController@getBuild');
    Route::get('bsetec/module/form/{id}','bsetec\ModuleController@getForm');
    Route::get('bsetec/module/formdesign/{id}','bsetec\ModuleController@getFormdesign');
    Route::get('bsetec/module/table/{id}','bsetec\ModuleController@getTable');
    Route::get('bsetec/module/sql/{id}','bsetec\ModuleController@getSql');
    Route::get('bsetec/module/config/{id}','bsetec\ModuleController@getConfig');



    Route::post('bsetec/module/savepermission/{id}','bsetec\ModuleController@postSavepermission');
    Route::post('bsetec/module/dobuild/{id}','bsetec\ModuleController@postDobuild');
    Route::post('bsetec/module/saveform/{id}','bsetec\ModuleController@postSaveform');
    Route::post('bsetec/module/formdesign/{id}','bsetec\ModuleController@postFormdesign');
    Route::post('bsetec/module/savetable/{id}','bsetec\ModuleController@postSavetable');
    Route::post('bsetec/module/savesql/{id}','bsetec\ModuleController@postSavesql');
    Route::post('bsetec/module/saveconfig/{id}','bsetec\ModuleController@postSaveconfig');

    Route::resource('bsetec/module','bsetec\ModuleController');


    Route::get('bsetec/tables/tableconfig/{id?}','bsetec\TablesController@getTableconfig');
    Route::get('bsetec/tables/tablefieldedit/{id?}','bsetec\TablesController@getTablefieldedit');
    Route::get('bsetec/tables/tablefieldremove/{id?}/{column?}','bsetec\TablesController@getTablefieldremove');
    Route::post('bsetec/tables/tablefieldsave/{id?}','bsetec\TablesController@postTablefieldsave');
    Route::post('bsetec/tables/tableinfo/{id?}','bsetec\TablesController@postTableinfo');
    Route::post('bsetec/tables/tables/{id?}','bsetec\TablesController@postTables');
    Route::post('bsetec/tables/tableremove','bsetec\TablesController@postTableremove');
    Route::get('bsetec/tables/mysqleditor','bsetec\TablesController@getMysqleditor');
    Route::post('bsetec/tables/mysqleditor','bsetec\TablesController@postMysqleditor');

    Route::resource('bsetec/tables','bsetec\TablesController');

  	
    Route::get('sitesubcategory/update/{id?}','SitesubcategoryController@getUpdate');
    Route::get('sitesubcategory/show/{id?}','SitesubcategoryController@getShow');
    Route::get('sitesubcategory/download','SitesubcategoryController@getDownload');

    Route::post('sitesubcategory/save/{id?}','SitesubcategoryController@postSave');
    Route::post('sitesubcategory/filter','SitesubcategoryController@postFilter');
    Route::post('sitesubcategory/multisearch','SitesubcategoryController@postMultisearch');
    Route::post('sitesubcategory/delete','SitesubcategoryController@postDelete');


  	Route::resource('sitesubcategory', 'SitesubcategoryController');

    Route::post('sitecategory/filter','SitecategoryController@postFilter');
    Route::post('sitecategory/multisearch','SitecategoryController@postMultisearch');
    Route::post('sitecategory/save/{id?}','SitecategoryController@postSave');
	Route::post('sitecategory/delete','SitecategoryController@postDelete');

	Route::get('sitecategory/update/{id?}','SitecategoryController@getUpdate');
    Route::get('sitecategory/show/{id?}','SitecategoryController@getShow');
    Route::get('sitecategory/download','SitecategoryController@getDownload');

  	Route::resource('sitecategory', 'SitecategoryController');
	
	
    Route::get('blogadmin/add/{id?}','BlogadminController@getAdd');
    Route::get('blogadmin/download','BlogadminController@getDownload');
    Route::get('blogadmin/show/{id?}','BlogadminController@getShow');
    Route::post('blogadmin/save/{id?}','BlogadminController@postSave');
    Route::post('blogadmin/multisearch','BlogadminController@postMultisearch');
    Route::post('blogadmin/filter','BlogadminController@postFilter');
    Route::post('blogadmin/destroy','BlogadminController@postDestroy');

	Route::resource('blogadmin', 'BlogadminController');
	
    Route::get('blogcategories/add/{id?}','BlogcategoriesController@getAdd');
    Route::get('blogcategories/show/{id?}','BlogcategoriesController@getShow');
    Route::get('blogcategories/download','BlogcategoriesController@getDownload');

    Route::post('blogcategories/save/{id?}','BlogcategoriesController@postSave');
    Route::post('blogcategories/destroy','BlogcategoriesController@postDestroy');
    Route::post('blogcategories/filter','BlogcategoriesController@postFilter');

	Route::resource('blogcategories', 'BlogcategoriesController');

	
    Route::get('blogcomment/update/{id?}','BlogcommentController@getUpdate');
    Route::get('blogcomment/save/{id?}','BlogcommentController@postSave');
    Route::get('blogcomment/download','BlogcommentController@getDownload');
    Route::get('/blogcomment/comboselect','BlogcommentController@getComboselect');
    Route::get('/blogcomment/show/{id?}','BlogcommentController@getShow');

    Route::post('blogcomment/filter','BlogcommentController@postFilter');
    Route::post('blogcomment/multisearch','BlogcommentController@postMultisearch');
    Route::post('blogcomment/save/{id?}','BlogcommentController@postSave');
    Route::post('blogcomment/delete','BlogcommentController@postDelete');

	Route::resource('blogcomment', 'BlogcommentController');

	Route::resource('blog', 'BlogController');

    Route::get('courselanguage/multisearch','CourselanguageController@postMultisearch');
    Route::get('courselanguage/update/{id?}','CourselanguageController@getUpdate');
    Route::get('courselanguage/show/{id?}','CourselanguageController@getShow');
    Route::post('courselanguage/save/{id?}','CourselanguageController@postSave');
    Route::post('courselanguage/delete','CourselanguageController@postDelete');
    Route::post('courselanguage/filter','CourselanguageController@postFilter');
    Route::post('courselanguage/multisearch','CourselanguageController@postMultisearch');

	Route::resource('courselanguage', 'CourselanguageController');

    Route::post('reportabuse/filter','ReportabuseController@postFilter');
    Route::post('reportabuse/multisearch','ReportabuseController@postMultisearch');
    Route::post('reportabuse/delete','ReportabuseController@postDelete');
    Route::post('reportabuse/save/{id?}','ReportabuseController@postSave');
    
    Route::get('/reportabuse/download','ReportabuseController@getDownload');
    Route::get('reportabuse/show/{id?}','ReportabuseController@getShow');
	Route::resource('reportabuse', 'ReportabuseController');

	//Message module

	Route::get('message', 'MessageController@getIndex');
	Route::post('messages/mfilter', 'MessageController@postMessageFilter');
	Route::post('messageupdate', 'MessageController@postMessageUpdate');
	Route::post('messagegroupupdate', 'MessageController@postGroupMessageUpdate');
	Route::post('messagesend', 'MessageController@postSave');
	Route::post('messagelabelsave', 'MessageController@postLabelSave');
	Route::post('messagelabeldelete', 'MessageController@postLabelDelete');
		
	Route::get('course/create/{id}/{step}', 'CourseController@getCreate');
	Route::get('course/toread/{id?}', 'CourseController@getToread');
	Route::get('deleteCourses/{id}', 'CourseController@getDeletecourses');
	Route::any('courseusers/{id}', 'CourseController@postCourseSubscriberlist');
	
	// Save Curriculum
	Route::post('courses/section/save', 'CourseController@postSectionSave');
	Route::post('courses/section/delete', 'CourseController@postSectionDelete');
	Route::post('courses/lecture/save', 'CourseController@postLectureSave');
	Route::post('courses/quiz/save', 'CourseController@postQuizSave');
	Route::post('courses/lecturequiz/delete', 'CourseController@postLectureQuizDelete');
	Route::post('courses/lecturedesc/save', 'CourseController@postLectureDescSave');
	Route::post('courses/lecturepublish/save', 'CourseController@postLecturePublishSave');
	Route::post('courses/lecturevideo/save/{lid}', 'CourseController@postLectureVideoSave');
	Route::post('courses/lectureaudio/save/{lid}', 'CourseController@postLectureAudioSave');
	Route::post('courses/lecturepre/save/{lid}', 'CourseController@postLecturePresentationSave');
	Route::post('courses/lecturedoc/save/{lid}', 'CourseController@postLectureDocumentSave');
	Route::post('courses/lectureres/save/{lid}', 'CourseController@postLectureResourceSave');
	Route::post('courses/lecturetext/save', 'CourseController@postLectureTextSave');
	Route::post('courses/lectureres/delete', 'CourseController@postLectureResourceDelete');
	Route::post('courses/lecturelib/save', 'CourseController@postLectureLibrarySave');
	Route::post('courses/lecturelib/save', 'CourseController@postLectureSave');
	Route::post('courses/lecturelibres/save', 'CourseController@postLectureLibraryResourceSave');
	Route::post('courses/lectureexres/save', 'CourseController@postLectureExternalResourceSave');
	Route::post('courses/quiz/questionadd', 'CourseController@postQuizQuestionAdd');
    Route::post('courses/quizques/delete', 'CourseController@postQuizQuestionDelete');
    Route::post('courses/quiz/certansweradd', 'CourseController@postQuizCertAnswerAdd');
    Route::post('course/coupon', 'CourseController@postCoupon');
    Route::post('course/video', 'CourseController@postVideo');
    Route::post('course/couponcheck', 'CourseController@postCouponcheck');



	// Sorting Curriculum
	Route::post('courses/curriculum/sort', 'CourseController@postCurriculumSort');
	Route::post('courses/curriculum/sortquiz', 'CourseController@postCurriculumQuizQuestionSort');
	Route::get('learn-course/{id}/{slug?}/{lid?}', 'CourseController@getTakeCourse')->where(array('id' => '\d+'));
	Route::get('learn-course-preview/{id}/{slug?}/{lid?}', 'CourseController@getTakeCoursePreview')->where(array('id' => '\d+'));
	Route::get('download-notes/{id}', 'CourseController@getDownloadnotes')->where(array('id' => '\d+'));
	Route::get('download-resource/{name}', 'CourseController@getDownloadresource');
	Route::get('learn/{id}/{slug?}', 'CourseController@getPreview')->where(array('id' => '\d+'));
	// Route::get('deletetestvideo', 'CourseController@postDeletetestvideo');

	Route::get('wallet/course_purchase/{slug}', 'CourseController@getConfirmPurchase');
	Route::post('wallet/takecourse', 'CourseController@postPurchaseCourse');


});	


Route::get('sitemap.xml', function(){
    // create new sitemap object
    $sitemap = App::make("sitemap");
    $sitemap->setCache('laravel.sitemap', 120,'true');
    
    if (!$sitemap->isCached()) {
     // get all pages from db
     $pages = DB::table('pages')->where('status', '=', 'enable')->where('allow_guest', '=', '1')->where('template', '=', 'frontend')->orderBy('pageID', 'desc')->select('*')->get();
     
     if(count($pages)>0){
      foreach ($pages as $page){
       $page_date = $page->updated;
       if($page->updated=='0000-00-00 00:00:00'){
        $page_date = date("Y-m-d H:i:s");
       }
       $sitemap->add(URL::to($page->alias), $page_date, '1.0', 'daily');
      }
     }
     // get all course from db
     $posts = DB::table('course')->select('course_id','image', 'course_title', 'updated_at', 'slug')->where('approved','=', '1')->where('privacy','=', '1')->orderBy('created_at', 'desc')->get();
     
     // add every course to the sitemap
     if(count($posts)>0){
      foreach ($posts as $post){
       $imgUrl = \bsetecHelpers::getImage($post->image);
       $images = [
     ['url' => $imgUrl, 'title' => $post->course_title, 'caption' => $post->course_title]
    ];
    $course_date = $post->updated_at;
       if($post->updated_at=='0000-00-00 00:00:00'){
        $course_date = date("Y-m-d H:i:s");
       }
       
       $curl = 'courseview/'.$post->course_id.'/'.$post->slug;
       $sitemap->add(URL::to($curl), $course_date, '1.0', 'daily', $images);
      }

  }
 	return $sitemap->render('xml');
 }else{

  	return $sitemap->render('xml');
 }
});


	




