@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('ccavenue?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('ccavenue?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('ccavenue/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Id</td>
				<td>{{ $row->id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>User Id</td>
				<td>{{ $row->user_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Course Id</td>
				<td>{{ $row->course_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Amount</td>
				<td>{{ $row->amount }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Updated At</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Order Details</td>
				<td>{{ $row->order_details }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				<td>{{ $row->status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Payment Method</td>
				<td>{{ $row->payment_method }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop