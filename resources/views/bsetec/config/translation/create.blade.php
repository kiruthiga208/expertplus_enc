 {!! Form::open(array('url'=>'bsetec/config/addtranslation', 'class'=>'form-horizontal')) !!}
 <div class="row">
  <div class="form-group">
    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.language_name')!!} </label>
	<div class="col-md-8">
	<input name="name" type="text" id="name" class="form-control input-sm" value="" required /> 
	 </div> 
  </div>   	
 
  <div class="form-group">
    <label for="ipt" class=" control-label col-md-4"> {!! Lang::get('core.Folder_Name')!!} </label>
	<div class="col-md-8">
	<input name="folder" type="text" id="folder" class="form-control input-sm" value="" required /> 
	 </div> 
  </div>   	
  
   <div class="form-group">
    <label for="ipt" class=" control-label col-md-4">  {!! Lang::get('core.Author')!!}</label>
	<div class="col-md-8">
	<input name="author" type="text" id="author" class="form-control input-sm" value="" required /> 
	 </div> 
  </div>   	
  
  <div class="form-group">
    <label for="ipt" class=" control-label col-md-4">  </label>
	<div class="col-md-8">
		<button type="submit" name="submit" class="btn btn-info"> {!! Lang::get('core.Add_Languange')!!}</button>
	</div> 
  </div>  
  </div> 	    
 
 {!! Form::close() !!}