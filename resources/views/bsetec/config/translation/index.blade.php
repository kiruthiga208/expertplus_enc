@extends('layouts.app')


@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {!! Lang::get('core.tab_translation')!!}  <small> {!! Lang::get('core.manage_translation')!!}</small></h3>
      </div>

		  <ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}"> {!! Lang::get('core.m_dashboard')!!}</a></li>
			<li><a href="{{ URL::to('bsetec/config') }}"> {!! Lang::get('core.Setting')!!} </a></li>
			<li class="active"> {!! Lang::get('core.tab_translation')!!} </li>
		  </ul>
			  
	  
    </div>


	<div class="page-content-wrapper m-t">  	
	@include('bsetec.config.tab',array('active'=>'translation'))
	 <div class="tab-pane active use-padding" id="info">	
<div class="tab-content m-t ">
		<div class="sbox   animated fadeInUp"> 
			<div class="sbox-title"> {!! Lang::get('core.manage_translation')!!} </div>
			<div class="sbox-content"> 		 

	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>	  
	  
	 {!! Form::open(array('url'=>'bsetec/config/translation/', 'class'=>'form-vertical row')) !!}
	
	<div class="col-sm-9">
		
		<a href="{{ URL::to('bsetec/config/addtranslation')}} " onclick="bsetecModal(this.href,'Add New Language');return false;" class="btn btn-primary"><i class="fa fa-plus-circle"></i> {!! Lang::get('core.add_translation')!!} </a>  
		<hr />
        <div class="table-responsive">
		<table class="table table-striped translation-sec">
			<thead>
				<tr>
					<th>{!! Lang::get('core.Name')!!} </th>
					<th>{!! Lang::get('core.Folder')!!} </th>
					<th>{!! Lang::get('core.Author')!!} </th>
					<th>{!! Lang::get('core.btn_action')!!} </th>
				</tr>
			</thead>
			<tbody>		
		
			@foreach(SiteHelpers::langOption() as $lang)
				<tr>
					<td> {{  $lang['name'] }}   </td>
					<td> {{  $lang['folder'] }} </td>
					<td> {{  $lang['author'] }} </td>
				  	<td>
					@if($lang['folder'] =='en')
					<a href="{{ URL::to('bsetec/config/translation?edit='.$lang['folder'])}} " class="btn btn-sm btn-primary"> {!! Lang::get('core.Manage')!!} </a>
					@elseif($lang['folder'] !='en')
					<a href="{{ URL::to('bsetec/config/translation?edit='.$lang['folder'])}} " class="btn btn-sm btn-primary"> {!! Lang::get('core.Manage')!!} </a>
					<a href="{{ URL::to('bsetec/config/removetranslation/'.$lang['folder'])}} " class="btn btn-sm btn-danger"> {!! Lang::get('core.Delete')!!} </a> 
					 
					@endif 
				
				</td>
				</tr>
			@endforeach
			
			</tbody>
		</table>
	</div> 
	</div>
	</div>
	</div>



 	
 </div>
 {!! Form::close() !!}
</div>
</div>
</div>
</div>






@endsection