@extends('layouts.app')

@section('content')


<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3><i class="fa fa-gears"></i> {!! Lang::get('core.wallet_point')!!}</h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.wallet_point')!!}</a></li>
		</ul>

	</div>

	<div class="page-content-wrapper">
		@if(Session::has('message'))
			{{ Session::get('message') }}
		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		<div class="block-content">
			@include('bsetec.config.tab')
			<div class="tab-content m-t">
				<div class="tab-pane active use-padding row" id="info">
				{!! Form::open(array('url'=>'bsetec/config/walletpoint/', 'class'=>'form-horizontal', 'parsley-validate', 'novalidate')) !!}
				<div class="col-sm-12">
				<div class="sbox   animated fadeInRight">
				<div class="sbox-title"> {!! Lang::get('core.wallet_point')!!} </div>
					<div class="sbox-content">
						<div class="form-group">
							<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.wallet_point')!!}  </label>
							<div class="col-sm-8">
							<input type="number" value="{{ $wallet_point['point'] }}" min="1" step="1" name="walletpoint" class="form-control" id="walletpoint" oninput="validity.valid||(value='');" placeholder="Wallet Point" required>
							</div>
						</div>
						<div class="form-group">
							<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.wallet_amount')!!}  </label>
							<div class="col-sm-8">
							<input type="number" value="{{ $wallet_point['amount'] }}" min="1" step="1" name="walletamount" class="form-control" id="walletamount" oninput="validity.valid||(value='');" placeholder="Wallet Amount" required>
							</div>
						</div>
						<div class="form-group">
							<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
							<div class="col-md-8">
							<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
							</div>
						</div>		  
					</div>
				</div>
				</div>
				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@stop




