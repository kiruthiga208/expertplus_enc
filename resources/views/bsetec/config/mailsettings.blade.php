@extends('layouts.app')

@section('content')


<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3><i class="fa fa-gears"></i> {!! Lang::get('core.E-Mail_Settings')!!} <small>{!! Lang::get('core.email_settings')!!} </small></h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.E-Mail_Settings')!!}</a></li>
			
		</ul>

	</div>

	<div class="page-content-wrapper">  
		@if(Session::has('message'))

		{{ Session::get('message') }}

		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>		
		<div class="block-content">
			@include('bsetec.config.tab')		
			<div class="tab-content m-t">
				<div class="tab-pane active use-padding row" id="info">	


					{!! Form::open(array('url'=>'bsetec/config/updatemailsettings/', 'class'=>'form-horizontal')) !!}
					
					<div class="col-sm-6">
						<div class="sbox   animated fadeInRight"> 
							<div class="sbox-title"> {!! Lang::get('core.E-Mail_Settings')!!} </div>
							<div class="sbox-content"> 	
								
								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4">{!! Lang::get('core.host')!!} </label>	
									<div class="col-sm-8">
										<input type="text" name="smtp_host" class="form-control" id="host" placeholder="{!! Lang::get('core.host')!!}" value="{!! $email_settings['smtp_host'] !!}">			
									</div>	
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4">{!! Lang::get('core.port')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="smtp_port" class="form-control " id="port" placeholder="{!! Lang::get('core.port')!!}" value="{!! $email_settings['smtp_port'] !!}">			
									</div>	
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4">{!! Lang::get('core.smtp_secure')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="smtp_secure" class="form-control " id="secure" placeholder="{!! Lang::get('core.smtp_secure')!!}" value="{!! isset($email_settings['smtp_secure']) ? $email_settings['smtp_secure'] : '' !!}">			
									</div>	
								</div> 


								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.smtp_username')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="smtp_username" class="form-control" id="username" placeholder="{!! Lang::get('core.smtp_username')!!}" value="{!! $email_settings['smtp_username'] !!}">			
									</div>	
								</div> 

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.smtp_password')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="smtp_password" class="form-control" id="password" placeholder="{!! Lang::get('core.smtp_password')!!}" value="{!! $email_settings['smtp_password'] !!}">			
									</div>	
								</div>  

								<div class="form-group">
									<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
									</div> 

								</div>	  
							</div>
						</div>
					</div>
					{!! Form::close() !!}

					<!-- test mail -->


					{!! Form::open(array('url'=>'bsetec/config/sendemail/', 'class'=>'form-horizontal')) !!}

					<div class="col-sm-6">
						<div class="sbox   animated fadeInRight"> 
							<div class="sbox-title"> {!! Lang::get('core.se_email')!!} </div>
							<div class="sbox-content"> 	

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.From') !!}  </label>	
									<div class="col-sm-8">
										<input type="email" name="femail" class="form-control" id="femail" placeholder="{!! Lang::get('core.from_email') !!}" required />				
									</div>	
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4">  {!! Lang::get('core.To') !!}  </label>	
									<div class="col-sm-8">
										<input type="email" name="temail" class="form-control" id="temail" placeholder="{!! Lang::get('core.to_email') !!}" required />				
									</div>	
								</div> 


								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.fr_emailsubject')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="subject" class="form-control" id="subject" placeholder="{!! Lang::get('core.fr_emailsubject')!!}">				
									</div>	
								</div> 

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.fr_emailmessage')!!}  </label>	
									<div class="col-sm-8">
										<textarea class="form-control" name="message" id="message"></textarea>			
									</div>	
								</div>  

								<div class="form-group">
									<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="submit">{!! Lang::get('core.sb_send')!!} </button>
									</div> 

								</div>	  
							</div>
						</div>
					</div>
					{!! Form::close() !!}

					<!-- test mail end -->


				</div>
			</div>
		</div>
	</div>

	@stop




