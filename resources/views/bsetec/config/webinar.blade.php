@extends('layouts.app')
@section('content')
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-info"></i> {!! Lang::get('core.bse_version')!!}  <sup> 5 </sup>  <small>{{ Lang::get('core.t_generalsettingsmall') }}</small></h3>
      </div>
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}">{{ Lang::get('core.t_generalsetting') }}</a></li>
	  </ul>	  
	 
    </div>
 	<div class="page-content-wrapper">   
	@if(Session::has('message'))
		{{ Session::get('message') }}	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('bsetec.config.tab')	
<div class="tab-content m-t">
<div class="tab-pane active use-padding" id="info">
  <div class="sbox"> 
  <div class="sbox-title"><h3>{!! Lang::get('core.webinar_settings')!!}</h3></div>
	<div class="sbox-content"> 
		{!! Form::open(array('url'=>'bsetec/config/webinar', 'class'=>'form-horizontal row', 'files' => true)) !!}
		<div class="col-sm-12 animated fadeInRight ">
			<div class="form-group">
			    <label for="user" class=" control-label col-md-4">{!! Lang::get('core.citrix_direct_user') !!}</label>
				<div class="col-md-8">
				<input name="citrix_direct_user" type="text" id="citrix_direct_user" class="form-control input-sm" required  value="{!! env('CITRIX_DIRECT_USER') !!}" />  
				 </div> 
		  </div>  
		</div>	
		<div class="col-sm-12 animated fadeInRight ">
			<div class="form-group">
			    <label for="password" class=" control-label col-md-4">{!! Lang::get('core.citrix_direct_password') !!} </label>
				<div class="col-md-8">
				<input name="citrix_direct_password" type="text" id="citrix_direct_password" class="form-control input-sm" required  value="{!! env('CITRIX_DIRECT_PASSWORD') !!}" />  
				 </div> 
		  </div>  
		</div>	
		<div class="col-sm-12 animated fadeInRight ">
			<div class="form-group">
			    <label for="consumer" class=" control-label col-md-4">{!! Lang::get('core.citrix_direct_consumer') !!} </label>
				<div class="col-md-8">
				<input name="citrix_direct_consumer" type="text" id="citrix_direct_consumer" class="form-control input-sm" required  value="{!! env('CITRIX_CONSUMER_KEY') !!}" />  
				 </div> 
		  </div>  
		</div>
		<div class="col-sm-12 animated fadeInRight ">
			<div class="form-group">
			    <label for="access" class=" control-label col-md-4">{!! Lang::get('core.access_token') !!}</label>
				<div class="col-md-8">
				<input name="access_token" type="text" id="access_token" class="form-control input-sm" required  value="{!! env('ACCESS_TOKEN') !!}" />  
				 </div> 
		  </div>  
		</div>
		<div class="col-sm-12 animated fadeInRight ">
			<div class="form-group">
			    <label for="organizer" class=" control-label col-md-4">{!! Lang::get('core.organizer_key') !!}</label>
				<div class="col-md-8">
				<input name="organizer_key" type="text" id="organizer_key" class="form-control input-sm" required  value="{!! env('ORGANIZER_KEY') !!}" />  
				 </div> 
		  </div>  
		</div>
		<div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 
		{!! Form::close() !!}
	</div>

</div>	
</div>
</div>
</div>
</div>
@stop