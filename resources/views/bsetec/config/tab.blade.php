<?php

$tabs = array(
		'' 		=> Lang::get('core.tab_siteinfo'),
		'email'			=> Lang::get('core.tab_email'),
		'security'		=> Lang::get('core.tab_loginsecurity') ,
		'translation'	=> Lang::get('core.tab_translation'),
		'log'			=> Lang::get('core.clear_cache'),
		'theme'			=> Lang::get('core.Theme_Management'),
		'payment'		=> Lang::get('core.Payment_Settings'),
		'mailsettings'	=> Lang::get('core.E-Mail_Settings'),
		'coursesettings'=> Lang::get('core.course_settings'),
		'awssettings'=> Lang::get('core.awss3_settings'),
		'membership'=> Lang::get('core.membership'),
		'walletpoint'		=> Lang::get('core.wallet_point')
	);

?>

<ul class="nav nav-tabs module_tab" >
@foreach($tabs as $key=>$val)
	<li  @if($key == $active) class="active" @endif><a href="{{ URL::to('bsetec/config/'.$key)}}"> {{ $val }}  </a></li>
@endforeach

</ul>
