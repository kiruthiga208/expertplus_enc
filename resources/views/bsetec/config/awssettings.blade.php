@extends('layouts.app')

@section('content')


<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3><i class="fa fa-gears"></i> {!! Lang::get('core.awss3_settings')!!}</h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.awss3_settings')!!}</a></li>

		</ul>

	</div>

	<div class="page-content-wrapper">
		@if(Session::has('message'))

		{{ Session::get('message') }}

		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
		<div class="block-content">
			@include('bsetec.config.tab')
			<div class="tab-content m-t">
				<div class="tab-pane active use-padding row" id="info">


					{!! Form::open(array('url'=>'bsetec/config/awssave/', 'class'=>'form-horizontal')) !!}

					<div class="col-sm-12">
						<div class="sbox   animated fadeInRight">
							<div class="sbox-title"> {!! Lang::get('core.awss3_settings')!!} </div>
							<div class="sbox-content">

								<div class="form-group hide">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.awss3_status')!!}  </label>
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="status" type="checkbox" id="status" value="1" @if(isset($awss3['status']) && $awss3['status'] == 'true') checked @endif />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.Key') !!} </label>
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="key" type="text" class="form-control" id="key" @if(isset($awss3['key'])) value="{!! $awss3['key'] !!}" @else value="" @endif />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.Secret') !!}  </label>
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="secret" type="text" class="form-control" id="secret" @if(isset($awss3['secret'])) value="{!! $awss3['secret'] !!}" @else value="" @endif />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.Region') !!}  </label>
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="region" type="text" class="form-control" id="region" @if(isset($awss3['region'])) value="{!! $awss3['region'] !!}" @else value="" @endif />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.Bucket') !!}  </label>
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="bucket" type="text" class="form-control" id="bucket" @if(isset($awss3['bucket'])) value="{!! $awss3['bucket'] !!}" @else value="" @endif />
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4">Status</label>	
									<div class="col-sm-8">
										<label class="checkbox">
										<input type="checkbox" name="enable" @if(isset($awss3['status'])) value="{!! $awss3['status'] !!}" @else value="" @endif @if(AWS_STATUS =='true') checked @endif /> 
										Enable
										</label>	
									</div>	
		                        </div> 
		  

								<div class="form-group">
									<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
									</div>

								</div>

							  
							</div>
						</div>
					</div>
					{!! Form::close() !!}




				</div>
			</div>
		</div>
	</div>

	@stop




