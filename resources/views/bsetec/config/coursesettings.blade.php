@extends('layouts.app')

@section('content')


<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3><i class="fa fa-gears"></i> {!! Lang::get('core.course_settings')!!}</h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
			<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.course_settings')!!}</a></li>
			
		</ul>

	</div>

	<div class="page-content-wrapper">  
		@if(Session::has('message'))

		{{ Session::get('message') }}

		@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>		
		<div class="block-content">
			@include('bsetec.config.tab')		
			<div class="tab-content m-t">
				<div class="tab-pane active use-padding row" id="info">	

				
					{!! Form::open(array('url'=>'bsetec/config/updatcoursesettings/', 'class'=>'form-horizontal')) !!}
					@if(count($auto)>0)
					@php ( $type = $auto['autoApprove'] )
					@endif

					@if(count($perpage)>0)
					@php ($pages = $perpage['paginationPerpage']  )
					@endif

					<div class="col-sm-12">
						<div class="sbox   animated fadeInRight"> 
							<div class="sbox-title"> {!! Lang::get('core.course_settings')!!} </div>
							<div class="sbox-content"> 	
								
								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.Auto_Approval')!!}  </label>	
									<div class="col-sm-8">
										<div class="checkbox">
											<input name="course_auto_approve" type="checkbox" id="course_auto_approve" value="1" @if ($type =='1') checked @endif />  
										</div>		
									</div>	
								</div>

								<div class="form-group">
									<label for="ipt" class=" control-label col-sm-4"> {!! Lang::get('core.course_pagination')!!}  </label>	
									<div class="col-sm-8">
										<input type="text" name="pagination_per_page" class="form-control " id="pagination_per_page" placeholder=" {!! Lang::get('core.course_pagination')!!} " value="{!! $pages or '' !!}">			
									</div>	
								</div> 

								

								<div class="form-group">
									<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
									<div class="col-md-8">
										<button class="btn btn-primary" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
									</div> 

								</div>	  
							</div>
						</div>
					</div>
					{!! Form::close() !!}

					


				</div>
			</div>
		</div>
	</div>

	@stop




