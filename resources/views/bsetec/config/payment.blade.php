@extends('layouts.app')


@section('content')
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/jquery.tinymce.js')!!}
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')!!}

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3><i class="fa fa-info"></i> {!! Lang::get('core.Payment_Settings')!!}</h3>
      </div>
	  
	 
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('config') }}">{!! Lang::get('core.Payment_Settings')!!}</a></li>
	  </ul>	  
    </div>

 	<div class="page-content-wrapper">   
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
	@include('bsetec.config.tab')	
<div class="tab-content m-t">
  <div class="tab-pane active use-padding" id="info">	
  <div class="sbox  "> 
  <div class="sbox-title"></div>
  <div class="sbox-content"> 
		 {!! Form::open(array('url'=>'bsetec/config/payment/', 'class'=>'form-horizontal row', 'files' => true)) !!}
		<div class="col-sm-6 animated fadeInRight ">
		@foreach($methods as $method_name => $method_values)
			<h2>{{ ucwords(str_replace('_', ' ', $method_name)) }}</h2>

			@foreach($method_values as $method => $method_value)
			  <div class="form-group">
			  <label for="ipt" class=" control-label col-md-4"> @if(($method!='payment') && ($method!='redirect_url') && ($method!='cancel_url' )){{ $method }}@endif </label>
				<div class="col-md-8">
					
					@if(in_array($method, array('TestMode', 'Status')))
						<input name="{{ $method_name }}[{{ $method }}]" type="hidden" value="false" />
						<input name="{{ $method_name }}[{{ $method }}]" type="checkbox" value="true"
						@if($method_value=='true') checked @endif
						  /> {!! Lang::get('core.fr_enable')!!}
						@elseif(in_array($method, array('payment')))
						<input name="{{ $method_name }}[{{ $method }}]" type="hidden" value="false" />
						@elseif(in_array($method,array('redirect_url') ))
						@elseif(in_array($method,array('cancel_url') ))
						@elseif(in_array($method,array('info')))
						<textarea name="{{ $method_name }}[{{ $method }}]" class="mceEditor form-control" style="width:100%" >{{ $method_value }}</textarea>

					@else
						<input name="{{ $method_name }}[{{ $method }}]" type="text" class="form-control input-sm"  value="{{ $method_value }}" />  
					@endif
				</div> 
			  </div>  
			@endforeach 

		@endforeach  
		  	  
		  
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 
		</div>

		
		 {!! Form::close() !!}
	</div>
	</div>	 
</div>
</div>
</div>
</div>



<script>
	$(document).ready(function() { 
		$(function(){
			tinymce.init({	
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				plugins : "openmanager",
				file_browser_callback: "openmanager",
				open_manager_upload_path: '../../../../../../../../uploads/images/',
				onchange_callback : "getcontent",
			});	
		});

	});
</script>




@stop
