<ul class="nav nav-tabs module_tab" style="margin-bottom:10px;">
  <li @if($active == 'config') class="active" @endif ><a href="{{ URL::to('bsetec/module/config/'.$module_name)}}">Info</a></li>
  <li @if($active == 'sql') class="active" @endif >
  <a href="{{ URL::to('bsetec/module/sql/'.$module_name)}}">SQL</a></li>
  <li @if($active == 'table') class="active" @endif >
  <a href="{{ URL::to('bsetec/module/table/'.$module_name)}}">Table</a></li>
  <li @if($active == 'form') class="active" @endif >
  <a href="{{ URL::to('bsetec/module/form/'.$module_name)}}">Form</a></li>
  
  <li @if($active == 'permission') class="active" @endif >
  <a href="{{ URL::to('bsetec/module/permission/'.$module_name)}}">Permission</a></li>
   <li @if($active == 'rebuild') class="active" @endif >
   <a href="javascript://ajax" onclick="bsetecModal('{{ URL::to('bsetec/module/build/'.$module_name)}}','Rebuild Module ')">Rebuild</a></li>
</ul>