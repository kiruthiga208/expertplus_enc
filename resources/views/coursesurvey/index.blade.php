@extends('layouts.app')

@section('content')
@php  usort($tableGrid, "SiteHelpers::_sort")@endphp
  <div class="page-content row course_survey">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> {{ Lang::get('core.dashboard') }} </a></li> 
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<!-- <div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div> -->
	</div>
	<div class="sbox-content"> 	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('coursesurvey/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('coursesurvey/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif			
		 
		</div> 		

	
	
	 {!! Form::open(array('url'=>'coursesurvey/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }}</th>
				<th> </th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
						<th>{{ Lang::get('core.contact') }}</th>
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /></td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>
					@if($t['field'] =='user_id')
						<select name="user_id" class="form-control">
						<option value=""> {{ Lang::get('core.select_user') }}  </option>
						  @foreach ($user_lists as $search)
						    <option value="{{ $search->userid }}">{{ $search->user_id }}</option>
						 @endforeach
						</select>
					@elseif($t['field'] =='course_id')
						<select name="course_id" class="form-control">
						<option value=""> {{ Lang::get('core.select_course') }} </option>
						 @foreach ($course_lists as $search)
						    <option value="{{ $search->course_id }}">{{ $search->course_title }}</option>
						 @endforeach
						</select>
					@elseif($t['field'] =='language_id')
						<select name="language_id" class="form-control">
						<option value=""> {{ Lang::get('core.Select_Language') }} </option>
						 @foreach ($language_lists as $search)
						    <option value="{{ $search->language_id }}">{{ $search->language }}</option>
						 @endforeach
						</select>
					@elseif($t['field'] =='category_id')
						<select name="category_id" class="form-control">
						<option value=""> {{ Lang::get('core.Select_category') }}</option>
						 @foreach ($category_lists as $search)
						    <option value="{{ $search->category_id }}">{{ $search->category }}</option>
						 @endforeach
						</select>
					@else
						{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}								
					@endif
					</td>
					@endif
				@endforeach
				<td> </td>
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info">  {{ Lang::get('core.GO') }}</button></td>
			 </tr>	        
						
            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->course_survey_id }}" />  </td>									
				 @foreach ($tableGrid as $field)
				 @php ($fieldname =$field['field'])
					 @if($field['view'] =='1')
					 <td>					 
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
						@elseif($fieldname=='course_id')
							{!! $row->course_title !!}
						@elseif($fieldname=='language_id')
							{!! $row->language !!}
						@elseif($fieldname=='category_id')
							{!! $row->category !!}
						@else
							@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ))
							{!! SiteHelpers::gridDisplay($row->$fieldname,$fieldname,$conn) !!}	
						@endif						 
					 </td>
					 @endif					 
				 @endforeach
				 <td> <a href="{{ URL::to('message?ctu='.$row->userid)}}" class="tips btn btn-xs btn-success" title="Contact Author" target="_blank"><i class="fa  fa-envelope "></i>{{ Lang::get('core.Contact_Author') }} </a></td>
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('coursesurvey/show/'.$row->course_survey_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('coursesurvey/update/'.$row->course_survey_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
												
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("coursesurvey/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
@stop