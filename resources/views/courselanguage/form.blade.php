@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('courselanguage?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		<?php
			if($row['id']!=''){
				$pgurl = 'courselanguage/save/'.$row['id'].'?return='.$return;
			}else{
				$pgurl = 'courselanguage/save?return='.$return;
			}

			if($row['status']!='' && $row['status']=='enable'){
				$status = 'checked="checked"';
			}else{
				$status = '';
			}

			
		?>

		 {!! Form::open(array('url'=>$pgurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><p><legend>{{ Lang::get('core.courselanguage') }} </legend></p>
								 {!! Form::hidden('id', $row['id'],array()) !!} 
								  <div class="form-group  " >
									<label for="Code" class=" control-label col-md-4 text-left"> {{ Lang::get('core.code') }} </label>
									<div class="col-md-6">
									  {!! Form::text('code', $row['code'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Language" class=" control-label col-md-4 text-left"> {{ Lang::get('core.language') }} </label>
									<div class="col-md-6">
									  {!! Form::text('language', $row['language'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Native Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.native_name') }} </label>
									<div class="col-md-6">
									  {!! Form::text('native_name', $row['native_name'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} </label>
									<div class="col-md-6">
									 <input type="checkbox" name="status" id="status" value="enable" {{$status}}> {{ Lang::get('core.Enable') }}
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								 <!--  <div class="form-group  " >
									<label for="Created At" class=" control-label col-md-4 text-left"> Created At </label>
									<div class="col-md-6">
									  
				{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Modified At" class=" control-label col-md-4 text-left"> Modified At </label>
									<div class="col-md-6">
									  
				{!! Form::text('modified_at', $row['modified_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div>  -->

								</fieldset>
			</div>
			
			
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8 btn-section">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('courselanguage?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop