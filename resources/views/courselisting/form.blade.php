@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('courselisting?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'courselisting/save/'.$row['course_id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> {{ Lang::get('core.courselisting') }} </legend>
								
							<div class="form-group  " >
							<label for="Course Id" class=" control-label col-md-4 text-left"> {!! Lang::get('core.course_id') !!} </label>
							<div class="col-md-6">
							{!! Form::text('course_id', $row['course_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="User Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.User_Id') }}</label>
							<div class="col-md-6">
							{!! Form::text('user_id', $row['user_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Cat Id" class=" control-label col-md-4 text-left">{{ Lang::get('core.CatID') }}</label>
							<div class="col-md-6">
							{!! Form::text('cat_id', $row['cat_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Lang Id" class=" control-label col-md-4 text-left">{{ Lang::get('core.lang_id') }}</label>
							<div class="col-md-6">
							{!! Form::text('lang_id', $row['lang_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Slug" class=" control-label col-md-4 text-left">{{ Lang::get('core.Slug') }}</label>
							<div class="col-md-6">
							{!! Form::text('slug', $row['slug'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Title" class=" control-label col-md-4 text-left">{{ Lang::get('core.course_title') }}</label>
							<div class="col-md-6">
							{!! Form::text('course_title', $row['course_title'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Subtitle" class=" control-label col-md-4 text-left"> {{ Lang::get('core.subtitle') }} </label>
							<div class="col-md-6">
							{!! Form::text('subtitle', $row['subtitle'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Description" class=" control-label col-md-4 text-left">{{ Lang::get('core.Description') }}</label>
							<div class="col-md-6">
							<textarea name='description' rows='2' id='description' class='form-control '  
			  >{{ $row['description'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Curriculum" class=" control-label col-md-4 text-left">{{ Lang::get('core.Curriculum') }}</label>
							<div class="col-md-6">
							<textarea name='curriculum' rows='2' id='curriculum' class='form-control '  
			  >{{ $row['curriculum'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Goal" class=" control-label col-md-4 text-left">{{ Lang::get('core.course_goal') }} </label>
							<div class="col-md-6">
							<textarea name='course_goal' rows='2' id='course_goal' class='form-control '  
			  >{{ $row['course_goal'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Int Audience" class=" control-label col-md-4 text-left"> {{ Lang::get('core.int_audience') }} </label>
							<div class="col-md-6">
							<textarea name='int_audience' rows='2' id='int_audience' class='form-control '  
			  >{{ $row['int_audience'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Req" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_req') }} </label>
							<div class="col-md-6">
							<textarea name='course_req' rows='2' id='course_req' class='form-control '  
			  >{{ $row['course_req'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Type" class=" control-label col-md-4 text-left"> {{ Lang::get('core.type') }} </label>
							<div class="col-md-6">
							{!! Form::text('type', $row['type'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Lang" class=" control-label col-md-4 text-left"> {{ Lang::get('core.lang') }} </label>
							<div class="col-md-6">
							{!! Form::text('lang', $row['lang'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Keywords" class=" control-label col-md-4 text-left"> {{ Lang::get('core.keywords') }} </label>
							<div class="col-md-6">
							<textarea name='keywords' rows='2' id='keywords' class='form-control '  
			  >{{ $row['keywords'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Tags" class=" control-label col-md-4 text-left"> {{ Lang::get('core.tags') }} </label>
							<div class="col-md-6">
							{!! Form::text('tags', $row['tags'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Image" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Image') }} </label>
							<div class="col-md-6">
							{!! Form::text('image', $row['image'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Video" class=" control-label col-md-4 text-left"> {{ Lang::get('core.video') }} </label>
							<div class="col-md-6">
							{!! Form::text('video', $row['video'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Test Video" class=" control-label col-md-4 text-left"> {{ Lang::get('core.test_video') }} </label>
							<div class="col-md-6">
							{!! Form::text('test_video', $row['test_video'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Video Type" class=" control-label col-md-4 text-left"> {{ Lang::get('core.video_type') }} </label>
							<div class="col-md-6">
							{!! Form::text('video_type', $row['video_type'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Privacy" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Privacy') }} </label>
							<div class="col-md-6">
							{!! Form::text('privacy', $row['privacy'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Pricing" class=" control-label col-md-4 text-left"> {{ Lang::get('core.pricing') }} </label>
							<div class="col-md-6">
							{!! Form::text('pricing', $row['pricing'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Survey" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Survey') }} </label>
							<div class="col-md-6">
							{!! Form::text('survey', $row['survey'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_status') }} </label>
							<div class="col-md-6">
							{!! Form::text('course_status', $row['course_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Level" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_level') }} </label>
							<div class="col-md-6">
							{!! Form::text('course_level', $row['course_level'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Manage Ins" class=" control-label col-md-4 text-left"> {{ Lang::get('core.manage_ins') }} </label>
							<div class="col-md-6">
							<textarea name='manage_ins' rows='2' id='manage_ins' class='form-control '  
			  >{{ $row['manage_ins'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Is Featured" class=" control-label col-md-4 text-left"> {{ Lang::get('core.is_featured') }} </label>
							<div class="col-md-6">
							{!! Form::text('is_featured', $row['is_featured'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Rating Count" class=" control-label col-md-4 text-left"> {{ Lang::get('core.rating_count') }} </label>
							<div class="col-md-6">
							{!! Form::text('rating_count', $row['rating_count'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Approved" class=" control-label col-md-4 text-left"> {{ Lang::get('core.approved') }}d </label>
							<div class="col-md-6">
							{!! Form::text('approved', $row['approved'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Con Analytic" class=" control-label col-md-4 text-left"> {{ Lang::get('core.con_analytic') }} </label>
							<div class="col-md-6">
							<textarea name='con_analytic' rows='2' id='con_analytic' class='form-control '  
			  >{{ $row['con_analytic'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Eng Analytic" class=" control-label col-md-4 text-left"> {{ Lang::get('core.eng_analytic') }} </label>
							<div class="col-md-6">
							<textarea name='eng_analytic' rows='2' id='eng_analytic' class='form-control '  
			  >{{ $row['eng_analytic'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Created At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Created_At') }} </label>
							<div class="col-md-6">
							
			{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Updated At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Updated_At') }} </label>
							<div class="col-md-6">
							
			{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Deleted At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Deleted_At') }} </label>
							<div class="col-md-6">
							
			{!! Form::text('deleted_at', $row['deleted_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('courselisting?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop