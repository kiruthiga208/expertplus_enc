
<div class="breadcrumb-c">
                  <div class="container">
                   <ul>
        <li><a href="{{ url('') }}">{{ Lang::get('core.home') }}</a></li>
		<li class="active"> <span>services</span></li>
     </ul>		
                  </div>
                  </div>	
                  
   <div class="services-s">
   <div class="container">
   <div class="page-title">
			<h3>services</h3>
		  </div>
          
          <p>Vestibulum nisi eros, aliquam sed dictum at, hendrerit posuere nunc. In at vehicula velit, ac iaculis erat. Curabitur vel mi lectus. Aliquam luctus congue luctus. Maecenas id ullamcorper urna, malesuada sollicitudin velit.  Fusce odio purus, efficitur in libero non, consequat luctus turpis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam lectus turpis, molestie non ex in, posuere laoreet enim. </p>
          </br>
          <p>Donec in mi sit amet mi eleifend posuere in ac nisl. Curabitur a ipsum quis erat egestas lacinia. Morbi convallis risus nec egestas volutpat. In egestas sit amet neque ornare feugiat. Phasellus vitae nunc erat. Nam suscipit vel mauris a condimentum. Nullam id mattis orci. Quisque orci magna, consequat eu pulvinar vel, fringilla vestibulum sapien. Ut rhoncus ligula eget velit bibendum hendrerit. Suspendisse condimentum blandit purus, sed porta eros hendrerit in. Fusce ac erat est. </p> </br>
           <p>Suspendisse dignissim quis ex iaculis efficitur. Donec id lobortis lectus. Donec et dui nulla. Quisque varius volutpat erat, volutpat dictum augue euismod vel. Sed imperdiet sapien eget eros interdum, ultrices suscipit dui pellentesque. Fusce sit amet ante ut est rutrum laoreet. Sed aliquam sed est id porta. Vestibulum lacinia libero leo, id fermentum urna posuere nec. Morbi libero tellus, pretium quis elit ac, molestie elementum metus. Curabitur tempor nulla purus, et suscipit quam ultricies non. Praesent porta aliquet euismod. Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula. </p>
          
   <div class="services-c">
   <div class="row">
   <div class="col-sm-4">
  <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-7.png')}}"></a>
   <h3>Keeping Business Data</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   <div class="col-sm-4">
   <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-8.png')}}"></a>
   <h3>24 Hour Support</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   <div class="col-sm-4 third">
   <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-9.png')}}"></a>
   <h3>Well Analyzing Team</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   <div class="col-sm-4">
   <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-10.png')}}"></a>
   <h3>Better Communication</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   <div class="col-sm-4">
   <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-11.png')}}"></a>
   <h3>Client Managment</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   <div class="col-sm-4 eight">
   <div class="services-block">
   <a href="javascript::"><img src="{{asset('assets/bsetec/images/img-12.png')}}"></a>
   <h3>Data Entry</h3>
   <p>Duis a nisi ut nisl efficitur tristique. Nam elementum nec elit at venenatis. Duis dignissim elit quam, quis tempor augue lobortis a. Aliquam nec vehicula quam. Vivamus pharetra varius vehicula...</p>
   </div>
   </div>
   
   
   </div>
   </div>
   
   <div class="process-model">
   <h3>PROCESS MODEL</h3>
   <p>Our process on creating awesome projects</p>
   
   <div class="step-method clearfix">
   <ul>
   <li><span>1. Envisioning</span></li>
   <li><span>2. Planning</span></li>
   <li><span>3. Development</span></li>
   <li><span>4. STABILIZATION</span></li>
   </ul>
   </div>
   </div>
   <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
   
   </div>
   
   </div>               
                  
    
    
    <script type="text/javascript">
$(function(){
		$('body').removeClass();
		$('body').addClass('service-p');
		$('#front-header').addClass('front-header');
		});
</script>