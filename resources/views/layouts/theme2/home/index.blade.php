<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {{ CNF_APPNAME }} | {{ $pageTitle}} </title>
<!-- ajax _token passing -->
<meta name="_token" content="{!! csrf_token() !!}"/>
<meta name="_site_name" content="{!! CNF_APPNAME !!}"/>

<meta name="keywords" content="{{ $pageMetakey }}">
<meta name="description" content="{{ $pageMetadesc }}"/>
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic' rel='stylesheet' type='text/css'>
	<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		
	<link href="{{ asset('assets/bsetec/js/plugins/all/common.css')}}" rel="stylesheet">
	<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/bootstrap-form/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/bsetec/rs-plugin/css/settings.css')}}" media="screen" />
        <link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/css/bsetec.css')}}" rel="stylesheet">	
	<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">
       
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/all/common.js') }}" ></script>	
	<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/jquery.mixitup.min.js') }}"></script>		
        <script type="text/javascript" src="{{asset('assets/bsetec/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
        <script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>
        
<noscript><meta http-equiv="refresh" content="0;url={{ URL::TO('nojs.php') }}"></noscript>
        <script src="{{ asset('assets/bsetec/js/common.js') }}" ></script>


		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- bootstrap form helper -->
		  
		
		<!-- ajax _token meta passing -->		
		<script type="text/javascript">
		$.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
		</script>
        

  	</head>

<body >
<div id="front-header">
@include('layouts/header')		
</div>		
<div class="expert-plus" id="expert_plus_main">
@include($pages)
</div>

<div class="clr"></div>
	

@include('layouts/footer')	

<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-default">
		
			<button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Modal title</h4>
		</div>
		<div class="modal-body" id="bsetec-modal-content">
	
		</div>
	</div>
</div>
</div>
@yield('css')
@yield('javascript')
{{ Sitehelpers::showNotification() }} 
<script type="text/javascript" >
$(".lazy").lazyload({effect:"fadeIn"});
$('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('/').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('/').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('/').'/assets/bsetec/static/img/star-half.png'}}"});
</script>
</body> 
</html>