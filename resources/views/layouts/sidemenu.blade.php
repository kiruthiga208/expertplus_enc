	  
	  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,600' rel='stylesheet' type='text/css'>
	  <?php $sidebar = SiteHelpers::menus('sidebar') ;?>
<nav role="navigation" class="navbar-default navbar-static-side admin-sidebar">
     <div class="sidebar-collapse">				  
       <ul id="sidemenu" class="nav expanded-menu">
		<li class="logo-header" >
		 <a class="navbar-brand" href="{{ URL::to('dashboard')}}">
		 	@if(file_exists('./uploads/images/'.CNF_LOGO) && CNF_LOGO !='')
		 	<img src="{{ asset('uploads/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
		 	@else
			<img src="{{ asset('assets/bsetec/images/logo.png')}}" alt="{{ CNF_APPNAME }}" />
			@endif
		 </a>
		</li>
		@foreach ($sidebar as $menu)
			 <li @if(Request::is($menu['module'])) class="active" @endif>
			 	<a 
					@if($menu['menu_type'] =='external')
						href="{{ URL::to ($menu['url']) }}" 
					@else
						href="{{ URL::to($menu['module'])}}" 
					@endif				
			 	
				 @if(count($menu['childs']) > 0 ) class="expand level-closed" @endif>
				 	<i class="{{$menu['menu_icons']}}"></i> <span class="nav-label">
					
					@if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][CNF_LANG]))
						{{ $menu['menu_lang']['title'][CNF_LANG] }}
					@else
						{{$menu['menu_name']}}
					@endif						
					</span> @if(count($menu['childs']) > 0 )<span class="fa arrow"></span>@endif
				</a> 
				@if(count($menu['childs']) > 0)
					<ul class="nav nav-second-level">
						@foreach ($menu['childs'] as $menu2)
						 <li @if(Request::is($menu2['module'])) class="active" @endif>
						 	<a 
								@if($menu2['menu_type'] =='external')
									href="{{ URL::to ($menu2['url'])}}" 
								@else
									href="{{ URL::to($menu2['module'])}}"  
								@endif									
							>
							<i class="{{$menu2['menu_icons']}}"></i>
							@if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][CNF_LANG]))
								{{ $menu2['menu_lang']['title'][CNF_LANG] }}
							@else
								{{$menu2['menu_name']}}
							@endif	
							</a> 
							@if(count($menu2['childs']) > 0)
							<ul class="nav nav-third-level">
								@foreach($menu2['childs'] as $menu3)
									<li @if(Request::is($menu3['module'])) class="active" @endif>
										<a 
											@if($menu['menu_type'] =='external')
												href="{{ URL::to ($menu3['url']) }}" 
											@else
												href="{{ URL::to($menu3['module'])}}" 
											@endif										
										
										>
										<i class="{{$menu3['menu_icons']}}"></i> 
										@if(CNF_MULTILANG ==1 && isset($menu3['menu_lang']['title'][CNF_LANG]))
											{{ $menu3['menu_lang']['title'][CNF_LANG] }}
										@else
											{{$menu3['menu_name']}}
										@endif											
											
										</a>
									</li>	
								@endforeach
							</ul>
							@endif							
						</li>							
						@endforeach
					</ul>
				@endif
			</li>
		@endforeach
      </ul>
	</div>
</nav>	  
	  
