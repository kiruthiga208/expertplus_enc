<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {{ CNF_APPNAME }} </title>
<meta name="keywords" content="">
<meta name="description" content=""/>
<!-- ajax _token passing -->
<meta name="_token" content="{!! csrf_token() !!}"/>

<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	
		<link href="{{ asset('assets/bsetec/js/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet"> 
		<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/bootstrap.summernote/summernote.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/select2/select2.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/css/bsetec.css')}}" rel="stylesheet">		
		<link href="{{ asset('assets/bsetec/css/animate.css')}}" rel="stylesheet">		
		<link href="{{ asset('assets/bsetec/css/icons.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
		


		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.cookie.js') }}"></script>			
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery-ui.min.js') }}"></script>				
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/select2/select2.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/prettify.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/parsley.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/switch.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap/js/bootstrap.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/bsetec.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.form.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.jCombo.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.summernote/summernote.min.js') }}"></script>
		

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->	
		
		<!-- ajax _token meta passing -->		
		<script type="text/javascript">
		$.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
		</script>	


	
  	</head>
  	<body class="sxim-init" >
<div id="wrapper">
	@include('layouts/sidemenu_clone')
	<div class="gray-bg " id="page-wrapper">
		@include('layouts/header_menu_clone')

		@yield('content')		
	</div>

</div>

<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
	<div class="modal-header bg-default">
		
		<button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title">Modal title</h4>
	</div>
	<div class="modal-body" id="bsetec-modal-content">

	</div>

  </div>
</div>
</div>
{{ Sitehelpers::showNotification() }} 
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-10127696-22', 'auto');
  ga('send', 'pageview');
jQuery(document).ready(function ($) {

    $('#sidemenu').sximMenu();


		
});	
	
	
</script>
</body> 
</html>