<div class="row  ">
	<nav style="margin-bottom: 0;" role="navigation" class="navbar navbar-static-top gray-bg admin_right_content">
		<div class="navbar-header">
			<a href="javascript:void(0)" class="navbar-minimalize minimalize-btn btn btn-primary "><i class="fa fa-bars"></i> </a>

		</div>
		<div class="admin-right-menu">
			<ul class="nav navbar-top-links navbar-right">
				<li><a href="javascript::" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>{!! \Session::get('fid') !!}</a>
					<ul class="dropdown-menu dropdown-menu-right icons-right">

						<li><a href="{{ URL::to('')}}" target="_blank"><i class="fa fa-desktop"></i>  {!! Lang::get('core.main_site')!!} </a></li>
						<li><a href="{{ URL::to('user/profile')}}"><i class="fa fa-user"></i> {{ Lang::get('core.m_profile') }}</a></li>
	
					</ul>
				</li>

				<li><a href="{{ URL::to('user/logout')}}"><i class="fa fa-power-off"></i>{{ Lang::get('core.m_logout') }}</a></li>
			</ul>
		</div>


	</nav>
</div>