<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {{ CNF_APPNAME }} | {{ $pageTitle}} </title>
<!-- ajax _token passing -->
<meta name="_token" content="{!! csrf_token() !!}"/>
<meta name="_site_name" content="{!! CNF_APPNAME !!}"/>

<meta name="keywords" content="{{ $pageMetakey }}">
<meta name="description" content="{{ $pageMetadesc }}"/>
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	

        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700,700italic,400italic' rel='stylesheet' type='text/css'>
		<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/bootstrap.summernote/summernote.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/themes/theme1/css/animate.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/css/icons.min.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/themes/theme1/js/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/themes/theme1/js/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
		<link href="{{ asset('assets/bsetec/js/plugins/select2/select2.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/redactor/css/redactor.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/bootstrap-form/css/bootstrap-formhelpers.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('assets/bsetec/css/jquery.mCustomScrollbar.min.css')}}" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/bsetec/rs-plugin/css/settings.css')}}" media="screen" />
        <link href="{{ asset('assets/bsetec/css/loadingbar.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/css/owl.theme.css') }}" rel="stylesheet"> 
        <link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/bsetec/css/bsetec.css')}}" rel="stylesheet">	
		<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">
       
       
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.cookie.js') }}"></script>			
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery-ui.min.js') }}"></script>	
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/prettify.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/parsley.js') }}"></script>	
        <script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/jquery.mixitup.min.js') }}"></script>				
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/redactor/js/redactor.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/select2/select2.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap-form/js/bootstrap-formhelpers.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/static/js/imagesloaded.pkgd.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/switch.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/bsetec.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.form.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.jCombo.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.summernote/summernote.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/jquery.mCustomScrollbar.js') }}"></script>	
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/magic-line.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/jquery.bxslider.min.js') }}"></script>      
        <script type="text/javascript" src="{{asset('assets/bsetec/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/bsetec/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{ asset('assets/bsetec/static/js/jquery.raty.min.js') }}"></script>
        <script src="{{ asset('assets/bsetec/js/owl.carousel.js') }}"></script>	
        <script src="{{ asset('assets/bsetec/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('assets/bsetec/js/additional-methods.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/jquery.history.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/custom.js') }}"></script>	

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- bootstrap form helper -->
		  
		
		<!-- ajax _token meta passing -->		
        

  	</head>

<body >
<div id="front-header">
@include('layouts/header')		
</div>		
<div class="expert-plus" id="expert_plus_main">
@include($pages)
</div>

<div class="clr"></div>
	

@include('layouts/footer')	

<div class="modal fade" id="bsetec-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-default">
		
			<button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">Modal title</h4>
		</div>
		<div class="modal-body" id="bsetec-modal-content">
	
		</div>
	</div>
</div>
</div>
@yield('css')
@yield('javascript')
{{ Sitehelpers::showNotification() }}
<script type="text/javascript" >
$(".lazy").lazyload({effect:"fadeIn"});
</script>
</body> 
</html>