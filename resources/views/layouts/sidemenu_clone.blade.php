<link href="{{ asset('assets/bsetec/css/datatable.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/bsetec/js/jquery.dataTables.min.js') }}"></script>
<nav role="navigation" class="navbar-default navbar-static-side admin-sidebar">
	<div class="sidebar-collapse">				  
		<ul id="sidemenu" class="nav expanded-menu">
			<li class="logo-header">
				<a class="navbar-brand" href="{!! url('').'/businessplan/courses'!!}">
					@if(file_exists('./uploads/images/'.CNF_LOGO) && CNF_LOGO !='')
				 	<img src="{{ asset('uploads/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
				 	@else
					<img src="{{ asset('assets/bsetec/images/logo.png')}}" alt="{{ CNF_APPNAME }}" />
					@endif
				</a>
			</li>
			
			@if(\Request::segment(2)=='courses' || \Request::segment(2)=='assignusers' || \Request::segment(2)=='assignuserslist' ||  \Request::segment(2)=='courseprogress' ||  \Request::segment(2)=='userslistbycategory' ||  \Request::segment(2)=='assignuserbycategory' || \Request::segment(2)=='userslistbysubscription' ||  \Request::segment(2)=='assignuserbysubscription' ||  \Request::segment(2)=='assignuserbycbudle' ||  \Request::segment(2)=='userslistbycbundle')
			{{--*/  $cls1 = 'active';   /*--}}
			@endif
			@if(\Request::segment(2)=='students' || \Request::segment(2)=='studentform' || \Request::segment(2)=='studentsum' || \Request::segment(2)=='trainingplan')
			{{--*/  $cls2 = 'active';   /*--}}
			@endif
			@if(\Request::segment(2)=='transaction')
			{{--*/  $cls3 = 'active';   /*--}}
			@endif

			@if(\Request::segment(2)=='mydashboard')
			{{--*/  $cls4 = 'active';   /*--}}
			@endif

			

			<!-- <li class="{!! $cls4 or '' !!}"><a href="{!! url('').'/businessplan/mydashboard'!!}"><i class="fa fa-tachometer"></i>  <span class="nav-label"> {!! Lang::get('core.my_dashboard') !!} </span></a></li>-->
			<li class="{!! $cls1 or '' !!}"><a href="{!! url('').'/businessplan/courses'!!}"><i class="fa fa-list"></i>  <span class="nav-label"> {!! Lang::get('core.my_course') !!} </span></a></li> 
			<li class="{!! $cls2 or '' !!}"><a href="{!! url('').'/businessplan/students'!!}"><i class="fa fa-users"></i>  <span class="nav-label">{!! Lang::get('core.my_students') !!} </span></a></li>
			<!-- <li class="{!! $cls3 or '' !!}"><a href="{!! url('').'/businessplan/transaction'!!}"><i class="fa fa-usd"></i>  <span class="nav-label"> {!! Lang::get('core.my_history') !!} </span></a></li> -->

		</ul>
	</div>
</nav>