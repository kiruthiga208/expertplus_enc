<div class="recent_block">
<div class="container">
   <div class="row">
   <div class="col-sm-4">
   <div class="expert_footer_blog">
   <h3>{!! Lang::get('core.recent') !!}<span>{!! Lang::get('core.Posts') !!}</span></h3>
   @php ( $blog = bsetecHelpers::recentBlogs() )
   @if(count($blog)>0)
   @foreach($blog as $blogs)
   <div class="recent_sub">
   <div class="row">
   <div class="col-xs-3"><div class="user-img"><a href="{!! url('').'/blogs/read/'.$blogs->slug !!}" title="{!! $blogs->title !!}">
   <img class="lazy" src="{{asset('assets/bsetec/images/spacer.gif')}}" data-src="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($blogs->blogID)}}" data-original="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($blogs->blogID)}}">
   </a>
   </div></div>
   <div class="col-xs-9"><h4><a href="{!! url('').'/blogs/read/'.$blogs->slug !!}" title="{!! $blogs->title !!}">{!! $blogs->title  !!}</a></h4>
   <p class="desc">{!! substr(ucfirst(strip_tags($blogs->content)),0,50) !!}...<span><a href="{!! url('').'/blogs/read/'.$blogs->slug !!}" title="{!! $blogs->title !!}"> {!! Lang::get('core.more') !!}</a></span></p>
   <p class="comments">{!! date('M j, Y',strtotime($blogs->created)) !!} , {!! $blogs->comments !!} {!! Lang::get('core.comments') !!} </p>
   </div>
   </div>
   </div>
   @endforeach
   <a href="{!! url('').'/blogs' !!}" class="view_more">{!! Lang::get('core.more_view') !!}</a>
   @else
   <div class="empty_msg">{!! Lang::get('core.no_blogs') !!}</div>
   @endif
  
   </div>
   </div>
   <div class="col-sm-4">
   @php ( $page_contents = \bsetecHelpers::get_options('page_contents') )
    
    @if(count($page_contents)>0)
        @if(isset($page_contents['advertising_footer_content']))
            {!! $page_contents['advertising_footer_content'] !!}
        @endif
    @endif
   </div>
   <div class="col-sm-4">
   <h3>{!! Lang::get('core.top') !!}<span>{!! Lang::get('core.rated') !!}</span></h3>
   @php ( $rated = bsetecHelpers::getToprated() )
   @if(count($rated)>0)
   @foreach($rated as $rated)
   <div class="recent_sub">
   <div class="row">
   <div class="col-xs-3"><div class="user-img"><a href="{{ url('courseview/'.$rated->course_id.'/'.$rated->slug) }}" title="{!! $rated->course_title !!}"><img src="{{asset('assets/bsetec/images/spacer.gif')}}" class="lazy" data-src="{{ \bsetecHelpers::getImage($rated->image) }}" data-original="{{ \bsetecHelpers::getImage($rated->image) }}" ></a></div></div>
   <div class="col-xs-9"><h4><a href="{{ url('courseview/'.$rated->course_id.'/'.$rated->slug) }}" title="{!! $rated->course_title !!}">{!! substr(ucfirst($rated->course_title),0,30) !!}</a></h4>
   <p class="desc">{!! substr(ucfirst(strip_tags($rated->description)),0,50) !!}...<span><a href="{{ url('courseview/'.$rated->course_id.'/'.$rated->slug) }}" title="{!! $rated->course_title !!}"> {!! Lang::get('core.more') !!}</a></span></p>
   <p class="comments">{!! date('M j, Y',strtotime($rated->created_at)) !!}, {!! $rated->ratings_count !!}  {!! Lang::get('core.comments') !!} </p>
   </div>
   </div>
   </div>
  @endforeach 
  <a href="{{ URL::to('topRated') }}" class="view_more">{!! Lang::get('core.more_view') !!}</a>
   @else
   <div class="empty_msg">{!! Lang::get('core.no_top') !!}</div>
   @endif
   </div>
   </div>
   </div>
   </div>
<footer>
<div id="footer">
	<div class=" container">
   <div class="row">
    <div class="col-sm-5">
        <p>{!! Lang::get('core.copyrights')!!} &copy; {!! date('Y') !!}, {!! Lang::get('core.rights')!!} {{ CNF_APPNAME }}</p>
        </div>
     <div class="col-sm-7">  
     <ul class="footer_menu clearfix">
      @foreach(\SiteHelpers::menus() as $menu)
      @if($menu['menu_type'] == "external")
      <li><a href="{{ url($menu['url']) }}">{{$menu['menu_name']}}</a></li>
      @else
      <li><a href="{{url::to($menu['module'])}}">{{$menu['menu_name']}}</a></li>
     @endif
     @endforeach
     </ul>
     </div> 
  </div>
		
	</div>	
</div>
</footer>
<?php /* ?> 
<!-- reset demo code start -->
<style>

.reset_nodification {
  position:fixed;
  left: 50%;
  margin: 0 0 0 -140px;
  text-align: center;
  top: 0;
  width: 250px;
  z-index: 1030;
}

#countdownToMidnight {
  background: #000000 none repeat scroll 0 0;
  color: #ffffff;
  display: block;
  font: 13px Arial,Helvetica,sans-serif;
  margin: 0 auto;
  padding: 2px;
  width: 250px;
}


</style>

<script>
var demo_reset_domain = '{!! url("home/check-time") !!}';
function initJQuery() {
  $(function() {
    check_demo_reset();
  });
}
initJQuery();

function check_demo_reset(){
if($('.reset_nodification').length==0){
$('body').append('<div class="reset_nodification"><span id="countdownToMidnight"><span>This demo will reset in </span><span class="reset_timer_time">-- : -- : --</span></span></div>');
}

$.get( demo_reset_domain, function( data ) {
var left_time = parseInt(data);
if(left_time>0){

setTimeout(function(){
display_timer(left_time-1);
}, 1000);

setTimeout("reload_page()", 1000*(left_time+1)); // a little late timeout to make sure old page expires.
} else {
reload_page();
}

});
}

function display_timer(left_time){
$('.reset_nodification').html('<span id="countdownToMidnight"><span>This demo will reset in </span><span class="reset_timer_time">'+reset_secondsToHms(left_time)+'</span></span>');
setTimeout(function(){
display_timer(left_time-1);
}, 1000);
}


function reload_page(){
$('.reset_nodification').hide();
$.get(demo_reset_domain, function( data ) {
setTimeout("reset_reload_final_func()", 1000);
});
}


function reset_reload_final_func(){
document.location.href=document.location.href;
}



function reset_secondsToHms(d) {
d = Number(d);
var h = Math.floor(d / 3600);
var m = Math.floor(d % 3600 / 60);
var s = Math.floor(d % 3600 % 60);
return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
}
</script>


<div class="reset_nodification">
  <span id="countdownToMidnight">
    <span>This demo will reset in </span>
    <span class="reset_timer_time">-- : -- : --</span>
  </span>
</div>
<!-- reset demo code end -->
<?php */ ?>