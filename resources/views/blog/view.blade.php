
<div class="breadcrumb-c">
      <div class="container">
       <ul>
			<li><a href="{!! URL::to('') !!}">{!! Lang::get('core.Home') !!}</a> </li> 
			<li><a href="{!! URL::to('blogs') !!}">{!! Lang::get('core.Blog') !!}</a></li> 
			<li><a href="{!! URL::to('blogs/category/'.$row->alias) !!}">{!! $row->alias !!}</a></li> 
			<li class="active"> <span> {!! $row->title !!}	</span> </li>
		</ul>		
      </div>
</div>

<div class="page-content container">
		<ul class="parsley-error-list">
			@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	
		</ul>
	<div class="row">
		<div class="col-sm-9">
        @if(!Auth::check())
			<div class="alert alert-danger"> {!! Lang::get('core.blog_hint') !!} </div>
		@endif
        <div class="page-title">
						<h3 class="blog-title"> {!! $row->title !!}</h3>
					</div>
			<div class="blog-post">
				<div class="post-item">
                <div class="row">
                <div class="col-sm-12">
					<div class="view-block right-content-b">
					<div class="blog-info-small clearfix">
                    <ul>
                    <li>
						<i class="fa fa-user icon-muted"></i>  <span>  {!! $row->username !!} </span>   </li>
						<li><i class="fa fa-calendar icon-muted"></i>  <span> {!! date("M j, Y " , strtotime($row->created)) !!} </span> </li>
						<li><i class="fa fa-comment-o icon-muted"></i>   <span>  {!! $row->comments !!} {!! Lang::get('core.commentt') !!}  </span> </li>
                        </ul>
					</div>	
                    <div class="blog-img">
                    <img class="lazy" src="{{ \URL::to('bsetec/images/spacer.gif') }}" data-src="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($row->blogID)}}" data-original="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($row->blogID)}}" />                    
                    </div>			
					<div class="summary">{!! SiteHelpers::renderHtml( str_replace('<hr />',"",$row->content)) !!}</div>
                    </div>
                    </div>
                </div>
				</div>	
			</div>
			
      <div class="total-cmt">
			<h3 id="comments" class="comments-b"> ( {!! $row->comments !!} ) {!! Lang::get('core.commentt') !!}</h3>
            </div>
			
			<div class="comment-list">
				@foreach($comments as $com)

				<div class="comm" >

					
					<blockquote>{!! SiteHelpers::BBCode2Html($com['comment']) !!}</blockquote>
                    <div class="created-block clearfix">
                    <p class="created"> {!! date("F j, Y " , strtotime($com['created'])) !!} | <a href="{{ URL::to('profile/'.$com['username'],'') }}" target="_blank" title="{!! $com['name'] !!}">{!! $com['name'] !!}</a></p>
					
					@if(Session::get('uid') == $com['user_id'])
					<div class="blog_editblocks"><a title="{!! Lang::get('core.btn_remove') !!}" href="{!! URL::to('blog/removecomm/'.$com['commentID'].'/'.$row->slug) !!}" onclick="return confirm('Are you sure want to delete?')" class=""> <i class="fa fa-trash-o"></i> </a></div>
					<div class="blog_editblocks">  <a title="{!! Lang::get('core.btn_edit') !!}" class="btn btn-info padding_width editt" data-blogid="{{$com['blogID']}}" data-cid="{{$com['commentID']}}" data-ti="" data-des="{!! $com['comment'] !!}" data-toggle="modal" data-target="#myModal">
		        	<i class="fa fa-pencil-square-o "></i>
		    		</a></div>
					@endif	
                    </div>
				</div>


				@endforeach	


			</div>
			<div class="pull-right">
				
			</div>		
			@if(Auth::check())
			{!! Form::open(array('url'=>'blogs/savecomment/',  'parsley-validate'=>'','novalidate'=>' ')) !!}
			<div class="form-group pull-in clearfix hidden" > 
				<div class="col-sm-6"> <label>{!! Lang::get('core.Your_Name')!!}</label> <input type="text" placeholder="Name" class="form-control"> </div> 
				<div class="col-sm-6"> <label>{!! Lang::get('core.Email')!!}</label> <input type="email" placeholder="Enter email" class="form-control"> </div> 
			</div> 
			<div class="form-group"> 
				<div class="text-b">
					<label>{!! Lang::get('core.Post_Your_Comment')!!}</label>
					<textarea placeholder=" {!! Lang::get('core.Type_your_comment')!!}" rows="5" id="comment_area" name="comment" class="form-control markItUp"></textarea> 
				</div>
			</div>
		
			<div class="form-group"> 

					<button class="btn btn-color" type="submit">{!! Lang::get('core.Submit_comment')!!}</button> 
				
			</div> 
			<input type="hidden" name="blogID" value="{!! $id !!}" />
			<input type="hidden" name="alias" value="{!! $alias !!}" />
			{!! Form::close() !!}	
			@else 
			
			@endif	

		</div>
		

		<div class="col-sm-3">
			@include('blog.sidebar')
		</div>

	</div>
	
</div>
<!-- modal content-->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog blog-popup">

    <!-- Modal content-->
    <span ></span>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!! Lang::get('core.blogedit') !!}</h4>
      </div>
      <div class="modal-body">
      	{!! Form::open(array('url'=>'blogs/updatecomm/', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
    	<input type="hidden" name="blogid" class="blogid" value=""/>
    	<input type="hidden" name="commentid" class="commentid" value=""/>
    	<input type="hidden" name="user_id" value="{{Session::get('uid')}}" />
    	<input type="hidden" name="slug" value="{{$row->slug}}"/>
    	<div class="form-group">
           <div class="row">
           <div class="col-sm-4">
				<label for="editor" class=" control-label text-left"> Description <span class="asterisk"> * </span></label>
                </div>
				<div class="col-sm-8">
				<div class="adjoined-bottom">
					<div class="grid-container">
						<div class="grid-width-50">
							<textarea id="editor" name="content"  class="comments" required></textarea>
						</div>
					</div>
				</div>
				  
				 </div> 
            </div>
			</div> 
	
			  <div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
				<button type="submit" name="submit" class="btn btn-color" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				</div>	  
	    </div> 
      	{!! Form::close() !!}
      </div>
     
    </div>

  </div>
</div>

<script type="text/javascript" >
$(function(){
	$('.editt').click(function(){
		var blogid=$(this).attr('data-blogid');
        var commentid=$(this).attr('data-cid');
        var des=$(this).attr('data-des');
        $('.blogid').val(blogid);
        $('.commentid').val(commentid);
        $('.comments').val(des);
	});
});

$(function() {
     // $(".markItUp").markItUp(mySettings );
	 $('body').removeClass();
	 $('body').addClass('blog-s');
     $(".fade").fadeOut(3000);
	 $('#front-header').addClass('front-header');
 });
</script>
