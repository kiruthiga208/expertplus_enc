{!! Html::style('resources/protected/resources/views/blog/blog.css') !!}	
<div class="breadcrumb-c">
  <div class="container">
   <ul>
	<li><a href="{{ url('') }}">{{ Lang::get('core.Home') }}</a></li>
	@if($pagecategory == '') 
		<li class="active"><span>{!!  $pageTitle !!}</span></li>
	@else
	<li><a href="{!! url('blogs') !!}">{!! $pageTitle !!}</a></li>	
		<li class="active"><span> {!! $pagecategory !!}</span></li>	
		@endif
  </ul>
  </div>
  </div>                               
<div class="blog-t">
  <div class="container">
	<div class="row">
	@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	
	<div class="col-sm-9">
    <div class="blog-list-section" id="blog-list-section">
    <div class="page-title">
					<h3 class="blog-title"> {{ $pageTitle }}</h3>
				  </div>
	@php $i=0; $rowDataa = $rowData;  @endphp
	 @foreach ($rowData as $row)
		<div class="blog-post">
			<div class="post-item">
            <div class="row">
            <div class="col-sm-6 col-md-4 col-md-4">
				<div class="blog-img">
				<img class="lazy" data-src="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($row->blogID)}}" data-original="{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($row->blogID)}}" src="{{asset('assets/bsetec/images/spacer.gif')}}"  style="background:url('{{ url('uploads/blog')}}/{{\bsetecHelpers::getBlogimage($row->blogID)}}'); background-repeat:no-repeat; background-position:center center; background-size:cover;"/>
				</div>
                </div>
                <div class="col-sm-6 col-md-8 col-md-8">
                <div class="right-content-b">
				<div class="title"><h3><a class="blog-view" href="{{ URL::to('blogs/read/'.$row->slug)}}"> {!! $row->title !!} </a></h3></div>
				<div class="blog-info-small clearfix">
                <ul>
                <li>
					<i class="fa fa-folder icon-muted"></i> <span>  <a href="{{ URL::to('blogs/category/'.$row->alias)}}">{!! $row->name!!}</a>  </span> 
                    </li>
                    <li>
					<i class="fa fa-user icon-muted"></i> <span>{!!Lang::get('core.By')!!} &nbsp;&nbsp;<a href="{{ URL::to('profile/'.$row->uname) }}">{!! $row->username !!} </a></span> </li> 
					<li><i class="fa fa-calendar"></i> <span> {!! date("M j, Y " , strtotime($row->created)) !!} </span> </li>
					<li><i class="fa fa-comments"></i> <span> <a href="{{ URL::to('blogs/read/'.$row->slug.'#comments')}}" class="ignorelink"> {!! $row->comments !!} {!! Lang::get('core.Comments') !!}</a> </span> </li>
                    </ul>
				</div>	
				<div class="summary">
				
				@php ( $content = str_replace('<hr />',"",$row->content) )
				@php (  $content = strip_tags($content) )
				@php ( $content = substr( $content,0,350 ) )
				{!! SiteHelpers::renderHtml('<p>'.$content.'</p>') !!}
					<a href="{!! URL::to('blogs/read/'.$row->slug) !!}" class="btn btn-color blog-view"> {!! Lang::get('core.more') !!} </a>
				</div>
			</div>	
            </div>
            </div>
            </div>
		</div>	
	 @endforeach
		
	<div class="pull-right">
		{!! str_replace('/?', '?', $pagination->render()) !!}
		</div>
        </div>
	</div>
	<div class="col-sm-3">
		@include('blog.sidebar')
	</div>
	</div>
	</div>	
    </div>  
<script>
$(function(){
	  $('body').removeClass();
      $('body').addClass('blog-s');
	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("blogs/multisearch")}}');
		$('#bsetecTable').submit();
	});
	$('#front-header').addClass('front-header');
});	
</script>		