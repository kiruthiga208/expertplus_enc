@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('core/users?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('core/users?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('core/users/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.avatar') }}</td>
						<td>{!! SiteHelpers::customavatar($row->email,$row->id,'small') !!}</td>
					</tr>
					@php( $prefix = \bsetecHelpers::getdbprefix())
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.group') }}</td>
						<td>{{ \bsetecHelpers::getGroupnameByID($row->group_id) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Username') }} </td>
						<td>{{ $row->username }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.firstname') }} </td>
						<td>{{ $row->first_name }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {{ Lang::get('core.lastname') }} </td>
						<td>{{ $row->last_name }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'> {{ Lang::get('core.email') }} </td>
						<td>{{ $row->email }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Created At') }}</td>
						<td>{{ $row->created_at }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.lastlogin') }}</td>
						@if($row->last_login != '')
						<td>{{ $row->last_login }} </td>
						@else
						<td>{{ $row->updated_at }} </td>
						@endif
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ Lang::get('core.Updated At') }}</td>
						<td>{{ $row->updated_at }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>Status</td>
						@if($row->active == 1)
						<td>{!! "Active" !!} </td>
						@else
						<td>{!! "Inactive" !!}
						@endif
						
					</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>

@if(count($instructor))
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i>{{ Lang::get('core.instructor_info') }} <small>{{ Lang::get('core.instructor_desc') }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
					<tr>
						<td width='30%' class='label-view text-right'>Headline</td>
						<td>{{$instructor->headline}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Biography</td>
						<td>{{ strip_tags($instructor->biography) }}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Billing address</td>
						<td>{{ strip_tags($instructor->b_address) }}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Billing city</td>
						<td>{{$instructor->b_city}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Billing Country</td>
						<td>{{$instructor->b_country}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Billing phone</td>
						<td>{{$instructor->b_phone}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Billing zip</td>
						<td>{{$instructor->b_zip}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Paypal Email</td>
						<td>{{$instructor->p_email}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Paypal Address</td>
						<td>{{ strip_tags($instructor->p_address)}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Paypal City</td>
						<td>{{$instructor->p_city}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Paypal zip</td>
						<td>{{$instructor->p_zip}}</td>
					</tr>
					<tr>
						<td width='30%' class='label-view text-right'>Paypal Country</td>
						<td>{{$instructor->p_country}}</td>
					</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	
@endif

	</div>
</div>
	  
@stop