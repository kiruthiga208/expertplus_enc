@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('core/users?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper m-t">


<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
		<div id="image_error"></div>	

		 <?php
			if($row['id']!=''){
				$pgurl = 'core/users/save/'.$row['id'].'?return='.$return;
			}else{
				$pgurl = 'core/users/save?return='.$return;
			}

		?>

		 {!! Form::open(array('url'=>$pgurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		<div class="col-md-6">
					
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
									<div class="col-md-6">
									  {!! Form::text('id', $row['id'],array('class'=>'form-control','id'=>'id','placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Group / Level" class=" control-label col-md-4 text-left"> {{ Lang::get('core.group_level') }} <span class="asterisk "> * </span></label>
									<div class="col-md-6">

									{!! Form::select('group_id',$user_groups,$row['group_id'],array('class'=>'select2','id'=>'group_id','placeholder'=>Lang::get('core.please_select'))) !!}
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Username" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Username') }} <span class="asterisk "> * </span></label>
									<div class="col-md-6">
									  {!! Form::text('username', $row['username'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="First Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.firstname') }}  <span class="asterisk "> * </span></label>
									<div class="col-md-6">
									  {!! Form::text('first_name', $row['first_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Last Name" class=" control-label col-md-4 text-left"> {{ Lang::get('core.lastname') }} <span class="asterisk "> * </span> </label>
									<div class="col-md-6">
									  {!! Form::text('last_name', $row['last_name'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Email" class=" control-label col-md-4 text-left"> {{ Lang::get('core.email') }} <span class="asterisk "> * </span></label>
									<div class="col-md-6">
									  {!! Form::text('email', $row['email'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email'   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
					
								  <div class="form-group  " >
									<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="asterisk "> * </span></label>
									<div class="col-md-6">
									   	<label class='radio radio-inline'>
							    		{!! Form::radio('active',1,($row['active']==1) ? true:false,array()) !!} Active
							    		</label>
							    		<label class='radio radio-inline'>
										{!! Form::radio('active',0,($row['active']==0) ? true:false,array()) !!} Inactive
							 			</label>
									 </div> 
									 <div class="col-md-2">
									 </div>
								  </div> 


								  <div class="form-group  " >
									<label for="Avatar" class=" control-label col-md-4 text-left"> {{ Lang::get('core.avatar') }} </label>
									<div class="col-md-6">
									  <input  type='file' name='avatar' id='avatar' @if($row['avatar'] =='') class='required' @endif   />
										<div>
										@if($row['id'] !="" && $row['avatar'] != 0)
											{!! SiteHelpers::customavatar($row['email'],$row['id'],'small','img-circle user_image_selector') !!}
											<span class="pull-right" id="removeImage" style="margin-top:35px;color:red;cursor:pointer;">{{ Lang::get('core.x_remove') }}</span>
										@else
											<img class="img-circle user_image_selector" id="avatar-circle" border="2" width="100" />
											<span class="pull-right" id="removeImage" style="margin-top:35px;color:red;cursor:pointer;display:none;">{{ Lang::get('core.x_remove') }}</span>
										@endif
										</div>					
				 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 
			</div>
			


			<div class="col-md-6">	  
		
		<div class="form-group">
			
			<label for="ipt" class=" control-label col-md-4 text-left" > </label>
			<div class="col-md-8">
				@if($row['id'] !='')
					{{ Lang::get('core.notepassword') }}
				@else
					Create Password
				@endif	 
			</div>
		</div>	


		<div class="form-group">
			<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.newpassword') }}
			@if($row['id'] =='')
			<span class="asterisk ">*</span>
			@endif
			 </label>
			<div class="col-md-8">
			<input name="password" type="password" id="password" autocomplete="off" class="form-control input-sm" value=""
			@if($row['id'] =='')
				required
			@endif
			 /> 
			 </div> 
		</div>  
		  
		  <div class="form-group">
			<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.conewpassword') }} 
			@if($row['id'] =='')
			<span class="asterisk ">*</span>
			@endif
			</label>
			<div class="col-md-8">
			<input name="password_confirmation" type="password" autocomplete="off" id="password_confirmation" class="form-control input-sm" value=""
			@if($row['id'] =='')
				required
			@endif		
			 />  
			 </div> 
		  </div>  				  
				  

		 
		 </div>			
			
			
			<div style="clear:both"></div>	
			<div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8 btn-section">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('core/users?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;
var avatar_image = $('.user_image_selector');
var image_error = $('#image_error');
var remove_btn = $('#removeImage');
$("#avatar").change(function(e) {
	var file, img,height;
	image_error.html("");
	avatar_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger">invalid file type!</div>';
		   image_error.html(html_error);
		   avatar_image.removeAttr('src');
		   remove_btn.hide();
		   $(this).val("");
		   return false;
		}else{
			avatar_image.attr('src',img.src);
			remove_btn.show();
			img.onload = function(){
				if(this.width < 500 || this.height < 500){
					html_error ='<div class="alert alert-danger">The selected image size is '+this.width+'px * '+this.height+'px. The Minimum allowed image size is 500px * 500px.</div>';
				   	image_error.html(html_error);
				   	$("#avatar").val("");
				   	avatar_image.removeAttr('src');
				   	remove_btn.hide();
				   	return false;
				}
			}
			
		}
    }
});
remove_btn.on('click',function(){
	var id = $('#id').val();
	if(id != ""){
		if(confirm('are u sure delete that image ?'))
		{
			$.ajax({
				url:'{!! url('').'/core/users/usersimage'!!}',
		        data:{id:id},
		        type:"DELETE",
		        success:function(res){
		        	$("#avatar_image").val("");
		        	avatar_image.removeAttr('src');
		        	remove_btn.hide();
		        },
		        fail:function(){
		            alert("error");
		        }
			});
		}
	}else{
		$("#avatar_image").val("");
		avatar_image.removeAttr('src');
		remove_btn.hide();	
	}
});
</script> 
@stop