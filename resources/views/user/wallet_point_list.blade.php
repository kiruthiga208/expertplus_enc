@extends('layouts.app')

@section('content')
<div class="page-content row">
  <!-- Page header -->
  <div class="page-header">
    <div class="page-title">
      <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
    </div>
    <ul class="breadcrumb">
      <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
	    <li><a href="">{{ $pageTitle }}</a></li>
    </ul>
  </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>

    <div class="sbox animated fadeInRight">
      <div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5></div>
      <div class="sbox-content"> 
       @if(count($walletusers)>0)
      {!! Form::open(array('url'=>'user/approval/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
      {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
      <div class="table-responsive course-listing-admin" style="min-height:300px;">
      <table class="table table-striped ">
          <thead>
          <tr>
            <th class="number"> {{ Lang::get('core.no') }} </th>
            <th> Avatar </th>
            <th> Username </th>
            <th> Email </th>
            <th> Wallet Point </th>
            <th> Status </th>
            <th> Action </th>
          </tr>
          </thead>
            @if(count($walletusers)>0)
            @php ($i = ($walletusers->currentPage() - 1) * $walletusers->perPage() + 1)
            @foreach($walletusers as $walletuser)
          <tbody>
            <tr>
            <td width="30">{{ $i++ }}  </td>  
             <td>{!! SiteHelpers::customavatar($walletuser->email, $walletuser->id, 'small') !!}</td>          
            <td>{{ $walletuser->username }}</td> 
            <td>{{ $walletuser->email }}</td> 
            <td>{{ $walletuser->walletpoint }}</td>
            @if($walletuser->active==1)
              <td>Active</td>
            @else
              <td>Inactive</td>
            @endif
            <td>
              <a  href="{{ URL::to('user/walletpointupdate/'.$walletuser->id) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>                        
           </td> 
            </tr>
          </tbody>
            @endforeach
          @endif
      </table>
      </div>
      {!! Form::close() !!}
      <div class="col-sm-4"> {!! str_replace('/?', '?', $walletusers->render()) !!} </div>
      @else
        <div>Your Wallet Point Setting is Empty</div>
      @endif
  </div>
</div>  
</div>

<script>

</script>

@stop