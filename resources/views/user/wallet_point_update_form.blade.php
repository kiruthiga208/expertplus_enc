@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('user/walletpoint') }}">{{ $pageTitle }}</a></li>
        <li class="active">Edit </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper m-t">


<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>

		{!! Form::open(array('url'=>'user/walletpointupdate/'.$userdata['id'], 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		{!! Form::hidden('amount', $userdata['wallet'] ) !!}
		<div class="col-md-6">		
		  <div class="form-group hidethis " style="display:none;">
			<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
			<div class="col-md-6">
			  {!! Form::text('id', $userdata['id'], array('class'=>'form-control','id'=>'id','placeholder'=>'',   )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 									
		  <div class="form-group  " >
			<label for="Username" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Username') }} <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('username', $userdata['username'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'disabled'  )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 												
		  <div class="form-group  " >
			<label for="Email" class=" control-label col-md-4 text-left"> {{ Lang::get('core.email') }} <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('email', $userdata['email'], array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'email', 'disabled'   )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 					 

		  <div class="form-group  " >
			<label for="walletpoint" class=" control-label col-md-4 text-left"> Wallet Point <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::text('walletpoint', $userdata['walletpoint'], array('class'=>'form-control walletpoint', 'placeholder'=>'', 'required'=>'true', 'readonly')) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 

		  <div class="form-group  " >
			<label for="walletpoint" class=" control-label col-md-4 text-left"> Type <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::select('type', array(''=>'Select', '1'=>'Add', '0'=>'Reduce'), null, array('class'=>'form-control type', 'placeholder'=>'', 'required'=>'true')) !!} 
			</div> 
			<div class="col-md-2"></div>
		  </div> 

		  <div class="form-group  " >
			<label for="walletpoint" class=" control-label col-md-4 text-left"> Point <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::number('point', null, array('class'=>'form-control point', 'min'=>'0', 'step'=>'0.01', 'placeholder'=>'', 'required'=>'true', 'oninput'=>'validity.valid||(value="");')) !!} 
			</div> 
			<div class="col-md-2"></div>
		  </div> 

		   <div class="form-group  " >
			<label for="Reason" class=" control-label col-md-4 text-left"> Reason <span class="asterisk "> * </span></label>
			<div class="col-md-6">
			  {!! Form::textarea('reason', $userdata['reason'], array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true' )) !!} 
			 </div> 
			 <div class="col-md-2">
			 	
			 </div>
		  </div> 

		</div>
		  	
			
		<div style="clear:both"></div>	
		<div class="form-group">
			<label class="col-sm-4 text-right">&nbsp;</label>
			<div class="col-sm-8 btn-section">	
			<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
			<button type="button" onclick="location.href='{{ URL::to('user/walletpoint') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
			</div>
		</div>  
		{!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script>
$(document).ready(function() {
	$('.type').on('change', function(){
		if(this.value=='0'){
			max = $('.walletpoint').val();
			value = $('.point').val();
			if(max<value){
				$('.point').val('');
			}
			$('.point').prop('max', max);
		}else{
			$('.point').prop('max', '');
		}
	});
});
</script> 
@stop