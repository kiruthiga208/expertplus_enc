@extends('layouts.frontend')
@section('content')
<style type="text/css">
  .sbox-title
  {
    background-color: #158d8e;
    color: #fff;
    }
    .sbox-title h5
    {
    font-size: 18px;
  }
  .course-listing-admin thead th,.course-listing-admin thead 
  {
    background-color: #ededed;
  }
  .course-listing-admin td,.course-listing-admin thead th
  {
    padding: 7px;
  }
</style>
<p></p>
<div class="container confirm-section">
<div class="row">
  <div class="col-sm-12">
    <div class="col-sm-10" style="border-bottom-color: #dedede;"><h2>Wallet</h2></div>
    <div class="col-sm-2"><div style="float: right;" class="">
  <button class="btn btn-color"><a class="btn add_wallet" id="addwalletamount" href="javascript:void(0);" style="color:#fff;">Add Wallet</a></button>
</div></div>
  </div>
  <div class="col-sm-12">
  <p style="font-size:20px;padding-left:15px;">Your Balance point : {{ $wallet }}</p>
</div>
</div>

<div class="modal fade" id="addWalletModal" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="myModalLabel">~ </h4>
      </div>
      <div class="modal-body">
      {!! Form::open(array('url' => 'payment/courseform', 'method' => 'post','id'=>'', 'parsley-validate', 'novalidate')) !!}
       <div class="form-group">
          <label for="walletamount">Add Your Wallet Amount</label>
          <input type="number" min="1" step="1" name="walletamount" class="form-control" id="walletamount" oninput="validity.valid||(value='');" placeholder="Wallet Amount" required>
      </div>
      <button type="submit" id="addamount" class="btn btn-color">Pay Now</button>
      {!! Form::close() !!}
      <br>
    </div>
  </div>
  </div>
</div>
<div class="page-content-wrapper">

    <div class="sbox animated fadeInRight">
      <div class="sbox-title"> <h5>Wallet Transactions List </h5></div>
      <div class="sbox-content">    
 
      <div class="table-responsive course-listing-admin" style="min-height:300px;">
      <table class="table table-striped ">
          <thead>
          <tr>
            <th class="number" style="background-color: #ededed;padding:10px;font-size:16px;"> {{ Lang::get('core.no') }} </th>
            <th style="background-color: #ededed;padding:10px;font-size:16px;"> Amount </th>
            <th style="background-color: #ededed;padding:10px;font-size:16px;"> Payment Method </th>
            <th style="background-color: #ededed;padding:10px;font-size:16px;"> Date </th>
            <th style="background-color: #ededed;padding:10px;font-size:16px;"> Status </th>
          </tr>
          </thead>
            @if(count($transactions)>0)
      @php ($i = ($transactions->currentPage() - 1) * $transactions->perPage() + 1)
      @foreach($transactions as $transaction)
          <tbody>
            <tr>
            <td width="30" style="background-color: #ededed;padding:7px;font-size:15px;">{{ $i++ }}  </td>             
            <td style="background-color: #fff;padding:7px;font-size:15px;">{{ $transaction->amount }}</td> 
            <td style="background-color: #fff;padding:7px;font-size:15px;">{{ $transaction->payment_method }}</td> 
            <td style="background-color: #fff;padding:7px;font-size:15px;">{{ $transaction->created_at }}</td>
            <td style="background-color: #fff;padding:7px;font-size:15px;">{{ $transaction->status }}</td>
            </tr>
          </tbody>
            @endforeach
            @endif
      </table>
      </div>
      <div class=""> {!! str_replace('/?', '?', $transactions->render()) !!} </div>
  </div>
</div>  
</div>
</div>
<script>
$(document).ready(function(){
  $(document).on('click','#addwalletamount',function(){
    $("#addWalletModal").addClass('in');
    $("#addWalletModal").css("display", "block");
      $('#addWalletModal').modal({
        show : true,
          backdrop: 'static',
          keyboard: false
      }); 
  });
  $(".close").click(function () {
    $("#addWalletModal").removeClass('in');
    $(".modal-backdrop").hide();
    $("#addWalletModal").css("display", "none");
    return false;
  });
});
</script>
@stop