<div class="course_info danger_zone">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-3">
@include('user.user_menu')
</div>
<div class="col-xs-12 col-sm-9">
<div class="block2 clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.danger_zone')!!}</h2>
<p>{!! Lang::get('core.danger_notes')!!}</p>
</div>
<div class="image_sec subblock">
{!! Form::open(array('url'=>'user/dangerzone/', 'class'=>'form-horizontal','id'=>'danger_form')) !!} 
<div class="remove_account">
<h4>{!! Lang::get('core.remove_account') !!}</h4>
<p>{!! Lang::get('core.remove_account_msg') !!} </p>
<input type="hidden" name="danger" value="1" />


<button class="btn alert-danger save_btn" type="button" data-toggle="modal" data-target="#myModal"> {!! Lang::get('core.btn_remove') !!}</button>
</div>
</div>
</div>
</div>
</div>
</div>
{!! Form::close() !!}

</div>
<script>
var main = function() {
 
 /* Push the body and the nav over by 285px over */
  
$('.mobile_menu .icon-menu').click(function() {
  
  $('.mobile_menu .menu').animate({left: "0px"
   
 }, 200);

    
$('body').animate({left: "285px"}, 200);
 
 });

  
/* Then push them back */
  
$('.mobile_menu .icon-close').click(function() {
   
 $('.mobile_menu .menu').animate({
    
  left: "-285px"
   
 }, 200);

    
$('body').animate({
    
  left: "0px"
  
  }, 200);
  
});

};



$(document).ready(main);
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	$('.mobile_menu .icon-menu').click(function(){
		$('body').addClass('open_sidebar');
		});
			$('.mobile_menu .icon-close').click(function(){
		$('body').removeClass('open_sidebar');
		});
		
		$(function(){
$('ul li a').on('click', function(){
    $(this).parent().addClass('active').siblings().removeClass('active');
  });
});
});

function submitForm(){
  $('#danger_form').submit();
}
</script>




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog confirm_account" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.delete_account')!!}</h4>
      </div>
      <div class="modal-body">
      <p>{!! Lang::get('core.delete_confirm')!!}</p>
      </div>
      <div class="modal-footer">
      <div class="pull-left">
        <button type="button" class="btn btn-danger" onclick="submitForm();" data-dismiss="modal">{!! Lang::get('core.Yes')!!}</button> <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close" aria-hidden="true">{!! Lang::get('core.no')!!}</button></div>
      </div>
    </div>
  </div>
</div>