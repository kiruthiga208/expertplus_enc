@extends('layouts.app')

@section('content')
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('transaction?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
		 {!! Form::open(array('url'=>'user/certificate', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		<div class="col-md-12">
			<fieldset>
			<div class="form-group">
				<span><b>Note:</b> Don't remove that. For Example: {{ "{{" }} ....... }} values.</span>
			</div>
				<div class="form-group">
					<textarea name='certificate' class="form-control input-sm  markItUp" name="regEmail" rows="20">{{ $content }}</textarea> 
				</div>
			</fieldset>	
		</div> 
			<div style="clear:both"></div>	
				<div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
						<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#previewModal"><i class="fa  fa-check-circle"></i> Preview </button>
						<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					</div>	  
				</div> 
		{!! Form::close() !!}
	</div>
</div>
</div>


<div class="modal fade model_width" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog paypal_popup">
        <div class="modal-content">
            <div class="modal-header header_bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Certificate view</h2>
            </div>
            <div class="modal-body cont">
            <iframe src="{!! URL::to('user/certificateview') !!}" style="zoom:0.60" width="100%" height="550" frameborder="0"></iframe>
            </div>
        </div>
    </div>
</div>



@stop