@extends('layouts.frontend')

@section('content')

<div class="register-b">

<div class="login-s animated fadeInDown delayp1">
<h3>{{ Lang::get('core.Register') }}</h3>
{!! Form::open(array('url'=>'user/create', 'class'=>'form-signup')) !!}
	@if(Session::has('message'))
				{!! Session::get('message') !!}
			@endif
            <ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
        <div class="clearfix">
<ul class="signup-b">
<li class="">
<label>{{ Lang::get('core.firstname') }}<span class="req">*</span></label>
<div class="form-group animated fadeInLeft">
<i class="fa fa-user"></i>		
	  {!! Form::text('firstname', $first_name, array('placeholder'=>Lang::get('core.firstname') ,'required'=>'' )) !!}
	</div>
  </li>
  <li>
  <label>{{ Lang::get('core.lastname') }}<span class="req">*</span></label>  
<div class="form-group animated fadeInRight">
<i class="fa fa-user"></i>		
	 {!! Form::text('lastname', $last_name, array('placeholder'=>Lang::get('core.lastname'),'required'=>'')) !!}
	</div>
</li><li>
 <label>{{ Lang::get('core.username') }}<span class="req">*</span></label>    
<div class="form-group animated fadeInLeft">
<i class="fa fa-user"></i>		
	  {!! Form::text('username', $username, array('placeholder'=>Lang::get('core.username') ,'required'=>'' )) !!}
	</div>
  </li>
  <li>
  <label>{{ Lang::get('core.email') }}<span class="req">*</span></label>
 <div class="form-group animated fadeInRight">
		<i class="fa fa-envelope"></i>	
	 {!! Form::text('email', $email, array('placeholder'=>Lang::get('core.email'),'required'=>'email')) !!}
	</div> 
  
  </li>
  <li>
  <label>{{ Lang::get('core.password') }}<span class="req">*</span></label>
  <div class="form-group animated fadeInLeft">
<i class="fa fa-lock"></i>		
	 {!! Form::password('password', array('placeholder'=>Lang::get('core.password'),'required'=>'')) !!}
	</div>
  </li>
 <li>
 <label>{{ Lang::get('core.repassword') }}<span class="req">*</span></label>
 <div class="form-group animated fadeInRight">
<i class="fa fa-lock"></i>			
	 {!! Form::password('password_confirmation', array('placeholder'=>Lang::get('core.conewpassword'),'required'=>'')) !!}
	</div>
 </li> 
    <li>
   @if(CNF_RECAPTCHA =='true') 
        <label class="text-left"> {!! Lang::get('core.captcha')!!} <span class="req">*</span></label>    
         {{ Lang::get('core.case_sensitive') }}
        <div class="captcha-block">
        {!! captcha_img() !!} 
        <div class="form-group animated fadeInRight">
        <i class="fa fa-lock"></i>
        <input type="text" name="recaptcha_response" placeholder="{!! Lang::get('core.security_code')!!}" class="form-control" required/>
        </div>
      </div>
    @endif  
    </li>
  </ul>  
  </div>

    <div class="clearfix">
          <div class="row form-actions">
        <div class="col-sm-12">
{!! Form::hidden('social_id', $social_id); !!}
        {!! Form::hidden('social_type', $social_type); !!}
        {!! Form::hidden('social_avatar' , $socialavatar); !!}
          <button type="submit" style="" class="btn btn-color animated fadeInUp">{{ Lang::get('core.signup') }}	</button>
          </div>
          </div>
    </div>      
 {!! Form::close() !!}
</div>



</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('body').removeClass();
		$('body').addClass('sxim-init');
		$('body').addClass('login-b');
		$('#front-header').addClass('front-header');	
	});
</script> 


@stop

