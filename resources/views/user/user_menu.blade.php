<div class="sidebar-user menu"> 
<div class="sidebar_block clearfix">
 <div class="profile_image">{!! SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'), 'medium') !!}
 <h3>{!! \Session::get('fid') !!}</h3>
</div>
@if(\Request::segment(2)=='profile')
@php ($cls1 = 'active')
@endif
@if(\Request::segment(2)=='photo')
@php (  $cls2 = 'active')
@endif
@if(\Request::segment(2)=='notification')
@php (  $cls3 = 'active')
@endif
@if(\Request::segment(2)=='dangerzone')
@php (  $cls4 = 'active')
@endif
@if(\Request::segment(2)=='account')
@php (  $cls5 = 'active')
@endif
@if(\Request::segment(2)=='usermembership')
@php (  $cls7 = 'active'  )
@endif
@if(\Request::segment(2)=='instructorinfo')
@php (  $cls6 = 'active')
@endif
<ul>
<li class="{!! $cls1 or '' !!}"><a href="{!! url('').'/user/profile'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.profile') !!} <span class="r-arrow"></span></a></li>
<li class="{!! $cls2 or '' !!}"><a href="{!! url('').'/user/photo'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.profile_image') !!}<span class="r-arrow"></span></a></li>
<li class="{!! $cls5 or '' !!}"><a href="{!! url('').'/user/account'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.account') !!} <span class="r-arrow"></span></a></li>
@if(bsetecHelpers::getmembershipstatus())
<li class="{!! $cls7 or '' !!}"><a href="{!! url('').'/user/usermembership'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.membership') !!} <span class="r-arrow"></span></a></li>
@endif
<li class="{!! $cls3 or '' !!}"><a href="{!! url('').'/user/notification'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.notifications') !!} <span class="r-arrow"></span></a></li>
<li class="{!! $cls4 or '' !!}"><a href="{!! url('').'/user/dangerzone'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.danger_zone') !!} <span class="r-arrow"></span></a></li>
@if(count($instructor))
@if($instructor->p_email != NULL)
<li class="{!! $cls6 or '' !!}"><a href="{!! url('').'/user/instructorinfo/personal_info'!!}"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.instructor_info') !!} <span class="r-arrow"></span></a></li>
@endif
@endif
</ul>
</div>
</div>
