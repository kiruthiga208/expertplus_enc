
<div class="course_info profile_block notification_block">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-3">
@include('user.user_menu')
</div>
<div class="col-xs-12 col-sm-9">
<div class="block2 clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.notification')!!}</h2>
<p> {!! Lang::get('core.n_msg')!!} </p>
</div>
@if(Session::has('message'))	  
		   {!! Session::get('message') !!}
	@endif	
	<!-- </div> -->
{!! Form::open(array('url'=>'user/notification/', 'class'=>'form-horizontal')) !!} 
<div class="section_notification">
<h4>{!! Lang::get('core.n_list')!!}:</h4>
<div class="image_sec subblock">
<ul>
	<li><label for="notif_announcement"> {!! Form::checkbox('notif_announcement', 1, $notif_announcement, ['id'=>'notif_announcement']) !!} {!! Lang::get('core.n_instructor')!!} </label></li>
	<li><label for="notif_special_promotion">{!! Form::checkbox('notif_special_promotion', 1, $notif_special_promotion, ['id'=>'notif_special_promotion']) !!}  {!! Lang::get('core.n_promotion')!!}  </label></li>
	<li><label for="notif_course_subscribed">{!! Form::checkbox('notif_course_subscribed', 1, $notif_course_subscribed, ['id'=>'notif_course_subscribed']) !!}  {!! Lang::get('core.n_subscribe')!!}  </label></li>
	<li><label for="notif_reviews">{!! Form::checkbox('notif_reviews', 1, $notif_reviews, ['id'=>'notif_reviews']) !!}  {!! Lang::get('core.n_review')!!} </label></li>
	<li><label for="notif_course_approve">{!! Form::checkbox('notif_course_approve', 1, $notif_course_approve, ['id'=>'notif_course_approve']) !!}  {!! Lang::get('core.n_course_approved')!!}   </label></li>
	<li><label for="notif_course_delete">{!! Form::checkbox('notif_course_delete', 1, $notif_course_delete, ['id'=>'notif_course_delete']) !!} {!! Lang::get('core.n_delete')!!}   </label></li>
	
</ul>
</div>
<!-- <div class="warning_note"><label for="notif_for_all">{!! Form::checkbox('notif_for_all', 1, $notif_for_all, ['id'=>'notif_for_all']) !!}  {!! Lang::get('core.no_notification')!!} </label></div>
 -->
</div>

<div class="button_footer">
<button class="btn btn-color save_btn" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
</div>
</div>

</div>
</div>
</div>
</div>
{!! Form::close() !!}


<script>
var main = function() {
 
 /* Push the body and the nav over by 285px over */
  
$('.mobile_menu .icon-menu').click(function() {
  
  $('.mobile_menu .menu').animate({left: "0px"
   
 }, 200);

    
$('body').animate({left: "285px"}, 200);
 
 });

  
/* Then push them back */
  
$('.mobile_menu .icon-close').click(function() {
   
 $('.mobile_menu .menu').animate({
    
  left: "-285px"
   
 }, 200);

    
$('body').animate({
    
  left: "0px"
  
  }, 200);
  
});

};



$(document).ready(main);
</script>

<script type="text/javascript">
$(document).ready(function(){
	
	$('.mobile_menu .icon-menu').click(function(){
		$('body').addClass('open_sidebar');
		});
			$('.mobile_menu .icon-close').click(function(){
		$('body').removeClass('open_sidebar');
		});
		
		$(function(){
$('ul li a').on('click', function(){
    $(this).parent().addClass('active').siblings().removeClass('active');
  });
});
});
</script>

<script>
$('input[type="checkbox"],input[type="radio"]').iCheck({
   checkboxClass: 'icheckbox_square-green',
   radioClass: 'iradio_square-green',
  });
</script>