 <div class="course_info account-b">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-3">

@include('user.user_menu')

</div>
<div class="col-xs-12 col-sm-9">
<div class="block2 account_section clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.account') !!}</h2>
<p>{!! Lang::get('core.a_message')!!}</p>
</div>
<div class="image_sec subblock">

		@foreach($errors->all() as $error)
			<p class="alert alert-danger">{{ $error }}</p>
		@endforeach

   @if(Session::has('message'))   
      {!! Session::get('message') !!}
   @endif
	<div id="pass" class="account_block">
    <label>{!! Lang::get('core.Email') !!}:</label>
    <div class="form-group">
    <div class="email_edit">
  	  <p>{!! Lang::get('core.your_email') !!} <span>{!! $email !!}&nbsp;&nbsp;</span><i class="fa fa-edit" data-toggle="modal" data-target="#myModal" style="cursor:pointer;"></i></p>
      </div>
    </div>
    <label>{!! Lang::get('core.Password') !!} :</label>
    {!! Form::open(array('url'=>'user/account/', 'class'=>'form-horizontal','method'=>'POST')) !!}
    <div class="form-group">
	{!! Form::password('current_password',array('class'=>'form-control','placeholder'=>Lang::get('core.current_pwd'),'autocomplete'=>"off")) !!}	
   </div> 
   {!! Form::hidden('request_page','pass' ,array('class'=>'form-control')) !!}
  <div class="form-group">
	{!! Form::password('password',array('class'=>'form-control','placeholder'=>Lang::get('core.new_pwd'),'autocomplete'=>"off")) !!}
  </div> 
  <div class="form-group">
	{!! Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>Lang::get('core.retype_pwd'),'autocomplete'=>"off")) !!}
  </div> 
   	
    </div>
</div>
<div class="button_footer">
		<button class="btn btn-color" type="submit">{!! Lang::get('core.Change_Password') !!} </button>
	</div>
</div>


</div>
</div>
</div>
	{!! Form::close() !!}

</div>
  <script type="text/javascript">
  var saveEmail = function(){
  	$('#email_error,#email_success').text("");
	  email_error = $('#email_error');
  	email = $("#email").val();
  	password = $("#email_password").val();
    if(email == ""){
      email_error.show().text("{{ Lang::get('core.enter_email') }}");
    }else if(password == ""){
      email_error.show().text("{{ Lang::get('core.enter_pwd') }}");
    }else{
  	$.ajax({
  		url:'{!! url('').'/user/account'!!}',
        data:{email:email,email_password:password,request_page:"email"},
        dataType:'json',
        type:"POST",
        success:function(res){
        	if(res.error){
        		email_error.show().text(res.error);
        	}else{
				location.reload();
        	}
        },
        fail:function(){
        	alert("{{ Lang::get('core.error') }}");
        }
  	});
  }
};
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog edit_popup" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.change_email') !!}</h4>
      </div>
      <div class="modal-body">
      <p id="email_error" class="alert alert-danger" style="display:none;"></p>
      <div class="form-group">
     {!! Form::email('email',$email ,array('id'=>'email','autocomplete'=>'off','required'=>true)) !!}
      </div>
      <div class="form-group">
      <input type="password" id="email_password" placeholder="{!! Lang::get('core.enter_pwd')!!}" autocomplete="off" required />
      </div>
      </div>
      <div class="modal-footer">
<input type="button" value="save"  onclick="saveEmail();" class="btn btn-color">
      </div>
    </div>
  </div>
</div>














