<style>
input[type=number] {
    -moz-appearance:textfield;
}
.payment-block .form-group label.error{
    color:red;
}
</style>
<div class="confirm-section">
    <div class="container">                
    <h2 class="confirm-title">Confirm Transaction</h2>    
    <div class="table-payment">  
    <div id="couponError"></div>       
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
            <tr>
            <th width="90%">{!! Lang::get('core.name_content') !!}</th>
            <th width="10%">{!! Lang::get('core.price')!!}</th>
            </tr>
            <tr>
            @if (defined('CNF_CURRENCY'))
                @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
            @endif
            <td>{{ $content }}</td>
            <td> {{$currency .' '. $you_pay}}</td>
            </tr>

            <tr>
            <td style="text-align:left;">{!! Lang::get('core.you_pay') !!}</td>
            <td>{!! $currency !!} <span id="you_pay">{{$you_pay}}</span><span id="coupon_price"></span></td>
            </tr>
        </table>     
    </div>     

    <div class="payment-block clearfix">
    @if($errors->any())
        <div class="alert alert-danger">
           <a class="close" data-dismiss="alert">×</a>
           {!! Lang::get('core.payment_error') !!}
        </div>
    @endif

    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
    @php( $i=1 ) 
    @if(count($payment)>0)  
        @php(  $me = array('ccavenue','bank','cod') )
        @foreach ($payment as $key => $method)
        @php( $kk = in_array($key, $me) )
            @if($kk==1)
                @php( $cc = \bsetecHelpers::ifInstalled($key) )
            @endif
            @if($i==1)
                @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                    @if($cc>0)
                    <li class="active"><a href="{!! '#'.$key !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                    @endif
                @else
                     <li class="active"><a href="{!! '#'.$key !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                @endif
                <script>
                    $(document).ready(function(){
                        var active = $('#tabs').find('.active').children().attr('href');
                        $(active).addClass('active');
                    });
                </script>
            @else
                @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                    @if($cc>0)
                    <li class=""><a href="{!! '#'.$key !!}" class="{!! $key.'-tab' !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                    @endif
                @else
                    <li class=""><a href="{!! '#'.$key !!}" class="{!! $key.'-tab' !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                @endif
            @endif
            @php( $i++ )
        @endforeach
    @else
        <h2>{!! Lang::get('core.no_payment')!!}</h2>
    @endif
    </ul>
    
    <div id="my-tab-content" class="tab-content">
    @foreach ($payment as $key => $method)
        @if($key == 'paypal_express_checkout' && $method['payment']=='1')
            <div id="paypal_express_checkout" class="tab-pane ">
            {!! Form::open(array('url' => url('payment/walletform'))) !!}
            <input type="hidden" name="payment_method" value="paypal_express_checkout" /> 
            
            <div class="form-group clearfix">
            <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
            <div class="col-sm-8">
            <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
            </div>
            </div> 
            {!! Form::close() !!}
            </div>
            @endif

            @if($key == 'paypal_standard' && $method['payment']=='1' )
            <div id="paypal_standard" class="tab-pane">
            {!! Form::open(array('url' => url('payment/walletform'),'parsley-validate'=>'','novalidate'=>' ' )) !!}
            <input type="hidden" name="payment_method" value="paypal_standard" /> 
            
            <div class="form-group clearfix">
            <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
            <div class="col-sm-8">
            <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
            </div>
            </div> 
            {!! Form::close() !!}
            </div>
        @endif

        @if($key == 'stripe' && $method['payment']=='1')
            <div id="stripe" class="tab-pane col-md-6 ">
            {!! Form::open(array('url' => url('payment/stripe'),'id'=>'payment-stripe' )) !!}
            <span class="payment-errors"></span>
            <input name='stripeToken' id="stripeToken" type="hidden" />
            <input type="hidden" name="payment_method" value="stripe" /> 
            <input type="hidden" name="type" value="3">
            <input type="hidden" name="amount" class="pay-amount" value="{!! $you_pay !!}"/>
            <div class="form-group clearfix">
            <label for="Card Number" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_number')!!} <span class="asterix"> * </span></label>
            <div class="col-sm-8">
            {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>Lang::get('core.enter_card'),'autocomplete'=>'off')) !!} 
            </div>
            </div>
            
            <div class="form-group clearfix">
            <label for="card-cvc" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_cvc') !!} <span class="asterix"> * </span></label>
            <div class="col-sm-3">
            {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'3','id'=>'card-cvc','required'=>true,'placeholder'=>Lang::get('core.enter_cvc'),'autocomplete'=>'off')) !!} 
            </div>
            </div>
            <div class="form-group clearfix">
            <label for="expire" class="col-sm-4">{!! Lang::get('core.Expiration') !!} <span class="asterisk"> * </span></label>
            <div class="expiry-wrapper clearfix">
            <div class="col-xs-6 col-sm-4">
            <div class="select-style_block">
            <select class="stripe-sensitive required" id="card-expiry-month">
            <option value="" selected="selected">{!! Lang::get('core.mm') !!}</option>
            </select>
            </div>
            <script type="text/javascript">
            var select = $("#card-expiry-month"),
            month = new Date().getMonth() + 1;
            for (var i = 1; i <= 12; i++) {
            select.append($("<option value='"+i+"'>"+i+"</option>"))
            }
            </script>
            </div>
            <div class="col-xs-6 col-sm-4">
            <div class="select-style_block">
            <select class="stripe-sensitive required" id="card-expiry-year">
            <option value="" selected="selected">{!! Lang::get('core.year') !!}</option>
            </select>
            </div>
            <script type="text/javascript">
            var select = $("#card-expiry-year"),
            year = new Date().getFullYear();
            for (var i = 0; i < 12; i++) {
            select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
            }
            </script>
            </div>
            </div>
            </div>
            <div class="form-group clearfix">
            <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
            <div class="col-sm-8">
            <input type="hidden" value="{!! $method['publishable_key'] !!}" class="stripe_key" />
            <input type="submit" id="submit_button" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
            </div>
            </div> 
            {!! Form::close() !!}
            </div>
        @endif
        @if($key == 'ccavenue' && $method['Status']=='true')
            <div id="ccavenue" class="tab-pane">
            <h1>{!! Lang::get('core.ccavenue-payment') !!}</h1>
                <form method="post" id="checkout" name="customerData" action="{!! url('ccavenue/ccavenuerequest') !!}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="tid" id="tid" readonly />
                   <?php 
                   if(!empty($method['merchant_id']))
                        $mech=$method['merchant_id'];
                    else
                        $mech='';
                    ?>
                    <input type="hidden" name="merchant_id" value="{!! $mech !!}"/>
                    <input type="hidden" id="order_id" name="order_id" value=""/>
                    <input type="hidden" name="currency" value="INR"/>
                    <input type="hidden" name="amount" class="pay-amount" value="{!! $you_pay !!}"/>
                    <input type="hidden" name="redirect_url" value="{!! \URL::to($method['redirect_url']) !!}"/>
                    <input type="hidden" name="cancel_url" value="{!! \URL::to($method['cancel_url']) !!}"/>
                    <input type="hidden" name="language" value="EN"/>
                    <input type="hidden" name="billing_name" value="{!! $user_info->username !!}"/>
                    <input type="hidden" name="billing_address" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="billing_city" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="billing_state" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="billing_zip" value="425001"/>
                    <input type="hidden" name="billing_country" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="billing_tel" value="7896541236"/>
                    <input type="hidden" name="billing_email" value="{!! $user_info->email !!}"/>
                    <input type="hidden" name="delivery_name" value="{!! $user_info->username !!}"/>
                    <input type="hidden" name="delivery_address" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="delivery_city" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="delivery_state" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="delivery_zip" value="425001"/>
                    <input type="hidden" name="delivery_country" value="{!! CNF_APPNAME !!}"/>
                    <input type="hidden" name="delivery_tel" value="7896541236"/>
                    <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color events-submit">
                </form>
                <script type="text/javascript">
                window.onload = function() {
                    var d = new Date().getTime();
                    document.getElementById("tid").value = d;
                    document.getElementById("order_id").value = d+"{{$content}}";
                };
                </script>
             </div>
        @endif
        @if($key == 'cod' && $method['Status']=='true')
            <div id="cod" class="tab-pane cashpi">
            <div class="cash">{!! Lang::get('core.cod_msg') !!}</div>
            {!! Form::open(array('url' => url('bank/cashondelivery'),'id' => 'cod_form')) !!}
                <input type="hidden" name="price" value="{{$you_pay}}" />
                <input type="hidden" name="devicetypes" value="web" />
                <input type="hidden" name="paymenttype" value ="cod" />
                <input type="hidden" name="purchasetype" value ="3" />
                <input type ="hidden" name="coupon_price" id="cou_price_cod"/>
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left"> {!! Lang::get('core.Name') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8">
                    <input type="text" name="name" class="form-control"  placeholder="Enter Your Name"  autocomplete="false"  />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left"> {!! Lang::get('core.Telephone') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8">
                    <input type="text"  name="phone" class="form-control"  placeholder="Enter Mobile Number" autocomplete="false" />
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left">{!! Lang::get('core.address') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8">
                   <textarea class="form-control" name="address" rows="5"  >  </textarea>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left"></label>
                    <div class="col-sm-8">
                   <input type="submit" value="submit" size="10" class="btn btn-color">
                    </div>
                </div>
            {!! Form::close() !!}         
            </div>
        @endif
        @if($key == 'bank' && $method['Status']=='true')
            <div id="bank" class="tab-pane banking">
            <div class="banks">{!! Lang::get('core.bank_msg') !!}</div>
            <div class="ban_det">
                <h3>{!! Lang::get('core.bank_details') !!}</h3>
                <ul>{!! $method['info'] !!}</ul>
            </div>
            {!! Form::open(array('url' => url('bank/cashondelivery'),'id'=>'bank_form')) !!}
                <input type="hidden" name="devicetypes" value="web" />
                <input type="hidden" name="price" value="{{$you_pay}}" />
                <input type="hidden" name="paymenttype" value ="bank" />
                <input type="hidden" name="purchasetype" value ="3" />
                <input type ="hidden" name="coupon_price" id="cou_price_bank"/>
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left">{!! Lang::get('core.Name') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8"><input type="text" name="name" class="form-control"  autocomplete="false" /></div>
                </div>

                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left">{!! Lang::get('core.Telephone') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8"><input type="text" name="phone" class="form-control"  autocomplete="false" /></div>
                </div>
            
                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left">{!! Lang::get('core.bank_details') !!} <span class="asterix"> * </span></label>
                    <div class="col-sm-8"><textarea class="form-control" name="bankdetails" rows="5"  ></textarea> </div>
                </div>

                <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-sm-4 text-left"></label>
                    <div class="col-sm-8"><input type="submit" value="submit" size="10" class="btn btn-color"></div>
                </div>
            {!! Form::close() !!}  
            </div>
        @endif
    @endforeach
    </div>
    </div>
    </div>
</div>
<!-- payment plugins -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script>

$('#cod_form').validate({
        rules:{
            name:{
                required:true,
            },
            phone:{
                required : true,
                number: true
            },
            address:{
                required : true,
            }

        },
        messages:{
            name:{
                required : "{{ Lang::get('core.name_cod_required') }}"
            },
            phone:{
                required : "{{ Lang::get('core.phone_cod_required') }}",
                number : "{{ Lang::get('core.phone_cod_number') }}"

            },
            address:{
                required : "{{ Lang::get('core.address_cod_required') }}"
            }
        },
        submitHandler:function(form){
            form.submit();
        }
});
$('#bank_form').validate({
        rules:{
            name:{
                required:true,
            },
            phone:{
                required : true,
                number: true
            },
            bankdetails:{
                required : true,
            }

        },
        messages:{
            name:{
                required : "{{ Lang::get('core.name_cod_required') }}"
            },
            phone:{
                required : "{{ Lang::get('core.phone_cod_required') }}",
                number : "{{ Lang::get('core.phone_cod_number') }}"

            },
            bankdetails:{
                required : "{{ Lang::get('core.details_bank_required') }}"
            }
        },
        submitHandler:function(form){
            form.submit();
        }
});
</script>
<script type="text/javascript">
// payment options script start
var keyy = $('.stripe_key').val();
Stripe.setPublishableKey(keyy);
var payment_error = $('.payment-errors');
$(document).ready(function() {
    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled");
        amount = $('.pay-amount').val();

        if(amount<50){
            alert('minimum amount 50 required for stripe transaction');
            return false;
        }

        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card numbers");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
                
            },
       

        },
        
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


</script>
<style>
.payment-errors{
    color:red;
}
</style>
