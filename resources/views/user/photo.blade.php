 <div class="course_info profile_block">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-3">

@include('user.user_menu')


</div>

<div class="col-xs-12 col-sm-9">
<div class="block2 clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.profile_image')!!} </h2>
</div>
<div class="image_sec subblock">
<div class="video_block">
<label>{!! Lang::get('core.cover_preview')!!}:<span class="text-danger">*</span></label>
<div class="block3">
<div id="image_up">
  <div id="image_prograss"></div>
</div>
            {!! Form::open(array('url'=>'user/photo/', 'class'=>'form-horizontal' ,'files' => true)) !!}       
                <div id="cropContainerPreload" class="image_container">

                 @if($image != "")
                    {!! SiteHelpers::customavatar(\Session::get('email'),\Session::get('uid'), 'normal', 'croppedImg') !!}
                 @endif 
                </div>
               


<div class="tip">{!! Lang::get('core.tip')!!}</div>
</div>
<div class="upload_type clearfix">
<label>{!! Lang::get('core.ad_image')!!} <em>( {!! Lang::get('core.min_size')!!} )</em> :</label>
<div class="format_type clearfix">
<div class="format">
  <span id="test_error">{!! Lang::get('core.use')!!}</span>
  <div class="progress progress-striped progress_upload" id="progress_bar" style="display:none;">
  <div class="progress-bar progress-bar-warning active" id="progress_bar_p"> </div>
  <p id="file_name"></p>
</div>
</div> 


</div>
@if($image != "")
 <input type='hidden' id='image_title' value='{!! $image_title !!}' />
 <input type='hidden' id='image_name' value='{!! $image !!}' />
   <input type='hidden' id='image_path' />
 @else
  <input type='hidden' id='image_title' />
   <input type='hidden' id='image_name' />
   <input type='hidden' id='image_path' />
 @endif


<input type="hidden" name="image_id" id="image_id" value="{!! $image_id !!}" />
<div class="button-block clearfix">
<div id="image_div">
  <div class="fileUpload btn btn-primary"> <span>{!! Lang::get('core.choose') !!}</span>    
    <input id="cImage" type="file" name="cImage" data-url="{!! url('').'/user/image' !!}" class="upload"/>
  </div>
</div>

<div class="change_btn" id="profile_delete_btn" style="display:none">
    <button id='cancel_btn' style="display:none;">{!! Lang::get('core.cancel') !!}</button>
    <input type="button" onClick='profile_picture_delete();' value="Change" /> 
</div>

</div>

</div>

</div>
</div>
<div class="button_footer">
  <button class="btn btn-color save_btn" type="submit" id="submit"> 
    {{ Lang::get('core.sb_savechanges') }}
  </button>
  </div>

</div>

</div>
</div>
</div>
</div>
 
      {!! Form::close() !!}

<!-- Cropic plugin files -->
<link href="{{ asset('assets/bsetec/js/plugins/croppic/css/croppic.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/croppic.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/croppic/jquery.mousewheel.min.js') }}"></script>
<!-- file upload plugin files -->
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript">



var error = $("#test_error");
var progress_bar = $("#progress_bar");
var cancel_btn = $("#cancel_btn");
var progress_bar_p = $('#progress_bar_p');
var delete_btn = $("#profile_delete_btn");
var image_div = $("#image_div");
var image_prograss = $('#image_prograss');
var submit_button = $('#submit');
submit_button.attr("disabled","disabled");

$(document).ready(function(){
    @if($image != "")
    delete_btn.show();
    $("#image_up,#image_div").hide();
    @else
    delete_btn.hide();
    $("#image_up,#image_div").show();
    @endif
});
$(function () {
   $('#cImage').fileupload({
        dataType:'json',
        add:function(e,data){
            var file=data.files[0];
            var uploadErrors = false;
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            if(data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
                error.text('{!! Lang::get("core.image_file_type_error") !!}').addClass('error_msg');
                return false;
            }

            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 5000000) {
                error.text('{!! Lang::get("core.image_big_size") !!}').addClass('error_msg');
                return false;
            }
            data.submit();
            error.hide();
            progress_bar.show();
            progress_bar_p.css('width','0%');
            $("#file_name").text(file.name); 
        },
        progress: function (e, data) {
            error.removeClass('error_msg').hide();
            var percentage = parseInt((data.loaded / data.total) * 100,10);
            progress_bar_p.css('width',percentage+'%');
            image_prograss.html("{!! Lang::get('core.img_uploading') !!} "+ percentage + '%');
        },
        done: function(e, data){
            error.removeClass('error_msg');
            return_data = data.result;
            v_html = "";
            if(return_data.status){
                image_div.hide();
                delete_btn.show();
                progress_bar.show();
                progress_bar_p.show();
               $("#image_name,#image_title").val(return_data.image_name); 
               $("#image_path").val(return_data.image_path);
            }else{
              image_prograss.html("");
              $("#file_name").text("");  
              delete_btn.hide();
              image_div.show();
              progress_bar.hide();
              progress_bar_p.hide();
              error.addClass('error_msg').css('font-size','10px').text(return_data.message).show();
              return false; 
            }
            error.removeClass('error_msg');
            image_prograss.html("{!! Lang::get('core.uploading_full') !!}");
            progress_bar_p.removeClass('active');
            $("#file_name").text("{{ Lang::get('core.photo_crop') }}");
            imageCrop();
        },
    });
});

function profile_picture_delete(){
    image_name = $('#image_title').val();
    $.ajax({
        url:'{!! url('').'/user/image'!!}',
        data:{image_file:image_name},
        type:"DELETE",
        success:function(res){
            if(res == "success"){
              $("#imageCrop").html("");
              $("#image_up,#image_div").toggle();
              $("#cropContainerPreload").html("");
              image_prograss.html("");
              error.show().css('font-size','').text("{{ Lang::get('core.use') }}");
              progress_bar.hide();
              delete_btn.toggle();
              submit_button.attr("disabled","disabled");
            }
        },
        fail:function(){
            alert("{{ Lang::get('core.error') }}");
        }
    });    
}

function imageCrop(){
    image_prograss.html("Please Wait..");
    image_path = $("#image_path").val();
    image_name = $("#image_name").val();
    var croppicContainerPreloadOptions = {
        cropUrl:'{{url('')}}/user/image',
        loadPicture: image_path,
        doubleZoomControls:false,
        rotateControls:false,
        enableMousescroll:true,
        cropData:{
            "image_name":image_name,
        },
        modal:false,
        onReset:function(){ 
          $("#cropContainerPreload").html("");
          profile_picture_delete();
        },
        onAfterImgCrop:function(res){ 
        $('#cropContainerPreload img').attr({
            width:'100%',
            height:'500px'
        });
        $("#image_id").val(res.image_id);
        $("#file_name").text("{{ Lang::get('core.photo_save') }}");
        submit_button.removeAttr('disabled');
        },
      }
     var cropContainerPreload = new Croppic('cropContainerPreload', croppicContainerPreloadOptions);
     $('#cropContainerPreload img').hide();
     imageLoading();
}

function imageLoading(){
    var imgLoad = new imagesLoaded('#cropContainerPreload');
    imgLoad.on( 'done', function( instance ) {
        setTimeout(function(){
          $("#image_up").hide();
          image_prograss.html(" ");
          $('#cropContainerPreload img').show();
        },1000);
    });
}
</script>


<script>
var main = function() {
 
 /* Push the body and the nav over by 285px over */
  
$('.mobile_menu .icon-menu').click(function() {
  
  $('.mobile_menu .menu').animate({left: "0px"
   
 }, 200);

    
$('body').animate({left: "285px"}, 200);
 
 });

  
/* Then push them back */
  
$('.mobile_menu .icon-close').click(function() {
   
 $('.mobile_menu .menu').animate({
    
  left: "-285px"
   
 }, 200);

    
$('body').animate({
    
  left: "0px"
  
  }, 200);
  
});

};



$(document).ready(main);
</script>

<script type="text/javascript">
$(document).ready(function(){
	
/*	$('.mobile_menu .icon-menu').click(function(){
		$('body').addClass('open_sidebar');
		});
			$('.mobile_menu .icon-close').click(function(){
		$('body').removeClass('open_sidebar');
		});*/
		
		$(function(){
$('ul li a').on('click', function(){
    $(this).parent().addClass('active').siblings().removeClass('active');
  });
});
		
	});
	
	
	
</script>





