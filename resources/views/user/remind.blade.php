@extends('layouts.login')

@section('content')
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,600,300' rel='stylesheet' type='text/css'>
<!-- Login wrapper -->
<!--<div class="sbox">
	<div class="sbox-title">
		<span class="text-semibold"><i class="icon-user-plus"></i> {{ CNF_APPNAME}}</span>
	</div>	
	<div class="sbox-content">
	<div class="text-center">
		<img src="{{ asset('assets/bsetec/images/logo-bsetec.png')}}" width="90" height="90" />
	</div>
		{!! Form::open(array('url' => 'user/doreset/'.$verCode, 'class'=>'form-vertical')) !!}
	
	    	@if(Session::has('message'))
				{{ Session::get('message') }}
			@endif
				

		<div class="form-group has-feedback">
			<ul class="parsley-error-list">
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>			
		</div>			
				
		<div class="form-group has-feedback">
			<label>New Password </label>
			{!! Form::password('password',  array('class'=>'form-control', 'placeholder'=>'New Password')) !!}
			<i class="icon-lock form-control-feedback"></i>
		</div>
		
		  <div class="form-group has-feedback">
			<label>Re-type Password</label>
		   {!! Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Confirm Password')) !!}
			<i class="icon-lock form-control-feedback"></i>
		</div>
      <div class="form-group has-feedback">
      		<label></label>
			<div class="col-xs-6">
			  <button type="submit" class="btn btn-primary pull-right">Reset My Password</button>
			</div>
      </div>
	  		
	
	 {!! Form::close() !!}
	 </div>
</div>--> 
</div>
<!-- /login wrapper -->



<div class="reset-pwd animated fadeInDown">
<div class="login-s">
{!! Form::open(array('url' => 'user/doreset/'.$verCode, 'class'=>'form-vertical')) !!}

<h3>password reset</h3>
@if(Session::has('message'))
				{!! Session::get('message') !!}
			@endif
<ul class="parsley-error-list">
				@foreach($errors->all() as $error)
					<li class="alert alert-danger">{{ $error }}</li>
				@endforeach
			</ul>
  <label>new password</label>
<div class="form-group animated fadeInLeft">
<i class="icon-lock"></i>
{!! Form::password('password',  array('placeholder'=>'New Password')) !!}
</div>
<label>Re-type Password</label>
<div class="form-group animated fadeInLeft">
<i class="icon-lock"></i>
{!! Form::password('password_confirmation', array('placeholder'=>'Confirm Password')) !!}
</div>
 <button type="submit" class="btn btn-color animated fadeInLeft">Reset My Password</button>
{!! Form::close() !!}
    </div>
</div>

@endsection