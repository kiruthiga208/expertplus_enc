@extends('layouts.frontend')
@section('content')
<div class="reg_form new_reg_form mycourse_block">

            <div id="mainwrapper">    
            
            <div class="head_block learning-b clearfix">
         <h2 class="title">{!! Lang::get('core.mycourses')!!}</h2>   
          @if (defined('CNF_CURRENCY'))
            @php  $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
          @endif 
        <div class="select-section">
            <form class="mycourse_learning" action="{{Request::url('')}}" id="learn-form">
                {!! Form::select('q', array(
                'all' => Lang::get('core.ALL'),
                'completed' => Lang::get('core.complete'),
                ),Input::get('q'),array('class' => 'selectpicker select_mrg','id'=>'mycourseFilter'))!!}
            </form> 
        </div>

        </div>
        
        <p class="mycourse-p">{!! Lang::get('core.mycourse_text') !!}</p>
        
           <div class="col-sm-12"> 
                  <div class="tab-block-mycourse clearfix">
                         <ul class="nav nav-tabs usernavbar price_align post">
                            <li class="current_page_item active"><a href="#LearningCourse" data-toggle="tab"> {{ Lang::get('core.Learning') }}</a></li>
                            <li><a href="{{url('user/mycourse')}}" >{{ Lang::get('core.Teaching')}}</a></li>
                            <li><a href="{{url('user/wishlist')}}" >{{ Lang::get('core.Wishlist') }}</a></li>
                            
                        </ul>
                 </div>
          </div>
             


                        <div id='content' class="HomeScroll 1 tab-content new_tabs">
                        
                        
                        <div id="LearningCourse" class="usergallery tab-pane active MyCourseDiv">
			   

			@if(count($mycourse) == 0)
			<p class="empty_courses">{!! Lang::get('core.learning_empty')!!} </p>
			<p class="discover_courses"><a href="{{url('course')}}">{!! Lang::get('core.discover_course') !!} {{ CNF_APPNAME }} {!! Lang::get('core.discover_course1') !!}</a></p>
			@endif

                        @php  $j=0 @endphp
                            @foreach($mycourse as $course)

                          @php  $cpercentage = \bsetecHelpers::completedPercentage($course->course_id) @endphp
                          @php $completion = explode('--X--',$cpercentage); @endphp
                            @php $total = json_decode($course->curriculum); @endphp
                                           
                           @php $tot_count = 1 @endphp
                            @if($total)
                            @for ($tot=0; $tot < count(@$total->MainTitle); $tot++)
                            @if($total->MainTitle[$tot] != 'Section')
                            @php  $couting = $tot_count++; @endphp
                            @endif
                            @endfor
			    @else
			    @php $couting = 1; @endphp
                            @endif

                            @php ( $percentage = round((count($completion) / $couting ) * 100) )
				@if(Input::get('By') == 'completed')
				@if($percentage == 100 )

                            <div class="col-xs-6 col-sm-3">
                             <div class="block_course clearfix">
            <div class="business_office">

                <div class="image">
                 <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}"><img id="image-3" data-src="{{ \bsetecHelpers::getImage($course->image) }}" data-original="{{ \bsetecHelpers::getImage($course->image) }}" src="{{ asset('assets/bsetec/images/spacer.gif') }}"  class="thumb lazy" alt="{{{ $course->course_title }}}" style="background-image: url('{{ \bsetecHelpers::getImage($course->image) }}');"/>
                </a>
                @if($course->featured)
                    <button class="feature follow_b" id="{{$course->course_id}}">{!! Lang::get('core.Featured') !!}</button>
                @else
                    <button class="feature follow_b" id="{{$course->course_id}}">{!! Lang::get('core.unfeatured') !!}</button>
                @endif

                <div class="course_detail clearfix">
                <h4>
                         <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ str_limit(ucfirst($course->course_title),30) }}}</a>
                    </h4>
                    <div class="star_rating">
                    <ul class="star_one clearfix">
                         <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($course->course_id, $course->user_id)}}"></li>
                    </ul>
                </div>
                      <p class="rate">
                        @if($course->pricing != 0)
                       {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                        @else
                       {!! Lang::get('core.free') !!}
                        @endif
                    </p>
                </div>
                
            </div>
            </div>
        </div>

                @endif
                @else

<div class="col-xs-6 col-sm-3">
 <div class="block_course clearfix">
            <div class="business_office">

                <div class="image">
                 <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}"><img id="image-3" data-src="{{ \bsetecHelpers::getImage($course->image) }}"  data-original="{{ \bsetecHelpers::getImage($course->image) }}" src="{{ asset('assets/bsetec/images/spacer.gif') }}"  class="thumb lazy" alt="{{{ $course->course_title }}}" style="background-image: url('{{ \bsetecHelpers::getImage($course->image) }}');"/>
                </a>
                @if($course->featured =='')
                    <button class="feature follow_b" id="{{$course->course_id}}">{!! Lang::get('core.Featured') !!}</button>
                @else
                    <button class="feature follow_b" id="{{$course->course_id}}">{!! Lang::get('core.unfeatured') !!}</button>
                @endif
                
               </div>
             
             <div class="course_detail clearfix">
                <h4>
                         <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ str_limit(ucfirst($course->course_title),30) }}}</a>
                    </h4>
                   
                
                <div class="star_rating">
                    <ul class="star_one clearfix"> 
                        <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($course->course_id, \Session::get('uid'))}}"></li> 
                    </ul>
                </div>
                <p class="rate">
                        @if($course->pricing != 0)
                            {{ $currency.' '.$course->pricing}}
                        @else
                         {!! Lang::get('core.free') !!}
                        @endif
                    </p>
                </div>
                
            </div>
            </div>
        </div>
                @endif

                            @endforeach
                        </div>

                        

            </div>
        </div>

</div>

<script type="text/javascript">
$(function() {
	$('#front-header').addClass('bsetec-init');
    $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});
    $(".feature").click(function(){
        var element = $(this);
        var noteid = element.attr("id");
        var spanval = element.html();
        $.ajax({
           type: "POST",
           url: "{{ URL::to('user/featured')}}",
           data: {course_id:noteid,action:spanval},
           success: function(a){
                element.html(a);
           },
           error:function(){
            alert("{!! Lang::get('core.error') !!}");
           }
         });
        return false;
    });
	$('#mycourseFilter').on('change',function(e){
    var form = $('#learn-form');
    // form.attr('action',form.attr('action')+'?q='+$(this).val());
    History.pushState(null, 'Expert Plus | My Courses', form.attr('action')+'?q='+$(this).val());
    //form.submit();
    });
});



</script>
@stop