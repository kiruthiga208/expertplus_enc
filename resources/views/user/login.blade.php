@extends('layouts.frontend')

@section('content')



<div class="login-s animated fadeInDown delayp1">
<h3>{{ Lang::get('core.Login') }}</h3>
@if(Session::has('message'))
				{!! Session::get('message') !!}
			@endif
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
<div class="left-s">
{!! Form::open(array('url'=>'user/signin', 'class'=>'form-vertical')) !!}
<label>{{ Lang::get('core.email') }}<span class="req">*</span></label>
<div class="form-group animated fadeInLeft">
<i class="fa fa-user"></i>
{!! Form::text('email', null, array('placeholder'=>Lang::get('core.email') ,'required'=>'email')) !!}
</div>
<label>{{ Lang::get('core.password') }}<span class="req">*</span></label>
<div class="form-group animated fadeInRight">
<i class="fa fa-lock"></i>
{!! Form::password('password', array('placeholder'=>Lang::get('core.password') ,'required'=>'')) !!}
</div>
	@if(CNF_RECAPTCHA =='true') 
	<label class="text-left"> {!! Lang::get('core.captcha')!!} <span class="req">*</span></label>
       {{ Lang::get('core.case_sensitive') }}
    <div class="captcha-block">
    {!! captcha_img() !!}
		<div class="form-group animated fadeInRight">
        <i class="fa fa-lock"></i>
        <input type="text" name="captcha" placeholder="{!! Lang::get('core.security_code')!!}" class="form-control" required/>
			
		</div>	
        </div>
	 @endif	

<label for="remmember" class="remember"><input type="checkbox" name="remember"> {{ Lang::get('core.remember') }}</label>
<div class="fgt">
<a href="{!! url('user/forgot') !!}">{{ Lang::get('core.forgotpassword') }}</a>
</div>
@if(CNF_REGIST !='false')
<div class="signup-l">
<a href="{{ URL::to('user/register')}}">{{ Lang::get('core.signup')}}</a>
</div>
@endif
<button type="submit" class="btn btn-color animated fadeInLeft"> {{ Lang::get('core.sb_submit') }} </button> 
	{!! Form::close() !!}	
</div>
<div class="right-s animated fadeInUp">
<p class="login">{{ Lang::get('core.social')}}</p>
@if($socialize['google']['client_id'] !='' || $socialize['twitter']['client_id'] !='' || $socialize['facebook'] ['client_id'] !='') 
			@endif
<ul class="social-link">
<li class="animated fadeInLeft">@if($socialize['facebook']['client_id'] !='') 
<a href="{{ URL::to('user/socialize/facebook')}}" class="btn btn-primary"><i class="icon-facebook"></i> <span lang="fb">{{ Lang::get('core.facebook')}}</span></a>
@endif</li>
<li class="animated fadeInRight">@if($socialize['twitter']['client_id'] !='') 
				<a href="{{ URL::to('user/socialize/twitter')}}" class="btn btn-info"><i class="icon-twitter"></i> <span class="twit">{{ Lang::get('core.twitter')}}</span> </a>
				@endif</li>
<li class="animated fadeInLeft">@if($socialize['google']['client_id'] !='') 
<a href="{{ URL::to('user/socialize/google')}}" class="btn btn-danger"><i class="icon-google"></i> <span class="google_plus">{{ Lang::get('core.google')}} </span></a>
@endif</li>
</ul>
</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		$('body').removeClass();
		$('body').addClass('sxim-init');
		$('body').addClass('login-b');
		$('#front-header').addClass('front-header');	
		$('#or').click(function(){
		$('#fr').toggle();
		});
	});
	$('input[type="checkbox"],input[type="radio"]').iCheck({
   checkboxClass: 'icheckbox_square-green',
   radioClass: 'iradio_square-green',
  });
</script>

@stop
