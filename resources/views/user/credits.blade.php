@extends('layouts.frontend')
@section('content')
<div class="credit-history">
<div class="head-block clearfix">
<div class="title-s">
@if (defined('CNF_CURRENCY'))
        @php ($currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
<?php
$uid=\Auth::user()->id;
?>

<h2>{!! Lang::get('core.credits') !!}    {!! SiteHelpers::getCurrencymethod($uid,$total_credits) !!}</h2>
</div>

<div class="right-block">
<a href="{!! url('user/requests')!!}" class="btn btn-success pull-right">{!! Lang::get('core.withdraws') !!}</a>
@if(count($credits)>0)
<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal">{!! Lang::get('core.withdraw_request') !!}</button>
@endif
</div>

</div>
<div class="table-block">

<table class="table">
	<tr>
		<th>#</th>
		<th>{!! Lang::get('core.user') !!}</th>
		<th>{!! Lang::get('core.course') !!}</th>
		<th>{!! Lang::get('core.credit_for') !!}</th>
		<th>{!! Lang::get('core.u_credit') !!}</th>
		<th>{!! Lang::get('core.on') !!}</th>
	</tr>
	@if(count($credits)>0)
	@foreach ($credits as $key => $credit)
		<tr>
			<td>{!! ++$serial_no !!}</td>
			<td>{!! $credit->customer_name !!}</td>
			<td>{!! $credit->course_title !!}</td>
			<td>{!! ucwords(str_replace('_', ' ', $credit->credits_for)) !!}</td>
			<td> {!! SiteHelpers::getCurrencymethod($uid,$credit->credit,'1' ) !!}</td>
			<td>{!! date('d-m-Y h:i:a', $credit->created_at) !!}</td>
		</tr>
	@endforeach
	@else
	<tr>
			<td colspan="6" align="center"> {!! Lang::get('core.no_credit')!!} </td>
	</tr>
	@endif
</table>
</div>
@if(count($credits)>0){!! str_replace('/?', '?', $credits->render()) !!}  @endif
</div>

@if(count($credits)>0)
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{!! Lang::get('core.withdraw_request') !!}</h4>
      </div>
      <div class="modal-body">
        	{!! Form::open(array('url'=>'user/withdraw-request', 'class'=>'form-horizontal row', 'files' => true, 'parsley-validate'=>'','novalidate'=>' ')) !!}
				<div class="col-sm-12">
				
					  <div class="form-group">
					    <label for="ipt" class=" control-label col-md-4">{!! Lang::get('core.paypal_id') !!}</label>
						<div class="col-md-8">
							<input name="paypal_id" type="email" class="form-control input-sm" required  value="" />  
						</div> 
					  </div>

					<div class="form-group">
					    <label for="ipt" class=" control-label col-md-4">{!! Lang::get('core.withdraw_amt') !!}</label>
						<div class="col-md-8">
							<input name="amount" type="number" class="form-control input-sm" required  value="" />  
						</div> 
					</div>
					
				  <div class="form-group">
				    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
					<div class="col-md-8">
						<button class="btn btn-color" type="submit">{!! Lang::get('core.send_req') !!}</button>
					 </div> 
				  </div> 
				</div>
			{!! Form::close() !!}
      </div>

    </div>

  </div>
</div>
@endif
<script>
$(function()
{
	$('body').removeClass();
	$('body').addClass('bsetec-init');
	$('#front-header').addClass('front-header');
});
</script>
@stop