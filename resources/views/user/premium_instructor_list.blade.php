@extends('layouts.app')

@section('content')
<div class="page-content row">
  <!-- Page header -->
  <div class="page-header">
    <div class="page-title">
      <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
    </div>
    <ul class="breadcrumb">
      <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
	    <li><a href="">{{ $pageTitle }}</a></li>
    </ul>
  </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>

    <div class="sbox animated fadeInRight">
      <div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5></div>
      <div class="sbox-content">  
        <div class="toolbar-line ">
          <a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Approve" data-type="1"><i class="fa fa-check"></i>&nbsp;{{ Lang::get('core.approve') }}</a>
          <a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Unapprove" data-type="0"><i class="fa fa-times"></i>&nbsp;{{ Lang::get('core.unapprove') }}</a>   
        </div>    
  
  
      {!! Form::open(array('url'=>'user/approval/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
      {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
      <div class="table-responsive course-listing-admin" style="min-height:300px;">
      <table class="table table-striped ">
          <thead>
          <tr>
            <th class="number"> {{ Lang::get('core.no') }} </th>
            <th> <input type="checkbox" class="checkall" /> </th>
            <th> Username </th>
            <th> Email </th>
            <th> Last Login </th>
            <th> Approval </th>
          </tr>
          </thead>
            @if(count($instructors)>0)
              @php ($i = ($instructors->currentPage() - 1) * $instructors->perPage() + 1)
              @foreach($instructors as $instructor)
                @php ($userdetails = \bsetecHelpers::getuserinfobyid($instructor->user_id))
          <tbody>
            <tr>
            <td width="30">{{ $i++ }}  </td>
            <td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $instructor->user_id }}" />  </td>                  
            <td>{{ $userdetails->username }}</td> 
            <td>{{ $userdetails->email }}</td> 
            <td>{{ $userdetails->last_login }}</td>
            @if($instructor->instrctor_status=='1')
            <td><span class="label label-success">Approved</span></td>         
            @elseif($instructor->instrctor_status=='0')
            <td><span class="label label-danger">Unapproved</span></td>
            @endif
            </tr>
          </tbody>
            @endforeach
          @endif
      </table>
      </div>
      {!! Form::close() !!}
      <div class="col-sm-4"> {!! str_replace('/?', '?', $instructors->render()) !!} </div>
  </div>
</div>  
</div>

<script>
  $(document).on('click','.updatestatus',function(){
    var status = $(this).data('type');
    $('#actiontype').val(status);
    if($("input[name='id[]']:checked").length==0){
      alert('{{ Lang::get('core.select_checkbox') }}');
    }else{
      $('#bsetecTable').submit();
    }
  });
</script>

@stop