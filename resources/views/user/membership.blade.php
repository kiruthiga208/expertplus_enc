@if (defined('CNF_CURRENCY'))
@php ($currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@else
@php ( $currency = '$' )
@endif

<div class="course_info account-b">
<div class="container">
<div class="row">
<div class="col-xs-12 col-sm-3">

@include('user.user_menu')

</div>
<div class="col-xs-12 col-sm-9">
<div class="block2 account_section clearfix">
<div class="promo_block">
<h2>{!! Lang::get('core.membership') !!}</h2>
<p>{!! Lang::get('core.info_membership')!!}</p>
</div>
<div class="image_sec subblock">

		@foreach($errors->all() as $error)
			<p class="alert alert-danger">{{ $error }}</p>
		@endforeach

   @if(Session::has('message'))
      {!! Session::get('message') !!}
   @endif


	<div id="pass" class="account_block">

      @if(isset($membership->status) && $membership->status == 'failed')
      <div class="alert alert-danger">
              <h4>Note&hellip;</h4>
              <p>{!! Lang::get('core.membership_failed_for').$membership->plan_name.Lang::get('core.membership_failed_on').date("F d, Y h:i:s A e",strtotime($membership->created_at)).Lang::get('core.membership_failed_refund') !!}</p>
      </div>
      @elseif(isset($membership->status) && $membership->status == 'cancelled')
      <div class="alert alert-danger">
              <h4>Note&hellip;</h4>
              <p>{!! Lang::get('core.membership_cancel_success').date("F d, Y h:i:s A e",strtotime('+1 '.str_replace('ly', '', $membership->purchase_period),strtotime($membership->created_at))) !!}.</p>
      </div>
      @elseif(isset($membership->status) && $membership->status == 'completed')
      <div class="alert alert-success">
              <h4>Note&hellip;</h4>
              <p>{!! Lang::get('core.membership_active').Lang::get('core.next_due_date').': '.date("F d, Y h:i:s A e",strtotime('+1 '.str_replace('ly', '', $membership->purchase_period),strtotime($membership->created_at))) !!}</p>
      </div>
      @endif

    <label>{!! Lang::get('core.current_membership') !!}:</label>
    <div class="form-group">
    <div class="email_edit">
      @if(isset($membership) && $membership->status != 'failed')
      <p><strong>{!! $membership->plan_name !!}</strong> {!! Lang::get('core.Membership') !!} <span class="arrow">{!! $currency.$membership->amount.' '.$membership->purchase_period !!}</span></p>
      @else
  	  <p><strong>{!! Lang::get('core.free') !!}</strong> {!! Lang::get('core.membership') !!}</p>
      @endif
      </div>
    </div>

    </div>
</div>

<div class="m-t-xl m-b-xl text-center">
@if(isset($membership->status) && $membership->status == 'cancelled')
    <a href="{{ URL::to('user/membership') }}" class="btn btn-color m-b-sm" type="submit">{!! Lang::get('core.change_membership') !!} </a>
@elseif(isset($membership->status) && $membership->status == 'completed')
    <a href="{{ URL::to('user/membership') }}" class="btn btn-color m-b-sm" type="submit">{!! Lang::get('core.change_membership') !!} </a>
		<a href="{{ URL::to('payment/cancelmembership') }}" class="btn btn-color btn-color-warning m-b-sm confirmcancel" type="submit">{!! Lang::get('core.cancel_membership') !!} </a>
@else
    <a href="{{ URL::to('user/membership') }}" class="btn btn-color m-b-sm" type="submit">{!! Lang::get('core.change_membership') !!} </a>
@endif
  </div>

</div>


</div>
</div>
</div>

</div>
<script type="text/javascript">
$(function(){
  $('.confirmcancel').click(function(event) {
      event.preventDefault();
      var r=confirm("{!! Lang::get('core.sure_you_want_to_cancel_membership') !!}");
      if (r==true)   {
         window.location = $(this).attr('href');
      }

  });
});
</script>














