@extends('layouts.frontend')
@section('content')
<div class="reg_form new_reg_form mycourse_block">

            <div id="mainwrapper">
 
        <div class="head_block clearfix">
         <h2 class="title">{!! Lang::get('core.mycourses')!!}</h2> 
                      </div>
          @if (defined('CNF_CURRENCY'))
           @php ( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
          @endif 
                      <p class="mycourse-p">{!! Lang::get('core.mycourse_text') !!} </p>
                      
          <div class="col-sm-12"> 
                    <div class="tab-block-mycourse clearfix">
                         <ul class="nav nav-tabs usernavbar price_align post">
                            <li><a href="{{url('user/learning')}}" > {{ Lang::get('core.Learning') }}</a></li>
                            <li><a href="{{url('user/mycourse')}}" >{{ Lang::get('core.Teaching')}}</a></li>
                            <li class="current_page_item active"><a href="#WishlistCourse" data-toggle="tab">{{ Lang::get('core.Wishlist') }}</a></li>
                            
                        </ul>
                    </div>
           </div>


                        <div id='content' class="HomeScroll 1 tab-content new_tabs">
                        
                        

                        <div id="WishlistCourse" class="tab-pane active MyCourseDiv wishlistblocks clearfix">
                  			@if(count($favourites) == 0)
                  			<p class="empty_courses">{!! Lang::get('core.wishlist_text')!!} </p>
                  			<p class="discover_courses"><a href="{{url('course')}}">{!! Lang::get('core.discover_course') !!} {{ CNF_APPNAME }} {!! Lang::get('core.discover_course1') !!}</a></p>
                  			@else
                        @php ( $k=0 )
                        <?php //echo '<pre>'; print_r($favourites->favorites); echo '</pre>'; ?>
                        @foreach($favourites as $course)

                            <div class="col-xs-6 col-sm-3">
                             <div class="block_course clearfix">
            <div class="business_office">

                <div class="image">
                  <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}"><img id="image-3" data-src="{{ \bsetecHelpers::getImage($course->image) }}" data-original="{{ \bsetecHelpers::getImage($course->image) }}" src="{{ asset('assets/bsetec/images/spacer.gif') }}" class="thumb lazy" alt="{{{ $course->course_title }}}" style="background-image: url('{{ \bsetecHelpers::getImage($course->image) }}');"/></a>
               </div>
              <div class="course_detail clearfix">
                <h4>
                         <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{{ str_limit(ucfirst($course->course_title),30) }}}</a>
                    </h4>
                    
                    <div class="star_rating">
                    <ul class="star_one clearfix">
                       <li class="late_star ReadonlyRating startvalue star_rating" data-score="{{\bsetecHelpers::checkReview($course->course_id, $course->id)}}"></li>
                    </ul>
                </div>
                    <p class="rate">
                        @if($course->pricing != 0)
                        {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                        @else
                        {!! Lang::get('core.free')!!}
                        @endif
                    </p>
                </div>
                
            </div>
            </div>
        </div>

                        @endforeach
             @endif
            </div>

            </div>
        </div>

</div>
<script type="text/javascript">
$(function() {
  $('body').removeClass();
  $('body').addClass('bsetec-init');
  $('#front-header').addClass('front-header');	
  $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});
    $(".feature").click(function(){
        var element = $(this);
        var noteid = element.attr("id");
        var spanval = element.html();
        var info = 'id=' + noteid+'&featured='+spanval;
        var siteurl = $('#getSiteUrl').val();
         $.ajax({
           type: "POST",
           url: siteurl+"/featured",
           data: info,
           success: function(a){
                element.html(a);
           }
         });
        return false;
    });
});
</script>
@stop