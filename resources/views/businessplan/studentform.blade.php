@extends('layouts.app_clone')
@section('content')

<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {!! Lang::get('core.create_student') !!} </h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('businessplan/students') }}">{{ Lang::get('core.students') }}  </a></li>
			<li class="active">{!! Lang::get('core.create_student') !!} </li>
		</ul>	

	</div>
	
	
	<div class="page-content-wrapper m-t">	 	

		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>

			</div>
			<div class="sbox-content clearfix"> 

				@if(Session::has('message'))	  
				{!! Session::get('message') !!}
				@endif		

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>

				{!! Form::open(array('url'=>'businessplan/createstudent/'.$id, 'class'=>'form-horizontal' ,'id' =>'profileForm' )) !!}
				<div class="col-md-12">
					<fieldset><p><legend> {!! Lang::get('core.create_student') !!}</legend></p>
						

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.firstname') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::text('firstname', $first_name, array('placeholder'=>Lang::get('core.firstname') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div> 

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.lastname') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::text('lastname', $last_name, array('placeholder'=>Lang::get('core.lastname') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div> 

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.username') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::text('username', $username, array('placeholder'=>Lang::get('core.username') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div>

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.email') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::text('email', $email, array('placeholder'=>Lang::get('core.email') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div> 

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.password') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::password('password', array('placeholder'=>Lang::get('core.password') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div> 

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.repassword') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							{!! Form::password('password_confirmation', array('placeholder'=>Lang::get('core.conewpassword') ,'required'=>'','class'=>'form-control' )) !!}
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>
						</div>  

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.Status') }}<span class="asterisk">*</span></label>
							<div class="col-md-6">
							   	<label class='radio radio-inline' >
					    		{!! Form::radio('active',1,($status==1) ? true:false,array()) !!} Active
					    		</label>
					    		<label class='radio radio-inline'>
								{!! Form::radio('active',0,($status==0) ? true:false,array()) !!} Inactive
					 			</label>
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div> 
						</div>



						{!! Form::hidden('id', $id) !!}

					</fieldset>
				</div>


				<div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	

						<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>

					</div>	  

				</div> 

				{!! Form::close() !!}


			</div>
		</div>	
	</div>	  
</div>	
@stop