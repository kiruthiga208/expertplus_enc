@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('businessplan?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('businessplan?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<!-- <a href="{{ URL::to('businessplan/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a> -->
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Id</td>
				<td>{{ $row->business_plan_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Name</td>
				<td>{{ $row->business_plan_name }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Amount</td>
				<td>{{ $row->business_plan_amount }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Interval</td>
				<td>{{ $row->business_plan_interval }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Periods</td>
				<td>{{ $row->business_plan_periods }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Statement</td>
				<td>{{ $row->business_plan_statement }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Business Plan Solicitor Count</td>
				<td>{{ $row->business_plan_solicitor_count }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Modified At</td>
				<td>{{ $row->modified_at }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop