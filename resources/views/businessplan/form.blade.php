@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('businessplan?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>

    </div>

 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content">

			@if($row['business_plan_id']!='')
				@php( $pgurl = 'businessplan/save/'.$row['business_plan_id'].'?return='.$return )
			@else
				@php( $pgurl = 'businessplan/save?return='.$return )
			@endif

		 {!! Form::open(array('url'=>$pgurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Business Plan</legend>

							<div class="form-group  " >
								<label for="Sub Plan Name" class=" control-label col-md-4 text-left">  Name <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('business_plan_name', $row['business_plan_name'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  " >
								<label for="Sub Plan Amount" class=" control-label col-md-4 text-left">   Amount <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('business_plan_amount', $row['business_plan_amount'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
							</div>
							<div class="col-md-2">

							</div>
							</div>
							<div class="form-group  " >
								<label for="Sub Plan Interval" class=" control-label col-md-4 text-left">  Interval <span class="text-danger">*</span></label>
							<div class="col-md-6">

							{!! Form::select('business_plan_interval', [
							   '1' => 'Monthly',
							   '2' => 'Yearly'], $row['business_plan_interval'], ['id' => 'business_plan_interval_id','class'=>'form-control']
							) !!}

							</div>
							<div class="col-md-2">

							</div>
							</div>
						<!-- 	<div class="form-group  " >
								<label for="Students Count" class=" control-label col-md-4 text-left">  Students Count <span class="text-danger">*</span></label>
							<div class="col-md-6">

							@if($row['business_plan_solicitor_count'] != '0' && $row['business_plan_solicitor_count'] !='10' && $row['business_plan_solicitor_count'] !='20' && $row['business_plan_solicitor_count'] !='30' && $row['business_plan_solicitor_count'] !='1')
							{!! Form::select('business_plan_solicitor_count', [
							   '1' => 'Individual',
							   '10' => '1-10',
							   '20' => '11-20',
							   '30' => '21-30'], 2, ['id' => 'business_plan_solicitor_count','class'=>'form-control']
							) !!}
							@else  
                            {!! Form::select('business_plan_solicitor_count', [
							   '1' => 'Individual',
							   '10' => '1-10',
							   '20' => '11-20',
							   '30' => '21-30'], $row['business_plan_solicitor_count'], ['id' => 'business_plan_solicitor_count','class'=>'form-control']
							) !!}

							@endif

							</div>
							<div class="col-md-2">

							</div>
							</div> -->

							<div class="form-group  " >
								<label for="Sub Plan Statement" class=" control-label col-md-4 text-left">   Course count  <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text("business_plan_course_count",$row['business_plan_course_count'],array("class"=>"form-control")) !!}
							</div>
							<div class="col-md-2">
							
							</div>
							</div>

							<div class="form-group  " >
								<label for="Sub Plan Statement" class=" control-label col-md-4 text-left">  Statement <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text("business_plan_statement[]",$business_plan_statement['0'],array("class"=>"form-control")) !!}
							</div>
							<div class="col-md-2">
							<a href="javascript:void(0);" class="btn btn-success addmore"><i class="fa fa-plus"></i> Add More</a>
							</div>
							</div>

							@if(count($business_plan_statement)>0)
							@foreach($business_plan_statement as $key=>$planvalue)
							@if($key>0)
							<div class="commonMore removeblock{!! $key !!}"><div class="form-group  multichoice"><label class=" control-label col-md-4 text-left"> Statement <span class="text-danger">*</span></label><div class="col-md-6">{!! Form::text("business_plan_statement[]",$planvalue,array("class"=>"form-control")) !!}</div><div class="col-md-2"><a href="javascript:void(0);" class="oremove" data-id="{!! $key !!}">Remove</a></div></div></div>
							@endif
							@endforeach
							@endif

							<!-- div for more options starthere -->
							<div class="moreoptions"></div>
							<!-- end here -->

							 </fieldset>
		</div>


			<div style="clear:both"></div>


				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">
					{!! Form::hidden('business_plan_id', $row['business_plan_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('businessplan?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>

				  </div>

		 {!! Form::close() !!}
	</div>
</div>
</div>
</div>
   <script type="text/javascript">



	$(document).on('click','.addmore',function(){
		var total    = $('.commonMore').length;
		var commonid = total+1; 
		var answer   = total+2;
		$('.moreoptions').append('<div class="commonMore removeblock'+commonid+'"><div class="form-group  multichoice"><label class=" control-label col-md-4 text-left"> Statement <span class="text-danger">*</span></label><div class="col-md-6">{!! Form::text("business_plan_statement[]","",array("class"=>"form-control")) !!}</div><div class="col-md-2"><a href="javascript:void(0);" class="oremove" data-id="'+commonid+'">Remove</a></div></div></div>');
	});
	$(document).on('click', '.oremove', function(){
		var removid = $(this).attr('data-id');
		$('.removeblock'+removid).remove();
	});
	
	
	</script>
@stop