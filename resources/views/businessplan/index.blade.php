@extends('layouts.app')

@section('content')
@php( usort($tableGrid, "SiteHelpers::_sort") )
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==1)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif 
		</div>
	</div>
	<div class="sbox-content"> 	
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('businessplan/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif  
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif 		
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('businessplan/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif			
		 
		</div> 		

	
	
	 {!! Form::open(array('url'=>'businessplan/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" /></th>
				
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1' && $t['label'] != 'Business Plan Solicitor Count')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th>Course count </th>
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
				        

            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->business_plan_id }}" />  </td>									
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	 	@php( $column = $field['field'] )							
					 <td>					 
					 	@if($field['attribute']['image']['active'])					 					
							{!! SiteHelpers::showUploadedFile($row->$column,$field['attribute']['image']['path']) !!}
						@else
							@php( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							@if($column == 'business_plan_interval')
								@if($row->$column == 1) 
									Monthly 
								@elseif($row->$column == 2) 
									Yearly 
								@endif


							<!-- @elseif($column == 'business_plan_solicitor_count')
								@if($row->$column == 1) 
									Individual 
								@elseif($row->$column == 10) 
									1-10 
								@elseif($row->$column == 20) 
									11-20 
								@elseif($row->$column == 30) 
									21-30 
								@elseif($row->$column == 0) 
									Enterprise 
								@endif

								@if($row->$column == 1) 
									Monthly 
								@elseif($row->$column == 2) 
									Yearly 
								@endif

								@if($row->$column != 2 && $row->$column != 1 && $row->$column != 20 && $row->$column != 10 && $row->$column != 30 && $row->$column != 0) 
									{{ $row->business_plan_solicitor_count }} 
								@endif -->

							@elseif($column == 'business_plan_amount')
								@if (defined('CNF_CURRENCY'))
									{{---*/ echo SiteHelpers::getCurrentcurrency(CNF_CURRENCY) /*---}}
								@endif
								{!! SiteHelpers::gridDisplay($row->$column,$field['field'],$conn) !!}
							@else
								{!! SiteHelpers::gridDisplay($row->$column,$field['field'],$conn) !!}
							@endif
						@endif	

					 </td>
					 @endif					 
				 @endforeach
				 <td>{{ $row->business_plan_course_count }}</td>
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('businessplan/show/'.$row->business_plan_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)	
						<a  href="{{ URL::to('businessplan/update/'.$row->business_plan_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif								
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("businessplan/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
@stop