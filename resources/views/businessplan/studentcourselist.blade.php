@extends('layouts.app_clone')
@section('content')
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> Course Status </h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('businessplan/students') }}">{{ Lang::get('core.students') }} </a></li>
			<li class="active">Course Status </li>
		</ul>	

	</div>

	<div class="page-content-wrapper m-t">	 	
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> {!! Lang::get('core.username') !!} - {!! $sname !!} </h5>
			</div>
			<div class="sbox-content clearfix"> 

				@if(Session::has('message'))	  
				{!! Session::get('message') !!}
				@endif		

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>

				{!! Form::open(array('url'=>'', 'class'=>'form-horizontal' ,'id' =>'profileForm' )) !!}
				<div class="col-md-12">
					<fieldset>
					<div>
						<legend>{!! Lang::get('core.course') !!} {!! Lang::get('core.status') !!}
							@if(count($lists)>0)
								<p><label>{!! Lang::get('core.filter_by') !!} </label> 
								{!! Form::select('course_info', $lists, null, ['id' => 'course_info', 'class' => 'pull-right form-control']) !!}</p>
							@endif
						</legend> 
					</div>
						
					@if(count($lists)>0)
					
					<div class="form-group"> 					
						<div class="col-md-12" id="summaryinfo">
							@if(count($courses)>0)
							@foreach($courses as $key => $cid)
							<div class="col-md-12 statusinfo{!! $cid !!} statusshow" id="summaryinfo{!! $key !!}">
							<script type="text/javascript">
							 	$(function(){
							 		$.ajaxSetup({
						              headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
						            });
						            $.ajax({
										type: 'POST',
										url: '<?php echo e(\URL::to("membership/indstatus")); ?>',
										data: 'course_id={!! $cid !!}&studentid={!! $sid !!}&keys={!! $key !!}',
										beforeSend: function() {
											$('.cloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
										},
										success : function(data) {
											console.log(data);
											$('#summaryinfo{!! $key !!}').html(data);
											return true;
										},complete: function() {
											$('.cloader').html('');
										}
									});
									
							 	});

							</script>
							</div>
							@endforeach
							@endif
						</div>
					</div>

					@else

					<div class="form-group"> 					
						<div class="col-md-12">

							<div class="alert alert-warning fade in block-inner">
								<button data-dismiss="alert" class="close" type="button">×</button>
								<i class="icon-warning"></i> {!! Lang::get('core.student_view_message') !!} 
							</div>

						</div> 
					</div> 
					@endif
					</fieldset>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$(document).on('change', '#course_info', function(){
			var cid  = $(this).val();
			if(cid){
				$('.statusshow').hide();
				$('.statusinfo'+cid).show();
			}else {
				$('.statusshow').show();
			}
			
		});
	});
</script>
@stop