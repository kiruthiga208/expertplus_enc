@extends('layouts.app_clone')
@section('content')
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {!! Lang::get('core.students') !!} </h3>
		</div>

		
		  
	</div>
	
	<div class="page-content-wrapper m-t">	 	
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> {!! Lang::get('core.students') !!}</h5>
			<div class="sbox-tools" >
				@if($coursecount>0)
				<a href="{{ URL::to('businessplan/assignuserbysubscription/') }}" class="tips btn btn-sm btn-primary"><i class="fa fa-users"></i>{{ Lang::get('core.assign_course') }}</a>

				<a href="{{ URL::to('businessplan/studentform') }}" class="tips btn btn-sm btn-primary" >{{ Lang::get('core.create_student') }}</a>
				@endif
			</div>
			</div>

			<div class="sbox-content"> 

				@if(Session::has('message'))	  
				{!! Session::get('message') !!}
				@endif		

				@foreach($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
				@endforeach

				<div class="table-responsive" style="min-height:300px;">
					<table class="table table-striped" id="students">
						<thead>
							<tr><th width="10">{{ Lang::get('core.no') }} </th>
								<th>{{ Lang::get('core.avatar') }}</th>
								<th>{{ Lang::get('core.Username') }}</th>
								<th>{{ Lang::get('core.Email') }}</th>
								<th>{{ Lang::get('core.Status') }}</th>
								<th>{{ Lang::get('core.btn_action') }}</th>
							</tr></thead>
							<tbody>

							</tbody>
						</table>
						<input type="hidden" name="md" value="" />
					</div>

				</div>
			</div>	
		</div>	  
	</div>	


<script type="text/javascript">
 	$(function(){
 		var oTable = $('#students').dataTable({
 			"bProcessing": true,
 			"bServerSide": true,
 			"pageLength": 10,
 			"ordering": false,
		    "bLengthChange": false,
		    "bFilter": false,  
		    "sPaginationType": "full_numbers",
 			"aoColumns": [
 			{ "bSortable": false ,"aTargets": [ 0 ] },
 			{ "bSortable": false ,"aTargets": [ 1 ] },
 			{ "bSortable": false ,"aTargets": [ 2 ] },
 			{ "bSortable": false ,"aTargets": [ 3 ] },
 			{ "bSortable": false ,"aTargets": [ 4 ] },
 			{ "bSortable": false ,"aTargets": [ 5 ] }
 			],
 			"fnRowCallback" : function(nRow, aData, iDisplayIndex){      
 				var oSettings = oTable.fnSettings();
 				$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
 				return nRow;
 			},
 			"sAjaxSource": "{{\URL::to('businessplan/allstudents')}}"
 		});

 		
 		//delete students

 		$(document).on('click','.deleteusers',function(){
 			var sid = $(this).data('sid');
 			var r = confirm("Are you sure? You want to delete this student.");
 			if (r == true) {
 				window.location.href = '{{ \URL::to("businessplan/deletestudents") }}'+'/'+sid;
 			}
 		});


 	});

 </script>


@stop