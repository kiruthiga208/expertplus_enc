@extends('layouts.app_clone')
@section('content')

<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {{ Lang::get('core.assign_course') }} </h3>
		</div>

		<ul class="breadcrumb">
			<li><a href="{{ URL::to('businessplan/students') }}">{{ Lang::get('core.students') }} </a></li>
			<li class="active">{{ Lang::get('core.assign_course') }} </li>
		</ul>	

	</div>
	
	
	<div class="page-content-wrapper m-t">	 	

		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> {{ Lang::get('core.assign_course') }} </h5>

			</div>
			<div class="sbox-content clearfix"> 

				@if(Session::has('message'))	  
				{!! Session::get('message') !!}
				@endif		

				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>


				{!! Form::open(array('url'=>'businessplan/assignusersbysubs', 'class'=>'form-horizontal' ,'id' =>'profileForm' )) !!}
				<div class="col-md-12">
					<fieldset><p><legend>  </legend></p>
						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.membership_course_name') }}</label>
							<div class="col-md-6">
								@if(count($course_lists)>0)
								{!! Form::select('course_id', $course_lists, null, ['id' => 'all_course_id','class'=>'form-control']) !!}
								@endif
							</div> 
							<div class="col-md-2">
								<div class="cloader"></div>
							</div>

						</div>

						<div class="form-group"> 
							<label class="control-label col-md-4 text-left">{{ Lang::get('core.student_assign_users') }}</label>
							<div class="col-md-6">
								@if(count($users_lists)>0)
								{!! Form::select('users_id[]', $users_lists, null, ['id' => 'all_users_id', 'multiple' => 'multiple','class'=>'select2']) !!}
								@else
								<a href="{{ URL::to('businessplan/studentform') }}" class="btn btn-default">{{ Lang::get('core.create_student') }}</a>
								@endif
							</div> 
							<div class="col-md-2">

							</div>

						</div>
				<div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	

						<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>

					</div>	  

				</div> 
					

					</fieldset>
				</div>


			

				{!! Form::close() !!}


			</div>
		</div>	
	</div>	  
</div>	

<script type="text/javascript">
	$(function(){

	/*	$(document).on('change','#all_course_id',function(){

			var cid = $(this).val();

			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("businessplan/getuserlistbyid")); ?>',
				data: 'cid='+cid,
				beforeSend: function() {
					$('.cloader').html('<h5><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h5>');
				},
				success : function(data) {
					console.log(data);

					$('#all_users_id').empty();
					$('#all_users_id').append(data);
					return true;
				},complete: function() {
					$('.cloader').html('');
				}
			});
			

		}); */
	});

</script>
@stop