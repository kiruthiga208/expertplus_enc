@extends('layouts.app_clone')
@section('content')
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {{ Lang::get('core.my_course') }} </h3>
		</div>
		
	</div>
	
	<div class="page-content-wrapper m-t">	 	
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5><i class="fa fa-table"></i> {{ Lang::get('core.my_course') }}</h5>
				<div class="course_count">
				<span>Total Number of Course : {{ $totalcount }}</span>
				<span>Created Course :  {{ $createdcount }}</span>
				 </div>
			</div>
			<div class="sbox-content"> 

				@if(Session::has('message'))	  
				{!! Session::get('message') !!}
				@endif		

				@foreach($errors->all() as $error)
				<p class="alert alert-danger">{{ $error }}</p>
				@endforeach

				<div class="table-responsive" style="min-height:300px;">
					<table class="table table-striped" id="students">
						<thead>
							<tr><th width="10">{{ Lang::get('core.no') }} </th>
								<th>{{ Lang::get('core.membership_name') }}</th>
								<th>Number of Courses</th>
								<!-- <th>Created Course count</th> -->
								<th>{{ Lang::get('core.business_request_type') }}</th>
								<!-- <th>{{ Lang::get('core.btn_action') }}</th> -->
							</tr>
						</thead>
							<tbody>

							</tbody>
					</table>
					<input type="hidden" name="md" value="" />
				</div>
		
				</div>
			</div>	
		</div>	  
	</div>	

	<script type="text/javascript">
		$(function(){
			var oTable = $('#students').dataTable({
				"bProcessing": true,
				"bServerSide": true,
				"pageLength": 10,
				"ordering": false,
				"bLengthChange": false,
				"bFilter": false,  
				"sPaginationType": "full_numbers",
				"aoColumns": [
				{ "bSortable": false ,"aTargets": [ 0 ] },
				{ "bSortable": false ,"aTargets": [ 1 ] },
				{ "bSortable": false ,"aTargets": [ 2 ] },
				{ "bSortable": false ,"aTargets": [ 3 ] },
			//	{ "bSortable": false ,"aTargets": [ 4 ] },
			//	{ "bSortable": false ,"aTargets": [ 5 ] }


				],
				"fnRowCallback" : function(nRow, aData, iDisplayIndex){      
					var oSettings = oTable.fnSettings();
					$("td:first", nRow).html(oSettings._iDisplayStart+iDisplayIndex +1);
					return nRow;
				},
				"sAjaxSource": "{{URL::to('businessplan/allcourses')}}"
			});


 		// Add event listener for opening and closing details
 		$('#students tbody').on('click', 'td.details-control', function () {
 			var tr = $(this).closest('tr');
 			var row = table.row( tr );

 			if ( row.child.isShown() ) {
	            // This row is already open - close it
	            row.child.hide();
	            tr.removeClass('shown');
	        }
	        else {
	            // Open this row
	            row.child( format(row.data()) ).show();
	            tr.addClass('shown');
	        }
	    } );


 	});

 </script>


 @stop