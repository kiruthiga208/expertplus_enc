@extends('layouts.app')


@section('content')
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3>{{ Lang::get('core.commision_settings') }}</h3>
      </div>
	  
	 
	  <ul class="breadcrumb">
		<li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('settings') }}">{{ Lang::get('core.commision_settings') }}</a></li>
	  </ul>	  
    </div>

 	<div class="page-content-wrapper">   
	@if(Session::has('message'))
	  
		   {{ Session::get('message') }}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="block-content">
		
<div class="tab-content m-t">
  <div class="tab-pane active use-padding" id="info">	
  <div class="sbox  "> 
  <div class="sbox-title"></div>
  <div class="sbox-content"> 
		 {!! Form::open(array('url'=>'settings/store', 'class'=>'form-horizontal row', 'files' => true, 'parsley-validate'=>'','novalidate'=>' ')) !!}
		<div class="col-sm-6 animated fadeInRight ">
		
			  <div class="form-group">
			    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.commision_percentage') }}</label>
				<div class="col-md-8">
					<input name="commision_percentage" type="number" class="form-control input-sm" required  value="{!! $commision_percentage !!}" />  
				</div> 
			  </div>


			<div class="form-group">
			    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.commision_to') }}</label>
				<div class="col-md-8">
					<select name='commision_to_admin_id' rows='5' id='group_id'
							class='select2 '  required  ></select> 
				</div> 
			  </div>  

			  <div class="form-group">
			    <label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.minimum_withdraw') }}</label>
				<div class="col-md-8">
					<input name="minimum_withdraw" type="number" class="form-control input-sm" required  value="{!! $minimum_withdraw !!}" />  
				</div> 
			  </div>
			
		  <div class="form-group">
		    <label for="ipt" class=" control-label col-md-4">&nbsp;</label>
			<div class="col-md-8">
				<button class="btn btn-primary" type="submit">{{ Lang::get('core.sb_savechanges') }} </button>
			 </div> 
		  </div> 
		</div>

		
		 {!! Form::close() !!}
	</div>
	</div>	 
</div>
</div>
</div>
</div>

<script type="text/javascript">
	$(document).ready(function() { 
		
		$("#group_id").jCombo("{{ URL::to('core/users/comboselect?filter=BSE_users:id:first_name|last_name&limit=where:group_id:in:(1,2)') }}",
		{  selected_value : '{!! $commision_to_admin_id !!}' });
		 
	});
	</script>	

@stop