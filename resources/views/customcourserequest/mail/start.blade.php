@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>A {!! $item !!} you have awarded has been started</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
<p> Click on the link below to join the {!! $item !!}! <br>
click <a href="{!! $link !!}"> here </a> to view the {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
