@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>A {!! $item !!} awarded to you was completed</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
<p> Thank you for your support to complete this {!! $item !!}! <br>
click <a href="{!! $link !!}"> here </a> to view the {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
