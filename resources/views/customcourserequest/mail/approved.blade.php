@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>Your {!! $item !!} has been approved by Admin</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
<p>Now <b> {!! $title !!} </b> {!! $item !!} has been published!! <br>
click <a href="{!! $link !!}"> here </a> to view your {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
