@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $authorname !!} , </h2>
<p>A {!! $item !!} has been awarded to you</p>
<p>
	{!! $item !!} Title : {!! $title !!}
</p>
<p> Start the conversation going with the Student! <br>
click <a href="{!! $link !!}"> here </a> to view the {!! $item !!}.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
