@extends('layouts.mailtemplate')
@section('content')

<h2>Hello Admin , </h2>
<p>A new Custom Course/Tutorial/Question Request has been created</p>

<p>
    Title : {!! $title !!} submitted by {!! $username !!}
</p>
<p>
    Description : {!! $desc !!}
</p>
click <a href="{!! $link !!}"> here </a> to view the Course/Tutorial/Question Request </p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!}

@stop
