<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.cancel_ccr')!!}</h4>
            </div>
            <div class="modal-body">
                <?php echo Form::open(array('url' => 'customcourserequest/cancel', 'method' => 'post','id'=>'askcancelform')); ?>
                <input name="ccr_id" id="ccr_id" type="hidden" value="{!! $row->ccr_id !!}">
                <input name="final_price" id="final_price" type="hidden" value="{!! $row->final_price !!}">
                <div class="form-group">
                    <h3 for="reason">{!! Lang::get('core.reason')!!}</h3>
                    <input type="text" name="reason" class="form-control required" required id="reason" placeholder="{!! Lang::get('core.reason')!!}">
                </div>
               <!--  <div class="form-group">
                    <h3 for="amount_refund">{!! Lang::get('core.amount_refund')!!} @if($row->payment_type == 'hour') ({!! Lang::get('core.note_hourly')!!}) @endif</h3>
                    <input type="text" name="amount_refund" class="form-control required" required id="amount_refund" placeholder="{!! Lang::get('core.amount_refund')!!}">
                </div> -->
                <button type="submit" id="askcancelsubmit" class="btn btn-color">{!! Lang::get('core.submit')!!}</button>
                <?php  echo Form::close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

	$(document).on('click','.cancel_ccr',function(){
        $('#reason').val('');
		$('#amount_refund').val('');
        $('#cancelModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        return this.optional(element) || parseFloat(value) <= parseFloat($(param).val());
    }, "{!! Lang::get('core.choose_proposeprice')!!}");
	$("#askcancelform").validate({
        rules: {
            reason: {
                required: true,
            },
            amount_refund: {
                required: true,
                lessThanEqual: "#final_price"
            }
        },
        messages: {
            reason: {
                required: "{{ Lang::get('core.reason_req') }}",
            },
            amount_refund: {
                required: "{{ Lang::get('core.amount_refund_req') }}",
            }
        },submitHandler: function() {
            $('#askcancelsubmit').prop("disabled", true);
            return true;
        }
    });

});
</script>