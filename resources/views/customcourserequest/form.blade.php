@extends('layouts.frontend')
@section('content')
@if (defined('CNF_CURRENCY'))
@php  $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
@else
@php $currency = 'USD'; @endphp
@endif
<style>
.btn.btn-xxl{
border-radius: 0;
font-family: open sans;
font-size: 18px;
    text-transform: uppercase;
font-weight: 600;
height: 50px;
}
.search-block{
    padding:0;
    margin-bottom:20px;
}
.search-block .course_searching{
    width:100%;
}
.course_searching .more_one_portiion{
    width:90%;
}
.course_searching .btn{
    width:6%;
    text-align:center;
    padding:0px;
}
.textccr{
    resize: none;
    min-height: 150px;
    overflow-y: scroll;
    font-size: 12px !important;
}
@media (max-width: 768px) {
    .course_searching .btn{
        width:15%;
    }
    .course_searching .more_one_portiion{
        width:85%;
    }
}
</style>
<div class="page-content row">
    <div class="reg_form new_reg_form mycourse_block">
        <div class="row">
            <div class="col-md-6">
                <div id="mainwrapper">
                    <div class="col-sm-12">
                        <div class="head_block clearfix">
                            <h2 class="title">{!! Lang::get('core.create_course_request'); !!}</h2>
                        </div>
                        <div class="mycourse-p">{!! Lang::get('core.ccr_text'); !!}</div>
                    </div>
                </div>
                <ul class="parsley-error-list">
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                {!! Form::open(array('url'=>'customcourserequest/save','method' => 'post','id'=>'customcourse', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
                <div class="col-md-12">
                    <fieldset>
                        {!! Form::hidden('ccr_id', $row['ccr_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="level" class="text-left"> {!! Lang::get('core.Difficultly_Level')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <select name="level" id="level" class="form-control required" required>
                                    <option value="">{!! Lang::get('core.Select_Level')!!}</option>
                                    <option value="easy" @if('easy' == $row['level']) selected="selected" @endif>{!! Lang::get('core.Easy')!!}</option>
                                    <option value="medium" @if('medium' == $row['level']) selected="selected" @endif>{!! Lang::get('core.Medium')!!}</option>
                                    <option value="hard" @if('hard' == $row['level']) selected="selected" @endif>{!! Lang::get('core.Hard')!!}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="category_id" class="text-left"> {!! Lang::get('core.Category')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <select name="category_id" id="category_id" class="form-control required" required>
                                    <option value="">{!! Lang::get('core.select_category')!!}</option>
                                    @foreach(\bsetecHelpers::siteCategories() as $category)
                                    <option value="{!!  $category->id  !!}" @if($category->id == $row['category_id']) selected="selected" @elseif($quest && $quest->category_id == $category->id) selected="selected" @endif>{!!  $category->name  !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="sub_cats" class="text-left"> {!! Lang::get('core.subcategory')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <select name="sub_cat_id" id="sub_cats" class="form-control" >
                                    <option value="">{!! Lang::get('core.select_subcategory')!!}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="ccr_name" class="text-left"> {!! Lang::get('core.Title')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                @if($quest && $quest->question_text != '')
                                {!! Form::text('ccr_name', $quest->question_text,array('class'=>'form-control', 'placeholder'=>'', 'required'=>'required')) !!}
                                @else
                                {!! Form::text('ccr_name', $row['ccr_name'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'required')) !!}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="ccr_description" class="text-left"> {!! Lang::get('core.Description_ccr')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <?php $is_te = '1'; if(empty($row['ccr_description'])) { $row['ccr_description'] = $taccr; $is_te = '0'; } ?>
                                @if($quest && $quest->question_description != '')
                                <textarea name='ccr_description' rows='2' id='ccr_description' data-empty="{{ $is_te }}" class='form-control textccr' required>{{ $quest->question_description }}</textarea>
                                @else
                                <textarea name='ccr_description' rows='2' id='ccr_description' data-empty="{{ $is_te }}" class='form-control textccr' required>{{ $row['ccr_description'] }}</textarea>
                                @endif
                                <textarea name='ccr_description_ex' id='ccr_description_ex' class='form-control textccr_sam hide'>{{ $taccr }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="tutor_help" class="text-left"> {!! Lang::get('core.what_happens')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <span>{!! Lang::get('core.tutor_help')!!}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="luploaddoc" class="text-left"> {!! Lang::get('core.Add_files___')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                {!! Form::hidden('deleteres', url('customcourserequest/docdelete')) !!}
                                <input id="luploaddoc" class="btn btn-color luploaddoc" type="file" name="luploaddoc" data-url="{!! url('customcourserequest/upload') !!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="hidden" id="probar_status" value="0" />
                                <div class="luploadvideo-progressbar meter" ><div class="bar" id="probar" style="width:0%"></div></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div id="docresponse">
                                    @if($quest)
                                    @if(!empty($quest->file_ids))
                                    @php $file_list = json_decode($quest->file_ids) @endphp
                                    @foreach($file_list as $file_id)
                                   @php $file = $model->getFiledata($file_id); @endphp
                                    <div class="file_id_{!! $file_id !!}"><p>{!! $file->file_title !!}<span style="cursor:pointer;" class="arrow resdeletedup" data-id="{!! $file_id !!}"><i class="fa fa-trash-o"></i></span></p></div>
                                    @endforeach
                                    @php $file_data = implode(",",$file_list); @endphp
                                    {!! Form::hidden('file_ids', $file_data,array('id'=>'file_ids')) !!}
                                    @else
                                    {!! Form::hidden('file_ids', '',array('id'=>'file_ids')) !!}
                                    @endif
                                    @else
                                    @if(!empty($row['file_ids']))
                                    @php $file_list = json_decode($row['file_ids']) @endphp
                                    @foreach($file_list as $file_id)
                                    @php $file = $model->getFiledata($file_id); @endphp
                                    <div class="file_id_{!! $file_id !!}"><p>{!! $file->file_title !!}<span style="cursor:pointer;" class="arrow resdelete" data-id="{!! $file_id !!}"><i class="fa fa-trash-o"></i></span></p></div>
                                    @endforeach
                                    @php $file_data = implode(",",$file_list); @endphp
                                    {!! Form::hidden('file_ids', $file_data,array('id'=>'file_ids')) !!}
                                    @else
                                    {!! Form::hidden('file_ids', '',array('id'=>'file_ids')) !!}
                                    @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="skills" class="text-left"> {!! Lang::get('core.Skills')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <?php
                                $skillset = json_decode($row['skills']);
                                $skilldata = '';
                                if(!empty($skillset))
                                $skilldata = implode(',',$skillset);
                                ?>
                                <input id="addskills" required="required" multiple="multiple" style="width:100%" name="skills" type="hidden" value="{!! $skilldata !!}" tabindex="-1">
                            </div>
                        </div>
                        <input type="hidden" id="offline" name="type" value="offline">
                        <input type="hidden" id="fixed" name="payment_type" value="0">
                        <!-- <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="type" class="text-left"> {!! Lang::get('core.Type_of_Assistance')!!} </h3>
                            </div>
                            <div class="show-grid m-l-md">
                                <div class="col-md-4">
                                    <div class="eo-button-box m-0-top-bottom text-center">
                                        <h3> {!! Lang::get('core.Offline')!!} </h3>
                                        <input type="radio" id="offline" name="type" class="hidden" value="offline" @if('offline' == $row['type']) checked="checked" @endif @if('offline' != $row['type'] && 'online' != $row['type']) checked="checked" @endif>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="eo-button-box m-0-top-bottom text-center">
                                        <h3> {!! Lang::get('core.Online')!!} </h3>
                                        <input type="radio" id="online" name="type" class="hidden" value="online" @if('online' == $row['type']) checked="checked" @endif>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="payment_type" class="text-left"> {!! Lang::get('core.Payment_Type')!!} </h3>
                            </div>
                            <div class="show-grid m-l-md">
                                <div class="col-md-4">
                                    <div class="eo-button-box m-0-top-bottom text-center">
                                        <h3> {!! Lang::get('core.Fixed')!!} </h3>
                                        <input type="radio" id="fixed" name="payment_type" class="hidden" value="0" @if('fixed' == $row['payment_type']) checked="checked" @endif checked="checked">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="eo-button-box m-0-top-bottom text-center">
                                        <h3 class="hour_label" @if('hour' != $row['payment_type']) style="text-decoration:line-through" @endif> {!! Lang::get('core.Hour')!!} </h3>
                                        <input type="radio" id="hour" name="payment_type" class="hidden" value="1" @if('hour' == $row['payment_type']) checked="checked" @else disabled="disbled" @endif>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="budget" class="text-left"> {!! Lang::get('core.Budget').' ('.$currency.')' !!} </h3>
                            </div>
                            <div class="col-md-12">
                                {!! Form::text('budget', $row['budget'],array('class'=>'form-control', 'placeholder'=>'',  'required'=>'required' )) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="selected_tutors" class="text-left"> {!! Lang::get('core.tutor_prefer')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <span>{!! Lang::get('core.tutor_prefer_desc')!!} {!! CNF_APPNAME !!} tutors</span>
                                <?php
                                $selected_tutorsset = json_decode($row['selected_tutors']);
                                $selected_tutorsdata = '';
                                if(!empty($selected_tutorsset))
                                $selected_tutorsdata = implode(',',$selected_tutorsset);
                                ?>
                                <input id="addselected_tutors" required="required" multiple="multiple" style="width:100%" name="selected_tutors" type="hidden" value="{!! $selected_tutorsdata !!}" tabindex="-1">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <h3 for="tutor_find" class="text-left"> {!! Lang::get('core.tutor_find')!!} </h3>
                            </div>
                            <div class="col-md-12">
                                <input type="radio" id="hour" name="tutor_any" class="hidden" value="any_tutors" @if('selected_tutors' != $row['tutor_any']) checked="checked" @endif>
                                {!! Lang::get('core.tutor_find_any')!!}
                            </div>
                            <div class="col-md-12 m-t-sm">
                                <input type="radio" id="fixed" name="tutor_any" class="hidden" value="selected_tutors" @if('selected_tutors' == $row['tutor_any']) checked="checked" @endif>
                                {!! Lang::get('core.tutor_find_selected')!!}
                            </div>
                        </div>
                    </fieldset>
                    <div style="clear:both"></div>
                    <div class="form-group">
                        <div class="col-sm-12 m-l-sm">
                            <button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            <div class="col-md-6">
                <div id="mainwrapper">
                    <div class="col-sm-12">
                        <div class="head_block clearfix">
                            <h2 class="title">{!! Lang::get('core.Online_tutor'); !!}</h2>
                        </div>
                    </div>
                </div>
                <!-- {!! $onlinetutors !!} -->
                 @php $onlineusers = \bsetecHelpers::onlineuserslist() @endphp
                 @if(!empty($onlineusers))
                 @foreach($onlineusers as $users)
                   @php $usrinfo=\bsetecHelpers::getuserinfobyid($users->user_id) @endphp
                   <div class="row">
                       <div class="col-sm-2 img_div nopadding">
                        <div class="image">
                        <a href="{{url('profile/'.$usrinfo->username) }}">
                        
                        {!! \SiteHelpers::customavatar('',$usrinfo->id,'normal','thumbs lazy') !!}</a>
                        </div>
                       </div>
                       {{ $usrinfo->first_name.' '.$usrinfo->last_name }}
                    </div><hr>                    
                 @endforeach  
                 @else
                 <div class="row">
                  No tutors in online
                </div>
                 @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(){

    var sub_catr = "{{ $row['sub_cat_id'] }}";
    var sub_catr_set = '1';
    if(sub_catr != ''){
        sub_catr_set = '0';
    }

    var lastResults = {results: [{text:'skill'}]};
    var $example = $("#addskills").select2({
        minimumInputLength: 1,
        maximumInputLength: 23,
        multiple: true,
        quietMillis: 300,
        tags: true,
        initSelection: function (element, callback) {
            callback([
                @if(!empty($skillset))
                @foreach($skillset as $key => $skill)
                    @if($key)
                    {!! ",{ id: '".$skill."', text: '".$skill."' }" !!}
                    @else
                    {!! "{ id: '".$skill."', text: '".$skill."' }" !!}
                    @endif
                @endforeach
                @endif
            ]);
        },
        ajax: {
            multiple: true,
            url: "{!! url('user/getskills')!!}",
            dataType: "json",
            type: "POST",
            data: function (term, page) {
                return {
                    json: JSON.stringify({results: [{id: "1", text:"skill"}]}),
                    q: term
                };
            },
            results: function (data, page) {
                lastResults = data.results;
                return data;
            }
        },
        createSearchChoice: function (term) {
            if(lastResults.some(function(r) { return r.text == term })) {
                return { id: term, text: term };
            } else {
                return { id: term, text: term };
            }
        }
    });

    var lastResults = {results: [{text:'selected_tutors'}]};
    var $example = $("#addselected_tutors").select2({
        minimumInputLength: 1,
        maximumInputLength: 23,
        multiple: true,
        quietMillis: 300,
        tags: true,
        initSelection: function (element, callback) {
            callback([
                @if(!empty($selected_tutorsset))
                @foreach($selected_tutorsset as $key => $selected_tutors)
                    @if($key)
                    {!! ",{ id: '".$selected_tutors."', text: '".$selected_tutors."' }" !!}
                    @else
                    {!! "{ id: '".$selected_tutors."', text: '".$selected_tutors."' }" !!}
                    @endif
                @endforeach
                @endif
            ]);
        },
        ajax: {
            multiple: true,
            url: "{!! url('customcourserequest/instructors')!!}",
            dataType: "json",
            type: "POST",
            data: function (term, page) {
                return {
                    json: JSON.stringify({results: [{id: "1", text:"selected_tutors"}]}),
                    q: term
                };
            },
            results: function (data, page) {
                lastResults = data.results;
                return data;
            }
        },
        // createSearchChoice: function (term) {
        //     if(lastResults.some(function(r) { return r.text == term })) {
        //         return { id: term, text: term };
        //     } else {
        //         return { id: term, text: term };
        //     }
        // }
    });

    var myTextAreas = $('.textccr');
    myTextAreas.on('focus',function() {
        if($(this).attr('data-empty') == '0'){
            $(this).val('');
        }
    });

    myTextAreas.on('blur',function() {
        if($(this).val() == ''){
            $(this).val($('.textccr_sam').val());
            $(this).attr('data-empty','0');
        } else {
            $(this).attr('data-empty','1');
        }
    });


    $('#offline').on('ifChecked', function(event){
        $('#hour').iCheck('disable');
        $('.hour_label').css('text-decoration','line-through');
        $('#fixed').iCheck('check');
    });

    $('#online').on('ifChecked', function(event){
        $('#hour').iCheck('enable');
        $('.hour_label').css('text-decoration','none');
    });

    $('.luploaddoc').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(zip|jpg|jpeg|png|pdf|doc|docx|txt)$/i,
        maxFileSize: 25000000, // 25 MB
        progress: function (e, data) {
            // console.log(data);
            $('#probar_status').val(1);
            var percentage = parseInt(data.loaded / data.total * 100);
            $('#probar').css('width',percentage+'%');
            if(percentage == '100') {
                $('#probar').text('{!! Lang::get("core.lecture_file_process")!!}');
            }
        },
        processfail: function (e, data) {
            $('#probar_status').val(0);
            file_name = data.files[data.index].name;
            alert("{!! Lang::get('core.file_not_allowed_size')!!}");
        },
        done: function(e, data){
            var return_data = $.parseJSON( data.result );
            if(return_data.status==true){
                $('#probar').css('width','0%');
                $('#probar').text('');
                var file_ids = $('#file_ids').val();
                if(file_ids == ''){
                    $('#file_ids').val(return_data.id);
                } else {
                    $('#file_ids').val(file_ids+','+return_data.id);
                }
                $("#docresponse").append('<div class="file_id_'+return_data.id+'"><p>'+return_data.file_title+'<span style="cursor:pointer;" class="arrow resdelete" data-id="'+return_data.id+'"><i class="fa fa-trash-o"></i></span></p></div>');
                $('#probar_status').val(0);
            }
            else{
                alert("Please upload correct format files!!!!");
                 $('#probar').text('');
            }

        }
    });

    $(document).on('click','.resdelete',function () {
        $(this).text('Deleting...');
        var _token=$('[name="_token"]').val();
        var id = $(this).data('id');
        var ccr_id = $('[name="ccr_id"]').val();
        $.ajax ({
            type: "POST",
            url: $('[name="deleteres"]').val(),
            data: "&lid="+ccr_id+"&rid="+id+"&_token="+_token,
            success: function (msg)
            {
                var file_ids = $('#file_ids').val();
                var val = removeValue(file_ids,id,',');
                $('#file_ids').val(val);
                $('.file_id_'+id).remove();
            }
        });
    });

    $(document).on('click','.resdeletedup',function () {
        $(this).text('Deleting...');
        var id = $(this).data('id');
        var file_ids = $('#file_ids').val();
        var val = removeValue(file_ids,id,',');
        $('#file_ids').val(val);
        $('.file_id_'+id).remove();
    });

    var removeValue = function(list, value, separator) {
      separator = separator || ",";
      var values = list.split(separator);
      for(var i = 0 ; i < values.length ; i++) {
        if(values[i] == value) {
          values.splice(i, 1);
          return values.join(separator);
        }
      }
      return list;
    };

    var onceup = '0';
    var subcid = '0';
    @if($quest && isset($quest->sub_cat_id))
    subcid = '{!! $quest->sub_cat_id !!}';
    @endif

    $(document).on('change','#category_id',function(){
      var cats = $(this).val();
      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
      });
      $.ajax({
        type: 'POST',
        url: '<?php echo e(\URL::to("course/subcategory")); ?>',
        data:'cats='+cats+'&subcat=0',
        success : function(data) {
          $('#sub_cats').html(data);
          setTimeout(function(){
              if(subcid != '0' && onceup == '0'){
                onceup = '1';
                $('#sub_cats').val(subcid);
              }
              if(sub_catr_set == '0'){
                sub_catr_set = '1';
                $('#sub_cats').val(sub_catr);
              }
          },1000);
        }
      });

    });
    $('#category_id').trigger('change');
});
</script>
@stop