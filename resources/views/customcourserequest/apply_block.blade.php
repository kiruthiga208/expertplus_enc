<div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.applyccr')!!}</h4>
            </div>
           <div class="modal-body">
                @if(($row->tutor_any == 'any_tutors') || ($row->tutor_any == 'selected_tutors' && in_array(Auth::user()->id,json_decode($row->selected_tutors)) && count(json_decode($row->selected_tutors))))
                <div>
                    <center>
                        <h3>{!! Lang::get('core.choose_bid')!!} </h3>
                        <h4 class="m-t-sm">{!! Lang::get('core.budget')!!} : @if(!empty($currency)){!! $currency !!} @endif <strong>{!! $row->budget !!}</strong> / {!! ucfirst($row->payment_type) !!} </h4>
                        <!--p class="m-t-sm"><em>{!! Lang::get('core.note')!!} : {!! Lang::get('core.choose_bid_note')!!} </em></p-->
                    </center>
                </div>
                <?php echo Form::open(array('url' => 'customcourserequest/apply', 'method' => 'post','id'=>'askApplyform')); ?>
                <input name="budget" id="budget" type="hidden" value="{!! $row->budget !!}">
                <input name="ccr_id" id="ccr_id" type="hidden" value="{!! $row->ccr_id !!}">
                <div class="form-group">
                    <h3 for="price">{!! Lang::get('core.ccrprice')!!} ({!! ucfirst($row->payment_type) !!})</h3>
                    <input type="text" name="price" class="form-control required" required id="price" placeholder="{!! Lang::get('core.ccrprice')!!}">
                </div>
                <button type="submit" id="askapplysubmit" class="btn btn-color">{!! Lang::get('core.submit')!!}</button>
                <?php  echo Form::close(); ?>
                <br>
                <div>
                  <p><strong>Fees and Charges:</strong> Please note that a fee of {!! \bsetecHelpers::get_options('commision_percentage')['commision_percentage']; !!}% with the transaction charge of {!! \bsetecHelpers::get_options('transaction_charges_percentage')['transaction_charges_percentage']; !!}% will be deducted from the your bidding price</p>
                </div>
                @else
                <div>
                    <center>
                        <h3>{!! Lang::get('core.only_applied')!!} </h3>
                        <!--p class="m-t-sm"><em>{!! Lang::get('core.note')!!} : {!! Lang::get('core.choose_bid_note')!!} </em></p-->
                    </center>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function(){

	$(document).on('click','.applyccr',function(){
		$('#price').val('');
        $('#applyModal').modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    $.validator.addMethod('lessThanEqual', function(value, element, param) {
        return this.optional(element) || parseInt(value) <= parseInt($(param).val());
    }, "{!! Lang::get('core.choose_bid_note')!!}");
	$("#askApplyform").validate({
        rules: {
            price: {
                required: true,
                // number: true,
                // lessThanEqual: "#budget"
            }
        },
        messages: {
            price: {
                required: "{{ Lang::get('core.bid_amount') }}",
                // number: "{{ Lang::get('core.numbers_only') }}"
            }
        },submitHandler: function() {
            $('#askapplysubmit').prop("disabled", true);
            return true;
        }
    });

	$('.remove_question').click(function(e){
		if (!confirm("{{ Lang::get('core.sure_remove') }}")) {
			e.preventDefault();
		}
	});

});
</script>