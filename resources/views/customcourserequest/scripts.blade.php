<script>
$(document).ready(function(){
    $('body').attr('class','course_body');
    $('.remove_ccr').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_remove') }}")) {
            e.preventDefault();
        }
    });
    $('#completed_ccr').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_completed_ccr') }}")) {
            e.preventDefault();
        }
    });
    $('.start_ccr').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_startccr') }}")) {
            e.preventDefault();
        }
    });
    $('#request_completion_ccr').click(function(e){
        if (!confirm("{{ Lang::get('core.sure_completion_ccr') }}")) {
            e.preventDefault();
        }
    });
    // $('.cancel_ccr').click(function(e){
    //     if (!confirm("{{ Lang::get('core.sure_cancel_ccr') }}")) {
    //         e.preventDefault();
    //     }
    // });
});
</script>