<link rel="stylesheet" href="{{asset('assets/bsetec/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bsetec/css/redactor.css') }}" />
<script type="text/javascript" src="{{ asset('assets/bsetec/js/redactor.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#redactor').redactor();
	}
);
$(document).ready(function(){
    $(".editt").click(function(){
        var comment_id=$(this).attr('data-id');
        var content=$(this).attr('data-des');
        var forum_id=$(this).attr('data-forumid');
        $('.comment').val(comment_id);
        $('.content').val(content);
        $('.content').html(content);
       	$('.forum_id').val(forum_id);
    });

    $(".delete").click(function(){
		var res=window.confirm('Do you want delete this forum')
		var comment_id=$(this).attr('data-id')
		if(res==true)
			window.location.href="comments/"+comment_id;
	});

   });
</script>
	<section class="services editor_block" id="why">
	<div class="container">
	  <div class="row">
	  <div class="col-sm-9">
	  	 @if(count($forum)>0){!! str_replace('/?', '?', $forum->render()) !!}  @endif
	  	  @foreach($forum as $forum)
		
		  <h3>{{$forum->name}}</h3>
		  <h4>{{ $forum->forum_topic }} -- Forum user: {{$forum->username}} </h4>
		 {{---*/$content=$forum->forum_content /*---}}
		 <p>{{ strip_tags($content) }}</p>
		
	  @endforeach

	  		@foreach($list as $list)
	  		<div class="avatar_img clearfix">
            <div class="row">
            <div class="col-xs-2">
            <img src="{{ URL::to('uploads/users/'.$list->avatar) }}" width="50" /></div>
            <div class="col-xs-10">
            <span>{{$list->username}}</span>
            <p>{{ strip_tags($list->comment)}} -- {{ $date= $list->created_at }}</p>
          
	  			@if( Auth::check() )
					@if(Auth::user()->id == $list->user_id )
					<span class="blog_editblocks">
				    <a title="edit" class="btn btn-info padding_width editt"  data-id="{{$list->comment_id}}" data-des="{{ strip_tags($list->comment)}}" data-forumid="{{$list->forum_id}}" data-toggle="modal" data-target="#myModal1">
				        <i class="fa fa-pencil-square-o"></i>
				    </a>
				     <a title="delete"  data-id="{{$list->comment_id}}" class="btn btn-danger padding_width delete" >
				        <i class="fa fa-trash-o"></i>
				    </a>
					</span>
                    @endif
           
				@endif
                    </div>
             </div>
            </div>
	  		@endforeach
	  	
          
	 	@if( Auth::check())
	 	{!! Form::open(array('url'=>'user-forum/insert/', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		
		<div id="page" style="width:100%;" class="editor_page">
			<label>Your Answer :</label>
			<textarea id="redactor" name="comments">
			</textarea>
		</div>
		{{---*/ $user_id=Auth::user()->id /*---}}
		{{---*/ $forum_id=$forum->forum_id /*---}}
		{{---*/ $slug=$forum->slug /*---}}
		{!! Form::hidden('slug', $slug,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		{!! Form::hidden('user_id', $user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		{!! Form::hidden('forum_id', $forum_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
		
		<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
	 	
	 	{!! Form::close() !!}
		@endif

	  </div>
      	
        <div class="col-sm-3">
        <div class="categories_block">
		<h4>Categories</h4>
		<ul>
		<li><a href="{{url('user-forum')}}"><i class="fa fa-angle-right icon_forum"></i> ALL</a></li>
		@foreach($category as $cat)
		<li><a href="{{url('topic/'.$cat->slug)}}"><i class="fa fa-angle-right icon_forum"> </i> {{ $cat->name }}</a></li>
		@endforeach
		</ul>
</div>
	</div>
        
      </div>
      </div>	   
	</section>


<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog add_forum">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Comments</h4>
      </div>
      <div class="modal-body">
        {{---*/ $id='' /*---}}
		{{---*/ $title='' /*---}}
		{{---*/ $cat_id='' /*---}}
		{{---*/ $desc='' /*---}}
		{{---*/ $status='' /*---}}
		{{---*/ $user_id=Auth::user()->id /*---}}
		{!! Form::open(array('url'=>'user-forum/comments', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 	
		 	<div class="form-group hidethis " style="display:none;">
				<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
				<div class="col-md-6">
				  <input type="hidden" class="comment" value="" name="id" />
				  <input type="hidden" class="forum_id" value="" name="forum_id" />
				  {!! Form::hidden('user_id',$user_id,array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 
				 </div> 
			 </div> 
			   <div class="form-group">
               <div class="row">
               <div class="col-sm-4">
					<label for="editor" class=" control-label text-left"> Description <span class="asterix"> * </span></label>
                    </div>
					<div class="col-sm-8">
					<div class="adjoined-bottom">
						<div class="grid-container">
							<div class="grid-width-50">
								<textarea id="editor" name="content"  class="content"></textarea>
							</div>
						</div>
					</div>
					 </div> 
                     </div>
				</div> 
				
				<div style="clear:both"></div>	
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					</div>	  
				 </div> 
		 	
		 {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>




