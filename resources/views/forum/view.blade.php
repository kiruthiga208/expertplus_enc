@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('forum?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('forum?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('forum/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Forum_Id')!!}</td>
				<td>{{ $row->forum_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Category_Id')!!}</td>
				<td>{{ $row->category_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Forum_Topic')!!}</td>
				<td>{{ $row->forum_topic }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Slug')!!}</td>
				<td>{{ $row->slug }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Forum_Content')!!}</td>
				<td>{{ strip_tags($row->forum_content) }} </td>
				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Created_By')!!}</td>
				<td>{{ $row->created_by }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Status')!!}</td>
				<td>{{ $row->status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Created_At')!!}</td>
				<td>{{ $row->created_at }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{!! Lang::get('core.Updated_At')!!}</td>
				<td>{{ $row->updated_at }} </td>

				</tr>
				
		
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop