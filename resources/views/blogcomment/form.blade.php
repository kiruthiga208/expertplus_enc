@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('blogcomment?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
	@if($row['commentID'])
		@php ( $pageurl = 'blogcomment/save/'.$row['commentID'].'?return='.$return )
	@else
		@php ( $pageurl = 'blogcomment/save/')
	@endif
		 {!! Form::open(array('url'=>$pageurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
						<fieldset><p><legend> {!! Lang::get('core.blog_comments') !!}</legend></p>
									
								  <div class="form-group hidethis " style="display:none;">
									<label for="CommentID" class=" control-label col-md-4 text-left">  {!! Lang::get('core.CommentID') !!}</label>
									<div class="col-md-6">
									  {!! Form::text('commentID', $row['commentID'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="User Id" class=" control-label col-md-4 text-left">  {!! Lang::get('core.User_Id') !!}</label>
									<div class="col-md-6">
									  <select name='user_id' rows='5' id='user_id' code='{$user_id}' 
							class='select2 '   required ></select> 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="BlogID" class=" control-label col-md-4 text-left">  {!! Lang::get('core.BlogID') !!}</label>
									<div class="col-md-6">
									  <select name='blogID' rows='5' id='blogID' code='{$blogID}' 
							class='select2 '    required></select> 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <div class="form-group  " >
									<label for="Comment" class=" control-label col-md-4 text-left">  {!! Lang::get('core.Comment') !!} </label>
									<div class="col-md-6">
									  <textarea name='comment' rows='2' id='comment' class='form-control '  required 
				           >{{ $row['comment'] }}</textarea> 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div> 					
								  <!-- <div class="form-group  " >
									<label for="Created" class=" control-label col-md-4 text-left"> Created </label>
									<div class="col-md-6">
									  
				{!! Form::text('created', $row['created'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
									 </div> 
									 <div class="col-md-2">
									 	
									 </div>
								  </div>  --></fieldset>
			</div>
			
			
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8 btn-section">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('blogcomment?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		$("#user_id").jCombo("{{ URL::to('blogcomment/comboselect?filter=users:id:email') }}",
		{  selected_value : '{{ $row["user_id"] }}' });
		
		$("#blogID").jCombo("{{ URL::to('blogcomment/comboselect?filter=blogs:blogID:title') }}",
		{  selected_value : '{{ $row["blogID"] }}' });
		 
	});
	</script>		 
@stop