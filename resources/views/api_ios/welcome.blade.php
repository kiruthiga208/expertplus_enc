<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="_token" content="{!! csrf_token() !!}"/>
<script src="{!! asset('assets/bsetec/api_ios/js/jquery.v1.8.2.js'); !!}" type="text/javascript"></script>
<link href="{!! asset('assets/bsetec/api_ios/css/api_docstyles.css'); !!}" rel="stylesheet" type="text/css" /> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>EXPERT PLUS API</title>

<script type="text/javascript">
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>   
</head>   
<body>
<h1 id="title">EXPERT PLUS API v1.1 Resources</h1>
<div id="controls">
    <ul>
        <li><a id="toggle-endpoints" href="#">Toggle All Endpoints</a></li>
        <li><a id="toggle-methods" href="#">Toggle All Methods</a></li>
    </ul>
</div>
<ul>
    <li class="endpoint expanded">
        <h3 class="title"><span class="name">Exam methods</span>
            <ul class="actions">
                <li class="list-methods"><a href="#">List Methods</a></li>
                <li class="expand-methods"><a href="#">Expand Methods</a></li>
            </ul>
        </h3>
        <ul class="methods hidden" style="display: block;">		
			<li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Signup via email</span>
                    <span class="uri">/api-ios/user/register</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/register" type="hidden">
                    <span class="description">It is used to register a user. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">First name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>                         
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Login via email</span>
                    <span class="uri">/api-ios/user/login</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/login" type="hidden">
                    <span class="description">It is used to login a user. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Type</td>
                                <td class="para">device_type</td>
                                <td class="parameter">
                                    <input name="device_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Type ( android / ios).</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get categories list</span>
                    <span class="uri">/api-ios/user/categories</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/categories" type="hidden">
                    <span class="description">It is used to get categories list. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Sub-categories list</span>
                    <span class="uri">/api-ios/user/subcategories</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/subcategories" type="hidden">
                    <span class="description">It is used to get subcategories list. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Category ID</td>
                                <td class="para">category_id</td>
                                <td class="parameter">
                                    <input name="category_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Category ID.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Listings</span>
                    <span class="uri">/api-ios/user/course</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/course" type="hidden">
                    <span class="description">It is used to list courses. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Category ID</td>
                                <td class="para">category_id</td>
                                <td class="parameter">
                                    <input name="category_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Category ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Price</td>
                                <td class="para">price</td>
                                <td class="parameter">
                                    <input name="price" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>price => 0-0,1-49,50-99,100-199,200-299,400-499.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Instruction Level</td>
                                <td class="para">instruction_level</td>
                                <td class="parameter">
                                    <input name="instruction_level" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>instruction_level => all,beginner,intermediate,advanced.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Sort By</td>
                                <td class="para">sort_by</td>
                                <td class="parameter">
                                    <input name="sort_by" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>sort_by => featured,freecourses,toprated,topfree,toppaid,mostviewed.</p></td>
                            </tr>  
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Featured/Latest/Most viewed course</span>
                    <span class="uri">/api-ios/user/courselist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/courselist" type="hidden">
                    <span class="description">It is used to get featured / Latest/ Mostviewd course list. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Course Details</span>
                    <span class="uri">/api-ios/user/coursedetails</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/coursedetails" type="hidden">
                    <span class="description">It is used to get user course details. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Curriculum</span>
                    <span class="uri">/api-ios/user/coursecurriculum</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/coursecurriculum" type="hidden">
                    <span class="description">It is used to get  course curriculum. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                              <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                            
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <!--- Course review-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Reviews</span>
                    <span class="uri">/api-ios/user/coursereview</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/coursereview" type="hidden">
                    <span class="description">It is used to get  course review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course view discussion-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course View Discussion</span>
                    <span class="uri">/api-ios/user/viewdiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/viewdiscussion" type="hidden">
                    <span class="description">It is used to get  course disucssion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- About Instructor-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">About Instructor</span>
                    <span class="uri">/api-ios/user/instructor</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/instructor" type="hidden">
                    <span class="description">It is used to get  about instructor. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Discussion</span>
                    <span class="uri">/api-ios/user/adddiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/adddiscussion" type="hidden">
                    <span class="description">It is used to Add Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion Text</td>
                                <td class="para">desc_title</td>
                                <td class="parameter">
                                    <input name="desc_title" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion Text .</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion Content</td>
                                <td class="para">desc_cnt</td>
                                <td class="parameter">
                                    <input name="desc_cnt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion content .</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID .</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Edit Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Edit Discussion</span>
                    <span class="uri">/api-ios/user/updatediscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/updatediscussion" type="hidden">
                    <span class="description">It is used to Edit Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion Text</td>
                                <td class="para">desc_title</td>
                                <td class="parameter">
                                    <input name="desc_title" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion Text .</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion Content</td>
                                <td class="para">desc_cnt</td>
                                <td class="parameter">
                                    <input name="desc_cnt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion content .</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID .</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID .</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Delete Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Discussion</span>
                    <span class="uri">/api-ios/user/discussiondelete</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/discussiondelete" type="hidden">
                    <span class="description">It is used to Delete Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID .</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Wishlist -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add to Wishlist</span>
                    <span class="uri">/api-ios/user/wishlist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/wishlist" type="hidden">
                    <span class="description">It is used to Add to Wishlist. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Status.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course Wishlist -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Wishlist</span>
                    <span class="uri">/api-ios/user/coursewishlist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/coursewishlist" type="hidden">
                    <span class="description">It is used to Course Wishlist. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Reply</span>
                    <span class="uri">/api-ios/user/addreply</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/addreply" type="hidden">
                    <span class="description">It is used to Add Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr>
                         <!--    <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID.</p></td>
                            </tr> -->
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Reply Comment</td>
                                <td class="para">reply_cmt</td>
                                <td class="parameter">
                                    <input name="reply_cmt" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply Comment.</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Edit Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Edit Reply</span>
                    <span class="uri">/api-ios/user/updatereply</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/updatereply" type="hidden">
                    <span class="description">It is used to Update Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Reply ID</td>
                                <td class="para">reply_id</td>
                                <td class="parameter">
                                    <input name="reply_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Reply Comment</td>
                                <td class="para">reply_cmt</td>
                                <td class="parameter">
                                    <input name="reply_cmt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply Comment.</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Delete Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Reply</span>
                    <span class="uri">/api-ios/user/replydestroy</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/replydestroy" type="hidden">
                    <span class="description">It is used to Delete Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Reply ID</td>
                                <td class="para">reply_id</td>
                                <td class="parameter">
                                    <input name="reply_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply ID.</p></td>
                            </tr>
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- View All Replies -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">View All Replies</span>
                    <span class="uri">/api-ios/user/viewreplies</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/viewreplies" type="hidden">
                    <span class="description">It is used to View All Replies. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- My course list -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">My Course list</span>
                    <span class="uri">/api-ios/user/mycourselist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/mycourselist" type="hidden">
                    <span class="description">It is used to View My Course list ( Learning ). </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Type</td>
                                <td class="para">type</td>
                                <td class="parameter">
                                    <input name="type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Type. ( All / Completed )</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- View Announcements -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Announcements</span>
                    <span class="uri">/api-ios/user/announcement</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/announcement" type="hidden">
                    <span class="description">It is used to Add Announcements. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Password reset -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Forgot password</span>
                    <span class="uri">/api-ios/user/forgetrequest</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/forgetrequest" type="hidden">
                    <span class="description">It is used to Forgot password. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Email ID</td>
                                <td class="para">email_id</td>
                                <td class="parameter">
                                    <input name="email_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email Id.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course curriculum for subscribed user -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">User Course curriculum </span>
                    <span class="uri">/api-ios/user/usercourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/usercourse" type="hidden">
                    <span class="description">It is used to Course curriculum for subscribed course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>   
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Login Via Facebook -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Social Network Login</span>
                    <span class="uri">/api-ios/user/signin</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/signin" type="hidden">
                    <span class="description">It is used to Social Network Login. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Email ID</td>
                                <td class="para">email_id</td>
                                <td class="parameter">
                                    <input name="email_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type. ( Facebook / Twitter / Google +)</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Type</td>
                                <td class="para">device_type</td>
                                <td class="parameter">
                                    <input name="device_type" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Type (ios/android).</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!--- Get Profile Information -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get profile information</span>
                    <span class="uri">/api-ios/user/profileinfo</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/profileinfo" type="hidden">
                    <span class="description">It is used to get Profile Information. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Update Profile Image -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Update Profile Image</span>
                    <span class="uri">/api-ios/user/updateprofile</span>
                </div>
                <form class="hidden" name="multipart" id="multipart" style="display: block;" enctype="multipart/form-data">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/updateprofile" type="hidden">
                    <span class="description">It is used to Update Profile Image. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Profile Image</td>
                                <td class="para">cimage</td>
                                <td class="parameter">
                                    <input type="file" name="cimage" id="cimage" />
                                    <!-- <input name="cimage" placeholder="required"> -->
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Profile Image.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- update profile information-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Update Profile Information</span>
                    <span class="uri">/api-ios/user/updateinfo</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/updateinfo" type="hidden">
                    <span class="description">It is used to Update Profile Information. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">First Name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Last Name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User Name</td>
                                <td class="para">user_name</td>
                                <td class="parameter">
                                    <input name="user_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Announcements</td>
                                <td class="para">announcement</td>
                                <td class="parameter">
                                    <input name="announcement" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcements.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Special Promotion</td>
                                <td class="para">spl_promotion</td>
                                <td class="parameter">
                                    <input name="spl_promotion" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Special Promotion.</p></td>
                            </tr>
                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Change Password -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Change Password</span>
                    <span class="uri">/api-ios/user/changepassword</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/changepassword" type="hidden">
                    <span class="description">It is used to Change Password. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">current_password</td>
                                <td class="parameter">
                                    <input name="current_password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Current Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">New Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Re type Password</td>
                                <td class="para">password_confirmation</td>
                                <td class="parameter">
                                    <input name="password_confirmation" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Re type Password.</p></td>
                            </tr>
                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Delete user Account -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Account</span>
                    <span class="uri">/api-ios/user/accountdestory</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/accountdestory" type="hidden">
                    <span class="description">It is used to Delete Account. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Lecture Details Status -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Lecture Details Status</span>
                    <span class="uri">/api-ios/user/lecturedetails</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/lecturedetails" type="hidden">
                    <span class="description">It is used to get Lecture Details Status. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Status. =>  0 --- incomplete - 1-- completed </p></td>
                            </tr>

                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Search -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Search Course</span>
                    <span class="uri">/api-ios/user/searchcourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/searchcourse" type="hidden">
                    <span class="description">It is used to get Search Course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Search Text</td>
                                <td class="para">course_search</td>
                                <td class="parameter">
                                    <input name="course_search" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search Text.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Search Discussion-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Search Discussion</span>
                    <span class="uri">/api-ios/user/searchdiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/searchdiscussion" type="hidden">
                    <span class="description">It is used to get Search Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Search Text</td>
                                <td class="para">dis_search</td>
                                <td class="parameter">
                                    <input name="dis_search" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search Text.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Rating and review-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Rating and review</span>
                    <span class="uri">/api-ios/user/addrating</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/addrating" type="hidden">
                    <span class="description">It is used to Add Rating and review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Review Text</td>
                                <td class="para">review_text</td>
                                <td class="parameter">
                                    <input name="review_text" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Review Text.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Rating Value</td>
                                <td class="para">rating</td>
                                <td class="parameter">
                                    <input name="rating" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Rating Value.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- get Rating and review-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">get Rating and review</span>
                    <span class="uri">/api-ios/user/rating</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/rating" type="hidden">
                    <span class="description">It is used to get Rating and review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Report Absue-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Report abuse</span>
                    <span class="uri">/api-ios/user/reportabuse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/reportabuse" type="hidden">
                    <span class="description">It is used to Report Abuse. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Issue Type</td>
                                <td class="para">issue_type</td>
                                <td class="parameter">
                                    <input name="issue_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Issue Type.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Details</td>
                                <td class="para">details</td>
                                <td class="parameter">
                                    <input name="details" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Details.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Report Absue-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Students enrolled</span>
                    <span class="uri">/api-ios/user/enrolled</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/enrolled" type="hidden">
                    <span class="description">It is used to Students enrolled. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Facebook Signup -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Social Network Signup </span>
                    <span class="uri">/api-ios/user/signup</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/signup" type="hidden">
                    <span class="description">It is used to Social Network Signup. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">First Name</td>
                                <td class="para">firstname</td>
                                <td class="parameter">
                                    <input name="firstname" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First Name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last Name</td>
                                <td class="para">lastname</td>
                                <td class="parameter">
                                    <input name="lastname" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last Name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User Name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr>    
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social APP ID</td>
                                <td class="para">social_id</td>
                                <td class="parameter">
                                    <input name="social_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social APP ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type.( Facebook / Twitter / Google + )</p></td>
                            </tr>  
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Quiz questions -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Quiz questions</span>
                    <span class="uri">/api-ios/user/quizquestions</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/quizquestions" type="hidden">
                    <span class="description">It is used to Quiz questions. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture Id.</p></td>
                            </tr>                             
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Quiz questions Result -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Quiz Result</span>
                    <span class="uri">/api-ios/user/quizresult</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/quizresult" type="hidden">
                    <span class="description">It is used to Quiz Result. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>  
                             <tr class="required">
                                <td class="name">Question ID</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question ID.(comma separated value)</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Answer ID</td>
                                <td class="para">answer_id</td>
                                <td class="parameter">
                                    <input name="answer_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question ID.(comma separated value)</p></td>
                            </tr>                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Change Email -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Change Email</span>
                    <span class="uri">/api-ios/user/changeemail</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/changeemail" type="hidden">
                    <span class="description">It is used to Change Email. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!--- Delete Review -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Review</span>
                    <span class="uri">/api-ios/user/delreview</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/delreview" type="hidden">
                    <span class="description">It is used to Delete Review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Trending course-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Trending,new and noteworthy,staff picks,digital marketing..</span>
                    <span class="uri">/api-ios/user/trending</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/trending" type="hidden">
                    <span class="description">It is used to trending,new and noteworthy,staff picks,digital marketing... </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <!--- Trending course-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Trending,new and noteworthy,staff picks,digital marketing -- View All</span>
                    <span class="uri">/api-ios/user/viewall</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/viewall" type="hidden">
                    <span class="description">It is used to trending,new and noteworthy,staff picks,digital marketing -- View All</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">type</td>
                                <td class="para">type</td>
                                <td class="parameter">
                                    <input name="type" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Type ( trending / staff_picks / digital_marketing / note_worthy ).</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Buy Course -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name"> Buy Course ( Free Course ) </span>
                    <span class="uri">/api-ios/user/buycourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/buycourse" type="hidden">
                    <span class="description">Buy Course ( Free Course )</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID .</p></td>
                            </tr> 
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Check Twitter user -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name"> Check User Email </span>
                    <span class="uri">/api-ios/user/usermail</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/usermail" type="hidden">
                    <span class="description">Check User Email</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Social ID</td>
                                <td class="para">id</td>
                                <td class="parameter">
                                    <input name="id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text </td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type.(facebook / Twitter / google+) </p></td>
                            </tr>  
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Currency code -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name"> Get Currency code </span>
                    <span class="uri">/api-ios/user/currency</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/currency" type="hidden">
                    <span class="description">Get Currency code</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>    
             <!---   Featured-unfeatured course -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Featured-unfeatured Course </span>
                    <span class="uri">/api-ios/user/features</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/features" type="hidden">
                    <span class="description">Featured-unfeatured Course</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Status</p></td>
                            </tr>  
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!---   Not subscriber course -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Details for Guest </span>
                    <span class="uri">/api-ios/user/notsubscriber</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/notsubscriber" type="hidden">
                    <span class="description">Course Details for Guest</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---   Recently view course  -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Set Recently view course </span>
                    <span class="uri">/api-ios/user/recently</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/recently" type="hidden">
                    <span class="description">Recently view Course</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---   Save Lecture notes  -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Save Lecture notes </span>
                    <span class="uri">/api-ios/user/notes</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/notes" type="hidden">
                    <span class="description">Save Lecture notes</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>                           
                            <tr class="required">
                                <td class="name">Lecture Id</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Notes</td>
                                <td class="para">notes</td>
                                <td class="parameter">
                                    <input name="notes" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Notes.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---  Downloads Lecture notes  -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Download Lecture notes </span>
                    <span class="uri">/api-ios/user/downloadnotes</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/downloadnotes" type="hidden">
                    <span class="description">Download Lecture notes</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Lecture Id</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---  Downloads Lecture notes  -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Notes </span>
                    <span class="uri">/api-ios/user/removenotes</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removenotes" type="hidden">
                    <span class="description">Delete Notes</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Note Id</td>
                                <td class="para">note_id</td>
                                <td class="parameter">
                                    <input name="note_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Note Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---  Add Announcements -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Announcements </span>
                    <span class="uri">/api-ios/user/announcements</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/announcements" type="hidden">
                    <span class="description">Add Announcements</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Announcement</td>
                                <td class="para">announcement</td>
                                <td class="parameter">
                                    <input name="announcement" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcement.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---  Update Announcements -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Update Announcements </span>
                    <span class="uri">/api-ios/user/updateannouncement</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/updateannouncement" type="hidden">
                    <span class="description">Update Announcements</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Announcement Id</td>
                                <td class="para">announcement_id</td>
                                <td class="parameter">
                                    <input name="announcement_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcement Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Announcement</td>
                                <td class="para">announcement</td>
                                <td class="parameter">
                                    <input name="announcement" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcement.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!---  Delete Announcements -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Announcements </span>
                    <span class="uri">/api-ios/user/removeannouncement</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removeannouncement" type="hidden">
                    <span class="description">Delete Announcements</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Announcement Id</td>
                                <td class="para">announcement_id</td>
                                <td class="parameter">
                                    <input name="announcement_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcement Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!---  Subscriber course -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Details for Guest </span>
                    <span class="uri">/api-ios/user/subscriber</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/subscriber" type="hidden">
                    <span class="description">Course Details for Guest</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                              <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!--- Course lecture for subscribed user -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">User Course Lecture </span>
                    <span class="uri">/api-ios/user/viewlecture</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/viewlecture" type="hidden">
                    <span class="description">It is used to get Course Lecture for subscribed course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>  
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- View discussion by lecture -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">View Course discussion by lecture</span>
                    <span class="uri">/api-ios/user/lecturediscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/lecturediscussion" type="hidden">
                    <span class="description">It is used to get course disucssion by lecture. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!-- course certificate -->
             <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course certificate</span>
                    <span class="uri">/api-ios/user/coursecertificate</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/coursecertificate" type="hidden">
                    <span class="description">It is used to get user course certificate. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr>                             
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- Unread Lectures -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Unread Lectures</span>
                    <span class="uri">/api-ios/user/unreadlecture</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/unreadlecture" type="hidden">
                    <span class="description">It is used to get unread lectures of the course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr>                             
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- Coupon -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Coupon Value</span>
                    <span class="uri">/api-ios/user/getcoupon</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/getcoupon" type="hidden">
                    <span class="description">It is used to get coupon value for coupon_id. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr>  -->
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Coupon Code</td>
                                <td class="para">coupon_code</td>
                                <td class="parameter">
                                    <input name="coupon_code" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>coupon_code.</p></td>
                            </tr>                             
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- Coupon -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Save Payemnt status</span>
                    <span class="uri">/api-ios/user/savepayment</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/savepayment" type="hidden">
                    <span class="description">It is used to save payment details. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Payment Method</td>
                                <td class="para">payment_method</td>
                                <td class="parameter">
                                    <input name="payment_method" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>payment_method(paypal_standard,paypal_express_checkout,stripe)</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Amount</td>
                                <td class="para">amount</td>
                                <td class="parameter">
                                    <input name="amount" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>amount</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>status(completed,pending)</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Transaction ID</td>
                                <td class="para">tr_details</td>
                                <td class="parameter">
                                    <input name="tr_details" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>tr_details</p></td>
                            </tr>                             
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- Purchase restore -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Purchase restore</span>
                    <span class="uri">/api-ios/user/purchaserestore</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/purchaserestore" type="hidden">
                    <span class="description">It is used to restore the already purchased courses </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Product identifier</td>
                                <td class="para">product_id</td>
                                <td class="parameter">
                                    <input name="product_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>product_id(comma separated)</p></td>
                            </tr>
                          
                            <tr class="required">
                                <td class="name">Transaction ID</td>
                                <td class="para">tr_details</td>
                                <td class="parameter">
                                    <input name="tr_details" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>tr_details(comma separated)</p></td>
                            </tr>  

                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                                                        
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- Question lists -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Questions</span>
                    <span class="uri">/api-ios/user/questions</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/questions" type="hidden">
                    <span class="description">It is used to get the questions </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>user id</p></td>
                            </tr>
                          
                            <tr class="required">
                                <td class="name">Search</td>
                                <td class="para">search_text</td>
                                <td class="parameter">
                                    <input name="search_text" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search text</p></td>
                            </tr>  

                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>page_no.</p></td>
                            </tr> 
                                                        
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <!-- View Question -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Questions By Id</span>
                    <span class="uri">/api-ios/user/questionview</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/questionview" type="hidden">
                    <span class="description">It is used to get the questions by Id</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Question Id</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>question_id</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Type</td>
                                <td class="para">type</td>
                                <td class="parameter">
                                    <input name="type" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Type (1-get details, 2-increment view count)</p></td>
                            </tr>
                          
                                                        
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <!-- Get Answers lists -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Answers List</span>
                    <span class="uri">/api-ios/user/answerslist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/answerslist" type="hidden">
                    <span class="description">It is used to get the answers using question id</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Question Id</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>question_id</p></td>
                            </tr>

                            <tr class="required">
                                <td class="name">Search</td>
                                <td class="para">search_text</td>
                                <td class="parameter">
                                    <input name="search_text" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search text</p></td>
                            </tr>  

                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>page_no.</p></td>
                            </tr> 
                          
                                                        
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!-- Add/Edit Questions -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add/Edit Questions</span>
                    <span class="uri">/api-ios/user/upsertquestion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/upsertquestion" type="hidden">
                    <span class="description">It is used to add/edit the questions</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>user_id</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Question Id</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>question_id</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Question Title</td>
                                <td class="para">question_title</td>
                                <td class="parameter">
                                    <input name="question_title" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>question_title</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Category Id</td>
                                <td class="para">category_id</td>
                                <td class="parameter">
                                    <input name="category_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>category</p></td>
                            </tr>
                            <tr class="optional">
                                <td class="name">SubCategory Id</td>
                                <td class="para">sub_cat_id</td>
                                <td class="parameter">
                                    <input name="sub_cat_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Subcategory</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Question Description</td>
                                <td class="para">description</td>
                                <td class="parameter">
                                    <input name="description" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question Description</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">File id</td>
                                <td class="para">file_ids</td>
                                <td class="parameter">
                                    <input name="file_ids" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>File ids with comma separaters</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>  

             <!-- Add/Edit Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add/Edit Reply</span>
                    <span class="uri">/api-ios/user/upsertreply</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/upsertreply" type="hidden">
                    <span class="description">It is used to add/edit the questions</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>user_id</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Answer Id</td>
                                <td class="para">answer_id</td>
                                <td class="parameter">
                                    <input name="answer_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>answer_id</p></td>
                            </tr>
                            
                            <tr class="required">
                                <td class="name">Question Id</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>question_id</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Reply Text</td>
                                <td class="para">reply_text</td>
                                <td class="parameter">
                                    <input name="reply_text" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>reply_text</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>  

             <!--- Upload File -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Upload Question File</span>
                    <span class="uri">/api-ios/user/uploadqtnfile</span>
                </div>
                <form class="hidden" name="qtn_multipart" id="qtn_multipart" style="display: block;" enctype="multipart/form-data">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/uploadqtnfile" type="hidden">
                    <span class="description">It is used to upload the question file. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Numeric</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">File</td>
                                <td class="para">qfiles</td>
                                <td class="parameter">
                                    <input type="file" name="qfiles" id="cimage" />
                                </td>
                                <td class="type">File</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question Files</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Upload File -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Question File</span>
                    <span class="uri">/api-ios/user/removeqtnfile</span>
                </div>
                <form class="hidden">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removeqtnfile" type="hidden">
                    <span class="description">It is used to upload the question file. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Numeric</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">File Id</td>
                                <td class="para">file_id</td>
                                <td class="parameter">
                                    <input type="file" name="file_id" placeholder="required" />
                                </td>
                                <td class="type">File</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>File id</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Delete Questions  -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Questions </span>
                    <span class="uri">/api-ios/user/removequestions</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removequestions" type="hidden">
                    <span class="description">Delete Questions</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Question Id</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Delete Answer  -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Answer </span>
                    <span class="uri">/api-ios/user/removeanswers</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removeanswers" type="hidden">
                    <span class="description">Delete Answers</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Answer Id</td>
                                <td class="para">answer_id</td>
                                <td class="parameter">
                                    <input name="answer_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Answer Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Membership list</span>
                    <span class="uri">/api-ios/user/membership</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/membership" type="hidden">
                    <span class="description">It is used to get membership. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>    
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

             <!--- List Business Packages  -->
            <!-- <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">List Business Packages</span>
                    <span class="uri">/api-ios/user/businesspackage</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/businesspackage" type="hidden">
                    <span class="description">It is used to list the business package</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

             <!--- List Assign Course  -->
            <!-- <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">List Assigned Courses</span>
                    <span class="uri">/api-ios/user/assignedcourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/assignedcourse" type="hidden">
                    <span class="description">It is used to get the assigned courses</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Business User Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Business User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

             <!--- List Assign Course  -->
            <!-- <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">List Business Courses</span>
                    <span class="uri">/api-ios/user/courselists</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/courselists" type="hidden">
                    <span class="description">It is used to get the business courses</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Business User Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Business User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

             <!-- <li class="method get">
                <div class="title">
                    <span class="http-method">Get</span>
                    <span class="name">List Business Users</span>
                    <span class="uri">/api-ios/user/listbusinessusers</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/listbusinessusers" type="hidden">
                    <span class="description">It is used to list business users</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Business Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

           <!--  <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Business Users</span>
                    <span class="uri">/api-ios/user/addbusinessusers</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/addbusinessusers" type="hidden">
                    <span class="description">It is used to add business users</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">First name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>email.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Confirm Password</td>
                                <td class="para">password_confirmation</td>
                                <td class="parameter">
                                    <input name="password_confirmation" placeholder="required">
                               </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>confirm Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description">(1.Active or 2.Inactive.)<p></p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Business Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Numeric</td>
                                <td class="location"><p>query</p></td>
                                <td class="description">businessid.<p></p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

          <!--   <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Edit Business Users</span>
                    <span class="uri">/api-ios/user/editbusinessusers</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/editbusinessusers" type="hidden">
                    <span class="description">It is used to edit business users</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">First name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>user_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User name.</p></td>
                            </tr>  -->
                            <!-- <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>email.</p></td>
                            </tr>  -->
                            <!-- <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Confirm Password</td>
                                <td class="para">password_confirmation</td>
                                <td class="parameter">
                                    <input name="password_confirmation" placeholder="required">
                               </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>confirm Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description">(1.Active or 2.Inactive.)<p></p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Business Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Numeric</td>
                                <td class="location"><p>query</p></td>
                                <td class="description">businessid.<p></p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

            <!--- List Assign Course  -->
            <!-- <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Assign Course</span>
                    <span class="uri">/api-ios/user/assigncourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/assigncourse" type="hidden">
                    <span class="description">It is used to assign course</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Business User Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Business User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> -->

            <!--- Delete business user  -->
            <!-- <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Business Users</span>
                    <span class="uri">/api-ios/user/removebusinessuser</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/removebusinessuser" type="hidden">
                    <span class="description">It is used to remove the business users</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Business User Id</td>
                                <td class="para">business_id</td>
                                <td class="parameter">
                                    <input name="business_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Business User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
 -->
            <!--- Get menu -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get Menu</span>
                    <span class="uri">/api-ios/user/menus</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/api-ios/user/menus" type="hidden">
                    <span class="description">It is used to get the menus</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User Id</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

        </ul>
	</li>
</ul>

<script src="{!! asset('assets/bsetec/api_ios/js/common.js') !!}" type="text/javascript"></script>
<script type="text/javascript">
  
$(document).ready(function() {
    
    $('form').on('submit', function() {
        var datatype = $(this).attr('id');
        //if($(this).attr('id')!='multipart'){
        var form = $(this);
    
        var methodtype = $(this).children('input[name=httpMethod]').val();
        
        var methoduri = $(this).children('input[name=methodUri]').val();
        $("#message").html("<span class='error'>API Request</span>");
        
        var siteurl = "{!! url('') !!}";
        
        var realpath = siteurl + methoduri;
        if (methodtype == 'DELETE') 
        {
            realpath = realpath + '?' + $(this).serialize();
        }
        if(datatype !='multipart' && datatype !='qtn_multipart'){
               $.ajax({
                type: methodtype, 
                url: realpath, // proper url to your "store-address.php" file
                data: $(this).serialize(),
                success: function(msg) 
                {
                    var msg1 = JSON.stringify(msg);
                    // $('#my_result' ).text(msg1);
                    form.find('.my_result').text(msg1);
                },
                error: function(msg)
                {
                    var msg1 = JSON.stringify(msg);
                    // $('#my_result' ).text(msg1);
                    form.find('.my_result').text(msg1);
                }
            }); 
        }
        
        return false;
    });

    ///multipart data submit
    $("#multipart").submit(function(e)
    {
        var form = $(this);
        var methodtype = $(this).children('input[name=httpMethod]').val();
        var methoduri = $(this).children('input[name=methodUri]').val();
        $("#message").html("<span class='error'>API Request</span>");
        
        var siteurl = "{!! url('') !!}";
        
        var realpath = siteurl + methoduri;
        if (methodtype == 'DELETE') 
        {
            realpath = realpath + '?' + $(this).serialize();
        }

        var formData = new FormData(this);
        $.ajax({
            url: realpath,
            type: methodtype,
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
        success: function(msg)
        {
           var msg1 = JSON.stringify(msg);
           
            //var msg1 = msg;
            form.find('.my_result').text(msg1);
        },
         error: function(msg) 
         {
            //alert(msg);
            var msg1 = JSON.stringify(msg);
            //var msg1 = msg;
            form.find('.my_result').text(msg1);   
         }          
        });
    }); 
    $("#multiform").submit();

    ///qtn_multipart data submit
    $("#qtn_multipart").submit(function(e)
    {
        var form = $(this);
        var methodtype = $(this).children('input[name=httpMethod]').val();
        var methoduri = $(this).children('input[name=methodUri]').val();
        $("#message").html("<span class='error'>API Request</span>");
        
        var siteurl = "{!! url('') !!}";
        
        var realpath = siteurl + methoduri;
        if (methodtype == 'DELETE') 
        {
            realpath = realpath + '?' + $(this).serialize();
        }

        var formData = new FormData(this);
        $.ajax({
            url: realpath,
            type: methodtype,
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
        success: function(msg)
        {
           var msg1 = JSON.stringify(msg);
           
            //var msg1 = msg;
            form.find('.my_result').text(msg1);
        },
         error: function(msg) 
         {
            //alert(msg);
            var msg1 = JSON.stringify(msg);
            //var msg1 = msg;
            form.find('.my_result').text(msg1);   
         }          
        });
    }); 
});
</script>
</body>
</html>

