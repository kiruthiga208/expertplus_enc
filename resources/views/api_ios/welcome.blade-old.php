<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="_token" content="{!! csrf_token() !!}"/>
<script src="{!! URL::asset('js/jquery.v1.8.2.js'); !!}" type="text/javascript"></script>
<link href="{!! URL::asset('css/api_docstyles.css'); !!}" rel="stylesheet" type="text/css" /> 
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
<title>EXPERT PLUS API</title>

<script type="text/javascript">
    $.ajaxSetup({
       headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>   
</head>   
<body>
<h1 id="title">EXPERT PLUS API v1.1 Resources</h1>
<div id="controls">
    <ul>
        <li><a id="toggle-endpoints" href="#">Toggle All Endpoints</a></li>
        <li><a id="toggle-methods" href="#">Toggle All Methods</a></li>
    </ul>
</div>
<ul>
    <li class="endpoint expanded">
        <h3 class="title"><span class="name">Exam methods</span>
            <ul class="actions">
                <li class="list-methods"><a href="#">List Methods</a></li>
                <li class="expand-methods"><a href="#">Expand Methods</a></li>
            </ul>
        </h3>
        <ul class="methods hidden" style="display: block;">		
			<li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Signup via email</span>
                    <span class="uri">/user/register</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/register" type="hidden">
                    <span class="description">It is used to register a user. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">First name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>                         
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Login via email</span>
                    <span class="uri">/user/login</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/login" type="hidden">
                    <span class="description">It is used to login a user. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Type</td>
                                <td class="para">device_type</td>
                                <td class="parameter">
                                    <input name="device_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Type ( android / ios).</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get categories list</span>
                    <span class="uri">/user/categories</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/categories" type="hidden">
                    <span class="description">It is used to get categories list. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Listings</span>
                    <span class="uri">/user/course</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/course" type="hidden">
                    <span class="description">It is used to list courses. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Category ID</td>
                                <td class="para">category_id</td>
                                <td class="parameter">
                                    <input name="category_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Category ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Price</td>
                                <td class="para">price</td>
                                <td class="parameter">
                                    <input name="price" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>price => 0-0,1-49,50-99,100-199,200-299,400-499.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Instruction Level</td>
                                <td class="para">instruction_level</td>
                                <td class="parameter">
                                    <input name="instruction_level" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>instruction_level => all,beginner,intermediate,advanced.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Sort By</td>
                                <td class="para">sort_by</td>
                                <td class="parameter">
                                    <input name="sort_by" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>sort_by => featured,freecourses,toprated,topfree,toppaid,mostviewed.</p></td>
                            </tr>  
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Featured/Latest/Most viewed course</span>
                    <span class="uri">/user/courselist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/courselist" type="hidden">
                    <span class="description">It is used to get featured / Latest/ Mostviewd course list. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Course Details</span>
                    <span class="uri">/user/coursedetails</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/coursedetails" type="hidden">
                    <span class="description">It is used to get user course details. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 

            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Course Curriculum</span>
                    <span class="uri">/user/coursecurriculum</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/coursecurriculum" type="hidden">
                    <span class="description">It is used to get  course curriculum. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <!--- Course review-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Course Reviews</span>
                    <span class="uri">/user/coursereview</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/coursereview" type="hidden">
                    <span class="description">It is used to get  course review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course view discussion-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Course View Discussion</span>
                    <span class="uri">/user/viewdiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/viewdiscussion" type="hidden">
                    <span class="description">It is used to get  course disucssion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- About Instructor-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">About Instructor</span>
                    <span class="uri">/user/instructor</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/instructor" type="hidden">
                    <span class="description">It is used to get  about instructor. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Discussion</span>
                    <span class="uri">/user/adddiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/adddiscussion" type="hidden">
                    <span class="description">It is used to Add Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion Text</td>
                                <td class="para">desc_title</td>
                                <td class="parameter">
                                    <input name="desc_title" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion Text .</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion Content</td>
                                <td class="para">desc_cnt</td>
                                <td class="parameter">
                                    <input name="desc_cnt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion content .</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID .</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">

                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Edit Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Edit Discussion</span>
                    <span class="uri">/user/updatediscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/updatediscussion" type="hidden">
                    <span class="description">It is used to Edit Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion Text</td>
                                <td class="para">desc_title</td>
                                <td class="parameter">
                                    <input name="desc_title" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion Text .</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion Content</td>
                                <td class="para">desc_cnt</td>
                                <td class="parameter">
                                    <input name="desc_cnt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion content .</p></td>
                            </tr> 
                             <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID .</p></td>
                            </tr> 
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Delete Discussion -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Discussion</span>
                    <span class="uri">/user/discussiondelete</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/discussiondelete" type="hidden">
                    <span class="description">It is used to Delete Course Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID .</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Wishlist -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add to Wishlist</span>
                    <span class="uri">/user/wishlist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/wishlist" type="hidden">
                    <span class="description">It is used to Add to Wishlist. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Status.</p></td>
                            </tr> 
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course Wishlist -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Course Wishlist</span>
                    <span class="uri">/user/coursewishlist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/coursewishlist" type="hidden">
                    <span class="description">It is used to Course Wishlist. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Reply</span>
                    <span class="uri">/user/addreply</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/addreply" type="hidden">
                    <span class="description">It is used to Add Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr>
                         <!--    <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID.</p></td>
                            </tr> -->
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Reply Comment</td>
                                <td class="para">reply_cmt</td>
                                <td class="parameter">
                                    <input name="reply_cmt" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply Comment.</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Edit Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Edit Reply</span>
                    <span class="uri">/user/updatereply</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/updatereply" type="hidden">
                    <span class="description">It is used to Update Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Reply ID</td>
                                <td class="para">reply_id</td>
                                <td class="parameter">
                                    <input name="reply_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Reply Comment</td>
                                <td class="para">reply_cmt</td>
                                <td class="parameter">
                                    <input name="reply_cmt" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply Comment.</p></td>
                            </tr>
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Delete Reply -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Reply</span>
                    <span class="uri">/user/replydestroy</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/replydestroy" type="hidden">
                    <span class="description">It is used to Delete Reply. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User_id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Reply ID</td>
                                <td class="para">reply_id</td>
                                <td class="parameter">
                                    <input name="reply_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Reply ID.</p></td>
                            </tr>
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- View All Replies -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">View All Replies</span>
                    <span class="uri">/user/viewreplies</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/viewreplies" type="hidden">
                    <span class="description">It is used to View All Replies. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Discussion ID</td>
                                <td class="para">discussion_id</td>
                                <td class="parameter">
                                    <input name="discussion_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Discussion ID.</p></td>
                            </tr>
                           
                        </tbody>

                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- My course list -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">My Course list</span>
                    <span class="uri">/user/mycourselist</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/mycourselist" type="hidden">
                    <span class="description">It is used to View My Course list ( Learning ). </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Type</td>
                                <td class="para">type</td>
                                <td class="parameter">
                                    <input name="type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Type. ( All / Completed )</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- View Announcements -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Announcements</span>
                    <span class="uri">/user/announcement</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/announcement" type="hidden">
                    <span class="description">It is used to Add Announcements. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Password reset -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Forgot password</span>
                    <span class="uri">/user/forgetrequest</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/forgetrequest" type="hidden">
                    <span class="description">It is used to Forgot password. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Email ID</td>
                                <td class="para">email_id</td>
                                <td class="parameter">
                                    <input name="email_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email Id.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Course curriculum for subscribed user -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">User Course curriculum </span>
                    <span class="uri">/user/usercourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/usercourse" type="hidden">
                    <span class="description">It is used to Course curriculum for subscribed course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>  
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Login Via Facebook -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Social Network Login</span>
                    <span class="uri">/user/signin</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/signin" type="hidden">
                    <span class="description">It is used to Social Network Login. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Email ID</td>
                                <td class="para">email_id</td>
                                <td class="parameter">
                                    <input name="email_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type. ( Facebook / Twitter / Google +)</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Device Type</td>
                                <td class="para">device_type</td>
                                <td class="parameter">
                                    <input name="device_type" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Type (ios/android).</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!--- Get Profile Information -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Get profile information</span>
                    <span class="uri">/user/profileinfo</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/profileinfo" type="hidden">
                    <span class="description">It is used to get Profile Information. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Update Profile Image -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Update Profile Image</span>
                    <span class="uri">/user/updateprofile</span>
                </div>
                <form class="hidden" name="multipart" id="multipart" style="display: block;" enctype="multipart/form-data">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/updateprofile" type="hidden">
                    <span class="description">It is used to Update Profile Image. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Profile Image</td>
                                <td class="para">cimage</td>
                                <td class="parameter">
                                    <input type="file" name="cimage" id="cimage" />
                                    <!-- <input name="cimage" placeholder="required"> -->
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Profile Image.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- update profile information-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Update Profile Information</span>
                    <span class="uri">/user/updateinfo</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/updateinfo" type="hidden">
                    <span class="description">It is used to Update Profile Information. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">First Name</td>
                                <td class="para">first_name</td>
                                <td class="parameter">
                                    <input name="first_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Last Name</td>
                                <td class="para">last_name</td>
                                <td class="parameter">
                                    <input name="last_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User Name</td>
                                <td class="para">user_name</td>
                                <td class="parameter">
                                    <input name="user_name" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Announcements</td>
                                <td class="para">announcement</td>
                                <td class="parameter">
                                    <input name="announcement" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Announcements.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Special Promotion</td>
                                <td class="para">spl_promotion</td>
                                <td class="parameter">
                                    <input name="spl_promotion" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Special Promotion.</p></td>
                            </tr>
                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Change Password -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Change Password</span>
                    <span class="uri">/user/changepassword</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/changepassword" type="hidden">
                    <span class="description">It is used to Change Password. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">current_password</td>
                                <td class="parameter">
                                    <input name="current_password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Current Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">New Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Re type Password</td>
                                <td class="para">password_confirmation</td>
                                <td class="parameter">
                                    <input name="password_confirmation" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Re type Password.</p></td>
                            </tr>
                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Delete user Account -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Account</span>
                    <span class="uri">/user/accountdestory</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/accountdestory" type="hidden">
                    <span class="description">It is used to Delete Account. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Lecture Details Status -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Lecture Details Status</span>
                    <span class="uri">/user/lecturedetails</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/lecturedetails" type="hidden">
                    <span class="description">It is used to get Lecture Details Status. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Status</td>
                                <td class="para">status</td>
                                <td class="parameter">
                                    <input name="status" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Status. =>  0 --- incomplete - 1-- completed </p></td>
                            </tr>

                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Search -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Search Course</span>
                    <span class="uri">/user/searchcourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/searchcourse" type="hidden">
                    <span class="description">It is used to get Search Course. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Search Text</td>
                                <td class="para">course_search</td>
                                <td class="parameter">
                                    <input name="course_search" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search Text.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Search Discussion-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Search Discussion</span>
                    <span class="uri">/user/searchdiscussion</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/searchdiscussion" type="hidden">
                    <span class="description">It is used to get Search Discussion. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Search Text</td>
                                <td class="para">dis_search</td>
                                <td class="parameter">
                                    <input name="dis_search" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Search Text.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Add Rating and review-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Add Rating and review</span>
                    <span class="uri">/user/addrating</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/addrating" type="hidden">
                    <span class="description">It is used to Add Rating and review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Review Text</td>
                                <td class="para">review_text</td>
                                <td class="parameter">
                                    <input name="review_text" placeholder="optional">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Review Text.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Rating Value</td>
                                <td class="para">rating</td>
                                <td class="parameter">
                                    <input name="rating" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Rating Value.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- get Rating and review-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">get Rating and review</span>
                    <span class="uri">/user/rating</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/rating" type="hidden">
                    <span class="description">It is used to get Rating and review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Report Absue-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Report abuse</span>
                    <span class="uri">/user/reportabuse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/reportabuse" type="hidden">
                    <span class="description">It is used to Report Abuse. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Issue Type</td>
                                <td class="para">issue_type</td>
                                <td class="parameter">
                                    <input name="issue_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Issue Type.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Details</td>
                                <td class="para">details</td>
                                <td class="parameter">
                                    <input name="details" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Details.</p></td>
                            </tr>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Report Absue-->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Students enrolled</span>
                    <span class="uri">/user/enrolled</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/enrolled" type="hidden">
                    <span class="description">It is used to Students enrolled. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Facebook Signup -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Social Network Signup </span>
                    <span class="uri">/user/signup</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/signup" type="hidden">
                    <span class="description">It is used to Social Network Signup. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">First Name</td>
                                <td class="para">firstname</td>
                                <td class="parameter">
                                    <input name="firstname" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>First Name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Last Name</td>
                                <td class="para">lastname</td>
                                <td class="parameter">
                                    <input name="lastname" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Last Name.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User Name</td>
                                <td class="para">username</td>
                                <td class="parameter">
                                    <input name="username" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Name.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr>    
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social APP ID</td>
                                <td class="para">social_id</td>
                                <td class="parameter">
                                    <input name="social_id" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social APP ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type.( Facebook / Twitter / Google + )</p></td>
                            </tr>  
                            <tr class="required">
                                <td class="name">Device Id</td>
                                <td class="para">device_id</td>
                                <td class="parameter">
                                    <input name="device_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Device Id.</p></td>
                            </tr> 
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Quiz questions -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Quiz questions</span>
                    <span class="uri">/user/quizquestions</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/quizquestions" type="hidden">
                    <span class="description">It is used to Quiz questions. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>
                             <tr class="required">
                                <td class="name">Lecture ID</td>
                                <td class="para">lecture_id</td>
                                <td class="parameter">
                                    <input name="lecture_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Lecture Id.</p></td>
                            </tr>                             
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Quiz questions Result -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Quiz Result</span>
                    <span class="uri">/user/quizresult</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/quizresult" type="hidden">
                    <span class="description">It is used to Quiz Result. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr>  
                             <tr class="required">
                                <td class="name">Question ID</td>
                                <td class="para">question_id</td>
                                <td class="parameter">
                                    <input name="question_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question ID.(comma separated value)</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Answer ID</td>
                                <td class="para">answer_id</td>
                                <td class="parameter">
                                    <input name="answer_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Question ID.(comma separated value)</p></td>
                            </tr>                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

             <!--- Change Email -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Change Email</span>
                    <span class="uri">/user/changeemail</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/changeemail" type="hidden">
                    <span class="description">It is used to Change Email. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Email</td>
                                <td class="para">email</td>
                                <td class="parameter">
                                    <input name="email" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Email.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Password</td>
                                <td class="para">password</td>
                                <td class="parameter">
                                    <input name="password" placeholder="required">
                                </td>
                                <td class="type">Text</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Password.</p></td>
                            </tr>                           
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
             <!--- Delete Review -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name">Delete Review</span>
                    <span class="uri">/user/delreview</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/delreview" type="hidden">
                    <span class="description">It is used to Delete Review. </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course Id</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course Id.</p></td>
                            </tr>                            
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!--- Trending course-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Trending,new and noteworthy,staff picks,digital marketing..</span>
                    <span class="uri">/user/trending</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/trending" type="hidden">
                    <span class="description">It is used to trending,new and noteworthy,staff picks,digital marketing... </span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li> 
            <!--- Trending course-->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name">Trending,new and noteworthy,staff picks,digital marketing -- View All</span>
                    <span class="uri">/user/viewall</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/viewall" type="hidden">
                    <span class="description">It is used to trending,new and noteworthy,staff picks,digital marketing -- View All</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">type</td>
                                <td class="para">type</td>
                                <td class="parameter">
                                    <input name="type" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Type ( trending / staff_picks / digital_marketing / note_worthy ).</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Page Number</td>
                                <td class="para">page_no</td>
                                <td class="parameter">
                                    <input name="page_no" placeholder="optional">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Page Number.</p></td>
                            </tr>
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Buy Course -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name"> Buy Course ( Free Course ) </span>
                    <span class="uri">/user/buycourse</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/buycourse" type="hidden">
                    <span class="description">Buy Course ( Free Course )</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr class="required">
                                <td class="name">User ID</td>
                                <td class="para">user_id</td>
                                <td class="parameter">
                                    <input name="user_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>User Id.</p></td>
                            </tr> 
                            <tr class="required">
                                <td class="name">Course ID</td>
                                <td class="para">course_id</td>
                                <td class="parameter">
                                    <input name="course_id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Course ID .</p></td>
                            </tr> 
                                                      
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Check Twitter user -->
            <li class="method post">
                <div class="title">
                    <span class="http-method">POST</span>
                    <span class="name"> Check User Email </span>
                    <span class="uri">/user/usermail</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="POST" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/usermail" type="hidden">
                    <span class="description">Check User Email</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="required">
                                <td class="name">Social ID</td>
                                <td class="para">id</td>
                                <td class="parameter">
                                    <input name="id" placeholder="required">
                                </td>
                                <td class="type">Number</td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social ID.</p></td>
                            </tr>
                            <tr class="required">
                                <td class="name">Social Type</td>
                                <td class="para">social_type</td>
                                <td class="parameter">
                                    <input name="social_type" placeholder="required">
                                </td>
                                <td class="type">Text </td>
                                <td class="location"><p>query</p></td>
                                <td class="description"><p>Social Type.(facebook / Twitter / google+) </p></td>
                            </tr>  
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

            <!--- Currency code -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name"> Get Currency code </span>
                    <span class="uri">/user/currency</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/currency" type="hidden">
                    <span class="description">Get Currency code</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Parameter</th>
                                <th>Value</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>
            <!---   Featured-unfeatured course -->
            <li class="method get">
                <div class="title">
                    <span class="http-method">GET</span>
                    <span class="name"> Featured-unfeatured Course</span>
                    <span class="uri">/user/features</span>
                </div>
                <form class="hidden" style="display: block;">
                    <input name="httpMethod" value="GET" type="hidden">
                    <input name="oauth" value="" type="hidden">
                    <input name="methodUri" value="/user/features" type="hidden">
                    <span class="description">Featured-unfeatured Course</span>
                    <br><br>
                    <div id="param1"></div>
                    <table class="parameters">
                        <thead>
                            <tr>
                                <th>User Id</th>
                                <th>Course Id</th>
                                <th>Status</th>                               
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <input value="Try it!" type="submit">
                    <pre>      
                        <div class="my_result"></div> 
                    </pre>
                </form>
            </li>

        </ul>
	</li>
</ul>

<script src="{!! URL::asset('js/common.js'); !!}" type="text/javascript"></script>
<script type="text/javascript">
  
$(document).ready(function() {
    
    $('form').on('submit', function() {
        var datatype = $(this).attr('id');
        //if($(this).attr('id')!='multipart'){
        var form = $(this);
    
        var methodtype = $(this).children('input[name=httpMethod]').val();
        
        var methoduri = $(this).children('input[name=methodUri]').val();
        $("#message").html("<span class='error'>API Request</span>");
        
        var siteurl = "{!! url() !!}";
        
        var realpath = siteurl + methoduri;
        if (methodtype == 'DELETE') 
        {
            realpath = realpath + '?' + $(this).serialize();
        }
        if(datatype !='multipart'){
               $.ajax({
                type: methodtype, 
                url: realpath, // proper url to your "store-address.php" file
                data: $(this).serialize(),
                success: function(msg) 
                {
                    var msg1 = JSON.stringify(msg);
                    // $('#my_result' ).text(msg1);
                    form.find('.my_result').text(msg1);
                },
                error: function(msg)
                {
                    var msg1 = JSON.stringify(msg);
                    // $('#my_result' ).text(msg1);
                    form.find('.my_result').text(msg1);
                }
            }); 
        }
        
        return false;
    });

    ///multipart data submit
    $("#multipart").submit(function(e)
    {
        var form = $(this);
        var methodtype = $(this).children('input[name=httpMethod]').val();
        var methoduri = $(this).children('input[name=methodUri]').val();
        $("#message").html("<span class='error'>API Request</span>");
        
        var siteurl = "{!! url() !!}";
        
        var realpath = siteurl + methoduri;
        if (methodtype == 'DELETE') 
        {
            realpath = realpath + '?' + $(this).serialize();
        }

        var formData = new FormData(this);
        $.ajax({
            url: realpath,
            type: methodtype,
            data:  formData,
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
        success: function(msg)
        {
           var msg1 = JSON.stringify(msg);
           
            //var msg1 = msg;
            form.find('.my_result').text(msg1);
        },
         error: function(msg) 
         {
            //alert(msg);
            var msg1 = JSON.stringify(msg);
            //var msg1 = msg;
            form.find('.my_result').text(msg1);   
         }          
        });
    }); 
    $("#multiform").submit();
});
</script>
</body>
</html>

