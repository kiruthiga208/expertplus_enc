<head>
<title>Mobile Payment</title>
</head>
<style>
input[type=number] {
    -moz-appearance:textfield;
}
</style>
 <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
<link href="{{ asset('assets/bsetec/themes/theme1/css/theme1.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/styles.css') }}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,700,700italic,600italic,400italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.cookie.js') }}"></script>            
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/select2/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/switch.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/bsetec.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.form.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.jCombo.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/bootstrap.summernote/summernote.min.js') }}"></script>
        <script src="{{ asset('assets/bsetec/js/jquery.validate.min.js') }}"></script>
        <script src="{!! URL::asset('assets/bsetec/js/jquery.form.js'); !!}" type="text/javascript"></script>
    
<div class="confirm-section">
    @if($already_taken!=1)
    
                <div class="container">                
                  <h2 class="confirm-title">{!! Lang::get('core.confirm_purchase') !!}</h2>    
                   <div class="table-payment">  
                    <div id="couponError"></div>       
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr>
                <!-- <th width="10%">Sl No:</th> -->
                <th width="60%">{!! Lang::get('core.name_content') !!}</th>
                <th width="40%" align="right" style="text-align:right;">{!! Lang::get('core.price')!!}</th>
                </tr>
                <tr>
               @if (defined('CNF_CURRENCY'))
                        @php $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
                @endif
                <!-- <td>1</td> -->
                <td>{{$course->course_title}}</td>
                <td align="right"> 
                    @if($mobamount)
                        {{$currency .' '. $mobamount}}
                    @else
                        {{$currency .' '. $course->pricing}}
                    @endif
                </td>
                </tr>
                @if(!$mobamount)
                    @if($admin_discount)
                    <tr>
                    <td>{!! Lang::get('core.admin_discount') !!}</td>
                    <td align="right">{{ $currency.' '.$discount_amount }}
                    </td>
                    </tr>
                    @endif  
                @endif  
                <tr id="coupon_discount_div" style="display:none;">
                <!-- <td>3</td> -->
                <td>{!! Lang::get('core.coupon_discount') !!}</td>
                <td>{!! $currency !!} <span id="coupon_discount"></span></td>
                </tr>
                @if(!$mobamount)
                    <tr>
                    <td colspan="2">
                                   @if(\bsetecHelpers::checkcoupon($course->course_id))
                    <h4 class="redeem">{!! Lang::get('core.redeem') !!}</h4>
                    <div class="hide_block clearfix" id="coupon_code_div">
                      <form id="myForm" action="{{ \URL::to('course/couponcheck') }}" method="post"> 
                      <input type="text" id="couponCode"  class="" autocomplete="off" placeholder="{!! Lang::get('core.enter_coupon')!!}">
                    
                     <input type="submit" class="btn btn-success" id="checkCoupon" value="{!! Lang::get('core.Apply')!!}" /> 
                    </form>
                        <!-- <input type="text" id="couponCode" onClick="$('#checkCoupon').removeAttr('disabled');" class="" autocomplete="off" placeholder="{!! Lang::get('core.enter_coupon')!!}"> -->
                        
                            <!-- <button class="btn btn-success" type="button" id="checkCoupon">{!! Lang::get('core.Apply')!!}</button> -->
             
                    </div>
                    @endif
                    </td>
                    </tr>
                @endif
                <tr>
                <td style="text-align:left;">{!! Lang::get('core.you_pay') !!}</td>
                <td align="right">{!! $currency !!} <span id="you_pay">
                @if($mobamount)
                    {{$mobamount}}
                @else
                    {{$you_pay}}
                @endif
                </span><span id="coupon_price"></span></td>
                </tr>
                </table>     
                </div> 
                </div>     
                <div class="payment-block clearfix">
                @if($errors->any())
                    <div class="alert alert-danger">
                       <a class="close" data-dismiss="alert">×</a>
                       {!! Lang::get('core.payment_error') !!}
                    </div>
                @endif
                <div class="container mob-block">
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                @if(!$mobamount)
                    @php $active='')
                    @if($stripe['Status'] == 'true')
                    <li  class="active"><a href="#stripe" data-toggle="tab">{!! Lang::get('core.strip_payment') !!}</a></li>
                    @endif
                    @if($paypal_standard['Status'] == 'true')
                    <li><a href="#paypal" data-toggle="tab">{!! Lang::get('core.paypal_standard') !!}</a></li>
                    @endif

                    @if($paypal_express_checkout['Status'] == 'true')
                    <li><a href="#paypal_ex" data-toggle="tab">{!! Lang::get('core.paypal_express')!!}</a></li>
                    @endif
                @else
                    @php $active='active')
                    @if($paypal_standard['Status'] == 'true')
                    <li  class="active"><a href="#paypal" data-toggle="tab">{!! Lang::get('core.paypal_standard') !!}</a></li>
                    @endif

                    @if($paypal_express_checkout['Status'] == 'true')
                    <li><a href="#paypal_ex" data-toggle="tab">{!! Lang::get('core.paypal_express')!!}</a></li>
                    @endif
                @endif

                

                
                

                </ul></div>
                
                <div class="container">
                
                <div id="my-tab-content" class="tab-content">
                        <div style="display:none" class="loader">
                           <center> <img src=" {!! URL::to('uploads/images/loader.gif') !!} " /></center>
                        </div>
                @if($paypal_express_checkout['Status'] == 'true')
                <div id="paypal_ex" class="tab-pane">
                {!! Form::open(array('url' => url('payment/mobform'))) !!}
                <input type="hidden" name="coupon_price" class="coupon_price_mob" value="" />
                <input type="hidden" name="user_id" value="{{$user_id}}" />
                <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                <input type="hidden" name="course_amount" value="{{$mobamount}}" />
                <input type="hidden" name="payment_method" value="paypal_express_checkout" /> 
                
                <div class="form-group paypal_standard clearfix">
                <h3 class="cmn_title">paypal express</h3>
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color payment_loader">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif

                @if($paypal_standard['Status'] == 'true')
                <div id="paypal" class="tab-pane {{$active}}"  >
                {!! Form::open(array('url' => url('payment/mobform'),'parsley-validate'=>'','novalidate'=>' ' )) !!}
                <input type="hidden" name="coupon_price" class="coupon_price_mob" value="" />
                <input type="hidden" name="user_id" value="{{$user_id}}" />
                <input type="hidden" name="course_id" id="course_id" value="{{$course->course_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                <input type="hidden" name="course_amount" value="{{$mobamount}}" />
                <input type="hidden" name="payment_method" value="paypal_standard" /> 
                
                <div class="form-group paypal_standard clearfix">
                <h3 class="cmn_title">paypal standard</h3>
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color payment_loader">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif
                @if(!$mobamount)
                    @if($stripe['Status'] == 'true')
                    <div id="stripe" class="tab-pane col-md-6 active">
                    {!! Form::open(array('url' => url('payment/stripemob'),'id'=>'payment-stripe' )) !!}
                    <span class="payment-errors"></span>
                    <input type="hidden" name="coupon_price" class="coupon_price_mob" value="" />
                    <input name='stripeToken' id="stripeToken" type="hidden" />
                    <input type="hidden" name="user_id" value="{{$user_id}}" />
                    <input type="hidden" name="course_id" value="{{$course->course_id}}" /> 
                    <input type="hidden" name="course_title" value="{{$course->slug}}" /> 
                    <input type="hidden" name="payment_method" value="stripe" /> 
                    <h3 class="cmn_title">stripe payment</h3>
                    <div class="form-group clearfix">
                    <label for="Card Number" class="control-label col-xs-5 col-sm-4 text-left"> {!! Lang::get('core.card_number')!!} <span class="asterix"> * </span></label>
                    <div class="col-xs-7 col-sm-8">
                    {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>Lang::get('core.enter_card'),'autocomplete'=>'off')) !!} 
                    </div>
                    </div>
                    
                    <div class="form-group clearfix">
                    <label for="card-cvc" class="control-label col-xs-5 col-sm-4 text-left"> {!! Lang::get('core.card_cvc') !!} <span class="asterix"> * </span></label>
                    <div class="col-xs-7 col-sm-3">
                    {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'3','id'=>'card-cvc','required'=>true,'placeholder'=>Lang::get('core.enter_cvc'),'autocomplete'=>'off')) !!} 
                    </div>
                    </div>
                    <div class="form-group clearfix">
                    <label for="expire" class="col-xs-5 col-sm-4">{!! Lang::get('core.Expiration') !!} <span class="asterisk"> * </span></label>
                    <div class="col-xs-7 expiry-wrapper clearfix">
                    <div class="col-xs-6 col-sm-4">
                    <div class="select-style_block">
                    <select class="stripe-sensitive required" id="card-expiry-month">
                    <option value="" selected="selected">{!! Lang::get('core.mm') !!}</option>
                    </select>
                    </div>
                    <script type="text/javascript">
                    var select = $("#card-expiry-month"),
                    month = new Date().getMonth() + 1;
                    for (var i = 1; i <= 12; i++) {
                    select.append($("<option value='"+i+"'>"+i+"</option>"))
                    }
                    </script>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                    <div class="select-style_block">
                    <select class="stripe-sensitive required" id="card-expiry-year">
                    <option value="" selected="selected">{!! Lang::get('core.year') !!}</option>
                    </select>
                    </div>
                    <script type="text/javascript">
                    var select = $("#card-expiry-year"),
                    year = new Date().getFullYear();
                    for (var i = 0; i < 12; i++) {
                    select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
                    }
                    </script>
                    </div>
                    </div>
                    </div>
                    <div class="form-group clearfix">
                    
                    <div class="submit_block"><label style="visibility:hidden;"  class="col-xs-5 col-sm-4 ">{!! Lang::get('core.sb_submit') !!}</label></div>
                    <div class="col-xs-7 col-sm-8 sbmt-btn">
                    <input type="submit" id="submit_button" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color payment_loader">
                    <!-- <input type="button" value="Buy with Credit balance $ {{ \bsetecHelpers::getUserCredit()}}" id="BuyWithCredit" class="btn btn-primary btn_pay"> -->
                    </div>
                    </div> 
                    
                    {!! Form::close() !!}
                    </div>
                    @endif
                @endif

                @if($stripe['Status'] != 'true' && $paypal_standard['Status'] != 'true' && $paypal_express_checkout['Status'] != 'true')
                <h2>{!! Lang::get('core.no_payment')!!}</h2>
                @endif
                <p class="stripe-desc" align="justify">{!! Lang::get('core.paypal_terms') !!}</p>
                </div>

                </div>
                </div>
@else
<h1><center>Already taken this course</center></h1>
@endif
</div>

@if($stripe['Status'] == 'true')
<!-- payment plugins -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript">
// payment options script start
Stripe.setPublishableKey("{{ $stripe['publishable_key'] }}");
var payment_error = $('.payment-errors');
$(document).ready(function() {
    $('.coupon_price_mob').val("");
    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
                $('.loader').css('display','block');
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
            },
        },
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


// COUPON PROCESS
 $('#myForm').submit(function() { 
  var couponCode = $('#couponCode');
  var couponError = $('#couponError');
  var couponprice = $('#coupon_price');
  var coupon_discount = $('#coupon_discount');
  var coupon_div =  $('#coupon_discount_div');
  var you_pay = $('#you_pay');
  var coupon_code_div =  $('#coupon_code_div');
  $(this).attr('disabled','disabled');
  courseID = $('#course_id').val();
  couponError.html("");
  if(couponCode.val().length > 4){
    $.ajaxSetup({
          headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      url: '{{ \URL::to("course/couponcheck") }}',
      type: 'POST',
      dataType:'json',
      data:{code:couponCode.val(),course_id:courseID},
      }).done(function(res){
           if(res.success_message){
              couponError.html('<div class="alert alert-success">'+res.success_message+'</div>');
              coupon_div.show();
              coupon_discount.text(res.discount_amount);
              $('.coupon_price_mob').val(res.amount)
              you_pay.hide();
              couponprice.text(res.amount);
              coupon_code_div.slideToggle();
              couponCode.val("");
            }
        }).error(function(data) {
            couponprice.text("");
            you_pay.show();
            coupon_div.hide();
            var errors = data.responseText;
            res = $.parseJSON(errors);
            couponError.html('<div class="alert alert-danger">'+res.errors+'</div>');
            $(this).removeAttr('disabled');
            couponCode.val("");
       });

  }else{
    couponError.html('<div class="alert alert-danger">{!! Lang::get("core.coupon_code_error") !!}</div>');
  }
  setTimeout(function(){
    $('.alert').fadeOut();
  },3000);
  $("#ajaxform").submit();
  return false;
});

</script>
<style>
.payment-errors{
    color:red;
}

</style>
<script type="text/javascript">
jQuery(window).load(function () {
    $('.loader').css('display','none');
});
</script>

@endif