@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		</div>
	</div>
	<div class="sbox-content"> 	
	
	 {!! Form::open(array('url'=>'usergroup/add', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <input type="hidden" name="group_id" value="{{ $group_id }}" />
	 <div class="table-responsive" style="min-height:300px;">
   	 <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" name="remove[]" /></th>
				<th>First Name</th>
				<th>Email</th>
			  </tr>
        </thead>

        <tbody>
				{{---*/ $i=0;/*---}}	
            @foreach ($list as $row)
            {{---*/ $name=$row->email /*---}}

                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{$row->id}}" />  </td>									
					<td>{{$row->first_name}}
					<input type="hidden" name="user_id[]" value="{{$row->first_name}}"/>
					</td>
					<td>{{$row->email}}
					<input type="hidden" name="email_id[]" value="{{$row->email}}"/>
					</td>
					
					{{---*/ $return=''/*---}}
					
                </tr>
				
            @endforeach
              
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
	<button type="button" onclick="location.href='{{ URL::to('usergroup') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
	</div>

	{!! Form::close() !!}
	@if(($search=='instructor')AND($search1=='country')AND($search2='name'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name,'group_name'=>$group_id,'usertype'=>'instructor'])->render()) !!}  @endif
	@elseif(($search=='student')AND($search1=='country')AND($search2='name'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name,'group_name'=>$group_id,'usertype'=>'student'])->render()) !!}  @endif
	@elseif(($search=='instructor')AND($search2=='name'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name,'usertype'=>'instructor'])->render()) !!}  @endif
	@elseif(($search=='student')AND($search2=='name'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name,'usertype'=>'student'])->render()) !!}  @endif
	@elseif(($search1=='country')AND($search2=='name'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name,'group_id'=>$group_id])->render()) !!}  @endif
	@elseif(($search=='instructor')AND($search1=='country'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['group_id' => $group_id,'usertype'=>'instructor'])->render()) !!}  @endif
	@elseif(($search=='student')AND($search1=='country'))
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['group_id' => $group_id,'usertype'=>'student'])->render()) !!}  @endif
	@elseif($search=='name')
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['user_id' => $name])->render()) !!}  @endif
	@elseif($search=='instructor')
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['usertype' => 'instructor'])->render()) !!}  @endif
	@elseif($search='student')
		@if(count($list)>0){!! str_replace('/?', '?', $list->appends(['usertype' => 'student'])->render()) !!}  @endif
	@endif
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("userforum/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
@stop