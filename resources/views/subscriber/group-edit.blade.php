@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		</div>
	</div>
	<div class="sbox-content"> 	
	
	 {!! Form::open(array('url'=>'subscriber/add', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <input type="hidden" name="group_id" value="{{ $group_id }}" />
	 <div class="table-responsive" style="min-height:300px;">
   	 <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> No </th>
				<th> <input type="checkbox" class="checkall" name="remove[]"  /></th>
				<th>First Name</th>
				<th>Email</th>
			  </tr>
        </thead>
        <tbody>
				{{---*/ $i=0;/*---}}	
            @foreach ($users as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{$row->user_id}}" checked />  </td>									
					<td>{{$row->first_name}}
					<input type="hidden" name="user_id[]" value="{{$row->first_name}}"/>
					</td>
					<td>{{$row->email}}
					<input type="hidden" name="email_id[]" value="{{$row->email}}"/>
					</td>
					
					{{---*/ $return=''/*---}}
					
                </tr>
            @endforeach
        </tbody>
      
    </table>
	<input type="hidden" name="md" value="" />
	<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ 'Apply Changes' }}</button>
	<button type="button" onclick="location.href='{{ URL::to('subscriber') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
	</div>

	{!! Form::close() !!}
	@if(count($users)>0){!! str_replace('/?', '?', $users->appends(['id' => $group_id])->render()) !!}  @endif
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("userforum/multisearch")}}');
		$('#bsetecTable').submit();
	});
	
});	
</script>		
@stop