@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>
<p><h4>Your Course Details</h4></p>
<p>
	Title : {!! $coursename !!} <br />
	Comments : {!! $feedback !!}
</p>
<p>click <a href="{!! $link !!}"> here </a> to view your course.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop