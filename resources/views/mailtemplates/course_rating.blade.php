@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $authorname !!} , </h2>
<p>Rating details of your Course<b> {!! $coursename !!}  </b></p>
<p>Rated By :<b> {!! $studentname !!} </b>
</p>
<p>
	Title : {!! $title !!} <br>
	Click <a href="{!! $link !!}"> here </a> to view it.
</p>
<p>
	Rating : {!! $rating !!} Star 
</p>
<p>
	Description : {!! $description !!} 
</p>

<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop