@extends('layouts.mailtemplate')
@section('content')
		<h2>Hello {!! $username !!} , </h2>
		<p>
			Title : {!! $title !!}<br />
		</p>
		<p>
			Coupon Code : {!! $couponcode !!}<br />
		</p>
		<p>
			Amount : {!! $amount !!}<br />
		</p>
		<p>
			Start Date : {!! $startdate !!}<br />
		</p>
		<p>
			End Date : {!! $enddate !!}<br />
		</p>

		{!! $link !!}
	
		<br /><br /><p> Thank You</p>
		
		{!! CNF_APPNAME !!} 
@stop