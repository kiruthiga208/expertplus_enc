@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>
<h3>Done!! Password has been Reset... </h3>
<p>Now onwards you can use the new password given below:</p>
<p>
	Password : {!! $password !!}
</p>
<p> Please Click <a href="{{ URL::to('course') }}"> here </a>to continue.</p>
<br /><br /><p> Thank You </p>

{!! CNF_APPNAME !!} 
@stop