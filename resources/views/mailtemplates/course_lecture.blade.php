@extends('layouts.mailtemplate')
@section('content')
<h2>Hello {!! $username !!} , </h2>
<p>Your courses are updated with an new lecture</p>
<p>Course details are given bellow</p>
<p>
	Author Name : {!! $authorname !!}
</p>
<p>
	Course Name : {!! $title !!}
</p>
<p>
	New lectures: Click <a href="{!! $link !!}">here</a> to view it.
</p>
<br /><br /><p> Thank You</p>

{!! CNF_APPNAME !!} 
@stop