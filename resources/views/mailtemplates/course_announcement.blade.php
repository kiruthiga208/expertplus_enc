@extends('layouts.mailtemplate')
@section('content')

<h2>Hello {!! $username !!} , </h2>
<p>Hi Guy's!! We have an new Announcement!!</p>
<p>
	Author Name : {!! $authorname !!}
</p>
<p>
	Course Name : {!! $title !!} <br>
	Click <a href="{!! $link !!}"> here </a> to view it.
</p>
<p>
	Announcement : {!! $announcement !!}
</p>
<p>Hope it will help you in someway!!</p>
<br /><br /><p> Thank You</p>

{!! CNF_APPNAME !!} 
@stop


