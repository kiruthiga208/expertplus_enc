@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('banner?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'banner/save', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 
<div class="col-md-12">
			<fieldset><legend> {!! Lang::get('core.banner') !!}</legend>
							<div id="image_error"></div>
							<div class="form-group  " >
							<!-- <label for="Banner Id" class=" control-label col-md-4 text-left"> Banner Id </label> -->
							<div class="col-md-6">
							{!! Form::hidden('banner_id', $row['banner_id'],array('class'=>'form-control', 'placeholder'=>'','id'=>'banner_id'   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Banner Title" class=" control-label col-md-4 text-left"> {!! Lang::get('core.banner_title') !!} <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('banner_title', $row['banner_title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Banner Image" class=" control-label col-md-4 text-left"> {!! Lang::get('core.banner_image') !!} <span class="asterix"> * </span></label>
							<div class="col-md-6">
							<input  type='file' name='banner_image' id='banner_image' @if($row['banner_image'] =='') class='required' @endif />
							<div style="margin-top:15px;">
							<!-- {!! SiteHelpers::showUploadedFile($row['banner_image'],'Banner Image') !!} -->
							@if($row['banner_image'] !='')
							<img class="img-circle" id="banner-circle" border="2" width="100" src="{{ url('uploads/banner/'.$row['banner_image'].'') }}">
							<span class="" id="removeImage" style="margin-top:35px;color:red;cursor:pointer; margin-left:10px;">{!! Lang::get('core.x_remove') !!}</span>
							@else

							@endif
							</div>					
			 				
							</div> 
							<!--<div class="col-md-2">
							<a href="#" data-toggle="tooltip" placement="left" class="tips" title="Banner Image"><i class="icon-question2"></i></a>
							</div>-->
							
							</div> 					
							<div class="form-group  " >
							<label for="Banner Status" class=" control-label col-md-4 text-left"> {!! Lang::get('core.banner_status') !!} </label>
							<div class="col-md-6">
							{!! Form::checkbox('banner_status',1, $row['banner_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<!-- <label for="Banner Order" class=" control-label col-md-4 text-left"> Banner Order </label> -->
							<div class="col-md-6">
							{!! Form::hidden('banner_order', $row['banner_order'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
											
												
							 </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('banner?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
<script type="text/javascript">
var _URL = window.URL || window.webkitURL;
var banner_image = $('.img-circle');
var image_error = $('#image_error');
var remove_btn = $('#removeImage');
$("#banner_image").change(function(e) {
	var file, img,height;
	image_error.html("");
	banner_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger">{!! Lang::get("core.banner_invalid") !!}</div>';
		   image_error.html(html_error);
		   banner_image.removeAttr('src');
		   remove_btn.hide();
		   $(this).val("");
		   return false;
		}else{
			banner_image.attr('src',img.src);
			remove_btn.show();
			img.onload = function(){
				if(this.width < 800 || this.height < 400){
					html_error ='<div class="alert alert-danger">{!! Lang::get("core.selected_image") !!} '+this.width+'px * '+this.height+'px. {!! Lang::get("core.exact_banner_image") !!}</div>';
				   	image_error.html(html_error);
				   	$("#banner_image").val("");
				   	banner_image.removeAttr('src');
				   	remove_btn.hide();
				   	return false;
				}
			}
			
		}
    }
});

remove_btn.on('click',function(){
	var banner_id = $('#banner_id').val();
	if(banner_id != ""){
		if(confirm('{!! Lang::get("core.delete_image") !!}'))
		{
			$.ajax({
				url:'{!! url('').'/banner/delete'!!}', 
		        data:{banner_id:banner_id},
		        type:"DELETE",
		        success:function(res){
		        	$("#banner_image").val("");
		        	banner_image.removeAttr('src');
		        	remove_btn.hide();
		        },
		        fail:function(){
		            alert("error");
		        }
			});
		}
	}else{
		$("#banner_image").val("");
		banner_image.removeAttr('src');
		remove_btn.hide();
	}
});
</script>		 
@stop
