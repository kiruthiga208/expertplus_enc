@extends('layouts.app')

@section('content')
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/jquery.tinymce.js')!!}
{!! Html::script('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')!!}
{!! Html::style('resources/views/blog/blog.css')!!}


  <div class="page-content row blogadmin-add">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('blogadmin') }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	</div>  
	
	 <div class="page-content-wrapper">
	
		@if(Session::has('message'))	  
			   {!! Session::get('message') !!}
		@endif	

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{!! $error !!}</li>
			@endforeach
		</ul>
		<div id="image_error"></div>
		 {!! Form::open(array('url'=>'blogadmin/save/'.$row['blogID'], 'files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
	
	<div class="col-md-8 form-vertical">
		  <div class="form-group  " >
			<label for="Title" class=" control-label text-left">  {!! Lang::get('core.Title') !!}<span class="red">*</span></label>
			
			  {!! Form::text('title', $row['title'],array('class'=>'form-control', 'placeholder'=>'' )) !!} 
			
		  </div> 					
					
		  <div class="form-group"  id="blog_content" style="display:none">
			<label for="Content" class=" control-label text-left"> {!! Lang::get('core.Content') !!} <span class="red">*</span></label>
			{!! Form::textarea('content', $row['content'],array('class'=>'mceEditor form-control', 'style'=>'width:100%;' )) !!} 
			<span class="error_content" style="color:red"></span>
		</div> 	
	
	</div>
	
	<div class="col-md-4 form-horizontal">

			  <div class="form-group hidethis " style="display:none;">
				<label for="BlogID" class=" control-label col-md-4 text-left"> {!! Lang::get('core.BlogID') !!} </label>
				<div class="col-md-8">
				  {!! Form::text('blogID', $row['blogID'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 </div> 
			  </div> 					
			  <div class="form-group  " >

				<label for="CatID" class=" control-label col-md-4 text-left">  {!! Lang::get('core.CatID') !!}<span class="red">*</span></label>
				<div class="col-md-8">
				  
				 {!! Form::select('CatID',$categorys,$row['CatID'],array('class'=>'select2','id'=>'CatID','placeholder'=>Lang::get('core.please_select'))) !!}

				 </div> 
			  </div> 	

			  <div class="form-group  " >
				<label for="Blog Image" class=" control-label col-md-4 text-left">  {!! Lang::get('core.Blog_Image') !!}</label>

				<div class="col-md-8">
					<img id="blog_image_src" src="{{ (!empty($row['blog_image'])) ? \URL::to('uploads/blog/'.$row['blog_image']) : \URL::to('uploads/blog/blog-800.gif') }}" border="2" width="200" height="100" />
				  <input type="file" name="blogImage" id="blogImage">
				 </div> 
			  </div>									
			  <div class="form-group  " >
				<label for="Tags" class=" control-label col-md-4 text-left">  {!! Lang::get('core.Tags') !!} </label>
				<div class="col-md-8">
				  {!! Form::text('tags', $row['tags'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
				 </div> 
			  </div> 					
			  <div class="form-group  " >
				<label for="Status" class=" control-label col-md-4 text-left">  {!! Lang::get('core.Status') !!}</label>
				<div class="col-md-8">
					<label class='radio'>{!! Form::radio('status','draft',($row['status']=='draft') ? true:false,array()) !!} {!! Lang::get('core.Draft') !!}</label>
					<label class='radio'>{!! Form::radio('status','publish',($row['status']=='publish') ? true:false,array()) !!} {!! Lang::get('core.Publish') !!}</label>
					<label class='radio'>{!! Form::radio('status','unpublish',($row['status']=='unpublish') ? true:false,array()) !!} {!! Lang::get('core.Unpublish') !!}</label>
			    </div> 
			  </div> 	

			  <div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
				<button type="submit" class="btn btn-primary ">  {{ Lang::get('core.sb_save') }} </button>
				<button type="button" onclick="location.href='{{ URL::to('blogadmin') }}' " id="submit" class="btn btn-success ">  {{ Lang::get('core.sb_cancel') }} </button>
				</div>	  
		
			  </div> 			  
			  				

	</div>	 
		 
			
			<div style="clear:both"></div>	
				

		 
		 {!! Form::close() !!}
		 </div>

</div>				 
   <script type="text/javascript">

	$(window).load(function() {
	      $('#blog_content').show();
	});

   function getcontent(){
   		var con = tinyMCE.activeEditor.getContent({format : 'text'});
			var content =con.trim();
			if(content.length < 200){
				$('.error_content').html('{!! Lang::get("core.blog_admin_error") !!}')
				return false;
			}else{
				$('.error_content').html('')
				return true;
			}
   	}
	$(document).ready(function() { 
		
		// $("#CatID").jCombo("{{ URL::to('blogadmin/comboselect?filter=blogcategories:CatID:name') }}",
		// {  selected_value : '{{ $row["CatID"] }}' });

	
		$(function(){
			tinymce.init({	
				mode : "specific_textareas",
				editor_selector : "mceEditor",
				 plugins : "openmanager",
				 file_browser_callback: "openmanager",
				 open_manager_upload_path: '../../../../../../../../uploads/images/',
				 onchange_callback : "getcontent",
			 });	
		});
		 
	});


var _URL = window.URL || window.webkitURL;
var blog_image = $('#blog_image_src');
var image_error = $('#image_error');
var remove_btn = $('#removeImage');
$("#blogImage").change(function(e) {
	var file, img,height;
	image_error.html("");
	blog_image.removeAttr('src');
    if ((file = this.files[0])) {
        img = new Image();
        img.src = _URL.createObjectURL(file);
        ext = $(this).val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
		   html_error ='<div class="alert alert-danger"> {!! Lang:: get("core.invalid_type") !!}</div>';
		   image_error.html(html_error);
		   blog_image.removeAttr('src');
		   remove_btn.hide();
		   $(this).val("");
		   return false;
		}else{
			blog_image.attr('src',img.src);
			remove_btn.show();
			img.onload = function(){
				/*if(this.width != 800 || this.height != 450){
					html_error ='<div class="alert alert-danger">{!! Lang::get("core.selected_image") !!} '+this.width+'px * '+this.height+'px. {!! Lang::get("core.blog_error") !!}.</div>';
				   	image_error.html(html_error);
				   	$("#blogImage").val("");
				   	blog_image.removeAttr('src');
				   	remove_btn.hide();
				   	return false;
				}*/
			}
			
		}
    }
});
</script>		 
@stop
