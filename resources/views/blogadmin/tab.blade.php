<?php $active = Request::segment(1);?>
<ul class="nav nav-tabs" style="margin-bottom:10px;">
  <li <?php if($active =='blogadmin') echo  'class="active"';?>><a href="{{ URL::to('blogadmin')}}" class="tips btn btn-sm btn-white"> {{ Lang::get('core.Posts') }} </a></li>
  <li <?php if($active =='blogcategories') echo  'class="active"';?>><a href="{{ URL::to('blogcategories')}}" class="tips btn btn-sm btn-white"> {{ Lang::get('core.Categories') }} </a></li>
  <li <?php if($active =='blogcomment') echo  'class="active"';?>><a href="{{ URL::to('blogcomment')}}" class="tips btn btn-sm btn-white">{{ Lang::get('core.Comments') }}</a></li>
</ul>	