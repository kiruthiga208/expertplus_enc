@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> Course Subcategory <small>Course Subcategory</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('sitesubcategory?return='.$return) }}">Course Subcategory</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('sitesubcategory?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('sitesubcategory/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> Course Subcategory <small>Course Subcategory</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				
				
				<tr>
				<td width='30%' class='label-view text-right'>Category</td>
				<td>@php ( $catname = bsetecHelpers::sitecategories($row->cat_id) )
							{!! $catname->name !!} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Subcategory Name</td>
				<td>{{ $row->sub_name }} </td>

				</tr>
			
				
				<tr>
				<td width='30%' class='label-view text-right'>Status</td>
				<td>{{ $row->status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Created At</td>
				<td>{{ $row->created_at }} </td>

				</tr>

				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop