@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> Course Subcategory <small>Course Subcategory</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('sitesubcategory?return='.$return) }}">Course Subcategory</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> Course Subcategory <small>Course Subcategory</small></h4></div>
	<div class="sbox-content"> 


		<?php
			if($row['sub_cat_id']!=''){
				$pgurl = 'sitesubcategory/save/'.$row['sub_cat_id'].'?return='.$return;
			}else{
				$pgurl = 'sitesubcategory/save?return='.$return;
			}

			if($row['status']!='' && $row['status']=='enable'){
				$status = 'checked="checked"';
			}else{
				$status = '';
			}

			
		?>	

		 {!! Form::open(array('url'=>$pgurl, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Course Subcategory</legend>
								
												
							<div class="form-group  " >
							<label for="Cat Id" class=" control-label col-md-4 text-left"> Category </label>
							<div class="col-md-6">


							{!! Form::select('cat_id',$category,$row['cat_id'],array('class'=>'select2','id'=>'cat_id','placeholder'=>Lang::get('core.please_select'))) !!}

							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Sub Name" class=" control-label col-md-4 text-left"> Subcategory Name </label>
							<div class="col-md-6">
							{!! Form::text('sub_name', $row['sub_name'],array('class'=>'form-control', 'placeholder'=>'Enter Subcategory Name',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
											
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
							<div class="col-md-6">


								<input type="checkbox" name="status" id="status" value="enable" {{$status}}> {{ Lang::get('core.Enable') }}



							<!-- {!! Form::text('status', $row['status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!}  -->
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 	


							 </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('sitesubcategory?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop