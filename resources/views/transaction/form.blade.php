@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('transaction?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li class="alert alert-danger">{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'transaction/save', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
		 {!! Form::hidden('type', $row['purchase_type']) !!}
<div class="col-md-12">
			<fieldset><p><legend> {{ Lang::get('core.transaction') }} </legend></p>
								
							<div class="form-group  " >
							<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
							<div class="col-md-6">
							{!! Form::text('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'', 'readonly'=>'true'  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="User Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.User_Id') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							<select name='user_id' rows='5' id='user_id'
							class='select2 '  required  ></select> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 	

							@if($row['purchase_type']!='wallet')				
							<div class="form-group  " >
							<label for="Course Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_id') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							<select name='course_id' rows='5' id='course_id'
							class='select2 '  required  ></select>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 		
							@endif		

							<div class="form-group  " >
							<label for="Amount" class=" control-label col-md-4 text-left"> {{ Lang::get('core.amount') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							{!! Form::text('amount', $row['amount'],array('class'=>'form-control amount', 'placeholder'=>'', 'required'=>''  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Created At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Created_At') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							
			{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;', 'required'=>'')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<!-- <div class="form-group  " >
							<label for="Updated At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Updated At') }} </label>
							<div class="col-md-6">
							
			{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> --> 		
							@if($row['purchase_type']!='wallet')			
							<div class="form-group  " >
							<label for="Order Details" class=" control-label col-md-4 text-left"> {{ Lang::get('core.order_details') }} </label>
							<div class="col-md-6">
							<textarea name='order_details' rows='2' id='order_details' class='form-control '  
			  >{{ $row['order_details'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 		
							@endif
										
							<div class="form-group  " >
							<label for="Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Status') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							@php ( $statuss = array('completed', 'pending') )
							<select name="status" class="form-control" required>
							<option value="">{{ Lang::get('core.select')}}</option>
							@foreach($statuss as $status)
								<option value="{!! $status !!}" {!! $row['status']==$status ? 'selected' : '' !!}>
									{!! ucwords(str_replace('_', ' ', $status)) !!}
								</option>
							@endforeach	 
							</select>	
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Payment Method" class=" control-label col-md-4 text-left"> {{ Lang::get('core.payment_method') }} <span class="text-danger">*</span></label>
							<div class="col-md-6">
							@php (  $payment_methods = $method  )
							<select name="payment_method" class="form-control method" required>
							<option value="">{{ Lang::get('core.select')}}</option>
							@foreach($payment_methods as $payment_method)
								<option value="{!! $payment_method !!}" {!! $row['payment_method']==$payment_method ? 'selected' : '' !!}>
									{!! ucwords(str_replace('_', ' ', $payment_method)) !!}
								</option>
							@endforeach	
							</select>
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('transaction?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
  <script type="text/javascript">
	$(document).ready(function() { 
		
		$("#user_id").jCombo("{{ URL::to('core/users/comboselect?filter=users:id:first_name|last_name') }}",
		{  selected_value : '{!! $row["user_id"] !!}' });

		$("#course_id").jCombo("{{ URL::to('core/users/comboselect?filter=course:course_id:course_title') }}",
		{  selected_value : '{!! $row["course_id"] !!}' });
		 

		var method = $('.method option:selected').val();
		if(method=='free'){
			$('.amount').val('0');
			$('.amount').attr('readonly', true);
		}

	});

	$(document).on('change','.method',function(e){

		var method = $('.method option:selected').val();
		if(method=='free'){
			$('.amount').val('0');
			$('.amount').attr('readonly', true);
		}else{
			$('.amount').val('');
			$('.amount').attr('readonly', false);
		}
	});
	</script>	 
@stop