@extends('layouts.app')

@section('content')
@php usort($tableGrid, "SiteHelpers::_sort");
$memberships = array('' => Lang::get('core.select'),'0' => Lang::get('core.free'));
$memberships_list = array('0' => Lang::get('core.not_included_in_membership'));
if(isset($membership_plan) && count($membership_plan) > 0):
foreach ($membership_plan as $membership):
$memberships[$membership->plan_id] = $membership->plan_name;
$memberships_list[$membership->plan_id] = $membership->plan_name;
endforeach;
endif;
@endphp
@if (defined('CNF_CURRENCY'))
        @php ( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
<style>
.view_block {
  list-style: outside none none;
  margin: 0;
  padding: 0;
}
.view_block li.dropdown i, .view_block li.dropdown b.caret {
  color: #676a6c;
  font-size: 12px;
}
.view_block li.dropdown > a {
  border: 1px solid #eee;
  display: inline-block;
  margin: 2px 0;
  padding: 2px 6px;
}
.view_block li a {
  margin-bottom: 3px;
  width: 37px;
}
.table-responsive.course-listing-admin .view_block li.dropdown ul {
  top: -40px;
}

.view_block li.dropdown ul {
  left: auto;
  right: 0;
}
</style>
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.dashboard') }}</a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>

    </div>


	<div class="page-content-wrapper m-t">

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==0)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif
		</div>
	</div>
	<div class="sbox-content">

        <div class="toolbar-line ">
            <a href="{{ URL::to('membership/course') }}" class="tips btn btn-sm btn-default"  title="List All {{ ucfirst(Lang::get('core.courses')) }}"> List All {{ ucfirst(Lang::get('core.courses')) }}</a>
            @if($memberships_list)
            @foreach ($memberships_list as $mkey => $memb)
            <a href="{{ URL::to('membership/course?search=curriculum:'.$mkey) }}" class="tips btn btn-sm btn-default"  title="List {{ $memb }} @if($mkey){{ Lang::get('core.Membership') }}@endif {{ ucfirst(Lang::get('core.courses')) }}"> List {{ $memb }} @if($mkey){{ Lang::get('core.Membership') }}@endif {{ ucfirst(Lang::get('core.courses')) }}</a>
            @endforeach
            @endif

        </div>

	 {!! Form::open(array('url'=>'courselisting/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 {!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
	 <div class="table-responsive course-listing-admin" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }} </th>
				<th> </th>

				<!-- @foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach -->
				<th>{{ Lang::get('core.logo') }}</th>
				<th>{{ Lang::get('core.Title') }}</th>
				<th>{{ Lang::get('core.Created_By') }}</th>
                <th>{{ Lang::get('core.Membership') }}</th>
				<th>{{ Lang::get('core.pricing') }}</th>
				<th>{{ ucfirst(Lang::get('core.students')) }}</th>
				<th>{{ Lang::get('core.approved') }}</th>
				<th>{{ Lang::get('core.Created_At') }}</th>

				<th width="200" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /> </td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>
					@if($t['field'] == "user_id")
					<select name="user_id" class="form-control">
						<option value=""> {{ Lang::get('core.select_user') }} </option>
						@foreach ($user_lists as $search)
							<option value="{{ $search->userid }}">{{ $search->user_id }}</option>
						@endforeach
					</select>
                    @elseif($t['field'] == "image")

					@elseif($t['field'] =='curriculum')
                        {!! Form::select('curriculum', $memberships,'',array('class'=>'form-control')); !!}

					@elseif($t['field'] == "approved")
					{!! Form::select('approved', array('' => '--select--','0' => 'Waiting','1' => 'Approved','2'=>'Unapproved'),'',array('class'=>'form-control')); !!}
					@elseif($t['field'] == "pricing")

					@else
						{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
					@endif
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> {{ Lang::get('core.GO') }} </button></td>
			 </tr>

            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->course_id }}" data-cc="{{ $row->curriculum }}" />  </td>
				 @foreach ($tableGrid as $field)
				 @php ($fieldname=$field['field'])
					 @if($field['view'] =='1')
					 <td>
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
						@else
							@php ($conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							@if($field['field']=='image')
							 <img src="{{ \bsetecHelpers::getImage($row->image,'small') }}" width="50" border="0" class="img-thumbnail">

							@elseif($fieldname=='pricing')
							<?php if($row->$fieldname==NULL ||$row->$fieldname=='0'){ $row->$fieldname = '<span class="label label-success">'.Lang::get('core.free').'</span>'; } else{ $row->$field['field'] = $currency.' '.number_format($row->$fieldname,2); }?>
							{!! SiteHelpers::gridDisplay($row->$fieldname,$fieldname,$conn) !!}
							@elseif($fieldname=='description')
							<?php $students = \SiteHelpers::getstudentslist($row->course_id);
								  $row->$fieldname= $students; ?>
							{!! SiteHelpers::gridDisplay($row->$fieldname,$fieldname,$conn) !!}
							@elseif($fieldname=='approved')
							<?php if($row->$fieldname=='1'){ $row->$fieldname = '<span class="label label-success">'.Lang::get('core.approved').'</span>'; } else if($row->$fieldname=='2'){$row->$fieldname = '<span class="label label-danger">'.Lang::get('core.unapproved').'</span>';} else if($row->$fieldname=='0'){ $row->$fieldname = '<span class="label label-danger">'.Lang::get('core.waiting_for_approval').'</span>'; }?>
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
							@else
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
							@endif


						@endif
					 </td>
					 @endif
				 @endforeach
				 <td>
                 {!! Form::select('membership', $memberships_list,$row->plan_id,array('class'=>'form-control','id'=>'mem_'.$row->course_id)); !!}
                 <a  href="javascript:void(0)" data-id="{{ $row->course_id }}" class="tips btn btn-xs btn-primary m-t-sm m-b-md change_membership" title="Change Membership"><i class="fa fa-check "></i> Change Membership</a>
				</td>
                </tr>

            @endforeach

        </tbody>

    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('.change_membership').click(function(event){
        event.preventDefault();
        var r=confirm("Are you sure you want to change membership?");
        if (r==true)   {
            var _token = $('input[name="_token"]').val();
            var course_id = $(this).attr('data-id');
            var plan_id = $('#mem_'+course_id).val();
            console.log(plan_id);
            $.ajax({
                url:'{!! url('').'/membership/updatecoursemembership'!!}',
                data:{_token:_token,course_id:course_id,plan_ids:plan_id},
                type:"POST",
                success:function(res){
                    if(res){
                        window.location.reload();
                    } else {
                        alert("{{ Lang::get('core.comments_error') }}");
                    }
                },
                fail:function(){
                }
            });
        }
    });

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("membership/multisearchcourses")}}');
		$('#bsetecTable').submit();
	});

});
</script>
@stop