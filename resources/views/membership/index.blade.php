@extends('layouts.app')

@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>

    </div>


	<div class="page-content-wrapper m-t">

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==0)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif
		</div>
	</div>
	<div class="sbox-content">
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('membership/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif
			@if($access['is_remove'] ==1)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('membership/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif

		</div>



	<?php $plan_periods_opt = array( '1' => 'Monthly' ,  '12' => 'Yearly' , ); $plan_active = array( '1' => 'Active' ,  '0' => 'Inactive' , ); ?>
	 {!! Form::open(array('url'=>'membership/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped " id="table">
        <thead>
			<tr>
				<th class="number "> No </th>
				<th> <input type="checkbox" class="checkall" /></th>

				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody class="no-border-x no-border-y">
			<!-- <tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> </td>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<td>
						{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> GO</button></td>
			 </tr> -->

            @foreach ($rowData as $row)
                <tr>
					<td width="30" class="index "><strong>{{ ++$i }}</strong></td>
					<td width="50">@if($row->plan_id != 4)<input type="checkbox" class="ids" name="id[]" value="{{ $row->plan_id }}" />@endif  </td>
				 @foreach ($tableGrid as $field)
				 @php ($fieldname=$field['field'])
					 @if($field['view'] =='1')
					 <td>
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
						@else
							@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
							@if($field['field'] == 'status' || $field['field'] == 'status_y')
							<?php if($row->$fieldname=='1'){ $row->$fieldname = '<span class="label label-success">'.$plan_active[$row->$fieldname].'</span>'; } else if($row->$fieldname=='0'){ $row->$fieldname = '<span class="label label-danger">'.$plan_active[$row->$fieldname].'</span>'; }?>
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
							@else
							{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
							@endif
						@endif
					 </td>
					 @endif
				 @endforeach
				 <td>
					 	@if($access['is_detail'] ==1)
						<a href="{{ URL::to('membership/show/'.$row->plan_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif
						@if($access['is_edit'] ==1)
						<a  href="{{ URL::to('membership/update/'.$row->plan_id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif


				</td>
				<td>
				<input type="hidden" name="sortlist[<?php echo $row->plan_id;?>]" class="reorder" value="<?php echo $row->level;?>" />
				</td>
                </tr>

            @endforeach

        </tbody>

    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>
	</div>
</div>
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("membership/multisearch")}}');
		$('#bsetecTable').submit();
	});

	var fixHelperModified = function(e, tr) {
		var $originals = tr.children();
		var $helper = tr.clone();
		$helper.children().each(function(index) {
			$(this).width($originals.eq(index).width())
		});
		return $helper;
		},
		updateIndex = function(e, ui) {
			$('td.index', ui.item.parent()).each(function (i) {
				var sval = i + 1;
				$(this).html('<strong>'+sval+'</strong>');
			});
			$('.reorder', ui.item.parent()).each(function (i) {
				$(this).val(i + 1);
			});

			var form = $('#bsetecTable');

			$.ajax ({
				type: "POST",
				url: '{{ URL::to("membership/updatelevel")}}',
				data: form.serialize(),
				success: function (data)
				{
					//
				}
			});

		};

	$("#table tbody").sortable({
		helper: fixHelperModified,
		stop: updateIndex
	});



});
</script>
@stop