@extends('layouts.app')
@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort");
$memberships = array('' => Lang::get('core.select'));
if(isset($membership_plan) && count($membership_plan) > 0):
foreach ($membership_plan as $membership):
$memberships[$membership->plan_id] = $membership->plan_name;
endforeach;
endif;
/*--}}
  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> {{ Lang::get('core.dashboard') }} </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>

    </div>


	<div class="page-content-wrapper m-t">

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		@if(Session::get('gid') ==0)
			<a href="{{ URL::to('bsetec/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
		@endif
		</div>
	</div>
	<div class="sbox-content">
	    <div class="toolbar-line ">
			@if($access['is_add'] ==1 && 1 == 2)
	   		<a href="{{ URL::to('membership/users/update') }}" class="tips btn btn-sm btn-white"  title="{{ Lang::get('core.btn_create') }}">
			<i class="fa fa-plus-circle "></i>&nbsp;{{ Lang::get('core.btn_create') }}</a>
			@endif
			@if($access['is_remove'] ==1 && 1 == 2)
			<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
			<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
			@endif
			@if($access['is_excel'] ==1)
			<a href="{{ URL::to('membership/users/download') }}" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_download') }}">
			<i class="fa fa-download"></i>&nbsp;{{ Lang::get('core.btn_download') }} </a>
			@endif
			<a href="{{ URL::to('membership/users') }}" class="tips btn btn-sm btn-white"><i class="fa fa-eraser"></i>&nbsp;Clear </a>

		</div>



	 {!! Form::open(array('url'=>'membership/users/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
	 <div class="table-responsive" style="min-height:300px;">
    <table class="table table-striped ">
        <thead>
			<tr>
				<th class="number"> {{ Lang::get('core.no') }} </th>
				<th> </th>
				{{--*/ $hidegrid = array('id','purchase_type'); /*--}}
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1' && !in_array($t['field'],$hidegrid))
						@if($t['field'] == 'course_title')
							<th>{{ Lang::get('core.Membership') }}</th>
						@else
							<th>{{ $t['label'] }}</th>
						@endif
					@endif
				@endforeach
				<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			  </tr>
        </thead>

        <tbody>
			<tr id="bsetec-quick-search" >
				<td class="number"> # </td>
				<td> <input type="checkbox" class="checkall" /></td>
				@foreach ($tableGrid as $t)
				@if($t['view'] =='1' && !in_array($t['field'],$hidegrid))
					{{--*/ $drops = ''; /*--}}
					@if(isset($t['search_value']))
						{{--*/ $drops = $t['search_value'];  /*--}}
					@endif
					<td>
					@if($t['field'] =='username')
						{!! Form::text('username', $drops,array('class'=>'form-control', 'placeholder'=>'' )) !!}
					@elseif($t['field'] =='email')
						{!! Form::text('email', $drops,array('class'=>'form-control', 'placeholder'=>'' )) !!}
					@elseif($t['field'] =='course_title')
						{!! Form::select('course_id', $memberships,$drops,array('class'=>'form-control')); !!}
					@elseif($t['field'] =='status')
						{!! Form::select('status', array('' => Lang::get('core.select'),'completed' => 'Completed','pending' => Lang::get('core.pending'),'failed'=>Lang::get('core.failed'),'cancelled' => 'Cancelled','closed' => 'Closed'), $drops,array('class'=>'form-control')); !!}
					@elseif($t['field'] =='payment_method')
						{!! Form::select('payment_method', array('' => Lang::get('core.select'),'paypal_standard' => Lang::get('core.paypal_standard'),'paypal_express_checkout' => Lang::get('core.paypal_express_checkout'),'stripe'=>Lang::get('core.stripe'),'free'=>Lang::get('core.free')), $drops,array('class'=>'form-control')); !!}
					@elseif($t['field'] =='purchase_type')
						{!! Form::select('purchase_type', array('' => Lang::get('core.select'),'course' => Lang::get('core.course'),'membership' => Lang::get('core.Membership')),$drops,array('class'=>'form-control')); !!}
					@elseif($t['field'] =='purchase_period')
						{!! Form::select('purchase_period', array('' => Lang::get('core.select'),'once' => 'Once','monthly' => 'Monthly','yearly' => 'Yearly'), $drops,array('class'=>'form-control')); !!}
					@elseif($t['field'] =='next_due_at')
						{!! Form::text('next_due_at', $drops,array('class'=>'form-control date', 'placeholder'=>'' )) !!}
					@elseif($t['field'] =='id')

					@else
						@if(isset($t['search_value']))
							{!! SiteHelpers::transForm($t['field'] , $tableForm, false, $t['search_value']) !!}
						@else
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}
						@endif
					@endif
					</td>
					@endif
				@endforeach
				<td >
				<input type="hidden"  value="Search">
				<button type="button"  class=" do-quick-search btn btn-xs btn-info"> {{ Lang::get('core.GO') }}</button></td>
			 </tr>

            @foreach ($rowData as $row)
                <tr>
					<td width="30"> {{ ++$i }} </td>
					<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->id }}" />  </td>
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1' && !in_array($field['field'],$hidegrid))
					 <td>
					 	@if($field['attribute']['image']['active'] =='1')
							{!! SiteHelpers::showUploadedFile($row->$field['field'],$field['attribute']['image']['path']) !!}
						@else
						@if($field['field'] == 'payment_method' && $row->$field['field'] == 'stripe')
							{{--*/ $row->$field['field'] = Lang::get('core.stripe');  /*--}}
						@elseif($field['field'] == 'payment_method' && $row->$field['field'] == 'paypal_express_checkout')
							{{--*/ $row->$field['field'] = Lang::get('core.paypal_express_checkout');  /*--}}
						@endif
							{{--*/ $conn = (isset($field['conn']) ? $field['conn'] : array() ); $row->$field['field'] = ucfirst($row->$field['field']);  /*--}}
							{!! SiteHelpers::gridDisplay($row->$field['field'],$field['field'],$conn) !!}
						@endif
					 </td>
					 @endif
				 @endforeach
				 <td>
				 		@if($access['is_edit'] ==1 && 1 == 2)
						<a  href="{{ URL::to('membership/users/update/'.$row->id.'?return='.$return) }}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a>
						@endif
					 	@if($access['is_detail'] ==1 && $row->payment_method != 'free' && 1 == 2)
						<a href="{{ URL::to('membership/users/show/'.$row->id.'?return='.$return)}}" class="tips btn btn-xs btn-white" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
						@endif



				</td>
                </tr>

            @endforeach

        </tbody>

    </table>
	<input type="hidden" name="md" value="" />
	</div>
	{!! Form::close() !!}
	@include('footer')
	</div>
</div>
	</div>
</div>
<script>
$(document).ready(function(){

	$('.do-quick-search').click(function(){
		$('#bsetecTable').attr('action','{{ URL::to("membership/multisearchusers")}}');
		$('#bsetecTable').submit();
	});

});
</script>
@stop