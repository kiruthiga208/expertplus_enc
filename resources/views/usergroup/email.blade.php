@extends('layouts.app')
<link rel="stylesheet" href="{{asset('assets/bsetec/css/style.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/bsetec/css/redactor.css') }}" />
@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>

      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ul>	  
	  
    </div>
	
	
	<div class="page-content-wrapper m-t">	 	

<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
<div class="sbox-tools" >
		</div>
	</div>
	<div class="sbox-content"> 	
	
	 	{!! Form::open(array('url'=>'usergroup/sendmail', 'class'=>'form-horizontal ','parsley-validate'=>' ' ,'novalidate'=>' ')) !!}
	 		
	 		<div class="form-group  " >
			<label for="Id" class=" control-label col-md-4 text-left"> Subject </label>
			<div class="col-md-6">			
			{!! Form::text('subject', '',array('class'=>'form-control email_type'   )) !!} 
			</div> 
			<div class="col-md-2">
			</div>
			</div> 
	 		<div class="form-group" >
			<label for="email_type" class=" control-label col-md-4 text-left"> Email Type </label>
			<div class="col-md-6">
			<select name="email_type" class="email_type form-control" required>
				<option value="1">Group</option>
				<option value="2">Individual</option>
			</select>
			</div> 
			</div>
			
			<div class="form-group group"  >
			<label for="group" class=" control-label col-md-4 text-left"> User Group </label>
			<div class="col-md-6">
			<select name="group_id" class="group_id form-control" >
				<option value=''>Select user group</option>
				@foreach($newsgroups as $news)
				<option value="{!! $news->id !!}">{!! $news->group_name!!}</option>
				@endforeach
			</select>
			</div> 
			</div> 	
			<div class="form-group user">
			<label for="individual" class=" control-label col-md-4 text-left"> Specific User </label>
			<div class="col-md-6">
			<select name="user_id" class="user_id form-control" >
				<option value=''>Select specific user </option>
				@foreach($users as $user)
				<option value="{!! $user->email !!}">{!! $user->first_name !!}</option>
				@endforeach
			</select>
			</div> 
			</div> 	
			<div class="form-group" >
			<label for="message" class=" control-label col-md-4 text-left"> Message </label>
			<div class="col-md-6">
				<textarea id="editor" name="email_message"  class="form-control "></textarea>
			</div> 
			</div> 
			 <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<!-- <button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button> -->
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> Send Mail</button>
					<button type="submit" name="apply" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> Send Mail Later</button>
					<button type="button" onclick="location.href='{{ URL::to('usergroup') }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
			
	 	{!! Form::close() !!}
	
	</div>
</div>	
	</div>	  
</div>	
<script>
$(document).ready(function(){
	$('.user').css('visibility', 'hidden');
	
	//alert('Hi');

	$('body').on('change', '.email_type', function(event)
	{
		//alert('Hi');
		var value=$(this).val();
		if(value==1)
		{
			$('.group').css('visibility', 'visible');
			$('.user').css('visibility', 'hidden');
		}
		else if(value==2)
		{
			$('.group').css('visibility', 'hidden');
			$('.user').css('visibility', 'visible');
		}
		
	});
});	
</script>		
@stop