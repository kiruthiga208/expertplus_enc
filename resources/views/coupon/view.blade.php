@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('coupon?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('coupon?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('coupon/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Username') }}</td>
				<td>{{ $row->username }} </td>

				</tr>
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.course_title') }}</td>
				<td>{{ $row->course_title }} </td>

				</tr>
				
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.coupon_code') }}</td>
				<td>{{ $row->coupon_code }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Coupon_Type') }}</td>
				<td>{!! ($row->coupon_type == 1) ? "Percentage" : "Fixed Price" !!}</td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Coupon_Value') }}</td>
				<td>{{ $row->coupon_value }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.coupon_Sdate') }}</td>
				<td>{{ $row->coupon_start_date }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.coupon_Edate') }}</td>
				<td>{{ $row->coupon_end_date }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>{{ Lang::get('core.Coupon_Status') }}</td>
				<td>{!! ($row->coupon_status == 1) ? Lang::get('core.fr_mactive') :  Lang::get('core.fr_minactive') !!} </td>

				</tr>
				
				
				
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop