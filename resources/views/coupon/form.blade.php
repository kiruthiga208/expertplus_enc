@extends('layouts.app')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('coupon?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	

		 {!! Form::open(array('url'=>'coupon/save/'.$row['id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> {{ Lang::get('core.coupon') }}</legend>
								
							<div class="form-group  " >
							<label for="Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.id') }} </label>
							<div class="col-md-6">
							{!! Form::text('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="User Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.User_Id') }} </label>
							<div class="col-md-6">
							{!! Form::text('user_id', $row['user_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Course Id" class=" control-label col-md-4 text-left"> {{ Lang::get('core.course_id') }} </label>
							<div class="col-md-6">
							{!! Form::text('course_id', $row['course_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Desc" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_desc') }} </label>
							<div class="col-md-6">
							<textarea name='coupon_desc' rows='2' id='coupon_desc' class='form-control '  
			  >{{ $row['coupon_desc'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Code" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_code') }} </label>
							<div class="col-md-6">
							{!! Form::text('coupon_code', $row['coupon_code'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Type" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Type') }} </label>
							<div class="col-md-6">
							{!! Form::text('coupon_type', $row['coupon_type'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Value" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Value') }} </label>
							<div class="col-md-6">
							{!! Form::text('coupon_value', $row['coupon_value'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Start Date" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_Sdate') }} </label>
							<div class="col-md-6">
							{!! Form::text('coupon_start_date', $row['coupon_start_date'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon End Date" class=" control-label col-md-4 text-left"> {{ Lang::get('core.coupon_Edate') }}  </label>
							<div class="col-md-6">
							{!! Form::text('coupon_end_date', $row['coupon_end_date'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Coupon Status" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Coupon_Status') }}  </label>
							<div class="col-md-6">
							{!! Form::text('coupon_status', $row['coupon_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Created At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Created_At') }} </label>
							<div class="col-md-6">
							{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Updated At" class=" control-label col-md-4 text-left"> {{ Lang::get('core.Updated_At') }} </label>
							<div class="col-md-6">
							{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('coupon?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop