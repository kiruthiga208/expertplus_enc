@extends('layouts.frontend')

@section('content')

  <div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('questionanswer?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active">{{ Lang::get('core.addedit') }} </li>
      </ul>
	  	  
    </div>
 
 	<div class="page-content-wrapper">

		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	
<h2>haiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii</h2>
		 {!! Form::open(array('url'=>'questionanswer/save/'.$row['question_id'].'?return='.$return, 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div class="col-md-12">
			<fieldset><legend> Questionanswer</legend>
								
							<div class="form-group hidethis " style="display:none;">
							<label for="Question Id" class=" control-label col-md-4 text-left"> Question Id </label>
							<div class="col-md-6">
							{!! Form::text('question_id', $row['question_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;">
							<label for="User Id" class=" control-label col-md-4 text-left"> User Id </label>
							<div class="col-md-6">
							{!! Form::text('user_id', $row['entry_by'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Enter your Question" class=" control-label col-md-4 text-left"> Enter your Question <span class="asterix"> * </span></label>
							<div class="col-md-6">
							{!! Form::text('question_text', $row['question_text'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;">
							<label for="Question Status" class=" control-label col-md-4 text-left"> Question Status </label>
							<div class="col-md-6">
							{!! Form::text('question_status', $row['question_status'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group  " >
							<label for="Question Description" class=" control-label col-md-4 text-left"> Question Description </label>
							<div class="col-md-6">
							<textarea name='question_description' rows='2' id='question_description' class='form-control '  
			  >{{ $row['question_description'] }}</textarea> 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;">
							<label for="Approved" class=" control-label col-md-4 text-left"> Approved </label>
							<div class="col-md-6">
							{!! Form::text('approved', $row['approved'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> 					
							<div class="form-group hidethis " style="display:none;">
							<label for="Views" class=" control-label col-md-4 text-left"> Views </label>
							<div class="col-md-6">
							{!! Form::text('views', $row['views'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
							</div> 
							<div class="col-md-2">
							
							</div>
							</div> </fieldset>
		</div>

		
			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
					<button type="button" onclick="location.href='{{ URL::to('questionanswer?return='.$return) }}' " class="btn btn-success btn-sm "><i class="fa  fa-arrow-circle-left "></i>  {{ Lang::get('core.sb_cancel') }} </button>
					</div>	  
			
				  </div> 
		 
		 {!! Form::close() !!}
	</div>
</div>		 
</div>	
</div>			 
   <script type="text/javascript">
	$(document).ready(function() { 
		 
	});
	</script>		 
@stop