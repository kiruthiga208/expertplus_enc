<div class="sbox-content">
	<div class="row">
		<div class="col-sm-12">
			<div class="col-sm-1 no-padding">
				<a href="{!! $row->user_link !!}">
					<center>
						{!! $row->imgpath !!}
						<h4 class="m-t-sm small">{!! $row->display_name !!}</h4>
					</center>
				</a>
			</div>
			<div class="col-sm-11">
				<input type="hidden" id="question_id_{!! $row->question_id !!}" value="{!! $row->question_id !!}">
				<input type="hidden" id="question_text_{!! $row->question_id !!}" value="{!! $row->question_text !!}">
				<input type="hidden" id="category_id_{!! $row->question_id !!}" value="{!! $row->category_id !!}">
				<input type="hidden" id="question_description_{!! $row->question_id !!}" value="{!! $row->question_description !!}">

				<input type="hidden" id="sub_cat_id_{!! $row->question_id !!}" value="{!! $row->sub_cat_id !!}">

				<div class="row m-t-sm">
					<div class="col-sm-10 text-left">
						<a href="{{ URL::to('questionanswer/show/'.$row->question_id.'?return='.$return) }}"><h3>{!! $row->question_text !!}</h3></a>
						<div>{!! $row->question_description !!}</div>
						<strong>{!! bsetecHelpers::siteCategories($row->category_id)->name !!}  
					    @if($row->sub_cat_id!=0)
						 / {!! bsetecHelpers::sitesubcategories($row->category_id,$row->sub_cat_id)->sub_name !!}
						@endif
						</strong>
						<span>{!! Lang::get('core.vert_sep')!!}</span>
						<span><a href="{{ URL::to('questionanswer/show/'.$row->question_id.'?return='.$return) }}">{!! ($row->answer_count > 0) ? (($row->answer_count == 1) ? $row->answer_count.' '.Lang::get('core.Answer') : $row->answer_count.' '.Lang::get('core.Answers')) : Lang::get('core.NoAnswers')  !!}</a></span>
						<span>{!! Lang::get('core.vert_sep')!!}</span>
						<h5 class="inline"><em>{!! ($row->views > 0) ? (($row->views == 1) ? $row->views.' '.Lang::get('core.view') : $row->views.' '.Lang::get('core.views')) : Lang::get('core.noviews')  !!}</em></h5>
						<span>{!! Lang::get('core.vert_sep')!!}</span>
						<h5 class="inline"><em>{!! bsetecHelpers::timeago(strtotime($row->createdOn)) !!}</em></h5>
					</div>
					<div class="col-sm-2 text-right m-t-sm">
						@if($type == '2')
							@if($access['is_edit'] ==1)
								<a href="javascript:void(0)" data-id="{!! $row->question_id !!}" class="tips btn btn-xs btn-white questionEdit" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-pencil "></i></a>
							@endif
							@if($access['is_remove'] ==1)
								<a href="{{ URL::to('questionanswer/deletequestion/'.$row->question_id.'?return='.$return) }}" class="tips btn btn-xs btn-white remove_question" title="{{ Lang::get('core.btn_remove') }}"><i class="fa fa-trash-o "></i></a>
							@endif

							<button type="button" class="btn btn-color block m-t-sm answerQuestion" id="answer_block_{!! $row->question_id !!}_click" data-id="answer_block_{!! $row->question_id !!}">{!! Lang::get('core.submit_your_reply')!!}</button>

						@elseif($type == '1')
					 		<button type="button" class="btn btn-color block m-t-sm answerQuestion" id="answer_block_{!! $row->question_id !!}_click" data-id="answer_block_{!! $row->question_id !!}">{!! Lang::get('core.submit_your_reply')!!}</button>
						@elseif($type == '0')
 						    <a href="{{ URL::to('questionanswer/show/'.$row->question_id) }}" class="btn btn-color m-t-sm logbackurl">{!! Lang::get('core.submit_your_reply')!!}</a>
						@endif
					</div>
				</div>
				<div class="row m-t-sm">
					@if(!empty($row->file_ids))
                       @php ( $file_list = json_decode($row->file_ids) )
                         <div class="form-group show-grid">
                            <div class="col-md-12">
                            @foreach($file_list as $file_id)
                                @php ( $file = $model->getFiledata($file_id))
                                @php ( $resourceid = \SiteHelpers::encryptID($file_id) )
                                <div class="file_id_{!! $file_id !!}">
                                	<div>{!! $file->file_title !!}
                                		<span style="cursor:pointer;" class="arrow"><a href="{!! URL::to('questionanswer/downloadresource/'.$resourceid) !!}"><i class="fa fa-download"></i></a></span>
                                	</div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    @endif
				</div>
				@if($type == '1' || $type == '2')
				<div class="row show-grid hidden" id="answer_block_{!! $row->question_id !!}">
					<div class="col-sm-12">
						{!! Form::open(array('url'=>'questionanswer/answer','id'=>'ans_form','class'=>'','files' => true,'novalidate'=>' ')) !!}
							{!! Form::hidden('question_id',$row->question_id) !!}
							{!! Form::hidden('user_id',$loggedid) !!}
							<div class="form-group">
								<label for="answer_text">{!! Lang::get('core.youranswer')!!}</label>
								<textarea name="answer_text" required class="form-control required" placeholder="{!! Lang::get('core.youranswer')!!}"></textarea>
							</div>
							<div class="form-group m-b-sm">
								<button type="submit" id="answer_submit" class="btn btn-color answerSubmit">{!! Lang::get('core.submit')!!}</button>
								<button type="button" data-id="answer_block_{!! $row->question_id !!}" class="btn btn-color btn-default answerCancel">{!! Lang::get('core.cancel')!!}</button>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#ans_form').validate({
			rules : {
				answer_text:{
					required : true,
					minlength: 5,
                	maxlength: 1000
				}
			},
			messages: {
	            answer_text: {
	                required: "{{ Lang::get('core.ans_req') }}",
	                minlength: "{{ Lang::get('core.ans_min') }}",
	                maxlength: "{{ Lang::get('core.ans_max') }}"
	            }
	        },submitHandler:function(){
	        	$('#answer_submit').prop('disabled',true);
	        	return true;
	        }
		});

	});
</script>