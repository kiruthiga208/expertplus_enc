<div class="modal fade" id="askQuestionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog course_popup" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" id="btn_close" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{!! Lang::get('core.askaquestion')!!}</h4>
            </div>
            <div class="modal-body">
               <!--  <div class="row show-grid">
                    <div class="col-md-6">
                        <div class="eo-button-box m-0-top-bottom text-center">
                            <h3>{!! Lang::get('core.submit_question')!!} </h3>
                            <input type="radio" id="question_type_free" name="question_type" class="hidden" value="0" checked="checked">
                            <p class="m-t-sm m-l-xl m-r-xl">{!! Lang::get('core.long_time_answer')!!}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="eo-button-box m-0-top-bottom text-center">
                            <h3>{!! Lang::get('core.fast_expert_answer')!!} </h3>
                            <input type="radio" id="question_type_paid" name="question_type" class="hidden" value="1">
                            <p class="m-t-sm m-l-xl m-r-xl">{!! Lang::get('core.answer_within')!!}</p>
                        </div>
                    </div>
                </div>
                 <div id="proceed_paid" style="display: none">
                    <center>
                    <a href="{{ URL::to('customcourserequest/update') }}" id="continue_to_paid" class="btn btn-color">{!! Lang::get('core.continue')!!}</a>
                    <p class="m-t-sm"><em>{!! Lang::get('core.continue_to_get')!!}</em></p>
                    </center>
                </div> -->
                <?php echo Form::open(array('url' => 'questionanswer/save', 'method' => 'post','id'=>'askQuestionform')); ?>
                <input name="question_id" id="question_id" type="hidden" value="">
                <div class="form-group">
                    <h3 for="question_text">{!! Lang::get('core.question_title')!!}</h3>
                    <input type="text" name="question_text" class="form-control required" required id="question_text" placeholder="{!! Lang::get('core.question_title')!!}">
                </div>
                <div class="form-group">
                    <h3 for="category_id">{!! Lang::get('core.question_category')!!}</h3>
                    <select name="category_id" id="category_id" class="form-control required" required>
                        <option value="">{!! Lang::get('core.select_category')!!}</option>
                        @foreach(\bsetecHelpers::siteCategories() as $category)
                        <option value="{!!  $category->id  !!}">{!!  $category->name  !!}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <h3 for="category_id">{!! Lang::get('core.subcategory')!!}</h3>
                    <select name="sub_cat_id" id="sub_cats" class="form-control">
                        <option value="">{!! Lang::get('core.select_subcategory')!!}</option>
                    </select>
                </div>
                <div class="form-group">
                    <h3 for="question_description">{!! Lang::get('core.Description')!!}</h3>
                    <textarea name="question_description" required class="form-control required" id="question_description" placeholder="{!! Lang::get('core.Description')!!}"></textarea>
                </div>
                <div class="form-group">
                    <h3 for="skills" class="text-left"> {!! Lang::get('core.Add_files___')!!} </h3>
                </div>
                <div class="form-group">
                    {!! Form::hidden('deleteres', url('questionanswer/docdelete')) !!}
                    {!! Form::hidden('file_ids', '',array('id'=>'file_ids')) !!}
                    <input id="luploaddoc" class="btn btn-color luploaddoc" type="file" name="luploaddoc" data-url="{!! url('questionanswer/upload') !!}">
                </div>
                <div class="form-group">
                    <input type="hidden" id="probar_status" value="0" />
                    <div class="luploadvideo-progressbar meter">
                        <div class="bar" id="probar" style="width:0%"></div>
                    </div>
                </div>
                <div class="form-group">
                    <div id="docresponse"></div>
                </div>
                <div class="form-group">
                    <button type="submit" id="askquestionsubmit" class="btn btn-color">{!! Lang::get('core.Ask_Questions')!!}</button>
                    <?php  echo Form::close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.ui.widget.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-process.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/fileupload/jquery.fileupload-validate.js') }}"></script>
<script>
$(document).ready(function(){

    $('input[type="checkbox"],input[type="radio"]').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('#question_type_free').on('ifChecked', function(event){
        $('#proceed_paid').hide();
        $('#askQuestionform').show();
    });

    $('#question_type_paid').on('ifChecked', function(event){
        $('#askQuestionform').hide();
        $('#proceed_paid').show();
    });

	$(document).on('click','#askQuestion,#postQuestion',function(){
		$('#question_id').val('');
		$('#question_text').val('');
		$('#category_id').val('');
		$('#question_description').val('');
        $('#askQuestionModal').modal({
            backdrop: 'static',
            keyboard: false
        });
		$('#question_text-error').hide();
		$('#category_id-error').hide();
		$('#question_description-error').hide();
        $('#sub_cats').html('<option value="">{!! Lang::get('core.select_subcategory')!!}</option>');
        $('#docresponse').html('');
        $('#file_ids').val('');
    });

	$("#askQuestionform").validate({
        rules: {
            question_text: {
                required: true,
                minlength: 5,
                maxlength: 200
            },
            category_id: {
                required: true
            },
			question_description: {
                required: true,
                minlength: 10,
                maxlength: 1000
            }
        },
        messages: {
            question_text: {
                required: "{{ Lang::get('core.question_title_req') }}",
                minlength: "{{ Lang::get('core.question_title_min') }}",
                maxlength: "{{ Lang::get('core.question_title_max') }}"
            },
            category_id: {
                required: "{{ Lang::get('core.question_category_req') }}"
            },
            question_description: {
                required: "{{ Lang::get('core.question_description_req') }}",
                minlength: "{{ Lang::get('core.question_description_min') }}",
                maxlength: "{{ Lang::get('core.question_description_max') }}"
            }
        },submitHandler: function() {
            $('#askquestionsubmit').prop("disabled", true);
            return true;
        }
    });

	$('.remove_question').click(function(e){
		if (!confirm("{{ Lang::get('core.sure_remove') }}")) {
			e.preventDefault();
		}
	});

	$('.answerQuestion').click(function(e){
		var elem = $(this);
		$('#'+elem.attr('data-id')).removeClass('hidden');
		elem.hide();
	});

	$('.answerCancel').click(function(e){
		var elem = $(this);
		$('#'+elem.attr('data-id')).addClass('hidden');
		$('#'+elem.attr('data-id')+'_click').show();
		$('#'+elem.attr('data-id')).find('textarea').val('');
	});

	$('.answerEdit').click(function(e){
		var elem = $(this);
		$('#'+elem.attr('data-id')).removeClass('hidden');
		elem.hide();
	});

	$('.answer_ans_Cancel').click(function(e){
		var elem = $(this);
		$('#'+elem.attr('data-id')).addClass('hidden');
		$('#'+elem.attr('data-id')+'_click').show();
		$('#'+elem.attr('data-id')).find('textarea').val('');
	});

	$('.questionEdit').click(function(e){
		var elem = $(this);
		var id = elem.attr('data-id');
		$('#question_id').val($('#question_id_'+id).val());
		$('#question_text').val($('#question_text_'+id).val());
		$('#category_id').val($('#category_id_'+id).val());

        $('#category_id').trigger("change");
        $(document).ajaxStop(function () {
            $('#sub_cats').val($('#sub_cat_id_'+id).val());
        });

		$('#question_description').val($('#question_description_'+id).val());
		$('#askQuestionModal').modal({
            backdrop: 'static',
            keyboard: false
        });
		$('#question_text-error').hide();
		$('#category_id-error').hide();
		$('#question_description-error').hide();

        var file_data = '';
        @if(!empty($row->file_ids))
            @php ($file_list = json_decode($row->file_ids) )
            $('#file_ids').val("{!! $row->file_id_list !!}");
            file_data = ' @foreach($file_list as $file_id) @php ( $file = $model->getFiledata($file_id) )<div class="file_id_{{$file_id}}"><p>{!! $file->file_title !!}<span style="cursor:pointer;" class="arrow resdelete" data-id="{{$file_id}}"><i class="fa fa-trash-o"></i></span></p></div> @endforeach ';
        @endif
        $('#docresponse').html(file_data);

	});

    $('.luploaddoc').fileupload({
        autoUpload: true,
        acceptFileTypes: /(\.|\/)(zip|jpg|jpeg|png|pdf|doc|docx|txt)$/i,
        maxFileSize: 25000000, // 25 MB
        progress: function (e, data) {
            // console.log(data);
            $('#probar_status').val(1);
            var percentage = parseInt(data.loaded / data.total * 100);
            $('#probar').css('width',percentage+'%');
            if(percentage == '100') {
                $('#probar').text('{!! Lang::get("core.lecture_file_process")!!}');
            }
        },
        processfail: function (e, data) {
            $('#probar_status').val(0);
            file_name = data.files[data.index].name;
            alert("{!! Lang::get('core.file_not_allowed_size')!!}");
        },
        done: function(e, data){
            var return_data = $.parseJSON( data.result );
            if(return_data.status==true){
                $('#probar').css('width','0%');
                $('#probar').text('');
                var file_ids = $('#file_ids').val();
                if(file_ids == ''){
                    $('#file_ids').val(return_data.id);
                } else {
                    $('#file_ids').val(file_ids+','+return_data.id);
                }
                $("#docresponse").append('<div class="file_id_'+return_data.id+'"><p>'+return_data.file_title+'<span style="cursor:pointer;" class="arrow resdelete" data-id="'+return_data.id+'"><i class="fa fa-trash-o"></i></span></p></div>');
                $('#probar_status').val(0);
            }
            else if(return_data.status==false)
            {
                 alert("Please upload correct format files!!!!");
                 $('#probar').text('');
            }
            else
            {

            }

        }
    });

    $(document).on('click','.resdelete',function () {
        $(this).text('Deleting...');
        var _token=$('[name="_token"]').val();
        var id = $(this).data('id');
        // var ccr_id = $('[name="ccr_id"]').val();
        var ccr_id = $('[name="question_id"]').val();
        $.ajax ({
            type: "POST",
            url: $('[name="deleteres"]').val(),
            data: "&lid="+ccr_id+"&rid="+id+"&_token="+_token,
            success: function (msg)
            {
                var file_ids = $('#file_ids').val();
                var val = removeValue(file_ids,id,',');
                $('#file_ids').val(val);
                $('.file_id_'+id).remove();
            }
        });
    });

    var removeValue = function(list, value, separator) {
      separator = separator || ",";
      var values = list.split(separator);
      for(var i = 0 ; i < values.length ; i++) {
        if(values[i] == value) {
          values.splice(i, 1);
          return values.join(separator);
        }
      }
      return list;
    };
    $(document).on('change','#category_id',function(){
      var cats = $(this).val();
      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
      });
      $.ajax({
        type: 'POST',
        url: '<?php echo e(\URL::to("course/subcategory")); ?>',
        data:'cats='+cats+'&subcat=0',
        success : function(data) {
          $('#sub_cats').html(data);
        }
      });

    });

});
</script>