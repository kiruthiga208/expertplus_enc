<div class="page-content m-l-xl m-r-xl">
    <div class="row">
        @if(!empty($rowData))
            @foreach($rowData as $row)
                @include('questionanswer/question_block')
            @endforeach
        @elseif(!empty($search))
            <h3><center>{!! Lang::get('core.your_search').' "'.$search.'" '.Lang::get('core.no_match_question') !!}</center></h3>
        @else
            <h3><center>{!! Lang::get('core.noquestions') !!}</center></h3>
        @endif
    </div>
    <div class="table-footer">
    <div class="row">
            <div class="col-sm-12">
                <div class="arrow m-t-md">
                    {!! $pagination->appends(['ty'=>1,'t'=>$type,'qt'=>$qtype])->render(); !!}
                </div>
            </div>
        </div>
    </div>
</div>