@extends('layouts.frontend')
@section('content')
<style>
.anwsers_block h4 {
    background: #158d8e;
    color: #fff;
    font-size: 16px;
    padding: 14px 20px;
    margin: 20px 0 20px;
}
.btn.btn-warning{
    font-size: 12px;
    color: #fff;
    padding: 7px 20px;
    text-transform: capitalize;
    border-radius: 3px;
    border: none;
}
</style>
<div class="page-header">
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('questionanswer/view') }}">Question & Answer</a></li>
        <li class="active"> {!! $row->question_text !!} </li>
      </ul>
</div> 
<div class="page-content row">
	<div class="reg_form new_reg_form mycourse_block">
		<div id="mainwrapper">
			<div class="col-sm-12">
				<div class="head_block clearfix">
					 <h2 class="title">{!! Lang::get('core.questions')!!}</h2>
					 <div class="TeachingCourse_create">
						<p class="discover_courses"><a class="btn course_create" id="askQuestion" href="javascript:void(0)">{!! Lang::get('core.askaquestion') !!}</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="page-content m-l-xl m-r-xl">
	<div class="row animated fadeInRight">
		@include('questionanswer/question_block')
	</div>

	<div class="row">
		<div class="anwsers_block">
			<h4>{!! Lang::get('core.Answers')!!}</h4>
		</div>
	</div>

	<div class="row">
		@foreach($ans as $an)
			@include('questionanswer/answer_block')
		@endforeach
	</div>

</div>

@include('questionanswer/add_block')
@stop