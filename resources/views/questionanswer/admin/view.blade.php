@extends('layouts.app')

@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
      <div class="page-title">
        <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
      </div>
      <ul class="breadcrumb">
        <li><a href="{{ URL::to('dashboard') }}">{{ Lang::get('core.home') }}</a></li>
		<li><a href="{{ URL::to('questionanswer?return='.$return) }}">{{ $pageTitle }}</a></li>
        <li class="active"> {{ Lang::get('core.detail') }} </li>
      </ul>
	 </div>  
	 
	 
 	<div class="page-content-wrapper">   
	   <div class="toolbar-line">
	   		<a href="{{ URL::to('questionanswer?return='.$return) }}" class="tips btn btn-xs btn-default" title="{{ Lang::get('core.btn_back') }}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{{ Lang::get('core.btn_back') }}</a>
			@if($access['is_add'] ==1)
	   		<a href="{{ URL::to('questionanswer/update/'.$id.'?return='.$return) }}" class="tips btn btn-xs btn-primary" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit"></i>&nbsp;{{ Lang::get('core.btn_edit') }}</a>
			@endif  		   	  
		</div>
<div class="sbox animated fadeInRight">
	<div class="sbox-title"> <h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h4></div>
	<div class="sbox-content"> 	


	
	<table class="table table-striped table-bordered" >
		<tbody>	
	
				<tr>
				<td width='30%' class='label-view text-right'>Question Id</td>
				<td>{{ $row->question_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Entry By</td>
				<td>{{ $row->entry_by }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Category Id</td>
				<td>{{ $row->category_id }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Question Text</td>
				<td>{{ $row->question_text }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Question Description</td>
				<td>{{ $row->question_description }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Question Status</td>
				<td>{{ $row->question_status }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Approved</td>
				<td>{{ $row->approved }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Views</td>
				<td>{{ $row->views }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>Answer Count</td>
				<td>{{ $row->answer_count }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>CreatedOn</td>
				<td>{{ $row->createdOn }} </td>

				</tr>
				
				<tr>
				<td width='30%' class='label-view text-right'>UpdatedOn</td>
				<td>{{ $row->updatedOn }} </td>

				</tr>
				
		</tbody>	
	</table>    
	
	</div>
</div>	

	</div>
</div>
	  
@stop