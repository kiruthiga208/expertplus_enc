@extends('layouts.app')
@section('content')
@php usort($tableGrid, "SiteHelpers::_sort") @endphp
<div class="page-content row">
	<!-- Page header -->
	<div class="page-header">
		<div class="page-title">
			<h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
		</div>
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
			<li class="active">{{ $pageTitle }}</li>
		</ul>
	</div>
	<div class="page-content-wrapper m-t">
		<div class="sbox animated fadeInRight">
			<div class="sbox-title"> <h5> <i class="fa fa-table"></i> </h5>
			</div>
			<div class="sbox-content">
				<div class="toolbar-line ">
				  <!--  <a href="{{ url('questionanswer/update') }}" class="tips btn btn-sm btn-white" title="Create"><i class="fa fa-plus-circle "></i>&nbsp;Create</a> -->
					<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Approve" data-type="1"><i class="fa fa-check"></i>&nbsp;{{ Lang::get('core.approve') }}</a>
					<a href="javascript://ajax" class="tips btn btn-sm btn-white updatestatus" title="Unapprove" data-type="2"><i class="fa fa-times"></i>&nbsp;{{ Lang::get('core.unapprove') }}</a>
					<a href="javascript://ajax"  onclick="bsetecDelete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}" data-type="0"><i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
				</div>
				{!! Form::open(array('url'=>'questionanswer/delete/', 'class'=>'form-horizontal' ,'id' =>'bsetecTable' )) !!}
				{!! Form::hidden('types', '',array('id'=>'actiontype'))  !!}
				<div class="table-responsive" style="min-height:300px;">
					<table class="table table-striped ">
						<thead>
							<tr>
								<th class="number"> No </th>
								<th> <input type="checkbox" class="checkall" /></th>
								@foreach ($tableGrid as $t)
								@if($t['view'] =='1')
								<th>{{ $t['label'] }}</th>
								@endif
								@endforeach
								<th width="70" >{{ Lang::get('core.btn_action') }}</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($rowData as $row)
							<tr>
								<td width="30"> {{ ++$i }} </td>
								<td width="50"><input type="checkbox" class="ids" name="id[]" value="{{ $row->question_id }}" />  </td>
								@foreach ($tableGrid as $field)
								@php ($fieldname =$field['field'])
								@if($field['view'] =='1')
								<td>
									@if($field['attribute']['image']['active'] =='1')
									{!! SiteHelpers::showUploadedFile($row->$fieldname,$field['attribute']['image']['path']) !!}
									@elseif($field['field'] == 'entry_by') 
									@php (
									 $usrinfo = \bsetecHelpers::getuserinfobyid($row->entry_by) )
									 @if(!empty($usrinfo))
									@php ( $uname = $usrinfo->first_name.' '.$usrinfo->last_name )
										{{ $uname }}
									@endif
							
									@elseif($field['field'] == 'approved')
									  @if($row->$fieldname == '2') 
  								       <span class="label label-success">Approved</span> 
									  @elseif($row->$fieldname == '1') 
									  <span class="label label-danger">Unapproved</span>
									  @else
									    <span class="label label-danger">Submitted</span>
									  @endif
									@elseif($field['field'] == 'category_id') 
																		{{ \bsetecHelpers::siteCategories($row->$fieldname)->name }}

									@elseif($field['field'] == 'question_description') 
									<span style="max-height: 85%;max-width: 270%;overflow: hidden;position: absolute;top:0;width: 100%;height: 100%;" title="{{ $row->question_description }}">{{ $row->question_description }}</span>
									@else
									@php ( $conn = (isset($field['conn']) ? $field['conn'] : array() ) )
									{!! SiteHelpers::gridDisplay($row->$fieldname,$field['field'],$conn) !!}
									@endif
								</td>
								@endif
								@endforeach
								<td>
									@if($access['is_detail'] ==1)
									<a href="{{ URL::to('questionanswer/show/'.$row->question_id.'?return='.$return)}}" class="tips btn btn-xs btn-white" target="_blank" title="{{ Lang::get('core.btn_view') }}"><i class="fa  fa-search "></i></a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<input type="hidden" name="md" value="" />
				</div>
				{!! Form::close() !!}
				@include('footer')
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$(document).on('click','.updatestatus',function(){
		var status = $(this).data('type');
		$('#actiontype').val(status);
		if($("input[name='id[]']:checked").length==0){
			alert('Please select atleast one checkbox!');
		}else if($("input[name='id[]']:checked").length>0){
			if(confirm('Are you sure?'))
			{
				$('#bsetecTable').submit();// do the rest here
			}
		}else{
			alert('{{ Lang::get('core.select_checkbox') }}');
		}
	});
});
</script>
@stop