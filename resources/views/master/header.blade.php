<script type="text/javascript">  
function chgcolor()
{
    $('#notifyarea').removeClass('badge-danger');
}
</script>
<script type="text/javascript">
$(document).ready(function() {
// Function to change form action.
$("#db").change(function() {
var selected = $(this).children(":selected").text();
switch (selected) {
case "My-Sql":
$("#myform").attr('action', 'mysql');
alert("Form Action is Changed");
break;
default:
$("#myform").attr('action', '#');
}
});
// Function For Back Button
$(".back").click(function() {
parent.history.back();
return false;
});
});
</script>
<style type="text/css">
.req_title:before {color: #729F34;font-size: 26px;left: 0;position: absolute;top: 21px;}
.req_details .req_title {border-bottom: 1px solid #DEDEDE;border-radius: 0;cursor: pointer;margin: 0;padding: 10px 10px 10px 0px;position: relative;}
.bx-default-pager{display: none;}
.notification-container {
    overflow:auto;
    max-height:500px;  
}
</style>
 @if(\Session::get('uid')=='')
<div class="menuheader">
<div class="container">
<div class="row">
<div class="col-lg-4">
<div class="logo">
<h1><a href="{{url()}}">  <img src="{{ url().'/static/css/images/'.\bsetecHelpers::siteSettings('siteLogo')}}" alt=""/> </a></a></h1>
</div>
</div>

<div class="menurap col-lg-8 clearfix">
<div class="menu">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="navbar-collapse collapse" style="height: 1px;">
<ul class="nav navbar-nav">
<li class="first"><a href="{{ url('course') }}">{{ Lang::get('core.Course') }}</a></li>
<li><a href="{{ url('login') }}">{{ Lang::get('core.Login') }}</a></li>
<li class="last"><a href="{{ url('registration') }}">{{ Lang::get('core.Register') }}</a></li>
</ul>

</div>
</div>     
</div>

</div>
</div>     
</div>
@else 
@if(Request::segment(1) != 'learn-course')
<div class="menuheader_expert">
<div class="container">
<div class="row">

<div class="col-lg-7 left_section_first clearfix">
    
    <div class="logo">
        <h1><a href="{{url()}}">  <img src="{{ url().'/static/css/images/'.\bsetecHelpers::siteSettings('siteLogo')}}" alt=""/> </a></a></h1>
    </div>
    @if(Request::segment(1) != '' && Request::segment(1) != 'login' && Request::segment(1) != 'registration')
    @php ( $sitecategories = \bsetecHelpers::sitecategories() )
    <form id="myform" method="post" name="myform" action="">
    <div class="dropdown">
        <a href="#" data-toggle="dropdown">Browse Course</a>
        <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu">

        @foreach($sitecategories as $category)
        <li class="one_andod"><a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a>
        <div class="and_one">
        <ul>
        <li><!--<a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a>--></li>
        </ul>
        </div></li>
        @endforeach
        </ul>
        <span class="left_drop_read"></span>
    </div>
    </form>
    <div class="search_for_all_courses">
       <form role="search" method="GET" action="{{ url('search') }}"><input type="text" class="more_one_portiion" placeholder="Search for Courses" name="q" id="srch-term"><button class="btn btn-primary"  type="submit"></button></form>
    </div>
    @endif
</div>

<div class="menurap col-lg-5 clearfix">
<div class="menu">
<button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<div class="navbar-collapse collapse" style="height: 1px;">
<ul class="nav navbar-nav">
<li class="first"><a href="{{ url('course') }}">{{ Lang::get('core.Course') }}</a></li>
<li><a href="{{ url(Auth::user()->username.'/myCourse') }}">My Courses</a></li>
<li class="bell">
    <span class="liko"></span>
    <a href="#" class="menudroplist dropdown-toggle" data-toggle="dropdown">
    @php ( $notification = \bsetecHelpers::unreadNotifications() )
    @php ( $totalnotification = \bsetecHelpers::numberOfNotifications() )
    @if(count($notification) > 0)
        <span class="badge badge-danger" onclick="chgcolor()" id="notifyarea">{{ count($notification) }}</span>
    @elseif($totalnotification > 0)
        <span class="badge" id="notifyarea">{{ $totalnotification }}</span>
    @endif
    
    </a>
    <ul class="dropdown-menu NotificationMenu span4">
        <li class="notificationHeader">Notifications</li>
                            <li class="divider"></li>
        @if($totalnotification > 0)
            <div class="notification-container">
            @foreach($notification as $notice)
                <li><div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" alt="{{{ $notice->fullname }}}" src="{{ $notice->avatar }}">
                    </a>
                    <div class="media-body">
                        <a href="{{ url('user/'.$notice->username) }}">{{{ $notice->fullname }}}</a>
                            <span class="msg-time pull-right"><i class="glyphicon glyphicon-time"></i>
                                <span><abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($notice->created_at)) }}">{{ date(DATE_ISO8601,strtotime($notice->created_at)) }}</abbr>&nbsp;</span>
                            </span>
                        @if($notice->type == 'follow')
                            <p class="NotificationMsg">Started Following you</p>
                        @elseif($notice->type == 'comment')
                            <p class="span3 NotificationMsg">Commented on your course <a href="{{ url('course/'.$notice->course_id.'/'.$notice->slug) }}">{{{ ucfirst($notice->course_title) }}}</a>
                            </p>
                            <a class="pull-right" href="#">
                                <img src="{{ asset(zoomCrop('uploads/course/'.$notice->image,75,75))}}" alt="{{ $notice->course_title }}">
                            </a>
                        @elseif($notice->type == 'forum')
                            <!-- <p class="span3 NotificationMsg">Anwsered on your question <a href="{{ url('forum/'.$notice->forums->forum_id.'/'.$notice->forums->slug) }}">{{{ ucfirst($notice->forums->forum_topic) }}}</a></p> -->
                        @elseif($notice->type == 'like')
                            <p class="span3 NotificationMsg">Liked your course <a href="{{ url('course/'.$notice->course->course_id.'/'.$notice->course->slug) }}">{{{ ucfirst($notice->course_title) }}}</a>
                            </p>
                            <a class="pull-right" href="#">
                                <img src="{{ asset(zoomCrop('uploads/course/'.$notice->course->image,75,75))}}" alt="{{ $notice->course_title }}">
                            </a>
                        @elseif($notice->type == 'reply')
                            <p class="span3 NotificationMsg">Replied on your comment <a href="{{ url('course/'.$notice->course->course_id.'/'.$notice->course->slug) }}">{{{ ucfirst($notice->course->course_title) }}}</a>
                            </p>
                            <a class="pull-right" href="#">
                                <img src="{{ asset(zoomCrop('uploads/course/'.$notice->course->image,75,75))}}" alt="{{ $notice->course_title }}">
                            </a>
                        @elseif($notice->type == 'blog')
                            <p class="span3 NotificationMsg">You commented on the blog <a href="{{ url('blog/'.$notice->blogs->blog_id) }}">{{{ ucfirst($notice->blogs->blog_title) }}}</a></p>    
                            <a class="pull-right" href="#"></a>
                        @endif
                    </div>
                </div></li>
                <li class="divider"></li>
            @endforeach
            </div>
        @endif
        <li><a class="AllNotification" target="_blank" href="{{ url('notifications') }}">View all notifications</a></li>
    </ul>
</li>
<li class="add_lits last"><div class="dropdown">
<!--<a href="#" data-toggle="dropdown"> <b class="caret"></b></a>-->
  <a href="#" class="menudroplist dropdown-toggle" data-toggle="dropdown">
    <span><img src="{{ Auth::user()->avatar }}"></span>
    <b class="subject_arrow caret"></b>
</a>
    
<ul class="dropdown-menu menu_wd">
    <li><span class="userimg_width"><img src="{{ Auth::user()->avatar }}"></span>
        <span class="userimg_right">
            <span class="" >
            {{{ Auth::user()->fullname }}} </span>
            <div class="clearfix"></div>
            <div class="user_icons clearfix">
                <a href="{{ url('user/'.Auth::user()->username) }}"><i class="icon-user" title="View Profile"></i></a>
            <a href="{{ url('settings') }}"><i class="icon-cog" title="Edit Profile"></i></a>
            </div>
        </span>
    </li>
    <li><a href="{{ url(Auth::user()->username.'/myCourse') }}"><i class="fa fa-home fa-fw"></i><span>My Courses</span></a></li>
    <li><a href="{{ url('user/'.Auth::user()->username) }}"><i class="fa fa-pencil fa-fw"></i><span>{{ Lang::get('core.My Profile') }}</span></a></li>
    <li class="edit_tutorial"><a href="{{ url('credits') }}"><i class="fa fa-money"></i><span>{{ \bsetecHelpers::siteSettings('siteName') }}</span> {{ Lang::get('core.credits') }} $ {{\bsetecHelpers::getUserCredit()}}</a></li>
    <li><a href="{{ url('settings') }}"><i class="fa fa-pencil fa-fw"></i><span>{{ Lang::get('core.Profile Settings') }}</span></a></li>
    <li class="signoutblocks"><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i><span>{{ Lang::get('core.Logout') }}</span></a></li>
</ul>
<span class="left_drop"></span>
</div></li>
</ul>
</div>
</div>     
</div>

</div>
</div>     
</div>
@endif
@endif
<!--header End-->
<!--slider-->
@if(Request::segment(1) == '')
 @php ( $sitecategories = \bsetecHelpers::sitecategories() )
<div class="banner"><img src="{{ url('/static/css/images/'.\bsetecHelpers::siteSettings('siteHeaderBanner')) }}"  height="450"/>
    <div class="easier_course">
        <h2>Learning <span>Online Becomes</span> Easier</h2>
        <p>Academy is a perfect theme for sharing and selling your knowledge online.</p>

        <div class="seach_alone_courses clearfix">
            <form id="myform" method="post" name="myform" action="">
            <div class="dropdown">
                <a href="#" data-toggle="dropdown">CATEGORIES<b class="caret"></b></a>
                <ul class="dropdown-menu ul_width">
                    <li><a href="">CATEGORIES</a></li>
                    @foreach($sitecategories as $category)
                    <li><a href="{{ url( 'category/'.$category->slug) }}">{{ $category->name }}</a></li>
                     @endforeach
                </ul>
                <span class="right_drop"></span>
                <span class="left_drop"></span>
            </div>
            </form>
            <div class="course_searching"><form role="search" method="GET" action="{{ url('search') }}"><input type="text" class="more_one_portiion" placeholder="Find courses" name="q" id="srch-term"><button class="btn btn-primary"  type="submit"></button></form></div>
        </div>

        <div class="learn_remeber edit_tutorial"><a href="{{ url('credits') }}" class="btn btn-primary">BECOME A MEMBER</a></div>
        </div>
    </div>
</div>

@elseif(Request::segment(1) == 'login')
<div class="banner innerpage_banner"><img src="{{ url('/static/css/images/'.\bsetecHelpers::siteSettings('siteHeaderBannerInnerpages')) }}" height="200"/>
    <div class="innerpage_banner_head">
        <h2>{{ Lang::get('core.Login') }}</h2>
        <p>Join the Expert Plus Community</p>
    </div>
</div>
@elseif(Request::segment(1) == 'password')

<div class="banner innerpage_banner"><img src="{{ url('/static/css/images/'.\bsetecHelpers::siteSettings('siteHeaderBannerInnerpages')) }}" height="200"/>
    <div class="innerpage_banner_head">
        <h2>Reset Password</h2>
        <p>Join the Expert Plus Community</p>
    </div>
</div>

@elseif(Request::segment(1) == 'registration')

<div class="banner innerpage_banner"><img src="{{ url('/static/css/images/'.\bsetecHelpers::siteSettings('siteHeaderBannerInnerpages')) }}" height="200"/>
    <div class="innerpage_banner_head">
        <h2>SignUp</h2>
        <p>Join the Expert Plus Community</p>
    </div>
</div>

@elseif(Request::segment(1) == 'course' AND Request::segment(2) != '')

<!--<div class="section">
    <div class="container">
        <div class="row-fluid">
        @if(checkPurchase($course->course_id) == 0)
            <div class="span12 align_course">
                <div class="image_course"><img src="{{ url().'/uploads/course/'.$course->image }}" alt="{{ $course->course_title }}" class="img-thumbnail"></div>
                <div class="video_course">
                @if(file_exists(public_path()."/uploads/videos/".$course->video) && $course->video!='')
                <video id='my_video' class='video-js vjs-default-skin' controls preload='auto' width='470' height='264' data-setup='{}'>
                <source src='{{url("/uploads/videos/".$course->video)}}' type='{{$course->video_type}}'></video>
                @endif
                </div>
            </div>
        @endif
        </div>
    </div>
</div>-->

@elseif(Request::segment(1) == 'CreateCourses')
 
<div class="banner innerpage_banner">
    <img src="{{ url('/static/css/images/'.\bsetecHelpers::siteSettings('siteHeaderBannerInnerpages')) }}" height="200"/>
    <div class="container">
        <div class="row-fluid">
            <div class="span12 align_courseimage">
                <div class="teacher_details">
                    <div class="teacher_image">
                        <img src="{{ asset(zoomCrop('uploads/course/'.$course->image,150,90))}}" alt="{{ $course->course_title }}" class="img-thumbnail my_coursesblockss">
                    </div>
                </div>
                <div class="teacher_course">
                    <div class="course_name">
                        <a>{{ $course->course_title }}</a>
                    </div>            
                </div>
                <div class="course_button pull-right">
                    <div class="course_pub preview_btnblocks">
                        <a class="PreviewButton course_preview" target="_blank" href="{{url('course').'/'.$course->course_id.'/'.$course->slug}}">Preview</a>
                    </div>
                    <div class="course_pub">
                        <input type="button" onclick="return validate_click($(this));" class="course_publish" id="{{ $course->course_id }}" value="PUBLISH">
                        <a data-toggle="modal" class="course_publish_trigger course_publish" style="display:none" href="#PublisCourse">PUBLISH</a>
                    </div>
                </div>
            </div>
        </div> 
    </div>   
</div>

@else
<div class="reg_banner_empty"></div>
@endif
<!--slider ends-->