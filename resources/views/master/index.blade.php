<!doctype html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{{ $title }}}</title>
    @yield('metaDescription')


 <!-- css styles -->
   <link href="{{ asset('assets/bsetec/static/css/front/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"/>
   
     <!--IE-->
    <!--[if lte IE 8]>
        {{ HTML::style('bsetec/static/css/front/ie8.css') }}
        {{ HTML::style('bsetec/static/css/ie8.css') }}
    <![endif]-->

    <link href="{{ asset('assets/bsetec/static/css/front/bootstrap-responsive.css',array('id'=>"colors")) }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/media.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/tooltip.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/video-js.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/dasky.min.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/bsetec/static/css/front/jquery.bxslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/flexslider.css') }}" rel="stylesheet">
     <!-- Owl Carousel Assets -->
    <link href="{{ asset('assets/bsetec/static/css/front/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/bsetec/static/css/front/owl.theme.css') }}" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic"  rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,400italic,600,600italic,700italic,800,800italic" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700" rel="stylesheet">
    <!--css-->

  <!--   HTML5 shim, for IE6-8 support of HTML5 elements 
    [if lt IE 9]>
      {{ HTML::script('assets/bsetec/static/js/html5shiv.js') }}
    <![endif]
   Fav and touch icons -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/static/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/static/js/docs.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/static/js/jquery.jquery.flexslider..js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/static/js/jquery.bxslider.min.js') }}"></script>

<link rel="stylesheet" id="min-width768">
<link rel="stylesheet" id="max-width767">
<link rel="stylesheet" id="min-width768max-width991">
<link rel="stylesheet" id="min-width992">
<link rel="stylesheet" id="min-width992px_max-width1199px">
<link rel="stylesheet" id="min-width1200">
<script type="text/javascript">
// JavaScript Document
$(document).ready(function() {
    
function adjustStyle(width) {
    width = parseInt(width)+17;
    if (width >= 1200) {
        $("#min-width1200").attr("href", "css/min-width1200.css");
    }else{
        $("#min-width1200").removeAttr("href");
        }
    if ((width >= 992)) {
        $("#min-width992").attr("href", "css/min-width992.css");
    } else {
        $("#min-width992").removeAttr("href");
        }
    if ((width >= 768)) {
        $("#min-width768").attr("href", "css/min-width768.css");
    }else {
        $("#min-width768").removeAttr("href");
        }
    if ((width <= 767)) {
        $("#max-width767").attr("href", "css/max-width767.css");
    }else {
        $("#max-width767").removeAttr("href");
        }
    if ((width >= 992) && (width <= 1199)) {
        $("#min-width992px_max-width1199px").attr("href", "css/min-width992px_max-width1199px.css");
    }else {
        $("min-width992px_max-width1199px").removeAttr("href");
        }
    if ((width >= 768) && (width <= 991)) {
        $("#min-width768max-width991").attr("href", "css/min-width768max-width991.css");
    }else {
        $("#min-width768max-width991").removeAttr("href");
        }       
}

$(function() {
    adjustStyle($(this).width());
    $(window).resize(function() {
        adjustStyle($(this).width());
    });
});



});
</script>
</head>
<style type="text/css">
    .pagination > li.active:next{
        padding: 20px;
    }
    #infscr-loading {
    left: 50%;padding: 0 0 20px;position: relative;
    }
    .ReadonlyRating.startvalue > img {
    width: 15px;
}
</style>
<!-- Header-->
<body>
@include('master/header')
@yield('carousel')

@include('master/notices')
@yield('content')

@include('master/footer')
{{ HTML::script('assets/bsetec/static/js/jquery.js') }}
{{ HTML::script('assets/bsetec/static/js/front/bootstrap-collapse.js') }}

{{ HTML::script('assets/bsetec/static/js/jquery-ui.min.js') }}
{{ HTML::script('assets/bsetec/static/js/jquery.jscroll.min.js') }}
{{ HTML::script('assets/bsetec/static/js/jquery.timeago.js') }}
{{ HTML::script('assets/bsetec/static/js/front/tooltip.js') }}

{{ HTML::script('assets/bsetec/static/js/video.js') }}
{{ HTML::script('assets/bsetec/static/js/front/jquery.bxslider.min.js') }}

{{ HTML::script('assets/bsetec/static/js/dasky.eval.js') }}
{{ HTML::script('assets/bsetec/static/js/jmpress.min.js') }}
{{ HTML::script('assets/bsetec/static/js/jquery.raty.min.js') }}

{{ HTML::script('assets/bsetec/static/js/custom.js') }}
{{ HTML::script('assets/bsetec/static/js/bootstrap-datepicker.js') }}
{{ HTML::script('assets/bsetec/static/js/jquery.lazyload.min.js') }}
{{ HTML::script('assets/bsetec/static/js/multiupload.js') }}

<input type="hidden" id="getSiteUrl" value="{{url()}}">
<input type="hidden" id="FollowUrl" value="{{url('follow')}}">

<script type="text/javascript">
    $(document).ready(function(){
        $("img.lazy").lazyload({ effect : "fadeIn" });
        $('.MoreFromUser, .MoreFromSite').bxSlider({slideWidth: 75,minSlides: 2,maxSlides: 3,slideMargin: 10});
        $('.LatestCourse').bxSlider();
        $('.req_details .req_title').click(function(){
        if($(this).hasClass('faqopen')){$(this).removeClass('faqopen');$(this).addClass('faqclose');}
        else{$(this).addClass('faqopen');$(this).removeClass('faqclose');}$(this).children('.req_ans').slideToggle('slow');});
        $('.HomeScroll').infinitescroll({navSelector  : "ul.pagination",nextSelector : "ul.pagination a:first",itemSelector : ".HomeScroll .business_content",loading: {msgText: "<em>Loading...</em>", finishedMsg: 'That\'s It :)',img: '{{url().'/assets/bsetec/static/img/loading.gif'}}'}},function() {$("img.lazy").lazyload();$('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url().'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url().'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url().'/assets/bsetec/static/img/star-half.png'}}"});});
        $('#rating-div').raty({score: function() {return $(this).attr('data-score');},width: 150,target : '#review_hint',starOff : "{{url().'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url().'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url().'/assets/bsetec/static/img/star-half.png'}}"});
        $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url().'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url().'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url().'/assets/bsetec/static/img/star-half.png'}}"});

        // Rating 

                $('#review-save').click(function(){
                var title = $('.review-box #review-title').val();
                var description = $('.review-box #review-description').val();
                var rating = $('.review-box input[name=score]').val();
                var Course_id = $('.review-box #rating-courseId').val();
                if(title == ''){
                    $('.review-box #review-title').css('border','1px solid red');
                }
                else{
                    $('.review-box #review-title').css('border','1px solid #CCCCCC');
                    if(rating == 0){alert('Please Give Rating');}else{
                        $.ajax({
                        type: "POST",
                        url: "{{url('review')}}",
                        data: {title:title,description:description,rating:rating,Course_id:Course_id},
                        success: function (data) {
                            location.reload();
                        }
                        });
                    }
                }
                });
        // Rating

        // My Course

            $('#mycourseFilter').change(function(){
                $(this).parent('form').submit();
            });
        });

        //My Course

        // Tap

            $('#Course_Tabs a').click(function (e) {
              e.preventDefault()
              $(this).tab('show')
            });

        // Tap

        // Take Course

            eval(function(p,a,c,k,e,d){e=function(c){return c};if(!''.replace(/^/,String)){while(c--){d[c]=k[c]||c}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('$(0(){$(\'#1\').2()});',3,3,'function|dasky|Dasky'.split('|'),0,{}))
            $(".dsk-content .video-js").bind("ended", function() {
               $('.dsk-next').trigger('click');
            });

        // Take Course 

    function resetTabs(){
        $("#content_new > div,#content_news > div").hide(); //Hide all content
        $("#tabs_new a,#tabs_news a").attr("id",""); //Reset id's      
    }

    var myUrl = window.location.href; //get URL
    var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For localhost/tabs.html#tab2, myUrlTab = #tab2     
    var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

    (function(){
        $("#content_new > div,#content_news > div").hide(); // Initially hide all content
        $("#tabs_new li:first a,#tabs_news li:first a").attr("id","current"); // Activate first tab
        $("#content_new > div:first,#content_news > div:first").fadeIn(); // Show first tab content
        
        $("#tabs_new a,#tabs_news a").on("click",function(e) {
            e.preventDefault();
            if ($(this).attr("id") == "current"){ //detection for current tab
             return       
            }
            else{             
            resetTabs();
            $(this).attr("id","current"); // Activate this
            $($(this).attr('name')).fadeIn(); // Show content for current tab
            }
        });

        for (i = 1; i <= $("#tabs_new li,#tabs_news li").length; i++) {
          if (myUrlTab == myUrlTabName + i) {
              resetTabs();
              $("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
              $(myUrlTab).fadeIn(); // Show url tab content        
          }
        }
    })();
</script>
{{ HTML::script('assets/bsetec/static/js/jquery-1.9.1.min.js') }}
{{ HTML::script('assets/bsetec/static/js/front/owl.carousel.js') }}

<script>
    $(document).ready(function() {

      //Sort random function
      function random(owlSelector){
        owlSelector.children().sort(function(){
            return Math.round(Math.random()) - 0.5;
        }).each(function(){
          $(this).appendTo(owlSelector);
        });
      }

      $("#owl-demo").owlCarousel({
        navigation: true,
        navigationText: [
        "<i class='icon-chevron-left icon-white'></i>",
        "<i class='icon-chevron-right icon-white'></i>"
        ],
        //Call beforeInit callback, elem parameter point to $("#owl-demo")
        beforeInit : function(elem){
          random(elem);
        }

      });

    });
    </script>
@yield('extrafooter')

</body>
</html>