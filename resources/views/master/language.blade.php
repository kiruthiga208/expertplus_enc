<style type="text/css">
    .modal{
        display: none;
    }
    .textBoxLimit{
        color: black;
    }
</style>

<script type="text/javascript">
    function max_text(Get,limit) {
        Get.next('span').text( limit - Get.val().length );
    }
</script>

<a data-toggle="modal" href="#myModal">Select Language</a>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="align_space modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Language</h4>
            </div>
            <div class="modal-body">
                <?php
                $path = app_path().'/lang';
                $results = scandir($path);
                foreach ($results as $result) {
                    if ($result === '.' or $result === '..') continue;
                    if (is_dir($path . '/' . $result)) {
                        echo '<p> <a href="'.url('lang/'.$result).'">'.langDecode($result).'</a>';
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

@if(Auth::check()==true)
<div class="modal fade" id="CreatCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create a Course</h4>
            </div>
            <div class="modal-body">
            <span id="errormsg" class="errorcolor"></span> 
            {{ Form::open(array('url' => url('CreateCourses'),'onsubmit' => 'return CheckCourseTitle()')) }}
            <div class="form-group"> 
            <h5 style="color:black">What do you want to teach?:</h5>
            {{ Form::text('course_title','',array('class'=>'form-control reg_textbox','id' => 'GetCourseTitle','onkeypress' => 'max_text($(this),60)', 'maxlength' => '60' ))}}
            <span class="textBoxLimit">60</span>
            </div>
            {{ Form::submit('Create',array('class'=>'reg_next reg_add')) }} 
            {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function CheckCourseTitle() {
    var title = $('#GetCourseTitle').val();
	if(title==''){
		//alert('Please enter the course title');
        $('#errormsg').html('Please enter the course title');
		return false;
	} else if(title.length < 5) {
      //  alert('Course title should be greater than 4 letters');
      $('#errormsg').html('Course title should be greater than 4 letters');
        return false;
    }
    return true;
}
</script>
@endif


@if(Request::segment(1) == 'CreateCourses')

<div class="modal fade" id="PublisCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Publish This Course</h4>
            </div>
            <div class="modal-body">
            <ul id="publishcourseValidation" style="color:black">
                
            </ul>
            </div>
        </div>
    </div>
</div>

@endif

@if(Auth::check() == false)
<div class="modal fade login_width" id="LoginBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header header_bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">Joint today and start learning</h2>
            </div>
            <div class="modal-body rem_padd">
			
			 <div class="align_center">

            {{ Form::open(array('url' => url('login'))) }}

            <a href="{{ Facebook::loginUrl() }}" class="facebook_singup facebook">facebook login</a>

<div class="singup">
Don't have an account?

<a class="btn btn-primary facebook_singup facebook ud-popup" href="{{ url('registration') }}" style="display:inline; margin-left: 5px;"> Signup </a>
</div>
             </div>
            <!-- <div class="or">OR</div> -->
             
	    <div class="align_left">
            <div class="form-group"> 
            <label>{{ t('Username or Email address') }}</label>
            {{ Form::text('username','',array('class'=>'form-control reg_textbox','id'=>'username','placeholder'=>t('Username or Email address')))}}
            </div>
             
            <div class="form-group">
            <label>{{ t('Password') }}</label>  
            {{ Form::password('password',array('class'=>'form-control reg_textbox','id'=>'password','placeholder'=>t('Password'),'autocomplete'=>'off')) }}
            </div>

            <div class="form-group remove_bot">
            <label>{{ t('Remember Me') }}</label>
             {{ Form::checkbox('remember-me', 'value' , null, ['class' => 'login-checkbox'])}}
            </div>
            
            <a href="{{ url('password/remind') }}"> Forgot your password? </a>
              
            {{ Form::submit(t('Login'),array('class'=>'reg_next reg_add')) }} 

            {{ Form::close() }}


            </div> </div>
        </div>
		
		
    </div>
</div>
@endif
