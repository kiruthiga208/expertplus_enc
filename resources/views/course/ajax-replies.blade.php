@if(count($replys)>0)
@for($j=0;$j < count($replys);$j++)
<div class="discussion-comments-container" id="reply-block{!!$replys[$j]->reply_id!!}">
	<li class="">
		<div>
			<div class="comment-box">
				<div class="profile_image"><a href="{{ URL::to('profile/'.$replys[$j]->username) }}">
					{!! SiteHelpers::customavatar($replys[$j]->email,$replys[$j]->user_id) !!}</a>
				</div>

				<div class="activity-box">
					<div class="activity-header">
						<div class="header-right clearfix pull-right">
							<div class="hidden-btns">
								@if($replys[$j]->user_id==\Session::get('uid'))
								<a href="javascript:void(0)" data-rcommentid="{!!$replys[$j]->reply_id!!}" class="editreply activity-edit-link mini-act-btn mini-tooltip2">
									<i class="fa fa-pencil"></i>
								</a>
								<a href="javascript:void(0)"  data-rcommentid="{!!$replys[$j]->reply_id!!}" class="removereply activity-delete-link mini-act-btn mini-tooltip2">
									<i class="fa fa-trash-o"></i>
								</a>
								@endif
							</div>
						</div>
						<div class="header-left">
							<a class="activity-header-link" href="{{ URL::to('profile/'.$replys[$j]->username) }}">{{$replys[$j]->first_name}} {{$replys[$j]->last_name or ''}}</a>
							<time class="activity-time"> &middot; {!! SiteHelpers::changeFormat($replys[$j]->created_at) !!}</time>
							<!--span class="report-comment">
								<span class="activity-actions-separator">&middot;</span>
								<a class="ud-popup mini-tooltip2 pull-tooltip-left fs16" href="javascript:void(0)">
									<i class="fa fa-flag"></i>
								</a>
							</span-->
						</div>
					</div>
					<div class="comment-body w3c-default"><span id="replytext-{!!$replys[$j]->reply_id!!}"><?php echo $replys[$j]->reply_comment; ?> </span></div>
				</div>
			</div>
		</div>
	</li>
	
	<div style="display:none;" id="replyopenbox-{!!$replys[$j]->reply_id!!}">
		<input type="hidden" name="pid" value="{!!$replys[$j]->reply_id!!}">
		<div class="question-answers-creation">
			<div class="comment-creation">
				<div class="comment-submit-box">
					<div class="form-group">
						<textarea id="edittextboxcontent-{!!$replys[$j]->reply_id!!}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $replys[$j]->reply_comment; ?></textarea>
					</div>
					<div class="form-group">
						<button class="btn btn-orange btn-bg updatereplybtn" type="button" data-commentid="{!!$replys[$j]->lecture_comment_id!!}" data-replyid="{!!$replys[$j]->reply_id!!}">{!! Lang::get('core.update')!!}</button>
						<button class="btn btn-orange btn-bg editcancelbtn" data-commentid="{!!$replys[$j]->reply_id!!}" type="button">{!! Lang::get('core.cancel')!!}</button> 
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endfor

@endif

