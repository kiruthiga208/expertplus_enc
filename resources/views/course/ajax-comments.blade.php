@if(count($comments)>0)
@for($i=0;$i<count($comments);$i++)

<div class="media profile clearfix" id="comment-{{$comments[$i]->lecture_comment_id}}">

	<div class="activity-post">
		<div class="profile_image"><a href="{{ URL::to('profile/'.$comments[$i]->username) }}">
			{!! SiteHelpers::customavatar($comments[$i]->email,$comments[$i]->user_id) !!}</a>
		</div>
		<div class="activity-box discussion-activity-box">
			<div class="activity-header clearfix">
				<div class="header-right clearfix pull-right">
					<div class="hidden-btns">
						@if($comments[$i]->user_id==\Session::get('uid'))
						<a href="javascript:void(0);" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="editcomments"><i class="fa fa-pencil"></i></a>														
						<a href="javascript:void(0);" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="removecomments"><i class="fa fa-trash-o"></i></a>
						@endif
					</div>
				</div>
				<div class="header-left clearfix">
					<a href="{{ URL::to('profile/'.$comments[$i]->username) }}"><span class="activity-header-link"><span><strong>{{$comments[$i]->first_name}} {{$comments[$i]->last_name or ''}}</strong></span></span></a>
					<a>{!! Lang::get('core.post_discussion')!!}</a>
					@php $type = SiteHelpers::gettype($comments[$i]->lecture_id);@endphp
					<span class="activity-details-flex-wrapper"><a><span class=""><strong>@if($type == 0) {!! Lang::get('core.in_lecture') !!}  @else {!! Lang::get('core.in_quiz') !!} @endif {{ SiteHelpers::getcheckOrder($comments[$i]->course_id,$comments[$i]->lecture_id) }}</strong></span></a></span>
					<span class="activity-actions-separator"> &middot; </span>
					<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($comments[$i]->created_at) !!}</a></time>
				</div>
			</div>
			<div>
				<div class="">
					<div class="activity-body" id="wholewrapper-{{$comments[$i]->lecture_comment_id}}">
						<div class="activity-title" id="titletext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment_title !!}</div>
						<div class="activity-content w3c-default" id="bodytext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment !!}</div>
					</div>
					<div style="display:none;" id="commentopenbox-{{$comments[$i]->lecture_comment_id}}">
						<div class="form-group">
							<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
							<input type="text" name="posttitle" class="form-control" id="edittitle-{{$comments[$i]->lecture_comment_id}}" value="{{$comments[$i]->lecture_comment_title}}" placeholder="{!! Lang::get('core.start_discussion')!!}">
						</div>
						<div class="form-group">
							<textarea id="editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $comments[$i]->lecture_comment; ?></textarea> 
						</div> 
						<div class="form-group">              
							<button class="btn btn-orange btn-bg updatecommentbtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}" >{!! Lang::get('core.update')!!}</button>
							<button class="btn btn-orange btn-bg editccancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
						</div>
					</div>
					<div class="activity-actions clearfix">
						@if(Auth::check() == TRUE)
						<a href="javascript:void(0)">
							<span>
								<span><span class="text-info replybutton" id="openreplybox" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply')!!}</span></span>
							</span>
						</a>
						@endif
					<!--span class="activity-actions-separator"> &middot; </span>
					<span class="activity-likes-follows">
						<div class="activity-like" style="display:inline-block;">
							<a class="action-like">
								<i class="fa fa-thumbs-o-up"></i>
								<span><span class="">Like</span></span>
							</a>
						</div>
						<span class="activity-count like-count">
							<span><span class="">(1 like)</span></span>
						</span>
						<span class="activity-actions-separator"> &middot; </span>
						<div class="activity-follow" style="display:inline-block;">
							<a class="follow-question-btn"><span class="">Follow</span></a>
						</div>
						<span class="activity-count follow-count">
							<span><span class="">(1 follower)</span></span>
						</span>
					</span>
					<span class="report-comment">
						<span class="activity-actions-separator">&middot; </span>
						<a class="ud-popup mini-tooltip2 pull-tooltip-left fs16">
							<i class="fa fa-flag"></i>
						</a>
					</span-->
				</div>
				<div class="commentReplyBox" style="display:none;" id="openbox-{{$comments[$i]->lecture_comment_id}}">
					<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
					<div class="form-group">
						<textarea id="textboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"></textarea>
					</div>
					<div class="form-group">
						<button class="btn btn-orange btn-bg replybtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply')!!}</button>
						<button class="btn btn-orange btn-bg cancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
					</div>
				</div>
				@php ( $replys = SiteHelpers::getlecturereplies($comments[$i]->lecture_comment_id))
				<div class="discussion-comments-block">
				<div id="replyies-{{$comments[$i]->lecture_comment_id}}">
					@include('course/ajax-replies')
				</div>
				@php ($next_replies = SiteHelpers::getlecturereplies($comments[$i]->lecture_comment_id, 4) )
				@if(count($next_replies)>0)
					<div id="loadmoreajaxloader-{!! $replys[0]->lecture_comment_id !!}" style="display:none;"><img src="{{asset('assets/bsetec/images/ajax-loader.gif')}}"></div>
					<a class="btn btn-color load-more" data-id="{!! $replys[0]->lecture_comment_id !!}">{!! Lang::get('core.loade_more')!!}</a>
				@endif
				</div>
			</div>
		</div>
	</div>
</div>

</div>
@endfor
@else
<h4 align="center" class="nodiscussion ">{!! Lang::get('core.no_discussions') !!}</h4>
@endif



<div class="pull-right ajax-comments-pagination">
	@if(count($comments)>0)
		{!! str_replace('/?', '?' , $comments->render()) !!}
	@endif
</div>
