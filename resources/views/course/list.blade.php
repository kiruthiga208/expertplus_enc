@extends('layouts.frontend')
@section('content')


<!--slider-->
<div class="develpoment_this_sec categ_list">
    <div class="container">
        <div class="row">
          
            <!-- category sidebar start-->
            @include('course.category-sidebar')
            <!-- category sidebar end-->
            @if (defined('CNF_CURRENCY'))
                @php( $curreny = SiteHelpers::getCurrentcurrency(CNF_CURRENCY))
            @endif
            <div class="col-sm-9 trendieng search">   
            <div class="course-list-section clearfix" id="course-list-section">     
            <div class="mycourse_block">        
            <div class="leak_coursing clearfix">
                <div class="newest_coursing clearfix">
                    <div class="trebt_ot">
                        <h2 class="list_title">{!! $search !!}</h2>  
                    </div> 
                    
                </div>
                
                <div class="business_trend clearfix">
                    @if(count($course) ==0 )
                    @php ($sitename = \bsetecHelpers::get_options('sitename') )
                    <p class="empty_courses">{!! Lang::get('core.no_course') !!} </p>
                    <p class="discover_courses">
                        <a href="{{url('course')}}">{!! Lang::get('core.discover_course') !!}{!! $sitename['sitename'] !!}</a>
                    </p>
                    @endif
                    <!-- $course->user AND  -->
                    @php ($courses = $course)
                    @foreach($course as $course)
                    
                    <div class="col-xs-6 col-sm-3">
                     <div class="block_course clearfix">
                      <div class="business_office">
                           <div class="image"> <!-- <a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}"> <img id="image-3" src="{{ URL::to('bsetec/images/spacer.gif') }}" class="thumbs lazy" alt="{{{ $course->course_title }}}" data-src="{{ \bsetecHelpers::getImage($course->image) }}" data-original="{{ \bsetecHelpers::getImage($course->image) }}" />
                            </a> -->
                            <a href="{{ url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{!! $course->course_title !!}"><img id="image-3" class="thumbs lazy" src="{{ asset('assets/bsetec/images/spacer.gif') }}" data-src="{{ \bsetecHelpers::getImage($course->image) }}" data-original="{{ \bsetecHelpers::getImage($course->image) }}" alt="{!! $course->course_title !!}" style="background-image: url('{{ \bsetecHelpers::getImage($course->image) }}');"/></a>
                            </div>
                            
                         <div class="course_detail clearfix">
                            <h4><a href="{{url('courseview/'.$course->course_id.'/'.$course->slug) }}" title="{{$course->course_title}}"> {{ substr(ucfirst($course->course_title),0,30) }}</a></h4>
                            <p class="descrip"> {{ strip_tags($course->description) }}</p>
                            <div class="ratings">
                            <div class="star_rating">
                                <ul class="star_one clearfix">
                                    <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($course->course_id)}}"></li>
                                </ul>
                                @if(isset($course->ratings))
                                    ({{$course->ratings}})
                                @endif
                            </div>
                            </div>
                        @if(\Auth::check())      
                            @if(!\bsetecHelpers::checkmembership(Auth::user()->id,$course->course_id))
                            <p class="rate">
                                @if($course->pricing != 0)
                                 {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                                @else
                                {!! Lang::get('core.free')!!}
                                @endif
                            </p> 
                             @endif
                        @else
                             <p class="rate">
                                @if($course->pricing != 0)
                                 {!! SiteHelpers::getCurrencymethod($course->user_id,$course->pricing) !!}
                                @else
                                {!! Lang::get('core.free')!!}
                                @endif
                            </p> 
                        @endif
                        </div>
                        </div>
                        </div>
                        
                        
                        
                    </div>
                    
                    @endforeach
                </div>
            </div>
            
            <div class="pull-right"> 
                @if(count($courses)>0)
                    @if(isset($search_page))
                        @if($search_type == 'keyword')
                            {!! str_replace('?', '?q='.$search.'&', $courses->render()) !!}
                        @elseif($search_type == 'price')
                            {!! str_replace('?', '?price_from='.$_GET['price_from'].'&price_to='.$_GET['price_to'].'&', $courses->render()) !!}
                        @elseif($search_type == 'level')
                            {!! str_replace('?', '?level='.$_GET['level'].'&', $courses->render()) !!}
                             @endif
                    @else
                        {!! str_replace('/?', '?', $courses->render()) !!}
                    @endif
                @endif
             </div>
            </div>
            <script>
$(function(){
  $('body').removeClass();
  $('body').addClass('course_body');
  $('#front-header').addClass('front-header');	
  $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});
});
</script>

</div>
        </div>   
    </div>
</div>
</div>  
           
@stop
