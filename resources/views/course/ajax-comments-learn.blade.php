@if(count($comments)>0)
<div class="para_lack_hut" id="allcomments">
@for($i=0;$i < count($comments);$i++)
<div class="media profile clearfix" id="comment-{{$comments[$i]->lecture_comment_id}}">

	<div class="activity-post lecture-preview">
		<div class="profile_image">
        <a href="{{ URL::to('profile/'.$comments[$i]->username) }}">
			{!! SiteHelpers::customavatar($comments[$i]->email,$comments[$i]->user_id, 'small') !!}</a>
		</div>
		<div class="activity-box discussion-activity-box">
			<div  class="activity-header clearfix" action-icon="icon-comments" action-text="posted a discussion">
			<div class="header-right clearfix pull-right">
					<div class="hidden-btns">
						@if($comments[$i]->user_id==\Session::get('uid'))
						<a href="javascript:void(0);" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="pull-right removecomments"><i class="fa fa-trash-o"></i></a> &nbsp;&nbsp;&nbsp;
						<a href="javascript:void(0);" style="padding-right: 10px;" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="pull-right editcomments"><i class="fa fa-pencil"></i></a>
						@endif	
						
					</div>
					
				</div>
				<div class="header-left clearfix">
					<a href="{{ URL::to('profile/'.$comments[$i]->username) }}"><span class="activity-header-link"><span><strong>{{$comments[$i]->first_name}} {{$comments[$i]->last_name or ''}}</strong></span></span></a>
					<a>{!! Lang::get('core.post_command')!!}</a>
					<!-- <span class="activity-details-flex-wrapper"><a><span class=""><strong>in Lecture </strong></span></a></span> -->
					<span class="activity-actions-separator"> : </span>
					<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($comments[$i]->created_at) !!}</a></time>
				</div>
			</div>
			<div>
				<div class="">
					<div class="activity-body" id="wholewrapper-{{$comments[$i]->lecture_comment_id}}">
						<div class="activity-title text-primary titletext" id="titletext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment_title !!}</div>
						<div class="activity-content w3c-default profile_content" id="bodytext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment !!}</div>
					</div>

					<div style="display:none;" id="commentopenbox-{{$comments[$i]->lecture_comment_id}}">
						<div class="form-group">
							<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
							<input type="text" name="posttitle" class="form-control" id="edittitle-{{$comments[$i]->lecture_comment_id}}" value="{{$comments[$i]->lecture_comment_title}}" placeholder="Start a new discussion">
						</div>
						<div class="form-group">
							<textarea id="editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $comments[$i]->lecture_comment; ?></textarea> 
						</div> 
						<div class="form-group">              
							<button class="btn btn-orange updatecommentbtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}" >{!! Lang::get('core.sb_save')!!}</button>
							<button class="btn btn-danger editccancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.sb_cancel')!!}</button>
						</div>
					</div>

					@php ( $replys = SiteHelpers::getlecturereplies($comments[$i]->lecture_comment_id) )
					@if(count($replys)>0)
					<div class="activity-actions clearfix">
						<a href="javascript:void(0)" class="show-reply">
							<span>
								<span><span class="text-primary">{!! Lang::get('core.show')!!} {!! count($replys)!!} {!! Lang::get('core.replies') !!}</span></span>
								
							</span>
						</a>
					</div>
					@endif
					@if(Auth::check() == TRUE)
						<a class="replybutton" id="openreplybox" data-commentid="{{$comments[$i]->lecture_comment_id}}" href="javascript::"> {!! Lang::get('core.Reply') !!}</a>
					@endif

					<div class="commentReplyBox" style="display:none;" id="openbox-{{$comments[$i]->lecture_comment_id}}">
						<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
						<textarea id="textboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor textboxcontent-{{$comments[$i]->lecture_comment_id}}" rows="2" placeholder="Comment" name="comment" cols="50"></textarea>                
						<button class="btn btn-orange replybtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply') !!}</button>
						<button class="btn btn-danger cancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.sb_cancel') !!}</button>
					</div>
					
					<div class="discussion-comments-block" style="display:none;" id="repc-{{$comments[$i]->lecture_comment_id}}">
						<div class="discussion-comments-container-dark" id="repcd-{{$comments[$i]->lecture_comment_id}}">
							@if(count($replys)>0)

							@for($j=0;$j < count($replys);$j++)
							<li id="reply-block{!!$replys[$j]->reply_id!!}">
								<div>
									<div class="comment-box">

										<span class="thumb-wrapper">
										<div class="profile_image">
										<a href="{{ URL::to('profile/'.$replys[$j]->username) }}">
										{!! SiteHelpers::customavatar($replys[$j]->email,$replys[$j]->user_id,'small') !!}
										</a>
										</div>
										</span>

										<div class="activity-box">
											<div class="activity-header">
												<div class="header-right clearfix pull-right">
													@if($replys[$j]->user_id==\Session::get('uid'))
													<div class="hidden-btns">
														<a href="javascript:void(0)" class="activity-edit-link mini-act-btn mini-tooltip2 editreply" data-rcommentid="{!!$replys[$j]->reply_id!!}">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="javascript:void(0)" class="activity-delete-link mini-act-btn mini-tooltip2 removereply" data-rcommentid="{!!$replys[$j]->reply_id!!}">
															<i class="fa fa-trash-o"></i>
														</a>
													</div>
													@endif
												</div>
												<div class="header-left">
													<a class="activity-header-link" href="{{ URL::to('profile/'.$replys[$j]->username) }}">{{$replys[$j]->first_name}} {{$replys[$j]->last_name or ''}}</a>
													<time class="activity-time"> &middot; {!! SiteHelpers::changeFormat($replys[$j]->created_at) !!}</time>
												</div>
											</div>
											<div class="comment-body w3c-default" id="replytext-{!!$replys[$j]->reply_id!!}"><p><?php echo $replys[$j]->reply_comment; ?></p></div>
											<div style="display:none;" id="replyopenbox-{!!$replys[$j]->reply_id!!}">
												<input type="hidden" name="pid" value="{!!$replys[$j]->reply_id!!}">
												<textarea id="edittextboxcontent-{!!$replys[$j]->reply_id!!}" class="form-control lectureeditor edittextboxcontent-{!!$replys[$j]->reply_id!!}" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $replys[$j]->reply_comment; ?></textarea>                
												<button class="btn btn-orange updatereplybtn" type="button" data-commentid="{!!$replys[$j]->lecture_comment_id!!}" data-replyid="{!!$replys[$j]->reply_id!!}">{!! Lang::get('core.sb_save') !!}</button>
												<button class="btn btn-danger editcancelbtn" data-commentid="{!!$replys[$j]->reply_id!!}" type="button">{!! Lang::get('core.sb_cancel') !!}</button>
											</div>
										</div>
									</div>
								</div>
							</li>
							@endfor
							@endif
							
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

</div>
	@endfor
    </div>
 @endif


 <div class="pull-right ajax-comments-pagination">
	@if(count($comments)>0)
		{!! str_replace('/?', '?' , $comments->render()) !!}
	@endif
</div>