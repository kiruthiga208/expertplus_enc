<div class="span3">
    @include('master/share')
    <div class="clearfix">
        <h4>
            <span>{{ t('Featured Image') }}</span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            @foreach(getFeaturedImage() as $featuredImage)
                <a href="{{ url('image/'.$featuredImage->id.'/'.$featuredImage->slug) }}" class="pull-left userimage">
                    <img src="{{ asset(zoomCrop('uploads/'.$featuredImage->image_name.'.' . $featuredImage->type,223,223)) }}"
                         alt="{{ $featuredImage->title }}" class="thumbnail">
                </a>
            @endforeach
        </div>

    </div>

    @if(getFeaturedUser()->count() >= 1)
    <hr>
    <div class="clearfix">
        <h4>
            <span>{{ t('Featured User') }}</span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            @foreach(getFeaturedUser() as $featuredUser)
            <div class="col-md-12">
                <div class="row">

                        <a href="{{ url('user/'.$featuredUser->username) }}" class="thumbnail pull-left">
                            <img src="{{ $featuredUser->avatar }}" alt="{{ $featuredUser->fullname }}">
                        </a>

                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p><strong><a href="{{ url('user/'.$featuredUser->username) }}">{{ $featuredUser->fullname }}</a></strong></p>
                        @if(Auth::check())
                        @if(checkFollow($featuredUser->id))
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">{{ t('Un-Follow') }}</button>
                        @else
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">{{ t('Follow-Me') }}</button>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif
    <hr>

    <div class="clearfix">
        <h4>
            <span>{{ t('More From') }} {{ siteSettings('siteName') }}</span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            @foreach(moreFromSite() as $sidebarImage)
                <a href="{{ url('image/'.$sidebarImage->id.'/'.$sidebarImage->slug) }}" class="pull-left userimage">
                    <img src="{{ asset('uploads/'.$sidebarImage->image_name) }}"
                         alt="{{ $sidebarImage->title }}" class="thumbnail">
                </a>
            @endforeach
        </div>
    </div>
</div>