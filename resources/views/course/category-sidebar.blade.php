<link href="{{ asset('assets/bsetec/css/category.css') }}" rel="stylesheet">
 @php ( $sitecategories = \bsetecHelpers::sitecategories() )
<div class="col-sm-3 multi_development">
                <div class="sidebar_left more_att clearfix">
                    <div class="categories_block extra_ort">
                        <h4> {!! Lang::get('core.Categories') !!}</h4>
                        <!-- <ul class="mang_develop clearfix "> -->
                         <ul class="mang_develop clearfix dropdown-menu" role="menu" aria-labelledby="dropdownMenu" style="display: block; position: static; margin-bottom: 5px;">

                            @foreach($sitecategories as $category)
                            @php ($results = \bsetecHelpers::sitesubcategories($category->id))
                            @if(count($results)>0)
                              @php ($cls = 'dropdown-submenu')
                            @else 
                              @php ($cls='')
                            @endif
                            @if(Request::segment(2) == $category->slug)
                            <li class="{!! $cls !!}"><a href="{{ url('category/'.$category->slug)}}"><span class="category-name"><i class="fa fa-angle-right"></i>{{$category->name}}</span><span class="category-count">( {{\bsetecHelpers::CourseCount($category->id) }} )</span></a>

                                @if(count($results)>0)
                                <ul class="dropdown-menu cat">
                                    @foreach($results as $subcats)
                                       <li><a href="{{ url('category/'.$category->slug.'/'.$subcats->sub_slug)}}">{!!  $subcats->sub_name  !!}</a></li>
                                       @endforeach
                                </ul>
                                @endif
                            </li>
                            @else
                            <li class="{!! $cls !!}"><a href="{{ url('category/'.$category->slug)}}"><span class="category-name"><i class="fa fa-angle-right"></i>{{ $category->name }}</span><span class="category-count">( {{\bsetecHelpers::CourseCount($category->id) }} )</span></a>
                                @if(count($results)>0)
                                <ul class="dropdown-menu cat">
                                      @foreach($results as $subcats)
                                       <li><a href="{{ url('category/'.$category->slug.'/'.$subcats->sub_slug)}}">{!!  $subcats->sub_name  !!}</a></li>
                                       @endforeach
                                </ul>
                                @endif
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="categories_block">
                        <h4>{!! Lang::get('core.price') !!}</h4>
                        <ul class="mang_develop clearfix">
                         @if (defined('CNF_CURRENCY'))
                         <?php $curreny = SiteHelpers::getCurrentcurrency(CNF_CURRENCY); ?>
                        @endif
                            <?php
                                $prices = array('price_from=0&price_to=0' => Lang::get('core.free'),
                                'price_from=1&price_to=49' => Lang::get('core.less_than').' '. $curreny. ' 50',
                                'price_from=50&price_to=99' => $curreny.' 50 - '.$curreny.' 99',
                                'price_from=100&price_to=199' => $curreny.' 100 - '.$curreny.' 199' ,
                                'price_from=200&price_to=299' => $curreny.' 200 - '.$curreny.' 299',
                                'price_from=400&price_to=499' => $curreny.' 400 - '.$curreny.' 499',
                                'price_from=500&price_to=10000' => Lang::get('core.more_than').' '.$curreny.' 500');
                            ?>

                            @foreach($prices as $key => $price)
                            @if(Request::segment(1) == $key )
                            {{ $style = '' }}
                            @else
                            {{ $style = '' }}
                            @endif
                            <li>
                                <a href="{{url('search?'.$key)}}"><span class="category-name"><i class="fa fa-angle-right"></i>{{$price}}</span> <span class="category-count">( {{\bsetecHelpers::CourseCountPrice($key) }} )</span></a>

                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="categories_block">
                        <h4>{!! Lang::get('core.level') !!}</h4>
                        <ul class="mang_develop clearfix">
                          @php ( $courseLevel = array('level=4' => Lang::get('core.appropriate_all'),
                             'level=1' => Lang::get('core.beginner'),
                             'level=2' => Lang::get('core.Intermediate'),
                             'level=3' => Lang::get('core.Advanced')) )

                            @foreach($courseLevel as $key => $level) 
                            @if(Request::segment(1) == $key )
                            {{ $style = '' }}
                            @else
                            {{ $style = '' }}
                            @endif
                            <li>
                                <a {{$style}} href="{{url('search?'.$key)}}"><span class="category-name"><i class="fa fa-angle-right"></i>{{$level}}</span><span class="category-count">( {{\bsetecHelpers::CourseCountLevel($key) }} )</span></a>
                                
                            </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="categories_block">
                        <h4>{!! Lang::get('core.sort_by') !!}</h4>
                        <ul class="mang_develop clearfix">
                            @php ( $specials = array('featured' => Lang::get('core.feature'),'freeCourses' => Lang::get('core.free_courses'), 'topRated' => Lang::get('core.top_rating'), 'topFree' => Lang::get('core.top_free') , 'topPaid' => Lang::get('core.top_paid'), 'mostViewed' => Lang::get('core.most_viewed') )  )

                            @foreach($specials as $key => $special)
                            @if(Request::segment(1) == $key )
                            {{ $style = '' }}
                            @else
                            {{ $style = '' }}
                            @endif
                            <li><a {{$style}} href="{{url($key)}}"><span class="category-name"><i class="fa fa-angle-right"></i>{{$special}}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $('.cat li').on('click',function(){
         $('.cat').css('display','none');
    });
    $('.extra_ort ul li').on('mouseover',function(){
        $(this).children('ul').css('display','block');
    });
    $('.extra_ort ul li').on('mouseleave',function(){
        $(this).children('ul').css('display','none');
    });
</script>