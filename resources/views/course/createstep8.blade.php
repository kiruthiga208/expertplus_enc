@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course dangerzone">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				@include('course.courseheader')
			</div>
			<div class="col col-sm-3 multi_development">
				@include('course.createsidebar')
			</div>
			<div class="col col-sm-9">

				<div class="lach_dev resp-tab-content course_tab"> 
					<div class="slider_divsblocks">
						<div>
							{!! Form::open(array('url'=>'createcourse', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
							{!! Form::hidden('course_id', $course->course_id )  !!}
							{!! Form::hidden('step', $step )  !!}
							{!! Form::hidden('courseunpulish', URL::to('course/unpublish') )  !!}
							<div class="col_space">


								<label for="description"><h2> {!! Lang::get('core.Danger_Zone')!!}</h2></label>
								<p>{!! Lang::get('core.danger_subtitle')!!}</p>
								
							
							<script>
							function un()
							{
								//alert("unpublished");
							}
							</script>
							<div class="danger-zone">
								 <h4>{!! Lang::get('core.unpulish_course')!!}</h4>

									<p>{!! Lang::get('core.unpublish_text')!!}</p>
									<?php if($course->approved==1) { ?>
									<a href="javascript:void(0);" class="btn btn-warning" id="unpublish" data-cid="{!! $course->course_id or '' !!}">{!! Lang::get('core.Unpublish')!!}</a>
									<?php } ?>
								<h4>{!! Lang::get('core.remove_course')!!}</h4>
									
									<p> {!! Lang::get('core.remove_text')!!} </p>

								<a href="{!! URL::to('deleteCourses/'.$course->course_id) !!}" onclick="return areyousure();" class="btn btn-danger">{!! Lang::get('core.btn_remove')!!}</a>
							</div>
						</div>
						{!! Form::close() !!}

					</div>
				</div>
			</div> 
			
		</div>
		
	</div>
</div>
</div>
<script>
$(function(){

	$('.toggle-price-form').click(function () {
		$('#pricetab').slideToggle();
	});

	$('.set_price_free').click(function () {
		$('.price_text').val('');
	});

	$(document).on('click','#unpublish',function(){
		var cid = $(this).data('cid');
		var courseunpulish =$('[name="courseunpulish"]').val();
		var r = confirm("{!! Lang::get('core.unpublish_confirm') !!}");
		if (r === true) {
			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax ({
				type: "POST",
				url: courseunpulish,
				data: "courseid="+cid,
				beforeSend: function() {
					$('.unpublisloader').html('<h4><i class="fa fa-refresh fa-spin"></i> {!! Lang::get("core.loading") !!}</h4>');
				},
				success: function (msg){
					var return_data = $.parseJSON(msg);
					if(return_data.status=='0'){
						toastr.success("Success", "{!! Lang::get('core.course_success')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
						window.location.reload();
					}else if(return_data.status=='1'){

						toastr.error("Error", "{!! Lang::get('core.course_failure')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}
					return false;
				},
				complete: function() {
					$('.unpublisloader').html("");
				}
			});
}

});

});
function areyousure()
{
	return confirm('{!! Lang::get("core.course_delete_confirm")!!}');
}

</script>
@stop