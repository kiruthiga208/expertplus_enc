@extends('layouts.frontend')
@section('content')


<div class="bg_container course_breadcrumblocks course_viewsblocks">

</div>

<!--slider-->
<div class="develpoment_this_sec categ_list">
    <div class="container">
        <div class="row">

            <!-- category sidebar start-->
            @include('course.category-sidebar')
            <!-- category sidebar end-->

       <div class="col-sm-9 trendieng">                    
        <div class="course-list-section clearfix"  id="course-list-section">

        <!-- trending course start -->
        @if(count($trending_course)>0)
        <div class="block_course clearfix">
            <h2 class="list_title">{!! Lang::get('core.trending') !!}</h2>
            <div class="view_block"><a href="{{ URL::to('mostViewed') }}">{!! Lang::get('core.view_all') !!} <i class="fa fa-plus-circle"></i></a></div>       

            <div id="owl-demo3" class="owl-carousel clearfix">            
           @foreach($trending_course as $trend_course)
            <div class="item">
               <div class="image">
               <a href="{{ url('courseview/'.$trend_course->course_id.'/'.$trend_course->slug) }}" title="{!! $trend_course->course_title !!}"><img class="lazy" src="{{ asset('assets/bsetec/images/spacer.gif') }}" data-src="{{ \bsetecHelpers::getImage($trend_course->image) }}" data-original="{{ \bsetecHelpers::getImage($trend_course->image) }}" alt="{!! $trend_course->course_title !!}" style="background-image: url('{{ \bsetecHelpers::getImage($trend_course->image) }}');"/></a>
               </div>
                <div class="course_detail clearfix">
                <h4><a href="{{ url('courseview/'.$trend_course->course_id.'/'.$trend_course->slug) }}" title="{!! $trend_course->course_title !!}">{!! substr(ucfirst($trend_course->course_title),0,30) !!}</a></h4>
                <p class="description">{!! substr(ucfirst(strip_tags($trend_course->description)),0,50) !!}</p>
                <div class="ratings">
                    <div class="star_rating">
                        <ul class="star_one clearfix">
                            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($trend_course->course_id)}}"></li>
                        </ul>
                    </div>
                </div>
                <p class="rate">
                @if($trend_course->pricing != 0)
                    {!! SiteHelpers::getCurrencymethod($trend_course->user_id,$trend_course->pricing) !!}
                @else
                {!! Lang::get('core.free')!!}
                @endif
                </p>
                </div>
            </div>
           @endforeach 
            </div>
       </div>
       @endif
    <!-- trending course end -->

        <!-- new and noteworthy start -->  
        @if(count($latest_course)>0)  
        <div class="block_course clearfix">
            <h2 class="list_title">{!! Lang::get('core.new') !!}</h2>
            <div class="view_block"><a href="{{ URL::to('latestcourse') }}">{!! Lang::get('core.view_all') !!} <i class="fa fa-plus-circle"></i></a></div>       

            <div id="owl-demo3" class="owl-carousel clearfix">            
            @foreach($latest_course as $late_course)
            <div class="item">
               <div class="image"><a href="{{ url('courseview/'.$late_course->course_id.'/'.$late_course->slug) }}" title="{!! $late_course->course_title !!}"><img class="lazy" src="{{ asset('assets/bsetec/images/spacer.gif') }}" data-src="{{ \bsetecHelpers::getImage($late_course->image) }}" data-original="{{ \bsetecHelpers::getImage($late_course->image) }}" alt="{{{ $late_course->course_title }}}" style="background-image: url('{{ \bsetecHelpers::getImage($late_course->image) }}');"/></a></div>
                <div class="course_detail clearfix">
                <h4><a href="{{ url('courseview/'.$late_course->course_id.'/'.$late_course->slug) }}" title="{!! $late_course->course_title !!}">{!! substr(ucfirst($late_course->course_title),0,30) !!}</a></h4>
                <p class="description">{!! substr(ucfirst(strip_tags($late_course->description)),0,50) !!}</p>
                <div class="ratings">
                    <div class="star_rating">
                        <ul class="star_one clearfix">
                            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($late_course->course_id)}}"></li>
                        </ul>
                    </div>
                </div>
                <p class="rate">
                @if($late_course->pricing != 0)
                     {!! SiteHelpers::getCurrencymethod($late_course->user_id,$late_course->pricing) !!}
                @else
                {!! Lang::get('core.free')!!}
                @endif
                </p>
                </div>
            </div>
           @endforeach 
            </div>
            </div>
        @endif
            <!-- new and noteworthy end -->  

        <!-- staff picks start --> 
        @if(count($staff_picks_course)>0)
        <div class="block_course clearfix">
            <h2 class="list_title">{!! Lang::get('core.staff') !!}</h2>
            <div class="view_block"><a href="{{ URL::to('topRated') }}">{!! Lang::get('core.view_all') !!} <i class="fa fa-plus-circle"></i></a></div>       

            <div id="owl-demo3" class="owl-carousel clearfix">            
            @foreach($staff_picks_course as $staff_course)
            <div class="item">
               <div class="image"><a href="{{ url('courseview/'.$staff_course->course_id.'/'.$staff_course->slug) }}"  title="{!! $staff_course->course_title !!}"><img class="lazy" src="{{ asset('assets/bsetec/images/spacer.gif') }}" data-src="{{ \bsetecHelpers::getImage($staff_course->image) }}" data-original="{{ \bsetecHelpers::getImage($staff_course->image) }}" alt="{{{ $staff_course->course_title }}}" style="background-image: url('{{ \bsetecHelpers::getImage($staff_course->image) }}');" /></a></div>
                <div class="course_detail clearfix">
                <h4><a href="{{ url('courseview/'.$staff_course->course_id.'/'.$staff_course->slug) }}" title="{!! $staff_course->course_title !!}">{!! substr(ucfirst($staff_course->course_title),0,30) !!}</a></h4>
                <p class="description">{!! substr(ucfirst(strip_tags($staff_course->description)),0,50) !!}</p>
                <div class="ratings">
                    <div class="star_rating">
                        <ul class="star_one clearfix">
                            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($staff_course->course_id)}}"></li>
                        </ul>
                    </div>
                </div>
                <p class="rate">
                @if($staff_course->pricing != 0)
                   {!! SiteHelpers::getCurrencymethod($staff_course->user_id,$staff_course->pricing) !!}
                @else
                {!! Lang::get('core.free')!!}
                @endif
                </p>
                </div>
            </div>
           @endforeach 
            </div>
            </div>
        @endif
        <!-- staff picks end -->  

        <!-- digital marketing start -->
        @if(count($digital_marketing_course)>0)    
        <div class="block_course clearfix">
            <h2 class="list_title">{!! Lang::get('core.digital') !!}</h2>
            <div class="view_block"><a href="{{ URL::to('takencourse') }}">{!! Lang::get('core.view_all') !!} <i class="fa fa-plus-circle"></i></a></div>       

            <div id="owl-demo3" class="owl-carousel clearfix">            
            @foreach($digital_marketing_course as $digital_course)
            <div class="item">
               <div class="image"><a href="{{ url('courseview/'.$digital_course->course_id.'/'.$digital_course->slug) }}" title="{!! $digital_course->course_title !!}"><img class="lazy" src="{{ asset('assets/bsetec/images/spacer.gif') }}" alt="{{{ $digital_course->course_title }}}" data-src="{{ \bsetecHelpers::getImage($digital_course->image) }}" data-original="{{ \bsetecHelpers::getImage($digital_course->image) }}" style="background-image: url('{{ \bsetecHelpers::getImage($digital_course->image) }}');"/></a></div>
                <div class="course_detail clearfix">
                <h4><a href="{{ url('courseview/'.$digital_course->course_id.'/'.$digital_course->slug) }}" title="{!! $digital_course->course_title !!}">{!! substr(ucfirst($digital_course->course_title),0,30) !!}</a></h4>
                <p class="description">{!! substr(ucfirst(strip_tags($digital_course->description)),0,50) !!}</p>
                <div class="ratings">
                    <div class="star_rating">
                        <ul class="star_one clearfix">
                            <li class="late_star ReadonlyRating startvalue" data-score="{{\bsetecHelpers::checkReview($digital_course->course_id)}}"></li>
                        </ul>
                    </div>
                </div>
                <p class="rate">
                @if($digital_course->pricing != 0)
                     {!! SiteHelpers::getCurrencymethod($digital_course->user_id,$digital_course->pricing) !!}
                @else
                {!! Lang::get('core.free')!!}
                @endif
                </p>
                </div>
            </div>
           @endforeach 
            </div>
            </div>
        @endif
        <!-- digital marketing end -->  
            </div>
        </div>   
    </div>
</div>
</div>  
<script>
$(function(){
	$('body').removeClass();
	$('body').addClass('course_body');
	$('#front-header').addClass('front-header');	
      var owl = $("#owl-demo3,#owl-demo4");
      owl.owlCarousel({        
        itemsCustom : [
		  [0, 1],
          [350, 2],
          [450, 2],
          [600, 3],
          [700, 3],
          [1000, 3],
          [1200, 4],
      ],
        navigation : true
      });
  $('.ReadonlyRating').raty({score: function() {return $(this).attr('data-score');},readOnly: true,starOff : "{{url('/').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('/').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('/').'/assets/bsetec/static/img/star-half.png'}}"});
});
</script>           
@stop

