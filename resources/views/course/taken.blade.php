@extends('master/index')

@section('content')

         <style>
.nav-bar.learn-course-header li {
  float: left;
  font-size: 14px;
}
.nav-bar.learn-course-header li a{
  color: white;
}

.btn_mess {padding: 3px 8px;}

.replybutton.reply_top{clear: both;}

.hr_margin {margin: 0 0 15px;}

</style>

<div class="course-header-image" style="margin-top: 13px;background-image: url('{{ asset(zoomCrop('uploads/course/'.$course->image,1170,100)) }}')">
         <div class="banners_colors" style="background: none repeat scroll 0 0 rgba(0, 0, 0, 0.5);">
                <div class="container">
                  <div class="row-fluid">
                      <div class="span12 align_height">
                        <div class="learn-course-title test">{{$course->course_title}}
                            <ul class="nav-bar learn-course-header">
                                  <li class="active">
                            <a href="{{url('user').'/'.$course->user->username}}">
                            <img class="media-object imgfix" alt="{{ $course->user->fullname }}" src="{{ avatar($course->user->avatar, 32,30) }}">
                            {{$course->user->fullname }}</a> </li>
                                  <li class="dropdown" id="accountmenu">
                                      <a class="dropdown-toggle icon-sharereviewss" data-toggle="dropdown" href="#"><i class="fa fa-dot-circle-o"></i></a>
                                      <ul class="dropdown-menu text_width">
                                    <li class="text_color"><a href="https://twitter.com/intent/tweet?url={{ Request::url() }}" target="_blank">{!! Lang::get('core.t') !!}</a></li>
                                    <li class="text_color"><a href="http://www.facebook.com/sharer/sharer.php?u={{ Request::url() }}" target="_blank">{!! Lang::get('core.f') !!}</a></li>
                                    <li class="text_color"><a href="https://plusone.google.com/_/+1/confirm?hl=en&url={{ Request::url() }}" target="_blank">{!! Lang::get('core.g') !!}</a></li>
                                      </ul>
                                  </li>
                                  <li><a data-toggle="modal" href="#reviewDiv">{!! Lang::get('core.rating_review') !!}</a></li>
                              </ul>
                              <div class="modal fade" id="reviewDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="border:none;">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header header_bg">
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h2 class="modal-title"> {!! Lang::get('core.rate_review_course') !!}:</h2>
                                      </div>
                                      @if(Auth::check())
                                      <div class="modal-body review-box move_inner">
                                          <div class="form-group clearfix">
                                          <div class="one_review_com">
                                          <label class="choose_color">{!! Lang::get('core.course_rate') !!}:</label></div>
                                          <div id="rating-div"  data-score="{{checkReview($course->course_id,Auth::user()->id)}}" class="span5 choose_star"></div>
                                          <div id="review_hint"></div>
                            </div>
                            <div class="form-group clearfix"> 
                            @php ($GetReview = explode('--Review--',getReview($course->course_id,Auth::user()->id)) )
                           <div class="add_opt">{!! Lang::get('core.r_title') !!}:</div>
                    <div class="opt_add"><input type="text" class="reg_textbox textbox_width" value="{{$GetReview[0]}}" id="review-title" placeholder="Please enter your review title here"  /> </div>
                                    <input type="hidden" id="rating-courseId" value="{{$course->course_id}}">
                                  </div>
                                  <div class="form-group clearfix"> 
                       <div class="less_opt">{!! Lang::get('core.r_description') !!}:</div>
    <div class="opt_less"><textarea placeholder="Please enter your review here" id="review-description" class="text_width">{{$GetReview[1]}}</textarea></div>         
                                  </div>
 <div class="ol_pt"><input class="reg_next reg_add text_width" id="review-save" type="button" value="{!! Lang::get('core.r_update') !!}"></div>
                                      </div>
                                      @endif
                          
                                      
                                </div>
                              </div>
                              </div>
                      </div>
                      </div>
                  </div>
              </div>
         </div>
         
        </div>


<div class="bg_container">
 <ul class="slider_menu container">
<li><a href="{{url()}}" class="blog_color">{!! Lang::get('core.Home') !!}</a></li>
 <li><span><i class="fa fa-angle-right new_icons"></i>{{$course->course_title}}</span></li>
 </ul>
 </div>
 
<div class="description">

<!--<div class="container">-->
<div class="row-fluid">
<div class="span12">

    <div class="span8 curr course_progressbars">
    <!-- <span class="section_txtcolor progress_font"> <p> You have not started any of 57 lectures </p>   </span> -->
    @php $completion = explode('--X--',CompletedPercentage($course->course_id)); @endphp
   @php $total = json_decode($course->curriculum); @endphp


    @if($total) 

    <div class="progress progress-striped active" style="width:96%">    

   @php ($tot_count = 1 )

    @for ($tot=0; $tot < count($total->MainTitle); $tot++)
      @if($total->MainTitle[$tot] != 'Section')
        @php $couting = $tot_count++; @endphp
      @endif
    @endfor

   

    @if(count($completion) == 1)
      @php ( $completionCount = 0 )
    @else
     @php ( $completionCount = count($completion))
    @endif

    @php ( $percentage = round(($completionCount / $couting ) * 100) )

        <div class="bar" style="width: {{$percentage}}%;"> {{$percentage}}% </div>
    </div>
            @if($percentage == '100')
            <div class="course_completionbtns clearfix">
			      <a class="btn move_right tooltip" onmouseover="tooltip.pop(this, 'Congratulations!! you have finished this course, <br/> Click to access your certificate!')" target="_blank" href="{{url('certification-course/'.$course->course_id.'/'.$course->slug)}}">            
            <i class="fa fa-trophy" style="margin-top:20px"></i></a>
            <!-- <div class="progress progress-striped active emptybars">
                    <div style="width: 100%;" class="bar"> 100% </div>
            </div> -->
            </div>
            @else
            <div class="course_completionbtns clearfix">
            <a class="btn move_right tooltip" onmouseover="tooltip.pop(this, 'You Need to Finish this Course to get your <br/> Certificate of Completion!')" href="#">            
            <i style="color: #D4D4D4; border:none;margin-top:20px" class="fa fa-trophy"></i></a>
            <!-- <div class="progress progress-striped active emptybars">
            <div class="bar"></div>
            </div> -->
            </div>
            @endif

       <div class="outer_padd new_outer_padd">

        <div class="curr_head clearfix"><div class="curr_title des_color">{!! Lang::get('core.Curriculum') !!}</div></div>


          <ul id="curriculum_ul" class="sortable_ul scroll">

                <?php 
                $curriculum = json_decode($course->curriculum);
                $curri_count = @count($curriculum->MainTitle); 
                ?>
                    @for ($j=0; $j < $curri_count; $j++)

                        @if($curriculum->MainTitle[$j] == 'Section')
                        <?php $class = 'main_li_curriculum';?>
                        @else
                        <?php $class = 'ul_newpadd sub_li_curriculum change_bg';?>
                        @endif

                    <li class="ui-state-default curriculum_ul_li change_bg  {{$class}}"> 

                     

                    @if($curriculum->MainTitle[$j] == 'Lecture')
                    <div>
                    <?php $file_count = count($curriculum->file_type[$j]); ?>

                            @for($k=0; $k < $file_count; $k++)
                            @if($curriculum->file_type[$j][$k] != '')
                            <div class="pull-left"> 
                             @if (strpos(CompletedPercentage($course->course_id),$curriculum->file_name[$j][$k]))
                                        <i class="fa fa-circle font_iconspace" style="color:green"></i>
                                    @else
                                        <i class="fa fa-circle font_iconspace" style="color:#808080"></i>
                                    @endif
                            </div>
                            <a href="{{url('learn-course/'.$course->course_id.'/'.$course->slug.'#/step-'.($j+1))}}"></a>
                            @endif
                            @endfor
                    </div>
                    @elseif($curriculum->MainTitle[$j] == 'Quiz')
                    <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv">
                        {{count($curriculum->file_type[$j])}} {{$curriculum->MainTitle[$j]}}
                        
                    </div>
                    @endif
          
          @if($curriculum->MainTitle[$j] == 'Section')
                        @php ( $count = 1 )
                        <span class="main_title main_padd main_font">{{$curriculum->MainTitle[$j]}}  : </span>
                        <span><h4> {{$curriculum->title[$j]}}</h4></span>
                        @else
                         
                         <a href="{{url('learn-course/'.$course->course_id.'/'.$course->slug.'#/step-'.($j+1))}}">
                        @php  ( $count++ )
                        <span class="col-lg-10"><h4 class="lecture"> {{$curriculum->title[$j]}}</h4></span>
                         <span class="lecture_time col-lg-2">
                         <?php $file_count = count($curriculum->file_type[$j]); ?>
                          @for($k=0; $k < $file_count; $k++)
                          @if($curriculum->file_type[$j][$k] != '')
                              <?php $shownfile = public_path()."/uploads/videos/".$curriculum->file_name[$j][$k]; ?>
                              @if(file_exists($shownfile))
                              @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                                  <i class="audio_icon icon-headphones"></i>
                                  <?php
                                  $getID3 = new getID3;
                                  $file = $getID3->analyze($shownfile);
                                  if($file) { $duration = ($file['playtime_string'])?$file['playtime_string']:'00:00'; } else { $duration = '00:00';}
                                  echo '<span class="space_color">'.$duration.'</span>';
                                  ?>
                              @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                                  <!--<i class="fa fa-play-circle-o icon_fonts icon-color"></i>-->
                                  <?php
                                  $getID3 = new getID3;
                                  $file = $getID3->analyze($shownfile);
                                  if($file) { $duration = ($file['playtime_string'])?$file['playtime_string']:'00:00'; } else { $duration = '00:00';}
                                  echo '<span class="space_color">'.$duration.'</span>';
                                  ?>
                              @elseif($curriculum->file_type[$j][$k] == 'application/pdf')
                                  <i class="text_icon icon-file"></i>
                                  <?php
                                    exec("identify -format %n $shownfile",$totpages1);
                                    if(!empty($totpages1)) { echo '<span class="space_color">'.$totpages1[0].' Page(s)</span>'; }
                                  ?>
                              @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                                  <i class="text_icon icon-file"></i>
                                  <?php
                                    exec("identify -format %n $shownfile",$totpages);
                                    if(!empty($totpages)) { echo '<span class="space_color">'.$totpages[0].' Page(s)</span>'; }
                                  ?>
                              @endif
                              @endif
                          @endif
                          @endfor
                         </span>
                        </a>
                        @endif
                    </li>

                    @endfor
              </ul>
      </div>
      @else
        <p style="color:green">Upgrading the course....</p>
      @endif
</div>


    <div class="section_tab section_color span4">
  
    <ul id="tabs_new">
      <li><a href="#" name="#tab1"><i class="fa fa-comments icon_font"></i></a></li>
      <li><a href="#" name="#tab2"><i class="fa fa-bullhorn icon_font"></i></i></a></li>
      <li><a href="#" name="#tab3"><i class="fa fa-users icon_font"></i></a></li>   
      </ul>

      <div id="content_new" class="sortable_ul sortable_bg taken_courseb_blockss">
      <div id="tab1">
      @if($numberOfComments != 0)
      <div class="profile_view add_p">

         <div class="response response_margin close_new">{{ $numberOfComments }} Comments</div>         

      <!--   @if(Auth::check() == true)
        <div class="commentBlock">
            {{ Form::open(array('files'=> TRUE,'role'=>'form')) }}
            <div class="form-group">
                {{ Form::textarea('comment','',array('class'=>"form-control text_top textarea_height",'rows'=>0,'placeholder'=>t('Comment'))) }}
            </div>
            {{ Form::submit(t('Comment'),array('class'=>'btn btn-info')) }}
            {{ Form::close() }}
        </div>
        @endif  -->

            @foreach($course->comments as $comment)

            <div class="media profile clsClearfix" id="comment-{{ $comment->comment_id }}">

            <!--<span class="msg-time pull-right replymargin">
            @if(Auth::check() == TRUE)
            @if($comment->user_id == Auth::user()->id || $course->id == Auth::user()->id)
            <span class="pull-right"><button data-content="{{ $comment->comment_id }}" type="button" class="close delete-comment close_padd close_new" aria-hidden="true">&times;</button></span>
            @endif
            @endif
            </span>-->
  
            <div class="profile_image"><a href="{{ url('user/'.$comment->user->username) }}">
            <img class="media-object" alt="{{ $comment->user->fullname }}" src="{{ avatar($comment->user->avatar,75,75) }}"></a>
            </div>
            
            <div class="profile_title change_font name_title newname"> <a href="{{ url('user/'.$comment->user->username) }}">{{ $comment->user->fullname }}</a> <div class="about_hours hours_mrg"><i class="fa fa-clock-o opt_class"></i> <abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}">{{ date(DATE_ISO8601,strtotime($comment->created_at)) }}</abbr>&nbsp;</div></div>
            <div class="profile_content font_media name_cont span9">{{ Smilies::parse(e($comment->comment)) }}</div>

            <!-- @if(Auth::check() == TRUE)
            <a class="replybutton reply_top" id="box-{{ $comment->comment_id }}">{{ t('Reply') }}</a>
            <div class="commentReplyBox replymargin" id="openbox-{{ $comment->comment_id }}">
                <input type="hidden" name="pid" value="19">
                {{ Form::textarea('comment','',array('id'=>'textboxcontent'.$comment->comment_id,'class'=>"form-control text_top textarea_height",'rows'=>0,'placeholder'=>t('Comment'))) }}
                <button class="btn btn-info replyMainButton" id="{{ $comment->comment_id }}">{{ t('Reply') }}</button>
                <a class="closebutton color_btn" id="box-{{ $comment->comment_id }}">{{ t('Cancel') }}</a>
            </div>
            @endif -->

            @foreach($comment->reply as $reply)
            <div class="media media_space" id="reply-{{ $reply->id }}">
                <hr class="hr_margin">
                <a class="pull-left bla profile_image menu" href="{{ url('user/'.$reply->user->username) }}">
                    <img class="media-object" alt="{{ $reply->user->fullname }}" src="{{ avatar($reply->user->avatar,64,64) }}">
                </a>

                <div class="media-body">
                    <h4 class="media-heading comment"><a href="{{ url('user/'.$reply->user->username) }}" class="profile_title change_font">{{ $reply->user->fullname }}</a>
              
                       <!--  @if(Auth::check() == TRUE)
                        @if($reply->user_id == Auth::user()->id || $course->id == Auth::user()->id || $reply->comment->user->id == Auth::user()->id)
                        <span class="right"><button data-content="{{ $reply->id }}" type="button" class="close delete-reply close_padd close_new" aria-hidden="true">&times;</button></span>
                        @endif
                        @endif -->
            
            <span class="about_hours about_space"><i class="fa fa-clock-o opt_class"></i>  <abbr class="timeago" title="{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}">{{ date(DATE_ISO8601,strtotime($reply->created_at)) }}</abbr></span>

                    <span class="msg-time">
                    <i class="glyphicon glyphicon-time"></i>
                    <span>{{ Smilies::parse(e($reply->reply)) }}&nbsp;</span>
                    </span>

                    </h4>
                    <div class="about_hours about_space">
          </div>
                </div>
            </div>
            @endforeach
    </div>
    @endforeach  

              {{ $comments->links() }}    

          </div>

          @else
          <div class="add_p"> 
           <p>There are no comments at this time. </p>
           </div>
          @endif
          
      </div>
	  
      <div id="tab2" class="add_p"> 
            <p>There are no more announcements to show at this time. </p>
      </div>
      <div id="tab3" class="add_p">
        <ul>
                @foreach($purchase as $key => $purchase)
                @if(Auth::user()->id!=$purchase->id)
                <li class="clearfix"><div class="pull-left name_width new_namewidth">
                
                <a class="pull-left bla profile_image" href="{{ url('user/'.$purchase->username) }}"><img class="media-object media-ob" alt="{{ $purchase->fullname }}" src="{{ avatar($purchase->avatar,64,64) }}"><span class="align_username">{{$purchase->username}}</span></a>
                
                </div>
                <div class="pull-right mrg_follow">
                  @if(Auth::check())
                  @if(checkFollow($purchase->id))
                  <a type="button" class="btn btn-default replyfollow btn-block followButton" id="{{ $purchase->id }}">{{ t('Un Follow') }}</a>
                  @else
                  <a type="button" class="btn btn-default replyfollow btn-block followButton" id="{{ $purchase->id }}">{{ t('Follow Me') }}</a>
                  @endif
                  @endif
                </div></li>
                @endif
                @endforeach
        </ul>
    
      </div>
  </div>

 
    </div>  
    

</div>
</div>

</div>
<style>

.vjs-default-skin .vjs-big-play-button {
    background-color: rgba(7, 20, 30, 0.7);
    border: 0.1em solid #3B4249;
    border-radius: 65px;
    box-shadow: 0 0 1em rgba(255, 255, 255, 0.25);
    cursor: pointer;
    display: block;
    font-size: 3em;
    height: 88px;
    opacity: 1;
    position: absolute;
    right: 240px;
    text-align: center;
    top: 25px;
    transition: all 0.4s ease 0s;
    vertical-align: middle;
    width: 88px;
    z-index: 2;
}

.video-js .vjs-tech {
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
}

</style>

<!--End Description-->

@stop

@section('extrafooter')

<script type="text/javascript">

      $(".followButton").on("click", function () {
        var c = $(this);
        var b = "id=" + c.attr("id");
        $.ajax({type: "POST", url: "{{url()}}/unfollow", data: b, success: function (a) {
            $.when(c.fadeOut(300).promise()).done(function () {
                if (c.hasClass("btn")) {
                    c.removeClass("btn-default").addClass("btn-success").text(a).fadeIn()
                } else {
                    //c.replaceWith('<button class="btn-default btn-xs replyfollow followButton follow_me" id="'+c.attr("id")+'">' + a + "</button>")
                  c.replaceWith('<span class="notice_mid_link">' + a + "</span>")
                }
            })
        }});
        return false
    })
</script>

@stop