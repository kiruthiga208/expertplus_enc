@extends('master/index')

@section('content')

{{ HTML::style('assets/bsetec/static/css/createcourse/easy-responsive-tabs.css') }}
{{ HTML::style('assets/bsetec/static/css/createcourse/tab.css') }}

<style type="text/css">
#cropContaineroutput{ width:400px; height:225px; position: relative; border:1px solid #ccc;}
.cropControls i.cropControlReset{ background-position:-180px 0px;}
.cropControls i{position: relative;z-index: 9999;}
.cropControls i{display:block;float:left;margin:0;cursor:pointer;background-image:url('{{url()}}/assets/bsetec/static/img/cropperIcons.png');width:30px;height:30px;text-align:center;line-height:20px;color:#FFF;font-size:13px;font-weight: bold;font-style: normal;}
.cropControls i:hover{ background-color:rgba(0,0,0,0.7);  }
.cropControls i.cropControlZoomMuchIn{ background-position:0px 0px;}
.cropControls i.cropControlZoomIn{ background-position:-30px 0px; }
.cropControls i.cropControlZoomOut{ background-position:-60px 0px; }
.cropControls i.cropControlZoomMuchOut{ background-position:-90px 0px; }
.cropControls i.cropControlCrop{ background-position:-120px 0px;}
.cropControls i.cropControlUpload{ background-position:-150px 0px;}
.cropControls i.cropControlReset{ background-position:-180px 0px;}
.cropControls i.cropControlRemoveCroppedImage{ background-position:-180px 0px;}
.cropControls i:last-child{margin-right:none;}
.vjs-default-skin .vjs-big-play-button {background-color: rgba(7, 20, 30, 0.7);border: 0.1em solid #3b4249;border-radius: 65px;box-shadow: 0 0 1em rgba(255, 255, 255, 0.25);
    cursor: pointer;display: block;font-size: 3em;height: 88px;left: 242px;opacity: 1;position: absolute;right: 226px;
    text-align: center;top: 95px;transition: all 0.4s ease 0s;vertical-align: middle;width: 88px;z-index: 2;}
.video_align > input {min-height: 25px;margin: 10px 5px!important;}
.video_newalign > input {margin: 10px 5px;}
.fa.fa-plus {color: #FFFFFF;font-size: 20px;}
h2.newresp-accordion{display:none;margin: 0 -20px;border: 1px solid #DDD;text-align: left;font-size: 16px;padding-left: 40px;}
.progressbar-wrapper{display:none;width: 400px;height:30px;border: 1px solid #ff871a;border-radius: 5px;margin:0 auto;}
.progressbar-value{width: 0%;height:30px;background: rgb(255,215,120);
background: -moz-linear-gradient(top, rgba(255,215,120,1) 0%, rgba(255,135,26,1) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,215,120,1)), color-stop(100%,rgba(255,135,26,1)));
background: -webkit-linear-gradient(top, rgba(255,215,120,1) 0%,rgba(255,135,26,1) 100%);
background: -o-linear-gradient(top, rgba(255,215,120,1) 0%,rgba(255,135,26,1) 100%);
background: -ms-linear-gradient(top, rgba(255,215,120,1) 0%,rgba(255,135,26,1) 100%);
background: linear-gradient(to bottom, rgba(255,215,120,1) 0%,rgba(255,135,26,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffd778', endColorstr='#ff871a',GradientType=0 );}
.progress-error{font-size:18px;	width:100%;	text-align:center;}
#pricetab{display:none;}
.video_text{width:360px;height:150px;margin: 0 auto;text-align:center;padding-top:114px;font-size:18px;font-family:'roboto',sans-serif;font-size: 30px;font-weight: 400;color: #333333;line-height: 30px;}
@media only screen and (max-width: 768px) {
	.category_rightoption{float:left !important;}
	#BasiceDetailes{height:750px;}
	.category_option{height:80px;}
	select.category_option{height:40px;}
	h2.newresp-accordion{display:block;}
	.growsign{display:none;}
}
video{background:#000 !important;}
</style>

<script type="text/javascript">
function areyousure()
{
    return confirm('Are you sure want to delete');
}
</script>

{{ Form::open(array('files'=> true)) }}
{{ Form::hidden('course_id',$course->course_id,array('id'=>'course_id')) }}

<div class="container">

    <div class="course_tab clsClearfix">

        <div id="verticalTab">

            <ul class="resp-tabs-list">
                <li class="main-leftmenu"><span>GETTING STARTED</span></li>
                <li class="main-leftmenu"><span class="toggletabs">COURSE CONTENT <span class="growsign">+</span></span>
                    <ul id="countrytabs" class="shadetabs" style="display:none;">
                     <li><a href="#" rel="BasiceDetailes" class="selected">&raquo; Basics</a></li>
                     <li><a href="#" rel="DetailDetailes">&raquo; Details</a></li>
                     <li><a href="#" rel="ImageDetails">&raquo; Image</a></li>
                     <li><a href="#" rel="VideoDetails">&raquo; Promo Video</a></li>
                     <li><a href="#" rel="PrivacyDetailes">&raquo; Privacy</a></li>
                     <li><a href="#" rel="PricingDetails">&raquo; Pricing</a></li>
                    </ul>
                </li>
                <li class="main-leftmenu"><span>DANGER ZONE</span></li><li class="main-leftmenu"><span>CURRICULUM</span></li>
            </ul>
            
            <div class="resp-tabs-container"> 
            <span id="errormsg" class="errorcolor"></span>   
            <input type="hidden" id="currenttab" name="currenttab" value="">
            <div>
                <div class="slider_divsblocks">
                    <h2>Getting Started</h2>
                    <h3 class="GetStartedTitle">Create Your course <span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                            <h4 class="fxac">Prepare - Connect with Udemy and fellow instructors.</h4>
                            <p>Join the Udemy Studio on Facebook, Udemy's online instructor community where you can get help and feedback on your course from hundreds of other instructors. </p>
                            <p>Take our How-To-Create-A-Course Course; we'll walk you through every step of creating your Udemy course. </p>
                            <h4 class="fxac">Plan - Make creating your course a breeze.</h4>
                            <p> Set your learning objective and determine your audience. What are you teaching? Who are you teaching?   </p>
                            <h4 class="fxac">
                            Produce &ndash; Build the heart of your course.  </h4>
                            <p>Set the price of your course and complete your premium instructor application so we can pay you. Or skip this step to leave your course free. </p>
                        </div>
                    </div>

                    <h3 class="GetStartedTitle">Submitting your course for review <span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                            <h4 class="fxac">Prepare - Connect with Udemy and fellow instructors.</h4>
                            <p>Join the Udemy Studio on Facebook, Udemy's online instructor community where you can get help and feedback on your course from hundreds of other instructors. </p>
                            <p>Take our How-To-Create-A-Course Course; we'll walk you through every step of creating your Udemy course. </p>
                            <h4 class="fxac"> Plan - Make creating your course a breeze.</h4>
                            <p> Set your learning objective and determine your audience. What are you teaching? Who are you teaching?   </p>
                            <h4 class="fxac"> Produce &ndash; Build the heart of your course.  </h4>
                            <p> Set the price of your course and complete your premium instructor application so we can pay you. Or skip this step to leave your course free. </p>
                        </div>
                    </div>

                    <h3 class="GetStartedTitle">After publishing your course <span class="arr"></span></h3>

                    <div class="GetStartedContent">
                        <div class="status-tabs-content">
                            <h4 class="fxac">Prepare - Connect with Udemy and fellow instructors.</h4>
                            <p>Join the Udemy Studio on Facebook, Udemy's online instructor community where you can get help and feedback on your course from hundreds of other instructors. </p>
                            <p>Take our How-To-Create-A-Course Course; we'll walk you through every step of creating your Udemy course. </p>
                            <h4 class="fxac">Plan - Make creating your course a breeze.</h4>
                            <p> Set your learning objective and determine your audience. What are you teaching? Who are you teaching?   </p>
                            <h4 class="fxac">Produce &ndash; Build the heart of your course.  </h4>
                            <p>Set the price of your course and complete your premium instructor application so we can pay you. Or skip this step to leave your course free. </p>
                        </div>
                    </div>
                </div>
            </div>  

        <!-- Course Coneten -->
        <div>
            <!--ul id="countrytabs" class="shadetabs">
                <li><a href="#" rel="BasiceDetailes" class="selected">Basics</a></li>
                <li><a href="#" rel="DetailDetailes">Details</a></li>
                <li><a href="#" rel="ImageDetails">Image</a></li>
                <li><a href="#" rel="VideoDetails">Promo Video</a></li>
                <li><a href="#" rel="PrivacyDetailes">Privacy</a></li>
                <li><a href="#" rel="PricingDetails">Pricing</a></li>
            </ul-->

            <div style="max-width:100%; margin-bottom: 1em;margin:0 20px; ">

            <!-- Basics -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span><span>&raquo; Basics</span></h2>
            <div id="BasiceDetailes" class="tabcontent tabconts">
                <div class="course_basic">
                <div class="course_basic course_newbasic course_basic">Basics<h5>Add and modify the basic elements of your course.</h5></div>
                <div class="course_title">Title</div>
                 <div class="title_txt">                 
                {{ Form::text('title',$course->course_title,array('class'=>'course_textbox','placeholder'=>'E.g. Learn Photoshop CS6','maxlength' => 60, 'onkeypress'=>'max_text($(this),60)')) }}
                <span class="textBoxLimit newtextBoxLimit">{{60-strlen($course->course_title)}}</span>
                </div>
                <div class="course_title">SubTitle</div>
                {{ Form::textarea('subtitle',$course->subtitle,array('class'=>'course_newsubtextbox','placeholder'=>'E.g. A-Z guide to creating amazing images and clips using the newest version of industry\'s preferred software.')) }}
                <div class="course_title">Keywords</div>
                <input type="text" autocomplete="off" value="{{$course->keywords}}" name="tags" id="tagit" placeholder="Tags" class="course_textbox tm-input tm-input-success new-input" data-original-title=""/>               
                
                    <div class="category_option clsClearfix">
                       <div class="course_ctitle new_ctitle">Category</div>
                        <select name="cat_id" class="category_option" required>
                        <option value="9">-- Select Category --</option>
                        @foreach(siteCategories() as $category)
                        @if($category->id == $course->cat_id)
                        <option value="{{ $category->id }}" selected="selected">{{ $category->name }}</option>
                        @else
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endif
                        @endforeach
                        </select>
                    </div>
              
                    <div class="category_rightoption clsClearfix">
                    
            <div class="course_ctitle new_ctitle">Language</div>
                        <select name="lang_id" class="category_option" required>
                            <option value="40">{{ t('Select Language') }}</option>
                            @foreach(siteLanguage() as $language)
                            @if($language->id == $course->lang_id)
                            <option value="{{ $language->id }}" selected="selected">{{ $language->native_name }}</option>
                            @else
                            <option value="{{ $language->id }}">{{ $language->native_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
             <div class="submit_btn">
                 
                    <input type="submit" name="" value="Save" class="course_next new_save">           
                 </div>
                </div>

            </div>

            <!-- Basics -->

            <!-- Detailes -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-3"><span class="resp-arrow"></span><span>&raquo; Details</span></h2>
        <div id="DetailDetailes" class="tabcontent tabconts">
            
             <div class="course_basic course_newbasic course_basic">Details<h5>Add and modify all the details about your course.</h5>
            
                    <div class="course_ctitle">Course Summary:</div>
                    {{ Form::textarea('description',$course->description,array('class'=>'ckeditor ck_mrg')) }}

                    <div class="course_ctitle">Course Goal and Objectives:</div>
                    <ul class="course_goal_ul sortable_ul sortable_scroll ui-sortable "> 
                    @php(  $key_goal = explode('--txt--', $course->course_goal) )
                        @if($key_goal[0] != '')
                           @php $i=1; @endphp
                            @foreach ($key_goal as $key => $value)
                            <li> {{$value}}<a class="pull-right course_goal{{$i}}" onclick="delete_li('course_goal','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
                            <a class="pull-right" href="javascript:void(0)"><i class="fa fa-align-justify justify_color"></i></a></li>
                       @php  $i++; @endphp
                        @endforeach
                        @endif
                    </ul>
                    <input type="text" id="course_goal" class="course_textbox tm-input" />
                    <input type="button" id="course_goal_btn" onclick="common_add('course_goal')" class="button_color btn" value="Add" />
                    <textarea style="display:none" name="course_goal" id="course_goal_text">{{$course->course_goal}}</textarea>

                    <div class="course_ctitle">Intended Audience:</div>
                    <ul class="int_audience_ul sortable_ul sortable_scroll ui-sortable"> 
                    @php ( $key_goal = explode('--txt--', $course->int_audience) )
                        @if($key_goal[0] != '')
                          @php $i=1; @endphp
                           @foreach ($key_goal as $key => $value)
                            <li> {{$value}}<a class="pull-right int_audience{{$i}}" onclick="delete_li('int_audience','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a>
                            <a class="pull-right" href="javascript:void(0)"><i class="fa fa-align-justify justify_color"></i></a></li>
                        @php $i++; @endphp
                        @endforeach
                        @endif
                    </ul>
                    <input type="text" id="int_audience" class="course_textbox tm-input"/>
                    <input type="button" id="int_audience_btn" onclick="common_add('int_audience')" class="button_color btn" value="Add" />
                    <textarea style="display:none" name="int_audience" id="int_audience_text">{{$course->int_audience}}</textarea>

                    <div class="course_ctitle">Course Requirements:</div>
                    <ul class="course_req_ul sortable_ul sortable_scroll ui-sortable"> 
                    @php( $key_goal = explode('--txt--', $course->course_req) )
                         @if($key_goal[0] != '')
                          @php $i=1; @endphp
                           @foreach ($key_goal as $key => $value)
                            <li> {{$value}}<a class="pull-right course_req{{$i}}" onclick="delete_li('course_req','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a>
                            <a class="pull-right" href="javascript:void(0)"> <i class="fa fa-align-justify justify_color"></i></a></li>
                        @php ($i++ )
                            @endforeach
                        @endif
                    </ul>
                    <input type="text" autocomplete="off" id="course_req" class="course_textbox tm-input tm-input-large"/>
                    <input type="button" id="course_req_btn" onclick="common_add('course_req')" class="button_color btn" value="Add" />
                    <textarea style="display:none" name="course_req" id="course_req_text">{{$course->course_req}}</textarea>

                    <div class="course_ctitle">Instructional Level:</div>
                    <span class="remove_mrg">{{Form::radio('course_level', '1', ($course->course_level == 1) ? true : false, array('class'=>'')) }} <span class="opt_class">Introductory</span></span>
                    <span class="remove_mrg">       {{Form::radio('course_level', '2', ($course->course_level == 2) ? true : false, array('class'=>'')) }} <span class="opt_class">Intermediate</span></span>
                    <span class="remove_mrg">     {{Form::radio('course_level', '3', ($course->course_level == 3) ? true : false, array('class'=>'')) }}<span class="opt_class"> Advanced</span></span>
                    <span class="remove_mrg">    {{Form::radio('course_level', '4', ($course->course_level == 4) ? true : false, array('class'=>'')) }} <span class="opt_class">All Levels (Comprehensive)</span></span>
                    <input type="submit" name="" value="Save" class="course_next">  
            </div>
        </div>

            <!-- Detailes -->

            <!-- Image Details -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-4"><span class="resp-arrow"></span><span>&raquo; Image</span></h2>
        <div id="ImageDetails" class="tabcontent tabconts">
            
            <div class="course_basic">
            
                <div class="course_basic course_newbasic course_basic">Image<h5>Add an image to your course.</h5></div>
                            
                <div class="video_width clearfix">
                    <div class="media-preview-wrapper span6">
                    <div class="media-preview">
                        <div class="media">
                            <div class="notes-table">
                                <div style="position:relative; height:50%;">
                                @if($course->image == '')
                                <img src="{{ url('uploads/course/default.jpg') }}">
                                @else
                                <img class="croppedImg media_img" src="{{url().'/uploads/course/'.$course->image}}">
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tip">
                        <b>TIP:</b>
                        Your beautiful, clean, non-pixelated image should be at minumum 480x270. If you want it to look great an on iPad or tablet, make it 960x540. Before you get started, please check out
                        <a target="_blank" href=""> {{ siteSettings('siteName') }}'s course images guidelines and resources here</a> .
                        <br>
                        <br>
                        <b>NOTE:</b>
                        Course images are an extremely important part of telling students you have a polished, high-quality course. We want your course image to look awesome! As such, {{ siteSettings('siteName') }} reserves the right to improve or create a custom course image for you. If we do, we'll be sure to notify you!
                    </div>
                    </div>
                </div>
                <div id="cropContaineroutput"></div>
				<div class="progressbar-wrapper">
					<div class="progressbar-value"></div>
				</div>
				<div class="progress-error"></div>
                <input type="hidden" id="error_upload" name="error_upload" value="0" />
                <input type="hidden" id="cropOutput" style="width:200px; padding:5px 4%; margin:20px auto; display:block; border: 1px solid #CCC;" />
                <input type="hidden" id="file_names" name="image_name" value="{{$course->image}}" />
                <input type="submit" name="" value="Save" class="course_next">  
            </div>
        </div>

            <!-- Image Details -->

            <!-- Video Details -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-5"><span class="resp-arrow"></span><span>&raquo; Video</span></h2>
        <div id="VideoDetails" class="tabcontent V_Details tabconts">
            
            <div class="course_basic course_newbasic course_basic">Promo Video<h5>Add a promotional video to your course.</h5></div>

            <div class="video_width clearfix">
                
                <div class="media-preview-wrapper span6">
                <div class="media-preview">
                    <div class="media">
                        <div class="notes-table">
                            <div style="position:relative; height:50%;width:570px;height:264px;">            
                                <div id="video_div">
								@if($course->video)
								<video id='my_video' class='video-js vjs-default-skin' controls preload='auto' width='570' height='264' data-setup='{}'>
                                <source src='{{url()."/uploads/videos/".$course->video}}' type='{{$course->video_type}}'></video>
								@else
								<div class="video_text">Your video content will be previewed here...</div>
								@endif
                                <input type='hidden' value='{{$course->video}}' id='video_name' name='video_name' />
                                <input type='hidden' value='{{$course->video_type}}' id='video_type' name='video_type' />
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="tip">
                <b>TIP:</b>Your video to be uploaded must be mp4.If it should perfectly run with clarity on iPad or Tablet, it should be in mp4 format. Before you get started, please check out
                <a target="_blank" href=""> {{ siteSettings('siteName') }}'s course videos guidelines and resources here</a>.
                <br><br>
                <b>NOTE:</b>Following the video uploading guidelines is mandatory, especially if you want presentation to exhibit your message, in the high-quality audio-visual, and in an inspiring way. We want your course video to look awesome! As such, {{ siteSettings('siteName') }} reserves the right to improve or create a custom course video for you. If we do, we'll be sure to notify you!
                </div>
                </div>
            </div>  
            <div id="video_iframe">
            </div>
            <input type="submit" name="" value="Save" class="course_next">  
        </div>

            <!-- Video Details -->

            <!-- Privacy Details -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-6"><span class="resp-arrow"></span><span>&raquo; Privacy</span></h2>
            <div id="PrivacyDetailes" class="tabcontent V_Details tabconts">
            
              <div class="course_basic course_newbasic course_basic">Privacy<h5>Set the level of privacy for your course.</h5></div>
            
            <ul id="myTab" class="privacy_tabs nav nav-tabs tab_public">
                <li class="{{{$course->privacy == 1 ? 'active' : ''}}}"><a class="selected" href="#public" data-toggle="tab">Public</a></li>
                <li class="{{{$course->privacy == 1 ? '' : 'active'}}}"><a class="selected" href="#private" data-toggle="tab">Private</a></li>
            </ul> 
            <div class="tab-content">
                <div class="tab-pane fade {{{$course->privacy == 1 ? 'in active' : ''}}}" id="public">
                     {{Form::radio('privacy', '1', ($course->privacy != 2) ? true : false, array('class'=>'privacy_pub')) }}
                     <div class="space_btw">Course is available for anyone to take on tutorial site.</div>
                </div>
               
               <div class="tab-pane fade {{{$course->privacy == 1 ? '' : 'in active'}}}" id="private">
                    <ul class="tab_ul">
                        {{Form::radio('privacy', '2', ($course->privacy == 2) ? true : false, array('class'=>'privacy_inv ')) }} Invitation Only
                      
                    </ul>
               </div>
            </div>
        
            <span id="tip">
                <strong>TIP:</strong>
                A “Public” course is available for anyone to take on {{ siteSettings('siteName') }}. A “Private” course is only accessible by a direct URL. It will not show up on {{ siteSettings('siteName') }}'s search. This is great if you have a course you'd like to offer but only to a select group.
            </span>

                <input type="submit" name="" value="Save" class="course_next">  
            </div>

            <!-- Privacy Details -->
			<h2 class="newresp-accordion" role="tab" aria-controls="tab_item-7"><span class="resp-arrow"></span><span>&raquo; Price</span></h2>
            <div id="PricingDetails" class="tabcontent V_Details tabconts">
                <div class="col-md-12 V_Details">
					<div class="course_basic course_newbasic course_basic">Price<h5>Set the price of your course.</h5></div>
					<div class="des_margin">
					<label for="description">Price Settings:</label>
					<div class="price-holder">
					<span class="price-label">Currently the course is for </span>
					<span class="new_price">{{{$course->pricing == 0 ? 'Free' : '$'.$course->pricing}}}</span>
					</div>
					<p> Click <a class="toggle-price-form" href="javascript:void(0)">here</a> to change the pricing </p>
					<div id="pricetab">
						<ul id="myTab2" class="privacy_tabs nav nav-tabs tab_public">
							<li class="{{{$course->pricing == 0 ? 'active' : ''}}}"><a class="selected set_price_free" href="#free2" data-toggle="tab">Free</a></li>
							<li class="{{{$course->pricing == 0 ? '' : 'active'}}}"><a class="selected" href="#paid2" data-toggle="tab">Paid</a></li>
						</ul> 	
						<div class="tab-content">
							<div class="tab-pane fade {{{$course->pricing == 0 ? 'in active' : ''}}}" id="free2">
								 <div class="space_btw">Your course will be free and available</div>
							</div>
						   
						   <div class="tab-pane fade {{{$course->pricing == 0 ? '' : 'in active'}}}" id="paid2">
								<ul class="tab_ul">
                                    <span class="price_dollar_span">$</span> <input type="text" name="price" class="price_text form-control" onkeypress="return isNumberKey(event)" placeholder="Set a Price(Please enter numbers only)" id="box1" value="{{$course->pricing}}" />
									 <span id="price_span"></span>
                                    <!--$ <input type="text" name="price" class="price_text form-control" placeholder="Set a Price" value="{{$course->pricing}}" />-->
								</ul>
						   </div>
						</div>
					</div>
                </div>
                <input type="submit" name="" value="Save" class="course_next"> 
                </div> 
            </div>

            </div>
        </div>

        <!-- Course Coneten -->

                <!-- Manage Instructor -->

                <!-- <div>
                    <div class="col-md-12 col_space">
                    <label for="description"><h2>Manage Instructors:</h2></label>
                        <h3>Create your course outline and add your course materials.</h3>
                    <div class="table-responsive add_newspace">
                        <table class="table table-bordered table-hover">
                            <thead><tr><th>Instructor</th><th>Visible</th><th>Can Edit</th><th>Delete</th></tr></thead>
                            <tbody id="insturction_tr">
                            <tr>
                                <td>{{Auth::user()->fullname}}</td>
                                <td>{{Form::checkbox('1','TRUE', true)}}</td>
                                <td>{{Form::checkbox('2','TRUE', true,array('disabled'=>'disabled'))}}</td>
                                <td></td>
                            </tr>
                            
                            <?php
                                $manage_instructor = json_decode($course->manage_ins);
                                if(isset($manage_instructor)){
                                $ins_count =  count($manage_instructor->id); ?>
                                    @for ($i=0; $i < $ins_count; $i++) 
                                    <tr>
                                    <td>{{$manage_instructor->name[$i]}}
                                    <input type="checkbox" name="instructor_name[]" value="{{$manage_instructor->name[$i]}}" checked="checked" style="display:none;" />
                                    <input type="checkbox" name="instructor_id[]" value="{{$manage_instructor->id[$i]}}" checked="checked" style="display:none;" /></td>

                                    @if(@$manage_instructor->visible[$i] == $manage_instructor->id[$i])
                                    <td><input type="checkbox" value="0" name="ins_visible[]" style="display:none;" /><input type="checkbox" class="checkSelected" value="{{$manage_instructor->id[$i]}}" checked="checked" name="ins_visible[]"/></td>
                                    @else
                                    <td><input type="checkbox" value="0" name="ins_visible[]" checked="checked" style="display:none;" /><input type="checkbox" class="checkSelected" value="{{$manage_instructor->id[$i]}}" name="ins_visible[]"/></td>
                                    @endif

                                    @if(@$manage_instructor->editable[$i] == $manage_instructor->id[$i])
                                    <td><input type="checkbox" value="0" name="ins_edit[]" style="display:none;" /><input type="checkbox" class="checkSelected" value="{{$manage_instructor->id[$i]}}" checked="checked" name="ins_edit[]"/></td>
                                    @else
                                    <td><input type="checkbox" value="0" name="ins_edit[]" checked="checked" style="display:none;" /><input type="checkbox" class="checkSelected" value="{{$manage_instructor->id[$i]}}" name="ins_edit[]"/></td>
                                    @endif

                                    <td><a class="glyphicon glyphicon-trash delete_tr" href="javascript:void(0)"></a></td>
                                    </tr>
                               @endfor
                           <?php }?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" id="get_instruction" class="form-control form_text add_newspace"/> 
                    <input type="hidden" id="get_id" class="form-control"/> 
                    <input type="button" id="add_instruction " class="button_color btn add_newspace" value="Add">
                    <input type="submit" name="" value="Save" class="course_next">  
                    </div>
                </div> -->

                <!-- Manage Instructor -->

<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
<div></div>
                <!-- Danger Zone -->

                <div>
                    <div class="col-md-12 col_space">
                    
                    
                        <label for="description"><h2>Danger Zone</h2></label>
                        <h3>Create your course outline and add your course materials.</h3>
                        
                    
                    <script>
                    function un()
                    {
                        //alert("unpublished");
                    }
                    </script>
                    <div class="col_header">
                         <h4>Unpublish Your Course</h4>

                            Your course is not published yet. In the future, you can unpublish your course here!
                            <?php if($course->approved==1) { ?>
                            <a href="{{ url('unpublish/'.$course->course_id) }}" class="btn btn-warning" onclick=""><!--{{$course->approved}}-->Unpublish </a>
                            <?php } ?>
                        <h4>Remove Your Course</h4>
                            
                            When you remove your course, there is no going back!

                            Please think twice before removing it.

                           <a href="{{ url('deleteCourses/'.$course->course_id) }}" onclick="return areyousure();" class="btn btn-danger">Remove</a>
                        </div>
                </div>
                </div>

                <!-- Danger Zone -->
                
                <!--Curriculam-->

                            <?php $jval = 0; ?>
                            <?php $kval = 0; ?>
                            <?php $lval = 0; ?>

                <div>
                    
                    <label for="description" class="cur_h2"><h2>Curriculum</h2></label>
                 <h3>Create your course outline and add your course materials.</h3>
                    <ul id="curriculum_ul" class="sortable_ul sortable_new">
                    @if($course->curriculum == '')
                            <?php $jval = 1; ?>
                            <?php $kval = 1; ?>
                        <li class="ui-state-default curriculum_ul_li ul_main ul_padd main_li_curriculum"> 
                        <!-- <a class="pull-right glyphicon glyphicon-trash DeleteTitle" href="javascript:void(0)"></a>
                        <a class="pull-right glyphicon glyphicon-edit" onclick="return EditTitle('Section',$(this))" href="javascript:void(0)"></a>
                        <a class="pull-left glyphicon glyphicon-align-justify"><i class="fa fa-align-justify justify_color"></i></a> -->
                        <a class="pull-right" onclick="return EditTitle('Section',$(this))" href="javascript:void(0)"><i class="fa fa-pencil-square-o color_icon"></i></a>
                        <a class="pull-left justify_color"><i class="fa fa-align-justify justify_color"></i></a>
                        
                       <span class="main_title cur_h3 text_color">Section</span><span>1</span><span class="close_new"> : </span><span class="SectionPTage title_text cur_h3 text_color">My First Section</span>
                        <div class="SectionEditDiv" style="display:none">
                        <input type="text" class="SectionEditText form-control edit_title" maxlength="80" name="section[]"><span class="charCount">80</span>
                        <input type="button" class="SectionEditButton btn" value="Save">
                        </div>
                        <div class="file_div clearfix" style="display:none;"> 
                        <input type='hidden' value='' class='lectureFileName'/>
                        <input type='hidden' value='' class='lectureFileType'/>
                        <textarea class='form-control lectureDescription' style="display:none"></textarea>
                        </div>
                        </li>

                        <li class="ui-state-default curriculum_ul_li ul_main ul_padd sub_li_curriculum">  
                        <a class="pull-left justify_color"><i class="fa fa-align-justify justify_color"></i></a>
                        <a class="pull-right DeleteTitle" onclick="return DeleteTitleFunction($(this))" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
                        <a class="pull-right" onclick="return EditTitle('Lecture',$(this))" href="javascript:void(0)"><i class="fa fa-pencil-square-o color_icon"></i></a>
                        <a class="pull-right AddContentClass" onclick="return AddContent('Lecture',$(this))" href="javascript:void(0)"><i class="fa fa-plus color_icon"></i></a>
                        <span class="main_title cur_h3">Lecture</span><span>1</span> : <span class="LecturePTage title_text cur_h3">My First Lecture</span>
  
                        <div class="LectureEditDiv clearfix" style="display:none">
                            <input type="text" class="LectureEditText form-control" maxlength="50" name="lecture[]">
                            <input type="button" class="LectureEditButton btn" value="Save">
                        </div>
                        
                        <div class="AddLectureContentDiv clearfix" style="display:none">
                            <div class="file_div clearfix" style="display:none">  
                                <input type='hidden' value='' class='lectureFileName'/>
                                <input type='hidden' value='' class='lectureFileType'/>
                                <textarea class='form-control lectureDescription' style="display:none"></textarea>  
                            </div>
                            <div class="add_contents clearfix"></div>                    
                        </div>
                                                
                        </li>
                        
                        @else
                        <?php 
                        $curriculum = json_decode($course->curriculum);
                        $curri_count = @count($curriculum->MainTitle); 
                       ?>

                            @for ($j=0; $j < $curri_count; $j++)
                            <?php // $jval = $j; ?>
                            @if($curriculum->MainTitle[$j] == 'Section')
                            <?php $class = 'main_li_curriculum';?>
                            @else
                            <?php $class = 'sub_li_curriculum';?>
                            @endif

                            <li class="ui-state-default curriculum_ul_li ul_main  ul_padd {{$class}}"> 
                            <a class="pull-right DeleteTitle" onclick="return DeleteTitleFunction($(this))" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
                            <a class="pull-right" onclick="return EditTitle('{{$curriculum->MainTitle[$j]}}',$(this))" href="javascript:void(0)"><i class="fa fa-pencil-square-o color_icon"></i></a>
                            @if($curriculum->MainTitle[$j] == 'Lecture' || $curriculum->MainTitle[$j] == 'Quiz' )
                            <a class="pull-right AddContentClass" onclick="return AddContent('{{$curriculum->MainTitle[$j]}}',$(this))" href="javascript:void(0)"><i class="fa fa-plus color_icon"></i></a>
                            @endif
                            <a class="pull-left"><i class="fa fa-align-justify justify_color"></i></a>
                            <span class="main_title cur_h3">{{$curriculum->MainTitle[$j]}}</span> 
                            @if($curriculum->MainTitle[$j] == 'Section')
                            <span class="cur_sec_count" id="cur_sec_count{{$jval+1}}">{{$jval+1}}</span>
                            <?php $jval = $jval+1; ?>
                            @endif
                            @if($curriculum->MainTitle[$j] == 'Lecture')
                            <span class="cur_lec_count" id="cur_lec_count{{$kval+1}}">{{$kval+1}}</span>
                            <?php $kval = $kval+1; ?>
                            @endif
                            @if($curriculum->MainTitle[$j] == 'Quiz' )
                            <span class="cur_quiz_count" id="cur_quiz_count{{$lval+1}}">{{$lval+1}}</span>
                            <?php $lval = $lval+1; ?>
                            @endif
                            : <span class="{{$curriculum->MainTitle[$j]}}PTage title_text cur_h3">{{$curriculum->title[$j]}}</span>
                            <div class="{{$curriculum->MainTitle[$j]}}EditDiv" style="display:none">
                            <!--<input type="text"   maxlength="80" class="{{$curriculum->MainTitle[$j]}}EditText form-control edit_title" value="<?php echo $curriculum->title[$j]; ?>" /><span class="charCount">80</span>-->
                            <input type="text" id="edit_ti" name="edit_ti" maxlength="80" class="{{$curriculum->MainTitle[$j]}}EditText form-control edit_title" value="<?php echo $curriculum->title[$j]; ?>" /><span class="charCount">80</span>
                            <div class="divblock">
                                <input type="hidden" id="lectureidval" value="{{$kval+1}}">
                                <input type="button"  class="{{$curriculum->MainTitle[$j]}}EditButton btn" value="Update" >
                                <input type="button" class="btn" onclick="return CloseEdit('{{$curriculum->MainTitle[$j]}}EditDiv')" value="Cancel">
                            </div>
                            </div>
                           

                            @if($curriculum->MainTitle[$j] == 'Lecture')
                            <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv" style="display:none">

                                <div class="file_div clearfix" style="display:none">
                                    <input type='hidden' value='' class='lectureFileName'/>
                                    <input type='hidden' value='' class='lectureFileType'/>
                                    <textarea style="display:none" class='form-control lectureDescription'></textarea>
                                </div>
                                <?php $file_count = count($curriculum->file_type[$j]); ?>
                                    @for($k=0; $k < $file_count; $k++)
                                    <?php // $kval = $k; ?>
                                    @if($curriculum->file_type[$j][$k] != '')
                                    <input type="button" value="Save and Close" onclick="CloseDivFunction($(this))" class="btn Close_Div">
                                    <div class="file_div clearfix">
                                        <a class="pull-right DeleteContent" onclick="DeleteContentFunction($(this))" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
                                        @if($curriculum->file_type[$j][$k] == 'audio/mpeg')
                                            <audio controls><source src='{{url()."/uploads/videos/".$curriculum->file_name[$j][$k]}}' type='{{$curriculum->file_type[$j][$k]}}'></audio>
                                        @elseif($curriculum->file_type[$j][$k] == 'video/mp4' || $curriculum->file_type[$j][$k] == 'video/x-flv')
                                            <video id='my_video_lecture' class='video-js vjs-default-skin' controls preload='auto' width='275' height='200' data-setup='{}'>
                                            <source src='{{url()."/uploads/videos/".$curriculum->file_name[$j][$k]}}' type='{{$curriculum->file_type[$j][$k]}}'></video>
                                        @elseif($curriculum->file_type[$j][$k] == 'text/plain')
                                            <?php $file = @file_get_contents(url()."/uploads/videos/".$curriculum->file_name[$j][$k],true);
                                            if($file){
                                            $file_content = substr($file, 0, 100).'...'; 
                                            echo $file_content; 
                                            } ?>
                                        @endif
                                            <input type='hidden' value='{{@$curriculum->file_name[$j][$k]}}' class='lectureFileName'/>
                                            <input type='hidden' value='{{@$curriculum->file_type[$j][$k]}}' class='lectureFileType'/>
                                            <textarea rows="5" class='form-control lectureDescription'>{{@$curriculum->file_description[$j][$k]}}</textarea>
                                    </div>
                                    @else
                                    <!--<div class="file_div clearfix" style="display:none">
                                            <input type='hidden' value='{{@$curriculum->file_name[$j][$k]}}' class='lectureFileName'/>
                                            <input type='hidden' value='{{@$curriculum->file_type[$j][$k]}}' class='lectureFileType'/>
                                            <textarea style="display:none" class='form-control lectureDescription'>{{@$curriculum->file_description[$j][$k]}}</textarea>
                                    </div> -->
                                    @endif
                                    @endfor
                                <div class="add_contents"></div>
                            </div>
                            @elseif($curriculum->MainTitle[$j] == 'Quiz')
                            <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv" style="display:none">
                                <div class="file_div clearfix" style="display:none">
                                <input type='hidden' value='' class='lectureFileName'/>
                                <input type='hidden' value='' class='lectureFileType'/>
                                <textarea rows="5" class='form-control lectureDescription' style="display:none"></textarea>
                                </div>
                                <?php $quiz_count = count($curriculum->file_type[$j]); ?>
                                @for($l=0; $l < $quiz_count; $l++)
                                    <?php // $lval = $l; ?>
                                @if($curriculum->file_type[$j][$l] != '')
                                <div class="file_div clearfix">
                                <a class="pull-right delete_quiz" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
                                <p>{{$curriculum->file_name[$j][$l]}}</p>
                                <input type="hidden" value="{{$curriculum->file_name[$j][$l]}}" class="lectureFileName"/>
                                <input type="hidden" value="{{$curriculum->file_type[$j][$l]}}" class="lectureFileType"/>
                                <textarea class="form-control lectureDescription" style="display:none">{{$curriculum->file_description[$j][$l]}}</textarea>
                                </div>
                                @endif
                                @endfor
                                <div class="add_contents">
                                <div class="quiz_form span8">
                                </div>
                                </div>
                            </div>
                            @else
                            <div class="Add{{@$curriculum->MainTitle[$j]}}ContentDiv" style="display:none">
                                <div class="file_div clearfix" style="display:none">
                                <input type='hidden' value='' class='lectureFileName'/>
                                <input type='hidden' value='' class='lectureFileType'/>
                                <textarea class='form-control lectureDescription' style="display:none"></textarea>
                                </div>
                            </div>
                            @endif
                            </li>
                            @endfor

                        @endif
                    </ul>
                
                    <div id="add_lecture" class="lectures2blockss" style="display:none;margin-left:5%;border: 1px solid #444;padding:1%;width:90%;">
                    <label class="lecture_texts">Lecture</label><span class="cur_lec_count add_lec_count c_marg" id="cur_lec_count{{$kval+1}}">&nbsp;{{$kval+1}}</span>
                        <input type="text" id="AddLectureTitletxt" maxlength="80" class="form-control form_new lecture_txtb"><span id="ch_AddLectureTitletxt" class="ch_count">80</span> 
                        <input type="hidden" id="current_lec" value="{{$kval+1}}">
                        <div class="divblock">
                            <input type="button" onclick="return AddTitle('Lecture', '{{$kval+1}}')" class="btn add_lecture_btns" value="Add Lecture">
                            <input type="button" onclick="return CloseFields('lecture');" class="btn btn-link lecture_cancel" value="Cancel">
                        </div>
                    </div>

                    <div id="add_section" class="addsectiondivs" style="display:none;margin-left:5%;border: 1px solid #444;padding:1%;width:90%;">
                    <label class="add_sections">Section</label><span class="cur_sec_count add_sec_count c_marg" id="cur_sec_count{{$jval+1}}">&nbsp;{{$jval+1}}</span>
                        <input type="text" placeholder="Enter a Title" id="AddSectionTitletxt" maxlength="80" class="form-control input_textboxes"/> <span id="ch_AddSectionTitletxt" class="ch_count">80</span>
                        <input type="hidden" id="current_sec" value="{{$jval+1}}">
                        <div class="divblock">
                            <input type="button" onclick="return AddTitle('Section', '{{$jval+1}}')" class="btn add_lecture_btns" value="Add Section">
                            <input type="button" onclick="return CloseFields('section');" class="btn btn-link lecture_cancel" value="Cancel">
                        </div>
                    </div>

                    <div id="add_quiz" class="lectures2blockss" style="display:none;margin-left:5%;border: 1px solid #444;padding:1%;width:90%;">
                    <label class="lecture_textss">Quiz</label><span class="cur_quiz_count add_quiz_count c_marg" id="cur_quiz_count{{$lval+1}}">&nbsp;{{$lval+1}}</span>
                        <input type="text" id="AddQuizTitletxt" maxlength="80" class="form-control form_new lecture_txtb"/> <span id="ch_AddQuizTitletxt" class="ch_count">80</span>
                        <input type="hidden" id="current_quiz" value="{{$lval+1}}">
                        <div class="divblock">
                            <input type="button" onclick="return AddTitle('Quiz'), '{{$lval+1}}'" class="btn add_lecture_btns" value="Add Quiz">
                            <input type="button" onclick="return CloseFields('quiz');" class="btn btn-link lecture_cancel" value="Cancel">
                        </div>
                    </div>

                    <div id="add_quiz_ques" style="display:none">
                        <h4>Add Quiz</h4>
                        <div class="clear"></div>
                        <label>Question</label>
                        <textarea  class="form-control question_text ckeditor" rows="5"></textarea>
                        <label>Answers</label>
                        
                        <p><input type="radio" value="0" class="correct_answer" name="answer_text" /><textarea class="form-control answer_text" rows="3"></textarea></p>
                        <p><input type="radio" value="0" class="correct_answer" name="answer_text" /><textarea class="form-control answer_text" rows="3"></textarea></p>
                        <p><input type="radio" value="0" class="correct_answer" name="answer_text" /><textarea class="form-control answer_text" rows="3"></textarea></p>
                        <p><input type="radio" value="0" class="correct_answer" name="answer_text" /><textarea class="form-control answer_text" rows="3"></textarea></p>
                        <!--</br>-->
                        <div class="quiz_saveblocks">
                        <input type="button" class="btn save_quiz add_lecture_btns" value="Save">
                        <input type="button" class="btn cancel_quiz lecture_cancel" value="Cancel">
                        </div>
                    </div>

                     <div id="add_lec_qui" class="clearfix">
                    <a href="javascript:void(0)" id="lec" onclick="return AddFields('lecture');" class="btn btn-primary add_button btn_bg btn_addmrg btn_align adds_btnss">Add Lecture</a>
                    <a href="javascript:void(0)" id="quiz" onclick="return AddFields('quiz');" class="btn btn-primary quiz_button btn_bg btn_addmrg btn_ralign adds_btnss">Add Quiz</a>  
                    </div><div class="clearfix">
                <button type="button" onclick="return AddFields('section');" class="btn btn-lg btn-block add_newsection btn_addbg"><i class="fa fa-plus"></i>Add Section</button>
                     </div>
                </div>

                 <!--Curriculam-->                
    </div>

</div>
 
 </div>
 </div>


{{ Form::close() }}

@stop

@section('extrafooter')

<script src="//bsedemo.com/ckeditor/ckeditor.js"></script>
{{ HTML::script('assets/bsetec/static/js/createcourse/easyResponsiveTabs.js') }}
{{ HTML::script('assets/bsetec/static/js/createcourse/tab.js') }}
{{ HTML::script('assets/bsetec/static/js/croppic.min.js') }}

<?php $key = explode(',', $course->keywords);
$keywords = implode('" , "', $key);
$Keywordss =  '"'.$keywords.'"'; ?>

<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        var tabNum = 0;
        if(hash!='') {
            var matches = hash.match(new RegExp("verticalTab([0-9]+)"));
            if (matches!==null && matches.length===2) {
                tabNum = parseInt(matches[1],10);
            }
        }
        $('#currenttab').val(tabNum);
        var curtableft = $('#currenttab').val();
        if(curtableft==2) {
            $('.toggletabs').click();
        }
    });


    $("#tagit").tagsManager({
        prefilled: [{{$Keywordss}}],
        maxTags: {{tagLimit()}},
    });

    function AddFields(txt){
        $('#add_'+txt).show();
        $('#add_lec_qui').hide();
        if(txt == 'section'){
            $('#add_lecture').hide();
        }else{
            $('#add_section').hide();
        }
    }
    function CloseFields(txt){
        $('#add_'+txt).hide();
        $('#add_lec_qui').show();
        $('#AddQuizTitletxt').val('');
        $('#AddSectionTitletxt').val('');
        $('#AddLectureTitletxt').val('');
        $('.edit_title').val('');
        $('#ch_AddQuizTitletxt').html('80');
        $('.charCount').html('80');
        $('#ch_AddSectionTitletxt').html('80');
        $('#ch_AddLectureTitletxt').html('80');
        if(txt == 'Section'){
            var val = $('#current_sec').val();
            $('.cur_sec_count').html(parseInt(val)-parseInt(1));
            $('#current_sec').val(parseInt(val)-parseInt(1));
        }
    }

    function CloseEdit(class_name){
        $('.'+class_name).hide();
    }

    function AddTitle(title){
        var text = $('#Add'+title+'Titletxt').val();
        if($.trim(text) == ''){
            //alert('Empty value Not allowed');
             $('#errormsg').html('Empty Value Not allowed');
            return false;
        }
        var li_class = (title == "Section") ? 'main_li_curriculum' : 'sub_li_curriculum' ;
        var edit_text = "'"+title+"'";
        if(title != "Section"){
            var AddOption = '<a class="pull-right AddContentClass" onclick="return AddContent('+edit_text+',$(this))" href="javascript:void(0)"><i class="fa fa-plus color_icon"></i></a>';
        }else{
            var AddOption = '';
        }
        if(title == 'Section'){
            var val = $('#current_sec').val();
            //$('.cur_sec_count').html(parseInt(val)+parseInt(1));
            $('#current_sec').val(parseInt(val)+parseInt(1));
            $('.add_sec_count').html('&nbsp;'+(parseInt(val)+parseInt(1)));
            $('#ch_AddSectionTitletxt').html('80');
        }
        if(title == 'Lecture'){
            var val = $('#current_lec').val();
            //$('.cur_sec_count').html(parseInt(val)+parseInt(1));
            $('#current_lec').val(parseInt(val)+parseInt(1));
            $('.add_lec_count').html('&nbsp;'+(parseInt(val)+parseInt(1)));
            $('#ch_AddLectureTitletxt').html('80');
        }
        if(title == 'Quiz'){
            var val = $('#current_quiz').val();
            //$('.cur_sec_count').html(parseInt(val)+parseInt(1));
            $('#current_quiz').val(parseInt(val)+parseInt(1));
            $('.add_quiz_count').html('&nbsp;'+(parseInt(val)+parseInt(1)));
            $('#ch_AddQuizTitletxt').html('80');
        }

        $('#curriculum_ul').append('<li class="ui-state-default curriculum_ul_li ul_main ul_padd '+li_class+'">'
                        +'<a class="pull-right DeleteTitle" onclick="return DeleteTitleFunction($(this))" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>'
                        +'<a class="pull-right" onclick="return EditTitle('+edit_text+',$(this))" href="javascript:void(0)"><i class="fa fa-pencil-square-o color_icon"></i></a>'+AddOption
                        +'<a class="pull-left"><i class="fa fa-align-justify justify_color"></i></a> <span class="main_title cur_h3">'+title+'</span> '+val+': <span class="'+title+'PTage title_text cur_h3">'+text+'</span>'
                        +'<div class="'+title+'EditDiv" style="display:none"><input type="text" maxlength="80" class="'+title+'EditText form-control edit_title" name="'+title+'[]" value=""><span class="charCount">'+80+'</span>'
                        +'<input type="button" class="'+title+'EditButton btn" value="Save"></div><div class="Add'+title+'ContentDiv" style="display:none"><div class="add_contents"><div class="quiz_form span8"></div>'
                        +'<input type="hidden" class="lectureFileName"><input type="hidden" class="lectureFileType">'
                        +'<textarea class="form-control lectureDescription" style="display:none"></textarea></div></div>'
                        +'</li>');
        $('#Add'+title+'Titletxt').val(''); 
        
        SaveCurriculum();
    }
    function EditTitle(title,e){

        $('.'+title+'EditDiv').hide();
        var parent_li = e.parent('li');


        
        var text = parent_li.children('.'+title+'EditDiv').show();

        $('.'+title+'EditButton').click(function(){
            var parent = $(this).parent('div');
            var c= $('.'+title+'EditText').val();

            var parent_div = parent_li.children('div');
            
           var value = parent_div.children('input.LectureEditText').val();
            
            if($.trim(value) != ''){
                parent.prev('.'+title+'PTage').text(value);
                parent_li.children('span.title_text').html(value);
                $('.'+title+'EditDiv').hide();
                SaveCurriculum();
           
            }else{
                //alert('Empty value Not allowed');
                $('#errormsg').html('Empty value Not allowed');

            }
        });
        SaveCurriculum();
    }

    function AddContent(title,e){
        var parent_li = e.parent('li');
        if (title == 'Lecture') {
        parent_li.children('.Add'+title+'ContentDiv').find('.add_contents').load("{{url().'/admin/UploadLecture'}}").append();
        var getDivlength = parent_li.children('.Add'+title+'ContentDiv').find('.file_div').length;
        if(getDivlength == 1){
            parent_li.children('.Add'+title+'ContentDiv').slideDown();
            parent_li.children('.Add'+title+'ContentDiv').children('.add_contents').slideDown();
        }else{
            parent_li.children('.Add'+title+'ContentDiv').children('.Close_Div').show();
            parent_li.children('.Add'+title+'ContentDiv').children('.Close_Div').next('.file_div').show();
            parent_li.children('.Add'+title+'ContentDiv').slideDown();

            parent_li.children('.Add'+title+'ContentDiv').children('.add_contents').children('.fileupload').hide();

            var checkElement = parent_li.children('.Add'+title+'ContentDiv').find('.file_div');
            if(checkElement.length > 0){
                parent_li.children('.Add'+title+'ContentDiv').children('.add_contents').hide();
            }else{
                parent_li.children('.Add'+title+'ContentDiv').children('.add_contents').show();
                parent_li.children('.Add'+title+'ContentDiv').children('.add_contents').children('.Close_Div').hide();
            }
        }
        
        }else if(title == 'Quiz'){
        var form = $('#add_quiz_ques');
        var form_div = form.html();
        parent_li.children('.Add'+title+'ContentDiv').find('.add_contents .quiz_form').html(form_div);
        parent_li.children('.Add'+title+'ContentDiv').show();
        parent_li.children('.Add'+title+'ContentDiv').find('.add_contents').show();
        parent_li.children('.Add'+title+'ContentDiv').find('.file_div').show();
        $('.save_quiz').click(function(){
            var question = $(this).parent('.quiz_saveblocks').parent('.add_contents .quiz_form').find('.question_text').val();
            if(question == ''){
                //alert('Please enter the question');
                $('#errormsg').html('Please enter the question');
                return false;
            }
            var answers  = [];
            var correct_answers  = [];
            var options = [];
            $(this).parent('.quiz_saveblocks').parent('.add_contents .quiz_form').find('.answer_text').each(function(){
            var value = $(this).val();
            if (value) {
                answers.push($(this).val());
            }
            correct_answers.push($(this).prev('.correct_answer:checked').val()?1:0);
            var option = $(this).prev('.correct_answer:checked').val();
            if (option) {
                options.push(option);
            }
            });
            if(answers.length < 4){
                   //alert('Please fill the answers'); 
                    $('#errormsg').html('Please fill the answers');
                   return false;
            }
            if(options.length == 0){
                   //alert('Please choose answer'); 
                    $('#errormsg').html('Please choose answer');
                   return false;
            }
            var html_content = '<div class="file_div clearfix"><a class="pull-right delete_quiz" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a><p>'+question+'</p><input type="hidden" value="'+question+'" class="lectureFileName"/>'
                                +'<input type="hidden" value="'+answers+'" class="lectureFileType"/>'
                                +'<textarea class="form-control lectureDescription" style="display:none">'+correct_answers+'</textarea>'
                                +'</div>';
            parent_li.children('.Add'+title+'ContentDiv').prepend(html_content);
            parent_li.children('.Add'+title+'ContentDiv').find('.add_contents').hide();
            parent_li.children('.Add'+title+'ContentDiv').find('.file_div').hide();
            SaveCurriculum();
        }); 
            $('.cancel_quiz').click(function(){
            parent_li.children('.Add'+title+'ContentDiv').find('.add_contents').hide();
            parent_li.children('.Add'+title+'ContentDiv').find('.file_div').hide();
             });

            $('.delete_quiz').click(function(){
            var del_li = $(this).parent('.file_div');
            if (confirm("Are you sure want to delete?")) {
                del_li.remove();
                SaveCurriculum();
                return false;
            }
            return false;
             });
        }
    }

    function DeleteContentFunction(ThisContent){
        var del_li = ThisContent.parent('.file_div');
        var videoDelete = del_li.children('.lectureFileName').val();

        arr = videoDelete.split('/');
        FileDelete = arr[arr.length-1];

        if (confirm("Are you sure want to delete?")) {
            $(this).parents('.AddLectureContentDiv').find('.Close_Div').hide();

            $.ajax({
                url : "{{url().'/admin/deleteVideo'}}",
                type : "POST",
                data : { FileDelete:FileDelete },
                success:function(data){
                    del_li.remove();
                    $('.Close_Div').hide();
                    del_li.parent('div').next('.ImageForm').show();

                    del_li.parent('div').next('add_contents').children('.ImageForm').show();

                    del_li.next('.ImageForm').show();
                    SaveCurriculum();
                }
            });
        }
        return false;
    }

    function SaveCurriculum(){
        var main_title = [];
        var title = [];
        var file_name = [];
        var file_type = [];
        var file_description = [];
        $('#curriculum_ul .curriculum_ul_li').each(function(i){
            var child = $(this);

            main_title.push(child.find('.main_title').text());
            title.push(child.find('.title_text').text());
            var file_names = [];
            child.find('.lectureFileName').each(function(){
            file_names.push($(this).val());
            });
            file_name.push(file_names);

            var file_types = [];
            child.find('.lectureFileType').each(function(){
            file_types.push($(this).val());
            });
            file_type.push(file_types);

            var file_descriptions = [];
            child.find('.lectureDescription').each(function(){
            file_descriptions.push($(this).val());
            });
            file_description.push(file_descriptions);
        });
        var course_id = $('#course_id').val();
        $.ajax({
            url: "{{url().'/admin/SaveCurriculum'}}",
            type: "POST",
            data: { course_id:course_id,main_title:main_title,title:title,file_name:file_name,file_type:file_type,file_description:file_description },
            dataType: "json",
            success:function(data){
               // alert(data);
            }
        });
    }

function DeleteTitleFunction(ThisContent){

   // alert(ThisContent);
    var del_li = ThisContent.parent('li');
    if (confirm("Are you sure want to delete?")) {
        del_li.children('.AddLectureContentDiv').children('.file_div').children('.DeleteContent').trigger('click');
        del_li.remove();
        SaveCurriculum();
        if(title == 'Section'){
            var val = $('#current_sec').val();
            $('#current_sec').val(parseInt(val)+parseInt(1));
            $('#ch_AddSectionTitletxt').html('80');
        }
    }
    return false;  
}

    $(function(){

        $('#curriculum_ul').sortable({
            update: function () {
                SaveCurriculum();
            }
        });

        $('.delete_quiz').click(function(){
            var del_li = $(this).parent('.file_div');
            if (confirm("Are you sure want to delete?")) {
                del_li.remove();
                SaveCurriculum();
                return false;
            }
            return false;
        });
        
    });

$(function(){
    $('#verticalTab').easyResponsiveTabs({type: 'vertical',width: 'auto',fit: true});
    $(".GetStartedTitle").click(function(){$(this).next(".GetStartedContent").slideToggle( "slow" );});
    $('.sortable_ul').sortable();
    $('.checkSelected').click(function(){var status = $(this).is(":checked");(status == true) ? $(this).prev().prop('checked', false) : $(this).prev().prop('checked', true);});
    $("#get_instruction" ).autocomplete({source: {{json_encode($user_list)}},select: function(event,ui) {$('#get_id').val(ui.item.desc);}});
    $('#add_instruction').click(function(){
        var value = $('#get_instruction').val();
        var id = $('#get_id').val();
        if(value != ''){
            $('#insturction_tr').append('<tr><td>'+value+'<input type="checkbox" name="instructor_name[]" value="'+value+'" checked="checked" style="display:none;" /><input type="checkbox" name="instructor_id[]" value="'+id+'" checked="checked" style="display:none;" /></td><td><input type="checkbox" value="'+id+'" name="ins_visible[]"/></td><td><input type="checkbox" value="'+id+'" name="ins_edit[]"/></td><td><a class="delete_tr" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a></td></tr>');
            $('#get_instruction').val('');
            $('.delete_tr').on('click',function(){var td = $(this).parent();var tr = td.parent();tr.css("background-color","#FF3700");tr.fadeOut(400, function(){tr.remove();});}); 
        }
    });
    $('.delete_tr').on('click',function(){var td = $(this).parent();var tr = td.parent();tr.css("background-color","#FF3700");tr.fadeOut(400, function(){tr.remove();});});  

    $( "#video_iframe" ).load("{{url().'/admin/uploadVideo'}}");

   
    $( ".course_goal_ul" ).sortable({
        start:function(event, ui){ 
            $('#course_goal_text').text('');
        },
        stop: function(event, ui){ 
            var n = $( ".course_goal_ul li" ).length;
            $( ".course_goal_ul li" ).each(function(index){
                var text = $(this).text();
                if($('#course_goal_text').text() == ''){
                $('#course_goal_text').text(text);
                }
                else{
                var add_text = $('#course_goal_text').text()+'--txt--'+text;
                $('#course_goal_text').text(add_text);
                }
            });
        }
    });

    $(".int_audience_ul" ).sortable({
        start:function(event, ui){ 
            $('#int_audience_text').text('');
        },
        stop: function(event, ui){ 
            var n = $( ".int_audience_ul li" ).length;
            $( ".int_audience_ul li" ).each(function(index){
                var text = $(this).text();
                if($('#int_audience_text').text() == ''){
                $('#int_audience_text').text(text);
                }
                else{
                var add_text = $('#int_audience_text').text()+'--txt--'+text;
                $('#int_audience_text').text(add_text);
                }
            });
        }
    });

    $(".course_req_ul" ).sortable({
        start:function(event, ui){ 
            $('#course_req_text').text('');
        },
        stop: function(event, ui){ 
            var n = $( ".course_req_ul li" ).length;
            $( ".course_req_ul li" ).each(function(index){
                var text = $(this).text();
                if($('#course_req_text').text() == ''){
                $('#course_req_text').text(text);
                }
                else{
                var add_text = $('#course_req_text').text()+'--txt--'+text;
                $('#course_req_text').text(add_text);
                }
            });
        }
    });
});

function common(str){
    $( "."+str+"_ul li" ).each(function(index){
        var text = $(this).text();
        if($('#'+str+'_text').text() == ''){
        $('#'+str+'_text').text(text);
        }
        else{
        var add_text = $('#'+str+'_text').text()+'--txt--'+text;
        $('#'+str+'_text').text(add_text);
        }
    });
}

function delete_li(str,i){
    $('.'+str+i).parent('li').remove();
    $('#'+str+'_text').text('');
    common(str);
}

function common_add(strs){
    var val = $('#'+strs).val();
    if(val == ''){
        //alert('Empty Values Not allowed')
         $('#errormsg').html('Empty Value Not allowed');
    }else{
        var number = Math.floor(Math.random() * 102)
        $('.'+strs+'_ul').append('<li>'+val+'<a class="pull-right '+strs+number+' " onclick="delete_li(\''+strs+'\',\''+number+'\')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a><a class="pull-right" href="javascript:void(0)"><i class="fa fa-align-justify justify_color"></i></a></li>');       
        if($('#'+strs+'_text').text() == ''){
            $('#'+strs+'_text').text(val);
        } else{
            var add_text = $('#'+strs+'_text').text()+'--txt--'+val;
            $('#'+strs+'_text').text(add_text);
        }
        $('#'+strs).val('');
    }
}

// Publish Course 

function validate_click(attrValue){
    $.noConflict();
    var id = attrValue.attr('id');
    $('#publishcourseValidation').html('<img src="{{url().'/assets/bsetec/static/img/loading.gif'}}"/>');
    $('#PublisCourse').modal('toggle');
    $.ajax({
        url: "{{url('PublishCourse')}}",
        type: "POST",
        data: {id:id},
        success:function(data){
            if(data != ''){
            $('#publishcourseValidation').html(data);
            }else{
            window.location.href = "{{url(Auth::user()->username.'/myCourse')}}"
            }
        }
    });
}

$('.resp-tabs-list li').click(function () {
  $('#currenttab').val($(this).index()+parseInt(1));
});
$('.resp-tabs-list li.main-leftmenu').click(function () {
  if($('#currenttab').val()!=2) {
    $('#countrytabs').slideUp('slow');
  }
});
$('#AddLectureTitletxt').keyup(function () {
    var count= $(this).val().length;
    var rev_count = parseInt(80) - parseInt(count);
    $('#ch_AddLectureTitletxt').html(rev_count);
    if(rev_count == 0){
        $('#ch_AddLectureTitletxt').css('background','#f00');
        $('#ch_AddLectureTitletxt').css('color','#fff');
    }
    else{
        $('#ch_AddLectureTitletxt').css('background','#ddd');
        $('#ch_AddLectureTitletxt').css('color','#000');
    }
});
$('#AddSectionTitletxt').keyup(function () {
    var count= $(this).val().length;
    var rev_count = parseInt(80) - parseInt(count);
    $('#ch_AddSectionTitletxt').html(rev_count);
    if(rev_count == 0){
        $('#ch_AddSectionTitletxt').css('background','#f00');
        $('#ch_AddSectionTitletxt').css('color','#fff');
    }
    else{
        $('#ch_AddSectionTitletxt').css('background','#ddd');
        $('#ch_AddSectionTitletxt').css('color','#000');
    }
});
$('#AddQuizTitletxt').keyup(function () {
    var count= $(this).val().length;
    var rev_count = parseInt(80) - parseInt(count);
    $('#ch_AddQuizTitletxt').html(rev_count);
    if(rev_count == 0){
        $('#ch_AddQuizTitletxt').css('background','#f00');
        $('#ch_AddQuizTitletxt').css('color','#fff');
    }
    else{
        $('#ch_AddQuizTitletxt').css('background','#ddd');
        $('#ch_AddQuizTitletxt').css('color','#000');
    }
});
$('.edit_title').keyup(function() {
     var count= $(this).val().length;
     //var cv= $(this).val();
     //alert(cv);
    var rev_count = parseInt(80) - parseInt(count);
    $(this).next('.charCount').html(rev_count);
    if(rev_count == 0){
        $(this).next('.charCount').css('background','#f00');
        $(this).next('.charCount').css('color','#000');
    }
    else{
        $(this).next('.charCount').css('background','#ddd');
        $(this).next('.charCount').css('color','#000');
    }
});

// Publish Course

var croppicContaineroutputOptions = {
		onBeforeImgUpload: 	function(){ progress_bar(); },
		onAfterImgUpload: 	function(){ progress_bar_full(); },
        uploadUrl:'{{url()}}/admin/uploadimg',cropUrl:'{{url()}}/admin/cropimg',
        outputUrlId:'cropOutput',modal:false,
        loaderHtml:'<div class="loader bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span></div> '
}
var cropContaineroutput = new Croppic('cropContaineroutput', croppicContaineroutputOptions);


$('#ImageDetails').on('change', '.cropContaineroutput_imgUploadForm input', function() {
	filen = this.files[0].name;
	var extension = filen.substr( (filen.lastIndexOf('.') +1) );
	if(extension != 'jpg' && extension != 'jpeg' && extension != 'gif' && extension != 'png' && extension != 'bmp'){
		$('#error_upload').val('1');
		$('.progress-error').html('Image files must be .jpg, .jpeg, .gif, .png, .bmp.');
		$('.progressbar-wrapper').hide();
		$('.progressbar-value').css("width", "0px");
		clearInterval(timer);
		return false;
	}
	var fr = new FileReader;
    fr.onload = function() {
        var img = new Image;
        img.onload = function() {
			if(img.width < 480 || img.height < 270) {
				$('#error_upload').val('1');
				$('.progress-error').html('Image resolution must be at minumum 480x270 pixels');
				$('.progressbar-wrapper').hide();
				$('.progressbar-value').css("width", "0px");
				clearInterval(timer);
			}
        };
        img.src = fr.result;
    };
    fr.readAsDataURL(this.files[0]);
	var a=(this.files[0].size);
	if(a > 1048576) {
		$('#error_upload').val('1');
		$('.progress-error').html('Image size must be at maximum 1MB');
		$('.progressbar-wrapper').hide();
		$('.progressbar-value').css("width", "0px");
		clearInterval(timer);
		return false;
	}
});
var timer = '';
function progress_bar(){
	$('#error_upload').val('0');
	$('.progress-error').html('');
	$('.progressbar-value').css("width", "0px");
	$('.progressbar-wrapper').show();
	var perc = 0;
	timer = setInterval(function(){
		var now_width = $('.progressbar-value').width();
		if(now_width == 400){
			clearInterval(timer);
		}
		if(perc < 400 && now_width < 400){
			$('#ImageDetails').find('.notes-table').css({'height':'150px','text-align':'center','padding-top':'120px'});
			$('#ImageDetails').find('.notes-table').html('Upload in progress '+ parseInt(perc)/parseInt(4) + '%');
			$('.progressbar-value').css("width", perc+"px");
			perc = perc+4;
		} else {
			perc = 0;
		}
	}, 150);
}

function progress_bar_full(){
		if($('#error_upload').val() == '1'){
			cropContaineroutput.reset();
			$('#ImageDetails').find('.notes-table').html('Image upload failed');
			clearInterval(timer);
		} else {
			setTimeout(function(){
				$('.progressbar-value').css("width", "400px");
				$('#ImageDetails').find('.notes-table').html('Upload in progress 100%');
			}, 100);
			setTimeout(function(){
				$('.progressbar-wrapper').hide();
				$('#ImageDetails').find('.notes-table').html('Image upload complete');
			}, 2000);
		}
}

$('.privacy_pswd').click(function () {
    $('#privacy_password').attr('disabled', false);
});
$('.privacy_inv').click(function () {
    $('#privacy_password').attr('disabled', true);
});
$('.privacy_pub').click(function () {
    $('#privacy_password').attr('disabled', true);
});


$('.toggletabs').click(function () {
    $('#countrytabs').slideToggle();
    if($('.growsign').html() == '+'){
  $('.growsign').html('-');
 } else {
  $('.growsign').html('+');
 }
});

$('.toggle-price-form').click(function () {
    $('#pricetab').slideToggle();
});

$('.set_price_free').click(function () {
    $('.price_text').val('');
});


$(document).on('click','h2.newresp-accordion',function () {
	var att = $(this).attr('aria-controls');
	if(att == 'tab_item-2'){
		$('.tabconts').slideUp();
		$('#BasiceDetailes').slideDown();
	} else if(att == 'tab_item-3'){
		$('.tabconts').slideUp();
		$('#DetailDetailes').slideDown();
	} else if(att == 'tab_item-4'){
		$('.tabconts').slideUp();
		$('#ImageDetails').slideDown();
	} else if(att == 'tab_item-5'){
		$('.tabconts').slideUp();
		$('#VideoDetails').slideDown();
	} else if(att == 'tab_item-6'){
		$('.tabconts').slideUp();
		$('#PrivacyDetailes').slideDown();
	} else if(att == 'tab_item-7'){
		$('.tabconts').slideUp();
		$('#PricingDetails').slideDown();
	}
	$('.resp-tab-content').each(function () {
		var att2 = $(this).attr('aria-labelledby');
		if(att2 == 'tab_item-1'){
			$(this).slideDown();
		} else {
			$(this).slideUp();
		}
	});
});


$(document).ready(function(){
	$('h2.resp-accordion').each(function () {
		var att = $(this).attr('aria-controls');
		if(att == 'tab_item-2'){
			$(this).remove();
		} else if(att == 'tab_item-3'){
			$(this).remove();
		} else if(att == 'tab_item-4'){
			$(this).remove();
		} else if(att == 'tab_item-5'){
			$(this).remove();
		} else if(att == 'tab_item-6'){
			$(this).remove();
		} else if(att == 'tab_item-7'){
			$(this).remove();
		}
	});
});

var countries = new ddtabcontent("countrytabs")
countries.setpersist(true)
countries.setselectedClassTarget("link") //"link" or "linkparent"
countries.init();

 function isNumberKey(evt)
  {
     var inputVal = $(this).val();
     var floatRegex = '[-+]?([0-9]*.[0-9]+|[0-9]+)'; 
     if(!floatRegex.test(inputVal)) { 
        return false;
        document.getElementById("price_span").innerHTML="Please Enter number/float number only ";
     }
     return true;
  }
</script>

@stop