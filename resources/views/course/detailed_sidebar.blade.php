<div class="col-md-3">
    @include('master/share')
    @if(strlen($course->user->about_me) > 10)
    <div class="clearfix">
        <h4>
            <span>{!! Lang::get('core.about')!!} <small>{{ $course->user->fullname }}</small></span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            <p>{{   Str::limit($course->user->about_me,150) }} </p>
        </div>
    </div>
    <hr>
    @endif

    @if($course->user->course()->where('approved','=',1)->count() > 2)
    <div class="clearfix">
        <h4>
            <span>{{ t('More From') }} <small>{{ $course->user->fullname }}</small></span>
        </h4>
    </div>
        <hr>
        <div class="imagesFromUser">
            @foreach($course->user->course()->where('approved','=',1)->take(12)->get() as $sidebarImage)
            <a href="{{ url('course/'.$sidebarImage->id.'/'.$sidebarImage->slug) }}"class="pull-left userimage">
                <img src="{{ asset(zoomCrop('uploads/course/'.$sidebarImage->image_name ,69,69)) }}" alt="{{ $sidebarImage->title }}" class="thumbnail">
            </a>
            @endforeach
        </div>
    @endif


    @if(getFeaturedUser()->count() >= 1)
    <div class="clearfix">
        <h4>
            <span>{{ t('Featured User') }}</span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            @foreach(getFeaturedUser() as $featuredUser)
            <div class="col-md-12">
                <div class="row">

                    <a href="{{ url('user/'.$featuredUser->username) }}" class="thumbnail pull-left">
                        <img src="{{ avatar($featuredUser->avatar,69,69) }}" alt="{{ $featuredUser->fullname }}">
                    </a>

                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p><strong><a href="{{ url('user/'.$featuredUser->username) }}">{{ $featuredUser->fullname }}</a></strong></p>
                        @if(Auth::check())
                        @if(checkFollow($featuredUser->id))
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">Un Follow</button>
                        @else
                        <button class="btn btn-default btn-xs replyfollow follow" id="{{ $featuredUser->id }}">Follow Me</button>
                        @endif
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    @endif

    <div class="clearfix">
        <h4>
            <span>{{ t('More From') }} {{ siteSettings('siteName') }}</span>
        </h4>
    </div>
        <div class="imagesFromUser">
            @foreach(moreFromSite() as $sidebarImage)

            <a href="{{ url('course/'.$sidebarImage->id.'/'.$sidebarImage->slug) }}"class="pull-left userimage">
                <img src="{{ asset(zoomCrop('uploads/'.$sidebarImage->image_name ,69,69)) }}"
                     alt="{{ $sidebarImage->title }}" class="thumbnail">
            </a>

            @endforeach
        </div>
    


    @if($course->favorite->count() >= 1)
    <hr>
    <div class="clearfix">
        <h4>
            <span>{{ t('Favorites') }} <small class="pull-right">{{ $course->favorite->count() }}</small></span>
        </h4>
        <hr>
        <div class="imagesFromUser">
            @foreach($course->favorite()->take(10)->get() as $sidebarImage)
            <a href="{{ url('user/'.$sidebarImage->user->username) }}"class="pull-left userimage">
                <img src="{{ avatar($sidebarImage->user->avatar ,69,69) }}" alt="{{ $sidebarImage->user->fullname }}" class="thumbnail">
            </a>
            @endforeach
        </div>
    </div>
    @endif

</div>