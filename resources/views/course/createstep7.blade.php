@extends('layouts.frontend')
@section('content')
@if (defined('CNF_CURRENCY'))
        @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
@endif
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course privacy">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			@include('course.courseheader')
		</div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">

			<div class="lach_dev resp-tab-content course_tab"> 
				<div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						
                        <div class="course_basic course_newbasic"><h4>{!! Lang::get('core.price')!!}</h4><p>{!! Lang::get('core.price_tips')!!} {!! $currency !!}</p></div>
						
						@if(!$priceinfo_complettion)
			<div class="alert alert-warning pricing-alert clearfix" role="alert"><p><i class="fa fa-exclamation-triangle"></i><p>{!! Lang::get('core.price_txt')!!}</p><a href="{!! url('user/instructorinfo')  !!}" class="btn btn-color">{!! Lang::get('core.Apply')!!}</a></div>
						@elseif($priceinfo_complettion && ($course->pricing =='0' || $course->pricing ==''))
						<div class="alert alert-warning pricing-alert clearfix" role="alert"><p><i class="fa fa-exclamation-triangle" style="font-size:30px;"></i><p>{!! Lang::get('core.paid_course')!!}<br/>
							</p></div>
						@endif	


							<div class="des_margin price_settings">
								<!--<label for="description">Price Settings:</label>-->
								<div class="price-holder">
									<span class="price-label">{!! Lang::get('core.current_course')!!} </span>
									
									@php ( $pri=SiteHelpers::getCurrencymethod($course->user_id,$course->pricing))
									<span class="new_price">{{{$course->pricing == 0 ? Lang::get('core.free') : $pri}}}</span>
								</div>
								@if($priceinfo_complettion)
								<p>{!! Lang::get('core.here')!!} <a class="toggle-price-form" href="javascript:void(0)"> {!! Lang::get('core.click_here') !!} </a> {!! Lang::get('core.change_price') !!}</p>
								@endif
								<div id="pricetab" style="display:none;" class="pricetab-block">
									<ul id="myTab2" class="privacy_tabs nav nav-tabs tab_public">
										<li class="{{{$course->pricing == 0 ? 'active' : ''}}}"><a class="selected set_price_free" href="#free2" data-toggle="tab">{!! Lang::get('core.free')!!}</a></li>
										<li class="{{{$course->pricing == 0 ? '' : 'active'}}}"><a class="selected" href="#paid2" data-toggle="tab">{!! Lang::get('core.Paid')!!}</a></li>
									</ul> 	
									<div class="tab-content">
										<div class="tab-pane fade clearfix {{{$course->pricing == 0 ? 'in active' : ''}}}" id="free2">
											<div class="space_btw">{!! Lang::get('core.available')!!}</div>
										</div>

										<div class="tab-pane fade clearfix {{{$course->pricing == 0 ? '' : 'in active'}}}" id="paid2">
											<ul class="tab_ul price_section">
                                            
                                            <div class="input-group">
                                            <span class="price_dollar_span">$</span> 
												<input type="number" min="0" required name="price" class="price_text form-control" placeholder="{!! Lang::get('core.set_price')!!}" id="box1" value="{{$course->pricing}}" />
												<span id="price_span"></span>
                                            </div>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="form-group pricetab_btn" id="pricetab" style="display:none;">
							<input type="submit" name="" value="{!! Lang::get('core.sb_save')!!}" class="btn btn-color">
				
							</div>
							{!! Form::close() !!}

						</div>
					</div>
				<!-- coupon process start -->
				@if($priceinfo_complettion && $course->pricing !='0' && $course->pricing !='')
				<div id="coupon" class="coupon-s">
			        <h3>{!! Lang::get('core.coupon_code')!!}</h3>
			   <!--     <button class="btn btn-info" data-toggle="modal" data-target="#couponModal">Create a Coupon Code</button>-->
                    <a class="btn btn-info" href="javascript:void(0)" data-toggle="modal" data-target="#couponModal" onclick="resetForm();">{!! Lang::get('core.coupon_create')!!}</a>
			        <p>{!! Lang::get('core.coupon_msg')!!}</p>
				</div>
				@else
				<div style="text-align:center;padding:15px;" id="coupon">
			        <h2>{!! Lang::get('core.Coupon_Codes')!!}</h2>
			       <a class="btn btn-info disabled" href="javascript:void(0)">{!! Lang::get('core.coupon_create')!!}</a>
			        <p style="text-align:center;padding:15px;">{!! Lang::get('core.coupon_msg')!!}</p>
				</div>
				@endif
				
				@if(count($couponinfo) > 0 && $course->pricing !='0' && $course->pricing !='')
                <div class="price-b">
				<a href="javascript://ajax"  onclick="coupon_delete();" class="tips btn btn-sm btn-white" title="{{ Lang::get('core.btn_remove') }}">
				<i class="fa fa-minus-circle "></i>&nbsp;{{ Lang::get('core.btn_remove') }}</a>
				<div class="table-responsive">
   				 <table class="table table-striped ">
		        <thead>
					<tr>
						<th class="number"> {!! Lang::get('core.No')!!} </th>
						<th> <input type="checkbox" class="checkall" /></th>
						<th> {!! Lang::get('core.coupon_code')!!}</th>
						<th> {!! Lang::get('core.Coupon_Type')!!}</th>
						<th> {!! Lang::get('core.Coupon_Value')!!}</th>
						<th> {!! Lang::get('core.Start_Date')!!}</th>
						<th> {!! Lang::get('core.End_Date')!!}</th>
						<th> {!! Lang::get('core.Status')!!}</th>
						<th width="70" >{{ Lang::get('core.btn_action') }}</th>
			 		</tr>
				</thead>
				
			@php ($i=1)
				@foreach($couponinfo as $coupon)
				<tr>
				<td>{!! $i++ !!}</td>
				<td><input type="checkbox" class="ids" name="coupon_id[]" value="{!! $coupon->id !!}"  /></td>
				<td>{!! $coupon->coupon_code !!}</td>
				<td>{!! ($coupon->coupon_type == 1) ? Lang::get('core.Percentage') : Lang::get('core.Fixed_Price') !!}</td>
				<td>{!! $coupon->coupon_value !!}</td>
				<td>{!! $coupon->coupon_start_date !!}</td>
				<td>{!! $coupon->coupon_end_date !!}</td>
				<td>{!! ($coupon->coupon_status == 1) ? Lang::get('core.fr_mactive') : Lang::get('core.fr_minactive') !!}</td>
				<td><a onclick="coupon_edit({{ $coupon->id }});" class="tips btn btn-xs btn-white edit-icon" title="{{ Lang::get('core.btn_edit') }}"><i class="fa fa-edit "></i></a></td>
				</tr>
				@endforeach
					</table>
				 <div class="page_count pull-right"> 
	                    @if(count($couponinfo) > 0)
	                        @if(isset($search_page))
	                            {!! str_replace('/?', '?q='.$search.'&', $couponinfo->render()) !!}
	                        @else
	                            {!! str_replace('/?', '?', $couponinfo->render()) !!}
	                        @endif
	                    @endif
	                </div>
				@endif
				</div>
                </div>
<!-- coupon process list end -->
				</div> 
			</div>
		</div>
	</div>
</div>
@if($course->pricing !='0' && $course->pricing !='' && $priceinfo_complettion)
<div class="modal fade model_width" id="couponModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog paypal_popup">
        <div class="modal-content">
            <div class="modal-header header_bg">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h2 class="modal-title">{!! Lang::get('core.coupon_create')!!}</h2>
            </div>
            <div class="modal-body cont">
            <div class="tab-pane">
             {!! Form::open(array('id'=>'coupon-form', 'parsley-validate'=>'')) !!}
              {!! Form::hidden('course_id', $course->course_id )  !!}
              {!! Form::hidden('coupon_id', null ,array('id'=>'coupon_id') )  !!}
               <div id="coupon-errors"></div>
                <div class="form-group" id="coupon_code_div" style="display:none;">
	                    <label class="col-sm-4">{!! Lang::get('core.Coupon_Codes')!!}</label>
	             		    <div class="col-xs-4 col-sm-4">
	                     	<span id="coupon_code" class="coupon_code"></span>     
	                      </div>
	                    </div>
                    <div class="form-group">
	                    <label class="col-sm-4"> {!! Lang::get('core.Coupon_Type')!!}</label>
	             		    <div class="col-xs-8 col-sm-8">
	                          <div class="select-style_block">
	                          {!! Form::select('coupon_type', array('' => 'Select Type', '1' => 'Percentage','2'=>'Fixed Price'), '',array('class'=>'form-controller','required'=>true,'id'=>'coupon_type')); !!}
	                      </div>
	                    </div>
                    </div>

                    <div class="form-group">
                       <label class="control-label col-sm-4 text-left">  {!! Lang::get('core.Coupon_Value')!!}</label>
                       <div class="col-sm-8">
                        {!! Form::input('number','coupon_value', null,array('class'=>'form-control','required'=>true,'placeholder'=>'0','min'=>0.1,'step'=>'any','autocomplete'=>'off','id'=>'coupon_value')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                       <label class="control-label col-sm-4 text-left">  {!! Lang::get('core.Description')!!}</label>
                       <div class="col-sm-8">
                        {!! Form::textarea('coupon_desc',null ,array('class'=>'form-control','id'=>'coupon_desc')) !!} 
                        </div>
                    </div>
               
                    <div class="form-group">
                       <label class="control-label col-sm-4 text-left">  {!! Lang::get('core.Start_Date')!!}</label>
                       <div class="col-sm-8">
                        {!! Form::text('coupon_start_date',null ,array('class'=>'form-control date','id'=>'coupon_start_date','autocomplete'=>'off')) !!} 
                        </div>
                    </div>
                    <div class="form-group">
                       <label class="control-label col-sm-4 text-left"> {!! Lang::get('core.End_Date')!!}</label>
                       <div class="col-sm-8">
                        {!! Form::text('coupon_end_date',null ,array('class'=>'form-control date','id'=>'coupon_end_date','autocomplete'=>'off')) !!} 
                        </div>
                    </div>
                      <div class="form-group">
	                    <label class="control-label col-sm-4"> {!! Lang::get('core.Coupon_Status')!!}</label>
	             		    <div class="col-sm-8">
	             		    <label for="coupon_status">
	             		    {!! Form::radio('coupon_status',1,"",['id'=>'active']) !!} {!! Lang::get('core.fr_mactive')!!}
	             		    </label>	 
	             		    <label for="coupon_status">
	             		    {!! Form::radio('coupon_status',0,"",['id'=>'inactive']) !!} {!!Lang::get('core.fr_minactive')!!}
	             		    </label>
	                    </div>
                    </div>

                    <div class="form-group">
                    <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!!Lang::get('core.sb_submit')!!}</label></div>
                          <div class="col-sm-8">
                            <button type="submit" id="submit_button" class="btn btn-color"><span id="loader" style="display:none"><i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp; Please wait </span><span id="button_text">{!! Lang::get('core.Add_Coupon')!!}</span></button>
                        </div>
                    </div> 
                     {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var coupon_error = $('#coupon-errors');
var submit_button = $('#submit_button');
	$('#coupon-form').submit(function(e) {
		submit_button.attr('disabled','disabled');
		$('#button_text').toggle();
		$('#loader').toggle();
		coupon_error.html("");
		var form = $(this);
		$.ajax({
		    url: "{{ URL::to('course/coupon')}}",
		    type: "post",
		    data: form.serialize(),
		    dataType: "json",
		}).done(function(data){
		    if(data.success){
		    	location.reload(true);
		    }
		}).error(function(data) { // the data parameter here is a jqXHR instance
			var errors = data.responseText;
			res = $.parseJSON(errors);
    		error_html = '<div class="alert alert-danger"><ul>';
			$.each(res.errors,function(index, value) {
				error_html += '<li>'+ value[0] +'</li>';
		    });
    		error_html +='</ul></div>';
    		coupon_error.html(error_html);
    		submit_button.removeAttr('disabled');
    		$('#button_text').toggle();
			$('#loader').toggle();
    		return false;
	    });
		return false;
	});

function coupon_edit(id){
	$('#coupon_code_div').toggle();
	coupon_error.html("");
    $.ajax({
        url:'{!! url('').'/course/coupon'!!}',
        data:{coupon_id:id},
        type:"GET",
        success:function(res){
        	$('#coupon_code').text(res.coupon_code);
        	//$('#coupon-form').closest('form').find("input[type=text], input[type=number], select, textarea").val("");
			$('#coupon_id').val(res.id);
			$('#coupon_desc').val(res.coupon_desc);
        	$('#coupon_type').val(res.coupon_type);
        	$('#coupon_value').val(res.coupon_value);
        	$('#submit_button').val("Update");
        	if(res.coupon_start_date !='0000-00-00')
        		$('#coupon_start_date').datepicker('update', res.coupon_start_date);

        	if(res.coupon_end_date !='0000-00-00')
				$('#coupon_end_date').datepicker('update', res.coupon_end_date);
        	if(res.coupon_status == 1){
        		$('#active').iCheck('check');
        	}else{
        		$('#inactive').iCheck('check');
        	}
			$('#couponModal').modal('show');
    	},
        fail:function(){
            alert("error");
        }
    });   
}

function coupon_delete(){
	var total = $('input[class="ids"]:checkbox:checked').length;
	if(total > 0){
	if(confirm('are u sure removing selected rows ?'))
	{
		var checkedIDs = $('input[class="ids"]:checkbox:checked').map(function() {
		    return this.value;
		}).get();	  
	  	 $.ajax({
	        url:'{!! url('').'/course/coupon'!!}',
	        data:{coupon_id:checkedIDs},
	        type:"DELETE",
	        success:function(res){
	        	if(res.success)
	        		location.reload(true);
	        },
	        fail:function(){
	            alert("error");	        
   		}   
	  });
	}
}	  else
	  {
	  	alert("Please Select coupon");
	  }
}

function resetForm(){
	coupon_error.html("");
	$('#coupon_code_div').hide();
	$('#coupon_id').val("");
	$('#coupon_desc').val("");
	$('#coupon_type').val("");
	$('#coupon_value').val("");
	$('#submit_button').val("Add Coupon");
	$('#coupon_start_date').val("");
	$('#coupon_end_date').val("");
	$('#active').prop("checked", false);
	$('#inactive').prop("checked", false );
	$('.checked').removeClass("checked");
}

</script>
@endif
<script>
	$(function(){

		$('.toggle-price-form').click(function () {
			$('#pricetab,#coupon').slideToggle();
		});

		$('.set_price_free').click(function () {
			$('.price_text').val('0');
		});
		$('.price_text').keypress(function(){
				var inputVal = $(this).val();
				
			  	var floatRegex = /^[-+]?([0-9]*.[0-9]+|[0-9]+$)/; 
				if(!$(this).val().match(inputVal)) { 
    		 	return false;
        		document.getElementById("price_span").innerHTML="Please Enter number/float number only ";
     			}
     			return true;
		});

	});
</script>
	@stop


