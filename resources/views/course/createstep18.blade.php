@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/taginputs/jquery.tagsinput.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ URL::to('assets/bsetec/js/plugins/taginputs/jquery.tagsinput.js') }}"></script>
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">
		
			 <div class="lach_dev resp-tab-content course_tab"> 
                <div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						<div class="course_basic course_newbasic"><h4>{!! Lang::get('core.Meta_Info')!!}</h4><p>{!! Lang::get('core.meta_txt') !!}</p></div>
						
						
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Meta_key') !!}</label>
							
				{!!  Form::text('meta_key', $course->meta_keywords, array('class'=>'form-control', 'id'=>'keyword', 'placeholder'=>'add a keyword' ))  !!}
							
						</div>

						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Meta_des') !!}</label>
						
				{!!  Form::textarea('Meta_Description', $course->meta_description, array('class'=>'form-control', 'placeholder'=>'add a description' ))  !!}	

						</div>

                          <div class="row">
                         <div class="col-sm-6">


						<div class="form-group">
<input type="submit" name="" value="Save" class="btn btn-color"> 
						
						</div>
						{!! Form::close() !!}

					</div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
</div>


<script type="text/javascript">
	$('#keyword').tagsInput();
</script>
@stop