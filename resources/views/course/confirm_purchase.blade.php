<style>
input[type=number] {
    -moz-appearance:textfield;
}
.payment-block .form-group label.error{
    color:red;
}
</style>
<div class="confirm-section">
<div class="container">                
<h2 class="confirm-title">Confirm Purchase</h2>    
<div> Wallet Balance : <span>{{ $user_info->wallet }}</span></div>

    <div class="table-payment">  
        <div id="couponError"></div>       
        <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
        <tr>
        <!-- <th width="10%">Sl No:</th> -->
        <th width="90%">{!! Lang::get('core.name_content') !!}</th>
        <th width="10%">{!! Lang::get('core.price')!!}</th>
        </tr>
        <tr>
        @if (defined('CNF_CURRENCY'))
            @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
        @endif
        <td>{{$course->course_title}}</td>
        <td> {{$currency .' '. $course->pricing}}</td>
        </tr>

        @if($admin_discount)
        <tr>
        <td>{!! Lang::get('core.admin_discount') !!}</td>
        <td> {{ $currency.' '.$discount_amount }}</td>
        </tr>
        @endif    

        <tr id="coupon_discount_div" style="display:none;">
        <td>{!! Lang::get('core.coupon_discount') !!}</td>
        <td>{!! $currency !!} <span id="coupon_discount"></span></td>
        </tr>

        <tr>
        <td colspan="2">
        @if(\bsetecHelpers::checkcoupon($course->course_id))
        <h4 class="redeem">{!! Lang::get('core.redeem') !!}</h4>
        <div class="hide_block clearfix" id="coupon_code_div">
            <input type="text" id="couponCode" onClick="$('#checkCoupon').removeAttr('disabled');" class="" autocomplete="off" placeholder="{!! Lang::get('core.enter_coupon')!!}">
            <button class="btn btn-success" type="button" id="checkCoupon">{!! Lang::get('core.Apply')!!}</button>     
        </div>
        @endif
        </td>
        </tr>
        <tr>
        <td style="text-align:left;">{!! Lang::get('core.you_pay') !!}</td>
        <td>{!! $currency !!} <span id="you_pay">{{$you_pay}}</span></td>
        </tr>
        </table>     
    </div>  

    {!! Form::open(array('url' => url('wallet/takecourse'))) !!}
    {!! Form::hidden('course_id', $course->course_id) !!}
    {!! Form::hidden('course_slug', $course->slug) !!}
    {!! Form::hidden('final_pay', $you_pay, array('class'=>'final_pay')) !!}
    {!! Form::hidden('wallet_balance', '', array('class'=>'wallet_balance')) !!}
    <div style="float: right;" class="">
    <div class="btn purchase" data-wallet="{{ $user_info->wallet }}" id="purchase"><button type="submit" class="btn btn-color">Purchase</button></div>
    </div>
    {!! Form::close() !!} 

    <div class="payment-block clearfix"></div>   
    <div class="modal fade" id="purchaseModal" role="dialog" >
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="myModalLabel">~ </h4>
          </div>
          <div class="modal-body">
            <p class="price_error alert-danger"></p>
            <a class="" id="addwallet" href="{{url('user/wallet')}}"><span class="btn btn-color">Add Wallet</span></a>
            <span> (Or) </span>
            <br><br>
            {!! Form::open(array('url' => url('payment/courseform'))) !!}
            {!! Form::hidden('course_id', $course->course_id) !!}
            {!! Form::hidden('page_type', 2) !!}
            <a class=""><button type="submit" class="btn btn-color"> Pay Via Transaction </button></a>
            {!! Form::close() !!}
            <br>
          </div>
        </div>
      </div>
    </div>
</div>
</div>

<script type="text/javascript">
// COUPON PROCESS
$(document).on('click', '#checkCoupon', function(){
  var couponCode = $('#couponCode');
  var couponError = $('#couponError');
  var coupon_discount = $('#coupon_discount');
  var coupon_div =  $('#coupon_discount_div');
  var you_pay = $('#you_pay');
  var coupon_code_div =  $('#coupon_code_div');
  $(this).attr('disabled','disabled');
  courseID = "{{$course->course_id}}";
  couponError.html("");
  if(couponCode.val().length > 4){
    $.ajaxSetup({
          headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
    });
    $.ajax({
      url: '{{ \URL::to("course/couponcheck") }}',
      type: 'POST',
      dataType:'json',
      data:{ code : couponCode.val(), course_id : courseID },
      }).done(function(res){
           if(res.success_message){
            console.log(res);
              couponError.html('<div class="alert alert-success">'+res.success_message+'</div>');
              coupon_div.show();
              coupon_discount.text(res.discount_amount);
              you_pay.text(res.amount);
              coupon_code_div.slideToggle();
              couponCode.val("");
              $('.final_pay').val(res.amount);
              $('.redeem').addClass('hide');
            }
        }).error(function(data) {
            coupon_div.hide();
            var errors = data.responseText;
            res = $.parseJSON(errors);
            couponError.html('<div class="alert alert-danger">'+res.errors+'</div>');
            $(this).removeAttr('disabled');
            couponCode.val("");
            $('.redeem').removeClass('hide');
       });

  } else {
    couponError.html('<div class="alert alert-danger">{!! Lang::get("core.coupon_code_error") !!}</div>');
    $('.redeem').removeClass('hide');
  }

  setTimeout(function() {
    $('.alert').fadeOut();
  },3000);
});

$(document).on('click', '.purchase', function(){
    wallet_amount = parseInt($(this).data('wallet'));
    final_price = parseInt($('#you_pay').text()); 
    if(wallet_amount<final_price){
        $(".price_error").text("Wallet amount is too low. You can't purchase this course.");
        $('#purchaseModal').modal({
            show : true,
            backdrop: 'static',
            keyboard: false
        });
        return false;
    }else {
        wallet_balance = wallet_amount - final_price;
        $('.wallet_balance').val(wallet_balance);
        return true;
    }
});
</script>
<style>
.payment-errors{
    color:red;
}
</style>
