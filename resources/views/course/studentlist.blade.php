@if(count($students)>0)
@foreach($students as $results)
@php  $sid = $results->taken_id;  @endphp
<li class="slitems">
  <a href="{!! URL::to('profile/'.$results->username) !!}">
    {!! SiteHelpers::customavatar($results->email,$results->user_id) !!}
  </a>
</li>
@endforeach

@else
<li>{!! Lang::get('core.no_more') !!}</li>
@endif

 @if(!empty($sid) && count($nextrec)>0)
<ul id="spagination-{!! $sid !!}" class="pagination" >
  <li class="next"><a href="javascript:void(0);" data-next="{!! $sid or '' !!}" data-cid="{!! $courseid !!}" class="seestudents"> {!! Lang::get('core.see_more_comments') !!}</a></li>
</ul>
@endif