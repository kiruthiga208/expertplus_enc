<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{!! Lang::get('core.certificate')!!}</title>
</head>

<body>
<div style="width:575px; margin:0 auto; border:2px solid #8a694a;">
<div style="padding:20px; background:#0f0b07;">
<div style="border:3px solid #a6937e;">
<div style="border:6px solid #0f0b07;">
<div style="background:url('{{ asset('assets/bsetec/images/certificate_background.png') }}') no-repeat; height:666px; width:100%; position:relative;">
<div style="text-align:center; padding-top: 40px; margin-bottom:15px;"><img src="{{ asset('assets/bsetec/images/certificate_logo.png') }}"></div>
<h2 style="color: #4a0f0f; font-family: arial; font-size: 42px; margin: 0; text-transform: uppercase; text-align:center;">{!! Lang::get('core.certificate')!!}</h2>
<h4 style="color: #4a0f0f; font-family: arial; font-size:17px; margin-top:17px; text-transform: uppercase; text-align:center;">{!! Lang::get('core.cert_appreciation') !!}</h4>
<h5 style="color: #4a0f0f; font-family: arial; font-size:15px; margin-top:30px; text-transform: uppercase; text-align:center; margin-bottom:0px;">{!! Lang::get('core.cert_to') !!}</h5>
<h3 style="color: #4a0f0f; font-family: arial; font-size:32px; margin-top:30px; text-transform: uppercase; text-align:center; margin-bottom: 0;">{{ $name }}</h3>
<div style="width:300px; margin:0 auto;">
<h5 style="color: #4a0f0f; font-family: arial; font-size:16px; margin-top:30px; text-transform: uppercase; text-align:center; margin-bottom:0px;">{{$course_title}}</h5>
<p style="color: #4a0f0f; font-family: arial; font-size:13px; margin-top:7px; text-align:center; line-height:20px; max-height:8em; overflow:hidden; margin-bottom: 0;">{{ substr(ucfirst(strip_tags($description)),0,100) }}@if(strlen($description)>=100){{"....."}}@endif</p>
</div>
<div style="text-align:center; clear:both;"><img src="{{ asset('assets/bsetec/images/certificate_seal.png') }}" style="margin-top:25px;"></div>
<p style="text-transform:capitalize; font-size:13px; color: #4a0f0f; font-family: arial; text-align:center; margin-top: 20px;">{{date('l')}}: {{date('d/m/Y')}}</p>



</div>

</div>

</div>
</div>