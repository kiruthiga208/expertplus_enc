@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<script>
$(document).ready(function(){
	$('body').addClass('feedback');
	});
</script>
<div class="create-course">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			@include('course.courseheader')
		</div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">

			<div class="lach_dev resp-tab-content course_tab"> 
				<div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}

                        <div class="course_basic course_newbasic "><h4>{!! Lang::get('core.Course_Feedback')!!}</h4><p>{!! Lang::get('core.feedback_txt')!!}</p></div>
						
						@if(count($feedback)>0)
						@php  $k = 0; $i=1; @endphp
						@foreach($feedback as $key => $value)
						@if($value->feedback_type=='0') @php  $class=''; $clear='details'; @endphp @else @php $clear='details user-block clearfix'; $class='sec2'; @endphp @endif 
						<div class="usercomments {{ ($value->feedback_type==0) ? "odd":"even" }} feedback-b {{$class}}">
							<div class="container">
								<div class="row">
									<div class="col-sm-2 col-xs-12">
										<a href="{{ URL::to('profile/'.$value->username,'') }}" target="_blank">
										{!! SiteHelpers::customavatar($value->email,$value->user_id,'small') !!}
										</a>
									</div>
									<div class="col-sm-10 col-xs-12">
										<div class="{{$clear}}">
											<h4 class="user_name"><a href="{{ URL::to('profile/'.$value->username,'') }}" target="_blank">{!! $value->username !!}</a></h4>
											<span class="time"> {!! SiteHelpers::changeFormat($value->created_at) !!} </span>
										</div>
										<div class="feedback-comment"><?php echo $value->comments; ?></div>
										@if($value->feedback_type=='0')
										<a href="javascript:void(0);" class="replyto" data-fid="{!! $value->feedback_id  !!}"><i class="fa fa-reply" ></i> {!! Lang::get('core.Reply')!!}</a>
										@endif
									</div>

									<div class="col-md-12 text-editor clearfix" id="feedbackreply-{!! $value->feedback_id !!}" style="display:none;">
										
											<div class="form-group">
												
												
													<textarea id="textboxcontent-{!! $value->feedback_id !!}" class="form-control feedbackeditor" rows="2" placeholder="{!! Lang::get('core.comment')!!}" name="comment" cols="50"></textarea> 
											
											</div> 	
											<div class="form-group">
											
													<button type="button" name="replytosave"  class="btn btn-color replybtn" data-fid="{!! $value->feedback_id  !!}"><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
													<button type="button"  class="btn btn-color cancel" data-fid="{!! $value->feedback_id  !!}"><i class="fa  fa-arrow-circle-left " ></i>  {{ Lang::get('core.sb_cancel') }} </button>
											  

											</div> 				
									
									</div>

								</div>
							</div>
						</div>

						@if($value->feedback_type=='0')
						<div id="replypush-{!! $value->feedback_id !!}"></div>
						@endif
						
						@php $k++; @endphp
						@endforeach
						@endif
					{!! Form::close() !!}
					</div>
				</div>
			</div> 
			
		</div>
		
	</div>
</div>
</div>
<script type="text/javascript" src="<?php echo e(URL::to('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js')); ?>"></script>	
<script type="text/javascript">
$(function(){

	$(document).on('click','.replyto,.cancel',function () { 
		var fid = $(this).data('fid');
		// alert(fid);
		if ($('#feedbackreply-'+fid).is(':visible')) { 
			$("#feedbackreply-"+fid).slideUp(300); 
		} 
		if ($("#feedbackreply-"+fid).is(':visible')) { 
			$("#feedbackreply-"+fid).slideUp(300);
		} else {
			$("#feedbackreply-"+fid).slideDown(300); 
		} 
	}); 

	tinymce.init({	
		mode : "specific_textareas",
		editor_selector : "feedbackeditor",
		theme : "advanced",
		theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		width : "100%",
		plugins : "paste",
		paste_text_sticky : true,
		setup : function(ed) {
			ed.onInit.add(function(ed) {
				ed.pasteAsPlainText = true;
			});
		}
	});

	$(document).on('click','.replybtn',function(e){
		var cid = $(this).data('fid');
		var textvalues = tinyMCE.get('textboxcontent-'+cid).getContent();
		var courseid   = $('[name="course_id"]').val();
		if($.trim(textvalues).length===0){
			toastr.error("Error", "{!! Lang::get('core.enter_valid_comments')!!}");
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		}else if ($.trim(textvalues).length>0){
			var me = $(this);
			e.preventDefault();
			if ( me.data('requestRunning') ) {
				return;
			}
			me.data('requestRunning', true);
			var lectureid 	= $('[name="lectureid"]').val();
			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("course/savefeedbackreply")); ?>',
				data:'courseid='+courseid+'&comments='+textvalues+'&commentid='+cid,
				success : function(data) {
					var obj = jQuery.parseJSON(data);
					if(obj.status=='1'){
						$('#feedbackreply-'+cid).hide();
					// $('#textboxcontent-'+cid).val('');
					tinyMCE.get('textboxcontent-'+cid).setContent('');

					$('#replypush-'+obj.results.commentid).append('<div class="usercomments sec2"><div class="container"><div class="row"><div class="col-sm-2 col-xs-12">'+obj.results.imgpath+'</div><div class="col-sm-10 col-xs-12"><div class="details user-block clearfix"><h4 class="user_name">'+obj.results.fname+'</h4><span class="time">Just now</span></div><div class="">'+obj.results.comments+'</div></div></div></div></div>');

					// ajaxtinymce();

					toastr.success("Success", "{!! Lang::get('core.comment_thanks')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}else if(obj.status=='2'){
					toastr.error("Error", "{!! Lang::get('core.comments_error')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}

				return false;
			},complete: function() {
				me.data('requestRunning', false);
			}
		});
}

});




});


function ajaxtinymce(){
	tinymce.init({	
		mode : "specific_textareas",
		editor_selector : "feedbackeditor",
		theme : "advanced",
		theme_advanced_buttons1 : "bold,italic,underline,bullist,numlist",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		width : "100%",
		plugins : "paste",
		paste_text_sticky : true,
		setup : function(ed) {
			ed.onInit.add(function(ed) {
				ed.pasteAsPlainText = true;
			});
		}
	});
}

</script>

@stop