@extends('layouts.mailtemplate')
@section('content')
<h2>Dear user</h2>

<p>
		{!! CNF_APPNAME !!} {!! Lang::get('core.announced_of') !!}  {!! $coupon_value !!} 
		@if($coupon_type == 1)
			{!! '%' !!}
		@elseif($coupon_type == 1)
			{!! '$' !!}	
		@endif	
		{!! Lang::get('core.valid_from') !!} {!! $coupon_start_date !!} to {!! $coupon_end_date !!}
</p>
<br /><br /><p> {!! Lang::get('core.thank_you') !!} </p>

{!! CNF_APPNAME !!} 

@stop