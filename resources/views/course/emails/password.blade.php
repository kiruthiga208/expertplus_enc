@extends('layouts.mailtemplate')
@section('content')
<h2>{!! Lang::get('core.email_dear') !!} {!! $username !!}</h2>

<p>
	{!! Lang::get('core.click') !!} <a href="{!! $link !!}"> {!! Lang::get('core.email_here') !!}</a> {!! Lang::get('core.subscriber_pwd')!!} <b>{!! $password !!}</b>
</p>

<br /><br /><p> {!! Lang::get('core.thank_you') !!} </p>

{!! CNF_APPNAME !!} 
@stop