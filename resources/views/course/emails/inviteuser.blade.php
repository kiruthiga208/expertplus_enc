@extends('layouts.mailtemplate')
@section('content')
<h2>Dear {!! $username !!}</h2>

<p>
	{!! Lang::get('core.email_invite') !!} <a href="{!! $link !!}">{!! Lang::get('core.email_here')!!}</a> {!! Lang::get('core.email_viewit') !!}
</p>
<br /><br /><p> {!! Lang::get('core.thank_you') !!} </p>

{!! CNF_APPNAME !!} 
@stop