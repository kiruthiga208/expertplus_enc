@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/taginputs/jquery.tagsinput.css') }}" rel="stylesheet">
<script type="text/javascript" src="{{ URL::to('assets/bsetec/js/plugins/taginputs/jquery.tagsinput.js') }}"></script>
<style type="text/css">
.form-group{ margin:25px 0px; }
.course_goal_ul li,.int_audience_ul li,.course_req_ul li{ margin:20px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course detail-block">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">
		
			 <div class="lach_dev resp-tab-content course_tab"> 
                <div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/updatecourse', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						<div class="course_basic course_newbasic"><h4>{!! Lang::get('core.Details')!!}</h4><p>{!! Lang::get('core.details_txt')!!}</p></div>
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.course_summary')!!}</label>
							<div class="text-a col-xs-12">
							@php( $description = strip_tags($course->description))
							{!!  Form::textarea('description',$description,array('class'=>'form-control mceEditor'))  !!}
							</div>
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.course_goal') !!}</label>
							
							<div class="col-xs-12">
								 <ul class="course_goal_ul sortable_ul sortable_scroll ui-sortable "> 
								@php( $key_goal = explode('--txt--', $course->course_goal) )
									@if($key_goal[0] != '')
									   @php ( $i=1 )
										@foreach ($key_goal as $key => $value)
										<li class="clearfix"> <p class="course-c">{{$value}}</p> <a class="pull-right course_goal{{$i}}" onclick="delete_li('course_goal','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors color_icon"></i></a>
										<a class="pull-right" href="javascript:void(0)">
										<!-- <i class="fa fa-align-justify justify_color"></i> -->
										</a></li>
										@php ( $i++ )
										@endforeach
									@endif
								</ul>
							</div>
							
							
							 <input type="text" id="course_goal" class="form-control" />
						
						
							 <input type="button" id="course_goal_btn" onclick="common_add('course_goal')" class="btn btn-color" value="{!! Lang::get('core.add')!!}" /><textarea style="display:none" name="course_goal" id="course_goal_text">{{$course->course_goal}}</textarea>
							
						</div>	
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.intended') !!}</label>
							
							<div class="col-xs-12">
								<ul class="int_audience_ul sortable_ul sortable_scroll ui-sortable"> 
								@php  ($key_goal = explode('--txt--', $course->int_audience) )
									@if($key_goal[0] != '')
									   @php ( $i=1 )
									   @foreach ($key_goal as $key => $value)
										<li class="clearfix"> <p class="course-c">{{$value}}</p><a class="pull-right int_audience{{$i}}" onclick="delete_li('int_audience','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a>
										<a class="pull-right" href="javascript:void(0)">
										<!-- <i class="fa fa-align-justify justify_color"></i> -->
										</a></li>
									@php ( $i++ )
									@endforeach
									@endif
								</ul>
							</div>
							
						
							 <input type="text" id="int_audience" class="form-control" />
							
							
							<input type="button" id="int_audience_btn" onclick="common_add('int_audience')" class="btn btn-color" value="Add" /> <textarea style="display:none" name="int_audience" id="int_audience_text">{{$course->int_audience}}</textarea>
						
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.course_req')!!}</label>
							
							<div class="col-xs-12">
								<ul class="course_req_ul sortable_ul sortable_scroll ui-sortable"> 
								@php ( $key_goal = explode('--txt--', $course->course_req) )
									 @if($key_goal[0] != '')
									   @php ( $i=1 )
									   @foreach ($key_goal as $key => $value)
										<li class="clearfix"> <p class="course-c">{{$value}}</p><a class="pull-right course_req{{$i}}" onclick="delete_li('course_req','{{$i}}')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a>
										<a class="pull-right" href="javascript:void(0)"> 
										<!-- <i class="fa fa-align-justify justify_color"></i> -->
										</a></li>
									@php( $i++ )
										@endforeach
									@endif
								</ul>
							</div>
							
							
								 <input type="text" autocomplete="off" id="course_req" class="form-control"/>
						
					
								 <input type="button" id="course_req_btn" onclick="common_add('course_req')" class="btn btn-color" value="Add" /><textarea style="display:none" name="course_req" id="course_req_text">{{$course->course_req}}</textarea>
							
						</div>
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.page_title') !!}</label>
							
				{!!  Form::text('page_title', $course->page_title, array('class'=>'form-control chcountfield', 'id'=>'title', 'placeholder'=>'add a page title', 'maxlength' => 150 ))  !!}
				<span class="basic_subtitle ch-count">{!! 150-strlen($course->page_title) !!}</span>
							
						</div>
						
						
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Meta_key') !!}</label>
							
				{!!  Form::text('meta_key', $course->meta_keywords, array('class'=>'form-control', 'id'=>'keyword', 'placeholder'=>'add a keyword' ))  !!}
							
						</div>

						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Meta_des') !!}</label>
						
				{!!  Form::textarea('meta_desc', $course->meta_description, array('class'=>'form-control', 'placeholder'=>'add a description' ))  !!}	

						</div>
						
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.level') !!}</label>
							<div class="col-xs-12">
								<div> <span class="remove_mrg">{!! Form::radio('course_level', '1', ($course->course_level == 0 || $course->course_level == 1) ? true : false, array('class'=>'')) !!} <span class="opt_class">{!! Lang::get('core.Introductory') !!}</span></span>
								<span class="remove_mrg">       {!! Form::radio('course_level', '2', ($course->course_level == 2) ? true : false, array('class'=>'')) !!} <span class="opt_class"> {!! Lang::get('core.Intermediate') !!}</span></span>
								<span class="remove_mrg">     {!! Form::radio('course_level', '3', ($course->course_level == 3) ? true : false, array('class'=>'')) !!}<span class="opt_class">  {!! Lang::get('core.Advanced') !!}</span></span>
								<span class="remove_mrg">    {!! Form::radio('course_level', '4', ($course->course_level == 4) ? true : false, array('class'=>'')) !!} <span class="opt_class">{!! Lang::get('core.all_levels') !!}</span></span></div>
							</div>
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.admin_discount') !!}</label>
							<div class="col-xs-12">
								<div>
								<span class="remove_mrg">{!! Form::checkbox('course_admin_discount', '1',($course->course_admin_discount == 1) ? true : false, array('class'=>'')) !!} <span class="opt_class">{!! Lang::get('core.fr_mactive')!!}</span></span>
							</div>
						</div>
						<div class="form-group">
					
							<input type="submit" name="" value="{!! Lang::get('core.sb_save')!!}" class="btn btn-color">
						
						</div>
						{!! Form::close() !!}

					</div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
</div>
<script type="text/javascript" src="{{ URL::to('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js') }}"></script>

<script>
$(function(){
	$('.sortable_ul').sortable();
	var align = 'ltr';
	@if(CNF_RTL==1)
 		var align = 'rtl';
	@endif

	tinymce.init({	
		mode : "specific_textareas",
		editor_selector : "mceEditor",
		theme : "advanced",
		theme_advanced_buttons1 : "undo,redo,|,bold,italic,underline,strikethrough,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,blockquote,bullist,numlist,|,formatselect,fontselect,|,removeformat",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		directionality : align,
	 });	
	
	$('#course_goal').keypress(function(e) {
      if (e.keyCode == '13') {
         e.preventDefault();
         common_add('course_goal');
       }
    });

    $('#int_audience').keypress(function(e) {
      if (e.keyCode == '13') {
         e.preventDefault();
         common_add('int_audience');
       }
    });

    $('#course_req').keypress(function(e) {
      if (e.keyCode == '13') {
         e.preventDefault();
         common_add('course_req');
       }
    });


});
function common_add(strs){
    var val = $('#'+strs).val();
		val = $.trim(val);
    if(val == ''){
        alert('Empty Values Not allowed')
         //$('#errormsg').html('Empty Value Not allowed');
    }else{
        var number = Math.floor(Math.random() * 102)
        $('.'+strs+'_ul').append('<li class="clearfix"><p class="course-c">'+val+'</p><a class="pull-right '+strs+number+' " onclick="delete_li(\''+strs+'\',\''+number+'\')" href="javascript:void(0)"><i class="fa fa-trash-o colors"></i></a><a class="pull-right" href="javascript:void(0)"></a></li>');       
        if($('#'+strs+'_text').text() == ''){
            $('#'+strs+'_text').text(val);
        } else{
            var add_text = $('#'+strs+'_text').text()+'--txt--'+val;
            $('#'+strs+'_text').text(add_text);
        }
        $('#'+strs).val('');
    }
}
function delete_li(str,i){
    $('.'+str+i).parent('li').remove();
    $('#'+str+'_text').text('');
    common(str);
}
$('#keyword').tagsInput();
$(document).on('input','.chcountfield', function() {
	var len = $(this).val().length;
	var setval = parseInt('150')-parseInt(len);
	$(this).next('.ch-count').text(setval);
});
$(document).on('input','.count600', function() {
	var len = $(this).val().length;
	var setval = parseInt('150')-parseInt(len);
	$(this).next('.ch-count').text(setval);
});
</script>
@stop