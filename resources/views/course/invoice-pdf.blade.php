@if (defined('CNF_CURRENCY'))
        @php $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) @endphp
@endif
<table border="0" cellpadding="0" cellspacing="0" width="500" style="border:1px solid #ddd; " align="center">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr height="25"></tr>
<tr>

<td width="20"></td>
<td width="460">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td>
<h3 style="text-transform:uppercase; text-align:right; color:#ccc; font-size:20px; margin:0px;">{!! Lang::get('core.invoice') !!}</h3>
</td>
</tr>
<tr><td>
<h3 style="text-align:right; color:#888; font-size:16px; margin:0px;">{!! date('d-m-Y') !!}</h3>
</td>
</tr>
<tr><td height="20"></tr>
<tr>

<td >
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr><td style="background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.in_number') !!}</td> <td width="70%" style="padding:10px; color:#666; text-transform:capitalize; font-size:15px;">{!! $invoice['invoice_number'] !!}</td></tr>
<tr><td height="5"></td></tr>
<tr><td style="background:#e8f1fa; padding:10px; color:#777;text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.in_cus_number') !!}</td> <td width="70%" style="padding:10px; color:#666;text-transform:capitalize; font-size:15px;">{!! $invoice['customer_name'] !!}</td></tr>
<tr><td height="5"></td></tr>
<tr><td style="background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.in_course_name') !!}</td> <td width="70%" style="padding:10px; color:#666; text-transform:capitalize; font-size:15px; ">{!! $invoice['course_title'] !!}</td></tr>
<tr><td height="5"></td></tr>
<tr><td style="background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.in_coruse_cost') !!}</td> <td width="70%" style="padding:10px; color:#666; text-transform:capitalize; font-size:15px; ">{!! $currency !!} {!! $invoice['amount'] !!}</td></tr>
<tr><td height="5"></td></tr>
<tr><td style="background:#e8f1fa; padding:10px; color:#777;text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.in_order_on') !!}</td> <td width="70%" style="padding:10px; color:#666; margin:0px; text-transform:capitalize; font-size:15px;">{!! $invoice['ordered_on'] !!}</td></tr>
<tr><td height="5"></td></tr>
<tr><td style="background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:15px; font-weight:bold;" width="30%">{!! Lang::get('core.Status') !!}</td> <td width="70%" style="padding:10px; color:#666;text-transform:capitalize; font-size:15px;">{!! ucwords($invoice['status']) !!}</td></tr>
</table>
</td>


</tr>

<tr><td height="20"></td></tr>
<tr><td><h3 style="text-transform:capitalize; font-size:20px; color:#777; margin:0px;">{!! Lang::get('core.in_bill_to')!!}</h3></td></tr>
<tr><td><p style="font-size:15px; color:#777; margin:0px;">{!! $invoice['email'] !!}</p></td></tr>
<tr><td height="15"></td></tr>
<tr><td>
<table cellpadding="0" cellspacing="0" width="100%" style="border:1px solid #ddd; border-collapse:collapse;">
<tr><th width="40%" style="text-align:left; background:#e8f1fa; padding:10px;color:#777; text-transform:capitalize; font-size:16px; border:1px solid #ddd;">{!! Lang::get('core.in_description')!!}</th>
<th width="20%" style="text-align:right; background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:16px; border:1px solid #ddd;">{!! Lang::get('core.in_quantity')!!}</th>
<th width="20%" style="text-align:right;background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:16px; border:1px solid #ddd;">{!! Lang::get('core.in_unit_price')!!}</th>
<th width="20%" style="text-align:right;background:#e8f1fa; padding:10px; color:#777; text-transform:capitalize; font-size:16px; border:1px solid #ddd;">{!! Lang::get('core.in_amount')!!}</th>
</tr>

<tr><td width="40%" height="35" style="text-align:right; padding:0px 5px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd; text-align:left;">{!! Lang::get('core.in_pdf_text') !!}</td>
<td width="20%" height="35" style="text-align:right; padding:0px 10px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd;">1</td>
<td width="20%" height="35" style="text-align:right; padding:0px 10px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd;">{!! $currency !!} {!! $invoice['amount'] !!}</td>
<td width="20%" height="35" style="text-align:right; padding:0px 10px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd;">{!! $currency !!} {!! $invoice['amount'] !!}</td></tr>

<tr>
<td height="35" colspan="3" style="text-align:center; padding:0px 10px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd;">{!! Lang::get('core.Total') !!}</td>
<td  height="35" style="text-align:right; padding:0px 10px; font-size:14px; color:#777; text-transform:capitalize; font-weight:400; border:1px solid #ddd;"> {!! $currency !!} {!! $invoice['amount'] !!}</td></tr>

</table>

</td>
</tr>

</table>

</td>


<td width="20"></td>




</tr>
<tr height="25"></tr>
</table>
</td></tr></table>
