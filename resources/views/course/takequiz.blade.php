<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title> {!! Lang::get('core.Take_Course') !!} {{ $page_title or '' }} </title>
<meta name="keywords" content="{{ $meta_keywords or '' }}">
<meta name="description" content="{{ $meta_description or '' }}"/>
<link rel="shortcut icon" href="{{ asset('uploads/images/'.CNF_FAV)}}" type="image/x-icon">	
<link href="{{ asset('assets/bsetec/themes/theme1/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/themes/theme1/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/css/icons.min.css') }}" rel="stylesheet">

<link href="{{ asset('assets/bsetec/static/css/video-js.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/coursepreview.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/js/plugins/iCheck/skins/square/green.css')}}" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600italic,700,800,800italic,700italic,600,400italic,300,300italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/themes/theme1/js/bootstrap.min.js') }}"></script>	
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/toastr/toastr.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/tinymce/jscripts/tiny_mce/tiny_mce.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/bsetec/js/plugins/iCheck/icheck.min.js') }}"></script>
<noscript><meta http-equiv="refresh" content="0;url={{ URL::TO('nojs.php') }}"></noscript>
<link href="{{ asset('assets/bsetec/js/plugins/redactor/css/redactor.css') }}" rel="stylesheet">
<style>
body{background:#333333;height: 100% !important; color:#FFF;}
html, body, .container-fluid, .row { height: 100%;}
</style>
</head>
<body>
<div class="taken-course">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-8 contentView nopadding">
			
				<div class="blackhead">	
					<a href="{{ URL::to('courseview/'.$courseid.'/'.$slug) }}"><i class="fa fa-angle-left"></i>{!! Lang::get('core.back_course')!!}</a>
					<span class="pull-right righthide"><i class="fa fa-angle-right"></i></span>
					<!--span class="pull-right use-padding">
						@if(count($getdata)>0)
						{!! Form::checkbox('staus', 'value', $completion_status,array('class'=>'updatelstatus','id'=>'lstatusid','data-lid'=> $getdata['0']->lecture_quiz_id,'data-courseid'=>$courseid)); !!}
						@endif
					</span-->
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="courseid" value="{{ $courseid }}">
				</div>
				
			<div class="clr-b clearfix">
				<div class="titlehead">
					@if(count($curriculum) > 0)
						@if(count($getdata)> 0) 
							@php  $quizcount = \SiteHelpers::getquizcount($getdata['0']->lecture_quiz_id); @endphp
							@php  $sec_count = 1; @endphp
							@for($i=0;$i < count($curriculum);$i++)
								@if($curriculum[$i]->section_id == $getdata['0']->section_id)
									@php $section = $sec_count.' - '.$curriculum[$i]->title; @endphp
								@endif
								@php  $sec_count++; @endphp
							@endfor
							<h2>{!! Lang::get('core.sec') !!} {!! $section !!}</h2>
							<h2><span>{!! $getdata['0']->title !!} (<strong>{!!  $quizcount !!}</strong> @if($quizcount > 1) {!! Lang::get('core.Questions')!!}) @else {!! Lang::get('core.Question')!!}) @endif</span></h2>
							<div class="quizbuttons">
								<a href="javascript:void(0)" class="btn btn-success btn-bg start-quiz-btn @if($completion_status == 2) hide @endif" data-lid="{!! $getdata['0']->lecture_quiz_id !!}" data-time="{!! $getdata['0']->time !!}">{!! Lang::get('core.Start_Quiz') !!}</a>
								<a href="javascript:void(0)" class="btn btn-primary btn-bg view-results-btn @if($completion_status == 0) hide @endif" data-lid="{!! $getdata['0']->lecture_quiz_id !!}">{!! Lang::get('core.view_result')!!}</a>
								<a href="javascript:void(0)" class="btn btn-orange btn-bg take-quiz-btn @if($completion_status == 0) hide @endif" data-lid="{!! $getdata['0']->lecture_quiz_id !!}">{!! Lang::get('core.Retake_Quiz')!!}</a>
							</div>
						@endif
					@endif
				</div>
				<div class="arw-nxt">
                	@if($prev)
					<?php $combine = $prev;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-left"></i></a>
					@endif 
					@if($next)
					<?php $combine = $next;  $encryptid = \SiteHelpers::encryptID($combine); ?>
					<a href="{{ URL::to('learn-course/'.$courseid.'/'.$slug.'/'.$encryptid) }}"><i class="fa fa-angle-right"></i></a>
					@endif 
					
                </div>
                
           
            </div>
			
			<div class="quiz-asset hide">
				<div class="quiz-navigation clearfix">
					<a href="javascript:void(0)" class="btn btn-default btn-bg back-btn2 back-btn-1 hide">{!! Lang::get('core.btn_back') !!}</a>
					<a href="javascript:void(0)" class="btn btn-default btn-bg back-btn2 back-btn-2 hide">{!! Lang::get('core.btn_back') !!}</a>
					<div class="dropdown quiz-dropdown">
						<a class="btn btn-bg btn-default dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
							 <span class="currentquiz">1</span> / <span class="total">{!!  $quizcount !!}</span>
						</a>
						Time : <span id="timerText">--:--:--</span>
						<ul class="dropdown-menu">
							@for($i=1;$i<=$quizcount;$i++)
							<li>
								<a href="javascript:void(0)" class="gotoquestion" data-id="{!! $i !!}">{!! Lang::get('core.Question') !!} <span class="num">{!! $i !!}</span></a>
							</li>
							@endfor
						</ul>
						
					</div>
				</div>
				<ul class="quiz-viewer">
					@if(count($questions)>0)
					@php $quescount = 1; @endphp
					@foreach($questions as $question)
					@php  $quizresults = \SiteHelpers::getquizresult($getdata['0']->lecture_quiz_id,$question->quiz_question_id,$luserid); @endphp
					@php $questioncomplete = 0; @endphp
					@if(!empty($quizresults) && isset($quizresults->complete) && $quizresults->complete == '1')
					@php $questioncomplete = 1; $canswer = $quizresults->correct_option; $uanswer = $quizresults->user_option; $qstatus = $quizresults->status; @endphp
					@endif
					<li class="frame question hide" id="quizquestion{!! $question->quiz_question_id !!}" data-qcount="{!! $quescount !!}" data-qid="{!! $question->quiz_question_id !!}">
						<div class="quiz-mask">
							<div class="quiz-wrapper">
								<div class="question-text">{!! $question->question !!}</div>
								<ul class="answers" id="quizanswers{!! $question->quiz_question_id !!}">
								@if($question->question_type == 1)
									@if($questioncomplete == 1)
									<li class="qanswer @if(($qstatus == 1 && $uanswer == 1) || $canswer == 1) correct @elseif($qstatus == 0 && $uanswer == 1) wrong @endif">{!! Lang::get('core.True') !!} <i class="fa @if(($qstatus == 1 && $uanswer == 1) || $canswer == 1) fa-check @elseif($qstatus == 0 && $uanswer == 1) fa-times @endif pull-right"></i></li>
									<li class="qanswer @if(($qstatus == 1 && $uanswer == 2) || $canswer == 2) correct @elseif($qstatus == 0 && $uanswer == 1) wrong @endif">{!! Lang::get('core.False') !!}<i class="fa @if(($qstatus == 1 && $uanswer == 2) || $canswer == 2) fa-check @elseif($qstatus == 0 && $uanswer == 1) fa-times @endif pull-right"></i></li>
									@else
									<li class="qanswer">{!! Lang::get('core.True') !!}<i class="fa pull-right"></i></li>
									<li class="qanswer">{!! Lang::get('core.False') !!} <i class="fa pull-right"></i></li>
									@endif
								@else
									@php $quesanswers = json_decode($question->options); @endphp
									@php $anscount = 1; @endphp
									@foreach($quesanswers as $answer)
										@if($questioncomplete == 1)
										<li class="qanswer @if(($qstatus == 1 && $uanswer == $anscount) || $canswer == $anscount) correct @elseif($uanswer == $anscount && $qstatus == 0) wrong @endif">{!! $answer->answer !!} <i class="fa @if(($qstatus == 1 && $uanswer == $anscount) || $canswer == $anscount) fa-check @elseif($uanswer == $anscount && $qstatus == 0) fa-times @endif pull-right"></i> @if(!empty($answer->feedback))<p class="quiz-answer-feedback">{!! $answer->feedback !!}</p>@endif</li>
										@else
										<li class="qanswer">{!! $answer->answer !!} <i class="fa pull-right"></i> <p class="quiz-answer-feedback hide"></p></li>
										@endif
									@php $anscount++; @endphp
									@endforeach
								@endif
								</ul>
								<div class="clearfix quizbuttons">
									@if($questioncomplete == 0 && $completion_status == false)
									<button type="button" class="submit-answer-btn btn btn-primary btn-bg" data-qid="{!! $question->quiz_question_id !!}" data-lid="{!! $question->quiz_id !!}" @if(isset($quizresults->result_id)) data-rid="{!! $quizresults->result_id !!}" @else data-rid="0" @endif @if($quizcount == $quescount) data-final="1"> {!! Lang::get('core.Finalize') !!} @else data-final="0">  {!! Lang::get('core.sb_submit') !!} @endif</button>
									@endif
									<button type="button" class="next-btn btn btn-orange btn-bg pull-right @if($questioncomplete == 0 && $completion_status == false) hide @endif" data-qid="{!! $question->quiz_question_id !!}" @if($quizcount == $quescount) data-final="1"> {!! Lang::get('core.View_Result') !!} @else data-final="0"> {!! Lang::get('core.Next_Question') !!} @endif</button>
								</div>
							</div>
						</div>
					</li>
					@php $quescount++; @endphp
					@endforeach
					@endif
					<li class="frame results-page hide">
						<div class="quiz-mask">
							<div class="quiz-wrapper">
								<div class="score">
									<h2>{!! Lang::get('core.Your_Score') !!}</h2>
									<div class="perc">
										<span class="webkit-mask">
											<i style="width: @if(isset($quizprogress['score'])){!! $quizprogress['score'] !!}@else{!! '0' !!}@endif%;"></i>
										</span>
										<b>@if(isset($quizprogress['score'])){!! $quizprogress['score'] !!}@else{!! '0' !!}@endif</b>
									</div>
									<div class="numbers">
										<p class="c">{!! Lang::get('core.Correct')!!}: <b>@if(isset($quizprogress['correctcount'])){!! $quizprogress['correctcount'] !!}@else{!! '0' !!}@endif</b></p>
										<p class="i">{!! Lang::get('core.Incorrect')!!}: <b>@if(isset($quizprogress['incorrectcount'])){!! $quizprogress['incorrectcount'] !!}@else{!! '0' !!}@endif</b></p>
										<p class="s">{!! Lang::get('core.Skipped')!!}: <b>@if(isset($quizprogress['skipcount'])){!! $quizprogress['skipcount'] !!}@else{!! '0' !!}@endif</b></p>
										<p class="co">{!! Lang::get('core.Course_Average')!!}: <b>@if(isset($quizprogress['overallscore'])){!! $quizprogress['overallscore'] !!}@else{!! '0' !!}@endif</b></p>
									</div>
								</div>
								<div class="review">
									<div class="wrapper">
										<h1>{!! Lang::get('core.quiz_result')!!}</h1>
										<div class="webkit-wrapper">
											<ul class="scoreboard">
												<li class="h">
													<div></div>
													<div><span class="instructor-thumb">{!! SiteHelpers::customavatar(\Session::get('eid'),\Session::get('uid')) !!}</span> {!! Lang::get('core.YOU')!!}</div>
													<div>
														<i class="fa fa-users text-primary"></i> {!! Lang::get('core.YOUR_CLASS') !!} <b class="userscount">(@if(isset($quizprogress['userscount'])){!! $quizprogress['userscount'] !!}@else{!! '0' !!}@endif)</b>
													</div>
												</li>
												@if(isset($quizprogress['quizresult']) && count($quizprogress['quizresult'])>0)
												@foreach($quizprogress['quizresult'] as $key => $result)
												<li class="q quizlist" data-position="{!! $key !!}">
													<div>{!! Lang::get('core.Question') !!} {!! $key !!}</div>
													@if($result == '1') <div class="a c"> <i class="fa fa-check"></i> {!! Lang::get('core.Correct')!!}</div> @elseif($result == '0') <div class="a i"> <i class="fa fa-times"></i> {!! Lang::get('core.Incorrect')!!}</div> @else <div class="a s"> <i class="fa fa-fast-forward"></i> {!! Lang::get('core.Skipped')!!}</div> @endif
													<div>
														<div class="relative h12px-ie">
															<div class="assessment-stats">
																@php $quizusersresult = $quizprogress['quizusersresult'][$key]; @endphp
																<span class="a c"> <i class="fa fa-check"></i> @if(isset($quizusersresult['correct'])){!! $quizusersresult['correct'] !!}@else{!! '0' !!}@endif</span>
																<span class="a i"> <i class="fa fa-times"></i> @if(isset($quizusersresult['incorrect'])){!! $quizusersresult['incorrect'] !!}@else{!! '0' !!}@endif</span>
															</div>
														</div>
													</div>
												</li>
												@endforeach
												@endif
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			
			
		</div>	
				@include('course/right-tab-learn')
							</div>
						</div>
                        </div>
					</div>
				</div>

<script type="text/javascript">
$(function(){

	$(document).on('click','.take-quiz-btn', function(){
		var e = $(this);
		e.prop("disabled", true);
		var lid = $(this).data('lid');
		var cid = $('[name="courseid"]').val();
		updatestatusretake(lid,cid,'0');
		//$('.clr-b').addClass('hide');
		//$('.quiz-asset').removeClass('hide');
	});

	$(document).on('click','.start-quiz-btn', function(){
		var e = $(this);
		e.prop("disabled", true);
		e.addClass('hide');
		var lid = $(this).data('lid');
		var cid = $('[name="courseid"]').val();
		updatestatus(lid,cid,'0');
		$('.clr-b').addClass('hide');
		$('.quiz-asset').removeClass('hide');
		$('.quiz-viewer').children('li').eq(0).removeClass('hide');				
		$('.submit-answer-btn').prop("disabled", false);				
		$('.next-btn').prop("disabled", false);

		var time = $(this).data('time');
		var split = time.split(':');
		var hrs = parseInt(split[0]); // Hour quiz time
	    var mins = parseInt(split[1]); // Min quiz time
	    var secs = parseInt(split[2]); // Seconds quiz time
	    var timerDisplay = $('#timerText');

	    var countDown = function (callback) {
	        var interval;
	        interval = setInterval(function () {
	            if (secs === 0) {
	                if (mins === 0) {
	                	if(hrs === 0){
	                		timerDisplay.text('00:00:00');
		                    clearInterval(interval);
		                    callback();
		                    return;
	                	} else {
	                		hrs--;
	                		mins = 59;
	                		secs = 59;
	                	}
	                } else {
	                    mins--;
	                    secs = 59;
	                }
	            }

	            var hour_text;
	            if(hrs > 0) {
	            	hour_text = hrs;
	            } else {
	            	hour_text = '00';
	            }

	            var minute_text;
	            if (mins > 0) {
	                minute_text = mins;
	            } else {
	                minute_text = '00';
	            }
	            var second_text = secs < 10 ? ('0' + secs) : secs;
	            var minute_text = mins < 10 ? ('0' + mins) : mins;
	            var hour_text 	= hrs < 10 ? ('0' + hrs) : hrs;

	            timerDisplay.text(hour_text+':'+minute_text + ':' + second_text);
	            secs--;
	        }, 1000, timeUp);
	    };

	    // When time elapses
	    var timeUp = function () {
	    	clicked = $('.next-btn').data('click');
	    	if(clicked!='clicked'){
	    		alert("Time's Up!");
	        	$('.next-btn').trigger('click');
	        	$('.submit-answer-btn').trigger('click');
	    	}
	    };

	    // Start the clock
	    countDown(timeUp);
	});
	
	$(document).on('click','.qanswer', function(){
		$(this).parent('ul').children('li').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('click','.submit-answer-btn', function(event){
		var e = $(this);
		e.prop("disabled", true);
		var lid = $(this).data('lid');
		var qid = $(this).data('qid');
		var rid = $(this).data('rid');
		var last = $(this).data('final');
		var cid = $('[name="courseid"]').val();
		var count = 1;
		var uoption = '';

		if (event.originalEvent === undefined) {
	        human = false;
	    } else {
	        human = true;
	    }

		$('#quizquestion'+qid+' ul.answers li').each(function(){
			if($(this).hasClass('selected')){
				uoption = count;
			}
			count++;
		});
		if(uoption == '' && human){
			alert('{!! Lang::get("core.quiz_answer") !!}');
			e.prop("disabled", false);
		} else {
			var _token =$('[name="_token"]').val();
			$.ajax ({
				type: "POST",
				url: '<?php echo e(\URL::to("course/quizsubmit")); ?>',
				data: "cid="+cid+"&lid="+lid+"&qid="+qid+"&rid="+rid+"&uoption="+uoption+"&last="+last+"&_token="+_token+"&human="+human,
				beforeSend: function() {
					e.html('<i class="fa fa-refresh fa-spin"></i>');
				},
				success: function (data)
				{
					var return_data = $.parseJSON( data );
					if(return_data.status=='1'){
						$('#quizanswers'+qid).find('.selected').addClass('correct'); 
						$('#quizanswers'+qid).find('.selected i').addClass('fa-check');
					} else if(return_data.status=='0'){
						$('#quizanswers'+qid).find('.selected').addClass('wrong');
						$('#quizanswers'+qid).find('.selected i').addClass('fa-times');
						$('#quizanswers'+qid).children('li').eq( parseInt(return_data.coption)-parseInt(1) ).addClass('correct');
						$('#quizanswers'+qid).children('li').eq( parseInt(return_data.coption)-parseInt(1) ).find('i').addClass('fa-check');
					}
					
					if(return_data.type=='0'){
						var options = $.parseJSON( return_data.options );
						jQuery.each(options, function(index, value) {
							if(value.feedback != ''){
								$('#quizanswers'+qid).children('li').eq( index ).find('.quiz-answer-feedback').text(value.feedback);
								$('#quizanswers'+qid).children('li').eq( index ).find('.quiz-answer-feedback').removeClass('hide');
							}
						});
					}
					
					$('.results-page').find('.perc').find('b').text(return_data.score);
					$('.results-page').find('.perc').find('.webkit-mask').find('i').css('width',return_data.score+'%');
					$('.results-page').find('.numbers').find('.c').find('b').text(return_data.correctcount);
					$('.results-page').find('.numbers').find('.i').find('b').text(return_data.incorrectcount);
					$('.results-page').find('.numbers').find('.s').find('b').text(return_data.skipcount);
					$('.results-page').find('.numbers').find('.co').find('b').text(return_data.overallscore);
					$('.results-page').find('.userscount').text('('+return_data.userscount+')');
					console.log(return_data.quizresult);
					console.log(return_data.quizusersresult);
					$('ul.scoreboard li.quizlist').remove();
					jQuery.each(return_data.quizresult, function(index, value) {
						var quizusersresult = return_data.quizusersresult[index];
						var stat = '';
						if(value == '1') stat = '<div class="a c"> <i class="fa fa-check"></i>{!! Lang::get("core.Correct") !!}</div>';
						else if(value == '0') stat = '<div class="a i"> <i class="fa fa-times"></i> {!! Lang::get("core.Incorrect") !!}</div>';
						else stat = '<div class="a s"> <i class="fa fa-fast-forward"></i> {!! Lang::get("core.Skipped") !!}</div>';
						
						$('.results-page').find('ul.scoreboard').append('<li class="q quizlist" data-position="'+index+'"><div>Question '+index+'</div>'+stat+'<div><div class="relative h12px-ie"><div class="assessment-stats"><span class="a c"> <i class="fa fa-check"></i> '+quizusersresult['correct']+'</span><span class="a i"> <i class="fa fa-times"></i> '+quizusersresult['incorrect']+'</span></div></div></div></li>');
					});

					if(last == '1'){
						$('.view-results-btn').removeClass('hide');
						if(return_data.completed!='1')
							$('.take-quiz-btn').removeClass('hide');
						$('.next-btn').removeClass('hide');
						$('.submit-answer-btn').remove();
					}
					
					e.next('button').removeClass('hide');							
					e.next('button').prop("disabled", false);					
					e.prop("disabled", false);
					e.remove();
				}
			});
		}
	});
	
	$(document).on('click','.next-btn', function(){
		var qid = $(this).data('qid');
		var last = $(this).data('final');
		$('#quizquestion'+qid).addClass('hide');
		if(last == '0'){
			$('.currentquiz').text($('#quizquestion'+qid).next('li').data('qcount'));
			$('#quizquestion'+qid).next('li').removeClass('hide');
		} else {
			$('.next-btn').data('click', 'clicked');
			$('.results-page').removeClass('hide');
			$('.quiz-navigation').removeClass('hide');
			$('.back-btn-1').removeClass('hide');
			$('.quiz-dropdown').addClass('hide');
			$('.back-btn-2').addClass('hide');
		}
	});
	
	$(document).on('click','.view-results-btn', function(){
		$('.results-page').removeClass('hide');
		$('.quiz-asset').removeClass('hide');
		$('.quiz-navigation').removeClass('hide');
		$('.back-btn-1').removeClass('hide');
		$('.quiz-dropdown').addClass('hide');
		$('.clr-b').addClass('hide');
	});
	
	$(document).on('click','.back-btn-1', function(){
		$('.results-page').addClass('hide');
		$('.quiz-asset').addClass('hide');
		$('.quiz-navigation').addClass('hide');
		$('.back-btn-1').addClass('hide');
		$('.quiz-dropdown').removeClass('hide');
		$('.clr-b').removeClass('hide');
	});
	
	$(document).on('click','.back-btn-2', function(){
		$('.results-page').removeClass('hide');
		$('.quiz-asset').removeClass('hide');
		$('.quiz-navigation').removeClass('hide');
		$('.back-btn-1').removeClass('hide');
		$('.quiz-dropdown').addClass('hide');
		$('.clr-b').addClass('hide');
		$('.back-btn-2').addClass('hide');
		$('.quiz-viewer > li').addClass('hide');
		$('.quiz-viewer').children('li:last-child').removeClass('hide');
	});
	
	$(document).on('click','.gotoquestion', function(){
		var id = $(this).data('id');
		$('.currentquiz').text(id);
		$('.quiz-viewer > li').addClass('hide');
		$('[data-qcount="'+id+'"]').removeClass('hide');
	});
	
	$(document).on('click','.quizlist', function(){
		var id = $(this).data('position');
		$('.currentquiz').text(id);
		$('.quiz-viewer > li').addClass('hide');
		$('[data-qcount="'+id+'"]').removeClass('hide');
		$('.quiz-navigation').removeClass('hide');
		$('.back-btn-1').addClass('hide');
		$('.back-btn-2').removeClass('hide');
		$('.quiz-dropdown').removeClass('hide');
	});
	
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});	

	$('.updatelstatus').on('ifChecked', function(event){
		var lid 		= $(this).data('lid');
		var courseid 	= $(this).data('courseid');
		updatestatus(lid,courseid,1);
	});

	$('.updatelstatus').on('ifUnchecked', function(event){
		var lid 		= $(this).data('lid');
		var courseid 	= $(this).data('courseid');
		updatestatus(lid,courseid,0);
	});

	

	$('#takenotes').keypress(function(e) {
		if (e.keyCode == '13') {
			e.preventDefault();

			var gettype 	= $('[name="mediatype"]').val();
			var lectureid 	= $('[name="lectureid"]').val();
			var sectionid 	= $('[name="sectionid"]').val();
			var notes 		= $(this).val();
			$('#takenotes').val('');
		// alert(lectureid);
		// alert(notes);
		if(gettype=='0'){
			var vid = document.getElementById("lecture_video");
			var length  = parseInt(vid.currentTime);
		}else if(gettype=='1'){
			var vid = document.getElementById("lecture_audio");
			var length  = parseInt(vid.currentTime);
		}else{
			var length  = 0;
		}
		
		var converts   = secondsTimeSpanToHMS(length);
		// alert(converts);
		

		if($.trim(notes).length>0){

			var me = $(this);
			if ( me.data('requestRunning') ) {
				return;
			}
			me.data('requestRunning', true);

			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("course/takenotes")); ?>',
				data:'lid='+lectureid+'&sectionid='+sectionid+'&duration='+converts+'&notes='+notes,
				success : function(data) {
					$('#takenotes').val('');
					$('#listnotes').append('<tr><td width="70"><span class="label label-warning">'+converts+'</span></td><td>'+notes+' <a href="javascript:void(0);" class="pull-right removenotes" data-noteid="'+data+'"><i class="fa fa-times"></i></a></td></tr>');
					$('#downloadlink').html('<a href="{{ \URL::to("download-notes") }}/'+lectureid+'" class="btn btn-orange">Download Your Notes</a>');

				},complete: function() {
					me.data('requestRunning', false);
				}
			});
		}

	}
}); //end keypress

$(document).on('click','.removenotes',function(){
var removeid = $(this).data('noteid');
var removelength = $('.removenotes').length;
		// alert(removelenght);
		var r = confirm("{!! Lang::get('core.notes_delete') !!}");
		if (r == true) {
			$(this).closest('tr').remove();
			if(removelength=='1'){
				$('#downloadlink').html('');
			}
			var me = $(this);
			if ( me.data('requestRunning') ) {
				return;
			}
			me.data('requestRunning', true);

			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("course/removenotes")); ?>',
				data:'noteid='+removeid,
				success : function(data) {
					
				},complete: function() {
					me.data('requestRunning', false);
				}
			});

		}
	});


	//comments start here

	




}); //end functions
function secondsTimeSpanToHMS(s) {
	var h = Math.floor(s/3600); //Get whole hours
	s -= h*3600;
	var m = Math.floor(s/60); //Get remaining minutes
	s -= m*60;
	if(h>0){
		return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
	}else{
		return (m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
	}
}


function updatestatus(lid,cid,sid){

	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo e(\URL::to("course/updatecoursestatus")); ?>',
		data:'lid='+lid+'&cid='+cid+'&sid='+sid,
		success : function(data) {
			
		}
	});
}

function updatestatusretake(lid,cid,sid){

	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo e(\URL::to("course/updatecoursestatus")); ?>',
		data:'lid='+lid+'&cid='+cid+'&sid='+sid+'&clearresult=1',
		success : function(data) {
			window.location.reload();
		}
	});
}

</script>


</body> 
</html>