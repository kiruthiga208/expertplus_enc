<div class="lach_dev course_sidebar clearfix">
	<div class="got_mat">
		<ul class="mang_develop clearfix">
			<!-- <li><a href="{!! URL::to('course/create/'.$course->course_id.'/1') !!}"><h4 class="leftnopad"><span @if($step == '1') class="text-info" @endif>Course Roadmap</span></h4></a></li> -->
			<!--li><a href="{!! URL::to('course/create/'.$course->course_id.'/12') !!}"><h4 class="leftnopad"><span @if($step == '12') class="text-info" @endif>Course Quality Checklist</span></h4></a></li-->
			<li><a href="{!! URL::to('course/create/'.$course->course_id.'/13') !!}"><h4 class="leftnopad"><span @if($step == '13') class="text-info" @endif>{!! Lang::get('core.Course_Feedback')!!}</span></h4></a></li>
		</ul>
	</div>

	<div class="lach_dev_at clearfix">
		<h4 class="tabtitle"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.Course_Content')!!}</h4>
		<ul class="tabtitlemang mang_develop clearfix">
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/9') !!}" rel="Curriculum"><span @if($step == '9') class="text-info" @endif>&raquo; {!! Lang::get('core.Curriculum')!!}</span> <span class="r-arrow"></span></a></li>
		</ul>
	</div>
	<div class="lach_dev_at clearfix">
		<h4 class="tabtitle"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.Course_Info')!!}</h4>
		<ul class="tabtitlemang mang_develop clearfix">
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/2') !!}" rel="BasiceDetails"><span @if($step == '2') class="text-info" @endif>&raquo;  {!! Lang::get('core.Basics')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/3') !!}" rel="DetailDetails"><span @if($step == '3') class="text-info" @endif>&raquo;  {!! Lang::get('core.Details')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/4') !!}" rel="ImageDetails"><span @if($step == '4') class="text-info" @endif>&raquo;  {!! Lang::get('core.Image')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/5') !!}" rel="VideoDetails"><span @if($step == '5') class="text-info" @endif>&raquo; {!! Lang::get('core.Promo_Video')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/14') !!}" rel="TestDetails"><span @if($step == '14') class="text-info" @endif>&raquo; {!! Lang::get('core.Test_Video')!!}</span> <span class="r-arrow"></span></a></li>
		</ul>
	</div>
	<div class="lach_dev_at clearfix">
		<h4 class="tabtitle"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.course_settings') !!}</h4>
		<ul class="tabtitlemang mang_develop clearfix">
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/6') !!}" rel="PrivacyDetails"><span @if($step == '6') class="text-info" @endif>&raquo;  {!! Lang::get('core.Privacy')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/7') !!}" rel="PricingDetails"><span @if($step == '7') class="text-info" @endif>&raquo;  {!! Lang::get('core.Pricing')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/10') !!}"><span @if($step == '10') class="text-info" @endif>&raquo; {!! Lang::get('core.Manage_Instructors')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/8') !!}"><span @if($step == '8') class="text-info" @endif>&raquo;  {!! Lang::get('core.Danger_Zone')!!}</span> <span class="r-arrow"></span></a></li>
		</ul>
	</div>

	<div class="lach_dev_at clearfix">
		<h4 class="tabtitle"><i class="fa fa-angle-double-right"></i> {!! Lang::get('core.more')!!}</h4>
		<ul class="tabtitlemang mang_develop clearfix">
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/15') !!}" rel="Getstarted"><span @if($step == '15') class="text-info" @endif>&raquo; {!! Lang::get('core.survey')!!}</span> <span class="r-arrow"></span></a></li>
			 <li><a href="{!! URL::to('course/create/'.$course->course_id.'/1') !!}" rel="Help"><span @if($step == '1') class="text-info" @endif>&raquo; {!! Lang::get('core.Help')!!}</span> <span class="r-arrow"></span></a></li>
		</ul>
	</div>

<!-- 	<div class="lach_dev_at clearfix">
		 <ul class="mang_develop clearfix">
			<li><a href="{!! URL::to('course/create/'.$course->course_id.'/8') !!}"><h4 class="leftnopad"><span @if($step == '8') class="text-info" @endif>Danger Zone</span></h4></a></li>
		</ul>
	</div> -->
</div>
<!--<script>
$(function(){

	$('.tabtitle').click(function () {
		$('.tabtitlemang').slideToggle();
	});

});
</script>-->