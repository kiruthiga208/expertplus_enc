<!-- <div class="cloader"></div> -->
@if(count($comments)>0)
@for($i=0;$i<count($comments);$i++)
<div class="media profile clearfix" id="comment-{{$comments[$i]->lecture_comment_id}}">

	<div class="activity-post">
		<div class="profile_image"><a href="{{ URL::to('profile/'.$comments[$i]->username) }}">
			{!! SiteHelpers::customavatar($comments[$i]->email,$comments[$i]->user_id) !!}</a>
		</div>
		<div class="activity-box discussion-activity-box">
			<div class="activity-header clearfix">
				<div class="header-right clearfix pull-right">
					<div class="hidden-btns">
						@if($comments[$i]->user_id==\Session::get('uid'))
						<a href="javascript:void(0);" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="editcomments"><i class="fa fa-pencil"></i></a>														
						<a href="javascript:void(0);" data-commentid="{!!$comments[$i]->lecture_comment_id!!}" class="removecomments"><i class="fa fa-trash-o"></i></a>
						@endif
					</div>
				</div>
				<div class="header-left clearfix">
					<a href="{{ URL::to('profile/'.$comments[$i]->username) }}"><span class="activity-header-link"><span><strong>{{$comments[$i]->first_name}} {{$comments[$i]->last_name or ''}}</strong></span></span></a>
					<a>{!! Lang::get('core.post_discussion') !!}</a>
					<span class="activity-details-flex-wrapper"><a><span class=""><strong>{!! Lang::get('core.in_lecture')!!} {{$comments[$i]->sort_order }}</strong></span></a></span>
					<span class="activity-actions-separator"> &middot; </span>
					<time class="activity-time nowrap"><a>{!! SiteHelpers::changeFormat($comments[$i]->created_at) !!}</a></time>
				</div>
			</div>
			<div>
				<div class="">
					<div class="activity-body" id="wholewrapper-{{$comments[$i]->lecture_comment_id}}">
						<div class="activity-title" id="titletext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment_title !!}</div>
						<div class="activity-content w3c-default" id="bodytext-{{$comments[$i]->lecture_comment_id}}">{!! $comments[$i]->lecture_comment !!}</div>
					</div>
					<div style="display:none;" id="commentopenbox-{{$comments[$i]->lecture_comment_id}}">
						<div class="form-group">
							<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
							<input type="text" name="posttitle" class="form-control" id="edittitle-{{$comments[$i]->lecture_comment_id}}" value="{{$comments[$i]->lecture_comment_title}}" placeholder="{!! Lang::get('core.start_discussion') !!}">
						</div>
						<div class="form-group">
							<textarea id="editctextboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $comments[$i]->lecture_comment; ?></textarea> 
						</div> 
						<div class="form-group">              
							<button class="btn btn-orange btn-bg updatecommentbtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}" >{!! Lang::get('core.sb_save')!!}</button>
							<button class="btn btn-orange btn-bg editccancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
						</div>
					</div>
					<div class="activity-actions clearfix">
						@if(Auth::check() == TRUE)
						<a href="javascript:void(0)">
							<span>
								<span><span class="text-info replybutton" id="openreplybox" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply')!!}</span></span>
							</span>
						</a>
						@endif

					</div>
					<div class="commentReplyBox" style="display:none;" id="openbox-{{$comments[$i]->lecture_comment_id}}">
						<input type="hidden" name="pid" value="{{$comments[$i]->lecture_comment_id}}">
						<div class="form-group">
							<textarea id="textboxcontent-{{$comments[$i]->lecture_comment_id}}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-orange btn-bg replybtn" type="button" data-commentid="{{$comments[$i]->lecture_comment_id}}">{!! Lang::get('core.Reply')!!}</button>
							<button class="btn btn-orange btn-bg cancelbtn" data-commentid="{{$comments[$i]->lecture_comment_id}}" type="button">{!! Lang::get('core.cancel')!!}</button>
						</div>
					</div>
					@php( $replys = SiteHelpers::getlecturereplies($comments[$i]->lecture_comment_id) )
					<div class="discussion-comments-block" id="replyies-{{$comments[$i]->lecture_comment_id}}">
						@if(count($replys)>0)
						@for($j=0;$j<count($replys);$j++)
						<div class="discussion-comments-container" id="reply-block{!!$replys[$j]->reply_id!!}">
							<li class="">
								<div>
									<div class="comment-box">
										<div class="profile_image"><a href="{{ URL::to('profile/'.$replys[$j]->username) }}">
											{!! SiteHelpers::customavatar($replys[$j]->email,$replys[$j]->user_id) !!}</a>
										</div>

										<div class="activity-box">
											<div class="activity-header">
												<div class="header-right clearfix pull-right">
													<div class="hidden-btns">
														@if($replys[$j]->user_id==\Session::get('uid'))
														<a href="javascript:void(0)" data-rcommentid="{!!$replys[$j]->reply_id!!}" class="editreply activity-edit-link mini-act-btn mini-tooltip2">
															<i class="fa fa-pencil"></i>
														</a>
														<a href="javascript:void(0)"  data-rcommentid="{!!$replys[$j]->reply_id!!}" class="removereply activity-delete-link mini-act-btn mini-tooltip2">
															<i class="fa fa-trash-o"></i>
														</a>
														@endif
													</div>
												</div>
												<div class="header-left">
													<a class="activity-header-link" href="{{ URL::to('profile/'.$replys[$j]->username) }}">{{$replys[$j]->first_name}} {{$replys[$j]->last_name or ''}}</a>
													<time class="activity-time"> &middot; {!! SiteHelpers::changeFormat($replys[$j]->created_at) !!}</time>

												</div>
											</div>
											<div class="comment-body w3c-default"><span id="replytext-{!!$replys[$j]->reply_id!!}"><?php echo $replys[$j]->reply_comment; ?> </span></div>
										</div>
									</div>
								</div>
							</li>

							<div style="display:none;" id="replyopenbox-{!!$replys[$j]->reply_id!!}">
								<input type="hidden" name="pid" value="{!!$replys[$j]->reply_id!!}">
								<div class="question-answers-creation">
									<div class="comment-creation">
										<div class="comment-submit-box">
											<div class="form-group">
												<textarea id="edittextboxcontent-{!!$replys[$j]->reply_id!!}" class="form-control lectureeditor" rows="2" placeholder="Comment" name="comment" cols="50"><?php echo $replys[$j]->reply_comment; ?></textarea>
											</div>
											<div class="form-group">
												<button class="btn btn-orange btn-bg updatereplybtn" type="button" data-commentid="{!!$replys[$j]->lecture_comment_id!!}" data-replyid="{!!$replys[$j]->reply_id!!}">{!! Lang::get('core.sb_save')!!}</button>
												<button class="btn btn-orange btn-bg editcancelbtn" data-commentid="{!!$replys[$j]->reply_id!!}" type="button">{!! Lang::get('core.cancel')!!}</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endfor
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endfor
@endif