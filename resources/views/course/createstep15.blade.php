@extends('layouts.frontend')
@section('content')
<link href="{{ asset('assets/bsetec/static/css/front/style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/sub-style.css') }}" rel="stylesheet">
<link href="{{ asset('assets/bsetec/static/css/front/common.css') }}" rel="stylesheet">
<style type="text/css">
.form-group{ margin:25px 0px; }
.leftnopad{padding-left:0px !important;}
</style>
<div class="create-course help-s">
<div class="container">
	<div class="row">
		<div class="col-sm-12">
            @include('course.courseheader')
        </div>
		<div class="col col-sm-3 multi_development">
			@include('course.createsidebar')
		</div>
		<div class="col col-sm-9">
		
			 <div class="lach_dev resp-tab-content course_tab"> 
                <div class="slider_divsblocks">
					<div>
						{!! Form::open(array('url'=>'course/survey', 'method' => 'post', 'class'=>'form-horizontal saveLabel','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
						{!! Form::hidden('course_id', $course->course_id )  !!}
						{!! Form::hidden('step', $step )  !!}
						{!! Form::hidden('survey', $course->survey )  !!}
<!--						<div class="course_basic course_newbasic">Help us Help you!<h5>Congratulations! You're on your way to sharing your expertise with 4+ million students around the world.<br /><br />We've helped thousands of instructors to plan, produce, and promote their amazing courses. Help us deliver custom tailored resources to you by answering a few quick questions:</h5></div>-->
						
                        
                        <div class="course_basic course_newbasic"><h4>{!! Lang::get('core.help_us')!!} </h4><p>{!! Lang::get('core.survey_text')!!}</p></div>
						
					
							<h3 class="title_block">{!! Lang::get('core.planning') !!}</h3>
				
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.planning_text')!!}</label>
							
							<select name="lang_id" class="form-control" required>
									<option value="">-- {!! Lang::get('core.Select_Language')!!} --</option>
									@foreach(\bsetecHelpers::siteLanguage() as $language)
									@if((isset($coursesurvey->language_id)) && $language->id == $coursesurvey->language_id)
									<option value="{!!  $language->id  !!}" selected="selected">{!!  $language->native_name  !!}</option>
									@elseif($language->id == $course->lang_id)
									<option value="{!!  $language->id  !!}" selected="selected">{!!  $language->native_name  !!}</option>
									@else
									<option value="{!!  $language->id  !!}">{!!  $language->native_name  !!}</option>
									@endif
									@endforeach
								</select>
							
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.topic_course')!!}</label>
					
							<select name="cat_id" class="form-control" required>
								<option value="9">--{!! Lang::get('core.Select_Category')!!}--</option>
								@foreach(\bsetecHelpers::siteCategories() as $category)
								@if((isset($coursesurvey->category_id)) && $category->id == $coursesurvey->category_id)
								<option value="{!!  $category->id  !!}" selected="selected">{!!  $category->name  !!}</option>
								@elseif($category->id == $course->cat_id)
								<option value="{!!  $category->id  !!}" selected="selected">{!!  $category->name  !!}</option>
								@else
								<option value="{!!  $category->id  !!}">{!!  $category->name  !!}</option>
								@endif
								@endforeach
								</select>
						
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.goal_teaching')!!}</label>
					
								<select class="form-control" id="objective_persona" name="objective_persona" required>
									<option value="">-- {!! Lang::get('core.Select_One')!!} --</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.goal_taching_sel1')) selected="selected" @endif value="{!! Lang::get('core.goal_taching_sel1')!!}">{!! Lang::get('core.goal_taching_sel1')!!}</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.goal_taching_sel2')) selected="selected" @endif value="{!! Lang::get('core.goal_taching_sel2')!!}">{!! Lang::get('core.goal_taching_sel2')!!}</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.goal_taching_sel3')) selected="selected" @endif value="{!! Lang::get('core.goal_taching_sel3')!!}">{!! Lang::get('core.goal_taching_sel3')!!}</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.goal_taching_sel4')) selected="selected" @endif value="{!! Lang::get('core.goal_taching_sel4')!!}">{!! Lang::get('core.goal_taching_sel4')!!}</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.goal_taching_sel5')) selected="selected" @endif value="{!! Lang::get('core.goal_taching_sel5')!!}">{!! Lang::get('core.goal_taching_sel5')!!}</option>
									<option @if((isset($coursesurvey->objective_persona)) && $coursesurvey->objective_persona == Lang::get('core.none')) selected="selected" @endif value="{!! Lang::get('core.none')!!}">{!! Lang::get('core.none')!!}</option>
									</select>
							
						</div>
						
							<h3 class="title_block">{!! Lang::get('core.Producing')!!}</h3>
					
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.Producing_txt')!!}</label>
						
							<select class="form-control" id="resource_persona" name="resource_persona" required>
									<option value="">-- {!! Lang::get('core.Select_One')!!} --</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.producing_sel1')) selected="selected" @endif value="{!! Lang::get('core.producing_sel1')!!}">{!! Lang::get('core.producing_sel1')!!}</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.producing_sel2')) selected="selected" @endif value="{!! Lang::get('core.producing_sel2')!!}">{!! Lang::get('core.producing_sel2')!!}</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.producing_sel3')) selected="selected" @endif value="{!! Lang::get('core.producing_sel3')!!}">{!! Lang::get('core.producing_sel3')!!}</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.producing_sel4')) selected="selected" @endif value="{!! Lang::get('core.producing_sel4')!!}">{!! Lang::get('core.producing_sel4')!!}</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.producing_sel5')) selected="selected" @endif value="{!! Lang::get('core.producing_sel5')!!}">{!! Lang::get('core.producing_sel5')!!}</option>
									<option @if((isset($coursesurvey->resource_persona)) && $coursesurvey->resource_persona == Lang::get('core.none')) selected="selected" @endif value="{!! Lang::get('core.none')!!}">{!! Lang::get('core.none')!!}</option>
									</select>
							
						</div>
						
							<h3 class="title_block">{!! Lang::get('core.Promoting_course')!!}</h3>
					
					
<p class="details">{!! Lang::get('core.Promoting_text')!!}</p>
				
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.email_subscriber')!!}</label>
						
								<select class="form-control" id="email_subscribers" name="email_subscribers" required>
									<option value="">-- {!! Lang::get('core.Select_One')!!} --</option>
									<option @if((isset($coursesurvey->email_subscribers)) && $coursesurvey->email_subscribers == Lang::get('core.promoting_course_sel1')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel1')!!}">{!! Lang::get('core.promoting_course_sel1')!!}</option>
									<option @if((isset($coursesurvey->email_subscribers)) && $coursesurvey->email_subscribers == Lang::get('core.promoting_course_sel2')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel2')!!}">{!! Lang::get('core.promoting_course_sel2')!!}</option>
									<option @if((isset($coursesurvey->email_subscribers)) && $coursesurvey->email_subscribers == Lang::get('core.promoting_course_sel3')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel3')!!}">{!! Lang::get('core.promoting_course_sel3')!!}</option>
									<option @if((isset($coursesurvey->email_subscribers)) && $coursesurvey->email_subscribers == Lang::get('core.promoting_course_sel4')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel4')!!}">{!! Lang::get('core.promoting_course_sel4')!!}</option>
									</select>
							
						</div>
						<div class="form-group">
							<label for="label" class="col-xs-12">{!! Lang::get('core.subscribers')!!}</label>
						
								<select class="form-control" id="youtube_subscribers" name="youtube_subscribers" required>
									<option value="">-- {!! Lang::get('core.Select_One')!!} --</option>
									<option @if((isset($coursesurvey->youtube_subscribers)) && $coursesurvey->youtube_subscribers == Lang::get('core.subscriber_sel1')) selected="selected" @endif value="{!! Lang::get('core.subscriber_sel1')!!}">{!! Lang::get('core.subscriber_sel1')!!}</option>
									<option @if((isset($coursesurvey->youtube_subscribers)) && $coursesurvey->youtube_subscribers == Lang::get('core.promoting_course_sel2')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel2')!!}">{!! Lang::get('core.promoting_course_sel2')!!}</option>
									<option @if((isset($coursesurvey->youtube_subscribers)) && $coursesurvey->youtube_subscribers == Lang::get('core.promoting_course_sel3')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel3')!!}">{!! Lang::get('core.promoting_course_sel3')!!}</option>
									<option @if((isset($coursesurvey->youtube_subscribers)) && $coursesurvey->youtube_subscribers == Lang::get('core.promoting_course_sel4')) selected="selected" @endif value="{!! Lang::get('core.promoting_course_sel4')!!}">{!! Lang::get('core.promoting_course_sel4')!!}</option>
									</select>
						
						</div>
						<div class="form-group">
					<input type="submit" name="" value="{!! Lang::get('core.sb_save')!!}" class="btn btn-color">  
							
						</div>
						{!! Form::close() !!}

					</div>
                </div>
            </div> 
			
		</div>
		
	</div>
</div>
</div>
@stop