<?php

$rateid 			= 0;
$rate 				= 0;
$text 				= 'Add Review';
if(count($rateinfo)>0){
	$title 			= $rateinfo['0']->review_title;
	$description 	= $rateinfo['0']->review_description;
	$rate 			= $rateinfo['0']->rating;
	$rateid 		= $rateinfo['0']->ratings_id;
	$text 			= 'Update Review';
}

?>
{!! Form::open(array('method' => 'post','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
<div id="loader"></div>
<div id="buttons">
<div class="form-group">
	<label for="recipient-name" class="control-label">{!! Lang::get('core.Your_Rating')!!}:</label>
	<div id="rating-div"  data-score="{!! $rate !!}" class="span5 choose_star"></div>
</div>

<div class="form-group">
	<label for="message-text" class="control-label">{!! Lang::get('core.rating_review')!!} :</label>
	<textarea class="form-control" name="reviewmsg" id="reviewmsg"  >{!! $description or '' !!}</textarea>
</div>
<div class="form-group">
	
		<button type="button" id="savereview" class="btn btn-primary">{!! $text  !!}</button>
		@if(count($rateinfo)>0)
		<button type="button" id="deleterate" data-deleteid="{!! $rateid !!}" class="btn btn-danger">{!! Lang::get('core.delete_review') !!}</button>
		@endif
	</div>
</div>

{!! Form::hidden('course_id', $courseid)  !!}
{!! Form::hidden('rate_id', $rateid )  !!}

{!! Form::close() !!}

<script type="text/javascript">
$(function(){

	$('#rating-div').raty({score: function() {return $(this).attr('data-score');},width: 150,starOff : "{{url('').'/assets/bsetec/static/img/star-off.png'}}",starOn: "{{url('').'/assets/bsetec/static/img/star-on.png'}}",starHalf : "{{url('').'/assets/bsetec/static/img/star-half.png'}}"});

	$(document).on('click','#savereview',function(e){
		var rating = $('input[name=score]').val();
		var courseid    = $('[name="course_id"]').val();
		var rateid    = $('[name="rate_id"]').val();
		var reviewmsg = $('#reviewmsg').val();
		
		if($.trim(rating).length===0){
			toastr.error("Error", "{!! Lang::get('core.rating_error')!!}");
			toastr.options = {
				"closeButton": true,
				"debug": false,
				"positionClass": "toast-bottom-right",
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
				"preventDuplicates": true
			}
		}else if ($.trim(rating).length>0){
			var me = $(this);
			e.preventDefault();
			if ( me.data('requestRunning') ) {
				return;
			}
			me.data('requestRunning', true);
			$.ajaxSetup({
				headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
			});
			$.ajax({
				type: 'POST',
				url: '<?php echo e(\URL::to("course/reviewrating")); ?>',
				data:'description='+reviewmsg+'&cid='+courseid+'&rate='+rating+'&rateid='+rateid,
				beforeSend: function() {
					$('#buttons').toggle();
					$('#loader').html('<h2><i class="fa fa-refresh fa-spin"></i>{!! Lang::get("core.loading")!!}</h2>');
				},
				success : function(data) {
					var obj = jQuery.parseJSON(data);
					if(obj.status=='1'){
						$('#ratingmodel').modal('hide');
						$('#reviewmsg').val('');
						$('#reviewtitle').val('');
						toastr.success("Success", "{!! Lang::get('core.rating_success')!!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}else if(obj.status=='2'){
						toastr.error("Error", "{!! Lang::get('core.comments_error') !!}");
						toastr.options = {
							"closeButton": true,
							"debug": false,
							"positionClass": "toast-bottom-right",
							"onclick": null,
							"showDuration": "300",
							"hideDuration": "1000",
							"timeOut": "5000",
							"extendedTimeOut": "1000",
							"showEasing": "swing",
							"hideEasing": "linear",
							"showMethod": "fadeIn",
							"hideMethod": "fadeOut"
						}
					}
					return false;
				},complete: function() {
					me.data('requestRunning', false);
					$('#buttons').toggle();
					$("#loader").html("");
				}
			});
		}
	});
});

$('#deleterate').on('click',function(e){
	var deleteid = $(this).data('deleteid');
	// e.preventDefault();
	// e.stopPropagation();
	if (confirm("{!! Lang::get('core.rating_delete') !!}")) {
		var me = $(this);
		if ( me.data('requestRunning') ) {
			return;
		}
		me.data('requestRunning', true);
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN': $('input[name="_token"]').val() }
		});
		$.ajax({
			type: 'POST',
			url: '<?php echo e(\URL::to("course/deletereview")); ?>',
			data:'did='+deleteid,
			success : function(data) {
				$('#ratingmodel').modal('hide');
				if(data=='1'){
					$('#ratingmodel').modal('hide');
					$('#reviewmsg').val('');
					$('#reviewtitle').val('');
					toastr.success("Success", "{!! Lang::get('core.review_del_sucess')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}else if(data=='2'){
					toastr.error("Error", "{!! Lang::get('core.comments_error')!!}");
					toastr.options = {
						"closeButton": true,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					}
				}
			},complete: function() {
				me.data('requestRunning', false);
				
			}
		});
	}
	return false;
});
</script>