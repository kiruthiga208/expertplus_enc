<style>
input[type=number] {
    -moz-appearance:textfield;
}
</style>
<div class="confirm-section">
                <div class="container">                
                  <h2 class="confirm-title">{!! Lang::get('core.confirm_purchase') !!}</h2>    
                   <div class="table-payment">  

                    <div id="couponError"></div>       
                <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
                <tr>
                <!-- <th width="10%">Sl No:</th> -->
                <th width="90%">{!! Lang::get('core.name_content') !!}</th>
                <th width="10%">{!! Lang::get('core.price')!!}</th>
                </tr>
                <tr>
               @if (defined('CNF_CURRENCY'))
                        @php( $currency = SiteHelpers::getCurrentcurrency(CNF_CURRENCY) )
                @endif
                <!-- <td>1</td> -->
                <td>{{$course->business_plan_name }}</td>
                <td> {{$currency .' '. $you_pay}}</td>
                </tr>
                <!-- @if($admin_discount)
                <tr>
               
                <td>{!! Lang::get('core.admin_discount') !!}</td>
                <td> {{ $currency.' '.$discount_amount }}</td>
                </tr>
                @endif  -->
                
                <!-- <tr>
                <td style="text-align:right;">VAT (20%)</td>
                <td>{!! $currency !!} <span id="you_pay">{{$you_pay*20/100}}</span><span id="coupon_price"></span></td>
                </tr> -->
                <tr>
                <td style="text-align:right;">Total</td>
                <td>{!! $currency !!} <span id="you_pay">{{$you_pay}}</span><span id="coupon_price"></span></td>
                </tr>
                </table>     
                </div>      
                <div class="payment-block clearfix">
                @if($errors->any())
                    <div class="alert alert-danger">
                       <a class="close" data-dismiss="alert">×</a>
                       {!! Lang::get('core.payment_error') !!}
                    </div>
                @endif
                
                
                 <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                @php( $i=1 ) 
                @if(count($payment)>0)  
                    @php(  $me = array('ccavenue','bank','cod') )
                   @foreach ($payment as $key => $method)
                    @php( $kk = in_array($key, $me) )
                        @if($kk==1)
                            @php( $cc = \bsetecHelpers::ifInstalled($key) )
                        @endif
                        @if($i==1)
                            @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                                
                            @else
                                 <li class="active"><a href="{!! '#'.$key !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                            @endif
                            <script>
                                $(document).ready(function(){
                                    var active = $('#tabs').find('.active').children().attr('href');
                                    $(active).addClass('active');
                                });
                            </script>
                        @else
                            @if($key=='ccavenue' || $key == 'bank' || $key=='cod')
                               
                            @else
                                <li class=""><a href="{!! '#'.$key !!}" class="{!! $key.'-tab' !!}" data-toggle="tab">{!! Lang::get('core.'.$key) !!}</a></li>
                            @endif
                        @endif
                        @php( $i++ )
                    @endforeach
                @else
                     <h2>{!! Lang::get('core.no_payment')!!}</h2>
                @endif
                </ul>
                
                

                 <div id="my-tab-content" class="tab-content">

    @foreach ($payment as $key => $method)
                @if($key == 'paypal_express_checkout' && $method['payment']=='1')
                <div id="paypal_express_checkout" class="tab-pane ">
                {!! Form::open(array('url' => url('payment/form'))) !!}
                <input type="hidden" name="course_id" value="{{$course->business_plan_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->business_plan_name}}" /> 
                <input type="hidden" name="payment_method" value="paypal_express_checkout" /> 
                
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif

                @if($key == 'paypal_standard' && $method['payment']=='1' )
                <div id="paypal_standard" class="tab-pane">
                {!! Form::open(array('url' => url('payment/form'),'parsley-validate'=>'','novalidate'=>' ' )) !!}
                <input type="hidden" name="course_id" id="course_id" value="{{$course->business_plan_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->business_plan_name}}" /> 
                <input type="hidden" name="payment_method" value="paypal_standard" /> 
                
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="submit" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                </div>
                </div> 
                {!! Form::close() !!}
                </div>
                @endif

                @if($key == 'stripe' && $method['payment']=='1')
                <div id="stripe" class="tab-pane col-md-6 ">
                {!! Form::open(array('url' => url('payment/stripe'),'id'=>'payment-stripe' )) !!}
                <span class="payment-errors"></span>
                <input name='stripeToken' id="stripeToken" type="hidden" />
                <input type="hidden" name="course_id" id="course_id" value="{{$course->business_plan_id}}" /> 
                <input type="hidden" name="course_title" value="{{$course->business_plan_name}}" /> 
                <input type="hidden" name="payment_method" value="stripe" /> 
                
                <div class="form-group clearfix">
                <label for="Card Number" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_number')!!} <span class="asterix"> * </span></label>
                <div class="col-sm-8">
                {!! Form::text('card-number', '' , array('class'=>'form-control','id'=>'card-number','required'=>true,'placeholder'=>Lang::get('core.enter_card'),'autocomplete'=>'off')) !!} 
                </div>
                </div>
                
                <div class="form-group clearfix">
                <label for="card-cvc" class="control-label col-sm-4 text-left"> {!! Lang::get('core.card_cvc') !!} <span class="asterix"> * </span></label>
                <div class="col-sm-3">
                {!! Form::input('number','card-cvc', '' , array('class'=>'form-control','maxlength'=>'3','id'=>'card-cvc','required'=>true,'placeholder'=>Lang::get('core.enter_cvc'),'autocomplete'=>'off')) !!} 
                </div>
                </div>
                <div class="form-group clearfix">
                <label for="expire" class="col-sm-4">{!! Lang::get('core.Expiration') !!} <span class="asterisk"> * </span></label>
                <div class="expiry-wrapper clearfix">
                <div class="col-xs-6 col-sm-4">
                <div class="select-style_block">
                <select class="stripe-sensitive required" id="card-expiry-month">
                <option value="" selected="selected">{!! Lang::get('core.mm') !!}</option>
                </select>
                </div>
                <script type="text/javascript">
                var select = $("#card-expiry-month"),
                month = new Date().getMonth() + 1;
                for (var i = 1; i <= 12; i++) {
                select.append($("<option value='"+i+"'>"+i+"</option>"))
                }
                </script>
                </div>
                <div class="col-xs-6 col-sm-4">
                <div class="select-style_block">
                <select class="stripe-sensitive required" id="card-expiry-year">
                <option value="" selected="selected">{!! Lang::get('core.year') !!}</option>
                </select>
                </div>
                <script type="text/javascript">
                var select = $("#card-expiry-year"),
                year = new Date().getFullYear();
                for (var i = 0; i < 12; i++) {
                select.append($("<option value='"+(i + year)+"'>"+(i + year)+"</option>"))
                }
                </script>
                </div>
                </div>
                </div>
                <div class="form-group clearfix">
                <div class="col-sm-4 submit_block"><label style="visibility:hidden;">{!! Lang::get('core.sb_submit') !!}</label></div>
                <div class="col-sm-8">
                <input type="hidden" value="{!! $method['publishable_key'] !!}" class="stripe_key" />
                <input type="submit" id="submit_button" value="{!! Lang::get('core.payment_option') !!}" class="btn btn-color">
                <!-- <input type="button" value="Buy with Credit balance $ {{ \bsetecHelpers::getUserCredit()}}" id="BuyWithCredit" class="btn btn-primary btn_pay"> -->
                </div>
                </div> 
                
                {!! Form::close() !!}
                </div>
                @endif
                @endforeach
                </div>

                </div>
                </div>
                </div>

<!-- payment plugins -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript">
// payment options script start
var keyy = $('.stripe_key').val();
Stripe.setPublishableKey(keyy);
var payment_error = $('.payment-errors');
$(document).ready(function() {
    function submit(form) {
        // given a valid form, submit the payment details to stripe
        $(form['submit-button']).attr("disabled", "disabled")
        Stripe.createToken({
            number: $('#card-number').val(),
            cvc: $('#card-cvc').val(),
            exp_month: $('#card-expiry-month').val(), 
            exp_year: $('#card-expiry-year').val()
        },function(status, response) {
            if (response.error) {
                // re-enable the submit button
                $(form['submit-button']).removeAttr("disabled")
                // show the error
               payment_error.html(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the stripe token
                $('#stripeToken').val(token);
                // and submit
                form.submit();
            }
        });
        
        return false;
    }
            
    // add custom rules for credit card validating
    jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
    jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
    // We use the jQuery validate plugin to validate required params on submit
    $("#payment-stripe").validate({
        submitHandler: submit,
        rules: {
            "card-cvc" : {
                cardCVC: true,
            },
            "card-number" : {
                cardNumber: true,
            },
        },
        errorPlacement: function(error, element){
            payment_error.html(" ");
            payment_error.html(error);
        }
    });
});


$('#course_quantity').on('keypress',function(){
    alert($(this).val());
})



</script>
<style>
.payment-errors{
    color:red;
}
</style>
