<?php 
define('CNF_APPNAME','Expert Plus');
define('CNF_APPDESC','learning management system');
define('CNF_COMNAME','BSEtec');
define('CNF_EMAIL','testtrending@gmail.com');
define('CNF_METAKEY','my site , my company  , Larvel Crud');
define('CNF_METADESC','Write description for your site');
define('CNF_GROUP','2');
define('CNF_ACTIVATION','confirmation');
define('CNF_MULTILANG','1');
define('CNF_LANG','en');
define('CNF_REGIST','true');
define('CNF_FRONT','true');
define('CNF_RECAPTCHA','false');
define('CNF_THEME','theme1');
define('CNF_RECAPTCHAPUBLICKEY','');
define('CNF_RECAPTCHAPRIVATEKEY','');
define('CNF_MODE','production');
define('CNF_LOGO','b-logo.png');
define('CNF_FLOGO','f-logo.png');
define('CNF_FAV','fav-favicon.ico');
define('CNF_CURRENCY','USD');
define('CNF_RTL','0');?>