<?php

/* ====================================================
*
*                 PHP Setup Wizard
*
*         -= FINAL OUTPUT CONFIG FILE =-
*
*  This file is a "mask" of the final output file, 
*  and it should be customized by YOU to fit your 
*  personal PHP system or script.
*  
*  The installer will read this file into one 
*  block of string and replace the keywords with 
*  the actual values
*  
*  Remove this comment from the file - othervice 
*  it will be included in your config file in your 
*  script after installation :)
*  
*  ==================================================*/

// Serial and/or Trial
define('LICENCE_SERIAL_KEY',    '{keyvalue}');
define('LICENCE_TRIAL_MODE',    {isTrial});
define('LICENCE_TRIAL_TIMEOUT', {trialtime});


// Want to set when system was installed?
$system = array();
$system['title'] = '{product}';
$system['company'] = '{company}';
$system['version'] = '{version}';
$system['installed'] = '{datenow}';

// Database connection
$dbConn = array();
$dbConn['hostname']  = '{hostname}';
$dbConn['database']  = '{database}';
$dbConn['username']  = '{username}';
$dbConn['password']  = '{password}';
$dbConn['prefix']    = '{dbprefix}'; 
$dbConn['port']      = '{dbport}'; 

// Time based settings
$time = array();
$time['language'] = '{language}';
$time['timezone'] = '{timezone}';

// Basic Administrator information
$admin = array();
$admin['admin_name'] = '{admin_realname}';
$admin['admin_email'] = '{admin_email}';

?>