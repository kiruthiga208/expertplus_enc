-- insert admin details --
INSERT INTO `{dbprefix}users` (`id`,`group_id`, `username`, `password`, `first_name`, `last_name`, `email`, `active`, `created_at`) 
VALUES (NULL, 1, '{admin_username}', '{admin_password}', '{admin_firstname}', '{admin_lastname}', '{admin_email}', 1, '{admin_created}');
-- insert website name --
INSERT INTO `{dbprefix}options` (`id`,`code`, `option_key`, `option`) 
VALUES (NULL, 'sitename', 'sitename', '{web_site_name}');