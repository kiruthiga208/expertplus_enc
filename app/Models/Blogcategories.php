<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
class Blogcategories extends bsetec  {
	
	protected $table = 'blogcategories';
	protected $primaryKey = 'CatID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."blogcategories.* FROM ".\bsetecHelpers::getdbprefix()."blogcategories  ";
	}
	public static function queryWhere(  ){
		
		return " WHERE ".\bsetecHelpers::getdbprefix()."blogcategories.CatID IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
