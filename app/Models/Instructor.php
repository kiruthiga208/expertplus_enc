<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instructor extends Model {
	protected $table 		= 'instructor';
	protected $primaryKey 	= 'instructor_id';

	public $timestamps = false;
			
	public function __construct() {
		parent::__construct();
	}

	protected function getDateFormat()
    {
        return 'U';
    }

    public function getInstructors(){
    	return  \DB::table('instructor')
    			->join('users','users.id', '=', 'instructor.user_id')
    			->whereNotNUll('instructor.p_email')->where('instructor.p_email','!=','')
    			->paginate(\Config::get('site.course_items_per_page'));
    }	
}
