<?php
/**
 * Company : Bsetec
 * Models : Search
 * Email : support@bsetec.com
 */
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Course;
use File;
class Api_droid extends bsetec
{
    public function __construct() 
    {
        parent::__construct();
        $this->courses = new Course();
        
    }

   /* public function course($type, $value)
    {
        if($type == 'price')
        {
            $split = explode('-', $value);
            $price_from = $split[0];
            $price_to = $split[1];
            $resultt = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->whereBetween('pricing', array($price_from,$price_to))
                        ->where('course.deleted_at', '=', NULL)->where('course.privacy','=', 1)->where('course.approved','=',1)->orderBy('course.created_at', 'desc')->get();
        }
        else if($type == 'category_id'){
            $resultt = \DB::table('course')
                    ->join('users', 'users.id', '=', 'course.user_id')
                    ->join('course_images','course_images.id','=','course.image')
                    ->select('course.course_id','course.course_title','course.description','course.pricing','users.username','course_images.image_hash','course.image')
                    ->where('course.cat_id', '=', $value)->where('course.deleted_at', '=', NULL)->where('course.approved','=',1)
                    ->where('course.privacy','=', 1)
                    ->orderBy('course.created_at', 'desc')->get();
        }
        else if($type == 'instruction_level'){
             $level =array('all'=>'4','beginner'=>'1','intermediate'=>'2','advanced'=>'3');
             $resultt = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->where('course.course_level', '=', $level[$value])
                        ->where('course.deleted_at', '=', NULL)->where('course.privacy','=', 1)->where('course.approved','=',1)->orderBy('course.created_at', 'desc')
                        ->get();
        }
        else if($type == 'sort_by')
        {
            switch($value)
            {
                case 'featured':
                    $resultt = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                      ->join('course_images','course_images.id','=','course.image')
                      ->join('featured', 'featured.course_id', '=', 'course.course_id')
                      ->where('course.approved','=', 1)
                      ->where('course.privacy','=', 1)
                      ->orderBy('course.created_at', 'desc')
                      ->groupBy('course.course_id')
                      ->get();
                break;
                case 'freecourses':
                 $resultt = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                            ->join('course_images','course_images.id','=','course.image')
                            ->where('course.pricing', '=', 0)
                            ->where('course.approved','=','1')
                            ->where('course.privacy','=', 1)
                            ->select('course.*','users.username','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                            ->groupBy('course.course_id')
                            ->orderBy('ratings', 'desc')
                            ->get();
                break;
                case 'toprated':
                    $resultt = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                              ->join('course_images','course_images.id','=','course.image')
                              ->join('ratings','ratings.course_id','=','course.course_id')
                              ->select('course.*','users.username','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                              ->where('course.approved', '=', 1)
                              ->where('course.privacy', '=', 1)
                              ->groupBy('ratings.course_id')
                              ->orderBy('ratings','DESC')
                              ->get();
                break;
                case 'topfree':
                    $resultt = \DB::table('course')
                              ->join('users', 'users.id', '=', 'course.user_id')
                              ->join('course_images','course_images.id','=','course.image')
                              ->join('ratings','ratings.course_id','=','course.course_id')
                              ->select('course.*','users.username','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                              ->where('course.approved', '=', 1)
                              ->where('course.pricing', '=', 0)
                              ->where('course.privacy', '=', 1)
                              ->groupBy('course.course_id')
                              ->orderBy('ratings', 'desc')
                              ->get();
                break;
                case 'toppaid':
                    $resultt =  \DB::table('course')
                                ->join('users', 'users.id', '=', 'course.user_id')
                                ->join('course_images','course_images.id','=','course.image')
                                ->where('course.approved', '=', 1)
                                ->where('course.pricing', '!=', 0)
                                ->groupBy('course.course_id')
                                ->where('course.privacy','=', 1)
                                ->orderBy('course.pricing', 'desc')
                                ->orderBy('course.created_at', 'desc')
                                ->get();
                break;
                case 'mostviewed':
                    $resultt = \DB::table('course')
                              ->join('hits', 'hits.course_id', '=', 'course.course_id')
                              ->join('users', 'users.id', '=', 'course.user_id')
                              ->join('course_images','course_images.id','=','course.image')
                              ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                              ->where('course.approved','=', 1)
                              ->where('course.privacy','=', 1)
                              ->groupBy('hits.course_id')
                              ->orderBy('hits','desc')
                              ->get();
                break;
                default:
                    $resultt='';
                break;
            }
        }
        $url = url();
        $current_url = str_replace('api/public','', $url);
        if(count($resultt>0)){
           foreach ($resultt as $key => $val) {
             $result[]=array(
                'course_id'=>$val->course_id,
                'course_name'=>$val->course_title,
                'course_description'=>strip_tags($val->description),
                'price'=>$val->pricing,
                'author'=>$val->username,
                'rating'=>$this->checkReview($val->course_id),
                'course_image'=>$current_url.'course/image/'.$val->image_hash.'/_medium',
                );
            }
        }
        return $result;
    }*/
    /*
    * @process --- courselisting
    */
    public function course($cat_id='',$price='',$ins_level='',$sort_by='',$user_id='',$page='')
    {


      $custom_query = '';
      $join = '';
      if($price)
      {
        $split = explode('-', $price);
        $price_from = $split[0];
        $price_to = $split[1];
      }

      $level =array('all'=>'4','beginner'=>'1','intermediate'=>'2','advanced'=>'3');
      if($cat_id)
      {
        $join ="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
        $custom_query =" where `".env('DB_PREFIX')."course`.`cat_id`=".$cat_id." AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC";
        //$page =" LIMIT 10,10";
        $final_query = $join.$custom_query;   
      }

      if(($price)&&($ins_level)&&($sort_by))
      {
        $price =" `".env('DB_PREFIX')."course`.`pricing` BETWEEN $price_from AND $price_to";
        $ins_level =" AND `".env('DB_PREFIX')."course`.`course_level`='".$level[$ins_level]."' AND `".env('DB_PREFIX')."course`.`approved`='1'";
        $final_query = $this->support_sortby($price,$ins_level,$sort_by);

      }else if(($price)&&($ins_level))
      {
          $join ="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
          $custom_query =" where `".env('DB_PREFIX')."course`.`course_level`='".$level[$ins_level]."' AND `".env('DB_PREFIX')."course`.`pricing` BETWEEN $price_from AND $price_to AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC ";
          $final_query = $join.$custom_query;   

      }else if(($price)&&($sort_by))
      {
        $price =" `".env('DB_PREFIX')."course`.`pricing` BETWEEN $price_from AND $price_to AND `".env('DB_PREFIX')."course`.`approved`='1'";
        $final_query = $this->support_sortby($price,'',$sort_by);

      }else if(($ins_level)&&($sort_by))
      {
          $ins_level ="  `".env('DB_PREFIX')."course`.`course_level`='".$level[$ins_level]."' AND `".env('DB_PREFIX')."course`.`approved`='1'";
          $final_query = $this->support_sortby('',$ins_level,$sort_by);
      }else{
          if($price)
          {

            $join ="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
            $custom_query =" where `".env('DB_PREFIX')."course`.`pricing` BETWEEN $price_from AND $price_to AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC ";
            $final_query = $join.$custom_query;
          }
          if($ins_level)
          {
            $level =array('all'=>'4','beginner'=>'1','intermediate'=>'2','advanced'=>'3');
            $join ="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
            $custom_query =" where `".env('DB_PREFIX')."course`.`course_level`='".$level[$ins_level]."' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC";
            $final_query = $join.$custom_query;
          }
          if($sort_by)
          {
            switch($sort_by)
            {
              case 'featured':
                $join .="select *,count(`".env('DB_PREFIX')."featured`.`course_id`)as `featured` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                // $join .="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                $join .="join `".env('DB_PREFIX')."featured` ON `".env('DB_PREFIX')."featured`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                
                $custom_query =" where `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1'  GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `featured`  DESC ";
              break;
              case 'freecourses':
                     $join .="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     // $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where `".env('DB_PREFIX')."course`.`pricing`='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC ";
                    // $custom_query =" where `".env('DB_PREFIX')."course`.`pricing`='0'  AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `rate` DESC ";

              break;
              case 'toprated':
                     $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."ratings`.`course_id` ORDER BY `ratings` DESC ";
              break;
              case 'topfree':
                    $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` left join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` left join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="left join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where `".env('DB_PREFIX')."course`.`pricing`='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `ratings` DESC ";
              break;
              case 'toppaid':
                     $join .="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     //$join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where `".env('DB_PREFIX')."course`.`pricing`!='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1'  ORDER BY `".env('DB_PREFIX')."course`.`pricing` DESC, `".env('DB_PREFIX')."course`.`created_at` DESC ";
              break;
              case 'mostviewed':
                     $join .="select *,count(".env('DB_PREFIX')."hits.course_id)as `hits` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` left join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."hits` ON `".env('DB_PREFIX')."hits`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."hits`.`course_id` ORDER BY `hits` DESC ";
              break;
              default:
                  $resultt='';
              break;
            }
            $final_query = $join.$custom_query;
          }
      }
       
      $page = $page-1;
      $start = $page.'0';
      $pagee = " LIMIT ".$start.",10";
      $total_count =  \DB::select(\DB::raw($final_query));
      $tot_cnt = count($total_count);
      $resultt = \DB::select(\DB::raw($final_query.$pagee));
      $url = url('');
      $result = array();
        $current_url = str_replace('api-droid','', $url);
        if(count($resultt)>0){

           foreach ($resultt as $key => $val) {
              $description = str_replace("\n", '', $val->description);
              $description = str_replace("\r", '', $description);
              $subscribe = ($user_id==$val->user_id)? 1 : $this->isSubscribecourse($val->course_id,$user_id);
              if($val->approved==1){
                $result[]=array(
                'c_user_id'=>$val->user_id,
                'course_id'=>$val->course_id,
                'course_name'=>$val->course_title,
                'course_description'=>strip_tags($description),
                'price'=>$val->pricing,
                'author'=>$val->username,
                'rating'=>$this->checkReview($val->course_id),
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'is_subscibed'=>$subscribe,
                'is_wishlist'=>$this->Iscoursewishlist($val->course_id,$user_id),
                'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,
                );
              }
            }
        }
        return array('status'=>true,'total'=>$tot_cnt, 'result'=>$result);
     // return $result;
    }

    /*
    * @process -- support  sort by function 
    */
    public function support_sortby($price = '',$level ='',$sort_by)
    {
      $query = $price.$level.' AND';
      $join ='';

      switch($sort_by)
            {
              case 'featured':
                $join .="select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                $join .="join `".env('DB_PREFIX')."featured` ON `".env('DB_PREFIX')."featured`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                $custom_query =" where $query `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC ";
              break;
              case 'freecourses':
                     $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where $query `".env('DB_PREFIX')."course`.`pricing`='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `ratings` DESC ";
              break;
              case 'toprated':
                     $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where $query `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."ratings`.`course_id` ORDER BY `ratings` DESC ";
              break;
              case 'topfree':
                    $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where $query `".env('DB_PREFIX')."course`.`pricing`='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `ratings` DESC ";
              break;
              case 'toppaid':
                     $join .="select *,sum(".env('DB_PREFIX')."ratings.rating) as `ratings` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."ratings` ON `".env('DB_PREFIX')."ratings`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where $query `".env('DB_PREFIX')."course`.`pricing`!='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `".env('DB_PREFIX')."course`.`pricing` DESC ";
              break;
              case 'mostviewed':
                     $join .="select *,sum(".env('DB_PREFIX')."hits.course_id) as `hits` from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` ON `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image` ";
                     $join .="join `".env('DB_PREFIX')."hits` ON `".env('DB_PREFIX')."hits`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
                     $custom_query =" where $query `".env('DB_PREFIX')."course`.`pricing`!='0' AND `".env('DB_PREFIX')."course`.`deleted_at` is NULL AND `".env('DB_PREFIX')."course`.`privacy`='1' AND `".env('DB_PREFIX')."course`.`approved`='1' GROUP BY `".env('DB_PREFIX')."hits`.`course_id` ORDER BY `hits` DESC ";
              break;
              default:
                  $resultt='';
              break;

            }
            return $join.$custom_query;
    }

    /*
    * @process -- course list featured and latest 
    */
     public function courselist($uid = ''){

      $condition ='';
      $result = array();
      $image=array();
          $featured = \DB::table('course')
                      ->join('users', 'users.id', '=', 'course.user_id')
                      ->join('course_images','course_images.id','=','course.image')
                      ->join('featured', 'featured.course_id', '=', 'course.course_id')
                      ->select('course_images.*','course.*','users.*',\DB::raw('count('.env('DB_PREFIX').'featured.course_id) as featured'))
                      ->where('course.approved','=', '1')
                      ->where('course.privacy','=', '1')
                      ->orderBy('featured', 'desc')
                      ->groupBy('course.course_id')
                      ->limit(9)->get();
          $result['featured']=$this->support($featured,$uid);

          $mostviewed = \DB::table('course')
                        ->join('hits', 'hits.course_id', '=', 'course.course_id')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                        ->where('course.approved','=', '1')
                        ->where('course.privacy','=', '1')
                        ->groupBy('hits.course_id')
                        ->orderBy('hits','desc')
                        ->limit(9)->get();
               
          $result['mostviewed']=$this->support($mostviewed,$uid);

//\DB::enableQueryLog();
    // $latest = \DB::select('select * from `tb_course` inner join `tb_users` on `tb_users`.`id` = `tb_course`.`user_id` inner join `tb_course_images` on `tb_course_images`.`id` = `tb_course`.`image` where `tb_course`.`approved` = 1 and `tb_course`.`privacy` = 1 order by `tb_course`.`course_id` desc limit 9');
           $latest = \DB::table('course')
      ->join('users', 'users.id', '=', 'course.user_id')
      ->join('course_images','course_images.id','=','course.image')
      ->where('course.approved','=', '1')
      ->where('course.privacy','=', '1')
      ->orderBy('course.created_at', 'desc')
      ->limit(9)->get();
                       // dd(\DB::getQueryLog());
              // echo json_encode( $latest); exit;  
                                                            // echo "<pre>";print_r($latest);exit;

          $result['latest']=$this->support($latest,$uid);


         
 
          $url = url('');
          $current_url = str_replace('api-droid','', $url);
          $banner = \DB::table('banner')->select('banner_image')->where('banner_status',1)->get();
          foreach ($banner as $key => $val) {
            $image[]=array(
                'banner_image'=>$current_url.'/uploads/banner/'.$val->banner_image,
              );
          }
          
          $result['banner_image_url']=$image;

        return $result;
    }
   /* public function courselist($uid = ''){
      $condition ='';
      $result = array();
      if($uid){
          $main_query = " select * from `".env('DB_PREFIX')."course` join `".env('DB_PREFIX')."users` on `".env('DB_PREFIX')."users`.`id` = `".env('DB_PREFIX')."course`.`user_id` join `".env('DB_PREFIX')."course_images` on `".env('DB_PREFIX')."course_images`.`id` = `".env('DB_PREFIX')."course`.`image`";
            $support_query="join `".env('DB_PREFIX')."featured` on `".env('DB_PREFIX')."featured`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
          $condition1=" where `".env('DB_PREFIX')."course`.`approved`='1' and `".env('DB_PREFIX')."course`.`privacy`";
            $condition2 = "and ".env('DB_PREFIX')."featured.user_id = ".$uid." ";
            $list =" GROUP BY `".env('DB_PREFIX')."course`.`course_id` ORDER BY `".env('DB_PREFIX')."course`.`created_at` DESC limit 5";
            $final_query = $main_query.$support_query.$condition1.$condition2.$list;
            $featured = \DB::select(\DB::raw($final_query));
          $result['featured']=$this->support($featured,$uid);
            $support_query = "join `".env('DB_PREFIX')."hits` on `".env('DB_PREFIX')."hits`.`course_id` = `".env('DB_PREFIX')."course`.`course_id` ";
            $list1 =" GROUP BY `".env('DB_PREFIX')."hits`.`course_id` ORDER BY `".env('DB_PREFIX')."hits`.`hits_id` DESC limit 5";
            $final_query = $main_query.$support_query.$condition1.$list1;
            $mostviewed = \DB::select(\DB::raw($final_query));
          $result['mostviewed']=$this->support($mostviewed,$uid);
            $final_query = $main_query.$list;
            $latest = \DB::select(\DB::raw($final_query));
          $result['latest']=$this->support($latest,$uid);
      }else{
          $featured = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->join('featured', 'featured.course_id', '=', 'course.course_id')
                        ->where('course.approved','=', 1)
                        ->where('course.privacy','=', 1)
                        ->orderBy('course.created_at', 'desc')
                        ->groupBy('course.course_id')
                        ->limit(9)->get();
          $result['featured']=$this->support($featured);
          $mostviewed = \DB::table('course')
                        ->join('hits', 'hits.course_id', '=', 'course.course_id')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                        ->where('course.approved','=', 1)
                        ->where('course.privacy','=', 1)
                        ->groupBy('hits.course_id')
                        ->orderBy('hits','desc')
                        ->limit(9)->get();
                        // ->get();
          $result['mostviewed']=$this->support($mostviewed);
          $latest = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->where('course.approved','=', 1)
                        ->where('course.privacy','=', 1)
                        ->orderBy('course.created_at', 'desc')
                        ->limit(9)->get();
                        // ->get();
          $result['latest']=$this->support($latest);
      }
        return $result;
    }*/

    /*
    * @process --- support function
    * @param -- array of parameter
    */
    public function support($results,$user_id =''){
        $url = url('');
        $result1=array();
        $current_url = str_replace('api-droid','', $url);

        if(count($results)>0){
           foreach ($results as $key => $val) {
            $description = str_replace("\n", '', $val->description);
            $description = str_replace("\r", '', $description);
            $subscribe = ($user_id==$val->user_id)? 1 : $this->isSubscribecourse($val->course_id,$user_id);
             $result1[]=array(
                'c_user_id'=>$val->user_id,
                'course_id'=>$val->course_id,
                'course_name'=>$val->course_title,
                'price'=>$val->pricing,
                'author'=>$val->username,
                'course_description'=>strip_tags($description),
                'rating'=>$this->checkReview($val->course_id),
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'is_subscribed'=>$subscribe,
                'is_wishlist'=>$this->Iscoursewishlist($val->course_id,$user_id),
                'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,
                );
            }
        }
        return $result1;
    }
    /*
    * @process - course rating
    * @param - course id
    */
      public function checkReview($id,$u_id='0')
      {
        if($u_id){
          $review = \DB::table('ratings')->where('course_id', '=', \DB::raw($id))->where('user_id', '=', $u_id)->limit(1)->get();
          if(count($review) > 0 ){
            foreach ($review as $review)
            {
              $count = $review->rating;
            }
            return $count;
          }else{
           return '0'; 
         }
       }else{
        $review = \DB::table('ratings')->where('course_id', '=', $id)->get();
        $count = count($review);
        if($count > 0){
          $total = \DB::table('ratings')->where('course_id', '=', $id)->sum('rating');
          return $result = $total / $count; 
        }else{
          return '0';
        }
      }
    }
    /*
    * @process user course details
    */
    public function usercoursedetails($cid='',$uid='')
    {
      $result1 =array();
      $result =array();
      $total_student_enrolled = \DB::table('course_taken')
                              ->join('users','users.id','=','course_taken.user_id')
                              ->where('course_id','=',$cid)->count();

      $count = \DB::table('course_taken')->where('course_id','=',$cid)->where('user_id',$uid)->count();
     
      if($count==0){
        $ccount = \DB::table('course')->where('course.course_id',$cid)->where('course.approved','1')->whereNotNull('course.user_id')->get();

              if(count($ccount)>0){
                $result =  \DB::table('course')
                    ->join('users','users.id','=','course.user_id')
                    ->leftJoin('course_images','course_images.id','=','course.image')
                    ->leftJoin('course_videos','course_videos.id','=','course.video')
                    ->leftJoin('ratings','ratings.course_id','=','course.course_id')
                    ->select('course_videos.id','course.*','users.username','course_videos.video_title','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                    ->where('course.course_id',$cid)
                    ->whereNotNull('course.user_id')->get();
              }

      }
        $wishlist = \DB::table('favorite')->where('course_id','=',$cid)->where('user_id','=',$uid)->count();
        $url = url('');
        $current_url = str_replace('api-droid','', $url);
        if(!empty($result)){
           foreach ($result as $key => $val) {
                
            $desc = strip_tags($val->description);
            $req = '<h4>What are the requirements?</h4>'.$val->course_req;
            $goal = '<h4>What am I going to get from this course?</h4>'.$val->course_goal;
            $aui = '<h4>What is the target audience?</h4>'.$val->int_audience;
            $overview = $desc.$req.$goal.$aui;
            $val->ratings = empty($val->ratings)? 0:$val->ratings;
            $val->video_title = empty($val->video_title)?$val->video_title:$val->video_title.'.mp4';

             $result1[]=array(
                'course_name'=>$val->course_title,
                'price'=>$val->pricing,
                'author'=>$val->username,
                'rating'=>$this->checkReview($val->course_id),
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'total_ratings'=>$val->ratings,
                'total_student_enrolled'=>$total_student_enrolled,
                'is_add_to_wishlist'=>$wishlist,
                'video_url'=> (empty($val->video_title))?'' :$current_url.'/uploads/videos/'.$val->video_title,
                'course_overview'=>$overview,
                'req'=>$val->course_req,
                'goal'=>$val->course_goal,
                'audience'=>$val->int_audience,
                'video_image'=>$current_url.'/course/videoimage/'.$val->id,
                'all_reviews'=>$this->getreviews($cid),
                );
            }
        }
        return $result1;
    }

    // get total reviews
    public function getreviews($cid='')
    {
        return \DB::table('ratings')
             ->join('users', 'users.id', '=', 'ratings.user_id')
             ->select('ratings.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
             ->where('course_id', '=', $cid)->count();
      
    }

    /*
    * @process user course details
    */
    public function usercoursedetails1($cid='',$uid='')
    {
      $result1 =array();
      $result =array();
      $count = \DB::table('course_taken')->where('course_id','=',$cid)->where('user_id',$uid)->count();

     $total_student_enrolled = \DB::table('course_taken')
                              ->join('users','users.id','=','course_taken.user_id')
                              ->where('course_id','=',$cid)->count();
      if($count==0){
        $ccount = \DB::table('course')->where('course.course_id',$cid)->where('course.approved','1')->whereNotNull('course.user_id')->get();
              if(count($ccount)>0){
                $result =  \DB::table('course')
                    ->join('users','users.id','=','course.user_id')
                    ->leftJoin('course_images','course_images.id','=','course.image')
                    ->leftJoin('course_videos','course_videos.id','=','course.video')
                    ->leftJoin('ratings','ratings.course_id','=','course.course_id')
                    ->select('course_videos.id','course.*','users.username','course_videos.video_title','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                    ->where('course.course_id',$cid)
                    ->whereNotNull('course.user_id')->get();
              }
      }
        $wishlist = \DB::table('favorite')->where('course_id','=',$cid)->where('user_id','=',$uid)->count();
        $url = url('');
        $current_url = str_replace('api-droid','', $url);
        if(!empty($result)){
           foreach ($result as $key => $val) {
                
            $desc = strip_tags($val->description);
           $description = str_replace("/\n+/", '', $desc);
            // $description = preg_replace("/\n+/", "\n", $desc);
            //echo '<pre>';print_r($description);exit;
            $description = str_replace("\r", '', $description);
            // $req = '<h4>What are the requirements?</h4>'.$val->course_req;
            // $goal = '<h4>What am I going to get from this course?</h4>'.$val->course_goal;
            // $aui = '<h4>What is the target audience?</h4>'.$val->int_audience;
            // $overview = $desc.$req.$goal.$aui;
            $val->ratings = empty($val->ratings)? 0:$val->ratings;
            $val->video_title = empty($val->video_title)?$val->video_title:$val->video_title.'.mp4';
             $result1[]=array(
                'course_name'=>$val->course_title,
                'price'=>$val->pricing,
                'author'=>$val->username,
                'rating'=>$this->checkReview($val->course_id),
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'total_ratings'=>$val->ratings,
                'total_student_enrolled'=>$total_student_enrolled,
                'is_add_to_wishlist'=>$wishlist,
                'video_url'=> (empty($val->video_title))?'' :$current_url.'/uploads/videos/'.$val->video_title,
                'course_overview'=>$description,
                'req'=>$val->course_req,
                'goal'=>$val->course_goal,
                'audience'=>$val->int_audience,
                'video_image'=>$current_url.'/course/videoimage/'.$val->id,
                'all_reviews'=>$this->getreviews($cid),
                );
            }
        }
        return $result1;
    }

    /*
    * @process -- isavialcourse
    */
    public function isAvailcourse($cid)
    {
         return \DB::table('course')->where('course_id','=',$cid)->where('approved','1')->count();
    }

    /*
    * @process - course curriculum
    */
    public function getcurriculum($cid)
    {
       $result = array();
       $resultt =  \DB::table('curriculum_sections')
              ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
              ->select('curriculum_lectures_quiz.lecture_quiz_id','curriculum_lectures_quiz.media','curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
              ->where('curriculum_sections.course_id', '=', $cid)
              ->where("curriculum_lectures_quiz.publish",'1')
              ->orderBy('curriculum_sections.sort_order', 'asc')
              ->groupBy('curriculum_sections.section_id')->get(); 
      if(count($resultt)>0)
      {
        foreach($resultt as $key => $val)
        {
          $section_id = $val->section_id;
          $section = \DB::table('curriculum_lectures_quiz')->where('section_id',$section_id)->where("curriculum_lectures_quiz.publish",'1')->get();
            if(count($section)>0)
            {
              foreach($section as $key => $sec) {
                  $lecture = $this->getlectures($sec->lecture_quiz_id);
                  $lecture_dur = $this->getlecturesfiles($sec->lecture_quiz_id);
                    if($lecture_dur->media_type == '0')
                      $type ='mp4';
                    else if($lecture_dur->media_type=='1')
                      $type='mp3';
                    else if($lecture_dur->media_type=='2')
                      $type='pdf';
                    else
                      $type='txt';
                  $duration = empty($lecture_dur->duration)? '': $lecture_dur->duration;
                  
                  $pages = $duration;
                  $lecture3[]= array(

                    'section_name'=>$val->title,
                    'lecture_name'=>$lecture->title,
                    'lecture_id'=>$sec->lecture_quiz_id,
                    'lecture_des'=>strip_tags($lecture->description),
                    'type'=>$type,
                    'video_duration/no_of_pages'=>$pages,
                    );
              }
              $result[]=array(
                    'section_name'=>$val->title,
                    'lecture'=>$lecture3,
              );
              $lecture3 = '';
            }
        }
        return $result;
      }else{
        return $result;
      }
    }

    /*
    * @process -- course curriculum support function
    */
    public  function getlectures($sid='')
    {
      return \DB::table("curriculum_lectures_quiz")->select('title','description','contenttext')->where('lecture_quiz_id',$sid)->first();
    }
   public function getlecturesfiles($lid='')
   {

    $getmediatype = \DB::table("curriculum_lectures_quiz")->where("lecture_quiz_id",'=',$lid)->first();
     
     if(count($getmediatype)>0){
          $mediaid = $getmediatype->media;
          $lecture_quiz_id = $getmediatype->lecture_quiz_id;
          if(isset($getmediatype->media_type) && $getmediatype->media_type=='0'){
              return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_videos', 'course_videos.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->first();
          }elseif(isset($getmediatype->media_type) && $getmediatype->media_type=='3'){
              return \DB::table("curriculum_lectures_quiz")
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->first();

          }elseif(isset($getmediatype->media_type) && ($getmediatype->media_type=='2' || $getmediatype->media_type=='1')){
             return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_files', 'course_files.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->first();
          } else {
        return $getmediatype;
      }
     }
   }

   /*
   * @process -- get course reviews
   */
   public function getcoursereview($cid) 
   {
      $result = array();
      $resultt = \DB::table('ratings')
             ->join('users', 'users.id', '=', 'ratings.user_id')
             ->select('ratings.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
             ->where('course_id', '=', $cid)->orderBy('ratings.ratings_id', 'asc')->get();
      if(count($resultt)>0)
      {
        foreach ($resultt as $key => $val) {
            $result [] =array(
            'user_image'=>$this->customavatar($val->email,$val->user_id),
            'user_name'=>$val->first_name.' '.$val->last_name,
            'review_desc'=>$val->review_description,
            'rating'=>$val->rating,
            'review_date'=>$val->created_at,
          );
        }

       return $result;
      }
      else
      {
        return $result;
      } 
   }
   /*
   * @process get user avatar image 
   */
    public function customavatar($email='',$userid ='',$type='normal',$class='img-circle')
    {

      if($class == 'croppedImg')
      {
        $width = '100%';
        $height = '500px';
      }
      else
      {
        $width = '75';
        $height = '';
      }
      $avatar ="http://www.gravatar.com/avatar/".md5($email)."" ;
      $Q = \DB::table("users")->where("id",'=',$userid)->get();
      if(count($Q)>=1) 
      {
        $row = $Q[0];
      
        if($row->avatar !='' )  
        { 
          $getimage   = \DB::table("course_images")->where("id",'=',$row->avatar)->get();
          // echo '<pre>';print_r($getimage);exit;
          if(count($getimage)>0){
            
           $rowimg     = $getimage[0];
          
           $base = base_path();
           $path = str_replace('protected','', $base);
            $path = app_path();
            $url = url('');
            $current_url = str_replace('protected','', $url);
            $destination = $path.'uploads/users/';
            $size = $type == 'normal' ? '' : '_'.$type;
            
            $files    = $destination.$rowimg->id.$size.'.'.$rowimg->image_type;
            
            if(file_exists($files)){
              return $current_url.'/user/image/'.$rowimg->image_hash.'/medium';
            } else {
              
              return $avatar;
            }
          }else {
            return $avatar;
          }

        } else {
          return $avatar;
        }
      } 
    }

    /*
    * @process --- course discussion
    */

    public function getdiscussion($cid,$uid)
    {
       $result = array();
        $resultt = \DB::table('lectures_comments')
              ->join('users', 'users.id', '=', 'lectures_comments.user_id')
              ->select('lectures_comments.course_id','lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
              ->where('lectures_comments.course_id', '=', $cid)
              ->where('lectures_comments.lecture_id','<>',0)
              ->orderBy('lectures_comments.lecture_comment_id', 'desc')->get();
        if(count($resultt)>0)
        {
            foreach ($resultt as $key => $val) 
            {
              $result[]=array(
                'user_id'=>$val->user_id,
                'user_name'=>$val->first_name.' '.$val->last_name,
                'user_image'=>$this->customavatar($val->email,$val->user_id),
                'lecture_order'=>$this->getlecturess($cid,$val->lecture_id),
                'lecture_number'=>$val->lecture_id,
                'discussion_id'=>$val->lecture_comment_id,
                'discussion_title'=>$val->lecture_comment_title,
                'discussion_cmt'=>strip_tags($val->lecture_comment),
                'added_date'=>$val->created_at,
                'reply_count'=>$this->replycount($val->lecture_comment_id),
                );
            }
           return $result;
        }else{
          return $result;
        }
   }
   public function getlecturename($quiz_id){
     $name = \DB::table('curriculum_lectures_quiz')->select('title')->where('lecture_quiz_id',$quiz_id)->first();
     $lecture_name = $name->title;
     return $lecture_name;
   }
   /*
   *@process ---getlectures
   */
   public function getlecturess($cid,$lid =''){
    $result = array();
      $resultt=\DB::table('curriculum_sections')
                  ->join('curriculum_lectures_quiz','curriculum_lectures_quiz.section_id','=','curriculum_sections.section_id')
                  ->select('curriculum_lectures_quiz.*')
                  ->where('curriculum_sections.course_id',$cid)
                  ->where('curriculum_lectures_quiz.publish','1')
                  ->orderBy('curriculum_sections.sort_order','asc')
                  ->get();
        $lno = 1;
        $qno = 1;
      if(count($resultt)>0){
        foreach ($resultt as $key => $val) {
          if($val->type==1){
            $lecture_no = 'Quiz '.$qno++;
          }else{
            $lecture_no = 'Lecture '.$lno++;
          }
          if($lid!='' && $lid == $val->lecture_quiz_id){
            return $lecture_no;
          }else{
            $result[]=array(
              'lecture_id'=>$val->lecture_quiz_id,
              'lecture_name'=>$val->title,
              'lecture_order'=>$lecture_no,
            );
          }
        }
        return $result;
      }else{
        return $result;
      }
   }
   /*
   * @process Reply count
   */
   public function replycount($cmtid)
   {
      return \DB::table('lectures_comment_reply')->where('lecture_comment_id',$cmtid)->count();
   }
   /*
   * @process check user subscribe course or not
   */
   public function isSubscribecourse($cid,$uid)
   {

      $status = \DB::table('course')->where('user_id',$uid)->where('course_id',$cid)->count();
      if($status==1){
        return $status;
      }else{
        return \DB::table('course')
                  ->join('course_taken','course_taken.course_id','=','course.course_id')
                  ->where('course_taken.user_id','=',$uid)
                  ->where('course_taken.course_id','=',$cid)->count();
      }
    
   }
   /*
   * @process get about user/ instrucor details
   */
   public function getInstructor($cid){
      $resultt = \DB::table('course')
                  ->join('users','users.id','=','course.user_id')
                  ->select('users.email','users.id','users.biography','users.username','users.first_name','users.last_name')
                  ->where('course.course_id',$cid)->first();
      $result = array(
          'user_name'=>$resultt->first_name.' '.$resultt->last_name,
          'user_image'=>$this->customavatar($resultt->email,$resultt->id),
          'about'=>strip_tags($resultt->biography),
        );
      return $result;
   }

   /*
   * @process add discussion 
   */
   public function adddiscussion($cid = '',$uid ='',$desc ='',$decs_title='',$lid=''){
      $section = \DB::table('curriculum_lectures_quiz')->select('section_id')->where('lecture_quiz_id',$lid)->first();
      if(count($section)>0){
         $dataarray['user_id']                = $uid;
         $dataarray['course_id']              = $cid;
         $dataarray['section_id']             = $section->section_id;
         $dataarray['lecture_id']             = $lid;
         $dataarray['lecture_comment_title']  = $decs_title;
         $dataarray['lecture_comment']        = $desc;
         $dataarray['created_at']             = date("Y-m-d H:i:s");
         $dataarray['modified_at']            = date("Y-m-d H:i:s");
         $last_id = \DB::table('lectures_comments')->insertGetId($dataarray);
         $result[]=array(
            'discussion_id'=>$last_id,
            );
        return $result;
      }else{
       
        return '0';
      }
   }
   /*
   * @process -- update discussion
   */
   public function editdiscussion($cid = '',$uid ='',$desc ='',$decs_title='',$did =''){
      
       $count = \DB::table('lectures_comments')->where('lecture_comment_id',$did)->where('user_id',$uid)->where('course_id',$cid)->first();
       
       if(count($count)>0){
         $dataarray                           = array();
         $dataarray['lecture_comment_title']  = $decs_title;
         $dataarray['lecture_comment']        = $desc;
         $dataarray['modified_at']            = date("Y-m-d H:i:s");
        \DB::table('lectures_comments')->where('lecture_comment_id',$did)->update($dataarray);
        return '1';
       }else{
        return '0';
       }
   }
   /*
   * @process --- Delete Discussion
   */
   public function destorydiscussion($uid='',$did=''){
      $count = \DB::table('lectures_comments')->where('lecture_comment_id',$did)->where('user_id',$uid)->first();
      if(count($count)>0){
        \DB::table('lectures_comments')->where('lecture_comment_id', '=', $did)->where('user_id', '=', $uid)->delete();    
        return '1';
      }else{
        return '0';
      }
    
   }
   /*
   * @process --- wishlist API
   */
   public function wishlist($uid,$cid,$status){
      $count = \DB::table('favorite')->where('user_id',$uid)->where('course_id',$cid)->count();
       
       if($status == 1){
          if($count>0){
            return '2';
          }else{
            $data = array();
            $data['user_id'] = $uid;
            $data['course_id'] = $cid;
            \DB::table('favorite')->insertGetId($data);
            return '1';
          }
      }else if($status == 0){
          \DB::table('favorite')->where('user_id',$uid)->where('course_id',$cid)->delete();
          return '0';
      }
   }

   /*
   * @process -- userwishlist
   */
   public function userwishlist($uid ='',$page=''){
      $count = \DB::table('users')->where('id',$uid)->count();
      $result =array();
      $page = $page-1;
      $start = $page.'0';
      if($count){
        $users = \DB::table('favorite')
                      ->join('course','course.course_id','=','favorite.course_id')
                      ->join('course_images','course_images.id','=','course.image')
                      ->join('users','users.id','=','favorite.user_id')
                      ->where('favorite.user_id',$uid)
                      ->where('course.approved','=','1')
                      ->skip($start)->take(10)
                      ->get();
        $url = url('');
        $current_url = str_replace('api-droid','', $url);
        
        if(count($users)>0){
          foreach ($users as $key => $val) {
              $description = str_replace("\n", '', $val->description);
              $description = str_replace("\r", '', $description);
            $result[]=array(
                'course_id'=>$val->course_id,
                'course_name'=>$val->course_title,
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'course_description'=>strip_tags($description),
                'rating'=>$this->checkReview($val->course_id,$uid),
                'author'=>$val->username,
                'price'=>$val->pricing,
                'is_subscribed'=>$this->isSubscribecourse($val->course_id,$uid),
                'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,
              );
          }
        }
        return $result;
      }
   }

   /*
   * @process -- Add reply
   */
   public function addreply($cid='',$uid='',$reply='',$did=''){
      //$section = \DB::table('curriculum_lectures_quiz')->select('section_id')->where('lecture_quiz_id',$lid)->first();
       $discussion = \DB::table('lectures_comments')->select('lecture_id')->where('lecture_comment_id',$did)->first();
       if(count($discussion)>0){
         $data  = array();
         $data['lecture_comment_id']=$did;
         $data['user_id']=$uid;
         $data['reply_comment']=$reply;
         $data['lecture_id']=$discussion->lecture_id;
         $data['created_at']   = date("Y-m-d H:i:s");
         $data['modified_at']  = date("Y-m-d H:i:s");
         $last_id = \DB::table('lectures_comment_reply')->insertGetId($data);
         $result[]=array(
          'reply_id'=>$last_id,
          );
         return $result;
       }else{
        return '0';
       }
   }

   /*
   * @process -- Edit reply
   */
   public function updatereply($uid='',$re_id='',$reply1 =''){
    
    $reply = \DB::table('lectures_comment_reply')->where('reply_id',$re_id)->where('user_id',$uid)->get();
    if(count($reply)>0){
      $data = array();
      $data['reply_comment'] = $reply1;
      $data['modified_at']  = date("Y-m-d H:i:s");
      $time =date("Y-m-d H:i:s");
      \DB::table('lectures_comment_reply')->where('reply_id',$re_id)->update($data);
      return '1';
    }else{
      return '0';
    }
   }

   /*
   * @process --- Delete reply 
   */
   public function destoryreply($uid='',$rid=''){
      $count = \DB::table('lectures_comment_reply')->where('reply_id',$rid)->where('user_id',$uid)->first();
      if(count($count)>0){
        \DB::table('lectures_comment_reply')->where('reply_id', '=', $rid)->where('user_id', '=', $uid)->delete();    
        return '1';
      }else{
        return '0';
      }
    
   }

   /*
   *@process --- check discussion ID available
   */
   public function isAvaildiscussion($cid,$did){
    return \DB::table('lectures_comments')->where('course_id',$cid)->where('lecture_comment_id',$did)->count();
   }
   /*
   *@process -- view replies
   */
   public function viewreplies($did =''){
    $result = array();
      $resultt = \DB::table('lectures_comment_reply')
                  ->join('users','users.id','=','lectures_comment_reply.user_id')
                  ->where('lecture_comment_id',$did)->get();
      if(count($resultt)>0){
        foreach ($resultt as $key => $val) {
          $result[]=array(
            'user_name'=>$val->first_name.' '.$val->last_name,
            'user_image'=>$this->customavatar($val->email,$val->user_id),
            'user_id'=>$val->user_id,
            'reply_txt'=>$val->reply_comment,
            'reply_id'=>$val->reply_id,
            'reply_time'=>$val->created_at,
            );
        }
         return $result;
      }else{
        return $result;
      }
   }

   /*
   * @process --- Check User is avail or not
   */
   public function Ischeckuser($uid){
     return \DB::table('users')->where('id',$uid)->count();
   }
     /*
   * @process --- Check featured course
   */

   public function getfeaturedcourse($cid,$uid)
   {
    return \DB::table('featured')->where('user_id',$uid)->where('course_id',$cid)->count();
   }
   public function getupdatefeatures($dat)
   {
    $result = array();
    $cid = $dat['course_id'];
    $uid = $dat['user_id'];
    $status =$dat['status'];
     $feature = $this->getfeaturedcourse($cid,$uid);  
     if($feature == 1)
     {
     if($status == 1)
     {
       $result="Already The course is in featured course list";
     }else{
      \DB::table('featured')->where('user_id',$uid)->where('course_id',$cid)->delete();
      $result="Successfully Marked as unfeatured course";
     }
     }else{
      if($status == 1)
     {    
      \DB::table('featured')->insert(array('course_id'=>$cid,'user_id'=>$uid));
       $result="Successfully Marked as Featured course";
     }
     }    
    return $result;
   }
   /*
   * @process --- learning
   */
   public function learning($uid='',$type='',$page=''){
    $per='';
    $result =array();
    $page = $page-1;
    $start = $page.'0';
     $resultt =\DB::table('course_taken')
          ->join('course','course.course_id','=','course_taken.course_id')
          ->join('users','users.id','=','course_taken.user_id')
          ->join('course_images','course_images.id','=','course.image')
          ->select('course.description','course.slug','course.course_title','course_taken.course_id','course.user_id','course_images.image_hash')
          ->where('course_taken.user_id',$uid)
          ->where('course.approved',1)
          ->skip($start)->take(10)
          ->get();
    $url = url('');  
    $current_url = str_replace('api-droid','', $url);
      if(count($resultt)>0){
        foreach ($resultt as $key => $val) {
          $courselecturecompleted = $this->getCourseCompletedCount($val->course_id,$uid);
          $courselecturetotal = $this->getCourseLectureTotal($val->course_id);
          $feature = $this->getfeaturedcourse($val->course_id,$uid);  
             
          if(!empty($courselecturetotal))
          {
            $per = $courselecturecompleted / $courselecturetotal * 100 ;
          }          
          if($feature >= 1)
          { $featuredcourse = 1;    }else{  $featuredcourse = 0;  }
          $subscribe = ($uid==$val->user_id)? 1 : $this->isSubscribecourse($val->course_id,$uid);
          $author = $this->getusername($val->user_id);          
          if(!empty($author->username)){
              if(($type=='all')||($type=='All')){
                $name = $author->username;
                $description = str_replace("\n", '', $val->description);
                $description = str_replace("\r", '', $description);
                $result[]=array(
                'c_user_id'=>$val->user_id,
                'course_id'=>$val->course_id,
                'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                'course_name'=>$val->course_title,
                'course_description'=>strip_tags($description),
                'rating'=>$this->checkReview($val->course_id,$uid),
                'author'=>$name,
                'completed %'=>$per,
                'is_wishlist'=>$this->Iscoursewishlist($val->course_id,$uid),
                'is_featured' => $featuredcourse,
                'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,
                'is_subscribed'=>$subscribe,
                );
              }else if(($type=='completed')||($type=='Completed')){
                if($per==100){
                  $description = str_replace("\n", '', $val->description);
                  $description = str_replace("\r", '', $description);
                  $name = $author->username;
                  $result[]=array(
                  'course_id'=>$val->course_id,
                  'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
                  'course_name'=>$val->course_title,
                  'course_description'=>strip_tags($description),
                  'rating'=>$this->checkReview($val->course_id,$uid),
                  'author'=>$name,
                  'completed %'=>$per,
                  'is_wishlist'=>$this->Iscoursewishlist($val->course_id,$uid),
                  'is_featured' => $featuredcourse,
                  'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,
                  'is_subscribed'=>$subscribe,
                  );
                }
              }
          }
        }
        return $result;
      }else{
        return $result;
      }
   }

   /*
   *@process -- get username
   */
   public function getusername($uid){
    return \DB::table('users')->select('username')->where('id',$uid)->first();
   }
   /*
   *@process --- course completed count
   */
   public function getCourseCompletedCount($cid='',$uid='')
  {
    return \DB::table('course_progress')->where('course_id','=', $cid)->where('user_id','=', $uid)->where('status','=', '1')->count();
  }
  /*
  *@process --- lecture count
  */
  public function getCourseLectureTotal($cid='')
  {   
    $lecturescount = 0;
    $sections = \DB::table('curriculum_sections')->where('course_id', '=', $cid)->get();
    if(!empty($sections)){
      foreach($sections as $section){
        $lecturescount += \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $section->section_id)->where('publish', '1')->orderBy('sort_order', 'asc')->count();
      }
    }
    return $lecturescount;
  }
  /*
  *@process --- getannouncement
  */
  public function getannouncement($cid='',$uid='')
  {
    $result =array();
      $resultt =\DB::table('course_announcement')
            ->join('users','users.id','=','course_announcement.user_id')
            ->where('course_announcement.course_id',$cid)
            ->orderby('announcement_id','desc')
            ->get();
      $status = \DB::table('course')->where('course_id',$cid)->where('user_id',$uid)->count();
      if(count($resultt)>0){
          foreach ($resultt as $key => $val) {
            $result[]=array(
                'announcement_id'=>$val->announcement_id,
                'user_image'=>$this->customavatar($val->email,$val->user_id),
                'user_name'=>$val->first_name.' '.$val->last_name,
                'description'=>strip_tags($val->announcement),
                'data'=>$val->created_at,
                'is_owner'=>$status,
              );
          }
          return $result;
      }else{
        return $result;
      }
  }


  /*
  *@process --- course details
  */
  public function getcoursedetails($cid){
    return \DB::table('course')->select('course_id','slug')->where('course_id',$cid)->first();
  }
  /*
  *@process --- course curriculum for subscribed user
  */

  

  public function getusercurriculum($cid='',$uid=''){
     $result = array();
     
       $resultt =  \DB::table('curriculum_sections')
              ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
              ->select('curriculum_lectures_quiz.lecture_quiz_id','curriculum_lectures_quiz.media','curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
              ->where('curriculum_sections.course_id', '=', $cid)
              ->where("curriculum_lectures_quiz.publish",'1')->orderBy('curriculum_sections.sort_order', 'asc')->groupBy('curriculum_sections.section_id')->get(); 
      $url = url('');
      $current_url = str_replace('api-droid','', $url);
      if(count($resultt)>0)
      {
        foreach($resultt as $key => $val)
        {
          $section_id = $val->section_id;
          $section = \DB::table('curriculum_lectures_quiz')->where('section_id',$section_id)->where('publish','1')->get();
            if(count($section)>0)
            {
              foreach($section as $key => $sec) {
                  $course_details = $this->getcoursedetails($cid);
                  $slug = $course_details->slug;
                  $encrypt = $this->encryptID($sec->lecture_quiz_id);
                  
                  $lecture = $this->getlectures($sec->lecture_quiz_id);
                  $lecture_dur = $this->getlecturesfiles($sec->lecture_quiz_id);
                  $type  = empty($lecture_dur->file_type)? '' : $lecture_dur->file_type.':';
                  $duration = empty($lecture_dur->duration)? '': $lecture_dur->duration;
                  $pages = $type.$duration;

                  $file_url='';
                  if($lecture_dur->media_type==0 ){
                    $urll = isset($lecture_dur->video_title)? $lecture_dur->video_title.'.mp4' : '';
                    $file_url = str_replace('api-droid', '', \URL::to('uploads/videos/'.$urll));
                  }else if($lecture_dur->media_type==1 || $lecture_dur->media_type==2){
                    $urll = isset($lecture_dur->file_name)? $lecture_dur->file_name.'.'.$lecture_dur->file_extension : '';
                    $file_url = str_replace('api-droid', '', \URL::to('uploads/files/'.$urll));
                  }
                  if($lecture_dur->type==1){
                    $file_url ='';
                  }
                  //resources links

                  $jsondecode = json_decode($sec->resources,true);

                    $resources_array = array();
                    $arr = 0;
                    if(!empty($jsondecode))
                    {
                    foreach ($jsondecode as $resource) { 

                      $getfiles   = \DB::table("course_files")->where("course_files.id",'=',$resource)->get();
                      $resourceid = $this->encryptID($resource);
                      if(isset($getfiles['0']->file_type))
                      {
                        if($getfiles['0']->file_type=='link')
                        {
                          $resources_array[$arr]['file_name'] = $getfiles['0']->file_title;
                          $resources_array[$arr]['link'] = $getfiles['0']->file_name;
                          $resources_array[$arr]['is_link']='true';
                          //echo '<r><td><a href="'.$getfiles['0']->file_name.'" target="_blank"><i class="fa fa-file-o"></i> '.$getfiles['0']->file_title.'</a></td><td width="30"><a href="'.$getfiles['0']->file_name.'" target="_blank"><i class="fa fa-download"></i></a></td></tr>';
                        }else{

                          $resources_array[$arr]['file_name'] = $getfiles['0']->file_title;
                          $resources_array[$arr]['link'] = str_replace('api-droid', '', \URL::to('download-resourcemob/'.$resourceid));
                          //echo '<tr><td><a href="'.\URL::to('download-resource/'.$resourceid).'"><i class="fa fa-file-o"></i> '.$getfiles['0']->file_title.'</a></td><td width="30"><a href="'.\URL::to('download-resource/'.$resourceid).'"><i class="fa fa-download"></i></a></td></tr>';
                          $resources_array[$arr]['is_link']='false';
                        }
                      }
                      $arr++;
                    }
                  }

                  if($lecture_dur->type==1)
                      $type ='Quiz';
                    else
                      $type = !empty($lecture_dur->video_type) ? 'video' : (!empty($lecture_dur->file_type)?$lecture_dur->file_type : 'text');
                  $lecture3[]= array(
                    'section_id'=>$section_id,
                    'section_name'=>$val->title,
                    'lecture_name'=>$lecture->title,
                    'lecture_id'=>$sec->lecture_quiz_id,
                    'lecture_des'=>strip_tags($lecture->description),
                    'lecture_content'=>strip_tags($lecture->contenttext),
                    'lecture_order'=>$this->getlecturess($cid,$sec->lecture_quiz_id),
                    'type'=>$type,
                    'video_duration/no_of_pages'=>$duration,
                    'url'=>$file_url,
                    'status'=>$this->Coursestatus($sec->lecture_quiz_id,$uid),
                    'resources_array'=>$resources_array,
                    );
              }
              
              $result[]=array(
                    'section_name'=>$val->title,
                    'lecture'=>$lecture3,
                    
              );
              $lecture3 = '';
            }
        }
       
        return $result;
      }else{
        return $result;
      }
  }

  public function getcoursetaken($id='', $userid='')
  {
    return \DB::table('course_taken')->where('course_id','=', $id)->where('user_id',$userid)->get();
  }

  /*
  *@process course completed function
  */
    public function checkCompletion($cid='',$uid=''){
       
       $takencourse  = $this->getcoursetaken($cid,$uid);
        if(count($takencourse) < 1){
            return false;
        }else{
           $courselecturetotal= $this->getCourseLectureTotal($cid);
           $courselecturecompleted = $this->getCourseCompletedCount($cid,$uid);
           if($courselecturetotal != $courselecturecompleted){
             return false;
           }
        }
        return true;
    }

  /*
  *@process course status 
  */
  public function Coursestatus($lid='',$uid=''){
      $status = \DB::table('course_progress')->select('status')->where('lecture_id',$lid)->where('user_id',$uid)->first();
      
      if(isset($status->status))
      {
        if($status->status==1)
        return 'Restart';
      else if($status->status==0)
        return 'Resume';
      }
      else
        return 'Start';
  }

  /*
  *@process --- lecture learn page
  */
    public static function encryptID($id,$decript=false,$pass='',$separator='-', & $data=array()) {
      $pass = $pass?$pass:\Config::get('app.key');
      $pass2 = \Config::get('app.url');;
      $bignum = 200000000;
      $multi1 = 500;
      $multi2 = 50;
      $saltnum = 10000000;
      if($decript==false){
        $strA = self::alphaid(($bignum+($id*$multi1)),0,0,$pass);
        $strB = self::alphaid(($saltnum+($id*$multi2)),0,0,$pass2);
        $out = $strA.$separator.$strB;
      } else {
        $pid = explode($separator,$id);
        
        $pid_index = (isset($pid[1])) ? 1 : 0;
          //trace($pid);
        $idA = (self::alphaid($pid[0],1,0,$pass)-$bignum)/$multi1;
        $idB = (self::alphaid($pid[$pid_index],1,0,$pass2)-$saltnum)/$multi2;
        $data['id A'] = $idA;
        $data['id B'] = $idB;
        $out = ($idA==$idB)?$idA:false;
      }

      return $out;
    }

  public static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
  {
    $index = "abcdefghijkmnpqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
    if ($passKey !== null) {
        // Although this function's purpose is to just make the
        // ID short - and not so much secure,
        // with this patch by Simon Franz (http://blog.snaky.org/)
        // you can optionally supply a password to make it harder
        // to calculate the corresponding numeric ID

      for ($n = 0; $n<strlen($index); $n++) {
        $i[] = substr( $index,$n ,1);
      }

      $passhash = hash('sha256',$passKey);
      $passhash = (strlen($passhash) < strlen($index))
      ? hash('sha512',$passKey)
      : $passhash;

      for ($n=0; $n < strlen($index); $n++) {
        $p[] =    substr($passhash, $n ,1);
      }

      array_multisort($p,    SORT_DESC, $i);
      $index = implode($i);
    }

    $base    = strlen($index);

    if ($to_num) {
        // Digital number    <<--    alphabet letter code
      $in    = strrev($in);
      $out = 0;
      $len = strlen($in) - 1;
      for ($t = 0; $t <= $len; $t++) {
        $bcpow = bcpow($base, $len - $t);
        $out     = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
      }

      if (is_numeric($pad_up)) {
        $pad_up--;
        if ($pad_up > 0) {
          $out -= pow($base, $pad_up);
        }
      }
      $out = sprintf('%F', $out);
      $out = substr($out, 0, strpos($out, '.'));
    } else {
        // Digital number    -->>    alphabet letter code
      if (is_numeric($pad_up)) {
        $pad_up--;
        if ($pad_up > 0) {
          $in += pow($base, $pad_up);
        }
      }

      $out = "";
      for ($t = floor(log($in, $base)); $t >= 0; $t--) {
        $bcp = bcpow($base, $t);
        $a     = floor($in / $bcp) % $base;
        $out = $out . substr($index, $a, 1);
        $in    = $in - ($a * $bcp);
      }
        $out = strrev($out); // reverse
    }

    return $out;
}
  /*
  *@process --- Check Facebook user
  */
  public function Ischeckfbuser($email='',$type='',$device_id=''){
       $result = array();
        $users = \DB::table('users')->where('email',$email)->where('social_type','=',$type)->first();
        if(count($users)>0){
          $result[] =array(
            'user_id'=>$users->id,
            'user_name'=>$users->first_name.' '.$users->last_name,
            'email'=>$users->email,
            'user_image'=>$this->customavatar($users->email,$users->id),
          );
        }
        
        
    return $result;
  }
  /*
  *@process --- Check user course wishlsit
  */
  public function Iscoursewishlist($cid='',$uid=''){
    $count = \DB::table('favorite')->where('user_id',$uid)->where('course_id',$cid)->count();
    if($count>0)
      return '1';
    else 
      return '0';
  }
  /*
  *@process -- get Profile Info
  */
  public function getProfileInfo($uid=''){
    $result =array();
     $resultt = \DB::table('users')
                  ->select('id','first_name','last_name','username','email','avatar','active')
                  ->where('id', '=', $uid)->first();
     
     if(count($resultt)>0){
          $status = 0;
          if($resultt->active=='1'){
            $status = 1;
          }
          $result=array(
              'id'=>$resultt->id,
              'first_name'=>$resultt->first_name,
              'last_name'=>$resultt->last_name,
              'username'=>$resultt->username,
              'email'=>$resultt->email,
              'user_image'=>$this->customavatar($resultt->email,$resultt->id),                
              'notification_settings'=>$this->notifications($uid),
              'active_status' => $status
            );
          return $result;
      }else{
        return $result;
      }

  }
  public function notifications($uid=''){
      $result =array();
      $resultt = \DB::table('notifications_settings')->where('user_id',$uid)->first();
      if(count($resultt)>0)
      {
        $result=array(
        'announcement'=>$resultt->notif_announcement,
        'spl_promotion'=>$resultt->notif_special_promotion,
        'for_all'=>$resultt->notif_for_all,
        );
      }else{
        $result=array(
        'announcement'=>0,
        'spl_promotion'=>0,
        'for_all'=>0,
        );
      }      
      return $result;
  }
  /*
  *@process --- update profile info
  */
  public function updateProfileInfo($uid='',$fi='',$la='',$uname='',$an='',$pro='')
  {
     $exist_info = \DB::table('users')->where('id',$uid)->first();
     
     $data = array();
     $data['first_name']=($fi!='')? $fi : $exist_info->first_name;
     $data['last_name']= ($la!='')? $la : $exist_info->last_name;
     $data['username'] = ($uname!='')?$uname : $exist_info->username;
     $data['updated_at']  = date("Y-m-d H:i:s");
     $id = \DB::table('users')->where('id',$uid)->update($data);
     
     $dataa=array();
     $count = \DB::table('notifications_settings')->where('user_id',$uid)->first();
     if(count($count)>0){
       $dataa['notif_announcement']=($an!='')? $an : $count->notif_announcement;
       $dataa['notif_special_promotion']= ($pro!='')? $pro : $count->notif_special_promotion;
       $data['updated_at']  = date("Y-m-d H:i:s");
       \DB::table('notifications_settings')->where('user_id',$uid)->update($dataa);
     }else{
       $dataa['user_id']=$uid;
       $dataa['notif_announcement']=$an;
       $dataa['notif_special_promotion']=$pro;
       $data['created_at']  = date("Y-m-d H:i:s");
       \DB::table('notifications_settings')->insertGetId($dataa);
     }
     return '1';

  }
  /*
  *@process --- Account Delete
  */
  public function userData($table,$column,$uid){
        $data_count = \DB::table($table)->where($column,$uid)->count();
        if($data_count>0){
            return  \DB::table($table)->where($column,$uid)->delete();
         }
    }

  /*
  *@process --- Lecture detalis status
  */
  public function lecturedetails($uid='',$cid='',$lid='',$status=''){
    $count = \DB::table('course_progress')->where('user_id',$uid)->where('lecture_id',$lid)->count();
    if($count>0){
      $data = array();
      $data['status']=$status;
      $data['modified_at']=date("Y-m-d H:i:s");
      \DB::table('course_progress')->where('user_id',$uid)->where('lecture_id',$lid)->where('course_id',$cid)->update($data);
      return '1';
    }else{
      $data['user_id']=$uid;
      $data['course_id']=$cid;
      $data['lecture_id']=$lid;
      $data['status']=$status;
      $data['created_at']=date("Y-m-d H:i:s");
      $data['modified_at']=date("Y-m-d H:i:s");
      \DB::table('course_progress')->insertGetId($data);
      return '1';
    }
  }
  /*
  *@process -- search course
  */
  public function searchcourse($uid='',$search='',$page=''){
    $result =array();
    $page = $page-1;
    $start = $page.'0';
    $resultt = \DB::table('course')
                  ->leftJoin('users', 'users.id', '=', 'course.user_id')
                  ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                  ->join('course_images','course_images.id','=','course.image')
                  ->select('course.*','course_images.image_hash','users.username')
                  ->where('course.privacy','=', '1')
                  ->where('course.deleted_at', '=', NULL)
                  ->where('course.approved','=','1')
                  ->where('course.course_title', 'LIKE', '%' . $search . '%')
                  ->orWhere('course.keywords', 'LIKE', '%' . $search . '%')
                  ->where('course.approved','=','1')
                  ->orWhere('course.subtitle', '=', $search)
                  ->where('course.approved','=','1')
                  ->orderBy('course.created_at', 'desc')
                  ->orWhere('course.description', 'LIKE', '%' . $search . '%')
                  ->where('course.privacy','=', '1')
                  ->where('course.approved','=','1')
                  ->skip($start)->take(10)
                  ->get();

    $count = \DB::table('course')
                  ->leftJoin('users', 'users.id', '=', 'course.user_id')
                  ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                  ->join('course_images','course_images.id','=','course.image')
                  ->select('course.*','course_images.image_hash','users.username')
                  ->where('course.privacy','=', '1')
                  ->where('course.deleted_at', '=', NULL)
                  ->where('course.approved','=','1')
                  ->where('course.course_title', 'LIKE', '%' . $search . '%')
                  ->orWhere('course.keywords', 'LIKE', '%' . $search . '%')
                  ->where('course.approved','=','1')
                  ->orWhere('course.subtitle', '=', $search)
                  ->where('course.approved','=','1')
                  ->orderBy('course.created_at', 'desc')
                  ->orWhere('course.description', 'LIKE', '%' . $search . '%')
                  ->where('course.privacy','=', '1')
                  ->where('course.approved','=','1')
                  ->get();

    $url = url('');
    $current_url = str_replace('api-droid','', $url);
    $total_count = count($count);
        if(count($resultt)>0){
          foreach ($resultt as $key => $val) {
            $description = str_replace("\n", '', $val->description);
            $description = str_replace("\r", '', $description);
            $subscribe = ($uid==$val->user_id)? 1 :$this->isSubscribecourse($val->course_id,$uid);
            $result[]=array(
              'c_user_id'=>$val->user_id,
              'course_id'=>$val->course_id,
              'course_name'=>$val->course_title,
              'course_image'=>$current_url.'/course/image/'.$val->image_hash.'/_medium',
              'course_description'=>strip_tags($description),
              'price'=>$val->pricing,
              'author'=>$val->username,
              'rating'=>$this->checkReview($val->course_id),
              'is_subscribed'=>$subscribe,
              'is_wishlist'=>$this->Iscoursewishlist($val->course_id,$uid),
              'share_url'=>$current_url.'/courseview/'.$val->course_id.'/'.$val->slug,             
              );
          }
        }

        return array('status'=>true,'total_count'=>$total_count, 'result'=>$result);
        //return $result;
  }
  /*
  *@process -- search discussion
  */
  public function searchdiscussion($uid='',$cid='',$search=''){
      $result =array();
      $resultt = \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'lectures_comments.lecture_id')
                ->select('curriculum_lectures_quiz.title','lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'curriculum_lectures_quiz.sort_order')
                ->where('lectures_comments.course_id', '=', $cid)
                ->where(function($query) use ($search) {
                      $query->where('lectures_comments.lecture_comment_title', 'like', '%'.$search.'%')
                            ->orWhere('lectures_comments.lecture_comment', 'like', '%'.$search.'%');
                })
                ->get();
          if(count($resultt)>0){
            foreach ($resultt as $key => $val) {
                $result[]=array(
                'user_id'=>$val->user_id,
                'user_name'=>$val->first_name.' '.$val->last_name,
                'user_image'=>$this->customavatar($val->email,$val->user_id),
                'lecture_name'=>$val->title,
                'lecture_id'=>$val->lecture_id,
                'lecture_order'=>$this->getlecturess($cid,$val->lecture_id),
                'discussion_id'=>$val->lecture_comment_id,
                'discussion_title'=>strip_tags($val->lecture_comment_title),
                'discussion_cmt'=>strip_tags($val->lecture_comment),
                'added_date'=>$val->created_at,
                'reply_count'=>$this->replycount($val->lecture_comment_id),
                );
            }
          }
          return $result;

  }

  /*
  * @process -- Add rating
  */
  public function Addrating($uid='',$cid='',$txt='',$rating=''){
      $data =array();
      $count = \DB::table('ratings')->where('user_id',$uid)->where('course_id',$cid)->count();
      if($count>0){
        $data['rating']               = $rating;
            if(isset($txt)  && $txt!=''){
              $data['review_description']   = $txt;
           }
           $data['updated_at']           = date("Y-m-d H:i:s");
           \DB::table('ratings')->where('course_id',$cid)->where('user_id',$uid)->update($data);
           return '1';
      }else{
         $data['course_id']            = $cid;
         $data['rating']               = $rating;
         $data['user_id']              = $uid;
         if($txt!=''){
            $data['review_description']   = $txt;
         }
         $data['created_at']           = date("Y-m-d H:i:s");
         $data['updated_at']           = date("Y-m-d H:i:s");
         return \DB::table('ratings')->insertGetId($data);
      }

  }
  /*
  *@process -- get rating and reviews
  */
  public function getrating($uid='',$cid=''){
       $result = array();
       $resultt = \DB::table('ratings')->where('user_id',$uid)->where('course_id',$cid)->first();
       if(count($resultt)>0){
       
            $result = array(
            'review_description'=>$resultt->review_description,
            'rating'=>$resultt->rating,
            );
         
        }
        
       return $result;
  }
  /*
  *@process --- report abuse
  */
  public function reportabuse($uid='',$cid='',$type='',$details=''){
      $data =array();
      $data['course_id']=$cid;
      $data['user_id']=$uid;
      $data['entry_by']=$uid;
      $data['type']=$type;
      $data['details']=$details;
      $data['created_at']  = date("Y-m-d H:i:s");
      $data['updated_at']  = date("Y-m-d H:i:s");
      \DB::table('report_abuse')->insertGetId($data);
      return '1';
  }
  
  /*
  *@process -- Studentsenrolled
  */
  public function Studentsenrolled($cid){
     $result=array();
      $student = \DB::table('course_taken')
      ->join('users', 'course_taken.user_id', '=','users.id')
      ->where('course_taken.course_id','=', $cid)->where('users.active','=',1)->orderBy('taken_id')->get();
      if(count($student)>0){
        foreach ($student as $key => $val) {
          $result[]=array(
              'student_id'=>$val->user_id,
              'student_imag'=>$this->customavatar($val->email,$val->user_id),
              'student_name'=>$val->first_name.' '.$val->last_name,
            );
        }
      }
      return $result;
  }

  /*
  *@process --- getquiz
  */
  public function getquiz($uid='',$cid='',$lid=''){
     $result =array();
     $resultt = \DB::table('curriculum_sections')
            ->join('curriculum_lectures_quiz','curriculum_lectures_quiz.section_id','=','curriculum_sections.section_id')
            ->join('curriculum_quiz_questions','curriculum_quiz_questions.quiz_id','=','curriculum_lectures_quiz.lecture_quiz_id')
            ->where('curriculum_lectures_quiz.lecture_quiz_id',$lid)
            ->where('curriculum_sections.course_id',$cid)->get();

    if(count($resultt)>0){
      foreach ($resultt as $key => $val){
      $ans = json_decode($val->options);
      $i=1;
          foreach ($ans as $key => $value) {
              $answers[] = array(
                  'answer'=>$value->answer,
                );
            $i++;
          }
       $result[]=array(
          'quiz_number'=>$val->lecture_quiz_id,
          'question'=>strip_tags($val->question),
          'question_id'=>$val->quiz_question_id,
          'question_type'=>($val->question_type==1)?'True/False':'Multiple Choice',
          'answer'=>$answers,
          'correct_answer'=>$val->correct_option,
        );
       $answers='';
     }
    }
    $curriculum['questions']=$result;
    return $curriculum;
  }
  /*
  *@process --- change email
  */
  public function changeemail($uid='',$email=''){
    $data =array();
    $data['email'] = $email;
    $data['updated_at'] = date("Y-m-d H:i:s");
    \DB::table('users')->where('id',$uid)->update($data);
    return '1';
  }

  /*
  *@process --- quizresult
  */
  public function quizresult($uid='',$cid='',$qid='',$aid=''){
      $correct=0;
      $incorrect=0;
      $question_id = explode(',',$qid);
      $answer_id = explode(',', $aid);
      $questions = \DB::table('curriculum_quiz_questions')
                    ->whereIn('quiz_question_id', $question_id)
                    ->get();
                    $i=0;
      foreach ($questions  as $key => $val) {
        if($val->correct_option == $answer_id[$i])
        {
          $correct = $correct +1;
          $status =1;
        }
        else{
          $incorrect = $incorrect+1;
          $status =0;
        }
        
          $count = \DB::table('curriculum_quiz_results')->where('quiz_id',$val->quiz_id)->where('quiz_question_id',$val->quiz_question_id)->where('user_id',$uid)->count();
          
          if($count>0){
            $data['user_option'] =$answer_id[$i];
            $data['status'] = $status;
            $now_date = date("Y-m-d H:i:s");
            $data['updatedOn'] = $now_date;
            \DB::table('curriculum_quiz_results')->where('quiz_id',$val->quiz_id)->where('quiz_question_id',$val->quiz_question_id)->where('user_id',$uid)->update($data);

          }else{
              $data['quiz_id'] = $val->quiz_id;
              $data['quiz_question_id'] = $val->quiz_question_id;
              $data['user_id'] = $uid;
              $data['user_option'] =$answer_id[$i];
              $data['correct_option'] = $val->correct_option;
              $data['status'] = $status;
              $data['complete'] = '1';
              $now_date = date("Y-m-d H:i:s");
              $data['createdOn'] = $now_date;
              $data['updatedOn'] = $now_date;
              \DB::table('curriculum_quiz_results')->insertGetId($data);
          }
        $i++;
      }

      $score = round( (100 * $correct) / count($questions) );
      $result=array(
          'correct'=>$correct,
          'incorrect'=> $incorrect,
          'Average'=>$score,
          'skipped'=>'0',
        );
      return $result;
  }

  // Delete Review
  public function reviewdestory($uid='',$cid=''){
      return \DB::table('ratings')->where('course_id',$cid)->where('user_id',$uid)->delete();
  }
  // trending course
  public function trending($uid=''){

      $trending = \DB::table('course')
                        ->join('hits', 'hits.course_id', '=', 'course.course_id')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                        ->where('course.approved','=', '1')
                        ->where('course.privacy','=', '1')
                        ->groupBy('hits.course_id')
                        ->orderBy('hits','desc')
                        ->limit(5)->get();
      
      $result['trending']=$this->support($trending,$uid);

      $latest = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->join('ratings','ratings.course_id','=','course.course_id')
                        ->select('users.username','course_images.*','course.*',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                        ->where('course.approved','=', '1')
                        ->where('course.privacy','=', '1')
                        ->groupBy('ratings.course_id')
                        ->orderBy('ratings', 'desc')
                        ->limit(5)->get();
      $result['staff_picks']=$this->support($latest,$uid);

      $news = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                              ->leftJoin('course_images','course_images.id','=','course.image')
                              ->select('course.*','users.username','course_images.image_hash')
                              ->where('course.approved', '=', '1')
                              ->where('course.privacy', '=', '1')
                              ->orderBy('course.created_at', 'desc')
                              ->limit(5)
                              ->get();
      $result['news']=$this->support($news,$uid);
      $digital = \DB::table('course')
                    ->leftJoin('course_taken', 'course.course_id', '=', 'course_taken.course_id')
                    ->leftJoin('users', 'users.id', '=', 'course_taken.user_id')
                    ->leftJoin('course_images','course_images.id','=','course.image')
                    ->select('course_images.image_hash','users.username','users.id','course.*',\DB::raw('count('.env('DB_PREFIX').'course_taken.course_id) as course_taken'))
                    ->where('approved','=','1')
                    ->where('course.privacy','=', '1')
                    ->groupBy('course.course_id')
                    ->orderBy('course_taken','desc')
                    ->limit(5)->get();
      $result['digital']=$this->support($digital,$uid);
    return $result;
  }

  public function viewall($type='',$uid='',$page=''){
    $result =array();
    $page = $page-1;
    $start = $page.'0';
      switch($type){
        case 'trending':
        $trending = \DB::table('course')
                    ->join('hits', 'hits.course_id', '=', 'course.course_id')
                    ->join('users', 'users.id', '=', 'course.user_id')
                    ->join('course_images','course_images.id','=','course.image')
                    ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                    ->where('course.approved','=', '1')
                    ->where('course.privacy','=', '1')
                    ->groupBy('hits.course_id')
                    ->orderBy('hits','desc')
                    ->skip($start)->take(10)
                    ->get();
        $result = $this->support($trending,$uid);
        break;
        case 'staff_picks':
          $staff = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->where('course.approved','=', '1')
                        ->where('course.privacy','=', '1')
                        ->orderBy('course.created_at', 'desc')
                        ->skip($start)->take(10)
                        ->get();
          $result = $this->support($staff,$uid);
        break;
        case 'note_worthy':
          $new = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                              ->join('course_images','course_images.id','=','course.image')
                              ->join('ratings','ratings.course_id','=','course.course_id')
                              ->select('course.*','users.username','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                              ->where('course.approved', '=', '1')
                              ->where('course.privacy', '=', '1')
                              ->groupBy('ratings.course_id')
                              ->orderBy('ratings','DESC')->skip($start)->take(10)
                              ->get();
        $result=$this->support($new,$uid);
        break;
        case 'digital_marketing':
          $digital = \DB::table('course')
                    ->leftJoin('course_taken', 'course.course_id', '=', 'course_taken.course_id')
                    ->join('users', 'users.id', '=', 'course_taken.user_id')
                    ->join('course_images','course_images.id','=','course.image')
                    ->select('course_images.image_hash','users.username','users.id','course.*',\DB::raw('count('.env('DB_PREFIX').'course_taken.course_id) as course_taken'))
                    ->where('approved','=','1')
                    ->where('course.privacy','=', '1')
                    ->groupBy('course.course_id')
                    ->orderBy('course_taken', 'desc')
                    ->skip($start)->take(10)
                    ->get();

          $result =$this->support($digital,$uid);
        break;
      }
      return $result;
  }

  //view all count
  public function viewall_count($type=''){
    $count = array();
    $result =array();
      switch($type){
        case 'trending':
         $count = \DB::table('course')
                    ->join('hits', 'hits.course_id', '=', 'course.course_id')
                    ->join('users', 'users.id', '=', 'course.user_id')
                    ->join('course_images','course_images.id','=','course.image')
                    ->select('course.*','users.username','course_images.image_hash',\DB::raw('count('.env('DB_PREFIX').'hits.course_id) as hits'))
                    ->where('course.approved','=', '1')
                    ->where('course.privacy','=', '1')
                    ->groupBy('hits.course_id')
                    ->orderBy('hits','desc')
                    ->get();
        break;
        case 'staff_picks':
          $count = \DB::table('course')
                        ->join('users', 'users.id', '=', 'course.user_id')
                        ->join('course_images','course_images.id','=','course.image')
                        ->where('course.approved','=', '1')
                        ->where('course.privacy','=', '1')
                        ->orderBy('course.created_at', 'desc')
                        ->get();
          
        break;
        case 'note_worthy':
          $count = \DB::table('course')->join('users', 'users.id', '=', 'course.user_id')
                              ->join('course_images','course_images.id','=','course.image')
                              ->join('ratings','ratings.course_id','=','course.course_id')
                              ->select('course.*','users.username','course_images.image_hash',\DB::raw('sum('.env('DB_PREFIX').'ratings.rating) as ratings'))
                              ->where('course.approved', '=', '1')
                              ->where('course.privacy', '=', '1')
                              ->groupBy('ratings.course_id')
                              ->orderBy('ratings','DESC')
                              ->get();
        break;
        case 'digital_marketing':
          $count = \DB::table('course')
                    ->leftJoin('course_taken', 'course.course_id', '=', 'course_taken.course_id')
                    ->join('users', 'users.id', '=', 'course_taken.user_id')
                    ->join('course_images','course_images.id','=','course.image')
                    ->select('course_images.image_hash','users.username','users.id','course.*',\DB::raw('count('.env('DB_PREFIX').'course_taken.course_id) as course_taken'))
                    ->where('approved','=','1')
                    ->where('course.privacy','=', '1')
                    ->groupBy('course.course_id')
                    ->orderBy('course_taken', 'desc')
                    ->get();
        break;
      }
        $cnt = count($count);
      return $cnt;
  }
  // buy course
  public function buycourse($cid='',$uid=''){
    $data =array();
    $data['user_id'] = $uid;
    $data['course_id'] = $cid;
    $data['created_at']= date("Y-m-d H:i:s");
    $data['updated_at']= date("Y-m-d H:i:s");
    $id = \DB::table('course_taken')->insertGetId($data);
    return $id;
  }
  //  check user email
  public function checkuseremail($tid='',$type=''){
    return \DB::table('users')->select('email')->where('social_id',$tid)->where('social_type',$type)->first();
  }

  public function coursepercentage($cid='',$uid=''){
      $per = '';
      $courselecturecompleted = $this->getCourseCompletedCount($cid,$uid);
      $courselecturetotal = $this->getCourseLectureTotal($cid);
      if(!empty($courselecturetotal))
      {
        $per = $courselecturecompleted / $courselecturetotal * 100 ;
      }
      return $per;
  }

  // Check recently view course
  public function checkrecently($cid='',$did=''){

    return \DB::table('hits')->where('course_id',$cid)->where('device_id',$did)->count();
    
  }
  public function updaterecntview($cid='',$did=''){
    $data =array();
    $data['course_id']=$cid;
    $data['device_id']=$did;
    $data['date']= date("Y-m-d");
    $id = \DB::table('hits')->insertGetId($data);
    return $id;
  }

  // recently view course
   public function getviewedcoures($device_id='',$uid='',$cid='')
   {
    $recent = \DB::table('hits')
           ->join('course', 'course.course_id', '=', 'hits.course_id')
           ->join('users', 'users.id', '=', 'course.user_id')
           ->join('course_images','course_images.id','=','course.image')
           ->where('course.approved','=', '1')->where('course.privacy','=', '1')
           ->where('hits.device_id', '=' , $device_id)->orderBy('hits.hits_id', 'desc')->limit(3)->get();
      
      $recently_view = $this->support($recent,$uid);
      return $recently_view;
   }

   // save notes
  public function insertlecturenotes($uid='',$lid='', $sid='', $notes='')
  {
    
      $data                       = array();
      $data['user_id']            = $uid;
      $data['lecture_id']         = $lid;
      $data['section_id']         = $sid;
      $data['notes']              = $notes;
      $data['created_at']         = date("Y-m-d H:i:s");
      $data['modified_at']        = date("Y-m-d H:i:s");
      $id = \DB::table('lectures_notes')->insertGetId($data);
      return $id;
  }

  // get lecture notes
  public function getnotes($uid='',$lid='')
  {
    $note =array();
    $notes = \DB::table('lectures_notes')->where('lecture_id',$lid)->where('user_id',$uid)->orderby('note_id','asc')->get();
    foreach ($notes as $key => $val) {
      $note[]=array(
        'note_id'=>$val->note_id,
        'notes'=>$val->notes,
        );
    }
    return $note;
  }

  //remove notes
  public function removenotes($uid='',$nid=''){
     return \DB::table('lectures_notes')->where('note_id',$nid)->where('user_id',$uid)->delete();
  }

  // Add Annoucements
  public function announcement($uid='',$cid='',$announcement=''){
    $data = array();
    $data['user_id']=$uid;
    $data['course_id']=$cid;
    $data['announcement']=$announcement;
    $data['created_at']         = date("Y-m-d H:i:s");
    $data['modified_at']        = date("Y-m-d H:i:s");
    $id = \DB::table('course_announcement')->insertGetId($data);
      $result = \DB::table('course_announcement')
                ->join('users','users.id','=','course_announcement.user_id')
                ->select('course_announcement.*','users.first_name','users.last_name')
                ->where('announcement_id',$id)->first();
      $res=array(
          'announcement_id'=>$result->announcement_id,
          'announcement'=>$result->announcement,
          'username'=>$result->first_name.' '.$result->last_name,
          'date'=>$result->created_at,
        );
      return $res;
  }

  public function checkInstructor($uid,$cid){
    return \DB::table('course')->where('user_id',$uid)->where('course_id',$cid)->count();
  }

  public function updateannouncement($uid='',$cid='',$announcement='',$aid=''){
    $data = array();
    $data['announcement']=$announcement;
    $data['modified_at']        = date("Y-m-d H:i:s");
    return $id = \DB::table('course_announcement')->where('announcement_id',$aid)->where('user_id',$uid)->where('course_id',$cid)->update($data);
  }

  public function subcategories($cat_id=''){
     $subCats = \DB::table('expert_sub_categories')->where('cat_id',$cat_id)->where('status','enable')->get();

     $result = array();
     if(count($subCats)>0){
      foreach ($subCats as $key => $scvalue) {
        $result[] = array(
                  'sub_cat_id' => $scvalue->sub_cat_id, 
                  'sub_name'   => $scvalue->sub_name, 
                  'sub_slug'   => $scvalue->sub_slug, 
                  'cat_id'     => $scvalue->sub_cat_id, 
                  );
      }
     }
     return array('status'=>true,'total'=>count($subCats), 'result'=>$result);
  }

  public function answers($qid=''){
     return \DB::table('answers')->where('question_id',$qid)->where('approved', 2)->get();
  }

  public function questions($user_id='', $search_text='', $page='0'){
    $filter = '';
    if($search_text!=''){
      $filter = " AND qtn.question_text LIKE '%".$search_text."%'";
    }

    if($user_id!=''){
      $filter .= " AND qtn.entry_by =".$user_id;
    }

      $sql              = "SELECT qtn.*,cats.`name`, sub_cats.`sub_name`,users.`username`,users.`email`, users.`avatar` FROM ".env('DB_PREFIX')."questions as qtn LEFT JOIN ".env('DB_PREFIX')."categories AS cats ON cats.`id` = qtn.`category_id` LEFT JOIN ".env('DB_PREFIX')."expert_sub_categories AS sub_cats ON sub_cats.`sub_cat_id` = qtn.`sub_cat_id` LEFT JOIN ".env('DB_PREFIX')."users AS users ON users.`id` = qtn.`entry_by` WHERE qtn.`approved` = '2' AND qtn.`question_status` = '1' ".$filter." ORDER BY qtn.`question_id` DESC";



   $iPerPage         = 10;
   $sLimitFrom       = $page * $iPerPage;
   $pgNo             = " LIMIT ".$sLimitFrom.",".$iPerPage;
    $total_count      = \DB::select(\DB::raw($sql));
    $tot_cnt          = count($total_count);

    
    $questions        = \DB::select(\DB::raw($sql.$pgNo)); 
    $result = array();
    if(count($questions)>0){
      foreach ($questions as $key => $qvalue) {
        $avatar   = $this->customavatar($qvalue->email, $qvalue->entry_by);
        $result[] = array(
          'question_id'           => $qvalue->question_id, 
          'user_id'               => $qvalue->entry_by, 
          'user_image'            => $avatar, 
          'user_name'             => $qvalue->username, 
          'question_title'        => $qvalue->question_text,
          'question_description'  => $qvalue->question_description, 
          'category'              => $qvalue->name, 
          'sub_category'          => $qvalue->sub_name, 
          'answer_count'          => $qvalue->answer_count,
          'created_date'          => $qvalue->createdOn
          );
      }
    }

    return array('status'=>true,'total'=> $tot_cnt, 'result'=>$result);
  }

  public function questionview($question_id='', $type=''){

  $sql              = "SELECT qtn.*, cats.name, sub_cats.sub_name,users.username, 
  users.email, users.avatar FROM ".env('DB_PREFIX')."questions as qtn 
  LEFT JOIN ".env('DB_PREFIX')."categories AS cats ON cats.id = qtn.category_id
  LEFT JOIN ".env('DB_PREFIX')."sub_categories AS sub_cats ON sub_cats.sub_cat_id = qtn.sub_cat_id
  LEFT JOIN ".env('DB_PREFIX')."users AS users ON users.id = qtn.entry_by
  WHERE qtn.approved = '2' AND qtn.question_status = '1' AND qtn.question_id='".$question_id."'";
  $questions        = \DB::select(\DB::raw($sql)); 
  $result = array();
  if(count($questions)>0){
    foreach ($questions as $key => $qvalue) {
      $avatar   = $this->customavatar($qvalue->email, $qvalue->entry_by);
      $files    = json_decode($qvalue->file_ids,true);
      $downloadinfo = array();
      if(count($files)>0){
        $fileinfo = \DB::table('course_files')->whereIn('id',$files)->get(); 
        if (count($fileinfo)>0) {
          foreach ($fileinfo as $key => $svalue) {
            $filedowns      = url('')."/uploads/files/".$svalue->file_name.'.'.$svalue->file_extension;
            $downloadinfo[] = array(
              'file_id'  => $svalue->id,
              'filename' => $svalue->file_title,
              'file_url_to_download' => $filedowns 
              );
          }
        } 
      }

      $result[] = array(
        'question_id'           => $qvalue->question_id, 
        'user_id'               => $qvalue->entry_by,
        'user_image'            => $avatar, 
        'user_name'             => $qvalue->username, 
        'question_title'        => $qvalue->question_text,
        'question_description'  => $qvalue->question_description,
        'category_id'           => $qvalue->category_id,
        'category'              => $qvalue->name, 
        'sub_cat_id'            => $qvalue->sub_cat_id,
        'sub_category'          => $qvalue->sub_name, 
        'view_count'            => $qvalue->views,
        'answer_count'          => $qvalue->answer_count,
        'created_date'          => $qvalue->createdOn,
        'files'                 => $downloadinfo
        );
    }
    if($type==2){
      \DB::table('questions')->where('question_id',$question_id)->increment('views');  
    }
    
  }

  return array('status'=>true,'total'=> 1, 'result'=>$result);
}

  public function answerslist($question_id='', $search_text='', $page='0'){
    
    $filter = '';
    if($search_text!=''){
      $filter = " AND ans.answer_text LIKE '%".$search_text."%'";
    }

    $sql              = "SELECT ans.*, users.username, 
                        users.email, users.avatar FROM ".env('DB_PREFIX')."answers as ans 
                      LEFT JOIN ".env('DB_PREFIX')."users AS users ON users.id = ans.user_id
                      WHERE ans.approved = '2' AND ans.answer_status = '1' AND ans.question_id='".$question_id."' ".$filter."";
    $iPerPage         = 10;
    $sLimitFrom       = $page * $iPerPage;
    $pgNo             = " LIMIT ".$sLimitFrom.",".$iPerPage;
    $total_count      = \DB::select(\DB::raw($sql));
    $tot_cnt          = count($total_count);
    $questions        = \DB::select(\DB::raw($sql.$pgNo)); 
    $result = array();
    if(count($questions)>0){
      foreach ($questions as $key => $qvalue) {
        $avatar   = $this->customavatar($qvalue->email, $qvalue->user_id);
        $answers  = $this->answers($qvalue->question_id);
        $result[] = array(
          'answer_id'             => $qvalue->answer_id, 
          'user_id'               => $qvalue->user_id, 
          'user_image'            => $avatar, 
          'user_name'             => $qvalue->username, 
          'answer_text'           => $qvalue->answer_text,
          'created_date'          => $qvalue->createdOn
          );
      }
    }

    return array('status'=>true,'total'=> $tot_cnt, 'result'=>$result);
  }

  public function savequestions($request, $questionid='0'){

    $user_id        = $request->input('user_id');
    $question_title = $request->input('question_title');
    $category_id    = $request->input('category_id');
    $sub_cat_id     = $request->input('sub_cat_id');
    if(!isset($sub_cat_id)){
      $sub_cat_id   = NULL;
    }
    $description    = $request->input('description');
    $files          = $request->input('file_ids');
    $xplods         = explode(',', $files);
    $question_data  = array();
    $question_data['category_id']           = $category_id;
    $question_data['sub_cat_id']            = $sub_cat_id;
    $question_data['question_text']         = $question_title;
    $question_data['question_description']  = $description;
    $question_data['entry_by']              = $user_id;
    $question_data['createdOn']             = date("Y-m-d H:i:s");
    $question_data['question_status']       = 1;
    if(count($xplods)>0){
      $question_data['file_ids']              = json_encode($xplods);  
    }
    if(!empty($questionid)){
      $question_data['updatedOn']             = date("Y-m-d H:i:s");
      $update   = \DB::table('questions')->where('question_id',$questionid)->update($question_data);
      $insertid = $questionid;
      $message  = 'Question has been updated successfully.';
    }else{
      $insertid  = \DB::table('questions')->insertGetId($question_data);
      $message  = 'Question has been added successfully.';
    }

    return array('status' => true, 'message' => $message, 'question_id' => $insertid);
  }

  public function savereply($request, $answer_id='0'){

    $user_id        = $request->input('user_id');
    $question_id    = $request->input('question_id');
    $reply_text     = $request->input('reply_text');

    $reply_data  = array();
    $reply_data['user_id']               = $user_id;
    $reply_data['question_id']           = $question_id;
    $reply_data['answer_text']           = $reply_text;
    $reply_data['answer_status']         = 1;
    $reply_data['approved']              = 2;
    $reply_data['createdOn']             = date("Y-m-d H:i:s");
    if(!empty($answer_id)){
      $reply_data['updatedOn']           = date("Y-m-d H:i:s");
      $update   = \DB::table('answers')->where('answer_id',$answer_id)->update($reply_data);
      $insertid = $answer_id;
      $message  = 'Reply has been updated successfully.';
    }else{
      \DB::table('questions')->where('question_id',$question_id)->increment('answer_count');
      $insertid  = \DB::table('answers')->insertGetId($reply_data);
      $message  = 'Reply has been added successfully.';
    }

    return array('status' => true, 'message' => $message, 'answer_id' => $insertid);
  }

  public function deleteqtn($request){
    $user_id        = $request->input('user_id');
    $question_id    = $request->input('question_id');
    $checkIf =  \DB::table('questions')
                ->where('entry_by','=', $user_id)
                ->where('question_id','=', $question_id)
                ->first();
    if(count($checkIf)>0){
      if(!empty($qinfo->file_ids)){
        $file_ids = json_decode($qinfo->file_ids,true);
        foreach ($file_ids as $key => $file_id) {
          $resfiles = \DB::table('course_files')->where('id', '=', $rid)->get();
          if(!empty($resfiles)){
            foreach($resfiles as $resfile) {
              File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
            }
            \DB::table('course_files')->where('id', '=', $rid)->delete();

            $ccr = \DB::table('questions')->where('question_id', '=', $lid)->get();

            if(!empty($ccr) && !is_null($ccr['0']->file_ids)){
              $resources = json_decode($ccr['0']->file_ids,true);
              if(($key = array_search($rid, $resources)) !== false) {
                unset($resources[$key]);
              }
              $resources = array_values($resources);
              if(!empty($resources))
                $file_ids = json_encode($resources);
              else
                $file_ids = '';
              \DB::table('questions')->where('question_id',$lid)->update(['file_ids' => $file_ids]);
            }
          }
        }
      }
      \DB::table('questions')->where('question_id', '=', $question_id)->delete();
      return array('status' => true, 'message' => 'Question has been deleted successfully.');
    }else{
      return array('status'=>false, 'message'=> 'Questions is not found.');
    }

  }

  public function deleteanswers($request){
    $user_id        = $request->input('user_id');
    $answer_id      = $request->input('answer_id');
    $checkIf        = \DB::table('answers')
                    ->where('user_id','=', $user_id)
                    ->where('answer_id','=', $answer_id)
                    ->first();
    if(count($checkIf)>0){
      $qtnid    = $checkIf->question_id;
      \DB::table('questions')->where('question_id',$qtnid)->decrement('answer_count');
      \DB::table('answers')->where('answer_id', '=', $answer_id)->delete();
      return array('status' => true, 'message' => 'Answer has been deleted successfully.');
    }else{
      return array('status'=>false, 'message'=> 'Answer is not found.');
    }
  }

  public function deletefiles($request){
    $user_id      = $request->input('user_id');
    $file_id      = $request->input('file_id');

    $resfiles = \DB::table('course_files')->where('id', '=', $file_id)->where('uploader_id', '=', $user_id)->get();
    if(!empty($resfiles)){
      foreach($resfiles as $resfile) {
        File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
      }
      \DB::table('course_files')->where('id', '=', $file_id)->delete();

      return array('status' => true, 'message' => 'File has been deleted successfully.');
    }else{
      return array('status'=>false, 'message'=> 'File is not found.');
    }

  }

  public function membership($request){

      $user_id          = $request->input('user_id');
      $plan_id          = 0;
      if(!empty($user_id)){
        $memberships    = \DB::table('transactions')
                      ->select('transactions.*','membership_plan.*','transactions.status as status','transactions.updated_at as updated_at','transactions.created_at as created_at')
                      ->leftJoin('membership_plan', 'membership_plan.plan_id', '=', 'transactions.course_id')
                      ->where('user_id', '=', $user_id)
                      ->where('transactions.purchase_type', '=', '2')
                      ->whereIn('transactions.status', array('completed','cancelled','failed'))
                      ->orderBy('transactions.id', 'desc')
                      ->first();
        if(count($memberships)>0){
          $plan_id = $memberships->plan_id;
        }
      }
      
     $membership = \DB::table('membership_plan')->orderby('plan_id', 'asc')->get();
     $results = array();
     if(count($membership)>0){
      foreach ($membership as $key => $scvalue) {  

        $status = 0;
        if(!empty($plan_id) && $scvalue->plan_id==$plan_id){
          $status = 1;
        }

        $results[]=array('membership_type' => $scvalue->plan_name,
                          'membership_id' => $scvalue->plan_id,
                          'membership info' => array(
                          'membership_name' => $scvalue->plan_name, 
                          'membership_description'   => $scvalue->plan_statement, 
                          'membership_sub_id'   => $scvalue->level, 
                          'membership_price'     => $scvalue->plan_amount,
                          'membership_status'   => $status 
                          ),
             );
          }
     }
     return array('status'=>true,'total'=>count($membership), 'result'=>$results);
  }

  public function membershipinfo($request){
    $user_id          = $request->input('user_id');

    $membership             = \DB::table('transactions')
                      ->select('transactions.*','membership_plan.*','transactions.status as status','transactions.updated_at as updated_at','transactions.created_at as created_at')
                      ->leftJoin('membership_plan', 'membership_plan.plan_id', '=', 'transactions.course_id')
                      ->where('user_id', '=', $user_id)
                      ->where('transactions.purchase_type', '=', '2')
                      ->whereIn('transactions.status', array('completed','cancelled','failed'))
                      ->orderBy('transactions.id', 'desc')
                      ->first();
    $result        = array();
    if (defined('CNF_CURRENCY')){
        $currency = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
    }else{
        $currency = '$';
    }
   
    if(count($membership)>0){

        if(isset($membership->status) && $membership->status == 'failed'){
          $enddate  = date("jS F Y | h:i:s A",strtotime('+1 '.str_replace('ly', '', $membership->purchase_period),strtotime($membership->created_at)));
          $status   = 'failed';
        }elseif(isset($membership->status) && $membership->status == 'cancelled'){
          $enddate  = date("jS F Y | h:i:s A",strtotime('+1 '.str_replace('ly', '', $membership->purchase_period),strtotime($membership->created_at)));
          $status   = 'cancelled';
        }elseif(isset($membership->status) && $membership->status == 'completed'){
          $enddate  = date("jS F Y | h:i:s A",strtotime('+1 '.str_replace('ly', '', $membership->purchase_period),strtotime($membership->created_at)));
          $status   = 'Active';
        }

        $price      = $currency.$membership->amount.' '.$membership->purchase_period;
        $member_id  = str_pad($membership->id,10,"0", STR_PAD_LEFT); 
        $result[] = array(
                    'membership_id'           => $member_id,
                    'membership_name'         => $membership->plan_name,
                    'membership_status'       => $status,
                    'membership_price'        => $price,
                    'membership_purchased_at' => date("jS F Y | h:i:s A", strtotime($membership->created_at)),
                    'membership_renewed_at'   => $enddate,
                    );
    }

    return array('status'=>true,'total'=>count($membership), 'result'=>$result);
  }
  
  public function cancelmembership($uid='', $mid=''){
    
     $user_details = \DB::table('users')->where('id', $uid)->get();
     if(count($user_details)>0){
        return array('status'=>true,'total'=>count($user_details), 'msg'=>'user available');
     }
     else{
      return array('status'=>true, 'msg'=>'select first user');
     }

     // $results = array();
     // if(count($membership)>0){
     //  foreach ($membership as $key => $scvalue) {  
     //    $results[]=array('membership_type' => $scvalue->plan_name,
     //                      'membership_id' => $scvalue->plan_id,
     //                      'membership info' => array(
     //                      'membership_name' => $scvalue->plan_name, 
     //                      'membership_description'   => $scvalue->plan_statement, 
     //                      'membership_sub_id'   => $scvalue->level, 
     //                      'membership_price'     => $scvalue->plan_amount, 
     //                      ),
     //         );
     //      }
     // }
     
  }


  public function businesspackagelist($request){
     $user_id      = $request->input('user_id');
     $package      = \DB::table('business_plan')
                   ->orderby('business_plan_id', 'asc')
                   ->get();
    $result        = array();
    if (defined('CNF_CURRENCY')){
     $currency = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
    }
    
    if (count($package)>0) {
      foreach ($package as $key => $pkvalue) {
        $type   = 'Monthly';
        if($pkvalue->business_plan_interval=='2'){
          $type = 'Yearly';
        }
        $result[] = array(
                    'pakcage_type'         => $type,
                    'pakcage_id'           => $pkvalue->business_plan_id,
                    'pakcage_name'         => $pkvalue->business_plan_name,
                    'pakcage_description'  => $pkvalue->business_plan_statement,
                    'pakcage_currency'     => $currency,
                    'pakcage_price'        => $pkvalue->business_plan_amount
                    );
      }
    }

    return array('status'=>true,'total'=>count($package), 'result'=>$result);

  }

  public function assignedinfo($request){

    $business_id      = $request->input('business_id');
    $user_id          = $request->input('user_id');
    $page             = $request->input('page_no');

    $sql              = "SELECT ct.*, ce.*, ci.image_hash FROM ".env('DB_PREFIX')."course_taken as ct 
                      LEFT JOIN ".env('DB_PREFIX')."course AS ce ON ce.course_id = ct.course_id
                      LEFT JOIN ".env('DB_PREFIX')."course_images as ci ON ci.id = ce.image
                      WHERE ct.user_id = '".$user_id."' AND ct.business_user_id = '".$business_id."'";
    $iPerPage         = 10;
    $sLimitFrom       = $page * $iPerPage;
    $pgNo             = " LIMIT ".$sLimitFrom.",".$iPerPage;
    $total_count      = \DB::select(\DB::raw($sql));
    $tot_cnt          = count($total_count);
    $courses          = \DB::select(\DB::raw($sql.$pgNo)); 
    $results          = array();
    if(count($courses)>0){
      foreach ($courses as $key => $cevalue) {
        $total                    = $this->courses->getCourseLectureTotal($cevalue->course_id);
        $completed                = $this->courses->getCourseCompletedCount($cevalue->course_id,$user_id);
        $percentage                = ($completed / $total) * 100;
        $image                    = url().'/course/image/'.$cevalue->image_hash.'/_medium';
        $results[] = array(
          'course_id'             => $cevalue->course_id, 
          'course_title'          => $cevalue->course_title,
          'course_sub_title'      => $cevalue->subtitle,
          'course_image'          => $image,
          'course_percentage'     => $percentage,
          'assigned_date'         => $cevalue->created_at,
          );
      }
    }
     return array('status'=>true,'total'=> $tot_cnt, 'result'=>$results);
  }

  public function courseinfo($request){

    $business_id      = $request->input('business_id');
    $getinfo          = \bsetecHelpers::getDashboards($business_id);
    $results          = array();
    if(count($getinfo)>0){
      $mycourse        = \DB::table('course')
                       ->leftJoin('users', 'users.id', '=', 'course.user_id')
                       ->leftJoin('course_images', 'course_images.id', '=', 'course.image')
                       ->select('course.*','users.id','course_images.image_hash')
                       ->where('course.approved','=',1)
                       ->groupBy('course.course_id')
                       ->orderBy('course.created_at', 'desc')
                       ->get();
      if(count($mycourse)>0){
        foreach ($mycourse as $key => $cevalue) {
          $image                    = url().'/course/image/'.$cevalue->image_hash.'/_medium';
          $results[] = array(
            'course_id'             => $cevalue->course_id, 
            'course_title'          => $cevalue->course_title,
            'course_image'          => $image
            );
        }
      }

    }

    return array('status' => true, 'total' => count($mycourse), 'result' => $results);
  }

  public function listbusinessusers($request){

    $business_id      = $request->input('business_id');
    $page_no          = $request->input('page_no');
    $iPerPage         = 10;
    $sLimitFrom       = $page_no * $iPerPage;
    $users_total = \DB::table('users')->select('id','username','avatar','email','active')->where('business_id', '=', $business_id)->get();
    $users       = \DB::table('users')->select('id','username','avatar','email','active')->where('business_id', '=', $business_id)->limit($iPerPage)->offset($sLimitFrom)->get();
    $results     = array();
    if(!empty($users)){
      foreach ($users as $key => $scvalue) {

        $results[]=array('user_id' => $scvalue->id,
          'user_name'   => $scvalue->username,
          'user_image'  => $scvalue->avatar, 
          'email'       => $scvalue->email, 
          'status'      => $scvalue->active,

          );
      }
      return array('status'=>true, 'total' => count($users_total), 'result'=>$results);
    }else{
      return array('status'=>false, 'message'=> 'User id is not found');
    }
    
  }

  public function addbusinessusers($request){
    $data                 = array();
    $data['first_name']   = $request['firstname'];
    $data['last_name']    = $request['lastname'];
    $data['password']     = $request['password'];
    $data['business_id']  = $request['business_id'];
    $data['username']     = $request['username'];
    $data['email']        = $request['email'];
    $data['active']       = $request['status'];
    $data['group_id']     = 3;
    $last_id = \DB::table('users')->insertGetId($data);
    return array('status'=>true, 'message'=> 'User hase been added Successfully' , 'user_id' => $last_id);
  }

  public function editbusinessusers($request){

     $resfiles = \DB::table('users')->select('id')->where('id', '=', $request['userid'])->get();
     if(count($resfiles)>0){
      $data  = array();
      $data['first_name']     = $request['firstname'];
      $data['last_name']      = $request['lastname'];
      $data['password']       = $request['password'];
      $data['business_id']    = $request['business_id'];
      $data['active']         = $request['status'];
      \DB::table('users')->where('id', '=', $request['userid'])->update($data);
      return array('status'=>true, 'message'=> 'User hase been edited Successfully' , 'user_id' => $request['userid']);
    }else{
      return array('status'=>false, 'message'=> 'User id is not found');
    }
  }

  public function getBusinessinfobysub($uid='', $taken_id='', $course_id=''){

    $takeninfo = \DB::table('course_taken')
            ->where('course_taken.taken_id', '=', $taken_id)
            ->where('course_taken.user_id', '=', $uid)
            ->where('course_taken.course_type', '=', '2')
            ->select('course_taken.*')
            ->first();

     if(count($takeninfo)>0){
      $companyid  = $takeninfo->company_id;
      return \DB::table('company_info')->where('user_id', '=' , $uid)->where('type', '=' , 3)->where('company_id', '=' , $companyid)->get();
    }
  }

  public function getAssignedinfobysub($uid='', $taken_id='', $course_id=''){
    return \DB::table('users')
        ->Join('course_taken', 'course_taken.user_id','=','users.id')
        ->where('course_taken.expire_id', '=', $taken_id)
        ->where('course_taken.course_id', '=', $course_id)
        ->where('users.business_id', $uid)
        ->select('users.id')
        ->groupBy('users.id','course_taken.taken_id')
        ->get();
  }

  public function checkTransactions($uid='', $cid=''){
    return \DB::table('transactions')
        ->where('transactions.course_id', '=', $cid)
        ->where('transactions.user_id', '=', $uid)
        ->get();
  }

  public function assigncoursetouser($request){

    $users_id     = $request->input('user_id');
    $course_id    = $request->input('course_id');
    $uid          = $request->input('business_id');
    $getinfo      = \bsetecHelpers::getDashboards($uid);
    $subid        = $getinfo['0']->course_id;
    $taken_id     = $getinfo['0']->taken_id;
    $getbusiness  = $this->getBusinessinfobysub($uid, $taken_id, $subid);
    $assigneduser = $this->getAssignedinfobysub($uid, $taken_id, $course_id);
    $courses      = explode(',', $course_id);
    if(count($getbusiness)>0){
      $total        = $getbusiness['0']->no_of_employee;
      $users        = count($assigneduser);
      $remaining    = $total-$users;
      if(count($users_id)>$remaining){
        if($users>0){
          $emessage    = \Lang::get('core.allowed_for').$total.' students. '.\Lang::get('core.alread_for').$users.' students';
        }else{
          $emessage   = \Lang::get('core.allowed_for').$total.' students.';
        }

        return array('status'=>false, 'message'=> $emessage);
      }
    }
    if(count($courses)>0 && !empty($course_id)){
      $i = 0;
      foreach ($courses as $key => $course_ids) {
        $checkexists        = $this->checkTransactions($users_id,$course_ids);
         $Subscriptioncourse =  \DB::table('course_taken')->where('user_id', '=', $uid)->where('course_type', '=', '1')->first();
         $getSubscription    =  \DB::table('transactions')->where('user_id', '=', $uid)->where('purchase_type', '=', '1')->first();
         if(!empty($Subscriptioncourse))
          $course_taken = $Subscriptioncourse->taken_id;
         else
          $course_taken = "";

         if(!empty($getSubscription)){
                  $takenid    = $getSubscription->id;
                  $created_at = $getSubscription->created_at;
         }else{
          $takenid = 0;
          $created_at = 0;
         }

        $purchase_type = '0';

        if(count($checkexists)=='0'){
            $purchase           = new Transaction();
            $purchase->user_id      = $users_id;
            $purchase->course_id    = $course_ids;
            if(!empty($created_at)){
              $purchase->created_at   = $created_at;
            }
            $purchase->amount       = 0;
            $purchase->status       = 'completed';
            $purchase->payment_method   = "business";
            $purchase->business_user_id = $uid;
            $purchase->expire_id    = $takenid;
            $purchase->save();
            // add for taken course by user
            $courseTaken        = new CourseTaken();
            $courseTaken->user_id     = $users_id;
            if(!empty($created_at)){
              $courseTaken->created_at = $created_at;
            }
            $courseTaken->course_id   = $course_ids;
            $courseTaken->business_user_id = $uid;
            $courseTaken->course_type = $purchase_type;
            $courseTaken->expire_id = $course_taken;
            $courseTaken->save();
          }
          $i++;
      }
      $message   = \Lang::get('core.assign_sucess');
      return array('status' => true,  'message' => $message);
    }else{
      return array('status'=>false, 'message'=> 'No records found');
    }

  }

  public function deletebusinessuser($request){
    $business_id  = $request->input('business_id');
    $user_id      = $request->input('user_id');
    $getinfos     = \DB::table('users')->where('business_id', '=', $business_id)->where('id', '=', $user_id)->get();
    if(count($getinfos)>0){
      \DB::table('course_taken')->where('user_id', '=', $user_id)->delete();
      \DB::table('transactions')->where('user_id', '=', $user_id)->delete();
      \DB::table('users')->where('id', '=', $user_id)->delete();
      return array('status' => true,  'message' => 'User has been removed Successfully.');
    }else{
      return array('status' => false, 'message' => 'sorry! this account cannot delete');
    }
   
  }
// end of class
}