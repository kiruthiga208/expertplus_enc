<?php 
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class sendmail extends bsetec  {
	
	protected $table = 'send_mail_list';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."send_mail_list.*,".\bsetecHelpers::getdbprefix()."user_group.group_name FROM ".\bsetecHelpers::getdbprefix()."send_mail_list left JOIN ".\bsetecHelpers::getdbprefix()."user_group ON ".\bsetecHelpers::getdbprefix()."send_mail_list.group_id = ".\bsetecHelpers::getdbprefix()."user_group.id  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."send_mail_list.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
