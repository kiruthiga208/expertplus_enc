<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class currency extends bsetec  {
	
	protected $table = 'currency';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."currency.* FROM ".\bsetecHelpers::getdbprefix()."currency  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."currency.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
