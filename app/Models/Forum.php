<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class forum extends bsetec  {
	
	protected $table = 'forum';
	protected $primaryKey = 'forum_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."forum.*,".\bsetecHelpers::getdbprefix()."forum_category.name,".\bsetecHelpers::getdbprefix()."users.first_name FROM ".\bsetecHelpers::getdbprefix()."forum JOIN ".\bsetecHelpers::getdbprefix()."forum_category ON ".\bsetecHelpers::getdbprefix()."forum.category_id = ".\bsetecHelpers::getdbprefix()."forum_category.forum_cat_id JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."users.id=".\bsetecHelpers::getdbprefix()."forum.created_by ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."forum.forum_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
