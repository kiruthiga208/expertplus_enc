<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class withdrawrequests extends bsetec  {
	
	protected $table = 'withdraw_requests';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."withdraw_requests.*, ".\bsetecHelpers::getdbprefix()."users.first_name FROM ".\bsetecHelpers::getdbprefix()."withdraw_requests left join ".\bsetecHelpers::getdbprefix()."users on ".\bsetecHelpers::getdbprefix()."users.id =  ".\bsetecHelpers::getdbprefix()."withdraw_requests.user_id";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."withdraw_requests.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public function get_withdraw_requests($user_id)
	{
	    return \DB::table('withdraw_requests')
	                // ->join('users', 'users.id', '=', 'withdraw_requests.user_id')
	                ->select('withdraw_requests.*'
	                          // \DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name) AS customer_name')
	                  )
	                ->where('withdraw_requests.user_id', '=', $user_id)
	                ->get();
	}

	public function save_withdraw_requests($data)
	{
	    //deduct the credits from user
	    \DB::table('user_details')->where('user_id',$data['user_id'])->decrement('total_credits', $data['amount']);

	    //insert the withdraw requests in withdraw table
	    \DB::table('withdraw_requests')->insertGetId($data);
	}

	public function get_delete($row)
	{
		if(isset($row))
		{
			$increment = \DB::table('user_details')->where('user_id',$row->user_id)->increment('total_credits',$row->amount);
			$delete = \DB::table('withdraw_requests')->where('id', $row->id)->Delete();	
			return true;
		}
	}
	

}
