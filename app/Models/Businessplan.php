<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class businessplan extends bsetec  {
	
	protected $table = 'business_plan';
	protected $primaryKey = 'business_plan_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."business_plan.* FROM ".\bsetecHelpers::getdbprefix()."business_plan  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."business_plan.business_plan_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public static function getMonthlyplan($count='0'){
		/*if(!empty($count)){
			return \DB::table('business_plan')->where('business_plan_interval', '=' , 1)->where('business_plan_solicitor_count', '=' , $count)->get();
		}else{
			return \DB::table('business_plan')
					->where('business_plan_interval', '=', '1')
					->where(function ($query) {
		                $query->orWhere('business_plan_solicitor_count', '=' , 1)
							  ->orWhere('business_plan_solicitor_count', '=' , 10)
							  ->orWhere('business_plan_solicitor_count', '=' , 20)
							  ->orWhere('business_plan_solicitor_count', '=' , 0);
		            })->get();	
		}*/
		return \DB::table('business_plan')->where('business_plan_interval', '=' , 1)->get();
		
	}

	public static function getYearlyplan(){

		// return \DB::table('business_plan')
		// 		->where('business_plan_interval', '=', '2')
		// 		->where(function ($query) {
	 //                $query->orWhere('business_plan_solicitor_count', '=' , 1)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 10)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 20)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 0);
	 //            })->get();
	     return \DB::table('business_plan')
				->where('business_plan_interval', '=', '2')->get();	
	}

	public function insertCompanydetails($postvalue=''){

 		$number_course 	= $postvalue['number_course'];
 		if(!empty($postvalue['plan_type'])){
 			$course_id			= $postvalue['plan_type'];
 			$type 				= 1;
 		}
 		$uid 				= \Session::get('uid');
 		$exist    			= \DB::table('company_info')->where('user_id', '=' , $uid)->where('course_id', '=' , $course_id)->where('type', '=' , 1)->get();
 		if (count($exist) == 0){
 			\DB::table('company_info')->insert(array('user_id' => $uid, 'no_of_course' => $number_course, 'course_id' => $course_id, 'type'=>$type, 'created_at' => Carbon::now()));
 		}else{
 			$cid 	= $exist['0']->company_id;
 			\DB::table('company_info')->where('company_id', '=' , $cid)->update(array('no_of_course' => $number_course, 'course_id' => $course_id, 'type'=>$type, 'created_at' => Carbon::now()));
 		}
 		return true;
 	}

 	public static function getPlanbyid($id=''){

		return \DB::table('business_plan')->where('business_plan_id', $id)->get();
	}

	public static function getMonthlyplanpayment(){

		// return \DB::table('business_plan')
		// 		->where('business_plan_interval', '=', '1')
		// 		->where(function ($query) {
	 //                $query->orWhere('business_plan_solicitor_count', '=' , 1)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 10)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 20)
		// 				  ->orWhere('business_plan_solicitor_count', '=' , 30);
	 //            })->limit(4)->get();	

	     return \DB::table('business_plan')
				->where('business_plan_interval', '=', '1')->limit(4)->get();

	}

	public function checktype($uid='', $course_id=''){
 		return \DB::table('company_info')->where('user_id', '=' , $uid)->where('course_id', '=' , $course_id)->where('type', '=' , 1)->get();
 	}

 	public function allowstudents($uid, $type='0'){
		if($type=='1'){

			return \DB::table('transactions')
		            ->leftJoin('company_info', 'company_info.course_id', '=', 'transactions.course_id')
		            ->select('transactions.*')
		            ->where('transactions.business_user_id', '=', $uid)
		            ->groupBy('transactions.id')
		            ->get();

		}elseif($type=='2'){

			return \DB::table('users')->where('business_id', '=', $uid)->get();

		}else{

			return \DB::table('company_info')
		            ->where('company_info.user_id', '=', $uid)
		            ->where('company_info.status', '=', '1')
		            ->get();
		}

	}

	
    public function getStudents($uid, $start, $length, $type='0'){
		if($type=='1'){
			return \DB::table('users')
					->where('group_id','=','3')
					->where('active','=','1')
					->where('id','!=',$uid)
					->get();
			// return \DB::table('users')
			// 		->where('business_id', $uid)
			// 		->get();
		}else{
			return \DB::table('users')
					->where('group_id','=','3')
					->where('active','=','1')
					->where('id','!=',$uid)
					->skip($start)->take($length)->orderBy('id', 'desc')->get();
			// return \DB::table('users')
			// 		->where('business_id', $uid)
			// 		->skip($start)->take($length)->orderBy('id', 'desc')->get();
		}
 	}

 	public function checkBusinessid($uid='',$sid=''){
 		$count = \DB::table('users')->where('id', '=' , $sid)->where(function ($query) use ($uid) {
				    $query->where('business_id', '=', $uid);
				})->get();
 		return count($count);
 	}

 	public function getAllcourses($uid, $start, $length, $type='0'){
 		
		if($type=='1'){
			return \DB::table('course_taken')
            ->leftJoin('company_info', 'company_info.company_id', '=', 'course_taken.company_id')
            ->select('course_taken.*','company_info.no_of_course')
            ->where('course_taken.user_id', '=', $uid)
            // ->where('company_info.user_id', '=', $uid)
            ->where('company_info.no_of_course', '>', '0')
            ->whereIn('course_taken.course_type', ['1'])
            ->groupBy('course_taken.taken_id')
            ->get();
		}else{
			return \DB::table('course_taken')
            ->leftJoin('company_info', 'company_info.company_id', '=', 'course_taken.company_id')
            ->select('course_taken.*','company_info.no_of_course')
            ->where('course_taken.user_id', '=', $uid)
            // ->where('company_info.user_id', '=', $uid)
            ->where('company_info.no_of_course', '>', '0')
            ->whereIn('course_taken.course_type', ['1'])
            ->orderBy('course_taken.taken_id', 'desc')
            ->groupBy('course_taken.taken_id')
            ->skip($start)->take($length)
            ->get();
		}
 	}

 	public function courseInfo($cid=''){
 		return \DB::table('course')->select('course_title','slug')->where('course_id', '=' , $cid)->get();
 	}

 	public function getallcourse($id=''){
 		if($id != ''){
 			return \DB::table('course')->select('course_title','course_id')->where('course_id',$id)->first();
 		}else{
 			return \DB::table('course')->select('course_title','course_id')->where('approved','=', '1')->orderBy('course_id', 'desc')->get();

 		}

 	}

 	public function getStudentinfos($uid='', $cid='', $type='0'){

 		if($type=='1'){
 			return \DB::table('users')
 				->where('users.business_id', $uid)
 				
 				->select('users.id','users.username','users.email')
 				->get();
 		}


 		$getinofs = \DB::table('users')
 				->Join('transactions', 'transactions.user_id','=','users.id')
 				->where('transactions.course_id', '=', $cid)
 				->where(function ($query) use ($uid) {
				    $query->where('users.business_id', '=', $uid);
				})
 				->select('users.id')
 				->groupBy('users.id','transactions.id')
 				->get();
 		$common  = array();

 		if (count($getinofs)>0) {
 			foreach ($getinofs as $key => $userid) {
				$common[] = $userid->id;
			}

			$implode   = implode(',', $common);

			return \DB::select("SELECT ".\bsetecHelpers::getdbprefix()."users.`id`, ".\bsetecHelpers::getdbprefix()."users.`username` , ".\bsetecHelpers::getdbprefix()."users.`email`FROM ".\bsetecHelpers::getdbprefix()."users where `business_id` = '".$uid."'  AND `id` not in (".$implode.")");

 		}else{

 			return \DB::table('users')
 				->where('users.business_id', $uid)
 				->select('users.id','users.username','users.email')
 				->get();

 		}
 	}

 	public function checkTransactions($uid='', $cid=''){
 		return \DB::table('transactions')
 				->where('transactions.course_id', '=', $cid)
 				->where('transactions.user_id', '=', $uid)
 				->where('transactions.status','=','completed')
 				->get();
 	}

 	public function getBusinessinfobysub($uid='', $taken_id='', $course_id=''){


 		$takeninfo = \DB::table('course_taken')
		 				->where('course_taken.taken_id', '=', $taken_id)
		 				->where('course_taken.user_id', '=', $uid)
		 				//->where('course_taken.course_type', '=', '2')
		 				->where('course_taken.type','=','business')
		 				->select('course_taken.*')
		 				->first();


		 if(count($takeninfo)>0){
		 	$companyid  = $takeninfo->company_id;
 			return \DB::table('company_info')->where('user_id', '=' , $uid)->where('type', '=' , 1)->where('company_id', '=' , $companyid)->get();
 		}
 	}

 	public function getAssignedinfobysub($uid='',  $course_id=''){
 		return \DB::table('users')
 				->Join('course_taken', 'course_taken.user_id','=','users.id')
 				->where('course_taken.course_id', '=', $course_id)
 				->where('course_taken.business_user_id', $uid)
 				->select('users.id')
 				->groupBy('users.id','course_taken.taken_id')
 				->get();
 	}

 	public function getAllassignedcoursebystd($uid='', $sid=''){

 		return \DB::table('users')
 				->Join('transactions', 'transactions.user_id','=','users.id')
 				->Join('course', 'course.course_id','=','transactions.course_id')
 				->Join('course_taken','course_taken.course_id','=','transactions.course_id')
 				->where('transactions.user_id', '=', $sid)
 				->where('course_taken.business_user_id',$uid)
 				/*->where(function ($query) use ($uid) {
				    $query->where('users.business_id', '=', $uid);
				    // ->orWhereRaw('FIND_IN_SET('.$uid.','.\bsetecHelpers::getdbprefix().'users.combine_business_id)');
				})*/
 				->select('course.course_id','course.course_title')
 				->groupBy('transactions.id')
 				->get();
	}

	public function insertCompanydetailsmobile($postvalue=''){

 		$number_employee 	= $postvalue['number_employee'];
 		if(!empty($postvalue['plan_type'])){
 			$course_id			= $postvalue['plan_type'];
 			$type 				= 1;
 		}
 		$uid 				= $postvalue['uid'];
 		$exist    			= \DB::table('company_info')->where('user_id', '=' , $uid)->where('course_id', '=' , $course_id)->where('type', '=' , 1)->get();
 		if (count($exist) == 0){
 			\DB::table('company_info')->insert(array('user_id' => $uid, 'no_of_employee' => $number_employee, 'course_id' => $course_id, 'type'=>$type, 'created_at' => Carbon::now()));
 		}else{
 			$cid 	= $exist['0']->company_id;
 			\DB::table('company_info')->where('company_id', '=' , $cid)->update(array('no_of_employee' => $number_employee, 'course_id' => $course_id, 'type'=>$type, 'created_at' => Carbon::now()));
 		}
 		return true;
 	}

 	public function getBusinessuser($id)
 	{
 		  return \DB::table('course_taken')
            ->leftJoin('company_info', 'company_info.company_id', '=', 'course_taken.company_id')
            ->select('course_taken.*')
            ->where('course_taken.user_id', '=', $id)
            ->where('course_taken.type','=','business')
            ->groupBy('course_taken.taken_id')
            ->get();
 	}

 	public function getBusinessusercourse($id)
 	{
	 	return \DB::table('transactions')->join('course_taken','course_taken.course_id','=','transactions.course_id')
	 		->select('transactions.course_id','transactions.quantity')
	 		->where('course_taken.type','=','course')
	 		->where('course_taken.user_id',$id)		
	 		->where('transactions.user_id',$id)
	 		->where('transactions.status','=','completed')
	 		->get();

 	}

 	public function getcoursebybusinessuser($id)
 	{
 		return \DB::table('course')->where('user_id',$id)->where('approved','=','1')->select('course_id','course_title')->get();
 	}

 	public function getUserslist()
 	{
 		return \DB::table('users')->where('group_id','3')->where('active','1')->where('id','!=',\Auth::user()->id)->select('id','username')->get();
 	}

 	public function checkcoursequantity($uid,$course_id)
 	{
 		return \DB::table('transactions')->where('user_id',$uid)->where('course_id',$course_id)->where('status','=','completed')->first();


 	
 	}

 	public function createdcourse($uid)
 	{
 		return \DB::table('course')->where('user_id',$uid)->where('approved','=','1')->get();
 	}


}
