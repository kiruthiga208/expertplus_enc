<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class blogcomment extends bsetec  {
	
	protected $table = 'blogcomments';
	protected $primaryKey = 'commentID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."blogcomments.* FROM ".\bsetecHelpers::getdbprefix()."blogcomments  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."blogcomments.commentID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
