<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseImages extends Model {

	protected $table 		= 'course_images';
	protected $primaryKey 	= 'id';

	public $timestamps = false;
			
	public function __construct() {
		parent::__construct();
	}

	protected function getDateFormat()
    {
        return 'U';
    }

    public function user(){
    	return $this->belongsTo('User');
    }

}
