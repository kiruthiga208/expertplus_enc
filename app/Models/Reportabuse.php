<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class reportabuse extends bsetec  {
	
	protected $table = 'report_abuse';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "SELECT ".\bsetecHelpers::getdbprefix()."report_abuse.*,
				".\bsetecHelpers::getdbprefix()."report_abuse.user_id AS userid,
				".\bsetecHelpers::getdbprefix()."course.course_title AS course_id,
				CONCAT(".\bsetecHelpers::getdbprefix()."users.first_name, ' ', ".\bsetecHelpers::getdbprefix()."users.last_name) AS user_id FROM ".\bsetecHelpers::getdbprefix()."report_abuse 
				LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."users.id = ".\bsetecHelpers::getdbprefix()."report_abuse.user_id
				LEFT JOIN ".\bsetecHelpers::getdbprefix()."course ON ".\bsetecHelpers::getdbprefix()."course.course_id = ".\bsetecHelpers::getdbprefix()."report_abuse.course_id";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."report_abuse.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public static function getsearchUser(){
		return \DB::table('report_abuse')
			   ->join('users', 'users.id', '=', 'report_abuse.user_id')
			   ->select('report_abuse.*',\DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, "", '.\bsetecHelpers::getdbprefix().'users.last_name) AS user_name'))
			   ->groupBy('report_abuse.user_id')
			   ->get();
	}
	public static function getsearchCourse(){
		return \DB::table('report_abuse')
			   ->join('course', 'course.course_id', '=', 'report_abuse.course_id')
			   ->select('report_abuse.*','course.course_title')
			   ->groupBy('report_abuse.course_id')
			   ->get();
	}
	

}
