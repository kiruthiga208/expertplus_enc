<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class forumcomments extends bsetec  {
	
	protected $table = 'forum_comments';
	protected $primaryKey = 'comment_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		// SELECT forum_comments.*,forum.forum_topic,users.first_name FROM forum_comments JOIN forum ON forum_comments.forum_id = forum.forum_id JOIN users ON forum_comments.user_id  = users.id
		return "  SELECT ".\bsetecHelpers::getdbprefix()."forum_comments.*,".\bsetecHelpers::getdbprefix()."forum.forum_topic,".\bsetecHelpers::getdbprefix()."users.first_name FROM ".\bsetecHelpers::getdbprefix()."forum_comments JOIN ".\bsetecHelpers::getdbprefix()."forum ON ".\bsetecHelpers::getdbprefix()."forum_comments.forum_id = ".\bsetecHelpers::getdbprefix()."forum.forum_id JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."forum_comments.user_id  = ".\bsetecHelpers::getdbprefix()."users.id  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."forum_comments.comment_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
