<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class courselisting extends bsetec  {
	
	protected $table = 'course';
	protected $primaryKey = 'course_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "SELECT ".\bsetecHelpers::getdbprefix()."course.*,
				".\bsetecHelpers::getdbprefix()."course.user_id AS userid,
				CONCAT(".\bsetecHelpers::getdbprefix()."users.first_name, ' ', ".\bsetecHelpers::getdbprefix()."users.last_name) AS user_id
				FROM ".\bsetecHelpers::getdbprefix()."course  
				LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."users.id = ".\bsetecHelpers::getdbprefix()."course.user_id";
	}

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."course.approved >= '0' AND ".\bsetecHelpers::getdbprefix()."course.course_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public function insertfeedback($comments,$courseid){

	   $loggeduser     					= \Session::get('uid');
       $dataarray                       = array();
       $dataarray['course_id']          = $courseid;
       $dataarray['user_id']            = $loggeduser;
       $dataarray['comments']           = $comments;
       $dataarray['created_at']         = date("Y-m-d H:i:s");
       $dataarray['modified_at']        = date("Y-m-d H:i:s");
       \DB::table('course_feedback')->insertGetId($dataarray);
		
	}
	
	public function getcourseinfos($cid='') {
      return \DB::table('course')
                  ->join('users', 'users.id', '=', 'course.user_id')
                  ->where('course_id','=',$cid)
                  ->select('course.*', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
                  ->first();
    }

	public function getallcomments($cid='',$parentid=0)
	{
		return \DB::table('course_feedback')
		        ->leftJoin('users', 'users.id', '=', 'course_feedback.user_id')
		        ->select('course_feedback.feedback_id', 'course_feedback.user_id', 'course_feedback.course_id','course_feedback.parentid', 'course_feedback.comments', 'course_feedback.feedback_type', 'course_feedback.created_at', 'course_feedback.modified_at', 'users.username', 'users.avatar', 'users.email')
		        ->where('course_id',$cid)
		        ->where('parentid',$parentid)
		        ->get();
	}

	public function deletefeedback($fid='', $cid)
	{
		return \DB::table('course_feedback')->where('course_id', '=', $cid)->where('feedback_id', '=', $fid)->delete(); 
	}

	public static function getsearchColumn($column){
		return \DB::table('course')
			   ->join('users', 'users.id', '=', 'course.user_id')
			   ->select('course.*','course.user_id AS userid',\DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, "", '.\bsetecHelpers::getdbprefix().'users.last_name) AS user_id'))
			   ->groupBy('course.'.$column)
			   ->get();
	}
	

	public static function getuser($value)
	{
		$dbresults = \DB::table('course')->where('course_id', '=', $value)->get();
		foreach($dbresults as $key)
		{
			$dbresults = \DB::table('users')->where('id', '=', $key->user_id)->get();
		}
		if(count($dbresults)>0){
            return 1;
        }else{
            return 0;
        }	
	}

	public function updateCourseGamification($cid='', $value)
	{
        $data['gamification']   = $value;
        return \DB::table('course')->where('course_id','=', $cid)->update($data);
	}
}
