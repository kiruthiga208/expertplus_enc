<?php namespace App\Models\Core;

use App\Models\bsetec;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Pages extends bsetec  {
	
	protected $table = 'pages';
	protected $primaryKey = 'pageID';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."pages.* FROM ".\bsetecHelpers::getdbprefix()."pages  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."pages.pageID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
