<?php namespace App\Models\Core;

use App\Models\bsetec;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Logs extends bsetec  {
	protected $table = 'logs';
	protected $primaryKey = 'auditID';

	public function __construct() {
		parent::__construct();
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."logs.* FROM ".\bsetecHelpers::getdbprefix()."logs  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."logs.auditID IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
