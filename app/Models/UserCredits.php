<?php
namespace App\Models;

class UserCredits extends bsetec
{
   
    protected $table = 'user_credits';
    protected $primaryKey = 'id';

    public function __construct() 
    {
        parent::__construct();
        
    }

    public function get_credits($user_id)
    {
      return \DB::table('user_credits')
                  ->join('users', 'users.id', '=', 'user_credits.by_user_id')
                  ->join('course', 'course.course_id', '=', 'user_credits.course_id')
                  ->select('user_credits.*',
                           'course.course_title', 
                            \DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name) AS customer_name')
                    )
                  ->where('user_credits.instructor_user_id', '=', $user_id)
                  ->orderBy('user_credits.created_at' , 'desc')
                  
                  ->paginate(10);
    }

    public function get_total_credits($user_id)
    {
        $credits =  \DB::table('user_details')
                    ->where('user_details.user_id', '=', $user_id)
                    ->first();
        if($credits)
        {
          return $credits->total_credits;
        }
        else
        {
          return false;
        }
                  
    }

    public function save_credits($data)
    {
      //check if the credits for admin, if so increment for admin
      if($this->check_key_value($data, 'is_admin', 1))
      {
          $admin = \DB::table('options')
                  ->where('code', '=', 'commision_to_admin_id')
                  ->first();
          $instructor_user_id = $admin->option;   
          $data['instructor_user_id'] = $instructor_user_id;
      }//else increment the credits for particular user
      else
      {
          $instructor_user_id = $data['instructor_user_id'];
      }
      //if already the instructor credits inserted, just update it or else insert it as new record
      $check = \DB::table('user_details')->where('user_id', '=' , $instructor_user_id)->get();
      if(count($check)>0)
      {

        \DB::table('user_details')->where('user_id',$instructor_user_id)->increment('total_credits', $data['credit']);
      }
      else
      {

        $options['user_id'] = $instructor_user_id;
        $options['total_credits'] = $data['credit'];
        \DB::table('user_details')->insertGetId($options);
      }
      
      //save the credits in history table
      \DB::table('user_credits')->insertGetId($data);
    }
  
  public function check_key_value($array, $key, $val) 
  {
      //commented code for multi dimensional array
      //foreach ($array as $item)
      foreach ($array as $item_key => $item_val)
          //if (isset($item[$key]) && $item[$key] == $val)
            if ($item_key == $key && $item_val == $val)
              return true;
      return false;
  }

}
