<?php

namespace App\Models;
use DB;

class Dashboard extends bsetec
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function get()
    {
        //category wise listing courses
        $category_wise_course = DB::table('course')
                        ->select('categories.name as category', DB::raw('COUNT('.\bsetecHelpers::getdbprefix().'course.course_id) as total'))
                        ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                        ->where('course.cat_id','!=',"NULL")
                        ->groupBy('course.cat_id')
                        ->get();
        $result['category_wise_course'] = json_encode($category_wise_course);

        //registered users date wise
        $registered_users = DB::select('SELECT DATE(created_at) as date, COUNT(id) as value
        FROM '.\bsetecHelpers::getdbprefix().'users GROUP BY DATE(created_at)');
        $result['registered_users'] = json_encode($registered_users);

        // $transactions = DB::select('SELECT 
        //                          SUM(if(status = "completed", 1, 0)) AS completed,
        //                          SUM(if(status = "pending", 1, 0)) AS pending,
        //                          SUM(if(status = "failed", 1, 0)) AS failed,
        //                          MONTHNAME(created_at) AS year
        //                          FROM transactions 
        //                          WHERE created_at >= now()-interval 3 month
        //                          GROUP BY MONTH(created_at)
        //                          ');

        $withdrawal_requests = DB::select('SELECT 
                                    SUM(if(status = "pending", amount, 0)) AS requests,
                                    MONTHNAME(FROM_UNIXTIME(requested_on)) AS month
                                    FROM '.\bsetecHelpers::getdbprefix().'withdraw_requests 
                                    WHERE requested_on >= UNIX_TIMESTAMP(now()-interval 3 month)
                                    GROUP BY MONTH(FROM_UNIXTIME(requested_on))
                                    ');

        $credits =  (array) DB::select('SELECT 
                                    SUM(if(credits_for = "course_commision", credit, 0)) AS commisions,
                                    SUM(if(credits_for = "course_cost", credit, 0)) AS purchases,
                                    SUM(credit) AS total,
                                    MONTHNAME(FROM_UNIXTIME(created_at)) AS month
                                    FROM '.\bsetecHelpers::getdbprefix().'user_credits 
                                    WHERE created_at >= UNIX_TIMESTAMP(now()-interval 3 month)
                                    GROUP BY MONTH(FROM_UNIXTIME(created_at))
                                    ');
        //merge two value in single array
        $j = 0;
        $transactions = array();
        for($i=2;$i>=0;$i--) 
        {
            $time = strtotime('-'.$i.' month', time()); 
            $month = date('F', $time);
            $transactions[$j] = array();
            $transactions[$j]['month'] = $month;

            foreach ($credits as $credit) 
            {
                if(in_array($month, (array) $credit))
                {
                    $transactions[$j] = (array) $credit;
                }   
            }
            foreach ($withdrawal_requests as $request) 
            {
                if(in_array($month, (array) $request))
                {
                    $transactions[$j] = array_merge($transactions[$j], (array) $request);
                }   
            }
            $j++;
        }
        // echo '<pre>';print_r($transactions);exit;
        $result['transactions'] = json_encode($transactions);

        return $result;
    }

    public function ajax_courses($days)
    {
        $condition = !empty($days) ? ' WHERE '.\bsetecHelpers::getdbprefix().'course.created_at >= now()-interval '.$days.' day and '.\bsetecHelpers::getdbprefix().'course.cat_id != "NULL"' : 'WHERE '.\bsetecHelpers::getdbprefix().'course.cat_id != "NULL"' ;

        $category_wise_course = DB::select('SELECT 
                                    '.\bsetecHelpers::getdbprefix().'categories.name as category,
                                    COUNT('.\bsetecHelpers::getdbprefix().'course.course_id) as total
                                    FROM '.\bsetecHelpers::getdbprefix().'course 
                                    LEFT JOIN '.\bsetecHelpers::getdbprefix().'categories on '.\bsetecHelpers::getdbprefix().'categories.id = '.\bsetecHelpers::getdbprefix().'course.cat_id
                                    '.$condition.'
                                    GROUP BY '.\bsetecHelpers::getdbprefix().'course.cat_id
                                    ');

        $result['category_wise_course'] = json_encode($category_wise_course);
        return $result;
    }
    

}