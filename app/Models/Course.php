<?php
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use File;
use Carbon\Carbon;


class Course extends bsetec
{
   
    protected $table = 'course';
    protected $primaryKey = 'course_id';

    public function __construct() {
        parent::__construct();
		    $driver             = config('database.default');
        $database           = config('database.connections');
		    //$this->dbprefix      = $database[$driver]['prefix']; 
    }

    public function getcourses()
    {
        $perpage = 100;
        return \DB::table('course')
                    ->leftJoin('users', 'users.id', '=', 'course.user_id')
                    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                    // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                    ->where('course.approved','=', '1')
                    ->where('course.privacy','=', '1')
                    // ->groupBy('course.course_id')
                    ->orderBy('course.created_at', 'desc')
                    ->get();
    }
    public function getlatestcourse($business_user_id=array())
    {
      $perpage = 100;
        return \DB::table('course')
                  ->join('users', 'users.id', '=', 'course.user_id')
                  ->where('course.approved','=', '1')
                  ->where('course.privacy','=', '1')
                  ->whereNotIn('users.id',$business_user_id)
                  ->orderBy('course.created_at', 'desc')
                  ->paginate($this->get_option('paginationPerpage'));
                    // ->groupBy('course.course_id')
                    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                    // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
    }
    public function getfeaturedcourse()
    {
       $business_user = \bsetecHelpers::getBusinessuser();
      return      \DB::table('course')
                  ->leftjoin('users', 'users.id', '=', 'course.user_id')
                  ->join('featured', 'featured.course_id', '=', 'course.course_id')
                  ->select('course.*','users.*',\DB::raw('count('.\DB::getTablePrefix().'featured.course_id) as featured'))
                  ->where('course.approved','=', '1')
                  ->whereNotIn('users.id',$business_user)
                  ->where('course.privacy','=', '1')
                  ->orderBy('featured', 'desc')
                  ->groupBy('course.course_id')
                  ->paginate($this->get_option('paginationPerpage'));
    }
    public function getmostviwedcourse($business_user_id=array())
    {
        return \DB::table('course')
              ->join('hits', 'hits.course_id', '=', 'course.course_id')
              ->Join('users', 'users.id', '=', 'course.user_id')
              ->select('course.*','users.*',\DB::raw('count('.\DB::getTablePrefix().'hits.course_id) as hits'))
              ->where('course.approved','=', '1')
              ->where('course.privacy','=', '1')
              ->whereNotIn('users.id',$business_user_id)
              ->groupBy('hits.course_id')
              ->orderBy('hits','desc')
              ->paginate($this->get_option('paginationPerpage'));
    }
    public function getcousestudents()
    {
      return \DB::table('course_taken')
             ->leftJoin('course', 'course.course_id', '=', 'course_taken.course_id')
             ->select('course_taken.course_id', \DB::raw('count(*) as total'))
             ->groupBy('course_id')
             ->get();
    }

    public function getbanners()
    {
        return \DB::table('banner')->where('banner_status', '=', '1')->get();;
    }

    public function getcourseinfo($id='')
    {
        // $loggeduser     = \Session::get('uid');
        // $course_author  = \DB::table('course')->where('course_id','=', $id)->where('user_id','=', $loggeduser)->count();
        // if($course_author==1) { //|| (Auth::user()->permission=='admin')
        //     $course_get = \DB::table('course')->where('course_id','=', $id)->first();
        // } else {
        //     $course_get = \DB::table('course')->where('course_id','=', $id)->where('approved','=', 1)->first();
        // }
        $course_get = \DB::table('course')->where('course_id','=', $id)->first();
        return $course_get;
    }

    public function getmorecourse($cid='', $id='')
    {       
        return \DB::table('course')->where('course_id','!=', $cid)->where('user_id','=', $id)->where('approved','=', '1')->orderBy('course_id', 'desc')->limit(3)->get();
    }

       
    public function getcurriculum($id='')
    {
       return \DB::table('curriculum_sections')
              ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
              ->select('curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
              ->where('curriculum_sections.course_id', '=', $id)->where("curriculum_lectures_quiz.publish",'=','1')->orderBy('curriculum_sections.sort_order', 'asc')->groupBy('curriculum_sections.section_id')->get(); 
    }

    public function getcurriculumcount($id='',$type='')
    {
      if($type==''){
          return \DB::table('curriculum_sections')
              ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
              ->select('curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
              ->where('curriculum_sections.course_id', '=', $id)->where("curriculum_lectures_quiz.publish",'=','1')->orderBy('curriculum_sections.sort_order', 'asc')->count(); 
      }else{
          return \DB::table('curriculum_sections')
              ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
              ->select('curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
              ->where('curriculum_sections.course_id', '=', $id)->orderBy('curriculum_sections.sort_order', 'asc')->count(); 
      }
       
    }

    public function getvisitors($id='')
    {
        $ip         = $_SERVER['REMOTE_ADDR'];
        //->where('date','=',Carbon::now()->toDateString())
        $visitor    = \DB::table('hits')->where('ip', '=' , $ip)->where('course_id', '=' , $id)->get();

        if (count($visitor) == 0)
        {
            \DB::table('hits')->insert(array('ip' => $ip, 'course_id' => $id,'date' => Carbon::now()));
        }       

        return true;
    }

    public function getviewedcoures()
    {
      $ip    = $_SERVER['REMOTE_ADDR'];
      return \DB::table('hits')
             ->join('course', 'course.course_id', '=', 'hits.course_id')
             ->where('course.approved','=', '1')->where('course.privacy','=', '1')
             ->where('hits.ip', '=' , $ip)->orderBy('hits.hits_id', 'desc')->limit(3)->get();
    }

    public function getuserinfos($id='')
    {
        // return \DB::table('users')->where('id', '=' , $id)->get();
      return \DB::table('users')
                ->where('users.id' , '=' , $id)
                ->join('instructor','users.id' , '=' , 'instructor.user_id')
                ->select('users.*','instructor.biography as ibiography')
                ->get();
    }

    public function checkalreadyexits($coursename='')
    {
       $dbcresults = \DB::table('course')->where('course_title', '=', $coursename)->get();
       if(count($dbcresults)>0){
            echo(json_encode(false));
        }else{
            echo(json_encode(true));
        }
    }

    public function deletecourses($id='')
    {
      $loggeduser     = \Session::get('uid');
      $getallcurriculam = \DB::table('curriculum_sections')
                        ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
                        ->where('curriculum_sections.course_id', '=', $id)->get();
      foreach ($getallcurriculam as $lvalue) {
          // $this->checkDeletePreviousFiles($lvalue->lecture_quiz_id);
          \DB::table('lectures_comments')->where('lecture_id',$lvalue->lecture_quiz_id)->where('section_id',$lvalue->section_id)->delete();
          \DB::table('lectures_notes')->where('lecture_id',$lvalue->lecture_quiz_id)->where('section_id',$lvalue->section_id)->delete();
          \DB::table('lectures_comment_reply')->where('lecture_id',$lvalue->lecture_quiz_id)->delete();
          \DB::table('curriculum_quiz_questions')->where('quiz_id',$lvalue->lecture_quiz_id)->delete();
          // \DB::table('curriculum_quiz_results')->where('quiz_id',$lvalue->lecture_quiz_id)->delete();
      }     
      \DB::table('curriculum_sections')
      ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
      ->where('curriculum_sections.course_id', '=', $id)->delete();
      \DB::table('favorite')->where('course_id',$id)->delete();
      \DB::table('hits')->where('course_id',$id)->delete();
      \DB::table('course_announcement')->where('course_id',$id)->delete();
      \DB::table('coupon')->where('course_id',$id)->delete();
      \DB::table('ratings')->where('course_id',$id)->delete();
      \DB::table('report_abuse')->where('course_id',$id)->delete();
      \DB::table('transactions')->where('course_id',$id)->delete();
      \DB::table('user_credits')->where('course_id',$id)->delete();
      \DB::table('course_progress')->where('course_id',$id)->delete();
      \DB::table('course_taken')->where('course_id',$id)->delete();
      \DB::table('course_feedback')->where('course_id',$id)->delete();
      if(\Session::get('gid')=='1'){
        \DB::table('course')->where('course_id', '=', $id)->delete();   
      }else{
        \DB::table('course')->where('course_id', '=', $id)->where('user_id','=',$loggeduser)->delete(); 
      }
      
      \DB::table('manage_instructors')->where('course_id',$id)->delete();
      \DB::table('invitation_users')->where('course_id',$id)->delete();
      return true;
    }

    public function getvideoinfo($id='')
    {
       return \DB::table('course_videos')->where('id', '=', $id)->get(); 
    }

    public function getimageinfo($id='')
    {
       return \DB::table('course_images')->where('id', '=', $id)->get(); 
    }

    public function getfileinfo($id='')
    {
       return \DB::table('course_files')->where('id', '=', $id)->get(); 
    }

    public function getsurveyinfo($id='')
    {
       return \DB::table('course_survey')->where('course_survey_id', '=', $id)->get(); 
    }

    public function getallusersinfos($cid='',$text='')
    { 
        $loggeduser     = \Session::get('uid');
        $checktype      = \DB::table('course')->where('course_id', '=', $cid)->get(); 
        if($checktype['0']->pricing=='0' || $checktype['0']->pricing==''){
            return \DB::table('users')->where('active', '=' , '1')->where('email', 'like', '%'.$text.'%')->where('id', '!=', $loggeduser)->get();
        }else{
            return \DB::table('users')->join('instructor', 'instructor.user_id', '=', 'users.id')->where('users.active', '=' , '1')->where('users.email', 'like', '%'.$text.'%')->where('users.id', '!=', $loggeduser)->get();
        }
    }

    public function getusersinfobyemail($cid='',$text='')
    {
       $loggeduser     = \Session::get('uid');
       $checktype      = \DB::table('course')->where('course_id', '=', $cid)->get(); 
       if($checktype['0']->pricing=='0' || $checktype['0']->pricing==''){
            $emails = \DB::table('users')->where('active', '=' , '1')->where('email', '=', $text)->where('id', '!=', $loggeduser)->get();
        }else{
            $emails = \DB::table('users')->join('instructor', 'instructor.user_id', '=', 'users.id')->where('users.active', '=' , '1')->where('users.email', 'like', '%'.$text.'%')->where('users.id', '!=', $loggeduser)->get();
        }

       if(count($emails)>0){
         $userid                          = $emails['0']->id;
         $dataarray                       = array();
         $now_date                        = date("Y-m-d H:i:s");
         $dataarray['course_id']          = $cid;
         $dataarray['user_id']            = $userid;
         $dataarray['created_at']         = $now_date;
         $dataarray['modified_at']        = $now_date;
         \DB::table('manage_instructors')->insertGetId($dataarray);
         return $emails;
       }
    }

    public function getmanageinstructors($cid='',$text='')
    {
       return \DB::table('users')
            ->join('manage_instructors', 'users.id', '=', 'manage_instructors.user_id')
            ->where('users.email','=', $text)
            ->where('manage_instructors.course_id','=', $cid)
            ->get();
    }

    public function getdeleteinstructors($cid='',$uid='')
    {
        return \DB::table('manage_instructors')->where('course_id', '=', $cid)->where('user_id', '=', $uid)->delete();
    }

    public function getinstructors($cid='',$type='')
    {
       $loggeduser     = \Session::get('uid');
       if($type=='1'){
            return \DB::table('manage_instructors')->where('course_id', '=', $cid)->where('user_id', '=', $loggeduser)->first();
       }else{
            return \DB::table('manage_instructors')->where('course_id', '=', $cid)->where('user_id', '!=', $loggeduser)->get();
       }
      
    }

    public function getcomments($cid='')
    {
        return \DB::table('comments')
                ->join('users', 'users.id', '=', 'comments.user_id')
                ->select('comments.comment_id', 'comments.user_id', 'comments.course_id', 'comments.comment', 'comments.created_at', 'comments.updated_at', 'comments.deleted_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
                ->where('comments.course_id', '=', $cid)->get();
    }

    public function insertcommentreply($commentid='', $courseid='', $comments='')
    {
       $loggeduser                      = \Session::get('uid');
       $dataarray                       = array();
       $dataarray['comment_id']         = $commentid;
       $dataarray['user_id']            = $loggeduser;
       $dataarray['course_id']          = $courseid;
       $dataarray['reply']              = $comments;
       $dataarray['created_at']         = date("Y-m-d H:i:s");
       $dataarray['updated_at']         = date("Y-m-d H:i:s");
       return \DB::table('reply')->insertGetId($dataarray);
    }

    public function insertcomment($courseid='', $comments='')
    {
       $loggeduser                      = \Session::get('uid');
       $dataarray                       = array();
       $dataarray['user_id']            = $loggeduser;
       $dataarray['course_id']          = $courseid;
       $dataarray['comment']            = $comments;
       $dataarray['created_at']         = date("Y-m-d H:i:s");
       $dataarray['updated_at']         = date("Y-m-d H:i:s");
       return \DB::table('comments')->insertGetId($dataarray);
    }


	public function getcurriculuminfo($id='',$user_id)
    {
		$result = array();
		$sections = array();
		$lecturesquiz = array();
		$lecturesquizquestions = array();
		$lecturesmedia = array();
		$lecturesresources = array();
		$uservideos = array();
		$useraudios = array();
		$userpresentation = array();
		$userdocuments = array();
		$userresources = array();
		$sections = \DB::table('curriculum_sections')->where('course_id', '=', $id)->get();
		if(count($sections)=='0'){	// IF EMPTY, CREATE DEFAULT SECTION AND LECTURE
			
			$data['course_id'] = $id;
			$data['title'] = 'Start Here';
			$data['sort_order'] = '1';
			
			$sectionId = $this->insertSectionRow($data , '');
			
			$ldata['section_id'] = $sectionId;
			$ldata['title'] = 'Introduction';
			$ldata['sort_order'] = '1';
			$ldata['type'] = '0';
			
			$lectureId = $this->insertLectureQuizRow($ldata , '');
			
			$sections = \DB::table('curriculum_sections')->where('course_id', '=', $id)->orderBy('sort_order', 'asc')->get();
			
		}
		
		
		foreach($sections as $section){
			$sectionid = $section->section_id;
			$lecturesquiz[$sectionid] = \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $sectionid)->orderBy('sort_order', 'asc')->get();
			
			if(empty($lecturesquiz[$sectionid])){
				$ldata['section_id'] = $sectionid;
				$ldata['title'] = 'Introduction';
				$ldata['sort_order'] = '1';
				$ldata['type'] = '0';
				
				$lectureId = $this->insertLectureQuizRow($ldata , '');
				$lecturesquiz[$sectionid] = \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $sectionid)->orderBy('sort_order', 'asc')->get();
			}
		
			foreach($lecturesquiz[$sectionid] as $lecture){
				$lecture_quiz_id = $lecture->lecture_quiz_id;
				if($lecture->type == 0){
					$mediaid = $lecture->media;
					if($lecture->media_type == 0) {
						$lecturesmedia[$sectionid][$lecture_quiz_id] = \DB::table('course_videos')->where('id', '=', $mediaid)->get();
					} else if($lecture->media_type == 1 || $lecture->media_type == 2 || $lecture->media_type == 5) {
						$lecturesmedia[$sectionid][$lecture_quiz_id] = \DB::table('course_files')->where('id', '=', $mediaid)->get();
					} else if($lecture->media_type == 3) {
						$lecturesmedia[$sectionid][$lecture_quiz_id] = $lecture->contenttext;
					}else if($lecture->media_type == 6) {
           $lecturesmedia[$sectionid][$lecture_quiz_id] = $lecture->youtube_id;
          }
				} else {
					$lecturesquizquestions[$sectionid][$lecture_quiz_id] = \DB::table('curriculum_quiz_questions')->where('quiz_id', '=', $lecture_quiz_id)->orderBy('sort_order', 'asc')->get();
				}
				
				if(!is_null($lecture->resources)){
					$resources = json_decode($lecture->resources,true);

					foreach($resources as $resource){	
						$lecturesresources[$sectionid][$lecture_quiz_id][] = \DB::table('course_files')->where('id', '=', $resource)->get();					
					}
				}
				
				
				$uservideos = \DB::table('course_videos')->where('uploader_id', '=', $user_id)->get();
				$useraudios = \DB::table('course_files')->where('uploader_id', '=', $user_id)->whereIn('file_extension', ['mp3', 'wav'])->get();
				$userpresentation = \DB::table('course_files')->where('uploader_id', '=', $user_id)->whereIn('file_extension', ['pdf'])->whereIn('file_tag', ['curriculum_presentation'])->get();
				$userdocuments = \DB::table('course_files')->where('uploader_id', '=', $user_id)->whereIn('file_extension', ['pdf'])->whereIn('file_tag', ['curriculum'])->get();
				$userresources = \DB::table('course_files')->where('uploader_id', '=', $user_id)->whereIn('file_extension', ['pdf', 'doc', 'docx'])->whereIn('file_tag', ['curriculum_resource'])->get();
				
			}
		}
		
		$result['sections'] = $sections;
		$result['lecturesquiz'] = $lecturesquiz;
		$result['lecturesquizquestions'] = $lecturesquizquestions;
		$result['lecturesmedia'] = $lecturesmedia;
		$result['lecturesresources'] = $lecturesresources;
		$result['uservideos'] = $uservideos;
		$result['useraudios'] = $useraudios;
		$result['userpresentation'] = $userpresentation;
		$result['userdocuments'] = $userdocuments;
		$result['userresources'] = $userresources;
		
		return $result;
		
		// if(!empty){
			// $quiz_questions = \DB::table('curriculum_quiz_questions')->where('course_id', '=', $id)->get();
		// }
		
    }
	
	
	public  function insertSectionRow($data,$id){
	
       $table = 'curriculum_sections';
	   $key = 'section_id';
	    if($id == NULL )
        {
			 $data['createdOn'] = date("Y-m-d H:i:s");	
			 $data['updatedOn'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);
        }    
        return $id;    
	}	
	
	public  function insertLectureQuizRow($data,$id){
	
       $table = 'curriculum_lectures_quiz';
	   $key = 'lecture_quiz_id';
	    if($id == NULL )
        {
			 $data['createdOn'] = date("Y-m-d H:i:s");	
			 $data['updatedOn'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);   
        }    
        return $id;    
	}
	
	public static function postSectionDelete($id){
		
		\DB::table('curriculum_sections')->where('section_id', '=', $id)->delete();
		
	}
	
	public static function postLectureQuizDelete($id){
		
		\DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id', '=', $id)->delete();
		
	}
	
	public function insertLectureQuizResourceRow($data,$id){
	
		$lecturesquiz = \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id', '=', $id)->get();
		
		if(!empty($lecturesquiz) && !is_null($lecturesquiz['0']->resources)){
			$resources = json_decode($lecturesquiz['0']->resources,true);
			array_push($resources,$data['resources']);
		} else {
			$resources = array($data['resources']);
		}
		$data['resources'] = json_encode($resources);
		$this->insertLectureQuizRow($data,$id);
	}
	
	public function postLectureResourceDelete($lid,$rid){
		
		$resfiles = \DB::table('course_files')->where('id', '=', $rid)->get();
		
		if(!empty($resfiles)){
			//foreach($resfiles as $resfile) {
				//File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
			//}
			\DB::table('course_files')->where('id', '=', $rid)->delete();
		
			$lecturesquiz = \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id', '=', $lid)->get();
			
			if(!empty($lecturesquiz) && !is_null($lecturesquiz['0']->resources)){
				$resources = json_decode($lecturesquiz['0']->resources,true);
				if(($key = array_search($rid, $resources)) !== false) {
					unset($resources[$key]);
				}
			}
			$data['resources'] = json_encode($resources);
			$this->insertLectureQuizRow($data,$lid);
		}
	}
	
	public function postQuizQuestionDelete($qid){
	
		\DB::table('curriculum_quiz_questions')->where('quiz_question_id', '=', $qid)->delete();
		
	}
	
	public function checkDeletePreviousFiles($id){

    $lecturesquiz = \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id', '=', $id)->get();

    if(!empty($lecturesquiz) && !is_null($lecturesquiz['0']->media)){
      $rid = $lecturesquiz['0']->media;
      if($lecturesquiz['0']->media_type == 0){
        $resvideos = \DB::table('course_videos')->where('id', '=', $rid)->get();
        if(!empty($resvideos)){
          foreach($resvideos as $resfile) {

            if($resfile->aws_url != NULL){
              preg_match("'https://(.*?).s3.amazonaws.com'si", $resfile->aws_url, $match);
              if(isset($match[1])){
                $bucket = $match[1];
                $s3 = new \S3(AWS_KEY, AWS_SECRET);
                if($s3->getObjectInfo($bucket, $resfile->video_title.".mp4", false)){
                  $s3->deleteObject($bucket, $resfile->video_title.".mp4", 'public-read');
                  $s3->deleteObject($bucket, $resfile->video_title.".webm", 'public-read');
                  $s3->deleteObject($bucket, $resfile->video_title.".ogv", 'public-read');
                }
              }
            }

            File::Delete('./uploads/videos/'.$resfile->video_title.'.webm') && File::Delete('./uploads/videos/'.$resfile->video_title.'.mp4') && File::Delete('./uploads/videos/'.$resfile->video_title.'.ogv');
          }
          \DB::table('course_videos')->where('id', '=', $rid)->delete();
        }
      } else {
        $resfiles = \DB::table('course_files')->where('id', '=', $rid)->get();
        if(!empty($resfiles)){
          foreach($resfiles as $resfile) {

            if($resfile->aws_url != NULL){
              preg_match("'https://(.*?).s3.amazonaws.com'si", $resfile->aws_url, $match);
              if(isset($match[1])){
                $bucket = $match[1];
                $s3 = new \S3(AWS_KEY, AWS_SECRET);
                if($s3->getObjectInfo($bucket, $resfile->file_name.".".$resfile->file_type, false)){
                  $s3->deleteObject($bucket, $resfile->file_name.".".$resfile->file_type, 'public-read');
                }
              }
            }

            File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
          }
          \DB::table('course_files')->where('id', '=', $rid)->delete();
        }
      }
    }

  }
  
  public function getcurriculumsection($id=''){

    return \DB::table('curriculum_sections')
                ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
                ->select('curriculum_sections.section_id','curriculum_sections.course_id','curriculum_sections.title','curriculum_sections.sort_order')
                ->where('curriculum_sections.course_id', '=', $id)->where("curriculum_lectures_quiz.publish",'=','1')->orderBy('curriculum_sections.sort_order', 'asc')->groupBy('curriculum_sections.section_id')->get();

  }

  public function getlecturedetails($lid='')
  {
     $getmediatype = \DB::table("curriculum_lectures_quiz")->where("lecture_quiz_id",'=',$lid)->get();
     if(count($getmediatype)>0){
          $mediaid = $getmediatype['0']->media;
          $lecture_quiz_id = $getmediatype['0']->lecture_quiz_id;
          if(isset($getmediatype['0']->media_type) && $getmediatype['0']->media_type=='0'){
              return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_videos', 'course_videos.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->get();
          }elseif(isset($getmediatype['0']->media_type) && $getmediatype['0']->media_type=='3'){
              return \DB::table("curriculum_lectures_quiz")
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->get();
          }elseif(isset($getmediatype['0']->media_type) && ($getmediatype['0']->media_type=='2' || $getmediatype['0']->media_type=='1')){
             return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_files', 'course_files.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)
                      ->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lecture_quiz_id)->get();

          } else {
				return $getmediatype;
			}
     }
  }

  public function getfirstlecturedetails($cid='')
  {    
    $getmediatype = \DB::table("curriculum_sections")
                      ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
                      ->where("curriculum_sections.course_id",'=',$cid)->get();

      if(count($getmediatype)>0){
          if(isset($getmediatype['0']->media_type) && $getmediatype['0']->media_type=='0'){
            $mediaid = $getmediatype['0']->media;
            return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_videos', 'course_videos.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)->get();
          }elseif(isset($getmediatype['0']->media_type) && $getmediatype['0']->media_type=='3'){
            $mediaid = $getmediatype['0']->media;
            return \DB::table("curriculum_lectures_quiz")
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)->get();
          }else{
             $mediaid = $getmediatype['0']->media;
             return \DB::table("curriculum_lectures_quiz")
                      ->leftJoin('course_files', 'course_files.id', '=', 'curriculum_lectures_quiz.media')
                      ->where("curriculum_lectures_quiz.media",'=',$mediaid)->get();

          }
     }                      
    
  }
  // get next and previous lecturer id's
  public function getuncompletelecture($cid=''){
    $sections = \DB::table("curriculum_sections")->select('section_id')->where("course_id",'=',$cid)->get();
    $section_id = array();
    foreach ($sections as $value) {
       $section_id[] = $value->section_id;
    } 

    $lectures = \DB::table("curriculum_lectures_quiz")
                  ->where("publish",'=','1')
                  ->whereIn('section_id',$section_id)
                  ->orderBy('section_id')
                  ->orderBy('sort_order')
                  ->pluck('lecture_quiz_id')->toArray();

    $completed = \DB::table("curriculum_lectures_quiz")
                  ->leftJoin('course_progress', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'course_progress.lecture_id')
                  ->where('course_progress.status', '=', '1')
                  ->where('course_progress.user_id', '=', \Auth::user()->id)
                  ->where("curriculum_lectures_quiz.publish",'=','1')
                  ->whereIn('curriculum_lectures_quiz.section_id',$section_id)
                  ->orderBy('curriculum_lectures_quiz.section_id')
                  ->orderBy('curriculum_lectures_quiz.sort_order')
                  ->pluck('curriculum_lectures_quiz.lecture_quiz_id')->toArray();

    $result = array_values(array_diff($lectures, $completed));
   
    return $result;
  }

  public function getalllecture($cid=''){
    $sections = \DB::table("curriculum_sections")->select('section_id')->where("course_id",'=',$cid)->get();
    $section_id = array();
    foreach ($sections as $value) {
       $section_id[] = $value->section_id;
    } 

    $lectures = \DB::table("curriculum_lectures_quiz")
                  ->select("lecture_quiz_id")
                  ->where("publish",'=','1')
                  ->whereIn('section_id',$section_id)
                  ->orderBy('section_id')
                  ->orderBy('sort_order')
                  ->get();
   
    return $lectures;

  }

  public function insertlecturenotes($lid='', $sid='', $duration='', $notes='')
  {
      $loggeduser                      = \Session::get('uid');
      $dataarray                       = array();
      $dataarray['user_id']            = $loggeduser;
      $dataarray['lecture_id']         = $lid;
      $dataarray['section_id']         = $sid;
      $dataarray['notes']              = $notes;
      $dataarray['duration']           = $duration;
      $dataarray['created_at']         = date("Y-m-d H:i:s");
      $dataarray['modified_at']        = date("Y-m-d H:i:s");
      return \DB::table('lectures_notes')->insertGetId($dataarray);
  }

  public function deletenotes($lid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('lectures_notes')->where('note_id', '=', $lid)->where('user_id', '=', $loggeduser)->delete();
  }

  public function getnotes($lid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('lectures_notes')->where('lecture_id', '=', $lid)->where('user_id', '=', $loggeduser)->get();
  }

  public function insertlcomment($lid='', $comments='', $title='', $sid='', $cid='')
  {
     $loggeduser                          = \Session::get('uid');
     $dataarray                           = array();
     $dataarray['user_id']                = $loggeduser;
     $dataarray['course_id']              = $cid;
     $dataarray['section_id']             = $sid;
     $dataarray['lecture_id']             = $lid;
     $dataarray['lecture_comment_title']  = $title;
     $dataarray['lecture_comment']        = $comments;
     $dataarray['created_at']             = date("Y-m-d H:i:s");
     $dataarray['modified_at']            = date("Y-m-d H:i:s");
     return \DB::table('lectures_comments')->insertGetId($dataarray);
  }
  
  public function getlecturecomment($id)
  {
    return \DB::table('lectures_comments')
            ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'lectures_comments.user_id')
            ->where('lectures_comments.lecture_comment_id', '=', $id)
            ->first();
  }

  public function getlecturecommentreply($id)
  {
    return \DB::table('lectures_comment_reply')
            ->select('lectures_comment_reply.*','users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'lectures_comment_reply.user_id')
            ->where('lectures_comment_reply.reply_id', '=', $id)
            ->first();
  }

  public function getlecturecomments($lid='',$cid='',$id='')
  {
    
        $loggeduser  = \Session::get('uid');
        if($loggeduser!=$cid){
             return \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
                ->where('lectures_comments.user_id', '=', $loggeduser)
                ->where('lectures_comments.lecture_id', '=', $lid)
                ->orderBy('lectures_comments.lecture_comment_id', 'desc')->paginate(4);
        }else if($loggeduser==$cid){
            return \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
                ->where('lectures_comments.course_id', '=', $id)
                ->where('lectures_comments.lecture_id', '=', $lid)
                ->orderBy('lectures_comments.lecture_comment_id', 'desc')->paginate(4);
        }
       
  }

  public function insertlcommentreply($commentid='', $lid='', $comments='')
  {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();

     $dataarray['lecture_comment_id'] = $commentid;
     $dataarray['user_id']            = $loggeduser;
     $dataarray['lecture_id']         = $lid;
     $dataarray['reply_comment']      = $comments;
     $dataarray['created_at']         = date("Y-m-d H:i:s");
     $dataarray['modified_at']        = date("Y-m-d H:i:s");
     return \DB::table('lectures_comment_reply')->insertGetId($dataarray);
  }


  public function deletelecturecomments($lid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('lectures_comments')->where('lecture_comment_id', '=', $lid)->where('user_id', '=', $loggeduser)->delete();
  }
  
  public function deletelecturereply($lid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('lectures_comment_reply')->where('reply_id', '=', $lid)->where('user_id', '=', $loggeduser)->delete();
  }

  public function updatelcommentreply($commentid='', $lid='', $comments='', $rid='')
  {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();

     $dataarray['reply_comment']      = $comments;
     $dataarray['modified_at']        = date("Y-m-d H:i:s");
     return \DB::table('lectures_comment_reply')->where('user_id',$loggeduser)->where('lecture_comment_id',$commentid)->where('reply_id',$rid)->update($dataarray);
  }

  public function updatelcomment($lid='', $comments='', $title='', $cid)
  {
     $loggeduser                          = \Session::get('uid');
     $dataarray                           = array();

     $dataarray['lecture_comment_title']  = $title;
     $dataarray['lecture_comment']        = $comments;
     $dataarray['modified_at']            = date("Y-m-d H:i:s");
     return \DB::table('lectures_comments')->where('lecture_comment_id',$cid)->where('user_id',$loggeduser)->update($dataarray);
  }

  public function updatelcoursestaus($cid='', $lid='', $sid='')
  {
    $loggeduser                      = \Session::get('uid');
    $getcount                        = \DB::table("course_progress")
                                     ->where("course_id",'=',$cid)
                                     ->where("lecture_id",'=',$lid)
                                     ->where("user_id",'=',$loggeduser)
                                     ->get();
     $dataarray                      = array();
     if(count($getcount)=='0'){
       $dataarray['course_id']          = $cid;
       $dataarray['user_id']            = $loggeduser;
       $dataarray['lecture_id']         = $lid;
       $dataarray['status']             = $sid;
       $dataarray['created_at']         = date("Y-m-d H:i:s");
       $dataarray['modified_at']        = date("Y-m-d H:i:s");
       return \DB::table('course_progress')->insertGetId($dataarray);
     }else{
       $getid                           = $getcount['0']->progress_id;
       $dataarray['status']             = $sid;
       $dataarray['modified_at']        = date("Y-m-d H:i:s");
       \DB::table('course_progress')->where("progress_id",'=',$getid)->update($dataarray);
	   return $getid;
     }
  }

  public function getcoursedetails($id='')
  {
    $loggeduser     = \Session::get('uid');
    return \DB::table('course')->where('course_id','=', $id)->where('user_id','=', $loggeduser)->get();
  }

  public function getcoursecurriculum($cid='')
  {
    return \DB::table("curriculum_sections")
                      ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.section_id', '=', 'curriculum_sections.section_id')
                      ->where("curriculum_lectures_quiz.description",'!=','')
                       ->where("curriculum_lectures_quiz.publish",'=','1')
                      ->where("curriculum_sections.course_id",'=',$cid)->get();
  }

  public function getsitesettings($option='')
  {
     return \DB::table('sitesettings')->where('option','=', $option)->get();
  }

  public function updatecourse($cid='',$status='')
  {
    $loggeduser         = \Session::get('uid');
    $data               = array();
    $data['approved']   = $status;
    return \DB::table('course')->where('course_id','=', $cid)->where('user_id','=', $loggeduser)->update($data);
  }

  public function getpdffilename($id='')
  {
     return \DB::table('course_files')->where('id','=', $id)->get();
  }

  public function getallcomments($cid='',$parentid=0)
  {
    return \DB::table('course_feedback')
        ->leftJoin('users', 'users.id', '=', 'course_feedback.user_id')
        ->select('course_feedback.feedback_id', 'course_feedback.user_id', 'course_feedback.course_id','course_feedback.parentid', 'course_feedback.comments', 'course_feedback.feedback_type', 'course_feedback.created_at', 'course_feedback.modified_at', 'users.username', 'users.avatar', 'users.email')
        ->where('course_id',$cid)
        ->where('parentid',$parentid)
        ->get();
  }

  public function insertfeedbackreply($commentid='', $courseid='', $comments='')
  {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();
     $dataarray['course_id']          = $courseid;
     $dataarray['user_id']            = $loggeduser;
     $dataarray['comments']           = $comments;
     $dataarray['feedback_type']      = 1;
     $dataarray['parentid']           = $commentid;
     $dataarray['created_at']         = date("Y-m-d H:i:s");
     $dataarray['modified_at']        = date("Y-m-d H:i:s");
     \DB::table('course_feedback')->insertGetId($dataarray);
  }

  public function getallmanageinstructors($cid='')
  {
    return \DB::table('users')
            ->join('manage_instructors', 'users.id', '=', 'manage_instructors.user_id')
            ->where('manage_instructors.course_id','=', $cid)
            ->get();
  }

  public function getpriceinfo($id='')
  {
    $loggeduser                      = \Session::get('uid');
    return \DB::table('instructor')->where('user_id','=', $loggeduser)->get();
  }

  /*
  * process - count taken student
  * @parm - table name
  * @return - data by desc
  */

  public function getCoursecountby($business_user_id=array()){
    $course = \DB::table('course')
              ->leftJoin('course_taken', 'course.course_id', '=', 'course_taken.course_id')
              ->join('users','users.id','=','course.user_id')
              ->select('course.*',\DB::raw('count('.\bsetecHelpers::getdbprefix().'course_taken.course_id) as course_taken'))
              ->where('approved','=','1')
              ->whereNotIn('users.id',$business_user_id)
              ->where('course.privacy','=', '1')
              ->groupBy('course.course_id')
              ->orderBy('course_taken', 'desc')
              ->paginate($this->get_option('paginationPerpage'));
    return $course;
  }
  
   public function getstudents($id='',$type='')
  {
    if($type=='1'){
        return \DB::table('course_taken')
            ->join('users', 'users.id', '=', 'course_taken.user_id')
            ->select('course_taken.taken_id','course_taken.user_id','course_taken.course_id','users.id','users.email','users.username','users.first_name','users.last_name')
            ->where('course_taken.course_id','=', $id)
            ->count();
    }else{
        return \DB::table('course_taken')
            ->join('users', 'users.id', '=', 'course_taken.user_id')
            ->select('course_taken.taken_id','course_taken.user_id','course_taken.course_id','users.id','users.email','users.username','users.first_name','users.last_name')
            ->where('course_taken.course_id','=', $id)
            ->orderBy('course_taken.taken_id', 'asc')
            ->limit(1)
            ->get();
    }
    
  }

  public function getalllecturecomments($id='')
  {
    
      return \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'lectures_comments.lecture_id')
                ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'curriculum_lectures_quiz.sort_order','lectures_comments.course_id')
                ->orderBy('lectures_comments.lecture_comment_id', 'desc')
                ->where('lectures_comments.course_id', '=', $id)->paginate(4);
  }

  public function insertratecomments($description='', $cid='', $rate='',$rateid='')
  {
     $loggeduser                        = \Session::get('uid');
     $dataarray                         = array();
     
     if($rateid=='0'){
         $dataarray['course_id']            = $cid;
         $dataarray['rating']               = $rate;
         $dataarray['user_id']              = $loggeduser;
         if(isset($description)  && $description!=''){
            $dataarray['review_description']   = $description;
         }
         $dataarray['created_at']           = date("Y-m-d H:i:s");
         $dataarray['updated_at']           = date("Y-m-d H:i:s");
         return \DB::table('ratings')->insertGetId($dataarray);
      }elseif (!empty($rateid)) {
           $dataarray['rating']               = $rate;
            if(isset($description)  && $description!=''){
              $dataarray['review_description']   = $description;
           }
           $dataarray['updated_at']           = date("Y-m-d H:i:s");
           \DB::table('ratings')->where('course_id',$cid)->where('user_id',$loggeduser)->where('ratings_id',$rateid)->update($dataarray);
           return $rateid;
      }
  }

  
  public function getCourseInstructor($course_id)
  {
    $course = \DB::table('course')->select('course.user_id')->where('course_id','=', $course_id)->first();
    return $course->user_id;
  }


  public function checkrating($cid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('ratings')->where('course_id',$cid)->where('user_id',$loggeduser)->get();
  }

  public function deletereview($id='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('ratings')->where('ratings_id',$id)->where('user_id',$loggeduser)->delete();
  }

  public function getcoursetaken($id='', $userid='')
  {
    return \DB::table('course_taken')->where('course_id','=', $id)->where('user_id',$userid)->get();
  }

  public function getinstructorinfo($cid='',$type='')
  {
     if($type=='1'){
          return \DB::table('manage_instructors')->where('manage_instructors.course_id', '=', $cid)->count();
     }else{
        return \DB::table('manage_instructors')
                ->join('instructor', 'instructor.user_id', '=', 'manage_instructors.user_id')
                ->where('manage_instructors.course_id', '=', $cid)->count();
    }
  }

  public function insertannouncementcomments($comments='', $cid='')
  { 
     $loggeduser                          = \Session::get('uid');
     $dataarray                           = array();
     $dataarray['user_id']                = $loggeduser;
     $dataarray['course_id']              = $cid;
     $dataarray['announcement']           = $comments;
     $dataarray['created_at']             = date("Y-m-d H:i:s");
     $dataarray['modified_at']            = date("Y-m-d H:i:s");
     return \DB::table('course_announcement')->insertGetId($dataarray);
  }

  public function getannouncement($id='')
  {
    return \DB::table('course_announcement')
            ->join('users', 'users.id', '=', 'course_announcement.user_id')
                ->select('course_announcement.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
            ->where('course_announcement.course_id', '=', $id)->get();
  }

  public function updateannouncementcomments($commentid='', $comments='', $cid)
  {
     $loggeduser                          = \Session::get('uid');
     $dataarray                           = array();
     $dataarray['announcement']           = $comments;
     $dataarray['modified_at']            = date("Y-m-d H:i:s");
     return \DB::table('course_announcement')->where('course_id',$cid)->where('announcement_id',$commentid)->where('user_id',$loggeduser)->update($dataarray);
  }

  public function deleteannoucement($lid='')
  {
    $loggeduser = \Session::get('uid');
    return \DB::table('course_announcement')->where('announcement_id', '=', $lid)->where('user_id', '=', $loggeduser)->delete();
  }

  public function getreviews($id='',$type='')
  {
    if($type=='1'){
      return \DB::table('ratings')
           ->join('users', 'users.id', '=', 'ratings.user_id')
           ->select('ratings.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
           ->where('course_id', '=', $id)->count();
    }else{
      return \DB::table('ratings')
           ->join('users', 'users.id', '=', 'ratings.user_id')
           ->select('ratings.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
           ->where('course_id', '=', $id)->orderBy('ratings.ratings_id', 'asc')->get();
    }
  }

  public function getreviewsall($rateid='', $id='')
  {
     return \DB::table('ratings')
           ->join('users', 'users.id', '=', 'ratings.user_id')
           ->select('ratings.*', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
           ->where('course_id', '=', $id)->where('ratings_id','>',$rateid)->limit(1)->get();
  }

  public function getallstudents($takenid='', $id='')
  {
      return \DB::table('course_taken')
            ->join('users', 'users.id', '=', 'course_taken.user_id')
            ->select('course_taken.taken_id','course_taken.user_id','course_taken.course_id','users.id','users.email','users.username')
            ->where('course_taken.course_id','=', $id)
            ->where('taken_id','>',$takenid)->limit(1)
            ->get();
  }

  public function addwishlist($cid='',$type='')
  { 
     $loggeduser                          = \Session::get('uid');
    if (\DB::table('favorite')->where('course_id', '=', $cid)->where('user_id', '=',$loggeduser)->count() == '1' && $type=='0') {
        \DB::table('favorite')->where('course_id', '=', $cid)->where('user_id', '=',$loggeduser)->delete();
        return true;
    }else{
        
         $dataarray                         = array();
         $dataarray['course_id']            = $cid;
         $dataarray['user_id']              = $loggeduser;
         $dataarray['created_at']           = date("Y-m-d H:i:s");
         $dataarray['updated_at']           = date("Y-m-d H:i:s");
         \DB::table('favorite')->insertGetId($dataarray);
         return true;
    }
  }

  public function getinstructorstatus($cuserid='', $cid='', $type="")
  {
     $loggeduser          = \Session::get('uid');

     if($type=='1'){
        return \DB::table('manage_instructors')->where('course_id', '=', $cid)->where('user_id', '!=',$cuserid)->where('user_id', '=',$loggeduser)->where('instrctor_status', '=','1')->get();
     }else{
        return \DB::table('manage_instructors')->where('course_id', '=', $cid)->where('user_id', '!=',$cuserid)->where('user_id', '=',$loggeduser)->get();
     }
     
  }

  public function updateinstrouctorstatus($cid='', $mid='')
  {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();
     $dataarray['instrctor_status']   = 1;
     $dataarray['modified_at']        = date("Y-m-d H:i:s");
     \DB::table('manage_instructors')->where('course_id',$cid)->where('manage_instrctor_id',$mid)->update($dataarray);
     return true;
  }

	public function getCourseNotificationInfo($cid='',$uid='')
	{
		return \DB::table('course_notification')->where('course_id','=', $cid)->where('user_id','=', $uid)->first();
	}
	
	public  function insertCourseNotificationInfo($data,$id){
	
       $table = 'course_notification';
	   $key = 'id';
	    if($id == NULL )
        {
			 $data['created_at'] = date("Y-m-d H:i:s");	
			 $data['updated_at'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['created_at'])) unset($data['created_at']);	
			if(isset($data['updated_at'])) $data['updated_at'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);
        }    
        return $id;    
	}
	
	public function checkInstructor($cid='',$uid=''){
		if(\DB::table('manage_instructors')->where('course_id','=', $cid)->where('user_id','=', $uid)->first())
			return 1;
		else 
			return 0;
	}
	
	public function deleteCourseTaken($cid='',$uid=''){
		return \DB::table('course_taken')->where('course_id',$cid)->where('user_id',$uid)->delete();
	}
	
	public function getCourseSubscribers($id,$search,$skip)
    {
	  	if(empty($search)){
	  		return \DB::table('course_taken')->leftJoin('users', 'course_taken.user_id', '=','users.id')->where('course_taken.course_id','=', $id)->where('users.active','=','1')->orderBy('taken_id')->skip($skip)->take(12)->get();
	  	} else {
	  		return \DB::table('course_taken')->leftJoin('users', 'course_taken.user_id', '=','users.id')
	  		->where('course_taken.course_id','=', $id)->where('users.active','=','1')
	  		->where(\DB::raw("CONCAT(".\bsetecHelpers::getdbprefix()."users.first_name, ' ', ".\bsetecHelpers::getdbprefix()."users.last_name)"), 'LIKE', '%'.$search.'%')
	  		->orderBy('taken_id')->skip($skip)->take(12)->get();
	  	}
    }

	public function getCourseProgress($cid='',$uid='')
	{
		return \DB::table('course_progress')->where('course_id','=', $cid)->where('user_id','=', $uid)->get();
	}

	public function getCourseCompletedCount($cid='',$uid='')
	{
		return \DB::table('course_progress')->where('course_id','=', $cid)->where('user_id','=', $uid)->where('status','=', '1')->count();
	}

	public function getCourseLectureTotal($cid='')
	{		
		$lecturescount = 0;
		$sections = \DB::table('curriculum_sections')->where('course_id', '=', $cid)->get();
		if(!empty($sections)){
			foreach($sections as $section){
				$lecturescount += \DB::table('curriculum_lectures_quiz')->where('section_id', '=', $section->section_id)->where('publish', '1')->orderBy('sort_order', 'asc')->count();
			}
		}
		return $lecturescount;
	}
	
	public  function insertQuizQuestionRow($data,$id){
	
       $table = 'curriculum_quiz_questions';
	   $key = 'quiz_question_id';
	    if($id == NULL )
        {
			 $data['createdOn'] = date("Y-m-d H:i:s");	
			 $data['updatedOn'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);   
        }    
        return $id;    
	}
    public function courseunpublish($cid='')
  {      
      $data     = array();
      \DB::table('course')->where('course_id',$cid)->update(array('approved'=>'2'));
      \DB::table('manage_instructors')->where('course_id',$cid)->update(array('instrctor_status'=>'0'));
      return true;
  }

  public function getallusersdata($text='')
  { 
      $loggeduser     = \Session::get('uid');

      return \DB::table('users')->where('active', '=' , '1')->where('email', 'like', '%'.$text.'%')->where('id', '!=', $loggeduser)->get();
      
  }

  public function checkinviteuserexists($cid='',$text='')
  {
     return \DB::table('users')
          ->join('invitation_users', 'users.id', '=', 'invitation_users.user_id')
          ->where('users.email','=', $text)
          ->where('invitation_users.course_id','=', $cid)
          ->get();
  }

  public function insertinviterusers($cid='',$text='')
  {
     $loggeduser     = \Session::get('uid');
     $emails = \DB::table('users')->where('active', '=' , '1')->where('email', '=', $text)->where('id', '!=', $loggeduser)->get();
      
     if(count($emails)>0){
       $userid                          = $emails['0']->id;
       $dataarray                       = array();
       $now_date                        = date("Y-m-d H:i:s");
       $dataarray['course_id']          = $cid;
       $dataarray['user_id']            = $userid;
       $dataarray['created_at']         = $now_date;
       $dataarray['modified_at']        = $now_date;
       \DB::table('invitation_users')->insertGetId($dataarray);
       return $emails;
     }
  }

  public function getdeleteinviteusers($cid='',$uid='')
  {
      return \DB::table('invitation_users')->where('course_id', '=', $cid)->where('user_id', '=', $uid)->delete();
  }

  public function getinviterusers($cid='')
  {
    return \DB::table('invitation_users')->where('course_id', '=', $cid)->get();
  }

  public function gettakencourseinfo($cid='')
  {
    return \DB::table('course_taken')
           ->leftJoin('users', 'users.id', '=', 'course_taken.user_id')
           ->leftJoin('course', 'course.course_id', '=', 'course_taken.course_id')
           ->select('course_taken.*', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'course.slug')
           ->where('course_taken.course_id',$cid)
           ->get();
  }

  public function getinviteuserinfo($cid='')
  {
    return \DB::table('invitation_users')
           ->leftJoin('users', 'users.id', '=', 'invitation_users.user_id')
           ->leftJoin('course', 'course.course_id', '=', 'invitation_users.course_id')
           ->select('invitation_users.*', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'course.slug')
           ->where('invitation_users.course_id',$cid)
           ->get();
  }

  public function getprivacystatus($cid='', $loggedid='')
  {
    return \DB::table('invitation_users')->where('course_id',$cid)->where('user_id',$loggedid)->get();
  }

  public function getsearchresults($cid='', $search='', $type='')
  {
      if($type=='1'){
          return \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'lectures_comments.lecture_id')
                ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'curriculum_lectures_quiz.sort_order')
                ->where('lectures_comments.course_id', '=', $cid)->get();
      }else{
          return \DB::table('lectures_comments')
                ->join('users', 'users.id', '=', 'lectures_comments.user_id')
                ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'lectures_comments.lecture_id')
                ->select('lectures_comments.lecture_comment_id','lectures_comments.user_id','lectures_comments.lecture_id','lectures_comments.lecture_comment_title','lectures_comments.lecture_comment','lectures_comments.created_at','lectures_comments.modified_at', 'users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name', 'curriculum_lectures_quiz.sort_order')
                ->where('lectures_comments.course_id', '=', $cid)
                ->where(function($query) use ($search) {
                      $query->where('lectures_comments.lecture_comment_title', 'like', '%'.$search.'%')
                            ->orWhere('lectures_comments.lecture_comment', 'like', '%'.$search.'%');
                })
                ->get();
      }
       
  }

  public function getCoursecompleteStatus($lecture_id){
    return \DB::table('course_progress')->where('lecture_id', $lecture_id)->where('user_id',\Auth::user()->id)->pluck('status')->toArray();
  }

  public function getCoursecompletedStatus($lecture_id){
    return \DB::table('course_progress')->where('lecture_id', $lecture_id)->where('user_id',\Auth::user()->id)->where('status',1)->count();
  }
  
  public function insertauthor($cid='')
  {
      $loggeduser                      = \Session::get('uid');
      $dataarray                       = array();
      $dataarray['course_id']          = $cid;
      $dataarray['user_id']            = $loggeduser;
      $dataarray['created_at']         = date("Y-m-d H:i:s");
      $dataarray['modified_at']        = date("Y-m-d H:i:s");
      return  \DB::table('manage_instructors')->insertGetId($dataarray);
  }
  
  public function getquizquestions($qid='')
  {
    return \DB::table('curriculum_quiz_questions')->where('quiz_id',$qid)->orderBy('sort_order', 'asc')->get();
  }
  
  public  function insertQuizResult($data,$id){
	
       $table = 'curriculum_quiz_results';
	   $key = 'result_id';
	    if($id == NULL )
        {
			 $data['createdOn'] = date("Y-m-d H:i:s");	
			 $data['updatedOn'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);
        }    
        return $id;    
	}	
  
  public function getQuizAnswer($qid='')
  {
    return \DB::table('curriculum_quiz_questions')->select('question_type','correct_option','options')->where('quiz_question_id',$qid)->first();
  }
  
  public function clearquizresultretake($lid='')
  {
    $loggeduser = \Session::get('uid');
    $dataarray = array();
	$dataarray['user_option'] = '0';
	$dataarray['status'] = '2';
	$dataarray['complete'] = '0';
	return \DB::table('curriculum_quiz_results')->where("quiz_id",'=',$lid)->where("user_id",'=',$loggeduser)->update($dataarray);
  }
  
  public function getaquizquestion($qid='')
  {
    return \DB::table('curriculum_quiz_questions')->where('quiz_question_id',$qid)->first();
  }
  
  public function getquizrecords($lid='')
  {
    return \DB::table('curriculum_quiz_results')->where('quiz_id',$lid)->orderBy('result_id', 'asc')->get();
  }

  /*
  * @process - fillter process for course sidebar
  */
  public function getfreecourses()
  {   

     $business_user = \bsetecHelpers::getBusinessuser();
    return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
    ->select('course.*')
    ->where('course.pricing', '=', '0')
    ->where('course.approved','=','1')
    ->where('course.privacy','=', '1')
    ->whereNotIn('users.id',$business_user)
    ->groupBy('course.course_id')
    ->orderBy('course.created_at', 'desc')
    ->paginate($this->get_option('paginationPerpage'));

  }

  public function gettopfreecourses()
  { 

    $business_user = \bsetecHelpers::getBusinessuser();  
    return \DB::table('course')
          ->leftJoin('users', 'users.id', '=', 'course.user_id')
          ->leftJoin('ratings','ratings.course_id','=','course.course_id')
          ->select('course.*',\DB::raw('sum('.\bsetecHelpers::getdbprefix().'ratings.rating) as ratings'))
          ->where('course.approved', '=', '1')
          ->where('course.pricing', '=', '0')
          ->where('course.privacy', '=', '1')
          ->whereNotIn('users.id',$business_user)
          ->groupBy('course.course_id')
          ->orderBy('ratings', 'desc')
          ->paginate($this->get_option('paginationPerpage'));
  } 

  public function gettopratedcourses($business_user_id=array())
  {   
    return \DB::table('course')
          ->leftJoin('users', 'users.id', '=', 'course.user_id')
          ->join('ratings','ratings.course_id','=','course.course_id')
          ->select('course.*',\DB::raw('sum('.\bsetecHelpers::getdbprefix().'ratings.rating) as ratings'))
          ->where('course.approved', '=', '1')
          ->where('course.privacy', '=', '1')
          ->whereNotIn('users.id',$business_user_id)
          ->groupBy('ratings.course_id')
          ->orderBy('ratings', 'desc')
          ->paginate($this->get_option('paginationPerpage'));
  }

  public function gettoppaidcourses()
  {   
    $business_user = \bsetecHelpers::getBusinessuser(); 
    return \DB::table('course')
    ->leftJoin('users', 'users.id', '=', 'course.user_id')
    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
    // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
    ->select('course.*')
    ->where('course.approved', '=', '1')
    ->where('course.pricing', '!=', '0')
    ->groupBy('course.course_id')
    ->where('course.privacy','=', '1')
     ->whereNotIn('users.id',$business_user)
    ->orderBy('course.pricing', 'desc')
    ->orderBy('course.created_at', 'desc')
    ->paginate($this->get_option('paginationPerpage'));

  }

  public function getvaliduser($id='')
  { 
    $loggeduser                      = \Session::get('uid');
    return \DB::table('course')->where('course_id', '=', $id)->where('user_id','=',$loggeduser)->get();
  }

  public function getallsubscribers($id='')
  {
    return \DB::table('course_taken')
            ->join('users', 'users.id', '=', 'course_taken.user_id')
            ->select('course_taken.taken_id','course_taken.user_id','course_taken.course_id','users.id','users.email','users.username')
            ->where('course_taken.course_id','=', $id)
            ->orderBy('course_taken.taken_id', 'asc')
            ->get();
  }

  public function getcourseid($lid='')
  {
    return \DB::table('curriculum_lectures_quiz')
            ->join('curriculum_sections', 'curriculum_sections.section_id', '=', 'curriculum_lectures_quiz.section_id')
            ->select('curriculum_sections.course_id')
            ->where('curriculum_lectures_quiz.lecture_quiz_id','=', $lid)
            ->get();
  }

  public function getalluserdetails($cid='',$type='')
  {
    if($type=='1'){
      $results = \DB::table('manage_instructors')->where('course_id', '=', $cid)->select('user_id')->get();  
      $userarray = array();
      if(count($results)>0){
        foreach ($results as $uservalue) {
          $userarray[] = $uservalue->user_id;
        }
      }
      return $userarray;
    }elseif ($type=='2') {
      $results = \DB::table('course_taken')->where('course_id', '=', $cid)->select('user_id')->get(); 
      $userarray = array();
      if(count($results)>0){
        foreach ($results as $uservalue) {
          $userarray[] = $uservalue->user_id;
        }
      }
      return $userarray;
    }
  }

  public function getcourseuserid($cid='')
  {
    $getmanageinstructor = $this->getalluserdetails($cid,1);
    $gettakencourse      = $this->getalluserdetails($cid,2);
    $merge               = array_merge($getmanageinstructor,$gettakencourse);
    return \DB::table('users')->whereNotIn('id', $merge)->select('id','email','first_name','last_name','username')->get();
  }
  
  public function getcourseuser($cid){
    return $user_id=\DB::table('course')->where('course_id','=',$cid)->select('user_id')->first();
  }

  public function lastlectureid($lastid){
    return  \DB::table('lectures_comments')
                  ->join('curriculum_lectures_quiz', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'lectures_comments.lecture_id')
                  ->select('curriculum_lectures_quiz.sort_order','curriculum_lectures_quiz.type')
                  ->where('lectures_comments.lecture_comment_id','=',$lastid)->first();
    } 

        // get notes for mobile
  public function getnotesmob($lid='',$uid='')
  {
    return \DB::table('lectures_notes')->where('lecture_id',$lid)->where('user_id', $uid)->get();
  }

  public function getVideobyid($video_id=''){
    return \DB::table('curriculum_lectures_quiz')
                ->join('course_videos','course_videos.id','=','curriculum_lectures_quiz.media')
                ->where('curriculum_lectures_quiz.lecture_quiz_id',$video_id)->first();
  }

  // youtube features code
  public function inserety_id($y_id='',$cid='')
    {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();
     $dataarray['youtube_id']      = $y_id;
     return \DB::table('course')->where('course_id','=', $cid)->where('user_id','=', $loggeduser)->update($dataarray);

    }

    public function inseretypri_id($y_id='',$cid='')
    {
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();
     $dataarray['youtube_preview_id']      = $y_id;
     return \DB::table('course')->where('course_id','=', $cid)->where('user_id','=', $loggeduser)->update($dataarray);


    }

     public function inseretyoucur_id($y_id='',$cid='')
    {

      /*pradeep*/
     $loggeduser                      = \Session::get('uid');
     $dataarray                       = array();
     $dataarray['file_name']      = $y_id;
     $dataarray['file_title']      = $y_id;
     $dataarray['file_type']      = 'youtube';
     $dataarray['file_size']      = '';
     $dataarray['duration']      = '';
     $dataarray['file_tag']      = '';
     $dataarray['uploader_id']      =  $loggeduser ;
     $dataarray['processed']      = '1';
     $dataarray['created_at']      = date("Y-m-d H:i:s");
     $dataarray['updated_at']      = date("Y-m-d H:i:s");

     $result =  \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id','=', $cid)->where('youtube','!=', '')->get();

     if(count($result)=='0'){
         $up_id = \DB::table('course_files')->insertGetId($dataarray);

         $data                  = array();
         $data['youtube']      = $up_id;
         $data['youtube_id']      = $y_id;
         $data['media_type']      = 6;

         return \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id','=', $cid)->update($data);

     }
     else{

         \DB::table('course_files')->where('id','=', $result[0]->youtube)->update($dataarray);

         $data                  = array();
         $data['youtube']      = $result[0]->youtube;
         $data['youtube_id']      = $y_id;
         $data['media_type']      = 6;

         return \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id','=', $cid)->update($data);

     }

    }
    
	  public function getwebminaruser($user_id='',$cid=null)
  {
    // echo $user_id.$cid;exit();
    return \DB::table('course_online')->where('user_id',$user_id)->where('course_id',$cid)->first();
  }

  public function getcoursetype($cid)
  {
    $course = \DB::table('course')->where('course_id',$cid)->select('course_type')->get();
    foreach ($course as $key) {
        $courseid=$key->course_type;
    }
    return $courseid;
  }

  public function getorganizerkey($uid)
  {
    $organizerkey=\DB::table('citrix')->where('user_id','=',$uid)->select('organizer_key','access_token')->first();
    if($organizerkey != ''){
      return  array('organizerkey' => $organizerkey->organizer_key , 
        'access_token' => $organizerkey->access_token);
    }
    else
    {
      return '';
    }
  }

  public function createmeeting($data,$course_id) {
    $meet_id = \DB::table('online_meetings')->insertGetId($data);
    if(!empty($meet_id)){
      \DB::table('course')->where('course_id',$course_id)->update(['meeting_id' => $meet_id]);
      return true;
    } else {
      return false;
    }
  }

  public function getmeetinginfo($id='') {
    return \DB::table('online_meetings')->where('meeting_id','=', $id)->first();
  }

  public function checkTransactions($uid='', $cid=''){
    return \DB::table('transactions')
        ->join('course_taken','course_taken.user_id','=','transactions.user_id')
        ->where('course_taken.course_id','=',$cid)
        ->where('transactions.course_id', '=', $cid)
        ->where('transactions.user_id', '=', $uid)
        ->where('transactions.status','=','completed')
        ->get();
  }

  public function getCompleteQuiz($lqid='') {
    return \DB::table('curriculum_lectures_quiz')->where('lecture_quiz_id','=', $lqid)->pluck('certificate_answer')->toArray();
  }

}
