<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class banner extends bsetec  {
	
	protected $table = 'banner';
	protected $primaryKey = 'banner_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."banner.* FROM ".\bsetecHelpers::getdbprefix()."banner  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."banner.banner_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
