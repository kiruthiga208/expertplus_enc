<?php
namespace App\Models;
use App\Models\Course;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Users extends bsetec
{
       
    public function getcourse($id='')
    {
        return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
                    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                    // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                    ->where('course.user_id','=', $id)
                    // ->groupBy('course.course_id')
                    ->orderBy('course.created_at', 'desc')
                    ->paginate('100');
    }

    public function getmycourse($id='',$fillter ='')
    {
        $taken                      = \DB::table('course_taken')->where('user_id', '=', $id)->get();
        $takenCourse                = array();
        foreach ($taken as $key => $value) {
            if($fillter == "completed"){
                $course = new Course;
                $lect_total = $course->getCourseLectureTotal($value->course_id);
                $comp_total = $course->getCourseCompletedCount($value->course_id,$id);
                if($lect_total == $comp_total)
                   $takenCourse[]          = $value->course_id;
            }else{
                $takenCourse[]          = $value->course_id;
            }
        }
        
        if($takenCourse){
            $mycourse               = \DB::table('course')
                                        ->leftJoin('users', 'users.id', '=', 'course.user_id')
                                        ->leftJoin('featured', 'featured.course_id', '=', 'course.course_id')
                                        ->select('course.*','users.id','featured.featured_id as featured')
                                        ->whereIn('course.course_id',$takenCourse)
                                        ->where('course.approved','=','1')
                                        ->groupBy('course.course_id')
                                        ->orderBy('course.created_at', 'desc')
                                        ->paginate('100');
        }else{
            $mycourse               = array();
        }

        return $mycourse;
    }

    public function getmyusers($id='')
    {
        return \DB::table('users')
                ->where('users.id', '=', $id)
                ->first();
    }

    public function getmyfavorites($id='')
    {
       return \DB::table('users')
                    ->join('favorite', 'favorite.user_id', '=','users.id')
                    ->join('course', 'course.course_id', '=','favorite.course_id')
                    ->where('users.id', '=', $id)->where('course.approved','=','1')->get();
    }

    public function userinfo($id='')
    {
        return \DB::table('users')
                    ->select('users.id',
                            'users.first_name',
                            'users.last_name',
                            'users.designation',
                            'users.biography',
                            'users.avatar',
                            'course_images.image_type'
                            )
                    ->leftJoin('course_images', 'course_images.id', '=','users.avatar')
                    ->where('users.id', '=', $id)->first();
    }

    public function coursecount($id='')
    {
        return \DB::table('course')
                    ->where('deleted_at', '=', NULL)->where('approved','=','1')
                    ->where('user_id', '=', $id)->count();
    }

    public function studentscount($id='')
    {
        return \DB::table('course_taken')
                    ->leftJoin('course', 'course.course_id', '=','course_taken.course_id')
                    ->where('course.user_id', '=',$id)
                    ->count();
    } 

    public function mycourses($id='', $offest = 0)
    {
        return \DB::table('course')
                    ->select(
                            'course.course_id',
                            'course.slug',
                            'course.description',
                            'course.course_title',
                            'course.pricing',
                            'course.user_id',
                            'course.image', 
                            'users.first_name',
                            'users.last_name',
                            'users.username',
                            'users.avatar',
                            'course_images.image_type'
                            )
                    ->leftJoin('users', 'users.id', '=','course.user_id')
                    ->leftJoin('course_images', 'course_images.id', '=','users.avatar')
                    ->where('course.cat_id', '!=', "NULL")
                    ->where('course.user_id', '=', $id)
                    ->where('course.deleted_at', '=', NULL)
                    ->where('course.approved','=','1')
                    ->limit(4)
                    ->offset($offest)
                    ->get();
    }   

    public function courseratings($id='')
    {
        return \DB::table('ratings')
                    ->leftJoin('course', 'course.course_id', '=','ratings.course_id')
                    ->where('course.user_id', '=',$id)
                    ->count();
      
    }

    public function userData($table,$column){
        $data_count = \DB::table($table)->where($column,\Auth::user()->id)->count();
        if($data_count>0){
            return  \DB::table($table)->where($column,\Auth::user()->id)->delete();
         }
    }


    //  added for custom course request
    public function gettutors($uid=1,$type='',$data='',$sort='',$page=1,$take=10) {
        $skip = $take * ($page - 1);
        $wherequery = ' 1=1 ';
        if($type == '1'){
            if(!empty($data)){
                $wherequery = " CONCAT(users.first_name,' ',users.last_name) LIKE '".$data."'";
                $keywords = explode(' ',$data);
                $pquery = '';
                foreach ($keywords as $key => $keyword) {
                    $pquery .= " OR custom_user_settings.skills LIKE '".$keyword."' OR users.first_name LIKE '".$keyword."' OR users.last_name LIKE '".$keyword."'";
                }//OR categories.name REGEXP '".$keyword."'
                $wherequery = $wherequery . $pquery;
            }
        } else if($type == '2'){
            $wherequery = "course.cat_id ='".$data."'";
        } else if($type == '3'){
            $search = explode('&', $data);
            $price_from = str_replace('price_from=', '', $search[0]);
            $price_to = str_replace('price_to=', '', $search[1]);
            $wherequery = ' custom_user_settings.hourly_rate BETWEEN '.$price_from.' AND '.$price_to;
        }

        $order = 'ratings desc';
        if($sort == '2'){
            $order = 'ratings asc';
        } else if($sort == '3'){
            $order = 'custom_user_settings.hourly_rate desc';
        } else if($sort == '4'){
            $order = 'custom_user_settings.hourly_rate asc';
        }

        $total = \DB::table('users')
            // ->leftjoin('course', 'users.id', '=', 'course.user_id')
            ->leftjoin('custom_user_settings', 'users.id', '=', 'custom_user_settings.user_id')
            ->leftjoin('custom_reviews', 'users.id', '=', 'custom_reviews.tutor_id')
            // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
            ->distinct()->select('users.id','users.email','users.username','users.first_name','users.last_name','users.biography','custom_user_settings.hourly_rate','custom_user_settings.skills', \DB::raw ('AVG('.\bsetecHelpers::getdbprefix().'custom_reviews.rating) as ratings'))
            // ->where('course.approved', '=', 1)
            ->where('users.active', '=', '1')
            // ->where('users.active_tutor', '=', 1)
            ->where('users.user_type', '=', 'tutor')
            ->where('users.id', '!=', $uid)
            ->where(function ($query) use ($wherequery) {
                $query->whereRaw($wherequery);
            })
            // ->toSql();
              ->count();
         
        $rows = \DB::table('users')
            // ->leftjoin('course', 'users.id', '=', 'course.user_id')
            ->leftjoin('custom_user_settings', 'users.id', '=', 'custom_user_settings.user_id')
            ->leftjoin('custom_reviews', 'users.id', '=', 'custom_reviews.tutor_id')
            // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
            ->distinct()->select('users.id','users.email','users.username','users.first_name','users.last_name','users.biography','custom_user_settings.hourly_rate','custom_user_settings.skills', \DB::raw ('AVG('.\bsetecHelpers::getdbprefix().'custom_reviews.rating) as ratings'), \DB::raw ('(select count('.\bsetecHelpers::getdbprefix().'custom_reviews.tutor_id) FROM '.\bsetecHelpers::getdbprefix().'custom_reviews WHERE '.\bsetecHelpers::getdbprefix().'custom_reviews.tutor_id = '.\bsetecHelpers::getdbprefix().'users.id) as totalratings'))
            // ->where('course.approved', '=', 1)
            ->where('users.active', '=', '1')
            ->where('users.active_tutor', '=', '1')
            ->where('users.user_type', '=', 'tutor')
            ->where('users.id', '!=', $uid)
            ->where(function ($query) use ($wherequery) {
                $query->whereRaw($wherequery);
            })
            ->skip($skip)->take($take)
            ->groupBy ('users.id')
            ->orderByRaw($order)
            ->get();
             // echo $rows; exit;
        return array('rows'=>$rows, 'total'=>$total);
    }  

    public function getskills($find)
    {
        return \DB::table('skills')->select('name as id','name as text')->whereRaw('name LIKE "'.$find.'"')->orderBy('name')->take(4)->get();
    }

    public function getselectedtutors($find)
    {
        return \DB::table('users')->selectRaw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name, " - ", '.\bsetecHelpers::getdbprefix().'users.username) AS id, CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name, " - ", '.\bsetecHelpers::getdbprefix().'users.username) as text')->where('user_type', '=', 'tutor')->whereRaw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " " ,'.\bsetecHelpers::getdbprefix().'users.last_name, " - ", '.\bsetecHelpers::getdbprefix().'users.username) LIKE "'.$find.'"')->orderBy('first_name')->take(4)->get();
    }

    public function addnewskill($skill)
    {
        $result = \DB::table('skills')->where('name','=', $skill)->first();
        if(empty($result)){
            $skills['name'] = $skill;
            \DB::table('skills')->insert($skills);
        }
    }

    public function getpublishedcourse($id=''){

        return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
                    // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                    // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                    // ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                    ->where('course.user_id','=', $id)
                    ->where('course.approved','=','1')
                    // ->groupBy('course.course_id')
                    ->orderBy('course.created_at', 'desc')
                    ->paginate('100');
    }

    public function getPremiumInstructors() {
        return \DB::table('manage_instructors')->groupBy('user_id')->paginate('10');
    }

    public function premiumInstructorApproval($id, $type) {
        return \DB::table('manage_instructors')->where('user_id', $id)->update(['instrctor_status' => $type]);
    }
 
}
