<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class CourseSurvey extends bsetec  {
	
	protected $table = 'course_survey';
	protected $primaryKey = 'course_survey_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT 
		".\bsetecHelpers::getdbprefix()."course_survey.*,
		".\bsetecHelpers::getdbprefix()."course_survey.user_id AS userid,
		CONCAT(".\bsetecHelpers::getdbprefix()."users.first_name, ' ', ".\bsetecHelpers::getdbprefix()."users.last_name) AS user_id,
		".\bsetecHelpers::getdbprefix()."course.course_title AS course_title,
		".\bsetecHelpers::getdbprefix()."language.language AS language,
		".\bsetecHelpers::getdbprefix()."categories.name AS category
		FROM ".\bsetecHelpers::getdbprefix()."course_survey 
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."users.id = ".\bsetecHelpers::getdbprefix()."course_survey.user_id
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."course ON ".\bsetecHelpers::getdbprefix()."course.course_id = ".\bsetecHelpers::getdbprefix()."course_survey.course_id
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."language ON ".\bsetecHelpers::getdbprefix()."language.id = ".\bsetecHelpers::getdbprefix()."course_survey.language_id
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."categories ON ".\bsetecHelpers::getdbprefix()."categories.id = ".\bsetecHelpers::getdbprefix()."course_survey.category_id
		";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."course_survey.course_survey_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}

	public static function getsearchColumn($column){
		return \DB::table('course_survey')
				->select('course_survey.*','course_survey.user_id AS userid',\DB::raw('CONCAT('.\bsetecHelpers::getdbprefix().'users.first_name, " ", '.\bsetecHelpers::getdbprefix().'users.last_name) AS user_id'),'course.course_title AS course_title','language.language AS language','categories.name AS category')
				->leftJoin('users', 'users.id', '=', 'course_survey.user_id')
				->leftJoin('course', 'course.course_id', '=', 'course_survey.course_id')
				->leftJoin('language', 'language.id', '=', 'course_survey.language_id')
				->leftJoin('categories', 'categories.id', '=', 'course_survey.category_id')
				->groupBy('course_survey.'.$column)
				->get();
	}
}
