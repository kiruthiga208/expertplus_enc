<?php
namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
class Blog extends bsetec  {
	
	protected $table = 'blogs';
	protected $primaryKey = 'blogID';

	public function __construct() {
		parent::__construct();
		
	}
	
	public static function getRows( $args )
	{

        extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> '1'	  
        ), $args ));
		
		$offset = ($page-1) * $limit ;	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';	
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = with(new static)->table;
		
        
		$rows = array();
	    $result = \DB::select( self::querySelect() . self::queryWhere(). "
				{$params} ". self::queryGroup() ." {$orderConditional}  {$limitConditional} ");
				
		$total = \DB::select( self::querySelect() . self::queryWhere()." {$params} ". self::queryGroup());			 


		return $results = array('rows'=> $result , 'total' => count($total));	

	
	}	

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."blogs.* ,".\bsetecHelpers::getdbprefix()."blogcategories.*, ".\bsetecHelpers::getdbprefix()."users.username as uname ,CONCAT(first_name,' ',last_name) as username ,count(".\bsetecHelpers::getdbprefix()."blogcomments.commentID) as comments
		FROM ".\bsetecHelpers::getdbprefix()."blogs 
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."blogcategories ON ".\bsetecHelpers::getdbprefix()."blogcategories.catID = ".\bsetecHelpers::getdbprefix()."blogs.catID 
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."blogcomments ON ".\bsetecHelpers::getdbprefix()."blogcomments.blogID = ".\bsetecHelpers::getdbprefix()."blogs.blogID 
		LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON ".\bsetecHelpers::getdbprefix()."blogs.entryby = ".\bsetecHelpers::getdbprefix()."users.id 
		
		"; 
	}
	public static function queryWhere(  ){
		
		return " WHERE ".\bsetecHelpers::getdbprefix()."blogs.blogID IS NOT NULL AND status = 'publish' AND enable ='1' ";
	}
	
	public static function queryGroup(){
		return " GROUP BY ".\bsetecHelpers::getdbprefix()."blogs.blogID ";
	}
	
	public static function getRowBlog( $slug )
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;

		$result = \DB::select( 
				self::querySelect() . 
				self::queryWhere().
				" AND slug = '{$slug}' ". 
				self::queryGroup()
			);	
		if(count($result) <= 0){
			$result = array();		
		} else {

			$result = $result[0];
		}
		return $result;		
	}		
	
	public static function summaryCategory()
	{
		$result = \DB::select("
		 SELECT 
			".\bsetecHelpers::getdbprefix()."blogcategories.* ,COUNT(".\bsetecHelpers::getdbprefix()."blogs.blogID) AS total
		 FROM ".\bsetecHelpers::getdbprefix()."blogcategories 
		 LEFT JOIN ".\bsetecHelpers::getdbprefix()."blogs ON ".\bsetecHelpers::getdbprefix()."blogs.catID = ".\bsetecHelpers::getdbprefix()."blogcategories.catID
		 where ".\bsetecHelpers::getdbprefix()."blogcategories.enable='1' GROUP BY ".\bsetecHelpers::getdbprefix()."blogcategories.catID 
		");
		return $result;	 
	}	
	
	public static function clouds()
	{
		$result = \DB::select(" SELECT tags FROM ".\bsetecHelpers::getdbprefix()."blogs	");
		$tags = array();
		foreach($result as $row)
		{
			foreach(explode(",",$row->tags) as $tg)
			{
				$item = trim(strtolower($tg));
				if(!in_array($item,$tags))
				{
					$tags[ $item]	= str_replace(" ","+",$item);
				}	
			}
		}
		
		return $tags;	 	
	}
	
	public static function recentPosts()
	{
		$result = \DB::select(" SELECT * FROM ".\bsetecHelpers::getdbprefix()."blogs ORDER BY created DESC LIMIT 5");
		return $result;
	}	
	
	public static function getComments( $blogID )
	{
		$result = \DB::select(
			" SELECT ".\bsetecHelpers::getdbprefix()."blogcomments.* , ".\bsetecHelpers::getdbprefix()."users.username,CONCAT(first_name,' ',last_name) as name  FROM ".\bsetecHelpers::getdbprefix()."blogcomments LEFT JOIN ".\bsetecHelpers::getdbprefix()."users ON  ".\bsetecHelpers::getdbprefix()."blogcomments.user_id = ".\bsetecHelpers::getdbprefix()."users.id  WHERE blogID ='{$blogID}'"
		);
		$comments = array();
		foreach($result as $row)
		{
			//$comments[] = (array) $row;
			
			$nested = \DB::select(" SELECT ".\bsetecHelpers::getdbprefix()."blogcomments.* FROM ".\bsetecHelpers::getdbprefix()."blogcomments WHERE parentID ='{$row->commentID}'" );
			$childs = array();
			foreach($nested as $row2)
			{
					$childs[] = (array) $row2;
			}
			$comments[] =  array_merge((array) $row ,array('nested' => $childs)) ; 
		}
		//echo '<pre>';print_r($comments);echo '</pre>';exit;
		return $comments;	
	}	
}
