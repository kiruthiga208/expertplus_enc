<?php
/**
 * Company : Bsetec
 * Models : Categories
 * Email : support@bsetec.com
 */
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
class Categories extends bsetec
{
    public function __construct() {
        parent::__construct();
        
    }

    public function getcategories($category,$subcategory='')
    {
        $business_user = \bsetecHelpers::getBusinessuser();

        if(empty($subcategory)){
    	   $cat_id = \DB::table('categories')->where('slug', '=', str_slug($category))->limit(1)->get();
            foreach ($cat_id as $value) {
               $id = $value->id;
            }
            return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
                        // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                        // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                        ->leftJoin('categories', 'categories.id', '=', 'course.cat_id')
                        ->select('course.*')
                        ->where('course.cat_id', '=', $id)->where('course.deleted_at', '=', NULL)->where('course.approved','=','1')
                        ->where('course.privacy','=', '1')
                        ->whereNotIn('users.id',$business_user)
                        ->orderBy('course.created_at', 'desc')
                        ->paginate($this->get_option('paginationPerpage'));
        } else {
           $sub_cat_id = \DB::table('expert_sub_categories')->where('sub_slug', '=', str_slug($subcategory))->limit(1)->get();
            foreach ($sub_cat_id as $value) {
               $id = $value->sub_cat_id;
            }
            return \DB::table('course')->leftJoin('users', 'users.id', '=', 'course.user_id')
                        // ->leftJoin('comments', 'comments.user_id', '=', 'course.user_id')
                        // ->leftJoin('favorite', 'favorite.user_id', '=', 'course.user_id')
                        ->leftJoin('expert_sub_categories', 'expert_sub_categories.sub_cat_id', '=', 'course.sub_cat_id')
                        ->select('course.*')
                        ->where('course.sub_cat_id', '=', $id)->where('course.deleted_at', '=', NULL)->where('course.approved','=','1')
                        ->where('course.privacy','=', '1')
                        ->whereNotIn('users.id',$business_user)
                        ->orderBy('course.created_at', 'desc')
                        ->paginate($this->get_option('paginationPerpage'));
        }

    }

    public function getcategroeiscount($category)
    {
        return \DB::table('categories')->where('slug', '=', str_slug($category))->count() ;
    }

    public function getsubcategroeiscount($subcategory='',$type='0')
    {
        if($type='1'){
           return \DB::table('expert_sub_categories')->where('sub_slug', '=', str_slug($subcategory))->limit(1)->get();

        }else{
             return \DB::table('expert_sub_categories')->where('sub_slug', '=', str_slug($subcategory))->count() ;
        }

    }

}