<?php
/**
 * Created by Balaji
 * Company : BSEtec
 * Model : Category
 * Email : support@bsetec.com
 */
class Sitecategories extends Eloquent
{
    protected $table = 'categories';
    protected $primaryKey = 'id';


    public function categories()
    {
        return $this->belongsTo('Course' ,'cat_id');
    }

}