<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class admincoupon extends bsetec  {
	
	protected $table = 'coupon_discount';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."coupon_discount.* FROM ".\bsetecHelpers::getdbprefix()."coupon_discount  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."coupon_discount.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
