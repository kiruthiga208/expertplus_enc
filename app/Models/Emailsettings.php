<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Emailsettings extends bsetec  {
	
	protected $table = 'email_settings';
	protected $primaryKey = 'setting_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT ".\bsetecHelpers::getdbprefix()."email_settings.* FROM ".\bsetecHelpers::getdbprefix()."email_settings  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE ".\bsetecHelpers::getdbprefix()."email_settings.setting_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
