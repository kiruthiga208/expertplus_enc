<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class bsetec extends Model {


	public static function getRows( $args )
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;
	   
        extract( array_merge( array(
			'page' 		=> '0' ,
			'limit'  	=> '0' ,
			'sort' 		=> '' ,
			'order' 	=> '' ,
			'params' 	=> '' ,
			'global'	=> 1	  
        ), $args ));
		
		$offset = ($page-1) * $limit ;	
		$limitConditional = ($page !=0 && $limit !=0) ? "LIMIT  $offset , $limit" : '';	
		$orderConditional = ($sort !='' && $order !='') ?  " ORDER BY {$sort} {$order} " : '';

		// Update permission global / own access new ver 1.1
		$table = with(new static)->table;

		//if($global == 0 )
			//	$params .= " AND {$table}.entry_by ='".\Session::get('uid')."'"; 	
		// End Update permission global / own access new ver 1.1			
        
	    $result = \DB::select( self::querySelect() . self::queryWhere(). " 
				{$params} ". self::queryGroup() ." {$orderConditional}  {$limitConditional} ");

		if($key =='' ) { $key ='*'; } else { $key = \bsetecHelpers::getdbprefix().$table.".".$key ; }	
		$counter_select = preg_replace( '/[\s]*SELECT(.*)FROM/Usi', 'SELECT count('.$key.') as total FROM', self::querySelect() ); 	
		
		$res = \DB::select( $counter_select . self::queryWhere()." {$params} ". self::queryGroup());	
	
		if(isset($res[0]->total))
		{
			$total = $res[0]->total;
		}
		else
		{
			$total = false;
		}


		return $results = array('rows'=> $result , 'total' => $total);	

	
	}	

	public static function getRow( $id )
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;

		$result = \DB::select( 
				self::querySelect() . 
				self::queryWhere().
				" AND ".\bsetecHelpers::getdbprefix().$table.".".$key." = '{$id}' ". 
				self::queryGroup()
			);	
		if(count($result) <= 0){
			$result = array();		
		} else {

			$result = $result[0];
		}
		return $result;		
	}	

	public  function insertRow( $data , $id)
	{
       $table = with(new static)->table;
	   $key = with(new static)->primaryKey;
	    if($id == NULL )
        {
			
            // Insert Here 
			if(isset($data['createdOn'])) $data['createdOn'] = date("Y-m-d H:i:s");	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");	
			 $id = \DB::table( $table)->insertGetId($data);				
            
        } else {
            // Update here 
			// update created field if any
			if(isset($data['createdOn'])) unset($data['createdOn']);	
			if(isset($data['updatedOn'])) $data['updatedOn'] = date("Y-m-d H:i:s");			
			 \DB::table($table)->where($key,$id)->update($data);    
        }    
        return $id;    
	}			

	static function makeInfo( $id )
	{	
		$row =  \DB::table('module')->where('module_name', $id)->get();
		$data = array();
		foreach($row as $r)
		{
			$data['id']		= $r->module_id; 
			$data['title'] 	= $r->module_title; 
			$data['note'] 	= $r->module_note; 
			$data['table'] 	= $r->module_db; 
			$data['key'] 	= $r->module_db_key;
			$data['config'] = \SiteHelpers::CF_decode_json($r->module_config);
			$field = array();	
			foreach($data['config']['grid'] as $fs)
			{
				foreach($fs as $f)
					$field[] = $fs['field']; 	
									
			}
			$data['field'] = $field;	
					
		}
		return $data;
			
	
	} 

    static function getComboselect( $params , $limit =null)
    {   

        $limit = explode(':',$limit);
        if(count($limit) >=3)
        {
            $table = $params[0]; 
            $condition = $limit[0]." `".$limit[1]."` ".$limit[2]." ".$limit[3]." "; 
            $row =  \DB::select( "SELECT * FROM ".$table." ".$condition);
        }else{
            $table = $params[0]; 
            $row =  \DB::table($table)->get();
        }

        return $row;


    }	

	public static function getColoumnInfo( $result )
	{
		$pdo = \DB::getPdo();
		$res = $pdo->query($result);
		$i =0;	$coll=array();	
		while ($i < $res->columnCount()) 
		{
			$info = $res->getColumnMeta($i);			
			$coll[] = $info;
			$i++;
		}
		return $coll;	
	
	}	


	function validAccess( $id)
	{

		$row = \DB::table('groups_access')->where('module_id','=', $id)
				->where('group_id','=', \Session::get('gid'))
				->get();
		
		if(count($row) >= 1)
		{
			$row = $row[0];
			if($row->access_data !='')
			{
				$data = json_decode($row->access_data,true);
			} else {
				$data = array();
			}	
			return $data;		
			
		} else {
			return false;
		}			
	
	}	

	static function getColumnTable( $table )
	{	  
        $columns = array();
        $prefix  = \DB::getTablePrefix();
	    foreach(\DB::select("SHOW COLUMNS FROM $prefix$table") as $column)
        {
           //print_r($column);
		    $columns[$column->Field] = '';
        }
	  

        return $columns;
	}	

	static function getTableList( $db ) 
	{
	 	$t = array(); 
		$dbname = 'Tables_in_'.$db ; 
		foreach(\DB::select("SHOW TABLES FROM {$db}") as $table)
        {
		    $t[$table->$dbname] = $table->$dbname;
        }	
		return $t;
	}	
	
	static function getTableField( $table ) 
	{
        $columns = array();
	    foreach(\DB::select("SHOW COLUMNS FROM $table") as $column)
		    $columns[$column->Field] = $column->Field;
        return $columns;
	}	

	function get_options($code = '')
	{
		$options = DB::table('options')->where('code', '=', $code)->get();
		
		$return	= array();
		foreach($options as $results)
		{
			$return[$results->option_key]	= $results->option;
		}
		return $return;	
	}

	function get_option($option_key = '')
	{
		$options = DB::table('options')->where('option_key', '=', $option_key)->get();
		
		foreach($options as $results)
		{
			return $results->option;
		}
	}
	
	function save_options($code, $values)
	{
	
		//get the options first, this way, we can know if we need to update or insert options
		//we're going to create an array of keys for the requested code
		$options	= $this->get_options($code);
			
		//echo '<pre>';print_r($values);exit;
		//loop through the options and add each one as a new row
		$pay=1;
		foreach($values as $key=>$value)
		{

			//if the key currently exists, update the option
			if(array_key_exists($key, $options))
			{
				if($key==='Status')
				{
					if($value=='true')
						$pay=1;
					else
						$pay=0;
				}
				DB::table('options')
			            ->where('option_key', $key)
			            ->where('code', $code)
			            ->update(['option' => $value]);

			   	DB::table('options')
			    		->where('option_key', 'payment')
			            ->where('code', $code)
			            ->update(['option' => $pay]);
			}
			//if the key does not exist, add it
			else
			{
				DB::table('options')->insert(
				    ['code' => $code, 'option_key' => $key, 'option' => $value]
				);
			}
			
		}
		
	}

	function get_payment_method(){
		return DB::table('options')->where('option_key','payment' )->get();
	}
	function get_payment_method_enable(){
		return DB::table('options')->where('option_key','payment' )->where('option',1)->get();
	}

}
