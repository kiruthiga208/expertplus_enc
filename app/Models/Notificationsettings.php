<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notificationsettings extends Model {

	protected $table 		= 'notifications_settings';
	protected $primaryKey 	= 'id';

	public $timestamps = false;
			
	public function __construct() {
		parent::__construct();
	}

	protected function getDateFormat()
    {
        return 'U';
    }

    public function get_users($field = '')
    {
	    return \DB::table('notifications_settings')
	    		->select('users.id', 'users.email', 'users.username')
                ->leftJoin('users', 'users.id', '=', 'notifications_settings.user_id')
                ->where('notifications_settings.'.$field,'=', '1')
                ->where('notifications_settings.notif_for_all','=', '0')
                ->get();
    }
}
