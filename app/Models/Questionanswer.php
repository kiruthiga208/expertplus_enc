<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use File;

class questionanswer extends bsetec  {

	protected $table = 'questions';
	protected $primaryKey = 'question_id';

	public function __construct() {
		parent::__construct();

	}

	public static function querySelect(  ){
           
		return "  SELECT ".\bsetecHelpers::getdbprefix()."questions.* FROM " .\bsetecHelpers::getdbprefix()."questions ";
	}

	public static function queryWhere(  ){

       return "  WHERE " .\bsetecHelpers::getdbprefix()."questions.question_id IS NOT NULL ";
	}

	public static function queryGroup(){
		return "  ";
	}

	function insertAnswer($data) {
		$data['createdOn'] = date("Y-m-d H:i:s");
		$data['updatedOn'] = date("Y-m-d H:i:s");
		\DB::table('questions')->where('question_id',$data['question_id'])->increment('answer_count');
		return \DB::table('answers')->insert($data);
	}

	function getAnswer($id) {
		return \DB::table('answers')->select('answer_text','answer_id','user_id','createdOn')->where('question_id',$id)->get();
	}

    function updateAnswer($ans_id,$answer_text) {
		return \DB::table('answers')->where('answer_id',$ans_id)->update(['answer_text' => $answer_text]);
	}

    function updateViews($id) {
		return \DB::table('questions')->where('question_id',$id)->increment('views');
	}

	function getquestioninfo($id='') {
        return \DB::table('questions')->where('question_id','=', $id)->first();
    }

    function updateStatus($id,$status) {
		return \DB::table('questions')->where('question_id',$id)->update(['approved' => $status]);
	}

	public function postDocdelete($lid,$rid){

		$resfiles = \DB::table('course_files')->where('id', '=', $rid)->get();

		if(!empty($resfiles)){
			foreach($resfiles as $resfile) {
				File::Delete('./uploads/files/'.$resfile->file_name.'.'.$resfile->file_extension);
			}
			\DB::table('course_files')->where('id', '=', $rid)->delete();

			$ccr = \DB::table('questions')->where('question_id', '=', $lid)->get();

			if(!empty($ccr) && !is_null($ccr['0']->file_ids)){
				$resources = json_decode($ccr['0']->file_ids,true);
				if(($key = array_search($rid, $resources)) !== false) {
					unset($resources[$key]);
				}
				$resources = array_values($resources);
				if(!empty($resources))
					$file_ids = json_encode($resources);
				else
					$file_ids = '';
				\DB::table('questions')->where('question_id',$lid)->update(['file_ids' => $file_ids]);
			}
		}
	}

	public function getfiledata($id='') {
        return \DB::table('course_files')->where('id', '=', $id)->first();
    }

}
