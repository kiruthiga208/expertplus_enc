<?php
class SiteHelpers
{
	public static function menus( $position ='top',$active = '1')
	{
		$data = array();  
		$menu = self::nestedMenu(0,$position ,$active);		
		foreach ($menu as $row) 
		{
			$child_level = array();
			$p = json_decode($row->access_data,true);
			
			
			if($row->allow_guest == 1)
			{
				$is_allow = 1;
			} else {
				$is_allow = (isset($p[Session::get('gid')]) && $p[Session::get('gid')] ? 1 : 0);
			}
			if($is_allow ==1) 
			{
				
				$menus2 = self::nestedMenu($row->menu_id , $position ,$active );
				if(count($menus2) > 0 )
				{	 
					$level2 = array();							 
					foreach ($menus2 as $row2) 
					{
						$p = json_decode($row2->access_data,true);
						if($row2->allow_guest == 1)
						{
							$is_allow = 1;
						} else {
							$is_allow = (isset($p[Session::get('gid')]) && $p[Session::get('gid')] ? 1 : 0);
						}						

						if($is_allow ==1)  
						{						

							$menu2 = array(
								'menu_id'		=> $row2->menu_id,
								'module'		=> $row2->module,
								'menu_type'		=> $row2->menu_type,
								'url'			=> $row2->url,
								'menu_name'		=> $row2->menu_name,
								'menu_lang'		=> json_decode($row2->menu_lang,true),
								'menu_icons'	=> $row2->menu_icons,
								'childs'		=> array()
								);	

							$menus3 = self::nestedMenu($row2->menu_id , $position , $active);
							if(count($menus3) > 0 )
							{
								$child_level_3 = array();
								foreach ($menus3 as $row3) 
								{
									$p = json_decode($row3->access_data,true);
									if($row3->allow_guest == 1)
									{
										$is_allow = 1;
									} else {
										$is_allow = (isset($p[Session::get('gid')]) && $p[Session::get('gid')] ? 1 : 0);
									}										
									if($is_allow ==1)  
									{								
										$menu3 = array(
											'menu_id'		=> $row3->menu_id,
											'module'		=> $row3->module,
											'menu_type'		=> $row3->menu_type,
											'url'			=> $row3->url,												
											'menu_name'		=> $row3->menu_name,
											'menu_lang'		=> json_decode($row3->menu_lang,true),
											'menu_icons'	=> $row3->menu_icons,
											'childs'		=> array()
											);	
										$child_level_3[] = $menu3;	
									}					
								}
								$menu2['childs'] = $child_level_3;
							}
							$level2[] = $menu2 ;
						}	

					}
					$child_level = $level2;

				}
				
				$level = array(
					'menu_id'		=> $row->menu_id,
					'module'		=> $row->module,
					'menu_type'		=> $row->menu_type,
					'url'			=> $row->url,						
					'menu_name'		=> $row->menu_name,
					'menu_lang'		=> json_decode($row->menu_lang,true),
					'menu_icons'	=> $row->menu_icons,
					'childs'		=> $child_level
					);			
				
				$data[] = $level;	
			}	

		}	
		//echo '<pre>';print_r($data); echo '</pre>'; exit;
		return $data;
	}
	
	public static function nestedMenu($parent=0,$position ='top',$active = '1')
	{
		$group_sql = " AND menu_access.group_id ='".Session::get('gid')."' ";
		$active 	=  ($active =='all' ? "" : "AND active ='1' ");
		$Q = DB::select("
			SELECT 
			".\bsetecHelpers::getdbprefix()."menu.*
			FROM ".\bsetecHelpers::getdbprefix()."menu WHERE parent_id ='". $parent ."' ".$active." AND position ='{$position}'
			GROUP BY ".\bsetecHelpers::getdbprefix()."menu.menu_id ORDER BY ordering			
			");					
		return $Q;					
	}
	
	public static function CF_encode_json($arr) {
		$str = json_encode( $arr );
		$enc = base64_encode($str );
		$enc = strtr( $enc, 'poligamI123456', '123456poligamI');
		return $enc;
	}
	
	public static function CF_decode_json($str) {
		$dec = strtr( $str , '123456poligamI', 'poligamI123456');
		$dec = base64_decode( $dec );
		$obj = json_decode( $dec ,true);
		return $obj;
	}	
	
	
	public static function columnTable( $table )
	{	  
		$columns = array();
		foreach(DB::select("SHOW COLUMNS FROM $table") as $column)
		{
           //print_r($column);
			$columns[] = $column->Field;
		}


		return $columns;
	}	
	
	public static function encryptID($id,$decript=false,$pass='',$separator='-', & $data=array()) {
		// echo $id;exit;
		$pass = $pass?$pass:Config::get('app.key');
		$pass2 = Config::get('app.url');;
		$bignum = 200000000;
		$multi1 = 500;
		$multi2 = 50;
		$saltnum = 10000000;
		if($decript==false){
			$strA = self::alphaid(($bignum+($id*$multi1)),0,0,$pass);
			$strB = self::alphaid(($saltnum+($id*$multi2)),0,0,$pass2);
			$out = $strA.$separator.$strB;
		} else {
			$pid = explode($separator,$id);
			
			$pid_index = (isset($pid[1])) ? 1 : 0;
		    //trace($pid);
			$idA = (self::alphaid($pid[0],1,0,$pass)-$bignum)/$multi1;
			$idB = (self::alphaid($pid[$pid_index],1,0,$pass2)-$saltnum)/$multi2;
			$data['id A'] = $idA;
			$data['id B'] = $idB;
			$out = ($idA==$idB)?$idA:false;
		}

		return $out;
	}
	
	public static function alphaID($in, $to_num = false, $pad_up = false, $passKey = null)
	{
		$index = "abcdefghijkmnpqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
		if ($passKey !== null) {
        // Although this function's purpose is to just make the
        // ID short - and not so much secure,
        // with this patch by Simon Franz (http://blog.snaky.org/)
        // you can optionally supply a password to make it harder
        // to calculate the corresponding numeric ID

			for ($n = 0; $n<strlen($index); $n++) {
				$i[] = substr( $index,$n ,1);
			}

			$passhash = hash('sha256',$passKey);
			$passhash = (strlen($passhash) < strlen($index))
			? hash('sha512',$passKey)
			: $passhash;

			for ($n=0; $n < strlen($index); $n++) {
				$p[] =    substr($passhash, $n ,1);
			}

			array_multisort($p,    SORT_DESC, $i);
			$index = implode($i);
		}

		$base    = strlen($index);

		if ($to_num) {
        // Digital number    <<--    alphabet letter code
			$in    = strrev($in);
			$out = 0;
			$len = strlen($in) - 1;
			for ($t = 0; $t <= $len; $t++) {
				$bcpow = bcpow($base, $len - $t);
				$out     = $out + strpos($index, substr($in, $t, 1)) * $bcpow;
			}

			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$out -= pow($base, $pad_up);
				}
			}
			$out = sprintf('%F', $out);
			$out = substr($out, 0, strpos($out, '.'));
		} else {
        // Digital number    -->>    alphabet letter code
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$in += pow($base, $pad_up);
				}
			}

			$out = "";
			for ($t = floor(log($in, $base)); $t >= 0; $t--) {
				$bcp = bcpow($base, $t);
				$a     = floor($in / $bcp) % $base;
				$out = $out . substr($index, $a, 1);
				$in    = $in - ($a * $bcp);
			}
        $out = strrev($out); // reverse
    }

    return $out;
}	


public static function toForm($forms,$layout)
{
	$f = '';
	//	echo '<pre>'; print_r($forms);echo '</pre>';
		//usort($forms,"_sort"); 
	$block = $layout['column'];
	$format = $layout['format'];
	$display = $layout['display'];
	$title = explode(",",$layout['title']);

	if($format =='tab')
	{
		$f .='<ul class="nav nav-tabs">';

		for($i=0;$i<$block;$i++)
		{
			$active = ($i==0 ? 'active' : '');
			$tit = (isset($title[$i]) ? $title[$i] : 'None');	
			$f .= '<li class="'.$active.'"><a href="#'.trim(str_replace(" ","",$tit)).'" data-toggle="tab">'.$tit.'</a></li>
			';	
		}
		$f .= '</ul>';		
	}

	if($format =='tab') $f .= '<div class="tab-content">';
	for($i=0;$i<$block;$i++)
	{		
		if($block == 4) {
			$class = 'col-md-3';
		}  elseif( $block ==3 ) {
			$class = 'col-md-4';
		}  elseif( $block ==2 ) {
			$class = 'col-md-6';
		} else {
			$class = 'col-md-12';
		}	

		$tit = (isset($title[$i]) ? $title[$i] : 'None');	
			// Grid format 
		if($format == 'grid')
		{
			$f .= '<div class="'.$class.'">
			<fieldset><legend> '.$tit.'</legend>
			';
		} else {
			$active = ($i==0 ? 'active' : '');
			$f .= '<div class="tab-pane m-t '.$active.'" id="'.trim(str_replace(" ","",$tit)).'"> 
			';			
		}	



		$group = array(); 

		foreach($forms as $form)
		{
			$tooltip =''; $required = ($form['required'] != '0' ? '<span class="asterix"> * </span>' : '');
			if($form['view'] != 0)
			{
				if($form['field'] !='entry_by')
				{
					if(isset($form['option']['tooltip']) && $form['option']['tooltip'] !='') 	
						$tooltip = '<a href="#" data-toggle="tooltip" placement="left" class="tips" title="'. $form['option']['tooltip'] .'"><i class="icon-question2"></i></a>';	
					$hidethis = ""; if($form['type'] =='hidden') $hidethis ='hidethis';
					$inhide = ''; if(count($group) >1) $inhide ='inhide';
						//$ebutton = ($form['type'] =='radio' || $form['option'] =='checkbox') ? "ebutton-radio" : "";
					$show = '';
					if($form['type'] =='hidden') $show = 'style="display:none;"';	
					if($form['form_group'] == $i)
					{	
						if($display == 'horizontal')
						{			
							$f .= '					
							<div class="form-group '.$hidethis.' '.$inhide.'" '.$show .'>
							<label for="'.$form['label'].'" class=" control-label col-md-4 text-left"> '.$form['label'].' '.$required.'</label>
							<div class="col-md-6">
							'.self::formShow($form['type'],$form['field'],$form['required'],$form['option']).' 
							</div> 
							<div class="col-md-2">
							'.$tooltip.'
							</div>
							</div> '; 
						} else {
							$f .= '					
							<div class="form-group '.$hidethis.' '.$inhide.'" '.$show .'>
							<label for="ipt" class=" control-label "> '.$form['label'].'  '.$required.' '.$tooltip.' </label>									
							'.self::formShow($form['type'],$form['field'],$form['required'],$form['option']).' 						
							</div> '; 							
							
						}	  
					}	  
				}	  

			}					
		}
		if($format == 'grid') $f .='</fieldset>';
		$f .= '
		</div>

		';
	} 		

		//echo '<pre>'; print_r($f);echo '</pre>'; exit;
	return $f;
	
}
public static function gridClass( $layout )
{
	$column = $layout['column'];
	$format = $layout['format'];

	if($block == 4) {
		$class = 'col-md-3';
	}  elseif( $block ==3 ) {
		$class = 'col-md-4';
	}  elseif( $block ==2 ) {
		$class = 'col-md-6';
	} else {
		$class = 'col-md-12';
	}	


	if(format == 'tab')
	{
		$tag_open = '<div class="col-md-">';
		$tag_close = '<div class="col-md-">';

	}  elseif($layout['format'] == 'accordion'){
		
	} else {					
		$tag_open = '<div class="col-md-">';
		$tag_close = '</div>';
	}		


	return $class;	
}


public static function formShow( $type , $field , $required ,$option = array()){
		//print_r($option);
	$mandatory = '';$attribute = ''; $extend_class ='';
	if(isset($option['attribute']) && $option['attribute'] !='') {
		$attribute = $option['attribute']; }
		if(isset($option['extend_class']) && $option['extend_class'] !='') {
			$extend_class = $option['extend_class']; 
		}				

		$show = '';
		if($type =='hidden') $show = 'style="display:none;"';	

		if($required =='required') {
			$mandatory = "'required'=>'true'";
		} else if($required =='email') {
			$mandatory = "'required'=>'true', 'parsley-type'=>'email' ";
		} else if($required =='url') {
			$mandatory = "'required'=>'true', 'parsley-type'=>'url' ";
		} else if($required =='date') {
			$mandatory = "'required'=>'true', 'parsley-type'=>'dateIso' ";
		} else if($required =='numeric') {
			$mandatory = "'required'=>'true', 'parsley-type'=>'number' ";
		} else {
			$mandatory = '';
		}		
		
		switch($type)
		{
			default;
			$form = "{!! Form::text('{$field}', \$row['{$field}'],array('class'=>'form-control', 'placeholder'=>'', {$mandatory}  )) !!}";
			break;

			case 'textarea';
			if($required !='0') { $mandatory = 'required'; }
			$form = "<textarea name='{$field}' rows='2' id='{$field}' class='form-control {$extend_class}'  
			{$mandatory} {$attribute} >{{ \$row['{$field}'] }}</textarea>";
			break;

			case 'textarea_editor';
			if($required !='0') { $mandatory = 'required'; }
			$form = "<textarea name='{$field}' rows='2' id='editor' class='form-control markItUp {$extend_class}'  
			{$mandatory}{$attribute} >{{ \$row['{$field}'] }}</textarea>";
			break;				


			case 'text_date';
			$form = "
			{{ Form::text('{$field}', \$row['{$field}'],array('class'=>'form-control date', 'style'=>'width:150px !important;')) }}";
			break;

			case 'text_time';
			$form = "<input  type='text' name='{$field}' id='{$field}' value='{{ \$row['{$field}'] }}' 
			{$mandatory}  {$attribute} style='width:150px !important;'  class='form-control {$extend_class}'
			data-date-format='yyyy-mm-dd'
			/>";
			break;				

			case 'text_datetime';
			if($required !='0') { $mandatory = 'required'; }
			$form = "
			{!! Form::text('{$field}', \$row['{$field}'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}";
			break;				

			case 'select';
			if($required !='0') { $mandatory = 'required'; }
			if($option['opt_type'] =='datalist')
			{
				$optList ='';
				$opt = explode("|",$option['lookup_query']);
				for($i=0; $i<count($opt);$i++) 
				{							
					$row =  explode(":",$opt[$i]);
					for($i=0; $i<count($opt);$i++) 
					{					

						$row =  explode(":",$opt[$i]);
						$optList .= " '".trim($row[0])."' => '".trim($row[1])."' , ";

					}							
				}	
				$form  = "
				<?php \$".$field." = explode(',',\$row['".$field."']);
				";
				$form  .= 
				"\$".$field."_opt = array(".$optList."); ?>
				";	

				if(isset($option['is_multiple']) && $option['is_multiple'] ==1)
				{

					$form  .= "<select name='{$field}[]' rows='5' {$mandatory} multiple  class='select2 '  > ";
					$form  .= "
					<?php 
					foreach(\$".$field."_opt as \$key=>\$val)
					{
						echo \"<option  value ='\$key' \".(in_array(\$key,\$".$field.") ? \" selected='selected' \" : '' ).\">\$val</option>\"; 						
					}						
					?>";
					$form .= "</select>";
				} else {

					$form  .= "<select name='{$field}' rows='5' {$mandatory}  class='select2 '  > ";
					$form  .= "
					<?php 
					foreach(\$".$field."_opt as \$key=>\$val)
					{
						echo \"<option  value ='\$key' \".(\$row['".$field."'] == \$key ? \" selected='selected' \" : '' ).\">\$val</option>\"; 						
					}						
					?>";
					$form .= "</select>";				
					
				}

			} else {
				$form = "<select name='{$field}' rows='5' id='{$field}' code='{\${$field}}' 
				class='select2 {$extend_class}'  {$mandatory} {$attribute} ></select>";
			}
			break;	

			case 'file';
			if($required !='0') { $mandatory = 'required'; }
			$form  = "<input  type='file' name='{$field}' id='{$field}' ";
			$form .= "@if(\$row['$field'] =='') class='required' @endif "; 
			$form .= "style='width:150px !important;' {$attribute} />
			<div >
			{!! SiteHelpers::showUploadedFile(\$row['{$field}'],'$option[path_to_upload]') !!}
			@if(\$row['".$field."'] !='')
			<a href=\"{{ URL::to('employee/removecurrentfiles?file=/uploads/'.\$row['Foto'].'&id='.\$row['EmployeeId'].'&field=Foto') }}\" class=\"removeCurrentFiles\"> Remove current file</a>
			@endif
			</div>					
			";
			break;						

			case 'radio';
			if($required !='0') { $mandatory = 'required'; }
			$opt = explode("|",$option['lookup_query']);
			$form = '';
			for($i=0; $i<count($opt);$i++) 
			{
				$checked = '';
				$row =  explode(":",$opt[$i]); 
				$form .= "
				<label class='radio radio-inline'>
				<input type='radio' name='{$field}' value ='".ltrim(rtrim($row[0]))."' {$mandatory} {$attribute}";
				$form .= "@if(\$row['".$field."'] == '".ltrim(rtrim($row[0]))."') checked=\"checked\" @endif";
				$form .= " > ".$row[1]." </label>";
			}
			break;

			case 'checkbox';
			if($required !='0') { $mandatory = 'required'; }
			$opt = explode("|",$option['lookup_query']);
			$form = "<?php \$".$field." = explode(\",\",\$row['".$field."']); ?>";
			for($i=0; $i<count($opt);$i++) 
			{

				$checked = '';
				$row =  explode(":",$opt[$i]);					
				$form .= "
				<label class='checked checkbox-inline'>   
				<input type='checkbox' name='{$field}[]' value ='".ltrim(rtrim($row[0]))."' {$mandatory} {$attribute} class='{$extend_class}' ";
				$form .= "
				@if(in_array('".trim($row[0])."',\$".$field."))checked @endif 
				";
				$form .= " /> ".$row[1]." </label> ";					
			}
			break;				
			
		}
		
		return $form;		
	}
	
	public static function toView( $grids )
	{
		$f = '';
		foreach($grids as $grid)
		{
			if(isset($grid['conn']) && is_array($grid['conn']))
			{
				$conn = $grid['conn'];
				//print_r($conn);exit;
			} else {
				$conn = array('valid'=>0,'db'=>'','key'=>'','display'=>'');
			}
			
			if($grid['detail'] =='1')  
			{
				if($grid['attribute']['image']['active'] =='1')
				{	
					$val = "{{ SiteHelpers::showUploadedFile(\$row->".$grid['field'].",'".$grid['attribute']['image']['path']."') }}";	
				} elseif($conn['valid'] ==1)  {
					$arr = implode(':',$conn);
					//$arg = "'".$arr['valid'].":".$arr['db'].":".$arr['key'].":".$arr['display']."'";
					$val = "{{ SiteHelpers::gridDisplayView(\$row->".$grid['field'].",'".$grid['field']."','".$arr."') }}";
				} else {
					$val = "{{ \$row->".$grid['field']." }}"; 
				}
				$f .= "
				<tr>
				<td width='30%' class='label-view text-right'>".$grid['label']."</td>
				<td>".$val." </td>

				</tr>
				";
			}
		}
		return $f;
	}
	
	public static  function transForm( $field, $forms = array(),$bulk=false , $value ='')
	{
		$type = ''; 
		$bulk = ($bulk == true ? '[]' : '');
		$mandatory = '';
		foreach($forms as $f)
		{
			if($f['field'] == $field && $f['search'] ==1)
			{
				$type = ($f['type'] !='file' ? $f['type'] : ''); 
				$option = $f['option'];
				$required = $f['required'];
				
				if($required =='required') {
					$mandatory = "data-parsley-required='true'";
				} else if($required =='email') {
					$mandatory = "data-parsley-type'='email' ";
				} else if($required =='date') {
					$mandatory = "data-parsley-required='true'";
				} else if($required =='numeric') {
					$mandatory = "data-parsley-type='number' ";
				} else {
					$mandatory = '';
				}				
			}	
		}
		
		switch($type)
		{
			default;
			$form ='';
			break;
			
			case 'text';			
			$form = "<input  type='text' name='".$field."{$bulk}' class='form-control input-sm' $mandatory value='{$value}'/>";
			break;

			case 'text_date';
			$form = "<input  type='text' name='$field{$bulk}' class='date form-control input-sm' $mandatory value='{$value}'/> ";
			break;

			case 'text_datetime';
			$form = "<input  type='text' name='$field{$bulk}'  class='date form-control input-sm'  $mandatory value='{$value}'/> ";
			break;				

			case 'select';

			
			if($option['opt_type'] =='external')
			{
				// $data = DB::table($option['lookup_table'])->get();
				$data = DB::Select("SELECT * FROM ".$option['lookup_table']);
				$opts = '';
				foreach($data as $row):
					$selected = '';
				if($value == $row->$option['lookup_key']) $selected ='selected="selected"';
				$fields = explode("|",$option['lookup_value']);
						//print_r($fields);exit;
				$val = "";
				foreach($fields as $item=>$v)
				{
					if($v !="") $val .= $row->$v." " ;
				}
				$opts .= "<option $selected value='".$row->$option['lookup_key']."' $mandatory > ".$val." </option> ";
				endforeach;

			} else {
				$opt = explode("|",$option['lookup_query']);
				$opts = '';
				for($i=0; $i<count($opt);$i++) 
				{
					$selected = ''; 
					if($value == ltrim(rtrim($opt[0]))) $selected ='selected="selected"';
					$row =  explode(":",$opt[$i]); 
					$opts .= "<option $selected value ='".trim($row[0])."' > ".$row[1]." </option> ";
				}				

			}
			$form = "<select name='$field{$bulk}'  class='form-control' $mandatory >
			<option value=''> -- Select  -- </option>
			$opts
			</select>";
			break;	

			case 'radio';
			
			$opt = explode("|",$option['lookup_query']);
			$opts = '';
			for($i=0; $i<count($opt);$i++) 
			{
				$checked = '';
				$row =  explode(":",$opt[$i]);
				$opts .= "<option value ='".$row[0]."' > ".$row[1]." </option> ";
			}
			$form = "<select name='$field{$bulk}' class='form-control' $mandatory ><option value=''> -- Select  -- </option>$opts</select>";
			break;												
			
		}
		
		return $form;	
	}
	
	public static function viewColSpan( $grid )
	{
		$i =0;
		foreach ($grid as $t):
			if($t['view'] =='1') ++$i;
		endforeach;
		return $i;	
	}
	
	public static function blend($str,$data) {
		$src = $rep = array();
		
		foreach($data as $k=>$v){
			$src[] = "{".$k."}";
			$rep[] = $v;
		}
		
		if(is_array($str)){
			foreach($str as $st ){
				$res[] = trim(str_ireplace($src,$rep,$st));
			}
		} else {
			$res = str_ireplace($src,$rep,$str);
		}
		
		return $res;
		
	}			

	public static function toJavascript( $forms , $app , $class )
	{
		$f = '';
		foreach($forms as $form){
			if($form['view'] != 0)
			{			
				if(preg_match('/(select)/',$form['type'])) 
				{
					if($form['option']['opt_type'] == 'external') 
					{
						$table 	=  $form['option']['lookup_table'] ;
						$val 	=  $form['option']['lookup_value'];
						$key 	=  $form['option']['lookup_key'];
						$lookey = '';
						if($form['option']['is_dependency']) $lookey .= $form['option']['lookup_dependency_key'] ;
						$f .= self::createPreCombo( $form['field'] , $table , $key , $val ,$app, $class , $lookey  );

					}

				}
				
			}	

		}
		return $f;	

	}
	
	public static function createPreCombo( $field , $table , $key ,  $val ,$app ,$class ,$lookey = null)
	{


		
		$parent = null;
		$parent_field = null;
		if($lookey != null)  
		{	
			$parent = " parent: '#".$lookey."',";
			$parent_field =  ":{$lookey}:";
		}	
		$pre_jCombo = "
		\$(\"#{$field}\").jCombo(\"{{ URL::to('{$class}/comboselect?filter={$table}:{$key}:{$val}') }}$parent_field\",
			{ ".$parent." selected_value : '{{ \$row[\"{$field}\"] }}' });
";	
return $pre_jCombo;
}	

static public function showNotification()
{
	$status = Session::get('msgstatus');
	if(Session::has('msgstatus')): ?>	  
	<script type="text/javascript">
	$(document).ready(function(){
		toastr.<?php echo $status;?>("success", "<?php echo Session::get('messagetext');?>");
		toastr.options = {
			"closeButton": true,
			"debug": false,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"

		}
	});
	</script>		
	<?php endif;	
}

public static function alert( $task , $message)
{
	if($task =='error') {
		$alert ='
		<div class="alert alert-danger  fade in block-inner">
		<button data-dismiss="alert" class="close" type="button"> x </button>
		<i class="icon-cancel-circle"></i> '. $message.' </div>
		';			
	} elseif ($task =='success') {
		$alert ='
		<div class="alert alert-success fade in block-inner">
		<button data-dismiss="alert" class="close" type="button"> x </button>
		<i class="icon-checkmark-circle"></i> '. $message.' </div>
		';			
	} elseif ($task =='warning') {
		$alert ='
		<div class="alert alert-warning fade in block-inner">
		<button data-dismiss="alert" class="close" type="button"> x </button>
		<i class="icon-warning"></i> '. $message.' </div>
		';	
	}else if($task =='account_success'){
		$alert ='
		<p class="alert alert-success fade in block-inner">
		<i class="icon-warning"></i><span> '. $message.' </span></p>
		';	
	} else {
		$alert ='
		<div class="alert alert-info  fade in block-inner">
		<button data-dismiss="alert" class="close" type="button"> x </button>
		<i class="icon-info"></i> '. $message.' </div>
		';			
	}
	return $alert;
	
} 		

public static function _sort($a, $b) {

	if ($a['sortlist'] == $a['sortlist']) {
		return strnatcmp($a['sortlist'], $b['sortlist']);
	}
	return strnatcmp($a['sortlist'], $b['sortlist']);
}


static public function cropImage($nw, $nh, $source, $stype, $dest) 
{
		$size = getimagesize($source); // ukuran gambar
		$w = $size[0];
		$h = $size[1];
		switch($stype) 
		{ // format gambar
			default :
			$simg = imagecreatefromjpeg($source);
			break;

			case 'gif':
			$simg = imagecreatefromgif($source);
			break;
			
			case 'png':
			$simg = imagecreatefrompng($source);
			break;
		}
		$dimg = imagecreatetruecolor($nw, $nh); // menciptakan image baru
		$wm = $w/$nw;
		$hm = $h/$nh;
		$h_height = $nh/2;
		$w_height = $nw/2;
		if($w> $h) 
		{
			$adjusted_width = $w / $hm;
			$half_width = $adjusted_width / 2;
			$int_width = $half_width - $w_height;
			imagecopyresampled($dimg,$simg,-$int_width,0,0,0,$adjusted_width,$nh,$w,$h);
		}
		elseif(($w <$h) || ($w == $h)) 
		{
			$adjusted_height = $h / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;
			imagecopyresampled($dimg,$simg,0,-$int_height,0,0,$nw,$adjusted_height,$w,$h);
		}
		else
		{
			imagecopyresampled($dimg,$simg,0,0,0,0,$nw,$nh,$w,$h);
		}
		imagejpeg($dimg,$dest,100);
	}	

	
	public static function showUploadedFile($file,$path , $width = 50) {

		$files =  public_path(). $path . $file ;		
		if(file_exists($files ) && $file !="") {
		//	echo $files ;
			$info = pathinfo($files);	
			if($info['extension'] == "jpg" || $info['extension'] == "jpeg" ||  $info['extension'] == "png" || $info['extension'] == "gif" || $info['extension'] == "JPG") 
			{
				$path_file = str_replace("./","",$path);
				return '<p><a href="'.URL::to(''). $path_file . $file.'" target="_blank" class="previewImage">
				<img src="'.URL::to(''). $path_file . $file.'" border="0" width="'. $width .'" class="img-circle" /></a></p>';
			} else {
				$path_file = str_replace("./","",$path);
				return '<p> <a href="'.URL::to($path_file . $file).'" target="_blank"> '.$file.' </a>';
			}

		} else {
			$info = pathinfo($files);
			if(isset($info['extension']))
			{
				if($info['extension'] == "jpg" || $info['extension'] == "jpeg" ||  $info['extension'] == "png" 
					|| $info['extension'] == "gif") 
				{
					$path_file = str_replace("./","",$path);
					return "<img src='".asset('/uploads/images/no-image.png')."' border='0' width='".$width."' class='img-circle' /></a>";
				} 	
			} else {
				
			}	
		}

	}	

	public static function globalXssClean()
	{
	    // Recursive cleaning for array [] inputs, not just strings.
		$sanitized = static::arrayStripTags(Input::get());
		Input::merge($sanitized);
	}

	public static function arrayStripTags($array)
	{
		$result = array();

		foreach ($array as $key => $value) {
	        // Don't allow tags on key either, maybe useful for dynamic forms.
			$key = strip_tags($key);

	        // If the value is an array, we will just recurse back into the
	        // function to keep stripping the tags out of the array,
	        // otherwise we will set the stripped value.
			if (is_array($value)) {
				$result[$key] = static::arrayStripTags($value);
			} else {
	            // I am using strip_tags(), you may use htmlentities(),
	            // also I am doing trim() here, you may remove it, if you wish.
				$result[$key] = trim(strip_tags($value));
			}
		}

		return $result;
	}
	
	public static function writeEncoder($val) {
		return base64_encode($val);
	}
	
	public static function readEncoder($val) {
		return base64_decode($val);
	}
	
	public static function gridDisplay($val , $field, $arr) {

		if(isset($arr['valid']) && $arr['valid'] ==1)
		{
			$fields = str_replace("|",",",$arr['display']);
			$Q = DB::select(" SELECT ".$fields." FROM ".$arr['db']." WHERE ".$arr['key']." = '".$val."' ");

			if(count($Q) >= 1 )
			{
				$row = $Q[0];
				$fields = explode("|",$arr['display']);
				$v= '';

				$v .= (isset($fields[0]) && $fields[0] !='' ?  $row->$fields[0].' ' : '');
				$v .= (isset($fields[1]) && $fields[1] !=''  ? $row-> $fields[1].' ' : '');
				$v .= (isset($fields[2]) && $fields[2] !=''  ? $row->$fields[2].' ' : '');
				
				
				return $v;
			} else {
				return '';
			}
		} else {
			return $val;
		}	
	}
	public static function gridDisplayView($val , $field, $arr) {
		$arr = explode(':',$arr);
		
		if(isset($arr['0']) && $arr['0'] ==1)
		{
			$Q = DB::select(" SELECT ".str_replace("|",",",$arr['3'])." FROM ".$arr['1']." WHERE ".$arr['2']." = '".$val."' ");
			if(count($Q) >= 1 )
			{
				$row = $Q[0];
				$fields = explode("|",$arr['3']);
				$v= '';
				$v .= (isset($fields[0]) && $fields[0] !='' ?  $row->$fields[0].' ' : '');
				$v .= (isset($fields[1]) && $fields[1] !=''  ? $row-> $fields[1].' ' : '');
				$v .= (isset($fields[2]) && $fields[2] !=''  ? $row->$fields[2].' ' : '');
				return $v;
			} else {
				return '';
			}
		} else {
			return $val;
		}	
	}	
	
	public static function langOption()
	{
		$path = base_path().'/resources/lang/';
		$lang = scandir($path);

		$t = array();
		foreach($lang as $value) {
			if($value === '.' || $value === '..') {continue;} 
			if(is_dir($path . $value))
			{
				$fp = file_get_contents($path . $value.'/info.json');
				$fp = json_decode($fp,true);
				$t[] =  $fp ;
			}	
			
		}	
		return $t;
	}
	
	
	public static function themeOption()
	{
		
		$path = base_path().'/resources/views/layouts/';
		$lang = scandir($path);
		$t = array();
		foreach($lang as $value) {
			if($value === '.' || $value === '..') {continue;} 
			if(is_dir($path . $value))
			{
				$fp = file_get_contents($path .$value.'/info.json');
				$fp = json_decode($fp,true);
				$t[] =  $fp ;
			}	
			
		}	
		return $t;
	}

	public static function avatar( $width =75)
	{
		$avatar = '<img alt="" src="http://www.gravatar.com/avatar/'.md5(Session::get('email')).'" class="img-circle" width="'.$width.'" />';
		$Q = DB::table("users")->where("id",'=',Session::get('uid'))->get();
		if(count($Q)>=1) 
		{
			$row = $Q[0];
			$files =  './uploads/users/'.$row->avatar ;
			if($row->avatar !='' ) 	
			{
				if( file_exists($files))
				{
					return  '<img src="'.URL::to('uploads/users').'/'.$row->avatar.'" border="0" width="'.$width.'" class="img-circle" />';
				} else {
					return $avatar;
				}	
			} else {
				return $avatar;
			}
		}	
	}

	
	public static function BBCode2Html($text) {

		$emotion =  URL::to('bsetec/js/plugins/markitup/images/emoticons/');
		
		$text = trim($text);

		// BBCode [code]
		if (!function_exists('escape')) {
			function escape($s) {
				global $text;
				$text = strip_tags($text);
				$code = $s[1];
				$code = htmlspecialchars($code);
				$code = str_replace("[", "&#91;", $code);
				$code = str_replace("]", "&#93;", $code);
				return '<pre class="prettyprint linenums"><code>'.$code.'</code></pre>';
			}	
		}
		$text = preg_replace_callback('/\[code\](.*?)\[\/code\]/ms', "escape", $text);

		// Smileys to find...
		$in = array( 	 ':)', 	
		':D',
		':o',
		':p',
		':(',
			';)'
		);
		// And replace them by...
		$out = array(	 '<img alt=":)" src="'.$emotion.'emoticon-happy.png" />',
		'<img alt=":D" src="'.$emotion.'emoticon-smile.png" />',
		'<img alt=":o" src="'.$emotion.'emoticon-surprised.png" />',
		'<img alt=":p" src="'.$emotion.'emoticon-tongue.png" />',
		'<img alt=":(" src="'.$emotion.'emoticon-unhappy.png" />',
			'<img alt=";)" src="'.$emotion.'emoticon-wink.png" />'
		);
		$text = str_replace($in, $out, $text);
		
		// BBCode to find...
		$in = array( 	 '/\[b\](.*?)\[\/b\]/ms',	
			'/\[div\="?(.*?)"?](.*?)\[\/div\]/ms',
			'/\[i\](.*?)\[\/i\]/ms',
			'/\[u\](.*?)\[\/u\]/ms',
			'/\[img\](.*?)\[\/img\]/ms',
			'/\[email\](.*?)\[\/email\]/ms',
			'/\[url\="?(.*?)"?\](.*?)\[\/url\]/ms',
			'/\[size\="?(.*?)"?\](.*?)\[\/size\]/ms',
			'/\[color\="?(.*?)"?\](.*?)\[\/color\]/ms',
			'/\[quote](.*?)\[\/quote\]/ms',
			'/\[list\=(.*?)\](.*?)\[\/list\]/ms',
			'/\[list\](.*?)\[\/list\]/ms',
			'/\[\*\]\s?(.*?)\n/ms'
			);
		// And replace them by...
		$out = array(	 '<strong>\1</strong>',
			'<div class="\1">\2</div>',
			'<em>\1</em>',
			'<u>\1</u>',
			'<img src="\1" alt="\1" />',
			'<a href="mailto:\1">\1</a>',
			'<a href="\1">\2</a>',
			'<span style="font-size:\1%">\2</span>',
			'<span style="color:\1">\2</span>',
			'<blockquote>\1</blockquote>',
			'<ol start="\1">\2</ol>',
			'<ul>\1</ul>',
			'<li>\1</li>'
			);
		$text = preg_replace($in, $out, $text);

		// paragraphs
		$text = str_replace("\r", "", $text);
		$text = "<p>".preg_replace("/(\n){2,}/", "</p><p>", $text)."</p>";
		$text = nl2br($text);
		
		// clean some tags to remain strict
		// not very elegant, but it works. No time to do better ;)
		if (!function_exists('removeBr')) {
			function removeBr($s) {
				return str_replace("<br />", "", $s[0]);
			}
		}	
		$text = preg_replace_callback('/<pre>(.*?)<\/pre>/ms', "removeBr", $text);
		$text = preg_replace('/<p><pre>(.*?)<\/pre><\/p>/ms', "<pre>\\1</pre>", $text);
		
		$text = preg_replace_callback('/<ul>(.*?)<\/ul>/ms', "removeBr", $text);
		$text = preg_replace('/<p><ul>(.*?)<\/ul><\/p>/ms', "<ul>\\1</ul>", $text);
		
		return $text;
	}	
	
	public static function seoUrl($str, $separator = 'dash', $lowercase = FALSE)
	{
		if ($separator == 'dash')
		{
			$search		= '_';
			$replace	= '-';
		}
		else
		{
			$search		= '-';
			$replace	= '_';
		}

		$trans = array(
			'&\#\d+?;'				=> '',
			'&\S+?;'				=> '',
			'\s+'					=> $replace,
			'[^a-z0-9\-\._]'		=> '',
			$replace.'+'			=> $replace,
			$replace.'$'			=> $replace,
			'^'.$replace			=> $replace,
			'\.+$'					=> ''
			);

		$str = strip_tags($str);

		foreach ($trans as $key => $val)
		{
			$str = preg_replace("#".$key."#i", $val, $str);
		}

		if ($lowercase === TRUE)
		{
			$str = strtolower($str);
		}
		
		return trim(stripslashes(strtolower($str)));
	}	
	
	
	static function renderHtml( $html )
	{

		$html = preg_replace( '/(\.+\/)+uploads/Usi' , URL::to('uploads') ,  $html );
	//	$content = str_replace($pattern , URL::to('').'/', $content );
		preg_match_all ( "#<([a-z]+)( .*)?(?!/)>#iU", $html, $result );
		$openedtags = $result[1];
        #put all closed tags into an array
		preg_match_all ( "#</([a-z]+)>#iU", $html, $result );
		$closedtags = $result[1];
		$len_opened = count ( $openedtags );
        # all tags are closed
		if( count ( $closedtags ) == $len_opened )
		{
			return $html;
		}
		$openedtags = array_reverse ( $openedtags );
        # close tags
		for( $i = 0; $i < $len_opened; $i++ )
		{
			if ( !in_array ( $openedtags[$i], $closedtags ) )
			{
				$html .= "</" . $openedtags[$i] . ">";
			}
			else
			{
				unset ( $closedtags[array_search ( $openedtags[$i], $closedtags)] );
			}
		}
		return $html;



	} 

	public static function activeLang( $label , $l )
	{
		//$activeLang = Session::get('lang');
		$activeLang = CNF_LANG;
		$lang = (isset($l[$activeLang]) ? $l[$activeLang] : $label );
		return $lang; 
		
	}
	
	public static function infoLang( $label , $l , $t = 'title' )
	{
		//$activeLang = Session::get('lang');
		$activeLang = CNF_LANG;
		$lang = (isset($l[$t][$activeLang]) ? $l[$t][$activeLang] : $label );
		return $lang; 
		
	}	

	public static function auditTrail( $request , $note )
	{
		$data = array(
			'module'	=> $request->segment(1),
			'task'		=> $request->segment(2),
			'user_id'	=> \Session::get('uid'),
			'ipaddress'	=> $request->getClientIp(),
			'note'		=> $note
			);
		
		\DB::table('logs')->insert($data);		

	}

	public static function customavatar($email='',$userid ='',$type='normal',$class='img-circle')
	{

		if($class == 'croppedImg')
		{
			$width = '100%';
			$height = '500px';
		}
		else
		{
			$width = '75';
			$height = '';
		}
		$avatar = '<img src="http://www.gravatar.com/avatar/'.md5($email).'" alt="" data-src="http://www.gravatar.com/avatar/'.md5($email).'" data-original="http://www.gravatar.com/avatar/'.md5($email).'" class="'.$class.' lazy" width="75" />';
		$Q = \DB::table("users")->where("id",'=',$userid)->get();

		if(count($Q)>=1) 
		{
			$row = $Q[0];
				
			if($row->avatar !='' && $row->avatar !='0'  ) 	
			{	
				$getimage 	= \DB::table("course_images")->where("id",'=',$row->avatar)->get();
				if(count($getimage)>0){
					$rowimg   	= $getimage[0];

					$size = $type == 'normal' ? '' : '_'.$type;
					$files 		= './uploads/users/'.$rowimg->id.$size.'.'.$rowimg->image_type;
					if(file_exists($files)){
						return  '<img src="'.URL::to('user/image').'/'.$rowimg->image_hash.'/'.$type.'" border="0" width="'.$width.'" height="'.$height.'" class="'.$class.'" />';
						// return  '<img src="'.URL::to('bsetec/images/spacer.gif').'" data-src="'.URL::to('user/image').'/'.$rowimg->image_hash.'/'.$type.'"  data-original="'.URL::to('user/image').'/'.$rowimg->image_hash.'/'.$type.'" border="0" width="'.$width.'" height="'.$height.'" class="'.$class.' lazy" />';
					} else {
						return $avatar;
					}
				}else {
					return $avatar;
				}

			} else {
				if(!empty($row->social_avatar)){
					$avatar = '<img src="'.$row->social_avatar.'" alt="" class="'.$class.' lazy" width="75" />';
				}else{
					$avatar = '<img src="http://www.gravatar.com/avatar/'.md5($email).'" alt="" data-src="http://www.gravatar.com/avatar/'.md5($email).'" data-original="http://www.gravatar.com/avatar/'.md5($email).'" class="'.$class.' lazy" width="75" />';

				}
				return $avatar;
			}
		}	
	}

	public static function changeFormat($date)
	{
		$_date = $date;
		$get = \DB::select('select now() as times');
		// $new_date = date("Y-m-d H:i:s");
		// echo $get['0']->times;
		// $new_date = $get['0']->times;
		$new_date = date('Y-m-d H:i:s');
		$date = date_parse($date);
		$new_date = date_parse($new_date);

		$years_ago = $new_date["year"] - $date["year"];
		$years_ago= abs($years_ago);
		if($years_ago != 0)
		{
			if($years_ago == 1)
			{
				return $years_ago." year ago";
				exit();
			}
			else 
			{
				return $years_ago." years ago";
				exit();
			}
		}

		if($new_date["month"] == $date["month"] and $new_date["day"] == $date["day"] and $new_date["hour"] == $date["hour"] and $new_date["minute"] <= ($date["minute"] + 1))
		{
			return "Just now";
			exit();
		}

		$min_ago = $new_date["minute"] - $date["minute"];
		$min_ago=abs($min_ago);
		if($new_date["month"] == $date["month"] and $new_date["day"] == $date["day"] and $new_date["hour"] == $date["hour"])
		{
			return $min_ago." min ago";
			exit();
		}

		$hour_ago = $new_date["hour"] - $date["hour"];
		$hour_ago = abs($hour_ago);
		if($new_date["month"] == $date["month"] and $new_date["day"] == $date["day"])
		{
			if($hour_ago == 1)
			{
				return $hour_ago." hour ago";
				exit();
			}
			else 
			{
				return $hour_ago." hours ago";
				exit();
			}
		}

		$day_ago = $new_date["day"] - $date["day"];
		$day_ago = abs($day_ago);
		if($new_date["month"] == $date["month"] and $day_ago <= 10)
		{
			if($day_ago == 1)
			{
				return $day_ago." day ago";
				exit();
			}
			else
			{
				return $day_ago." days ago";
				exit(); 
			}
		}

		$dateModified = strtotime($_date);
		$dateModified = date("M j, Y", $dateModified);
		return $dateModified;exit(0);
	}

	public static function getreplies($commentid='')
	{
		return \DB::table("reply")
		->join('users', 'users.id', '=', 'reply.user_id')
		->select('reply.id','reply.comment_id','reply.user_id','reply.course_id','reply.reply','reply.created_at','reply.updated_at','users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
		->where("reply.comment_id",'=',$commentid)->get();
	}	

	public static function getuncompletelectures($sid='') {
		$lectures = \DB::table("curriculum_lectures_quiz")
                  ->where("publish",'=','1')
                  ->where('section_id',$sid)
                  ->orderBy('section_id')
                  ->orderBy('sort_order')
                  ->pluck('lecture_quiz_id')->toArray();
	    $completed = \DB::table("curriculum_lectures_quiz")
	                  ->leftJoin('course_progress', 'curriculum_lectures_quiz.lecture_quiz_id', '=', 'course_progress.lecture_id')
	                  ->where('course_progress.status', '=', '1')
	                  ->where('course_progress.user_id', '=', \Auth::user()->id)
	                  ->where("curriculum_lectures_quiz.publish",'=','1')
	                  ->where('curriculum_lectures_quiz.section_id',$sid)
	                  ->orderBy('curriculum_lectures_quiz.section_id')
	                  ->orderBy('curriculum_lectures_quiz.sort_order')
	                  ->pluck('curriculum_lectures_quiz.lecture_quiz_id')->toArray();
	    $lqid = array_diff($lectures, $completed);
	    $alllectures = \DB::table("curriculum_lectures_quiz")
						->leftJoin('course_videos', 'course_videos.id', '=', 'curriculum_lectures_quiz.media')
						->whereIn("curriculum_lectures_quiz.lecture_quiz_id",$lqid)->where("curriculum_lectures_quiz.publish",'=','1')->orderBy('sort_order', 'asc')->get();
	    return $alllectures;
	}

	public static function getlectures($sid='')
	{
		return \DB::table("curriculum_lectures_quiz")
		->leftJoin('course_videos', 'course_videos.id', '=', 'curriculum_lectures_quiz.media')
				// ->leftJoin('course_files', 'course_files.id', '=', 'curriculum_lectures_quiz.media')
                // ->select('reply.id','reply.comment_id','reply.user_id','reply.course_id','reply.reply','reply.created_at','reply.updated_at','users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
		->where("curriculum_lectures_quiz.section_id",'=',$sid)->where("curriculum_lectures_quiz.publish",'=','1')->orderBy('sort_order', 'asc')->get();
	}

	public static function getlecturesfiles($lid='')
	{
		return \DB::table("curriculum_lectures_quiz")
		->leftJoin('course_files', 'course_files.id', '=', 'curriculum_lectures_quiz.media')
		->where("curriculum_lectures_quiz.lecture_quiz_id",'=',$lid)->orderBy('sort_order', 'asc')->get();
	}

	public static function getcoursefiles($fid='')
	{
		return \DB::table("course_files")->where("course_files.id",'=',$fid)->get();
	}

	public static function getlecturereplies($commentid='', $offset = 0)
	{
		return \DB::table("lectures_comment_reply")
		->join('users', 'users.id', '=', 'lectures_comment_reply.user_id')
		->select('lectures_comment_reply.reply_id','lectures_comment_reply.lecture_comment_id','lectures_comment_reply.user_id','lectures_comment_reply.lecture_id','lectures_comment_reply.reply_comment','lectures_comment_reply.created_at','lectures_comment_reply.modified_at','users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
		->where("lectures_comment_reply.lecture_comment_id",'=',$commentid)
		->limit(4)
		->offset($offset)
		->get();
	}

	public static function getstudentslist($fid='')
	{
		return \DB::table("course_taken")->where("course_id",'=',$fid)->count();
	}	
	
	public static function getquizcount($qid='')
	{
		return \DB::table("curriculum_quiz_questions")->where("quiz_id",'=',$qid)->count();
	}	
	
	public static function getquizresult($lid='',$qid='',$uid='')
	{
		return \DB::table("curriculum_quiz_results")->where("quiz_id",'=',$lid)->where("quiz_question_id",'=',$qid)->where("user_id",'=',$uid)->first();
	}	

	public static function getunreadnotifications()
	{
		return \DB::table("notifications")->where("user_id",'=',Auth::user()->id)->where("notify",'=',0)->count();
	}

	public static function getnotifications()
	{
		return \DB::table("notifications")
				->join('users', 'users.id', '=', 'notifications.from_id')
				->select('notifications.*','users.id', 'users.group_id', 'users.username', 'users.email', 'users.first_name', 'users.last_name')
				->where("user_id",'=',Auth::user()->id)
				->get();
	}

	public static function getforumcomments($id='')
	{
		return \DB::table("forum_comments")->where("comment_id",'=',$id)->first();
	}

	public static function getblogcomments($id='')
	{
		return \DB::table("blogcomments")
				->join('blogs', 'blogs.blogID', '=', 'blogcomments.blogID')
				->select('blogcomments.*','blogs.slug')
				->where("commentID",'=',$id)->first();

	}
	public static function gettype($id='')
	{
		$type = \DB::table("curriculum_lectures_quiz")->select('type')->where("lecture_quiz_id",'=',$id)->first();
		foreach ($type as $key => $value) {
			return $value;
		}		
	}

	public static function getcheckOrder($course_id,$lecture_quiz_id){
      $no = 1;$lno = 1;$qno = 1;
      $curriculum = \DB::table("curriculum_sections")->select('section_id')->where("course_id",'=',$course_id)->get();
      for($i=0;$i<count($curriculum);$i++){
       $lectures = self::getlectures($curriculum[$i]->section_id);
        for($j=0;$j<count($lectures);$j++){
          if($lectures[$j]->type==0){
          	if($lectures[$j]->lecture_quiz_id==$lecture_quiz_id)
              return $lno;

            $lno++;
            }else{
              if($lectures[$j]->lecture_quiz_id==$lecture_quiz_id)
                return $qno;
              $qno++;
            }
        $no++;
      }
    }
}
	//For currency 
	public static function getCurrency(){
		$currency=array(
			'AED'=>'AED','AFN'=>'AFN','ALL'=>'ALL','AMD'=>'AMD','ANG'=>'ANG','AOA'=>'AOA','ARS'=>'ARS','AUD'=>'AUD',
			'AWG'=>'AWG','AZN'=>'AZN','BAM'=>'BAM','BBD'=>'BBD','BDT'=>'BDT',
			'BGN'=>'BGN','BHD'=>'BHD','BIF'=>'BIF','BMD'=>'BMD','BND'=>'BND','BOB'=>'BOB','BRL'=>'BRL','BSD'=>'BSD','BTC'=>'BTC',
			'BTN'=>'BTN','BWP'=>'BWP','BYR'=>'BYR','BZD'=>'BZD','CAD'=>'CAD','CDF'=>'CDF','CHF'=>'CHF','CLF'=>'CLF','CLP'=>'CLP','CNY'=>'CNY','COP'=>'COP',
			'CRC'=>'CRC','CUC'=>'CUC','CUP'=>'CUP','CVE'=>'CVE','CZK'=>'CZK','DJF'=>'DJF','DKK'=>'DKK','DOP'=>'DOP','DZD'=>'DZD','EEK'=>'EEK','EGP'=>'EGP','ERN'=>'ERN',
			'ETB'=>'ETB','EUR'=>'EUR','FJD'=>'FJD','FKP'=>'FKP','GBP'=>'GBP','GEL'=>'GEL','GGP'=>'GGP','GHS'=>'GHS','GIP'=>'GIP','GMD'=>'GMD','GNF'=>'GNF','GTQ'=>'GTQ','GYD'=>'GYD',
			'HKD'=>'HKD','HNL'=>'HNL','HRK'=>'HRK','HTG'=>'HTG','HUF'=>'HUF','IDR'=>'IDR','ILS'=>'ILS','IMP'=>'IMP','INR'=>'INR','IQD'=>'IQD',
			'IRR'=>'IRR','ISK'=>'ISK','JEP'=>'JEP','JMD'=>'JMD','JOD'=>'JOD','JPY'=>'JPY','KES'=>'KES','KGS'=>'KGS',
			'KHR'=>'KHR','KMF'=>'KMF','KPW'=>'KPW','KRW'=>'KRW','KWD'=>'KWD','KYD'=>'KYD','KZT'=>'KZT','LAK'=>'LAK','LBP'=>'LBP','LKR'=>'LKR','LRD'=>'LRD','LSL'=>'LSL','LTL'=>'LTL','LVL'=>'LVL','LYD'=>'LYD','MAD'=>'MAD','MDL'=>'MDL',
			'MGA'=>'MGA','MKD'=>'MKD','MMK'=>'MMK','MNT'=>'MNT','MOP'=>'MOP','MRO'=>'MRO','MTL'=>'MTL','MUR'=>'MUR','MVR'=>'MVR',
			'MWK'=>'MWK','MXN'=>'MXN','MYR'=>'MYR','MZN'=>'MZN','NAD'=>'NAD',
			'NGN'=>'NGN','NIO'=>'NIO','NOK'=>'NOK','NPR'=>'NPR','NZD'=>'NZD','OMR'=>'OMR','PAB'=>'PAB','PEN'=>'PEN','PGK'=>'PGK','PHP'=>'PHP','PKR'=>'PKR','PLN'=>'PLN', 
			'PYG'=>'PYG','QAR'=>'QAR','RON'=>'RON','RSD'=>'RSD','RUB'=>'RUB','RWF'=>'RWF','SAR'=>'SAR','SBD'=>'SBD','SCR'=>'SCR',
			'SDG'=>'SDG','SEK'=>'SEK','SGD'=>'SGD','SHP'=>'SHP','SLL'=>'SLL','SOS'=>'SOS','SRD'=>'SRD','STD'=>'STD',
			'SVC'=>'SVC','SYP'=>'SYP','SZL'=>'SZL','THB'=>'THB','TJS'=>'TJS','TMT'=>'TMT','TND'=>'TND','TOP'=>'TOP','TRY'=>'TRY','TTD'=>'TTD','TWD'=>'TWD','TZS'=>'TZS',
			'UAH'=>'UAH','UGX'=>'UGX','USD'=>'USD','UYU'=>'UYU','UZS'=>'UZS',
			'VEF'=>'VEF','VND'=>'VND','VUV'=>'VUV','WST'=>'WST','XAF'=>'XAF','XAG'=>'XAG','XAU'=>'XAU','XCD'=>'XCD','XDR'=>'XDR','XOF'=>'XOF','XPF'=>'XPF','YER'=>'YER','ZAR'=>'ZAR',
			'ZMK'=>'ZMK','ZMW'=>'ZMW','ZWL'=>'ZWL',
			);
		// $currency = array (
  //       	'USD'=>'USD','AED'=>'AED', 'AFN'=>'AFN', 'ALL'=>'ALL', 'AMD'=>'AMD', 'ANG'=>'ANG', 'AOA'=>'AOA', 'ARS'=>'ARS', 'AUD'=>'AED', 'AWG'=>'AWG', 'AZN'=>'AZN', 'BAM'=>'BAM', 'BBD'=>'BBD', 'BDT'=>'BDT', 'BGN'=>'BGN', 'BHD'=>'BHD', 'BIF'=>'BIF', 'BMD'=>'BMD', 'NOK'=>'NOK','INR'=>'INR'
    		
  //   	);
		return $currency;
	}
	//get current currency
	public static function getCurrentcurrency($key){
		$currency=array(
			'AED'=>'AED','AFN'=>'AFN','ALL'=>'ALL','AMD'=>'AMD','ANG'=>'ANG','AOA'=>'AOA','ARS'=>'ARS','AUD'=>'AUD',
			'AWG'=>'AWG','AZN'=>'AZN','BAM'=>'BAM','BBD'=>'BBD','BDT'=>'BDT',
			'BGN'=>'BGN','BHD'=>'BHD','BIF'=>'BIF','BMD'=>'BMD','BND'=>'BND','BOB'=>'BOB','BRL'=>'BRL','BSD'=>'BSD','BTC'=>'BTC',
			'BTN'=>'BTN','BWP'=>'BWP','BYR'=>'BYR','BZD'=>'BZD','CAD'=>'CAD','CDF'=>'CDF','CHF'=>'CHF','CLF'=>'CLF','CLP'=>'CLP','CNY'=>'CNY','COP'=>'COP',
			'CRC'=>'CRC','CUC'=>'CUC','CUP'=>'CUP','CVE'=>'CVE','CZK'=>'CZK','DJF'=>'DJF','DKK'=>'DKK','DOP'=>'DOP','DZD'=>'DZD','EEK'=>'EEK','EGP'=>'EGP','ERN'=>'ERN',
			'ETB'=>'ETB','EUR'=>'EUR','FJD'=>'FJD','FKP'=>'FKP','GBP'=>'GBP','GEL'=>'GEL','GGP'=>'GGP','GHS'=>'GHS','GIP'=>'GIP','GMD'=>'GMD','GNF'=>'GNF','GTQ'=>'GTQ','GYD'=>'GYD',
			'HKD'=>'HKD','HNL'=>'HNL','HRK'=>'HRK','HTG'=>'HTG','HUF'=>'HUF','IDR'=>'IDR','ILS'=>'ILS','IMP'=>'IMP','INR'=>'INR','IQD'=>'IQD',
			'IRR'=>'IRR','ISK'=>'ISK','JEP'=>'JEP','JMD'=>'JMD','JOD'=>'JOD','JPY'=>'JPY','KES'=>'KES','KGS'=>'KGS',
			'KHR'=>'KHR','KMF'=>'KMF','KPW'=>'KPW','KRW'=>'KRW','KWD'=>'KWD','KYD'=>'KYD','KZT'=>'KZT','LAK'=>'LAK','LBP'=>'LBP','LKR'=>'LKR','LRD'=>'LRD','LSL'=>'LSL','LTL'=>'LTL','LVL'=>'LVL','LYD'=>'LYD','MAD'=>'MAD','MDL'=>'MDL',
			'MGA'=>'MGA','MKD'=>'MKD','MMK'=>'MMK','MNT'=>'MNT','MOP'=>'MOP','MRO'=>'MRO','MTL'=>'MTL','MUR'=>'MUR','MVR'=>'MVR',
			'MWK'=>'MWK','MXN'=>'MXN','MYR'=>'MYR','MZN'=>'MZN','NAD'=>'NAD',
			'NGN'=>'NGN','NIO'=>'NIO','NOK'=>'NOK','NPR'=>'NPR','NZD'=>'NZD','OMR'=>'OMR','PAB'=>'PAB','PEN'=>'PEN','PGK'=>'PGK','PHP'=>'PHP','PKR'=>'PKR','PLN'=>'PLN', 
			'PYG'=>'PYG','QAR'=>'QAR','RON'=>'RON','RSD'=>'RSD','RUB'=>'RUB','RWF'=>'RWF','SAR'=>'SAR','SBD'=>'SBD','SCR'=>'SCR',
			'SDG'=>'SDG','SEK'=>'SEK','SGD'=>'SGD','SHP'=>'SHP','SLL'=>'SLL','SOS'=>'SOS','SRD'=>'SRD','STD'=>'STD',
			'SVC'=>'SVC','SYP'=>'SYP','SZL'=>'SZL','THB'=>'THB','TJS'=>'TJS','TMT'=>'TMT','TND'=>'TND','TOP'=>'TOP','TRY'=>'TRY','TTD'=>'TTD','TWD'=>'TWD','TZS'=>'TZS',
			'UAH'=>'UAH','UGX'=>'UGX','USD'=>'USD','UYU'=>'UYU','UZS'=>'UZS',
			'VEF'=>'VEF','VND'=>'VND','VUV'=>'VUV','WST'=>'WST','XAF'=>'XAF','XAG'=>'XAG','XAU'=>'XAU','XCD'=>'XCD','XDR'=>'XDR','XOF'=>'XOF','XPF'=>'XPF','YER'=>'YER','ZAR'=>'ZAR',
			'ZMK'=>'ZMK','ZMW'=>'ZMW','ZWL'=>'ZWL',
			);
    	return $currency[$key];
	}

	public static function getCurrencymethod($userid,$price,$only='',$code='')
	{
		$currency =\DB::table("users")->select('currency')->where("id",'=',$userid)->first();
		if($currency->currency=='0'){
				$finalprice=$price;
				$method='USD';
		}else{
			$multicurrency =\DB::table("currency")->select('name','value')->where("id",'=',$currency->currency)->first();

			if(count($multicurrency)>0){
				$value=$multicurrency->value;
				$method=$multicurrency->name;
				$finalprice=floatval($price*$value);
			}else{
				//If deleted currency in adminpanel we can update USD
				\DB::table('users')->where('id', $userid)->update(['currency' => '0']);
				$finalprice=$price;
				$method='USD';
			}
		}
		if($only)
			$result = $finalprice;
		elseif($code)
			$result = $method;
		else
			$result = $method.' '.$finalprice;

		return $result; 
	}


	public static function getCurrencyid($userid)
	{
		$currency =\DB::table("users")->select('currency')->where("id",'=',$userid)->first();
		if($currency->currency=='0'){
				$countryCode='USD';
		}else{
			$multicurrency =\DB::table("currency")->select('name')->where("id",'=',$currency->currency)->first();
			$countryCode=$multicurrency->name;
		}
		
		$countryCode = strtoupper($countryCode);
		$currency = array(
		"AUD" => "&#36;" , //Australian Dollar
		"BRL" => "R&#36;" , // OR add &#8354; Brazilian Real
		"BDT" => "&#2547;", //Bangladeshi Taka
		"CAD" => "C&#36;" , //Canadian Dollar
		"CHF" => "Fr" , //Swiss Franc
		"CRC" => "&#8353;", //Costa Rican Colon
		"CZK" => "K&#269;" , //Czech Koruna
		"DKK" => "kr" , //Danish Krone
		"EUR" => "&euro;" , //Euro
		"GBP" => "&pound;" , //Pound Sterling
		"HKD" => "&#36" , //Hong Kong Dollar
		"HUF" => "Ft" , //Hungarian Forint
		"ILS" => "&#x20aa;" , //Israeli New Sheqel
		"INR" => "&#8377;", //Indian Rupee
		"ILS" => "&#8362;",	//Israeli New Shekel
		"JPY" => "&yen;" , //also use &#165; Japanese Yen
		"KZT" => "&#8376;", //Kazakhstan Tenge
		"KRW" => "&#8361;",	//Korean Won
		"KHR" => "&#6107;", //Cambodia Kampuchean Riel	
		"MYR" => "RM" , //Malaysian Ringgit 
		"MXN" => "&#36" , //Mexican Peso
		"NOK" => "kr" , //Norwegian Krone
		"NGN" => "&#8358;",	//Nigerian Naira
		"NZD" => "&#36" , //New Zealand Dollar
		"PHP" => "&#x20b1;" , //Philippine Peso
		"PKR" => "&#8360;" , //Pakistani Rupees
		"PLN" => "&#122;&#322;" ,//Polish Zloty
		"SEK" => "kr" , //Swedish Krona 
		"TWD" => "&#36;" , //Taiwan New Dollar 
		"THB" => "&#3647;" , //Thai Baht
		"TRY" => "&#8378;", //Turkish Lira
		"USD" => "&#36;" , //U.S. Dollar
		"VND" => "&#8363;"	//Vietnamese Dong
		);
		//check country code exit in array or not
		if(array_key_exists($countryCode, $currency)){
			return $currency[$countryCode];
		} 
	}

	public static function getCurrencycodes($userid)
	{
		$currency =\DB::table("users")->select('currency')->where("id",'=',$userid)->first();
		if($currency->currency=='0'){
				$method='USD';
		}else{
			$multicurrency =\DB::table("currency")->select('name','value')->where("id",'=',$currency->currency)->first();

			if(count($multicurrency)>0){
				$method=$multicurrency->name;
			}else{
				//If deleted currency in adminpanel we can update USD
				\DB::table('users')->where('id', $userid)->update(['currency' => '0']);
				$method='USD';
			}
		}
		return $method; 
	}

	public static function checkcatecourse($id,$subid='')
	{
		if(!$subid)
			return \DB::table("course")->select('cat_id')->where("cat_id",'=',$id)->first();
		else
			return \DB::table("course")->select('sub_cat_id')->where("sub_cat_id",'=',$id)->first();


	}


}
