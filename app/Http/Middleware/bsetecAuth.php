<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Validator, Input, Redirect ;
class bsetecAuth {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next  )
	{
		$except_urls = array('bsetec/*', 'core/*', 'config/*', 'forum/*', 'settings/*', 'forumcategory/*');
	    $except_urls_1 = array('fourmcomments', 'forum', 'questionanswer/index', 'customcourserequest/index');

		$getPath    = \Request::path();
		$explode    = explode('/', $getPath);
		$pathx 		= (count($explode)>0) ? $explode['0'].'/*' : ''; 
		$pathy 		= (count($explode)>0) ? $explode['0'] : ''; 

		if(\Session::get('gid') != '1'){
			if(in_array($pathx, $except_urls)){
				return Redirect::to('/')->send();
			}else{
				if(in_array($pathy, $except_urls_1)){
					return Redirect::to('/')->send();
				}
			}
		}
		$except_urls_2 = array('bsetec/module/*');
		$explodex      = explode('/', $getPath);
		$pathz 		   = (count($explodex)>2) ? $explodex['0'].'/'.$explodex['1'].'/*' : ''; 
		if(\Session::get('gid') == '1' && in_array($pathz, $except_urls_2)){
			return Redirect::to('/')->send();
		}

		return $next($request);
	}

}
