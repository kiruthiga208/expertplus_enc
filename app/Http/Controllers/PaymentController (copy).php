<?php  
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator, Input, Redirect; 
use App\Models\Course;
use App\Models\bsetec;
use App\Models\Transaction;
use App\Models\UserCredits;

use App\Models\CourseTaken;
use App\Models\Coupon;
use App\Models\Admincoupon;
use Carbon\Carbon;
use danielme85\CConverter\Currency;
use App\Models\Businessplan;
class PaymentController extends Controller {

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->transaction = new Transaction();
		$this->course = new Course();
		$this->user_credits = new UserCredits();
		$this->businessplan= new Businessplan;

        if (!defined('CNF_CURRENCY')) {
			define('CNF_CURRENCY','USD');
		}
	}

	

	function getSuccess()
	{
		if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');

		$gateway = \Omnipay::gateway('paypal');

		//get the transaction id from session, so as to update the status and order details
		$transaction = Transaction::find(\Session::get('transaction_id'));

		if($transaction->amount!=0){
		//get values from db and pass it to paypal
		$express_checkout = $this->bsetec->get_options('paypal_express_checkout');

		$gateway->setUsername($express_checkout['Username']);
		$gateway->setPassword($express_checkout['Password']);
		$gateway->setSignature($express_checkout['Signature']);
		$gateway->setTestMode($express_checkout['TestMode']);

		$currency = new Currency;
		$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $transaction->amount, $decimals = 2);

		$response = $gateway->completePurchase([
		'amount'    => floatval($paypal_amount),
		//'description'    => $payment['description'],
		'returnUrl' => url('payment/success'),
		'cancelUrl' => url('payment/failure'),
		])->send();

		$response_data = $response->getData();
		}else{
			$response_data = array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time(),
					'ACK' =>'Success',
			);
		}

		if(!empty(\Session::get('business_plan_id'))){
			$course_id  = \Session::get('business_plan_id');
			$course = $this->businessplan->getPlanbyid($course_id);
			$p_type = 'business';
			$course = $course['0'];
			$purchasetype = 1;
		}else{
			$course_id = \Session::get('course_id');
			$course = Course::find($course_id);
			$p_type = 'course';
			$purchasetype = 0;
		}

		if(isset($response_data['ACK'])){
			//process only if the acknowledgement is success
			if($response_data['ACK'] == 'Success')
			{
				$membership 			= $this->businessplan->checktype(\Session::get('uid'), $course_id);
				$putype  	 	 = 0;
				$company_id  	 = 0;
				if(count($membership)>0){
					$putype  	 = $membership['0']->type;
					$company_id  = $membership['0']->company_id;
				}
				$save_transaction['id'] = \Session::get('transaction_id');
				$save_transaction['order_details'] = json_encode($response_data);
				$save_transaction['purchase_type'] = $putype;
				$save_transaction['company_id']    = $company_id;
				$save_transaction['status'] = 'completed';
				$transaction_id = $this->save_transaction($save_transaction);
			
				// add for taken course by user
				$courseTaken = new CourseTaken;
				$courseTaken->user_id = \Session::get('uid');
				$courseTaken->course_id = $course_id;
				$courseTaken->course_type 	= $putype;
				$courseTaken->company_id 	= $company_id;
				$courseTaken->type = $p_type;
				$courseTaken->save();

				\Session::forget('business_plan_id');
				\Session::forget('course_id');
				\Session::forget('transaction_id');

				if(\Session::has('admin_discount'))
					\Session::forget('admin_discount');

				if(\Session::has('coupon_price'))
						\Session::forget('coupon_price');
				if($purchasetype==1){
					return view('course/success_subscription')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);
				}else{	
					return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);
				}
			}else{
				if($purchasetype==1){
					return view('course/success_subscription')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
				}else{
				return view('course/success')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
				}
			}
		}else{
			if($purchasetype==1){
				return view('course/success_subscription')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
			}else{
				return view('course/success')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
			}	
		}
	}

	function getFailure()
	{
		if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');
		
		$save_transaction['id'] = \Session::get('transaction_id');
		$save_transaction['status'] = 'failed';
		$save_transaction['order_details'] = json_encode(array('token'=>Input::get('token')));
		$transaction_id = $this->save_transaction($save_transaction);
		$course_id = \Session::get('course_id');
		$course = Course::find($course_id);

		\Session::forget('course_id');
		\Session::forget('transaction_id');

		if(\Session::has('admin_discount'))
			\Session::forget('admin_discount');

		if(\Session::has('coupon_price'))
			\Session::forget('coupon_price');
	
		return view('course/success')->with('course', $course)->with('status', 'failed')->with('transId', $transaction_id)->with('title', 'Course');
	}

	public function postCourseform( Request $request ){
		if($request->get('number_employee') > 0){
			$this->businessplan->insertCompanydetails($request);
		}
		if(\Session::has('course_id'))
			\Session::forget('course_id');

		if($request->get('course_id') != null){
			\Session::put('course_id', $request->get('course_id'));
			\Session::save();
		}
		if($request->get('plan_type') != null && !empty($request->get('plan_type'))){
			\Session::forget('course_id');
			if(\Session::has('purchase_type')){
				\Session::forget('purchase_type');
			}
			if(\Session::has('business_plan_id')){
				\Session::forget('business_plan_id');
			}
			\Session::put('purchase_type', 1);
			\Session::put('business_plan_id', $request->get('plan_type'));
			\Session::save();
		}
		if($request->get('page_type')!=''){
			$type = $request->get('page_type');
			return Redirect::to('payment/form/'.$type);
		}else{
			return Redirect::to('payment/form');
		}
	}

	function postFormpoints()
	{
		// get all values from form
		$course_id = Input::get('course_id');
		$course_title = Input::get('course_title');
		$points_balance = Input::get('points_balance');
		$course_points = Input::get('course_points');

		$course = Course::find($course_id);
		$user_id = \Session::get('uid');
		if(isset($course->pricing)){
			$amount = $course->pricing;
			$gamification = $this->bsetec->get_options('gamification');
			$points_balance = \bsetecHelpers::getUserPoints();
			$course_points = round($course->pricing * $gamification['equivalent_points']);
		
			if($points_balance >= $course_points){
			// echo $course_points;exit();
				// add the transaction
				$transaction['user_id'] = $user_id;
				$transaction['course_id'] = $course_id;
				$transaction['amount'] = floatval($amount);
				$transaction['status'] = 'completed';
				$save_transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
				));
				$transaction['payment_method'] = 'points';
				$transaction_id = $this->save_transaction($transaction);

				if($transaction_id){
					\bsetecHelpers::updateGamification($user_id,$course_id,'1',$course_points);		// update gamification joining using points
				}

				// add for taken course by user
				$courseTaken = new CourseTaken;
				$courseTaken->user_id = $user_id;
				$courseTaken->course_id = $course_id;
				$courseTaken->save();

				return view('course/success')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);

			} else {
	        	// payment failed: display message to customer
	        	return Redirect::to('payment/form')->withErrors(['payment_error', true]);
	    	}
		} else {
        	// payment failed: display message to customer
        	return Redirect::to('payment/form')->withErrors(['payment_error', true]);
    	}
	}

	function postForm()
	{
		// get all values from form
		$payment_method = Input::get('payment_method');
		$course_title = Input::get('course_title');
		$course_id = \Session::get('course_id');
		$gateway = \Omnipay::gateway('paypal');
		if(\Session::has('business_plan_id')){
			$course_id = \Session::get('business_plan_id');
			$p_type = 'business';
			$course = $this->businessplan->getPlanbyid(\Session::get('business_plan_id'));
			$course = $course['0'];
		}else{
			$course_id 		= \Session::get('course_id');
			$p_type = 'course';
		}
			// echo '<pre>';print_r($gateway);exit;
			if($payment_method == 'paypal_express_checkout')
			{
				$paypal = $this->bsetec->get_options('paypal_express_checkout');
			}
			elseif($payment_method == 'paypal_standard')
			{
				$paypal = $this->bsetec->get_options('paypal_standard');
				$gateway->setSolutionType('Sole');
			}

			if(\Session::has('business_plan_id')){
			$course = $this->businessplan->getPlanbyid(\Session::get('business_plan_id'));
			$course = $course['0'];
			}else{
				$course = Course::find($course_id);
			}

			if(!empty($course->pricing)){
				$amount = $course->pricing;
			}else{
					$amount = $course->business_plan_amount;
			}
			$amount = \SiteHelpers::getCurrencymethod($course->user_id,$course->pricing,'1');
			$code   = \SiteHelpers::getCurrencymethod($course->user_id,$course->pricing,'','1');



			if(\Session::has('coupon_price'))
				$amount = \Session::get('coupon_price');
			
			$currency = new Currency;
			$paypal_amount = $currency->convert($code, $to = 'USD', $value = $amount, $decimals = 2);
			
			//get values from db and pass it to paypal
			$gateway->setUsername($paypal['Username']);
			$gateway->setPassword($paypal['Password']);
			$gateway->setSignature($paypal['Signature']);

			if($paypal['TestMode'] == 'true')
			{
				$gateway->setTestMode(true);
			}
			//save the transaction details in DB
			$transaction['user_id'] = \Session::get('uid');
			$transaction['course_id'] = $course_id;
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'pending';
			$transaction['payment_method'] = $payment_method;

			$id = $this->save_transaction($transaction);

			\Session::put('transaction_id', $id);
			\Session::put('course_id', $course_id);
			\Session::save();
			
			if($amount==0){
				return Redirect::to('payment/success');
			}

			$response = $gateway->purchase([
						'amount'    => floatval($paypal_amount),
						'description' => 'Course Title: '.$course_title,
						'returnUrl' => url('payment/success'),
						'cancelUrl' => url('payment/failure'),
						])->send();

			if ($response->isRedirect()) 
			{
            	// redirect to offsite payment gateway
           		 $response->redirect();
        	} 
        	else 
        	{
            	// payment failed: display message to customer
            	return Redirect::to('payment/form')->withErrors(['payment_error', true]);
        	}
	}

	function save_transaction($data,$type = '0')
	{

		//check if the status is completed
		$completed = in_array('completed', $data) ? true : false;
		
		//check if there is transaction id, if so find it or else create a new one
		$transaction = array_key_exists('id', $data) ? Transaction::find($data['id']) : new Transaction;

		//insert all the values in object
		foreach ($data as $key => $value) 
		{
			$transaction->$key = $value;
		}
		$transaction->save();
		$transaction_id =  \DB::table('transactions')->orderBy('id', 'desc')->first();
		//process the invoice generation(get transaction details and save it in invoice table), if the status is completed
		if($completed)
		{

			$this->generate_invoice($transaction_id->id,$type);

			//save credits
			if($type == '0'){
				$this->save_credits($transaction_id->id,$type);
			}
		}
		return $transaction->id;
	}

	function generate_invoice($transaction_id,$type = '0')
	{
		// $transaction_detail = $this->transaction->get_transaction_detail($transaction_id);
		if(!empty($type) && $type==2){
			$transaction_detail = $this->transaction->get_transaction_detail_membership($transaction_id);
		}else{ 
			$transaction_detail = $this->transaction->get_transaction_detail($transaction_id);
		}
		if($transaction_detail){
			//update the order details on transaction table
			$invoice['transaction_id'] = $transaction_detail->id;
			$invoice['customer_name'] = $transaction_detail->customer_name;
			$invoice['course_title'] = $transaction_detail->course_title;
			$invoice['amount'] = $transaction_detail->amount;
			$invoice['email'] = $transaction_detail->email;
			$invoice['status'] = $transaction_detail->status;
			$invoice['payment_method'] = $transaction_detail->payment_method;
			
			$invoice['invoice_number'] = time().$transaction_id;
			$invoice['ordered_on'] = date('Y-m-d H:i:s');
			
			$this->transaction->save_invoice($invoice);

			$email = $invoice['email'];
			$this->data['invoice'] = $invoice;
		}
		// echo view('course.invoice',$this->data);exit;
		//send mail to the customer
		/*\Mail::send('course.invoice', $this->data, function($message) use($email)
		{
		    $message->subject('Invoice from expert plus');
		    $message->from('admin@expertplus.com', 'Expert Plus');
		    $message->to($email);

			//pass the same data to pdf view file and generate a pdf    
			$pdf = \PDF::loadView('course.invoice-pdf', $this->data);
			//return $pdf->download('invoice.pdf');
			//get the pdf file as data
			$attach =  $pdf->stream();
			//attach the pdf in mail
		    $message->attachData($attach, 'invoice.pdf');
		});*/
	}

	function save_credits($transaction_id,$type = "0")
	{
		//get transaction details
		$transaction = $this->transaction->find($transaction_id);

		//get commision percentage from db
		$commision_percentage = $this->bsetec->get_option('commision_percentage');
		
		//calculate the credits
		$amount = $transaction->amount;
		$admin_credit = ($amount * $commision_percentage)/100;
		$instructor_credit = $amount - $admin_credit;

		//get instructor id for the course id
		// $instructor_id = $this->course->getCourseInstructor($transaction->course_id);
		$instructor_id = 1;
		if($type=='0'){
			$instructor_id = $this->course->getCourseInstructor($transaction->course_id);
		}
		//save credit for instructor
		$credit['transaction_id'] = $transaction_id;
		$credit['instructor_user_id'] = $instructor_id;
		$credit['course_id'] = $transaction->course_id;
		$credit['by_user_id'] = $transaction->user_id;
		$credit['is_admin'] = 0;
		$credit['credits_for'] = 'course_cost';
		$credit['credit'] = $instructor_credit;
		$credit['created_at'] = time();

		$this->user_credits->save_credits($credit);
		// echo '<pre>';print_r($credit);exit;
		//save credit for instructor
		$credit['instructor_user_id'] = 0;
		$credit['is_admin'] = 1;
		$credit['credits_for'] = 'course_commision';
		$credit['credit'] = $admin_credit;
		$this->user_credits->save_credits($credit);
		// exit;
		
	}

	public function getForm($type='')
	{	

		if(!\Auth::check() && (!\Session::has('course_id') || !\Session::has('category_id'))) return Redirect::to('user/login');

	    if(\Session::has('business_plan_id') && $type==0){
	      return Redirect::to('payment/form/1');
	    }


		//default values 
		$this->data['pageTitle'] = 'Payment Page';
		$this->data['pageNote'] = 'Payment Page';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;

	    $this->data['payment']=array();

		// get payment config options
	    $pay = $this->bsetec->get_payment_method_enable();
	    foreach ($pay as $key => $method) {
	    	$this->data['payment'][$method->code] = $this->bsetec->get_options($method->code);
	    }

		//get course details
		// $this->data['course'] = Course::find(\Session::get('course_id'));
		if(\Session::has('business_plan_id')){
	      $this->data['course'] = $this->businessplan->getPlanbyid(\Session::get('business_plan_id'));
	      $this->data['course'] = $this->data['course']['0'];
	    }else{
	      //get course details
	      $this->data['course'] = Course::find(\Session::get('course_id'));
	    }

$this->data['gamification'] = $this->bsetec->get_options('gamification');
if(empty($this->data['gamification'])){
	   $this->data['gamification']['Status'] = 'false';
}
  
		if(Coupon::count() > 0)
			$this->data['coupon_status'] = true;
		else
			$this->data['coupon_status'] = false;



		// admin discount
		if(\Session::has('admin_discount'))
			\Session::forget('admin_discount');
			$periods    =  0;

    	if(!empty(\Session::has('course_id'))){

			$admin_discount = $this->Admindiscount(\Session::get('course_id'));


			if(count($admin_discount)>0  && is_array($admin_discount)){
				\Session::put('admin_discount',  $admin_discount['amount']);
				\Session::put('coupon_price',  $admin_discount['amount']);
				\Session::save();
				$this->data['admin_discount'] = true;
				$this->data['discount_amount'] = $admin_discount['discount_amount'];
				$this->data['you_pay'] = $admin_discount['amount'];

			}else{

				if(\Session::has('business_plan_id')){					
					$you_pay = $this->data['course']->business_plan_amount;
				}else{
					$you_pay = $this->data['course']['pricing'];
				}

				$this->data['admin_discount'] = false;
				$this->data['d_amount'] = false;
				$this->data['you_pay'] = $you_pay;
				$this->data['final_amount'] = false;

			}
		}else{
			      $this->data['admin_discount'] = false;
			      $this->data['d_amount'] = false;
			      if(isset($this->data['course']->pricing)){
			        $pricing    = $this->data['course']->pricing;
			      //  $no_student = $this->data['course']->no_of_students;
			      }else{
			        $pricing    = $this->data['course']->business_plan_amount;
			      //  $no_student = $this->data['course']->business_plan_solicitor_count;
			        $periods    = $this->data['course']->business_plan_periods;
			        if($periods==365){
			          $periods = 1;
			        }else{
			          $periods = 0;
			        }
			      }
			      $this->data['you_pay'] = $pricing;
  		}


  		// if(isset($no_student)){
	   //    if($no_student=='10'){
	   //      $solicitors   = '1 - 10 Solicitors';
	   //      $msolicitors  = '10 Solicitors';
	   //      $count      = 10;
	   //    }elseif ($no_student=='20'){
	   //      $solicitors   = '11 - 20 Solicitors';
	   //      $msolicitors  = '20 Solicitors';
	   //      $count      = 20;
	   //    }elseif ($no_student=='30'){
	   //      $solicitors   = '21 - 30 Solicitors';
	   //      $msolicitors  = '30 Solicitors';
	   //      $count      = 30;
	   //    }elseif ($no_student=='1'){
	   //      $count      = 1;
	   //      $msolicitors  = 'Individual Solicitors';
	   //      $solicitors   = 'Individual Solicitors';
	   //    }else{
	   //      $solicitors   = $no_student.' Solicitors';
	   //      $msolicitors  = $no_student.' Solicitors';
	   //      $count      = $no_student;
	   //    }
	   //  }else{
	   //    $count      = 1;
	   //    $msolicitors  = 'Individual Solicitors';
	   //    $solicitors   = 'Individual Solicitors';
	   //  }

	    $this->data['singleplan']  = $this->businessplan->getMonthlyplan();
	    $this->data['monthly']     = $this->businessplan->getMonthlyplanpayment();
	   // $this->data['solicitors']  = $solicitors;
	   // $this->data['msolicitors'] = $msolicitors;
	   // $this->data['usercount']   = $count;
	    $this->data['periods']     = $periods;
		$user_info=\DB::table('users')->where('id','=',\Session::get('uid'))->first();
		$this->data['user_info'] = $user_info;

		//load the view
		if($type=='1'){			
	      $this->data['pages'] = 'course.payment-form-subscription';
	    }else{
	      $this->data['pages'] = 'course.payment-form';
	    }
	    // echo "<pre>";print_r($this->data);exit;
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data);
	}

	public function postStripe( Request $request ){
		$subtype 					= 1;
		if(!empty(\Session::get('business_plan_id'))){
			$course_id  = \Session::get('business_plan_id');
			$p_type = 'business';
			$course = $this->businessplan->getPlanbyid($course_id);
			$course = $course['0'];
			$purchasetype = 1;
			$amount = $course->business_plan_amount;
			$amount = \SiteHelpers::getCurrencymethod(\Auth::user()->id,$course->business_plan_amount,'1');
			$code   = \SiteHelpers::getCurrencymethod(\Auth::user()->id,$course->business_plan_amount,'','1');
			if($course->business_plan_periods=='30'){
				$striptype = 'month';
			}else{
				$striptype = 'year';
			}
			$subtype					= 2;
		}else{
			$course_id = \Session::get('course_id');
			$p_type = 'course';
			$course = Course::find(\Session::get('course_id'));
			$amount = $course->pricing;
			$amount = \SiteHelpers::getCurrencymethod($course->user_id,$course->pricing,'1');
			$code   = \SiteHelpers::getCurrencymethod($course->user_id,$course->pricing,'','1');
			$purchasetype = 0;
		}

		if(\Session::has('coupon_price'))
			$amount = \Session::get('coupon_price');
		$currency = new Currency;
		$paypal_amount = $currency->convert($code, $to = 'USD', $value = $amount, $decimals = 2);
		$paypal_amount =round($paypal_amount);
		// get all values from form
		$payment['payment_method'] 	= $request->get('payment_method');
		$payment['course_title'] 	= $request->get('course_title');
		$payment['course_id'] 		= \Session::get('course_id');
		$payment['amount']	 		= floatval($amount);

			if($amount==0){
			//save the transaction details in DB
			$transaction['user_id'] = \Session::get('uid');
			$transaction['course_id'] = \Session::get('course_id');
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'completed';
			$transaction['payment_method'] = $request->get('payment_method');
			$transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
			));

			$transaction_id = $this->save_transaction($transaction);
			// add for taken course by user
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = \Session::get('uid');
			$courseTaken->course_id = $course_id;
			$courseTaken->type = $p_type;
			$courseTaken->save();

			\Session::forget('course_id');
			if(\Session::has('admin_discount'))
				\Session::forget('admin_discount');

			if(\Session::has('coupon_price'))
				\Session::forget('coupon_price');

			if($transaction_id){
				return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
			}
		}else{
				if($subtype==2){

					$stripeToken = $request->get('stripeToken');
					$card = array('number' => $request->get('card-number'), 'expiryMonth' => $request->get('card-expiry-month'), 'expiryYear' => $request->get('card-expiry-year'), 'cvv' => $request->get('card-cvc'), 'email' => \Session::get('eid'));

					// $c_response = $gateway->createCustomer([
					//             "card" => $card,
					//             "email" => \Session::get('eid')
					//             ])->send();
					$cust_desc = "Member for ".\Session::get('eid');

		   	    $stripe = $this->bsetec->get_options('stripe');
				\Stripe\Stripe::setApiKey($stripe['secret_key']);			
				$customer = \Stripe\Customer::create(array(
					"description" =>$cust_desc,
					"source" => $stripeToken
					));
				$response = \Stripe\Subscription::create(array(
					  "customer" => $customer->id,
					  "items" => array(
					    array(
					      "plan" => \Session::get('business_plan_id'),
					    ),
					  )
					));
					// $c_response = $gateway->createCustomer([ "source" => $request->get('stripeToken'),"email" => \Session::get('eid')])->send();
					// $customerid = $c_response->getCustomerReference();
					// $response 	= $gateway->createSubscription(
					// 						["customerReference" => $customerid,
					// 						 "plan" => \Session::get('business_plan_id')
					// 						]
					// 				)->send();
				}else{
		    $stripe = $this->bsetec->get_options('stripe');
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			$paypal_amount=100*$paypal_amount;


				try {
					$response = \Stripe\Charge::create (array (
						'card' => $request->get('stripeToken'),
						'amount' => floatval($paypal_amount),
						//'description' =>'basg',
						'currency' => 'USD'
						));
				}catch(\Stripe\Error\Card $e) {
					return redirect()
					->back()
					->with('error', 'Your credit card was been declined. Please try again or contact us.');
				}
				}
				// echo "<pre>";print_r($response);print_r($response->status);exit;
				if ($response->status == 'active') {

					$membership= $this->businessplan->checktype(\Session::get('uid'), $course_id);
					$putype  	 	 = 0;
					$company_id  	 = 0;
					if(count($membership)>0){
						$putype  	 = $membership['0']->type;
						$company_id  = $membership['0']->company_id;
					}
					//save the transaction details in DB
					$transaction['user_id'] = \Session::get('uid');
					$transaction['course_id'] = \Session::get('course_id');
					$transaction['amount'] = floatval($amount);
					$transaction['status'] = 'completed';
					$transaction['purchase_type'] = $putype;
					$transaction['company_id']    = $company_id;
					$transaction['payment_method'] = $request->get('payment_method');
					$transaction['order_details'] = json_encode(array(
							"TOKEN" => '',
							"status" => "succeeded",
							"Timestamp"=>time()
					));
					$transaction_id = $this->save_transaction($transaction, $purchasetype);

					// add for taken course by user
						$courseTaken = new CourseTaken;
						$courseTaken->user_id = \Session::get('uid');
						$courseTaken->course_id = $course_id;
						$courseTaken->course_type 	= $putype;
						$courseTaken->company_id 	= $company_id;
						$courseTaken->type = $p_type;
						$courseTaken->save();
					
					\Session::forget('business_plan_id');
					\Session::forget('course_id');
					\Session::forget('transaction_id');
					if(\Session::has('admin_discount'))
						\Session::forget('admin_discount');

					if(\Session::has('coupon_price'))
						\Session::forget('coupon_price');

					if($transaction_id){
						if($purchasetype==1){
						return view('course/success_subscription')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);
						}else{
							return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
						}
					}
				} else {
					if($purchasetype==1){
						return view('course/success_subscription')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
					}else{
						return view('course/success')->with('course', $course)->with('status', 'failed')->with('transId', '')->with('title','Course');
					}
			}
		}
		
		return Redirect::to('payment/form');
	}
	public function Admindiscount($course_id){
        $coupon = Admincoupon::where('coupon_start_date','<=',Carbon::now()->toDateString())
                    ->where('coupon_end_date','>=',Carbon::now()->toDateString())
                    ->where('coupon_status',1)
                    ->first();

        $course = Course::find($course_id);
        $course_price = $course->pricing;

        if($course->course_admin_discount == 0){
			return false;
        }
     
        if($coupon == NULL){
           return false;
        }else{
        	$discount_amount = $coupon->coupon_value;
	        if($coupon->coupon_type == 1){
	        	$discount_amount =  $course_price * ($discount_amount / 100);
	            $amount = $course_price - $discount_amount;
	        	
	        }else{
	           $amount = $course_price - $discount_amount;
	        }
        }

        if($amount < 0)
            $amount = 0;

        return array('discount_amount'=>$discount_amount,'amount'=>$amount);
   }

   // Payment API extra function

   public function getPaymentapi($cid='',$uid='',$amt=''){

   		$this->data['pageTitle'] = 'Payment Page';
		$this->data['pageNote'] = 'Payment Page';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['payment']=array();

		$taken = CourseTaken::where('course_id',$cid)->where('user_id',$uid)->first();
		//if(count($taken)==0){
			 $pay = $this->bsetec->get_payment_method_enable();
	 	      foreach ($pay as $key => $method) {
	 		$this->data['payment'][$method->code] = $this->bsetec->get_options($method->code);
		      }

			$this->data['course'] = $this->course->find($cid);
			if(Coupon::count() > 0)
				$this->data['coupon_status'] = true;
			else
				$this->data['coupon_status'] = false;
			
			$admin_discount = $this->Admindiscount($cid);
			
			if(is_array($admin_discount)){
				$this->data['admin_discount'] = true;
				$this->data['discount_amount'] = $admin_discount['discount_amount'];
				$this->data['you_pay'] = $admin_discount['amount'];
			}else{
				$this->data['admin_discount'] = false;
				$this->data['d_amount'] = false;
				$this->data['you_pay'] = $this->data['course']['pricing'];
			}
			$this->data['user_id'] = $uid;
			$this->data['already_taken']=count($taken);
			$this->data['mobamount']=$amt;
		// get payment config options
		
		//load the view
		//$this->data['pages'] = 'payment-mobile.payment-form';	
		//$theme=\bsetecHelpers::get_options('theme');
		
		//$page = 'layouts.'.$theme['theme'].'.home.index';
		$user_info=\DB::table('users')->where('id','=',$uid)->first();
		$this->data['user_info'] = $user_info;

		return view('payment-mobile.payment-form',$this->data);
   }

   // mobile payment
   public function postStripemob( Request $request ){
   	
		$course = Course::find($request->input('course_id'));
		$amount = $course->pricing;
		$coupon_price = $request->input('coupon_price');
		if($coupon_price)
			$amount = $coupon_price;

		$currency = new Currency;
		$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
		// get all values from form
		$payment['payment_method'] 	= $request->get('payment_method');
		$payment['course_title'] 	= $request->get('course_title');
		$payment['course_id'] 		= $request->input('course_id');
		$payment['amount']	 		= floatval($amount);

		$gateway = \Omnipay::gateway('stripe');

		//get values from db and pass it to paypal
		$stripe = $this->bsetec->get_options('stripe');
		$gateway->setApiKey($stripe['secret_key']);


		if($amount==0){
			//save the transaction details in DB
			$transaction['user_id'] = $request->input('user_id');
			$transaction['course_id'] = $request->input('course_id');
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'completed';
			$transaction['payment_method'] = $request->get('payment_method');
			$transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
			));

			$transaction_id = $this->save_transaction($transaction);
			// add for taken course by user
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = $request->input('user_id');
			$courseTaken->course_id = $request->input('course_id');
			$courseTaken->save();

			// \Session::forget('course_id');
			// if(\Session::has('admin_discount'))
			// 	\Session::forget('admin_discount');

			// if(\Session::has('coupon_price'))
			// 	\Session::forget('coupon_price');

			if($transaction_id){
				return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
			}
		}else{
				$response = $gateway->purchase([
							'amount'    	=> floatval($paypal_amount),
							'currency' 		=> 'USD',
							'token' 		=> $request->get('stripeToken'),
							])->send();

				if ($response->isSuccessful()) {
					//save the transaction details in DB
					$transaction['user_id'] = $request->input('user_id');
					$transaction['course_id'] = $request->input('course_id');
					$transaction['amount'] = floatval($amount);
					$transaction['status'] = 'completed';
					$transaction['payment_method'] = $request->get('payment_method');
					$transaction['order_details'] = json_encode(array(
							"TOKEN" => $response->getTransactionReference(),
							"status" => "succeeded",
							"Timestamp"=>time()
					));

					$transaction_id = $this->save_transaction($transaction);
					// add for taken course by user
					$courseTaken = new CourseTaken;
					$courseTaken->user_id = $request->input('user_id');
					$courseTaken->course_id = $request->input('course_id');
					$courseTaken->save();

					// \Session::forget('course_id');
					// if(\Session::has('admin_discount'))
					// 	\Session::forget('admin_discount');

					// if(\Session::has('coupon_price'))
					// 	\Session::forget('coupon_price');

					if($transaction_id){
						return view('payment-mobile/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
					}
				} else {

				return view('payment-mobile/success')->with('course', $course)->with('status', 'failed')->with('transId', '')->with('title','Course');
			}
		}
		return Redirect::to('payment/payment-form');
	}

	// post mobile payment --- paypal Standarad and express checkout
	function postMobform(Request $request)
	{
		// get all values from form
		$payment_method = Input::get('payment_method');
		$course_title = Input::get('course_title');
		$course_id = Input::get('course_id');
		$coupon_price = Input::get('coupon_price');
		$course_amount = Input::get('course_amount');
		
		$gateway = \Omnipay::gateway('paypal');

			
			if($payment_method == 'paypal_express_checkout')
			{
				$paypal = $this->bsetec->get_options('paypal_express_checkout');
			}
			elseif($payment_method == 'paypal_standard')
			{
				$paypal = $this->bsetec->get_options('paypal_standard');
				$gateway->setSolutionType('Sole');
			}

			$course = Course::find($course_id);
			$amount = $course->pricing;

			
			if($coupon_price)
				$amount = $coupon_price;
			if($course_amount)
				$amount = $course_amount;
			
			// if(\Session::has('coupon_price'))
			// 	$amount = \Session::get('coupon_price');
			
			$currency = new Currency;
			$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
			
			//get values from db and pass it to paypal
			$gateway->setUsername($paypal['Username']);
			$gateway->setPassword($paypal['Password']);
			$gateway->setSignature($paypal['Signature']);

			if($paypal['TestMode'] == 'true')
			{
				$gateway->setTestMode(true);
			}
			//save the transaction details in DB
			$transaction['user_id'] = $request->input('user_id');
			$transaction['course_id'] = $course_id;
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'pending';
			$transaction['payment_method'] = $payment_method;

			$id = $this->save_transaction($transaction);

			// \Session::put('transaction_id', $id);
			// \Session::put('course_id', $course_id);
			// \Session::save();
			
			if($amount==0){
				return Redirect::to('payment/successmob');
			}

			$response = $gateway->purchase([
						'amount'    => floatval($paypal_amount),
						'description' => 'Course Title: '.$course_title,
						'returnUrl' => url('payment/successmob/'.$id.'/'.$course_id.''),
						'cancelUrl' => url('payment/failuremob/'.$id.'/'.$course_id.''),
						])->send();
	
			if ($response->isRedirect()) 
			{ 
            	// redirect to offsite payment gateway
           		 $response->redirect();
        	} 
        	else 
        	{
            	// payment failed: display message to customer
            	return Redirect::to('payment/mobform')->withErrors(['payment_error', true]);
        	}
	}

	// mobile payment success 

	function getSuccessmob($tid,$cid)
	{	
		//if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');

		$gateway = \Omnipay::gateway('paypal');

		//get the transaction id from session, so as to update the status and order details
		$transaction = Transaction::find($tid);
		
		if($transaction->amount!=0){ 
		//get values from db and pass it to paypal
		$express_checkout = $this->bsetec->get_options('paypal_express_checkout');

		$gateway->setUsername($express_checkout['Username']);
		$gateway->setPassword($express_checkout['Password']);
		$gateway->setSignature($express_checkout['Signature']);
		$gateway->setTestMode($express_checkout['TestMode']);

		$currency = new Currency;
		$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $transaction->amount, $decimals = 2);

		$response = $gateway->completePurchase([
		'amount'    => floatval($paypal_amount),
		//'description'    => $payment['description'],
		'returnUrl' => url('payment/successmob/'.$tid.'/'.$cid.''),
		'cancelUrl' => url('payment/failuremob/'.$tid.'/'.$cid.''),
		])->send();

		$response_data = $response->getData();
		}else{
			$response_data = array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time(),
					'ACK' =>'Success',
			);
		}

		$course_id = $cid;
		$course = Course::find($course_id);

		if(isset($response_data['ACK'])){
			//process only if the acknowledgement is success
			if($response_data['ACK'] == 'Success')
			{
				$save_transaction['id'] = $tid;
				$save_transaction['order_details'] = json_encode($response_data);
				$save_transaction['status'] = 'completed';
				$transaction_id = $this->save_transaction($save_transaction);
			
				// add for taken course by user
				$courseTaken = new CourseTaken;
				$courseTaken->user_id = $transaction['user_id'];
				$courseTaken->course_id = $course_id;
				$courseTaken->save();
				return view('payment-mobile/success')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);
			}else{
				return view('payment-mobile/success')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
			}
		}else{
			return view('payment-mobile/success')->with('course', $course)->with('status', 'failed')->with('transId', \Session::get('transaction_id'))->with('title', 'Course');
		}
	}


	// mobile payment failure
	function getFailuremob($tid,$cid)
	{
		//if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');
		
		$save_transaction['id'] = $tid;
		$save_transaction['status'] = 'failed';
		$save_transaction['order_details'] = json_encode(array('token'=>Input::get('token')));
		$transaction_id = $this->save_transaction($save_transaction);
		$course_id = $cid;
		$course = Course::find($course_id);	
		return view('payment-mobile/success')->with('course', $course)->with('status', 'failed')->with('transId', $transaction_id)->with('title', 'Course');
	}

	function getPaymentmob()
	{
		
		if($_GET['payment_method'] == 'stripe')
		{
			$gateway = \Omnipay::gateway('stripe');
			//get values from db and pass it to stripe
			$stripe = $this->bsetec->get_options('stripe');
			$gateway->setApiKey($stripe['secret_key']);

			$response = $gateway->purchase([
							'amount'    	=> floatval($_GET['amount']),
							'currency' 		=> 'USD',
							'token' 		=> $_GET['order_details'],
							])->send();
			// echo $response->isSuccessful();exit;
			if ($response->isSuccessful()) 
			{
				//save the transaction details in DB
				$_GET['status'] = 'completed';
				$_GET['order_details'] = json_encode(array(
						"TOKEN" => $response->getTransactionReference(),
						"status" => "succeeded",
						"Timestamp"=>time()
				));
			}
			else
			{
				$_GET['status'] = 'pending';
				return 'false';
			}
		}

		if($_GET['status'] == 'completed')
		{	
			// add for taken course by user
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = $_GET['user_id'];
			$courseTaken->course_id = $_GET['course_id'];
			$courseTaken->save();
		}
		// echo '<pre>';print_r($_GET);exit;
		return $this->save_transaction($_GET);
	}


	//Method to become a membership
	public function postStripemembership( Request $request ){
		$plan_id = $request->get('membership');
		$plan_type = $request->get('membership_type');
		$payment['course_title'] = $request->get('course_title');
		$coupon_code = $request->get('coupon_code');
		$user_email = \Session::get('eid');
		$user_id = \Session::get('uid');
		$coupon_used = 0;
		$stripe_coupon = 0;
		$plan_name = 'Subscription';

		if(empty($plan_id) && !empty($payment['course_title'])){
			$purchase_type = 'course';
			$payment['course_id'] = \Session::get('course_id');
			$course = Course::find(\Session::get('course_id'));
			$amount = $course->pricing;

			if(\Session::has('coupon_price')){
				$amount = \Session::get('coupon_price');
			}
		} else if(!empty($plan_id)){
			$purchase_type = 'membership';
			if(!empty($request->get('user_email'))){
				$user_email = $request->get('user_email');
			}
			if(!empty($request->get('user_id'))){
				$user_id = $request->get('user_id');
			}
			$membership_plan = \DB::table('membership_plan')->where('plan_id',$plan_id )->first();
			if($membership_plan){
				if($plan_type == 'm'){
                    $amount = $membership_plan->plan_amount;
                    $stripe_plan = $membership_plan->stripe_plan;
                } else if($plan_type == 'y'){
                    $amount = $membership_plan->plan_amount_y;
                    $stripe_plan = $membership_plan->stripe_plan_y;
                }
				$plan_name = $membership_plan->plan_name;
				if(!empty($coupon_code)){
					$coupon_discount = \DB::table('coupon_discount')->where('coupon_code',$coupon_code )->first();
					if($coupon_discount){
	                	$discount_amount = $coupon_discount->coupon_value;
						$coupon_used = $coupon_discount->id;
						$stripe_coupon = $coupon_discount->coupon_code;
		                if($coupon_discount->coupon_type == 1){
		                    $discount_amount =  $amount * ($discount_amount / 100);
		                    $amount = $amount - $discount_amount;
		                }else{
		                    $amount = $amount - $discount_amount;
		                }
		            }
	            }
			}
		}

		if($amount==0){
			//save the transaction details in DB
			$transaction['user_id'] = $user_id;
			$transaction['course_id'] = \Session::get('course_id');
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'completed';
			$transaction['purchase_type'] = $purchase_type;
			$transaction['payment_method'] = $request->get('payment_method');
			$transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
			));

			$transaction_id = $this->save_transaction($transaction);
			// add for taken course by user
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = $user_id;
			$courseTaken->course_id = \Session::get('course_id');
			$courseTaken->save();

			\Session::forget('course_id');
			if(\Session::has('admin_discount'))
				\Session::forget('admin_discount');

			if(\Session::has('coupon_price'))
				\Session::forget('coupon_price');

			if($transaction_id){
				return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
			}
		}else{
			$order_details = array();
			$course_plan_id = \Session::get('course_id');
			$stripeToken = $request->get('stripeToken');

			$purchase_period = 'once';
			$responseisSuccessful = false;

			if($purchase_type == 'membership'){
				// Cancel and Refund if any previous active membership
				 $customerReference = $this->cancelRefundPreviousMembership();

				if($plan_type == 'm'){
                    $purchase_period = 'monthly';
                } else if($plan_type == 'y'){
                    $purchase_period = 'yearly';
                }

                // prepare and store card details on stripe
				$nameOnCard = $request->get('nameOnCard');
                $nameparts = explode(" ", $nameOnCard);
				$lastName = array_pop($nameparts);
				if(!empty($nameparts)){
					$firstName = implode(" ", $nameparts);
				} else {
					$firstName = $lastName;
					$lastName = '';
				}
				$firstName = trim($firstName);
				$lastName = trim($lastName);
				$cardnumber = $request->get('card-number');
				$expiryMonth = $request->get('card-expiry-month');
				$expiryYear = $request->get('card-expiry-year');
				$cvv = $request->get('card-cvc');
				$billingAddress1 = $request->get('billingAddress1');
				$billingCountry = $request->get('billingCountry');
				$billingCity = $request->get('billingCity');
				$billingPostcode = $request->get('billingPostcode');
				$billingState = $request->get('billingState');

				// $card = array('number' => $cardnumber, 'expiryMonth' => $expiryMonth, 'expiryYear' => $expiryYear, 'cvv' => $cvv, 'email' => $user_email);
				$card = array(
					'firstName' => $firstName,
					'lastName' => $lastName,
					'number' => $cardnumber,
					'expiryMonth' => $expiryMonth,
					'expiryYear' => $expiryYear,
					'cvv' => $cvv,
					'email' => $user_email,
					'billingAddress1' => $billingAddress1,
					'billingCountry' => $billingCountry,
					'billingCity' => $billingCity,
					'billingPostcode' => $billingPostcode,
					'billingState' => $billingState,
					);

				// create customer and subscribe to plan
				$course_plan_id = $plan_id;
				$cust_desc = "Member for ".$user_email;

		   	    $stripe = $this->bsetec->get_options('stripe');
				\Stripe\Stripe::setApiKey($stripe['secret_key']);			
				$customer = \Stripe\Customer::create(array(
					"description" =>$cust_desc,
					"source" => $stripeToken
					));

				if($stripe_coupon != ''){
					//with coupon
					$coupon_obj = \Stripe\Coupon::retrieve($stripe_coupon);
					$response = \Stripe\Subscription::create(array(
					  "customer" => $customer->id,
					  "items" => array(
					    array(
					      "plan" => $stripe_plan,
					    ),
					  ),
					   "coupon" => $coupon_obj
					));
				}else{
					//without coupon
					$response = \Stripe\Subscription::create(array(
					  "customer" => $customer->id,
					  "items" => array(
					    array(
					      "plan" => $stripe_plan,
					    ),
					  )
					));
				}
				$token_response = '';
				$order_details['customerReference'] = $customer->id;
				if(isset($response['id'])){
					$token_response = $response['id'];
					$responseisSuccessful = true;
				}

				$order_details['subscriptionReference'] = $token_response;
				$order_details['subscriptionEvent'] = $response;
			}else{
				// single course purchase
			    $stripe = $this->bsetec->get_options('stripe');
				\Stripe\Stripe::setApiKey($stripe['secret_key']);	

				try {
					$response = \Stripe\Charge::create (array (
						'card' => $stripeToken,
						'amount' => floatval($amount),
						//'description' =>'basg',
						'currency' => CNF_CURRENCY
						));
				}catch(\Stripe\Error\Card $e) {
					return redirect()
					->back()
					->with('error', 'Your credit card was been declined. Please try again or contact us.');
				}

				if ($response['status']=='succeeded') {
					$responseisSuccessful = true;
				}

			}


			if ($responseisSuccessful) {
				//save the transaction details in DB
				$transaction['user_id'] = $user_id;
				$transaction['course_id'] = $course_plan_id;
				$transaction['amount'] = floatval($amount);
				$transaction['status'] = 'completed';
				$transaction['purchase_type'] = 2;
				$transaction['purchase_period'] = $purchase_period;
				$transaction['payment_method'] = $request->get('payment_method');
				$transaction['order_details'] = json_encode($order_details);

				$transaction_id = $this->save_transaction($transaction, 2);

				if($purchase_type == 'course'){
					// add for taken course by user
					$courseTaken = new CourseTaken;
					$courseTaken->user_id = $user_id;
					$courseTaken->course_id = \Session::get('course_id');
					$courseTaken->save();
				} else {
					// update user membership
					\DB::table('users')->where('id',$user_id)->update(array('membership'=>$course_plan_id));
				}

				// reduce the use coupon count
				if($coupon_used){
					\DB::table('coupon_discount')->where('id',$coupon_used)->decrement('usage_limit');
				}

				\Session::forget('course_id');
				if(\Session::has('admin_discount'))
					\Session::forget('admin_discount');

				if(\Session::has('coupon_price'))
					\Session::forget('coupon_price');

				if($transaction_id){
					if($purchase_type == 'course'){
						return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
					} else {
						return view('membership/success')->with('status', 'success')->with('transId', $transaction_id)->with('planId', $course_plan_id)->with('title','Membership');
					}
				}
			} else {
				if($purchase_type == 'course'){
					return view('course/success')->with('course', $course)->with('status', 'failed')->with('transId', '')->with('title','Course');
				} else {
					return view('membership/success')->with('status', 'failed')->with('transId', '')->with('planId', $course_plan_id)->with('title','Membership');
				}
			}
		}

		return Redirect::to('payment/form');
	}

//Method to cancel a membership
	public function getCancelmembership()
	{
		$user_id = \Session::get('uid');
		$membership = \bsetecHelpers::getMembershipofuser();
		if($membership){
			if($membership->payment_method == 'stripe'){

				$transaction_id = $membership->id;
				$odr_details = json_decode($membership->order_details,true);
				if(isset($odr_details['subscriptionReference']) && $odr_details['subscriptionReference'] && isset($odr_details['customerReference']) && $odr_details['customerReference']){

					$subscriptionReference = $odr_details['subscriptionReference'];
					$customerReference = $odr_details['customerReference'];

				    $stripe = $this->bsetec->get_options('stripe');
					\Stripe\Stripe::setApiKey($stripe['secret_key']);	
					$subscription = \Stripe\Subscription::retrieve($subscriptionReference);
										$subscription  = $subscription->cancel();

						//update the transaction details in DB
						$transactionupdate['status'] = 'cancelled';
						\DB::table('transactions')->where('id',$transaction_id)->update($transactionupdate);
						\DB::table('users')->where('id',$user_id)->update(array('membership'=>'0'));

						if($membership->purchase_period == 'monthly'){
							$updated_at = strtotime($membership->updated_at);
							$expiredate = date("F d, Y h:i:s A e",strtotime('+1 month',$updated_at));
						} else if($membership->purchase_period == 'yearly'){
							$expiredate = date("F d, Y h:i:s A e",strtotime('+1 year',$updated_at));
						}

						$message = \Lang::get('core.membership_cancel_success').$expiredate;
						return Redirect::to('user/usermembership')->with('messagetext', $message)->with('msgstatus','success');

				}

				$message = \Lang::get('core.membership_cancel_fail');
				return Redirect::to('user/usermembership')->with('messagetext', $message)->with('msgstatus','error');

			} else if($membership->payment_method == 'paypal_express_checkout'){

				$transaction_id = $membership->id;
				$odr_details = json_decode($membership->order_details,true);
				$subscriptionReference = $odr_details['recurringProfileResponse'];

				if(isset($odr_details['recurringProfileResponse']['PROFILEID']) && $odr_details['recurringProfileResponse']['PROFILEID']){

					$PROFILEID = $odr_details['recurringProfileResponse']['PROFILEID'];

					$paypal = new PaypalController();
					//get values from db and pass it to paypal
					$paypal_info = $this->bsetec->get_options('paypal_express_checkout');
					$paypal->setUsername($paypal_info['Username']);
					$paypal->setPassword($paypal_info['Password']);
					$paypal->setApiSignature($paypal_info['Signature']);

					$recurringParams=array(
						"PROFILEID"=>$PROFILEID,
						"ACTION"=>'Cancel'
					);

					$recurringProfileResponse = $paypal->ManageRecurringPaymentsProfileStatus($recurringParams);

					if (is_array($recurringProfileResponse) && $recurringProfileResponse['ACK'] == 'Success') { //Request successful
						//update the transaction details in DB
						$odr_details['cancelEvent'.date('_Y_m_d')] = $recurringProfileResponse;
	    				$transactionupdate['order_details'] = json_encode($odr_details);
						$transactionupdate['status'] = 'cancelled';
						\DB::table('transactions')->where('id',$transaction_id)->update($transactionupdate);
						\DB::table('users')->where('id',$user_id)->update(array('membership'=>'0'));
						$updated_at = strtotime($membership->updated_at);

						if($membership->purchase_period == 'monthly'){
							$expiredate = date("F d, Y h:i:s A e",strtotime('+1 month',$updated_at));
						} else if($membership->purchase_period == 'yearly'){
							$expiredate = date("F d, Y h:i:s A e",strtotime('+1 year',$updated_at));
						}

						$message = \Lang::get('core.membership_cancel_success').$expiredate;
						return Redirect::to('user/usermembership')->with('messagetext', $message)->with('msgstatus','success');

					}
				}

				$message = \Lang::get('core.membership_cancel_fail');
				return Redirect::to('user/usermembership')->with('messagetext', $message)->with('msgstatus','error');

			}
		}
		else
		{
			$message = \Lang::get('core.membership_cancel_fail');
				return Redirect::to('user/usermembership')->with('messagetext', $message)->with('msgstatus','error');
		}
	}


//stripe type method for mobile 
   public function postStripemembershipmobile( Request $request, $user_id='', $plan_id=''){

		$plan_id = $request->get('membership');
		$plan_type = $request->get('membership_type');
		$payment['course_title'] = $request->get('course_title');
		$coupon_code = $request->get('coupon_code');
		$coupon_used = 0;
		$stripe_coupon = 0;
		$plan_name = 'Subscription';

		if(!empty($user_id)){
			$userinfo   = \DB::table('users')->where('id', $user_id)->first();
			$user_email = $userinfo->email;
		}

		if(empty($plan_id) && !empty($payment['course_title'])){
			$purchase_type = 'course';
			$payment['course_id'] = \Session::get('course_id');
			$course = Course::find(\Session::get('course_id'));
			$amount = $course->pricing;

			if(\Session::has('coupon_price')){
				$amount = \Session::get('coupon_price');
			}
		} else if(!empty($plan_id)){
			$purchase_type = 'membership';
	
			$membership_plan = \DB::table('membership_plan')->where('plan_id',$plan_id )->first();
			if($membership_plan){
				if($plan_type == 'm'){
                    $amount = $membership_plan->plan_amount;
                    $stripe_plan = $membership_plan->stripe_plan;
                } else if($plan_type == 'y'){
                    $amount = $membership_plan->plan_amount_y;
                    $stripe_plan = $membership_plan->stripe_plan_y;
                }
				$plan_name = $membership_plan->plan_name;
				if(!empty($coupon_code)){
					$coupon_discount = \DB::table('coupon_discount')->where('coupon_code',$coupon_code )->first();
					if($coupon_discount){
	                	$discount_amount = $coupon_discount->coupon_value;
						$coupon_used = $coupon_discount->id;
						$stripe_coupon = $coupon_discount->stripe_coupon;
		                if($coupon_discount->coupon_type == 1){
		                    $discount_amount =  $amount * ($discount_amount / 100);
		                    $amount = $amount - $discount_amount;
		                }else{
		                    $amount = $amount - $discount_amount;
		                }
		            }
	            }
			}
		}

		// $currency = new Currency;
		// $paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
		// // get all values from form
		// $payment['payment_method'] 	= $request->get('payment_method');
		// $payment['amount']	 		= floatval($amount);

		$gateway = \Omnipay::gateway('stripe');

		//get values from db and pass it to stripe
		$stripe = $this->bsetec->get_options('stripe');
		$gateway->setApiKey($stripe['secret_key']);


		if($amount==0){
			//save the transaction details in DB
			$transaction['user_id'] = $user_id;
			$transaction['course_id'] = \Session::get('course_id');
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'completed';
			$transaction['purchase_type'] = $purchase_type;
			$transaction['payment_method'] = $request->get('payment_method');
			$transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
			));

			$transaction_id = $this->save_transaction($transaction);
			// add for taken course by user
			$courseTaken = new CourseTaken;
			$courseTaken->user_id = $user_id;
			$courseTaken->course_id = \Session::get('course_id');
			$courseTaken->save();

			\Session::forget('course_id');
			if(\Session::has('admin_discount'))
				\Session::forget('admin_discount');

			if(\Session::has('coupon_price'))
				\Session::forget('coupon_price');

			if($transaction_id){
				return view('payment-mobile/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
			}
		}else{

			$order_details = array();
			$course_plan_id = \Session::get('course_id');
			$stripeToken = $request->get('stripeToken');
			$purchase_period = 'once';
			$responseisSuccessful = false;
			if($purchase_type == 'membership'){

				// Cancel and Refund if any previous active membership
				$customerReference = $this->cancelRefundPreviousMembership();

				if($plan_type == 'm'){
                    $purchase_period = 'monthly';
                } else if($plan_type == 'y'){
                    $purchase_period = 'yearly';
                }

                // prepare and store card details on stripe
				$nameOnCard = $request->get('nameOnCard');
                $nameparts = explode(" ", $nameOnCard);
				$lastName = array_pop($nameparts);
				if(!empty($nameparts)){
					$firstName = implode(" ", $nameparts);
				} else {
					$firstName = $lastName;
					$lastName = '';
				}
				$firstName = trim($firstName);
				$lastName = trim($lastName);
				$cardnumber = $request->get('card-number');
				$expiryMonth = $request->get('card-expiry-month');
				$expiryYear = $request->get('card-expiry-year');
				$cvv = $request->get('card-cvc');
				$billingAddress1 = $request->get('billingAddress1');
				$billingCountry = $request->get('billingCountry');
				$billingCity = $request->get('billingCity');
				$billingPostcode = $request->get('billingPostcode');
				$billingState = $request->get('billingState');

				// $card = array('number' => $cardnumber, 'expiryMonth' => $expiryMonth, 'expiryYear' => $expiryYear, 'cvv' => $cvv, 'email' => $user_email);
				$card = array(
					'firstName' => $firstName,
					'lastName' => $lastName,
					'number' => $cardnumber,
					'expiryMonth' => $expiryMonth,
					'expiryYear' => $expiryYear,
					'cvv' => $cvv,
					'email' => $user_email,
					'billingAddress1' => $billingAddress1,
					'billingCountry' => $billingCountry,
					'billingCity' => $billingCity,
					'billingPostcode' => $billingPostcode,
					'billingState' => $billingState,
					);

				// create customer and subscribe to plan
				$course_plan_id = $plan_id;
				$cust_desc = "Member for ".$user_email;
				$c_response = $gateway->createCustomer([
							"card" => $card,
							"description" => $cust_desc,
							"email" => $user_email
							])->send();
				$customerid = $c_response->getCustomerReference();
				$order_details['customerReference'] = $customerid;

				$new_stripe = new StripeController();
				$new_stripe_data["plan"] = $stripe_plan;
				if($stripe_coupon){
					$new_stripe_data["coupon"] = $stripe_coupon;
					$order_details['coupon_used'] = $coupon_used;
				}
				$response = $new_stripe->send('customers/'.$customerid.'/subscriptions',$new_stripe_data,'POST');
				$token_response = '';
				if(isset($response['id'])){
					$token_response = $response['id'];
					$responseisSuccessful = true;
				}

				// $response 	= $gateway->createSubscription([
				// 			"customerReference" => $customerid,
				// 			"plan" => $stripe_plan
				// 			])->send();
				// $token_response = $response->getSubscriptionReference();
				$order_details['subscriptionReference'] = $token_response;
				$order_details['subscriptionEvent'] = $response;
			}else{
				// single course purchase
				$response 	= $gateway->purchase([
							'amount'    	=> floatval($amount),
							'currency' 		=> CNF_CURRENCY,
							'token' 		=> $stripeToken,
							])->send();
				$token_response = $response->getTransactionReference();
				$order_details['TransactionReference'] = $token_response;
				if ($response->isSuccessful()) {
					$responseisSuccessful = true;
				}
			}


			if ($responseisSuccessful) {
				//save the transaction details in DB
				$transaction['user_id'] = $user_id;
				$transaction['course_id'] = $course_plan_id;
				$transaction['amount'] = floatval($amount);
				$transaction['status'] = 'completed';
				$transaction['purchase_type'] = 2;
				$transaction['purchase_period'] = $purchase_period;
				$transaction['payment_method'] = $request->get('payment_method');
				$transaction['order_details'] = json_encode($order_details);

				$transaction_id = $this->save_transaction($transaction, 2);

				if($purchase_type == 'course'){
					// add for taken course by user
					$courseTaken = new CourseTaken;
					$courseTaken->user_id = $user_id;
					$courseTaken->course_id = \Session::get('course_id');
					$courseTaken->save();
				} else {
					// update user membership
					\DB::table('users')->where('id',$user_id)->update(array('membership'=>$course_plan_id));
				}

				// reduce the use coupon count
				if($coupon_used){
					\DB::table('coupon_discount')->where('id',$coupon_used)->decrement('usage_limit');
				}

				\Session::forget('course_id');
				if(\Session::has('admin_discount'))
					\Session::forget('admin_discount');

				if(\Session::has('coupon_price'))
					\Session::forget('coupon_price');

				if($transaction_id){
					if($purchase_type == 'course'){
						return view('payment-mobile/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
					} else {
						return view('payment-mobile/membership_success')->with('status', 'success')->with('transId', $transaction_id)->with('planId', $course_plan_id)->with('title','Membership');
					}
				}
			} else {
				if($purchase_type == 'course'){
					return view('payment-mobile/success')->with('course', $course)->with('status', 'failed')->with('transId', '')->with('title','Course');
				} else { //echo "ji";exit;
					return view('payment-mobile/membership_success')->with('status', 'failed')->with('transId', '')->with('planId', $course_plan_id)->with('title','Membership');
				}
			}
		}

		return Redirect::to('payment/form');
	}

//Method to refund	
	public function cancelRefundPreviousMembership(){

		$customerReference = '';

		// Cancel and Refund if any previous active membership
		$umembership = \bsetecHelpers::getMembershipofuseractive();
		if($umembership){
			$utransaction_id = $umembership->id;
			$user_id = $umembership->user_id;
			$utransaction_amount = $umembership->amount;
			$uplan_name = $umembership->plan_name;
			$charge_date = strtotime($umembership->created_at);
			$now_date = time();
			$odr_details = json_decode($umembership->order_details,true);
			if(isset($odr_details['last_success_charge_date'])){
				$charge_date = strtotime($odr_details['last_success_charge_date']);
			}

			// calculate the refund amount
			$refundAmount = 0;
			if($umembership->purchase_period == 'monthly'){
				$refundAmount = ($utransaction_amount - (($utransaction_amount/30) * (floor(($now_date - $charge_date) / (60 * 60 * 24)))));
			} else if($umembership->purchase_period == 'yearly'){
				$refundAmount = ($utransaction_amount - (($utransaction_amount/365) * (floor(($now_date - $charge_date) / (60 * 60 * 24)))));
			}

			if($umembership->payment_method == 'stripe'){
				$charge_id = '';
				if(isset($odr_details['last_success_charge_date'])){
				$charge_id = $odr_details['last_success_charge'];			
				}


				$subscriptionReference = $odr_details['subscriptionReference'];
				$customerReference = $odr_details['customerReference'];

				$stripe = $this->bsetec->get_options('stripe');
				\Stripe\Stripe::setApiKey($stripe['secret_key']);	
					$subscription = \Stripe\Subscription::retrieve($subscriptionReference);
					if($refundAmount){
						$objInvoiceCollection = \Stripe\Invoice::all([
						    'subscription' => $subscriptionReference
						]);
						$objInvoiceCollection_encode = json_encode($objInvoiceCollection);
						$objInvoiceCollection_array = json_decode($objInvoiceCollection_encode);
						$refundResponse 	 = \Stripe\Refund::create(array(
									 "charge" => $objInvoiceCollection_array->data[0]->charge,
									 "amount" => $refundAmount,
							));
					    // perform the refund
						if ($refundResponse) {
							// $refund_id = $refundResponse->getTransactionReference();
							//update the transaction details in DB
							$odr_details['refundReference'] = $refundResponse->id;
							$odr_details['refundAmount'] = $refundAmount;
		    				$transactionupdate['order_details'] = json_encode($odr_details);
							\DB::table('transactions')->where('id',$utransaction_id)->update($transactionupdate);
						}
					}
					$cancelResponse  = $subscription->cancel();
						// echo "<pre>";print_r($objInvoiceCollection_array);exit;
				if ($cancelResponse) {

					//update the transaction details in DB
					$ctransactionupdate['status'] = 'closed';
					\DB::table('transactions')->where('id',$utransaction_id)->update($ctransactionupdate);
					\DB::table('users')->where('id',$user_id)->update(array('membership'=>'0'));
				}

			} else if($umembership->payment_method == 'paypal_express_checkout'){

				$charge_id = 0;
				if(isset($odr_details['last_success_charge'])){
					$charge_id = $odr_details['last_success_charge'];
				} else if(isset($odr_details['transactionResponse']['PAYMENTINFO_0_TRANSACTIONID'])){
					$charge_id = $odr_details['transactionResponse']['PAYMENTINFO_0_TRANSACTIONID'];
				}

				if(isset($odr_details['recurringProfileResponse']['PROFILEID']) && $odr_details['recurringProfileResponse']['PROFILEID'] && $charge_id){

					$paypal = new PaypalController();
					//get values from db and pass it to paypal
					$paypal_info = $this->bsetec->get_options('paypal_express_checkout');
					$paypal->setUsername($paypal_info['Username']);
					$paypal->setPassword($paypal_info['Password']);
					$paypal->setApiSignature($paypal_info['Signature']);

					$PROFILEID = $odr_details['recurringProfileResponse']['PROFILEID'];

					// perform the cancellation
					$recurringParams=array(
						"PROFILEID"=>$PROFILEID,
						"ACTION"=>'Cancel'
					);
					$recurringProfileResponse = $paypal->ManageRecurringPaymentsProfileStatus($recurringParams);
					if (is_array($recurringProfileResponse) && $recurringProfileResponse['ACK'] == 'Success') { //Request successful

						// perform the refund
						if($refundAmount){
							$currency = new Currency;
							if(CNF_CURRENCY != 'USD'){
								$refundAmount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $refundAmount, $decimals = 2);
							} else {
								$refundAmount = round($refundAmount, 2);
							}

							$refundNote = "Refund for your previously active ".$uplan_name." membership on ".CNF_APPNAME;
							$refundParams=array(
								"TRANSACTIONID"=>$charge_id,
								"REFUNDTYPE"=>'Partial',
								"AMT"=>$refundAmount,
								"CURRENCYCODE"=>CNF_CURRENCY,
								"NOTE"=>$refundNote
							);
							$refundResponse = $paypal->RefundTransaction($refundParams);
							$odr_details['refundEvent'.date('_Y_m_d')] = $refundResponse;
							$odr_details['refundAmount'] = $refundAmount;
		    				$transactionupdate['order_details'] = json_encode($odr_details);
						}

						//update the transaction details in DB
						$odr_details['cancelEvent'.date('_Y_m_d')] = $recurringProfileResponse;
	    				$transactionupdate['order_details'] = json_encode($odr_details);
						$transactionupdate['status'] = 'closed';
						\DB::table('transactions')->where('id',$utransaction_id)->update($transactionupdate);
						\DB::table('users')->where('id',$user_id)->update(array('membership'=>'0'));

					}
				}
			}
		}
		return $customerReference;
	}


	// Paypal Membership
	public function postPaypalform( Request $request )
	{
		$plan_id = $request->get('membership');
		$plan_type = $request->get('membership_type');
		$coupon_code = $request->get('coupon_code');
		$user_email = \Session::get('eid');
		$user_id = \Session::get('uid');
		$coupon_used = 0;
		$plan_name = 'Subscription';
		$purchase_type = 'membership';
		$purchase_period = 'monthly';
		$billingperiod = 'Month';

		if(!empty($request->get('user_email'))){
			$user_email = $request->get('user_email');
		}
		if(!empty($request->get('user_id'))){
			$user_id = $request->get('user_id');
		}
		
		$membership_plan = \DB::table('membership_plan')->where('plan_id',$plan_id)->first();
		if($membership_plan){
			if($plan_type == 'm'){
                $amount = $membership_plan->plan_amount;
                $purchase_period = 'monthly';
					$billingperiod = 'Month';
            } else if($plan_type == 'y'){
                $amount = $membership_plan->plan_amount_y;
                $purchase_period = 'yearly';
					$billingperiod = 'Year';
            }
			$plan_name = $membership_plan->plan_name;
			if(!empty($coupon_code)){
				$coupon_discount = \DB::table('coupon_discount')->where('coupon_code',$coupon_code )->first();
				if($coupon_discount){
                	$discount_amount = $coupon_discount->coupon_value;
					$coupon_used = $coupon_discount->id;
	                if($coupon_discount->coupon_type == 1){
	                    $discount_amount =  $amount * ($discount_amount / 100);
	                    $amount = $amount - $discount_amount;
	                }else{
	                    $amount = $amount - $discount_amount;
	                }
	            }
            }
		}


		$currency = new Currency;
		//if(CNF_CURRENCY != 'USD'){
		//	$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
		//} else {
			$paypal_amount = round($amount, 2);
		//}
		$currency_symbol = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
        $desc = "Subscription to ".$plan_name." Plan. ".$currency_symbol.$paypal_amount." per ".$billingperiod;


		$paypal = new PaypalController();
		//get values from db and pass it to paypal
		$paypal_info = $this->bsetec->get_options('paypal_express_checkout');
		$paypal->setUsername($paypal_info['Username']);
		$paypal->setPassword($paypal_info['Password']);
		$paypal->setApiSignature($paypal_info['Signature']);

		$paypal_url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
		if($paypal_info['TestMode'] == 'true'){
			$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=';
		}

		if($paypal_info['TestMode'] == 'false'){
			// Cancel and Refund if any previous active membership
			$this->cancelRefundPreviousMembership();
		}
		

		//save the transaction details in DB
		$transaction['user_id'] = $user_id;
		$transaction['course_id'] = $plan_id;
		$transaction['amount'] = floatval($amount);
		$transaction['status'] = 'pending';
		$transaction['purchase_type'] = $purchase_type;
		$transaction['purchase_period'] = $purchase_period;
		$transaction['payment_method'] = $request->get('payment_method');
		$transaction_id = $this->save_transaction($transaction, 2);

		\Session::put('transaction_id', $transaction_id);
		\Session::save();

		$requestParams = array(
		    'RETURNURL' => url('payment/paypalsuccess'), 	// Enter return URL here
		    'CANCELURL' => url('payment/paypalfailure') 	// Enter cancel URL here
		);

		$orderParams = array(
		    'LOGOIMG' => url('uploads/images/'.CNF_FLOGO), 	// Logo image URL
		    // "MAXAMT" => "10000", 							// Max transaction amount
		    "NOSHIPPING" => "1", 							// I do not want shipping
		    "BRANDNAME" => CNF_APPNAME,
		    "GIFTRECEIPTENABLE" => "0",
		    "GIFTMESSAGEENABLE" => "0",

		);
		// $item = array(
		//     'PAYMENTREQUEST_0_AMT' => $paypal_amount,
		//     'PAYMENTREQUEST_0_CURRENCYCODE' => CNF_CURRENCY,
		//     'PAYMENTREQUEST_0_ITEMAMT' => $paypal_amount,
		//     'L_PAYMENTREQUEST_0_NAME0' => CNF_APPNAME,
		//     'L_PAYMENTREQUEST_0_DESC0' => $desc,
		//     'L_PAYMENTREQUEST_0_AMT0' => $paypal_amount,
		//     'L_PAYMENTREQUEST_0_QTY0' => '1',
		//     'PAYMENTREQUEST_0_NOTIFYURL' => url('payment/paypalnotify'),
		//     'PAYMENTREQUEST_0_INVNUM' => $transaction_id  		// This field is useful to send internal transaction ID
		// );

		$item = array(
			'PAYMENTREQUEST_0_AMT' => $paypal_amount,
			'PAYMENTREQUEST_0_CURRENCYCODE' => CNF_CURRENCY,
			'PAYMENTREQUEST_0_PAYMENTACTION' => "Sale",
		    'L_BILLINGTYPE0' => 'RecurringPayments',
		    'L_BILLINGAGREEMENTDESCRIPTION0' => $desc,
		    'PAYMENTREQUEST_0_INVNUM' => $transaction_id  		// This field is useful to send internal transaction ID
		);

		$response = $paypal->SetExpressCheckout($requestParams + $orderParams + $item);

		if (isset($response) && $response['ACK'] == 'Success') { //Request successful

			//update the transaction details in DB
			$transactionupdate['order_details'] = json_encode(array(
				"TOKEN"=>$response['TOKEN'],
				"status" => "pending",
				"coupon_used" => $coupon_used,
				"desc" => $desc,
				"plan_type" => $plan_type,
				"paypal_amount" => $paypal_amount,
				"Timestamp"=>time()
			));
			\DB::table('transactions')->where('id',$transaction_id)->update($transactionupdate);

		    //Now we have to redirect user to the PayPal
		    return Redirect::to($paypal_url . urlencode($response['TOKEN']));

		// } else if (is_array($response) && $response['ACK'] == 'Failure') {
		} else {
			return view('membership/success')->with('status', 'failed')->with('transId', '')->with('planId', $plan_id)->with('title','Membership');
		}

	}


	public function getPaypalsuccess( Request $request )
	{
		if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');

		//get the transaction id from session, so as to update the status and order details
		$transaction_id = \Session::get('transaction_id');
		$transaction = Transaction::find($transaction_id);

		$paypal = new PaypalController();
		//get values from db and pass it to paypal
		$paypal_info = $this->bsetec->get_options('paypal_express_checkout');
		$paypal->setUsername($paypal_info['Username']);
		$paypal->setPassword($paypal_info['Password']);
		$paypal->setApiSignature($paypal_info['Signature']);

		$plan_id = $transaction->course_id;
		$odr_details = json_decode($transaction->order_details,true);
		$paypal_token = $odr_details['TOKEN'];
		$coupon_used = $odr_details['coupon_used'];
		$desc = $odr_details['desc'];
		$plan_type = $odr_details['plan_type'];
		$paypal_amount = $odr_details['paypal_amount'];
		$order_details['orderDetails'] = $odr_details;

		$requestParams = array('TOKEN' => $paypal_token);

		$response = $paypal->GetExpressCheckoutDetails($requestParams);
		$order_details['response'] = $response;
		$payerId=$response["PAYERID"];	//Payer id returned by paypal

		//Create request for DoExpressCheckoutPayment
		$requestParams=array(
			"TOKEN"=>$paypal_token,
			"PAYERID"=>$payerId,
			"PAYMENTREQUEST_0_AMT"=>$paypal_amount,
			"PAYMENTREQUEST_0_CURRENCYCODE"=>CNF_CURRENCY,
		    'PAYMENTREQUEST_0_NOTIFYURL' => url('payment/paypalnotify')
		);
		$transactionResponse=$paypal->DoExpressCheckoutPayment($requestParams);	//Execute transaction
		$order_details['transactionResponse'] = $transactionResponse;

		if(is_array($transactionResponse) && $transactionResponse["ACK"]=="Success"){	//Payment was successfull
			//Successful Payment

			// reduce the use coupon count
			if($coupon_used){
				\DB::table('coupon_discount')->where('id',$coupon_used)->decrement('usage_limit');
			}

			if($plan_type == 'm'){
				$profilestartdate = date("Y-m-d\TH:i:s\Z",strtotime('+1 month'));
				$billingperiod = 'Month';
			} else if($plan_type == 'y'){
				$profilestartdate = date("Y-m-d\TH:i:s\Z",strtotime('+1 year'));
				$billingperiod = 'Year';
			}

			// create recurring payment profile
			$recurringParams=array(
				"EMAIL"=>\Session::get('eid'),
				"PAYERID"=>\Session::get('uid'),
				"SUBSCRIBERNAME"=>\Session::get('fid'),
				"TOKEN"=>$paypal_token,
				"PROFILESTARTDATE"=>$profilestartdate,
				"DESC"=>$desc,
				"BILLINGPERIOD"=>$billingperiod,
				"BILLINGFREQUENCY"=>"1",
				"AMT"=>$paypal_amount,
				"CURRENCYCODE"=>CNF_CURRENCY,
				"PROFILEREFERENCE"=>$transaction_id 	// This value is returned by PayPal in IPN for reference
			);

			$recurringProfileResponse = $paypal->CreateRecurringPaymentsProfile($recurringParams);
			$order_details['recurringProfileResponse'] = $recurringProfileResponse;

			if (is_array($recurringProfileResponse) && $recurringProfileResponse['ACK'] == 'Success') { //Request successful

				$order_details['orderDetails']['status'] = 'completed';
				$save_transaction['id'] = $transaction_id;
				$save_transaction['purchase_type'] = 'membership';
				$save_transaction['order_details'] = json_encode($order_details);
				$save_transaction['status'] = 'completed';
				$transaction_id = $this->save_transaction($save_transaction, 2);
				\Session::forget('transaction_id');

				// update user membership
				$user_id = \Session::get('uid');
				\DB::table('users')->where('id',$user_id)->update(array('membership'=>$plan_id));

			}

			return view('membership/success')->with('status', 'success')->with('transId', $transaction_id)->with('planId', $plan_id)->with('title','Membership');

		} else {
			//Failed Payment
			return view('membership/success')->with('status', 'failed')->with('transId', '')->with('planId', $plan_id)->with('title','Membership');
		}

	}

	function getPaypalfailure()
	{
		// PayPal Payment failed
		if(!\Auth::check() || !\Session::has('transaction_id')) return Redirect::to('user/login');

		$transaction_id 			= \Session::get('transaction_id');
		$save_transaction['id'] 	= $transaction_id;
		$save_transaction['status'] = 'failed';
		$transaction_id 			= $this->save_transaction($save_transaction);
		$transaction 				= Transaction::find($transaction_id);
		$plan_id 					= $transaction->course_id;
		$user_id 					= $transaction->user_id;
		\DB::table('users')->where('id',$user_id)->update(array('membership'=>'0'));

		\Session::forget('transaction_id');

		return view('membership/success')->with('status', 'failed')->with('transId', '')->with('planId', $plan_id)->with('title','Membership');
	}


	function cancelprevioustestmodeMembership($userid='')
	{

		\DB::table('transactions')->where('user_id',$userid)->where('purchase_type','membership')->update(['status'=>'cancelled']);
		\DB::table('course_taken')->where('user_id',$userid)->where('course_type',2)->delete();
		return true;

	}

	function postFormmobile()
	{
		// get all values from form
		$payment_method = Input::get('payment_method');
		$course_title = Input::get('course_title');
		$user_id = Input::get('user_id');
		//$course_id = Input::get('course_id');
		$plan_id = Input::get('course_id');
		$gateway = \Omnipay::gateway('paypal');

		if($plan_id){
			$course = $this->businessplan->getPlanbyid($plan_id);
			$course = $course['0'];
		}else{
			$course_id 		= \Session::get('course_id');
		}

			// echo '<pre>';print_r($gateway);exit;
			if($payment_method == 'paypal_express_checkout')
			{
				$paypal = $this->bsetec->get_options('paypal_express_checkout');
			}
			elseif($payment_method == 'paypal_standard')
			{
				$paypal = $this->bsetec->get_options('paypal_standard');
				$gateway->setSolutionType('Sole');
			}



			if($plan_id){
				$course = $this->businessplan->getPlanbyid($plan_id);
				$course = $course['0'];
			}else{
				$course = Course::find($course_id);
			}
			
			if(!empty($course->pricing)){
				$amount = $course->pricing;
			}else{
				$amount = $course->business_plan_amount;
			}

			if(\Session::has('coupon_price'))
				$amount = \Session::get('coupon_price');
			
			$currency = new Currency;
			$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
			$paypal_amount = $amount;
			
			//get values from db and pass it to paypal
			$gateway->setUsername($paypal['Username']);
			$gateway->setPassword($paypal['Password']);
			$gateway->setSignature($paypal['Signature']);

			if($paypal['TestMode'] == 'true')
			{
				$gateway->setTestMode(true);
			}
			//save the transaction details in DB
			$transaction['user_id'] = $user_id;
			$transaction['course_id'] = $plan_id;
			$transaction['amount'] = floatval($amount);
			$transaction['status'] = 'pending';
			$transaction['payment_method'] = $payment_method;

			$id = $this->save_transaction($transaction);

			//\Session::put('transaction_id', $id);
			//\Session::put('course_id', $course_id);
			//\Session::save();
			
			if($amount==0){
				return Redirect::to('payment/successmob');
			}

			$response = $gateway->purchase([
						'amount'    => floatval($paypal_amount),
						'description' => 'Course Title: '.$course_title,
						'returnUrl' => url('payment/successmob/'.$id.'/'.$plan_id.'/1'),
						'cancelUrl' => url('payment/failuremob/'.$id.'/'.$plan_id.'/1'),
						])->send();

			if ($response->isRedirect()) 
			{
            	// redirect to offsite payment gateway
           		 $response->redirect();
        	} 
        	else 
        	{
            	// payment failed: display message to customer
            	return Redirect::to('payment/formmobile')->withErrors(['payment_error', true]);
        	}
	}


public function postStripemobile( Request $request ){

		$user_id    = $request->get('user_id');
		$course_id  = $request->get('course_id');
		$course  	= $this->businessplan->getPlanbyid($course_id);
		$course  	= $course['0'];
		$purchasetype = 1;
		$amount 	= $course->business_plan_amount;
		if($course->business_plan_periods=='30'){
			$striptype = 'month';
		}else{
			$striptype = 'year';
		}
		$subtype					= 2;

		if($course->business_plan_solicitor_count > 1){
			$companydata['number_employee'] = $course->business_plan_solicitor_count;
			$companydata['plan_type'] 		= $course_id;
			$companydata['uid'] 			= $user_id;
			$this->businessplan->insertCompanydetailsmobile($companydata);
		}

		if(!empty($user_id)){
			$userinfo   = \DB::table('users')->where('id', $user_id)->first();
			$user_email = $userinfo->email;
		}
		
		if(\Session::has('coupon_price'))
			$amount = \Session::get('coupon_price');
		$currency = new Currency;
		$paypal_amount = $currency->convert($from = CNF_CURRENCY, $to = 'USD', $value = $amount, $decimals = 2);
		// get all values from form
		$payment['payment_method'] 	= $request->get('payment_method');
		$payment['course_title'] 	= $request->get('course_title');
		$payment['course_id'] 		= $course_id;
		$payment['amount']	 		= floatval($amount);

		$gateway = \Omnipay::gateway('stripe');

		//get values from db and pass it to paypal
		$stripe = $this->bsetec->get_options('stripe');
		$gateway->setApiKey($stripe['secret_key']);


		if($amount==0){
			//save the transaction details in DB
			$transaction['user_id']   = $user_id;
			$transaction['course_id'] = $course_id;
			$transaction['amount']    = floatval($amount);
			$transaction['status']    = 'completed';
			$transaction['payment_method'] = $request->get('payment_method');
			$transaction['order_details'] = json_encode(array(
					"TOKEN" => 'success',
					"status" => "succeeded",
					"Timestamp"=>time()
			));

			$transaction_id 		= $this->save_transaction($transaction);
			// add for taken course by user
			$courseTaken 			= new CourseTaken;
			$courseTaken->user_id 	= $user_id;
			$courseTaken->course_id = $course_id;
			$courseTaken->save();


			if($transaction_id){
				return view('course/success')->with('course', $course)->with('status', 'success')->with('transId', $transaction_id)->with('title','Course');
			}
		}else{
				if($subtype==2){


					$card = array('number' => $request->get('card-number'), 'expiryMonth' => $request->get('card-expiry-month'), 'expiryYear' => $request->get('card-expiry-year'), 'cvv' => $request->get('card-cvc'), 'email' => $user_email);

					$c_response = $gateway->createCustomer([
					            "card" => $card,
					            "email" => $user_email
					            ])->send();

					// $c_response = $gateway->createCustomer([ "source" => $request->get('stripeToken'),"email" => \Session::get('eid')])->send();
					$customerid = $c_response->getCustomerReference();
					$response 	= $gateway->createSubscription(
											["customerReference" => $customerid,
											 "plan" => $course_id
											]
									)->send();
				}else{

					$response 	= $gateway->purchase([
								'amount'    	=> floatval($amount),
								'currency' 		=> CNF_CURRENCY,
								'token' 		=> $request->get('stripeToken'),
								])->send();
				}


				if ($response->isSuccessful()) {

					$membership 			= $this->businessplan->checktype(\Session::get('uid'), $course_id);
					$putype  	 	 = 0;
					$company_id  	 = 0;
					if(count($membership)>0){
						$putype  	 = $membership['0']->type;
						$company_id  = $membership['0']->company_id;
					}
					//save the transaction details in DB
					$transaction['user_id'] = $user_id;
					$transaction['course_id'] = $course_id;
					$transaction['amount'] = floatval($amount);
					$transaction['status'] = 'completed';
					$transaction['purchase_type'] = $putype;
					$transaction['company_id']    = $company_id;
					$transaction['payment_method'] = $request->get('payment_method');
					$transaction['order_details'] = json_encode(array(
							"TOKEN" => $response->getTransactionReference(),
							"status" => "succeeded",
							"Timestamp"=>time()
					));


					$transaction_id = $this->save_transaction($transaction, $purchasetype);
					// add for taken course by user
					$courseTaken 				= new CourseTaken;
					$courseTaken->user_id 		= $user_id;
					$courseTaken->course_id 	= $course_id;
					$courseTaken->course_type 	= $putype;
					$courseTaken->company_id 	= $company_id;
					$courseTaken->save();
					
					return view('payment-mobile/success_subscription')->with('course', $course)->with('title', 'Course')->with('status', 'success')->with('transId', $transaction_id);
				} else {

					return view('payment-mobile/success_subscription')->with('course', $course)->with('status', 'failed')->with('transId', '')->with('title', 'Course');
				}
		}
		
		return Redirect::to('payment/form');
	}



}
