<?php 
namespace App\Http\Controllers;
use Symfony\Component\HttpFoundation\File\File;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
//use Illuminate\Support\Facades\Request;

class BannerController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'banner';
	static $per_page	= '10';

	public function __construct()
	{
		

		$this->model = new Banner();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'banner',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'banner_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('banner');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('banner.index',$this->data);
	}	

	function getUpdate(Request $request, $id = null)
	{
		if($id=='')
		{
			$view='Add New Banner';
			$this->data['row'] = $this->model->getColumnTable('banner'); 
			return view('banner.form',$this->data);	
		}
		elseif($id)
		{	
			$row = $this->model->find($id);
			$this->data['row'] =  $row;
			return view('banner.edit-form',$this->data);
		}
	}

	/*function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('banner'); 
		}

		$this->data['id'] = $id;
		return view('banner.form',$this->data);
	}	*/

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('banner'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('banner.view',$this->data);	
	}	

	function postSave( Request $request , $id = null)
	{
		
		$id = Input::get('banner_id');
		if($id)
		{
			$banner_title= Input::get('banner_title');
			$status = (Input::get('banner_status') == '1' ? '1' : '0' );
			$file = Input::file('banner_image');
			$banner = Banner::find($request->get('banner_id'));
			$banner_path = 'uploads/banner/'.$banner->banner_image;
			if(!file_exists($banner_path)){
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if($validator->fails()){
					return Redirect::to('banner/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
					->withErrors($validator)->withInput();	
				}
			}

			if($file)
			{
				if(file_exists($banner_path)){
					\File::Delete($banner_path);
				}

				$path='uploads/banner';
				$tmp = $_FILES['banner_image']['tmp_name'];
				$name = $file->getClientOriginalName();
				$actual_file_name = time().'_'.$name;		
				if(move_uploaded_file($tmp, $path.'/'.$actual_file_name))
				{		
					Banner::where('banner_id',$id)->update(['banner_title' => $banner_title,'banner_image'=>$actual_file_name,'banner_status'=>$status]);
				}
			}
			else
			{
				Banner::where('banner_id',$id)->update(['banner_title' => $banner_title,'banner_status'=>$status]);
			}
			
			return Redirect::to('banner')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		}
		else{
			$rules = $this->validateForm();
			$validator = Validator::make($request->all(), $rules);

			if ($validator->passes()) {
				$banner_title= Input::get('banner_title');
				$status = (Input::get('banner_status') == '1') ? '1' : '0';

				$path='uploads/banner';
				$file = Input::file('banner_image');
				$tmp = $_FILES['banner_image']['tmp_name'];
				$name = $file->getClientOriginalName();
				$actual_file_name = time().'_'.$name;
				if(move_uploaded_file($tmp, $path.'/'.$actual_file_name))
				{		
					Banner::insert(['banner_title' => $banner_title,'banner_image'=>$actual_file_name,'banner_status'=>$status]);
				}
			}else{
				return Redirect::to('banner/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
				->withErrors($validator)->withInput();
			}
		}
		
	return Redirect::to('banner')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
	}
	
	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			
			// banner image delete
			foreach ($request->input('id') as $banner_id) {
				$banner = Banner::find($banner_id);
				$banner_path = 'uploads/banner/'.$banner->banner_image;
				if(file_exists($banner_path)){
					\File::Delete($banner_path);
				}
			}

			$this->model->destroy($request->input('id'));
			
			// redirect
			return Redirect::to('banner')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('banner')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			

	public function deleteBanner( Request $request ){
		$banner = Banner::find($request->get('banner_id'));
		if($banner != NULL){
			$banner_path = 'uploads/banner/'.$banner->banner_image;
			if(file_exists($banner_path)){
				\File::Delete($banner_path);
			}
		}
	}		
}
