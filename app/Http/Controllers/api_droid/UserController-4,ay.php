<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator, Input, Redirect, Response ; 

use App\Models\Users;
use App\Models\Categories;
use App\Models\Api;
use App\Models\bsetec;
use App\Models\Coupon;
use App\Models\Admincoupon;
use Carbon\Carbon;
use App\User;
use File;
use App\Models\Course;
use App\Models\CourseImages;
use App\Http\Controllers\CommonmailController;
use Intervention\Image\ImageManagerStatic as Image;
class UserController extends Controller {

	
	public function __construct()
	{
		// $this->middleware('guest');
		$this->bsetec = new Bsetec();
		$this->users = new Users();
		$this->course = new Course();
		$this->categories = new Categories();
		$this->api = new Api();
		$this->sendmail = new CommonmailController();
	}

	public function postRegister(Request $request)
	{
		//validation rules
		$rules = array(
			'username'=>'required|alpha_num|unique:users|min:2',
			'first_name'=>'required|alpha|min:2',
			'last_name'=>'required|alpha|min:2',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|between:6,12',
			'device_id'=>'required',
			);	
		$validator = Validator::make($request->all(), $rules);
		//if validation passed, save into database
		if ($validator->passes()) 
		{
			$code = rand(10000,10000000);

			$this->users->first_name = $request->input('first_name');
			$this->users->last_name = $request->input('last_name');
			$this->users->email = trim($request->input('email'));
			$this->users->username = trim($request->input('username'));
			$this->users->activation = $code;
			$this->users->group_id = 3;
			$this->users->password = \Hash::make($request->input('password'));
			$this->users->active = '1';
			$this->users->device_id = $request->input('device_id');
			$this->users->save();

			$user = $this->users->find($this->users->id);

			$result = array(
                'id'=>$user['id'],
                'username'=>$user['username'],
                'email'=>$user['email'],
                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
            );
			return Response::json(array('status'=>true, 'result'=>$result), 200); 
		}
		else
		{
			return Response::json(array(
                'status' => false,
                'errors' => array_values($validator->getMessageBag()->toArray())
            ), 400); 
		}
	}

	public function postLogin(Request $request)
	{
		
		//validation rules
		$rules = array(
			'email'=>'required|email',
			'password'=>'required|alpha_num|between:6,12',
			'device_type'=>'required',
			);	
		$validator = Validator::make($request->all(), $rules);
		//if validation passed, save into database
		if ($validator->passes()) 
		{
			if (\Auth::attempt(array('email'=>$request->input('email'), 'password'=>$request->input('password')))) 
			{
				if(\Auth::check())
				{
					$user = $this->users->find(\Auth::user()->id); 
					if($user->active =='1')
					{
						$email = Users::where('email',$request->input('email'))->first();
						$email->device_id = empty($request->input('device_id'))? $email->device_id : $request->input('device_id');
						$email->device_type = $request->input('device_type');
						$email->save();
						$result = array(
				            'id'=>$user['id'],
			                'username'=>$user['username'],
			                'email'=>$user['email'],
			                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
			            	);
			            return Response::json(array('status'=>true, 'result'=>$result), 200);
					}
					else
					{
						return Response::json(array(
						    'status' => false,
						    'errors' => 'Not Approved'
						), 400); 
					}
				}
			}
			else
			{
				return Response::json(array(
				    'status' => false,
				    'errors' => 'Username/Password incorrect'
				), 400); 
			}
		}
		else
		{
			return Response::json(array(
                'status' => false,
                'errors' => array_values($validator->getMessageBag()->toArray())
            ), 400); 
		}
	}

	public function getCategories(Request $request)
	{
		
		$categories = $this->categories
					->select('id','name', 'slug')
					->where('status','enable')
					->get();
		return Response::json(array('status'=>true, 'result'=>$categories), 200);
	}

	/*
	* @process -- course
	*/
		public function postCourse(Request $request){
			//validation rules
			$rules = array(
				'category_id'=>'numeric',
				'instruction_level'=>'alpha',
				'sort_by'=>'alpha',
				);
			$validator = Validator::make($request->all(),$rules);
			if($validator->passes()){
				$categoryid=$request->input('category_id');
				$price=$request->input('price');
				$ins_level=$request->input('instruction_level');
				$sort_by=$request->input('sort_by');
				$user_id =$request->input('user_id');
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1':$page_no;
				if(empty($categoryid) && empty($price) && empty($ins_level) && empty($sort_by)){
					return Response::json(array('status'=>false,'errors'=>'Atleast Fill One Value'),400);
				}else{
					$course = $this->api->course($categoryid, $price,$ins_level,$sort_by,$user_id,$page);
				return Response::json(array('status'=>true, 'result'=>$course), 200);
				}
				
			}else{
				return Response::json(array('status'=>false,'errors'=>array_values($validator->getMessageBag()->toArray())),400);
			}
		}

	/*public function postCourse(Request $request)
	{
		//validation rules
		$rules = array(
			'type'=>'required',
			'value'=>'required'
			);	
		$validator = Validator::make($request->all(), $rules);
		$type = $request->input('type');
		$value = $request->input('value');
		//if validation passed, save into database
		if ($validator->passes()) 
		{
			$type_values = array('category_id'=>'',
								 'price'=>array('0-0','1-49','50-99','100-199','200-299','400-499'),
								 'instruction_level'=>array('all','beginner','intermediate','advanced'),
								 'sort_by'=>array('featured','freecourses','toprated','topfree','toppaid','mostviewed')
								 );
			//validate the type and values
			if(array_key_exists($type, $type_values) && ($type=='category_id' ? is_numeric($value) : in_array($value, $type_values[$type])))
			{
				$course = $this->api->course($type, $value);
				return Response::json(array('status'=>true, 'result'=>$course), 200);
			}
			else
			{
					return Response::json(array(
	                'status' => false,
	                'errors' => 'Incorrect type or value'
	            ), 400); 
			}

		}
		else
		{
			return Response::json(array(
                'status' => false,
                'errors' => array_values($validator->getMessageBag()->toArray())
            ), 400); 
		}
		
	}*/
	/*
	*@process - Featured/Latest/Most viewed course
	*/
	public function postCourselist(Request $request){
		// if($request->input('user_id'))
		// 	$courselist = $this->api->courselist();
		// else
			$courselist = $this->api->courselist($request->input('user_id'));
		return Response::json(array('status'=>true, 'result'=>$courselist), 200);
	}
	/*
	* @process --- coursedetails
	* @param --- course id and user id
	*/
	public function postCoursedetails(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(), $rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$details = $this->api->usercoursedetails($request->input('course_id'),$request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$details), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Course Not available'),400);
			}
		}
		else{
			return Response::json(array(
                'status' => false,
                'errors' => array_values($validator->getMessageBag()->toArray())
            ), 400);
		}
	}
	/*
	* @process --- course curriculum
	*/
	public function postCoursecurriculum( Request $request )
	{

		$rules = array(
			'course_id'=>'required|numeric'
			);
		$validator = Validator::make($request->all(), $rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$curriculum = $this->api->getcurriculum($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$curriculum), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Course Not available'),400);
			}
			
		}else{
			return Response::json(array(
                'status' => false,
                'errors' => array_values($validator->getMessageBag()->toArray())
            ), 400); 
		}
	}

	public function postCoursereview(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$review = $this->api->getcoursereview($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$review), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Course Not available'),400);
			}
		}
		else
		{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process --- course view discussion
	*/
	public function postViewdiscussion(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$discussion = $this->api->getdiscussion($request->input('course_id'),$request->input('user_id'));
				$lectures = $this->api->getlecturess($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$discussion,'lecture'=>$lectures), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}

		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	
	/*
	* @process -- about instructor
	* @param -- course_id
	*/
	public function postInstructor(Request $request)
	{
		$rules = array('course_id'=>'required|numeric');
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$instructor = $this->api->getInstructor($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$instructor), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Course Not available'),400);
			}
		}
		else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process --- Add Discussion API
	*/
	public function postAdddiscussion(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'desc_cnt'=>'required',
			'lecture_id'=>'required|numeric',
			'desc_title'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->adddiscussion($request->input('course_id'),$request->input('user_id'),$request->input('desc_cnt'),$request->input('desc_title'),$request->input('lecture_id'));
				if($msg > 0)
					return Response::json(array('status'=>true, 'result'=>$msg), 200);
				else
					return Response::json(array('status'=>false,'errors'=>'Lecture ID not Available this course'),400);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process --- Edit Discussion API
	*/
	public function postUpdatediscussion(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'desc_cnt'=>'required',
			'discussion_id'=>'required|numeric',
			'desc_title'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->editdiscussion($request->input('course_id'),$request->input('user_id'),$request->input('desc_cnt'),$request->input('desc_title'),$request->input('discussion_id'));
				if($msg==1)
					return Response::json(array('status'=>true, 'result'=>'Update Successfully '), 200);
				else
					return Response::json(array('status'=>false,'errors'=>'Invalid Discussion ID'),400);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process -- Delete Discussion API
	*/
	public function postDiscussiondelete(Request $request){
		
		$rules = array(
			'user_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->destorydiscussion($request->input('user_id'),$request->input('discussion_id'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Deleted Successfully '), 200);
			else
				return Response::json(array('status'=>false,'errors'=>'Invalid Discussion ID'),400);
				
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process -- Add wishlist
	*/
	public function postWishlist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'status'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->wishlist($request->input('user_id'),$request->input('course_id'),$request->input('status'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Added Successfully '), 200);
			else if($msg == 0)
				return Response::json(array('status'=>false,'errors'=>'removed Successfuly'),200);
			else
				return Response::json(array('status'=>true, 'result'=>'Already Added '), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process --- course wishlist
	*/
	public function postCoursewishlist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$user = $this->api->userwishlist($request->input('user_id'),$page);
			//$user = empty($user)? '':$user;
			return Response::json(array('status'=>true, 'result'=>$user), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process -- Add Reply
	*/
	public function postAddreply(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			'reply_cmt'=>'required',
			// 'lecture_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->addreply($request->input('course_id'),$request->input('user_id'),$request->input('reply_cmt'),$request->input('discussion_id'));
				if($msg>0)
					return Response::json(array('status'=>true, 'result'=>$msg), 200);
				else
					return Response::json(array('status'=>false,'errors'=>'Lecture ID not Available this course'),400);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}
	}

	/*
	* @process -- Edit Reply
	*/
	public function postUpdatereply(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'reply_cmt'=>'required',
			'reply_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->updatereply($request->input('user_id'),$request->input('reply_id'),$request->input('reply_cmt'));
				if($msg==1)
					return Response::json(array('status'=>true, 'result'=>'Successfully updated'), 200);
				else
					return Response::json(array('status'=>false,'errors'=>'Invalid Reply ID'),400);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	* @process -- Delete Reply
	*/
	public function postReplydestroy(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'reply_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->destoryreply($request->input('user_id'),$request->input('reply_id'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Deleted Successfully '), 200);
			else
				return Response::json(array('status'=>false,'errors'=>'Invalid Reply ID'),400);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	*@process -- view all replies
	*/
	public function postViewreplies(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$avail = $this->api->isAvaildiscussion($request->input('course_id'),$request->input('discussion_id'));
			
			if($avail>0){
				$replies = $this->api->viewreplies($request->input('discussion_id'));
				return Response::json(array('status'=>true, 'result'=>$replies), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'Invalid Discussion and Course  ID'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	* @process -- mycourse list ( Learning)
	*/
	public function postMycourselist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'type'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$check = $this->api->Ischeckuser($request->input('user_id'));
			if($check>0){
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1':$page_no;
				$learning = $this->api->learning($request->input('user_id'),$request->input('type'),$page);
				return Response::json(array('status'=>true, 'result'=>$learning), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Invalid User ID'),400);	
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	* @process --- announcement
	*/
	public function postAnnouncement(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric'
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$announcement = $this->api->getannouncement($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$announcement), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Course Not available'),400);
			}

		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}
	}
	/*
	* @process -- forgetrequest
	*/
	public function postForgetrequest(Request $request)
	{
		$rules = array(
			'email_id'=>'required|email'
			);	
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
			$user =  User::where('email','=',$request->input('email_id'));
			if($user->count() >=1)
			{
				$user = $user->get();
				$user = $user[0];
				$token = uniqid();
				$data = array('token'=>$token);	
				$toMail = $request->input('email_id');
				$subject = env('APP_APPNAME')." REQUEST PASSWORD RESET "; 			
				$message = view('reminder', $data);
				$fromMail = env('APP_EMAIL');
				$tempname = 'reminder';
				$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
				
				$affectedRows = User::where('email', '=',$user->email)
				->update(array('reminder' => $token));
				return Response::json(array('status'=>true, 'result'=>'Reset Link send your email'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Cant find email address'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	* @process --- user course curriculum for subscribed user
	*/
	public function postUsercourse(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$cid = $request->input('course_id');
				$uid = $request->input('user_id');
				$url = url();
      			$current_url = str_replace('api/public','', $url);
				$curriculum = $this->api->getusercurriculum($request->input('course_id'),$request->input('user_id'));
				$percentage = $this->api->coursepercentage($request->input('course_id'),$request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$curriculum,'course_percentage'=>$percentage,'course_completed'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id'))) ? '1':'0','certificate_url'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id')))? $current_url.'course/certificate/'.$cid.'/'.$uid : ''), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	*@process --- Check Facebook Login
	*/
	public function postSignin(Request $request){
		
		$rules = array(
			'email_id'=>'required',
			'social_type'=>'required',
			'device_type'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$email = Users::where('email',$request->input('email_id'))->first();
			$email->device_id = empty($request->input('device_id'))? $email->device_id : $request->input('device_id');
			$email->device_type = $request->input('device_type');
			$email->save();
			$count = $this->api->Ischeckfbuser($request->input('email_id'),$request->input('social_type'));
			if(count($count)>0)
				return Response::json(array('status'=>true, 'result'=>$count), 200);
			else
				return Response::json(array('status'=>false,'errors'=>'email id is not register with '.$request->input('social_type').' / Device ID '),400);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	public function postProfileinfo(Request $request)
	{
		$rules=array(
				'user_id'=>'required|numeric',
				);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$Info = $this->api->getProfileInfo($request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$Info), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	*@process -- update profile information
	*/
	public function postUpdateinfo(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$Info = $this->api->updateProfileInfo($request->input('user_id'),$request->input('first_name'),$request->input('last_name'),$request->input('user_name'),$request->input('announcement'),$request->input('spl_promotion'));
				if($Info==1)
					return Response::json(array('status'=>true, 'result'=>'Success'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}
	}

	/*
	* @process -- Change Password
	*/
	public function postChangepassword(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'current_password'=>'required',
			'password'=>'required|confirmed|between:6,12|different:current_password',
			'password_confirmation'=>'required|between:6,12|required_with:password',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$user = User::find($request->input('user_id'));
				if(!\Hash::check($request->input('current_password'), $user->password)){
					return Response::json(array('status'=>false,'errors'=>'Enter valid Current Password'),400);	
				}else{
					$new_password = \Hash::make($request->get('password'));
					$user->password =$new_password;
					$user->save();
					return Response::json(array('status'=>true, 'result'=>'Password Updated'), 200);
				}
					
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Account Delete
	*/
	public function postAccountdestory(Request $request){
		$rules=array(
			'user_id'=>'required|numeric'
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$blog_comments 			= $this->api->userData('blogcomments','user_id',$request->input('user_id'));
				$forum_comments 		= $this->api->userData('forum_comments','user_id',$request->input('user_id'));
				$favorite 				= $this->api->userData('favorite','user_id',$request->input('user_id'));
				$quiz_results 			= $this->api->userData('curriculum_quiz_results','user_id',$request->input('user_id'));
				$featured 				= $this->api->userData('featured','user_id',$request->input('user_id'));
				$instructor 			= $this->api->userData('instructor','user_id',$request->input('user_id'));
				$invitation_users 		= $this->api->userData('invitation_users','user_id',$request->input('user_id'));
				$lectures_comments 		= $this->api->userData('lectures_comments','user_id',$request->input('user_id'));
				$lectures_comment_reply = $this->api->userData('lectures_comment_reply','user_id',$request->input('user_id'));
				$logs 					= $this->api->userData('logs','user_id',$request->input('user_id'));
				$notifications 			= $this->api->userData('notifications','user_id',$request->input('user_id'));
				$report_abuse 			= $this->api->userData('report_abuse','user_id',$request->input('user_id'));
				$subscriber_list 		= $this->api->userData('subscriber_list','user_id',$request->input('user_id'));
				$withdraw_requests 		= $this->api->userData('withdraw_requests','user_id',$request->input('user_id'));
				$course_progress 		= $this->api->userData('course_progress','user_id',$request->input('user_id'));
				$courses = Course::where('user_id',$request->input('user_id'))->get();

				if(count($courses)>0){
					foreach ($courses as $course) {
						$this->course->courseunpublish($course->course_id);
					}
				}
				User::find($request->input('user_id'))->delete();
				return Response::json(array('status'=>true, 'result'=>'Account Deleted Successfully'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);	
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- lecture details status
	*/
	public function postLecturedetails(Request $request){
		$rules=array(
				'user_id'=>'required|numeric',
				'lecture_id'=>'required|numeric',
				'course_id'=>'required|numeric',
				'status'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$status = $this->api->lecturedetails($request->input('user_id'),$request->input('course_id'),$request->input('lecture_id'),$request->input('status'));
				if($status==1)
					return Response::json(array('status'=>true, 'result'=>'Lecture Status updated Successfully'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);	
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	/*
	*@process --- search course
	*/
	public function postSearchcourse(Request $request){
			$rules=array(
				'course_search'=>'required'
				);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$search = $this->api->searchcourse($request->input('user_id'),$request->input('course_search'),$page);
			return Response::json(array('status'=>true, 'result'=>$search), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}
	}

	/*
	*@process --- searchdiscussion
	*/
	public function postSearchdiscussion(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'dis_search'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$search = $this->api->searchdiscussion($request->input('user_id'),$request->input('course_id'),$request->input('dis_search'));
				return Response::json(array('status'=>true, 'result'=>$search), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process -- updateprofile
	*/
	public function postUpdateprofile(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'cimage'=>'required'
			);
		// echo $request->input('cimage');exit();
		$image = Input::file('cimage');
						
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$what = getimagesize($image);
			
            if($what[0] < 500 || $what[1] < 500){
                $response = array(
                    'status'=>false,
                    'message'=>'Uploaded image size is '.$what[0].'px * '.$what[1].'px. The Minimum allowed size is 500px * 500px.'
                );
                return Response::json(array('status'=>false,'errors'=>$response),400);
             }else
             {
             	switch(strtolower($what['mime']))
		        {
		            case 'image/png':
		                $img_r = imagecreatefrompng($image);
		                $source_image = imagecreatefrompng($image);
		                $type = '.png';
		                break;
		            case 'image/jpeg':
		                $img_r = imagecreatefromjpeg($image);
		                $source_image = imagecreatefromjpeg($image);
		                $type = '.jpg';
		                break;
		            case 'image/gif':
		                $img_r = imagecreatefromgif($image);
		                $source_image = imagecreatefromgif($image);
		                $type = '.gif';
		                break;
		            default: die('image type not supported');
		         }
		         $base = base_path();
		         $path = str_replace('api','', $base);
		         $destination = $path.'uploads/users/';
		         $filename = $image->getClientOriginalName();

             	 //echo $filename;
         	 	$courseImages = new CourseImages;
	            $courseImages->image_title = $filename;
	            $courseImages->image_type = substr($type, 1);
	            $courseImages->image_tag = "dummy tag";
	            $courseImages->uploader_id = $request->input('user_id');
	            $courseImages->created_at = time();
	            $courseImages->updated_at = time();
	            $courseImages->save();
	            //update image hash
             	 	$courseUpdate = CourseImages::find($courseImages->id);
            		$courseUpdate->image_hash = md5($courseImages->id.$courseUpdate->created_at);
            		$courseUpdate->save();
             	 $image_n = $courseImages->id;
             	 //$filename = $request->input('user_id').$type;
    			 //$extension =$image->getClientOriginalExtension();
    				$image_size[] = array('imgW'=>'500','imgH'=>'500','value'=>'normal');
		            $image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
		            $image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

                $path = $destination.$image_n.$type;
                
                foreach($image_size as $size){
                    $save_path = ($size['value'] =='normal') ? $image_n.$type : $image_n.'_'.$size['value'].$type; 
                    Image::make($image->getRealPath())->resize($size['imgW'],$size['imgH'])->save($destination.$save_path);
                }
                
    			 
             	 $image_id = $courseImages->find($courseImages->id);
             	 
             	 $data = array();
             	 $data['avatar']=$image_id->id;
             	 $data['updated_at']=time();
             	 Users::where('id',$request->input('user_id'))->update($data);
             	 $users = $this->users->find($request->input('user_id'));
             	 
             	 $image_url=array('profile_image_url'=>$this->api->customavatar($users['email'],$users['id']));
             	 return Response::json(array('status'=>true, 'result'=>$image_url), 200);
		            
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Add Rating
	*/
	public function postAddrating(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'rating'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$search = $this->api->Addrating($request->input('user_id'),$request->input('course_id'),$request->input('review_text'),$request->input('rating'));
				return Response::json(array('status'=>true, 'result'=>'Rating Successfully Added'), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}
	
	/*
	*@process -- get Rating and review
	*/
	public function postRating(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$rating = $this->api->getrating($request->input('user_id'),$request->input('course_id'));
			return Response::json(array('status'=>true, 'result'=>$rating), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process -- Report Abuse
	*/
	public function postReportabuse(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'issue_type'=>'required',
			'details'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$this->api->reportabuse($request->input('user_id'),$request->input('course_id'),$request->input('issue_type'),$request->input('details'));
				return Response::json(array('status'=>true, 'result'=>'Report Successfully Added'), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Studentsenrolled
	*/
	public function postEnrolled(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$enrolled = $this->api->Studentsenrolled($request->input('course_id'));
			return Response::json(array('status'=>true, 'result'=>$enrolled), 200);
			
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- signupwithfb
	*/
	public function postSignup(Request $request){
		$rules = array(
				'username'=>'required|alpha_num|unique:users|min:2',
				'firstname'=>'required|alpha|min:2',
				'lastname'=>'required|alpha|min:2',
				'email'=>'required|email|unique:users',
				'password'=>'required|alpha_num|between:6,12',
				'social_id'=>'required',
				'social_type'=>'required',
				'device_id'=>'required',
				);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$code = rand(10000,10000000);
			$authen = new User;
			$authen->first_name = $request->input('firstname');
			$authen->last_name = $request->input('lastname');
			$authen->email = trim($request->input('email'));
			$authen->username = trim($request->input('username'));
			$authen->social_id = $request->input('social_id');
			$authen->social_type = $request->input('social_type');
			$authen->activation = $code;
			$authen->group_id = 3;
			$authen->password = \Hash::make($request->input('password'));
			$authen->device_id = $request->input('device_id');
			$authen->save();

			$user = $authen->find($authen->id);

			$result = array(
                'id'=>$user['id'],
                'username'=>$user['username'],
                'email'=>$user['email'],
                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
            );
			return Response::json(array('status'=>true, 'result'=>$result), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}

	}

	/*
	*@process --- Quiz Questions
	*/
	public function postQuizquestions(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$quiz = $this->api->getquiz($request->input('user_id'),$request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$quiz), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'User not subscribe this course'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	public function postChangeemail(Request $request){
		$rules=array(
			'email'=>'required|email|unique:users',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$this->api->changeemail($request->input('user_id'),$request->input('email'));
				return Response::json(array('status'=>true, 'result'=>'Successfully Updated Email ID'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>'Enter Valid User id'),400);	
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process -- Quiz questio result 
	*/
	public function postQuizresult(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'question_id'=>'required',
			'answer_id'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$question_id = $request->input('question_id');
			$answer_id = $request->input('answer_id');
			$cid =  $request->input('course_id');
			$uid = $request->input('user_id');
			 $result = $this->api->quizresult($uid,$cid,$question_id,$answer_id);
			 return Response::json(array('status'=>true, 'result'=>$result), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Delete Review
	*/
	public function postDelreview(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$this->api->reviewdestory($request->input('user_id'),$request->input('course_id'));
			 return Response::json(array('status'=>true, 'result'=>'Review Successfully Deleted'), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*process -- course list page trending and 
	*/
	public function postTrending(Request $request){
		$trending = $this->api->trending($request->input('user_id'));
		return Response::json(array('status'=>true, 'result'=>$trending), 200);
	}
	/*
	*@process -- view all
	*/
	public function postViewall(Request $request){
		$rules=array(
			'type'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$view_all = $this->api->viewall($request->input('type'),$request->input('user_id'),$page);
			return Response::json(array('status'=>true, 'result'=>$view_all), 200);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);	
		}
	}

	/*
	*@process --- Buy Course
	*/
	public function postBuycourse(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count==0){
				$course = $this->course->find($request->input('course_id'));
				if($course->pricing==0){
					$this->api->buycourse($request->input('course_id'),$request->input('user_id'));
					return Response::json(array('status'=>true, 'result'=>'Successfully Subscribed'), 200);
				}else{
					return Response::json(array('status'=>false,'errors'=>'This is Paid Course'),400);
				}
			}
			else{
				return Response::json(array('status'=>false,'errors'=>'Already subscribed'),400);
			}
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Twitter email id
	*/
	public function postUsermail(Request $request){
		$rules=array(
			'id'=>'required',
			'social_type'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$email = $this->api->checkuseremail($request->input('id'),$request->input('social_type'));
		    if(count($email)>0)
		    	return Response::json(array('status'=>true, 'email'=>$email->email), 200);	
		    else
		    	return Response::json(array('status'=>false,'errors'=>'Invalid id and type'),400);
		}else{
			return Response::json(array('status'=>false,'error'=>array_values($validator->getMessageBag()->toArray())),404);
		}
	}

	/*
	*@process --- Payment -- Paypal, Paypal Checkout, Stripe
	*/
	public function getPaymentsucess(Request $request){
		echo 'success';
	}
	

//end of controller class
}
