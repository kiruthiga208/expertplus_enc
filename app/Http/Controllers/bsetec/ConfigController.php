<?php namespace App\Http\Controllers\bsetec;

use App\Http\Controllers\Controller;
use App\Models\Core\Groups;
use App\Models\bsetec;
use App\User;
use Illuminate\Http\Request;
use Validator, Input, Redirect; 
use File;
use App\Models\Emailsettings;
use App\Http\Controllers\CommonmailController;
// use AWS;

class ConfigController extends Controller {

	public function __construct()
	{
		parent::__construct();
		$this->sendmail = new CommonmailController();
		$this->bsetec = new bsetec();
		if( \Auth::check() && \Session::get('gid') != '1')
		{
			return Redirect::to('dashboard');
		}
	}

	public function postPayment()
	{
    	$pay = $this->bsetec->get_payment_method();
		foreach ($pay as $key => $method) {
			$options = $this->bsetec->save_options($method->code, Input::get($method->code));
		 }
		return Redirect::to('bsetec/config/payment')->with('messagetext','Payment Details Saved')->with('msgstatus','success');
	}

	public function getPayment()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$pay = $this->bsetec->get_payment_method();
			 	foreach ($pay as $key => $method) {
			 		$methods[$method->code] = $this->bsetec->get_options($method->code);
			}
			
			// echo '<pre>';print_r($methods);exit;
			$this->data = array(
				'pageTitle'	=> 	'Payment',
				'pageNote'	=>  'Settings',
				'pageModule'=>  'payment',
				'active' 	=> 'payment',
				'methods' 	=> $methods
				);		
			return view('bsetec.config.payment', $this->data);
		}else{
			return Redirect::to('');
		}
	}

	public function getTheme()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$theme = $this->bsetec->get_option('theme');

			$this->data = array(
				'pageTitle'	=> 	'Theme',
				'pageNote'	=>  'List',
				'pageModule'  =>  'theme',
				'path' => app_path().'/resources/views/layouts/',
				'theme' => $theme,
				'active' => 'theme'
				
				);	
		// echo '<pre>';print_r($this->data);exit;
		// $this->data['tableGrid'] 	= $this->info['config']['grid'];		
			return view('bsetec.config.theme',$this->data);
		}else{
			return Redirect::to('');
		}
	}

	public function getChange($theme = '')
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$options = $this->bsetec->save_options('theme', array('theme'=>$theme));

		//redirect to theme listing page, with an alert
			return Redirect::to('bsetec/config/theme')->with('messagetext','Default theme modified')->with('msgstatus','success');
		}else{
			return Redirect::to('');
		}
	}
	
	public function Index()
	{	if( \Auth::check() && \Session::get('gid') == '1'){
		$this->data['active'] = '';
		return view('bsetec.config.index',$this->data);	
	}else{
		return Redirect::to('');
	}
}


static function postSave( Request $request )
{
	
	$rules = array(
		'cnf_appname'=>'required|min:2',
		'cnf_appdesc'=>'required|min:2',
		'cnf_comname'=>'required|min:2',
		'cnf_email'=>'required|email',
		);
	$validator = Validator::make($request->all(), $rules);	
	if (!$validator->fails()) 
	{
		$logo = '';
		if(!is_null(Input::file('logo')))
		{

			$file = Input::file('logo'); 
			$tmp = $_FILES['logo']['tmp_name'];
			$destinationPath ='./uploads/images/'; 
				//echo $destinationPath;exit;
			$filename = $file->getClientOriginalName();
				$extension =$file->getClientOriginalExtension(); //if you need extension of the file
				
				$logo = str_slug(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', 'b_'.pathinfo($filename, PATHINFO_FILENAME))).'.'.$extension;
				$uploadSuccess = File::move($file,'./uploads/images/'.$logo);
				chmod('./uploads/images/'.$logo, 0644);
				//$uploadSuccess = $file->move($destinationPath, $logo);
			}
			$flogo='';
			if(!is_null(Input::file('flogo')))
			{				
				$ffontfile = Input::file('flogo'); 
				
				$destinationPath ='./uploads/images/'; 
				
				//frontend logo
				$ffilename = $ffontfile->getClientOriginalName();
				
				$fextension =$ffontfile->getClientOriginalExtension(); //if you need extension of the file
				
				//$flogo = 'f_'.$ffilename;
				$flogo = str_slug(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', 'f_'.pathinfo($ffilename, PATHINFO_FILENAME))).'.'.$fextension;
				$uploadSuccess = $ffontfile->move($destinationPath, $flogo);
			}

			$fav='';
			
			if(!is_null(Input::file('fav_icon')))
			{				
				$ffontfile = Input::file('fav_icon'); 
				//echo base_path()."<br/>";
				$destinationPath = './uploads/images/'; 
				//echo $destinationPath;
				//frontend logo
				$ffilename = $ffontfile->getClientOriginalName();
				
				$fextension =$ffontfile->getClientOriginalExtension(); //if you need extension of the file
				
				$fav = str_slug(preg_replace('/[^a-zA-Z0-9_ %\[\]\.\(\)%&-]/s', '', 'fav_'. pathinfo($ffilename, PATHINFO_FILENAME))).'.'.$fextension;
				//echo $fav; exit;
				$uploadSuccess = $ffontfile->move($destinationPath, $fav);
			}


			$val  =		"<?php \n"; 
			$val .= 	"define('CNF_APPNAME','".$request->input('cnf_appname')."');\n";
			$val .= 	"define('CNF_APPDESC','".$request->input('cnf_appdesc')."');\n";
			$val .= 	"define('CNF_COMNAME','".$request->input('cnf_comname')."');\n";
			$val .= 	"define('CNF_EMAIL','".$request->input('cnf_email')."');\n";	
			$val .= 	"define('CNF_METAKEY','".$request->input('cnf_metakey')."');\n";	
			$val .= 	"define('CNF_METADESC','".$request->input('cnf_metadesc')."');\n";		
			$val .= 	"define('CNF_GROUP','".CNF_GROUP."');\n";	
			$val .= 	"define('CNF_ACTIVATION','".CNF_ACTIVATION."');\n";	
			$val .= 	"define('CNF_MULTILANG','".(!is_null($request->input('cnf_multilang')) ? 1 : 0 )."');\n";
			$val .= 	"define('CNF_LANG','".$request->input('cnf_lang')."');\n";
			$val .= 	"define('CNF_REGIST','".CNF_REGIST."');\n";	
			$val .= 	"define('CNF_FRONT','".CNF_FRONT."');\n";		
			$val .= 	"define('CNF_RECAPTCHA','".CNF_RECAPTCHA."');\n";	
			$val .= 	"define('CNF_THEME','".$request->input('cnf_theme')."');\n";		
			$val .= 	"define('CNF_RECAPTCHAPUBLICKEY','".CNF_RECAPTCHAPUBLICKEY."');\n";
			$val .= 	"define('CNF_RECAPTCHAPRIVATEKEY','".CNF_RECAPTCHAPRIVATEKEY."');\n";
			$val .= 	"define('CNF_MODE','".(!is_null($request->input('cnf_mode')) ? 'production' : 'development' )."');\n";	
			$val .= 	"define('CNF_LOGO','".($logo !=''  ? $logo : CNF_LOGO )."');\n";										
			$val .= 	"define('CNF_FLOGO','".($flogo !=''  ? $flogo : CNF_FLOGO )."');\n";	
			$val .= 	"define('CNF_FAV','".($fav !=''  ? $fav : CNF_FAV )."');\n";	
			$val .=		"define('CNF_CURRENCY','".CNF_CURRENCY."');\n";
			$val .=		"define('CNF_RTL','".(($request->input('cnf_rtl') == '1' ) ? '1' : '0' )."');";
			$val .= 	"?>";
			
			$filename = 'setting.php';
			$fp=fopen($filename,"w+"); 
			fwrite($fp,$val); 
			fclose($fp); 
			return Redirect::to('bsetec/config')->with('messagetext','Setting Has Been Save Successful')->with('msgstatus','success');
		} else {
			return Redirect::to('bsetec/config')->with('messagetext', 'The following errors occurred')->with('msgstatus','success')
			->withErrors($validator)->withInput();
		}			
		
	}




	public function getEmail()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			
			$regEmail = str_replace("/app",'',base_path())."/resources/views/user/emails/registration.blade.php";
			$resetEmail = str_replace("/app",'',base_path())."/resources/views/user/emails/auth/reminder.blade.php";
			$resetPassword = str_replace("/app",'',base_path())."/resources/views/mailtemplates/reset_password.blade.php";
			$courseFeedback = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_feedback.blade.php";
			$courseJoined  = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_joined.blade.php";
			$courseRating = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_rating.blade.php";
			$courseSubmit = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_submitted.blade.php";
			$courseSurvey = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_survey.blade.php";
			$emailActivation = str_replace("/app",'',base_path())."/resources/views/mailtemplates/email_activation.blade.php";

			$courseStudjoined = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_student_join.blade.php";
			$courseApprove = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_approved.blade.php";
			$courseUnpprove = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_unapproved.blade.php";
			$courseDelete = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_delete.blade.php";
			$courseAnnoucement = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_announcement.blade.php";

			$this->data = array(
				'groups'	=> Groups::all(),
				'pageTitle'	=> 'Blast Email',
				'pageNote'	=> 'Send email to users',
				'regEmail' 	=> file_get_contents($regEmail),
				'resetEmail' => 	file_get_contents($resetEmail),
				'resetPassword'		=> 	file_get_contents($resetPassword),
				'courseFeedback'	=> 	file_get_contents($courseFeedback),
				'courseJoined'	=> 	file_get_contents($courseJoined),
				'courseRating'	=> 	file_get_contents($courseRating),
				'courseSubmit'	=> 	file_get_contents($courseSubmit),
				'courseSurvey'	=> 	file_get_contents($courseSurvey),
				'emailActivation'	=> 	file_get_contents($emailActivation),
				'courseStudjoined'	=> 	file_get_contents($courseStudjoined),
				'courseApprove'	=> 	file_get_contents($courseApprove),
				'courseUnpprove'	=> 	file_get_contents($courseUnpprove),
				'courseDelete'	=> 	file_get_contents($courseDelete),
				'courseAnnoucement'	=> 	file_get_contents($courseAnnoucement),
				'active'		=> 'email',
				);	
			return view('bsetec.config.email',$this->data);	
		}else{
			return Redirect::to('');
		}	
		
	}
	
	function postEmail( Request $request)
	{
		
		//print_r($_POST);exit;
		$rules = array(
			'regEmail'		=> 'required|min:10',
			'resetEmail'		=> 'required|min:10',				
			);	
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) 
		{
			$regEmailFile = str_replace("/app",'',base_path())."/resources/views/user/emails/registration.blade.php";
			$resetEmailFile = str_replace("/app",'',base_path())."/resources/views/user/emails/auth/reminder.blade.php";	
			$resetPassword = str_replace("/app",'',base_path())."/resources/views/mailtemplates/reset_password.blade.php";
			$courseFeedback = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_feedback.blade.php";
			$courseJoined  = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_joined.blade.php";
			$courseRating = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_rating.blade.php";
			$courseSubmit = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_submitted.blade.php";
			$courseSurvey = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_survey.blade.php";	
			$emailActivation = str_replace("/app",'',base_path())."/resources/views/mailtemplates/email_activation.blade.php";	
			$courseStudjoined = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_student_join.blade.php";
			$courseApprove = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_approved.blade.php";
			$courseUnpprove = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_unapproved.blade.php";
			$courseDelete = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_delete.blade.php";	
			$courseAnnoucement = str_replace("/app",'',base_path())."/resources/views/mailtemplates/course_announcement.blade.php";

			$fp=fopen($regEmailFile,"w+"); 				
			fwrite($fp,$_POST['regEmail']); 
			fclose($fp);	
			
			$fp=fopen($resetEmailFile,"w+"); 				
			fwrite($fp,$_POST['resetEmail']); 
			fclose($fp);

			$fp=fopen($resetPassword,"w+"); 				
			fwrite($fp,$_POST['resetPassword']); 
			fclose($fp);

			$fp=fopen($courseFeedback,"w+"); 				
			fwrite($fp,$_POST['courseFeedback']); 
			fclose($fp);

			$fp=fopen($courseJoined,"w+"); 				
			fwrite($fp,$_POST['courseJoined']); 
			fclose($fp);

			$fp=fopen($courseRating,"w+"); 				
			fwrite($fp,$_POST['courseRating']); 
			fclose($fp);

			$fp=fopen($courseSubmit,"w+"); 				
			fwrite($fp,$_POST['courseSubmit']); 
			fclose($fp);

			$fp=fopen($courseSurvey,"w+"); 				
			fwrite($fp,$_POST['courseSurvey']); 
			fclose($fp);

			$fp=fopen($emailActivation,"w+"); 				
			fwrite($fp,$_POST['emailActivation']); 
			fclose($fp);

			$fp=fopen($courseStudjoined,"w+"); 				
			fwrite($fp,$_POST['courseStudjoined']); 
			fclose($fp);

			$fp=fopen($courseApprove,"w+"); 				
			fwrite($fp,$_POST['courseApprove']); 
			fclose($fp);

			$fp=fopen($courseUnpprove,"w+"); 				
			fwrite($fp,$_POST['courseUnpprove']); 
			fclose($fp);

			$fp=fopen($courseDelete,"w+"); 				
			fwrite($fp,$_POST['courseDelete']); 
			fclose($fp);

			$fp=fopen($courseAnnoucement,"w+"); 				
			fwrite($fp,$_POST['courseAnnoucement']); 
			fclose($fp);
			
			
			return Redirect::to('bsetec/config/email')->with('messagetext', 'Email Has Been Updated')->with('msgstatus','success');	
			
		}	else {

			return Redirect::to('bsetec/config/email')->with('messagetext', 'The following errors occurred')->with('msgstatus','success')
			->withErrors($validator)->withInput();
		}
		
	}
	
	public function getSecurity()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$this->data = array(
				'groups'	=> Groups::all(),
				'pageTitle'	=> 'Login And Security',
				'pageNote'	=> 'Login Configuration and Setting',
				'active'	=> 'security'
				
				);
			
			
			return view('bsetec.config.security',$this->data);
		}else{
			return Redirect::to('');
		}		
		
	}	
	
	public function postSocial()
	{	
		
		
		$rules = array(
			
			);	
		$validator = Validator::make(Input::all(), $rules);	
		if ($validator->passes()) 
		{
			
			$content = '<?php
			return array(	
				"base_url"   => "'.URL::to('').'/user/socmed",
				"providers"  => array (
					"Google"     => array (
						"enabled"    => '.(!is_null($request->input('GOOGLE_ENABLE')) ? 'true':'false').',
						"keys"       => array ( "id" => "'.$request->input('GOOGLE_ID').'", "secret" => "'.$request->input('GOOGLE_SECRET').'" ),
						),
"Facebook"   => array (
	"enabled"    => '.(!is_null($request->input('FB_ENABLE')) ? 'true':'false').',
	"keys"       => array ( "id" => "'.$request->input('FB_ID').'", "secret" => "'.$request->input('FB_SECRET').'" ),
	),
"Twitter"    => array (
	"enabled"    => '.(!is_null($request->input('TWIT_ENABLE')) ? 'true':'false').',
	"keys"       => array ( "key" => "'.$request->input('TWIT_ID').'", "secret" => "'.$request->input('TWIT_SECRET').'" )
	)
),
);';

$hybrid = public_path() ."protected/app/config/hybridauth.php";

$fp=fopen($hybrid,"w+"); 				
fwrite($fp,$content); 
fclose($fp);		
return Redirect::to('config/security')->with('messagetext','Social Media Has Been Updated')->with('msgstatus','success');
}	else {

	return Redirect::to('config/security')->with('messagetext', 'The following errors occurred')->with('msgstatus','error')
	->withErrors($validator)->withInput();
}	
}		


public function postLogin( Request $request)
{

	$rules = array(

		);
	$validator = Validator::make($request->all(), $rules);	
	if ($validator->passes()) {
		$val  =		"<?php \n"; 
		$val .= 	"define('CNF_APPNAME','".CNF_APPNAME."');\n";
		$val .= 	"define('CNF_APPDESC','".CNF_APPDESC."');\n";
		$val .= 	"define('CNF_COMNAME','".CNF_COMNAME."');\n";
		$val .= 	"define('CNF_EMAIL','".CNF_EMAIL."');\n";
		$val .= 	"define('CNF_METAKEY','".CNF_METAKEY."');\n";	
		$val .= 	"define('CNF_METADESC','".CNF_METADESC."');\n";					
		$val .= 	"define('CNF_GROUP','".$request->input('CNF_GROUP')."');\n";	
		$val .= 	"define('CNF_ACTIVATION','".$request->input('CNF_ACTIVATION')."');\n";	
		$val .= 	"define('CNF_MULTILANG','".CNF_MULTILANG."');\n";
		$val .= 	"define('CNF_LANG','".CNF_LANG."');\n";		
		$val .= 	"define('CNF_REGIST','".(!is_null($request->input('CNF_REGIST')) ? 'true':'false')."');\n";		
		$val .= 	"define('CNF_FRONT','".(!is_null($request->input('CNF_FRONT')) ? 'true':'false')."');\n";		
		$val .= 	"define('CNF_RECAPTCHA','".(!is_null($request->input('CNF_RECAPTCHA')) ? 'true':'false')."');\n";	
		$val .= 	"define('CNF_THEME','".CNF_THEME."');\n";	
		$val .= 	"define('CNF_RECAPTCHAPUBLICKEY','');\n";
		$val .= 	"define('CNF_RECAPTCHAPRIVATEKEY','');\n";
		$val .= 	"define('CNF_MODE','".CNF_MODE."');\n";	
		$val .= 	"define('CNF_LOGO','".CNF_LOGO."');\n";									
		$val .= 	"define('CNF_FLOGO','".CNF_FLOGO."');\n";
		$val .= 	"define('CNF_FAV','".CNF_FAV."');\n";		
		$val .=		"define('CNF_CURRENCY','".CNF_CURRENCY."');\n"; 
		$val .=		"define('CNF_RTL','".CNF_RTL."');" ;
		$val .= 	"?>";
		
		$filename = 'setting.php';
		$fp=fopen($filename,"w+"); 
		fwrite($fp,$val); 
		fclose($fp); 
		return Redirect::to('bsetec/config/security')->with('messagetext','Setting Has Been Save Successful')->with('msgstatus','success');
	} else {
		return Redirect::to('bsetec/config/security')->with('messagetext', 'The following errors occurred')->with('msgstatus','error')
		->withErrors($validator)->withInput();
	}	
}

public function getLog( $type = null)
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		
		$this->data = array(
			'pageTitle'	=> 'Help Manual',
			'pageNote'	=> 'Documentation',
			'active'	=> 'log'
			);	
		return view('bsetec.config.log',$this->data);
	}else{
		return Redirect::to('');
	}	
}


public function getClearlog()
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		$dir = str_replace('/app','',app_path())."/storage/logs";	
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
			{
				//removedir($file);
			} else {

				unlink($file);
			}
		}

		$dir = str_replace('/app','',app_path())."/storage/framework/views";	
		foreach(glob($dir . '/*') as $file) {
			if(is_dir($file))
			{
				//removedir($file);
			} else {
				
				unlink($file);
			}
		}		

		return Redirect::to('bsetec/config/log')->with('messagetext','Cache has been cleared !')->with('msgstatus','success');

	}else{
		return Redirect::to('');
	}	
}

function removeDir($dir) {
	foreach(glob($dir . '/*') as $file) {
		if(is_dir($file))
			removedir($file);
		else
			unlink($file);
	}
	rmdir($dir);
}

public function getTranslation( Request $request, $type = null)
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		if(!is_null($request->input('edit')))
		{
			$file = (!is_null($request->input('file')) ? $request->input('file') : 'core.php'); 
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
				$files = scandir(str_replace('\app','',app_path())."/resources/lang/".$request->input('edit')."/");
			}else{
				$files = scandir(str_replace('/app','',app_path())."/resources/lang/".$request->input('edit')."/");
			}
			
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
				$str = \File::getRequire(str_replace('\app','',app_path())."/resources/lang/".$request->input('edit').'/'.$file);
			}else{
				$str = \File::getRequire(str_replace('/app','',app_path())."/resources/lang/".$request->input('edit').'/'.$file);
			}
			//$str = serialize(file_get_contents('./protected/app/lang/'.$request->input('edit').'/core.php'));
			
			
			
			$this->data = array(
				'pageTitle'	=> 'Help Manual',
				'pageNote'	=> 'Documentation',
				'stringLang'	=> $str,
				'lang'			=> $request->input('edit'),
				'files'			=> $files ,
				'file'			=> $file ,
				);	
			$template = 'edit';
			
		} else {

			$this->data = array(
				'pageTitle'	=> 'Help Manual',
				'pageNote'	=> 'Documentation',
				);	
			$template = 'index';
		}

		return view('bsetec.config.translation.'.$template,$this->data);	
	}else{
		return Redirect::to('');
	}
}

public function getAddtranslation()
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		return view("bsetec.config.translation.create");
	}else{
		return Redirect::to('');
	}
} 

public function postAddtranslation( Request $request)
{
	$rules = array(
		'name'		=> 'required',
		'folder'	=> 'required|alpha',
		'author'	=> 'required',
		);
	$validator = Validator::make($request->all(), $rules);	
	if ($validator->passes()) {


		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
			$template = str_replace('\app','',app_path());
		}else{
			$template = str_replace('/app','',app_path());
		}

		$folder = $request->input('folder');
		mkdir( $template."/resources/lang/".$folder ,0777 );	
		
		$info = json_encode(array("name"=> $request->input('name'),"folder"=> $folder , "author" => $request->input('author')));
		$fp=fopen(  $template.'/resources/lang/'.$folder.'/info.json',"w+"); 
		fwrite($fp,$info); 
		fclose($fp); 	
		
		$files = scandir( $template .'/resources/lang/en/');
		foreach($files as $f)
		{
			if($f != "." and $f != ".." and $f != 'info.json')
			{
				copy( $template .'/resources/lang/en/'.$f, $template .'/resources/lang/'.$folder.'/'.$f);
			}
		}
		return Redirect::to('bsetec/config/translation')->with('messagetect','New Translation has been added !')->with('msgstatus','success');	;			
		
	} else {
		return Redirect::to('bsetec/config/translation')->with('messagetext','Failed to add translation !' )->with('msgstatus','error')->withErrors($validator)->withInput();
	}		
	
}

public function postSavetranslation( Request $request)
{
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
		$template = str_replace('\app','',app_path());
	}else{
		$template = str_replace('/app','',app_path());
	}
	//$template = str_replace('\app','',app_path());
	
	$form  	= "<?php \n"; 
	$form 	.= "return array( \n";
		foreach($_POST as $key => $val)
		{
			if($key !='_token' && $key !='lang' && $key !='file') 
			{
				if(!is_array($val))
				{
					$val = str_replace('"',"'",$val);
					$form .= '"'.$key.'"			=> "'.$val.'", '." \n ";
					
				} else {
					$form .= '"'.$key.'"			=> array( '." \n ";
						foreach($val as $k=>$v)
						{
							$v = str_replace('"',"'",$v);
							$form .= '      "'.$k.'"			=> "'.$v.'", '." \n ";
						}
						$form .= "), \n";
}
}		

}
$form .= ');';
		//echo $form; exit;
$lang = $request->input('lang');
$file	= $request->input('file');
$filename='';
if($file!=NULL){
	$filename = $template .'/resources/lang/'.$lang.'/'.$file;
	$fp=fopen($filename,"w+"); 
	fwrite($fp,$form); 
	fclose($fp); 	
}
	//	$filename = 'lang.php';

return Redirect::to('bsetec/config/translation?edit='.$lang.'&file='.$file)
->with('messagetext','Translation has been saved !')->with('msgstatus','success');	

} 	


public function getRemovetranslation( $folder )
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		self::removeDir( str_replace('/app','',app_path())."/resources/lang/".$folder);
		return Redirect::to('bsetec/config/translation')->with('messagetext','Translation has been removed !')->with('msgstatus','success');	
	}else{
		return Redirect::to('');
	}
	
}		

public function getMailsettings(Request $request)
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		$this->data = array(
			'groups'	=> Groups::all(),
			'pageTitle'	=> 'E-mail Settings',
			'pageNote'	=> 'E-mail Configuration and Setting',
			'active'	=> 'mailsettings'
			
			);
		
		$this->data['email_settings'] = $this->bsetec->get_options('email_settings');
		// echo '<pre>';print_r($this->data['email_settings']);exit;
		return view("bsetec.config.mailsettings",$this->data);
	}else{
		return Redirect::to('');
	}
}

public function postUpdatemailsettings(Request $request)
{
	$rules = array(
		'smtp_host'		=> 'required',
		'smtp_port'		=> 'required',
		'smtp_username'	=> 'required',
		'smtp_password'	=> 'required'
		);

	$validator = Validator::make($request->all(), $rules);	
	if ($validator->passes()) {
		$save = $request->all();
		unset($save['_token']);
		try{
	
			$transport = new Swift_SmtpTransport($request->get('smtp_host'), $request->get('smtp_port'), $request->get('smtp_secure'));
			$transport->setUsername($request->get('smtp_username'));
			$transport->setPassword($request->get('smtp_password'));
			$mailer =  new Swift_Mailer($transport);
			$mailer->getTransport()->start();
			$this->bsetec->save_options('email_settings', $save);
			return Redirect::to('bsetec/config/mailsettings')->with('messagetext','E-mail settings updated successfully!')->with('msgstatus','success');
		}catch (\Swift_TransportException $e) {
				$errorMsg = explode('[',$e->getMessage());
				return Redirect::to('bsetec/config/mailsettings')->with('messagetext',' Failed to update settings.SMTP credentials wrong !' )->with('msgstatus','error')->withErrors($validator)->withInput();
		}
	} else {
		return Redirect::to('bsetec/config/mailsettings')->with('messagetext','Failed to update settings !' )->with('msgstatus','error')->withErrors($validator)->withInput();
	}
}

public function postSendemail(Request $request)
{
	$rules = array(
		'femail'		=> 'required:email',
		'temail'		=> 'required:email',
		'subject'		=> 'required',
		'message'		=> 'required'
		);
	$validator = Validator::make($request->all(), $rules);	
	if ($validator->passes()) {
		$fromMail  = $request->input('femail');
		$toMail    = $request->input('temail');
		$subject   = $request->input('subject');
		$message   = $request->input('message');
		$data  	   = array('tomail'=>$toMail,'content'=>$message);
		$tempname  = 'mailtemplates.testemail';

		$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		return Redirect::to('bsetec/config/mailsettings')->with('messagetect','Mail sent successfully!')->with('msgstatus','success');	;			
		
	} else {
		return Redirect::to('bsetec/config/mailsettings')->with('messagetext','Failed to sent mail !' )->with('msgstatus','error')->withErrors($validator)->withInput();
	}
}	

public function getCoursesettings(Request $request)
{
	$this->data = array(
		'groups'	=> Groups::all(),
		'pageTitle'	=> 'Course Settings',
		'pageNote'	=> 'Course Setting',
		'active'	=> 'coursesettings'
		
		);
	$this->data['auto']  	 = \bsetecHelpers::get_options('autoApprove');
	$this->data['perpage']   = \bsetecHelpers::get_options('paginationPerpage');
	return view("bsetec.config.coursesettings",$this->data);
}

public function postUpdatcoursesettings(Request $request)
{
	if( \Auth::check() && \Session::get('gid') == '1'){
		$rules = array(
			'pagination_per_page'	=> 'required'
			);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$updated_at	= date("Y-m-d h:i:sa");
			$checkbox 	= $request->input('course_auto_approve');
			$perpage 	= $request->input('pagination_per_page');
			$acode       = 'autoApprove';
			$pcode       = 'paginationPerpage';

			if($checkbox=='1'){
				$checkbox = 1;
			}else{
				$checkbox = 2;
			}

			\DB::table('options')->where('code', '=', $acode)->update(array('option'=>$checkbox));
			\DB::table('options')->where('code', '=', $pcode)->update(array('option'=>$perpage));
			
			return Redirect::to('bsetec/config/coursesettings')->with('messagetect','Course settings updated successfully!')->with('msgstatus','success');	;			
			
		} else {
			return Redirect::to('bsetec/config/coursesettings')->with('messagetext','Failed to update settings !' )->with('msgstatus','error')->withErrors($validator)->withInput();
		}
	}else{
		return Redirect::to('');
	}
}


	/*
	* @process - custom html for home page
	* @param - page contents
	* @method - get
	*/

	public function getCustomsettings()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$page_contents = $this->bsetec->get_options('page_contents');
			$this->data = array(
				'page_contents'			=> $page_contents,
				'pageTitle'				=> 'Page Custom Settings',
				'pageNote'				=> 'Page Customize and Setting',
				'active'				=> ''
				);

			return view("bsetec.config.custom-settings",$this->data);
		}else{
			return Redirect::to('');
		}
	} 

	public function postCustomsettings( Request $request ){

		$data = array(
			'page_services_content'=>$request->get('page_services_content'),
			'page_about_content'=>$request->get('page_about_content'),
			'advertising_footer_content'=>$request->get('advertising_footer_content')
			);

		if($data['advertising_footer_content']!=NULL || $data['page_about_content']!=NULL || $data['advertising_footer_content']!=NULL ){
			$this->bsetec->save_options('page_contents',$data);
			return Redirect::to('bsetec/config/customsettings')->with('messagetect','Services Content settings updated successfully!')->with('msgstatus','success');	

		}else{
			$this->bsetec->save_options('page_contents',$data);
			
			return Redirect::to('bsetec/config/customsettings')->with('messagetect','Services Content settings required')->with('msgstatus','success');	
		}
			
	}

	
	public function getCheckcoursesettings(Request $request)
	{
	  $this->data['video_settings']   = \bsetecHelpers::get_options('video_settings');

	  return json_encode( $this->data['video_settings'] , TRUE);
	}


	public function getRtl(Request $request)
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$this->data = array(
				'pageTitle'				=> 'RTL Settings',
				'pageNote'				=> 'RTL Customize and Setting',
				'active'				=> ''
				);
			$this->data['CNF'] =  CNF_RTL;
			$this->data['active'] =  'rtl';
		return view("bsetec.config.rtl",$this->data);
		}else{
			return Redirect::to('');
		}
	}

	public function postUpdatertl(Request $request)
	{
		 // echo "<pre>";print_r($request->input('rtl_approve'));exit();
		$val  =		"<?php \n"; 
		$val .= 	"define('CNF_APPNAME','".CNF_APPNAME."');\n";
		$val .= 	"define('CNF_APPDESC','".CNF_APPDESC."');\n";
		$val .= 	"define('CNF_COMNAME','".CNF_COMNAME."');\n";
		$val .= 	"define('CNF_EMAIL','".CNF_EMAIL."');\n";
		$val .= 	"define('CNF_METAKEY','".CNF_METAKEY."');\n";	
		$val .= 	"define('CNF_METADESC','".CNF_METADESC."');\n";					
		$val .= 	"define('CNF_GROUP','".CNF_GROUP."');\n";	
		$val .= 	"define('CNF_ACTIVATION','".CNF_ACTIVATION."');\n";	
		$val .= 	"define('CNF_MULTILANG','".CNF_MULTILANG."');\n";
		$val .= 	"define('CNF_LANG','".CNF_LANG."');\n";		
		$val .= 	"define('CNF_REGIST','".CNF_REGIST."');\n";		
		$val .= 	"define('CNF_FRONT','".CNF_FRONT."');\n";		
		$val .= 	"define('CNF_RECAPTCHA','".CNF_RECAPTCHA."');\n";	
		$val .= 	"define('CNF_THEME','".CNF_THEME."');\n";	
		$val .= 	"define('CNF_RECAPTCHAPUBLICKEY','');\n";
		$val .= 	"define('CNF_RECAPTCHAPRIVATEKEY','');\n";
		$val .= 	"define('CNF_MODE','".CNF_MODE."');\n";	
		$val .= 	"define('CNF_LOGO','".CNF_LOGO."');\n";									
		$val .= 	"define('CNF_FLOGO','".CNF_FLOGO."');\n";
		$val .= 	"define('CNF_FAV','".CNF_FAV."');\n";		
		$val .=		"define('CNF_CURRENCY','".CNF_CURRENCY."');\n";
		$val .=		"define('CNF_RTL','".(($request->input('rtl_approve') == '1' ) ? '1' : '0' )."');";
		$val .= 	"?>";
		$filename = 'setting.php';
		$fp=fopen($filename,"w+"); 
		fwrite($fp,$val); 
		fclose($fp); 
		return Redirect::to('bsetec/config/rtl')->with('messagetext','Setting Has Been Save Successful')->with('msgstatus','success');
	}

		/*
	@function Add webiner citrix settings
	*/
	public function getWebinar(){
		$this->data = array(
				'pageTitle'				=> 'Page Custom Settings',
				'pageNote'				=> 'Page Customize and Setting',
				'active'				=> 'webinar'
				);

		$env = file_get_contents(base_path() . '/.env');
         // split env file string convert to array format
		$env = preg_split('/\s+/', $env);
        foreach ($env as $value) {
         	if(!empty($value)){
       	        $fields=explode("=", $value);
       	        $this->data[$fields['0']]=$fields['1'];
        	 }
        }
		return view("bsetec.config.webinar",$this->data);
	}

	public function postWebinar(Request $request){
			$env_data = array();
			$env_data['CITRIX_DIRECT_USER']=$request->get('citrix_direct_user');
			$env_data['CITRIX_DIRECT_PASSWORD']=$request->get('citrix_direct_password');
			$env_data['CITRIX_CONSUMER_KEY']=$request->get('citrix_direct_consumer');
			$env_data['ACCESS_TOKEN']=$request->get('access_token');
			$env_data['ORGANIZER_KEY']=$request->get('organizer_key');
			$this->changeEnv($env_data);
			return Redirect::to('bsetec/config/webinar')->with('messagetext',\Lang::get('core.webinar_settings'))->with('msgstatus','success');
	}

	// Change env file dynamically
function changeEnv($data){
	if(count($data) > 0){
            // read .env-file
            $env = file_get_contents(base_path() . '/.env');
            // split env file string convert to array format
            $env = preg_split('/\s+/', $env);
            // Loop through given data
            foreach((array)$data as $key => $value){
                // loop old env key
                foreach($env as $env_key => $env_value){
                    // split env key in array
                    $entry = explode("=", $env_value, 2);
                    // check old env key equal($entry[0]) to new env key ($key)
                    if($entry[0] == $key){
                        // If yes, overwrite it with the new one
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        // If not, keep the old one
                        $env[$env_key] = $env_value;
                    }
                }
            }
            // Turn the array back to an String
            $env = implode("\n", $env);
            // And overwrite the .env with the new data
            file_put_contents(base_path() . '/.env', $env);
            return true;
        } else {
            return false;
        }
    }

	public function getGamificationsettings()
	{
		if( \Auth::check() && \Session::get('gid') == '1'){
			$gamification = $this->bsetec->get_options('gamification');
			if(!empty($gamification))
			{
			$this->data = array(
				'gamification'			=> $gamification,
				'pageTitle'				=> 'Gamification Settings',
				'pageNote'				=> 'Gamification Customize and Setting',
				'active'				=> 'gamificationsettings'
				);

			return view("bsetec.config.gamificationsettings",$this->data);
			}
			return Redirect::to('bsetec/config');
		}else{
			return Redirect::to('');
		}
	}

	public function postGamificationsave( Request $request ){
		$data = array(
			'equivalent_points'=>$request->get('equivalent_points'),
			'Status'=>(!is_null($request->input('Status')) ? 'true':'false'),
			'apply_for_free_course'=>(!is_null($request->input('apply_for_free_course')) ? 'true':'false'),
			'signup_points'=>$request->get('signup_points')
			);
		$this->bsetec->save_options('gamification',$data);
		return Redirect::to('bsetec/config/gamificationsettings')->with('messagetect','Gamification settings updated successfully!')->with('msgstatus','success');	;
	}

	public function getAwssettings()
	{
		// $s3 = \AWS::get('s3');
		if( \Auth::check() && \Session::get('gid') == '1'){
			$awss3 = $this->bsetec->get_options('awss3');
			$this->data = array(
				'awss3'					=> $awss3,
				'pageTitle'				=> 'Amazon AWS S3 Settings',
				'pageNote'				=> 'Amazon AWS S3 Settings',
				'active'				=> 'awssettings'
				);

			return view("bsetec.config.awssettings",$this->data);
		}else{
			return Redirect::to('');
		}
	}

	public function postAwssave( Request $request ){
		$data = array(
			'status'=>(!is_null($request->get('enable')) ? 'true':'false'),
			'key'=>$request->get('key'),
			'secret'=>$request->get('secret'),
			'region'=>$request->get('region'),
			'bucket'=>$request->get('bucket')
			);
		$this->bsetec->save_options('awss3',$data);

		$val  =		"<?php \n";
		$val .= 	"define('AWS_STATUS','".(!is_null($request->input('status')) ? 'true':'false')."');\n";
		$val .= 	"define('AWS_KEY','".$request->input('key')."');\n";
		$val .= 	"define('AWS_SECRET','".$request->input('secret')."');\n";
		$val .= 	"define('AWS_REGION','".$request->input('region')."');\n";
		$val .= 	"define('AWS_BUCKET','".$request->input('bucket')."');\n";
		$val .= 	"?>";

		$filename = 'awssetting.php';
		$fp=fopen($filename,"w+");
		fwrite($fp,$val);
		fclose($fp);

		return Redirect::to('bsetec/config/awssettings')->with('messagetect','Amazon AWS S3 settings updated successfully!')->with('msgstatus','success');
	}

	public function getMembership()
	{
		$membership = $this->bsetec->get_options('membership');	
		if( \Auth::check() && \Session::get('gid') == '1'){
			$this->data = array(
				'membership'			=> $membership,
				'pageTitle'				=> 'Membership Settings',
				'pageNote'				=> 'Membership Settings',
				'active'				=> 'membershipsettings'
			);
			return view("bsetec.config.membership",$this->data);
		}else{
			return Redirect::to('');
		}

	}

	public function postMembership(Request $request){
		$option['membership'] = ($request->get('membership') == 1 ? '1':'0');
		$this->bsetec->save_options('membership',$option);
		return Redirect::to('bsetec/config/membership')->with('messagetext','Mmebership settings updated successfully!')->with('msgstatus','success');
	}
	
	public function getWalletpoint() {
		$wallet_point = $this->bsetec->get_options('wallet_point');	
		if(count($wallet_point)==0){
			$wallet_point['point'] = '';
			$wallet_point['amount'] = '';
		}
		// print_r($wallet_point);die();
		if( \Auth::check() && \Session::get('gid') == '1'){
			$this->data = array(
				'wallet_point'			=> $wallet_point,
				'pageTitle'				=> 'Wallet Point',
				'pageNote'				=> 'Wallet Point',
				'active'				=> 'walletpoint'
			);
			return view("bsetec.config.walletpoint",$this->data);
		}else{
			return Redirect::to('');
		}
	}

	public function postWalletpoint(Request $request){
		$option['point'] = $request->get('walletpoint');
		$option['amount'] = $request->get('walletamount');
		$this->bsetec->save_options('wallet_point', $option);
		return Redirect::to('bsetec/config/walletpoint')->with('messagetext','Wallet Point settings updated successfully!')->with('msgstatus','success');
	}
	
}
