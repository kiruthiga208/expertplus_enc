<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admincoupon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Notificationsettings;
use App\Http\Controllers\CommonmailController;
use App\Models\bsetec;
use Carbon\Carbon;
use Illuminate\Support\Str;




class AdmincouponController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'admincoupon';
	static $per_page	= '10';

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->model = new Admincoupon();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		$this->notification_settings = new Notificationsettings();
		$this->sendmail = new CommonmailController();

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'admincoupon',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{
		
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$today =date('Y-m-d h:i:s');
		// echo $today;exit;
		$upadte  = \DB::table('coupon_discount')
		->where('coupon_end_date', '<' , $today )
		->update(['coupon_status' => 0]);
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('admincoupon');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('admincoupon.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('coupon_discount'); 
		}

		$this->data['id'] = $id;
		return view('admincoupon.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('coupon_discount'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('admincoupon.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		// $rules = $this->validateForm();
		// $rules['coupon_value'] = 'required|min:1';
  //       $rules['coupon_start_date'] = 'required|before:coupon_end_date';
  //       $rules['coupon_end_date'] = 'required|after:coupon_start_date';

		if($id==0)
		{
		$lastdate=\DB::table('coupon_discount')->select('coupon_end_date')->orderBy('coupon_end_date','desc')->first();
		}
		else{
						
		$lastdate=\DB::table('coupon_discount')->where('id','>' , $id)->select('coupon_start_date')->first();
		$previousdate=\DB::table('coupon_discount')->select('coupon_end_date')->where('id','<',$id)->orderBy('created_at','desc')->first();
		}
		$rules = $this->validateForm();
		$rules['coupon_value'] = 'required|min:1';
		if($id==0)
		{
			if($lastdate)
	        {
	        	$rules['coupon_start_date'] = 'required|before:coupon_end_date|after:'.$lastdate->coupon_end_date;
			}
	    	else
	    	{
	    		$rules['coupon_start_date'] = 'required|before:coupon_end_date';
	    	}
        
        	$rules['coupon_end_date'] = 'required|after:coupon_start_date';	
    	}
    	else
		{
	    	if(isset($lastdate) && isset($previousdate))
	        {
	        	

	        $rules['coupon_start_date'] = 'required|before:coupon_end_date|after:'.$previousdate->coupon_end_date;
	        $rules['coupon_end_date'] = 'required|after:coupon_start_date|before:'.$lastdate->coupon_start_date;

	        
	    	}
	    	else if(isset($lastdate))
	    	{
	    		
	    		$rules['coupon_end_date'] = 'required|after:coupon_start_date|before:'.$lastdate->coupon_start_date;
	    		$rules['coupon_start_date'] = 'required|before:coupon_end_date';
	    		

	    	}
	    	else if(isset($previousdate))
	    	{
	    		
	    		$rules['coupon_start_date'] = 'required|after:'.$previousdate->coupon_end_date.'|before:coupon_end_date';
	    		$rules['coupon_end_date'] = 'required|after:coupon_start_date';

	    	}
	    	else
	    	{
	    		
	    		$rules['coupon_start_date'] = 'required|before:coupon_end_date';
	    		$rules['coupon_end_date'] = 'required|after:coupon_start_date';
	    	}
    	}

        $messages = array(
            'coupon_value.min' =>'Coupon must be positive value.',
            'coupon_start_date.before' =>'Coupon start date must be less than end date.',
            'coupon_end_date.after' =>'Coupon end date must be greater than start date.',
        );

		$validator = Validator::make($request->all(), $rules, $messages);

		if ($validator->passes()) {
			$data = $this->validatePost('admincoupon');
			$data['user_id'] = \Auth::user()->id;
			$data['created_at'] = time();
			$data['updated_at'] = time();
			$stripe = $this->bsetec->get_options('stripe');
			
			$newID = $this->model->insertRow($data , $id);
			if(isset($stripe['payment']) == 1){

				$carbon = Carbon::now();
				$coupon_code = $carbon->month.$carbon->year.strtoupper(Str::random(4)).$newID;
				$coupon_update = Admincoupon::find($newID);
                $coupon_update->coupon_code = $coupon_code;
                $coupon_update->save();
                $amount=round($data['coupon_value']);

                $stripe = $this->bsetec->get_options('stripe');
				\Stripe\Stripe::setApiKey($stripe['secret_key']);
				if($request->coupon_type == 1){
					$stripe_response_percentage = \Stripe\Coupon::create(array(
					  "percent_off" => $amount,
					  "duration" => "repeating",
					  "duration_in_months" => 3,
					  "id" => $coupon_code)
					);
				}
				if($request->coupon_type == 2){
					$stripe_response_price = \Stripe\Coupon::create(array(
					  "amount_off" => $amount,
					  "currency" => CNF_CURRENCY,
					  "duration" => "repeating",
					  "duration_in_months" => 3,
					  "id" => $coupon_code)
					);
				}

			}
			if(!is_null($request->input('apply')) && $id)
			{
				$return = 'admincoupon/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'admincoupon?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				//send mail to users 
				if($data['coupon_status'])
				{
					$maildata = array('user_id' => $data['user_id'],
									'coupon_desc' => $data['coupon_desc'],
									'coupon_type' => $data['coupon_type'],
									'coupon_value' => $data['coupon_value'],
									'coupon_start_date' => $data['coupon_start_date'],
									'coupon_end_date' => $data['coupon_end_date'],
									'coupon_status' => $data['coupon_status']
								);
					$users = $this->notification_settings->get_users('notif_special_promotion');
					$push_notification =array();
					if(count($users) > 0)
					{
						foreach ($users as $user) 
						{
							$fromMail   = \Auth::user()->email;
			                $toMail     = $user->email;
			                $subject    = CNF_APPNAME." Course Discount";
			                
			                $tempname   = 'course.emails.discount';
			                $this->sendmail->getMail($fromMail,$toMail,$subject,$maildata,$tempname);
							//array_push($push_notification, $user->id);
							$data = array();
		                    $data['type']= 'coupon';
		                    $data['type_id'] = $newID;
		                    $data['users'] = $user->id;
		                    $data['status'] = 0;
		                      \DB::table('push_notification')->insertGetId($data);
						}
					}
				}
				
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('admincoupon/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			//coupon delete
			$stripe = $this->bsetec->get_options('stripe');
			if(isset($stripe['payment']) == 1){
				\Stripe\Stripe::setApiKey($stripe['secret_key']);
				
				
				for($i=0;$i<count($request->input('id'));$i++){
					$coupon = \bsetecHelpers::getadmincouponcode($request->input('id')[$i]);
					$couponcode = $coupon->coupon_code;
					$cpn = \Stripe\Coupon::retrieve($coupon->coupon_code);
					$cpn->delete();
					$del = \DB::table('coupon_discount')->where('coupon_code', '=', $couponcode)->delete();
				}
			}
			// $this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('admincoupon')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('admincoupon')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}
