<?php namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Dashboard;


class DashboardController extends Controller {

	public function __construct()
	{
		$this->model = new Dashboard();

	}

	public function Index( Request $request )
	{
		if(\Session::get('gid')=='3'){
			return \Redirect::to('');
		}elseif(\Auth::check()==false){
			return \Redirect::to('user/login');
		}else{
			$this->data = $this->model->get();
			return view('dashboard.index', $this->data);
		}
	}	

	public function postAjaxCourse()
	{
		
		$days = \Input::get('value');
		$this->data = $this->model->ajax_courses($days);

       	return view('dashboard.ajax-course', $this->data);
	}


}