<?php namespace App\Http\Controllers;

use Mail;
use App\Models\Users;
use App\Models\bsetec;
use App\Models\Message;
use App\Models\CourseFiles;
use Illuminate\Http\Request;
// use App\Library\BigBlueButton;
use App\Models\Customcourserequest;
use App\Models\Notificationsettings;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CommonmailController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;


class CustomcourserequestController extends Controller {

	protected $layout 	= "layouts.main";
	protected $data 	=  array();
	public $module 		= 'customcourserequest';
	static $per_page	= '10';

	public function __construct()
	{

		$this->bsetec 		= new bsetec();
		$this->user_model 	= new Users();
		$this->users 		= new UserController();
		// $this->bbb 			= new BigBlueButton();
		$this->model 		= new Customcourserequest();
		$this->info 		= $this->model->makeInfo( $this->module);
		$this->access 		= $this->model->validAccess($this->info['id']);
      	$this->sendmail 	= new CommonmailController();
      	$this->notification_settings = new Notificationsettings();

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'customcourserequest',
			'return'	=> self::returnUrl()

		);
	}

	public function getSettings( Request $request )
	{
		$gid = \Session::get('gid');
		if($this->access['is_view'] ==0 || $gid != 1)
			return Redirect::to('customcourserequest/view');

		$options = \DB::table('options')->select('option')->where('code', '=', 'course_request')->where('option_key', '=', 'default_textarea_content')->get();
		if(!empty($options)) {
			$this->data['taccr'] = $options[0]->option;
		} else {
			$this->data['taccr'] = '';
		}
		return view('customcourserequest.admin.settings',$this->data);

	}

	public function postSettingssave( Request $request )
	{
		$data['option'] = $request->input('taccr');
		$options = \DB::table('options')->where('code', '=', 'course_request')->where('option_key', '=', 'default_textarea_content')->update($data);
		return Redirect::to('customcourserequest/settings')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
	}

	public function Index( Request $request )
	{
		$gid = \Session::get('gid');
		if($this->access['is_view'] ==0 || $gid != 1)
			return Redirect::to('customcourserequest/view');

		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : 'ccr_id');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');


		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('customcourserequest');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		return view('customcourserequest.admin.index',$this->data);
	}

	public function getView( Request $request )
	{

		if($this->access['is_view'] ==0)
			return Redirect::to('course')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$uid = \Session::get('uid');


		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'ccr_id');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		$type = $request->input('t', '5');
		$page = $request->input('page', 1);

		// if(\Session::get('ut') == "student")
		// 	$type = $request->input('t', '5');
		// else if(\Session::get('ut') == "tutor")
		// 	$type = $request->input('t', '2');
        
		if(\Session::get('ut') == "student")
			// $qtype = $request->input('qt', '1');
			$qtype = '1';
		// else if(\Session::get('ut') == "tutor"){
		// 	// $qtype = $request->input('qt', '2');
		// 	// if($qtype != '2' || $qtype != '3'){
		// 		$qtype = '2';
		// 	// }
		// }
       //else{
	   //	$qtype = 1;
	   //}
        // echo $type;echo $qtype;exit;
		if($type == '1'){
			$filter .= " AND ccr_status ='0' ";
		} else if($type == '2'){
			$filter .= " AND (ccr_status ='1' OR ccr_status ='2' OR ccr_status ='4') ";
		} else if($type == '3'){
		    $filter .= " AND (ccr_status ='3' OR ccr_status ='5') ";
		}
		else if($type == '4'){
		    $filter .= " AND entry_by!=$uid";
		}
		else if($type == '5'){
		    $filter .= " AND entry_by=$uid";
		}
		// else if($type == '5'){
		//     $filter .= " AND FIND_IN_SET(selected_tutors,$uid) ";
		// }
		// echo '';print_r($filter);exit;
		// if($qtype == '1'){
		// 	$filter .= " AND entry_by = '$uid' ";
		// } else if($qtype == '2'){
		// 	$filter .= " AND FIND_IN_SET($uid, applied_tutor_ids) ";
		// } else if($qtype == '3'){
		// 	$filter .= " AND tutor_id = '$uid' ";
		// }
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );
		//echo '<pre>';print_r($results);exit;

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('view');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		$this->data['type']		= $type;
		$this->data['qtype'] 	= $qtype;
		$this->data['view'] 	= 'view';
		$this->data['entry_by'] = $uid;
		$this->data['model'] 	= $this->model;
		return view('customcourserequest.index',$this->data);
	}

	function getUpdate(Request $request, $id = null)
	{
		if(\Session::get('ut') == "tutor")
			return Redirect::to('customcourserequest/view');

		$id = $request->input('id');
		$q = $request->input('q','0');
		$quest = '';

		if($q){
			$quest = \DB::table("custom_questions")->where("custom_questions.question_id",'=',$q)->first();
		}

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('custom_course_request');
		}

		$this->data['id'] 		 = $id;
		$this->data['model'] 	 = $this->model;
		$options = \DB::table('options')->select('option')->where('code', '=', 'course_request')->where('option_key', '=', 'default_textarea_content')->get();
		if(!empty($options)) {
			$this->data['taccr'] = stripslashes($options[0]->option);
		} else {
			$this->data['taccr'] = '';
		}
		$this->data['onlinetutors'] = $this->users->getOnlinetutors();
		
		$this->data['quest'] 		= $quest;
		return view('customcourserequest.form',$this->data);
	}

	public function getShow( $id = null)
	{

		// if($this->access['is_detail'] ==0)
		// 	return Redirect::to('dashboard')
		// 		->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		// echo "<pre>"; print_r($row); exit;
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('custom_course_request');
		}

		$this->data['applications'] 	= '';
		$this->data['row']->useraccess 	= 0;
		if(\Session::get('uid') == $this->data['row']->entry_by && !$this->data['row']->ccr_status){
			$this->data['row']->useraccess = 1;
			$rowData = $this->model->getApplied($id);
			foreach ($rowData as $key => $row) {
				$usrinfo 				  	 = \bsetecHelpers::getuserinfobyid($row->user_id);
				$rowData[$key]->imgpath      = \SiteHelpers::customavatar($usrinfo->email,$row->user_id);
				$rowData[$key]->user_link 	 = \URL::to('profile/'.$usrinfo->username);
	            $rowData[$key]->display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
			}
			$this->data['applications'] = $rowData;
		} else if((\Session::get('uid') == $this->data['row']->entry_by || \Session::get('uid') == $this->data['row']->tutor_id || \Session::get('gid') == 1) && $this->data['row']->ccr_status){
			if(\Session::get('uid') == $this->data['row']->entry_by || \Session::get('gid') == 1){
				$this->data['row']->useraccess = 1;
			}

			$rowData = $this->model->getAwarded($id,$this->data['row']->tutor_id);
			$usrinfo = \bsetecHelpers::getuserinfobyid($this->data['row']->tutor_id);
			$rowData->display_name  = $usrinfo->first_name.' '.$usrinfo->last_name;
			$rowData->imgpath   	= \SiteHelpers::customavatar($usrinfo->email,$this->data['row']->tutor_id);
			$rowData->user_link 	= \URL::to('profile/'.$usrinfo->username);
           
            $this->data['applications'] = $rowData;
		}

		$this->data['application_id'] = '';
		if(!empty($this->data['row']->tutor_id)){
			$application_id = $this->model->getApplicationId($id,$this->data['row']->tutor_id);
			$this->data['application_id'] = $application_id->application_id;
		}

		$shared_course = '';
		if((!empty($this->data['row']->tutor_id) && !empty($this->data['row']->shared_course_id)) && ($this->data['row']->tutor_id == \Session::get('uid') || $this->data['row']->entry_by == \Session::get('uid')) ){
			$course_id 				  = $this->data['row']->shared_course_id;
			$courseinfo 			  = $this->model->get_course_info($course_id);
			$shared_course['title']   = $courseinfo->course_title;
			$shared_course['link']    = \URL::to('courseview/'.$course_id.'/'.$courseinfo->slug);
		}

		$recordings = array();
		$revised_total = 0;
		$amount_refund = 0;
		$time_spent = 0;
		$total_meeting_time = "00:00:00";
		$amount_calc = 0;
		$enough_credits = true;
		$show_meeting = true;
		if($this->data['row']->ccr_status == '5' && $this->data['row']->dispute_id){
			$revised_total = number_format(($this->data['row']->final_price - $this->data['row']->refund_price),2, '.', '');
			$amount_refund = $this->data['row']->refund_price;
		}

		if($this->data['row']->meeting_id != 0){
			$minfo = $this->model->getmeetinginfo($this->data['row']->meeting_id);

			if(!empty($minfo->online_users)){
				$onlineusers = json_decode($minfo->online_users,true);
				if(($key = array_search(\Session::get('uid'), $onlineusers)) !== false) {
					$show_meeting = false;
				}
			}
			if($this->data['row']->payment_type == 'hour'){

				$check_credits = false;
				$meeting_duration = $minfo->meeting_duration;
				if(!empty($meeting_duration)){
					$durations = json_decode($meeting_duration,true);
					foreach ($durations as $key => $duration_value) {
						if(isset($duration_value['end'])){
							$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
						}
					}
					if($time_spent != 0){
						$hours_spent = $time_spent / (60 * 60);
						if($hours_spent >= $this->data['row']->deducted_hours){
							$check_credits = true;
						}
					}
				} else {
					$check_credits = true;
				}

				if($check_credits){
					$user_credits = $this->model->userCredits($this->data['row']->entry_by);
					if($user_credits < $this->data['row']->final_price){
						$enough_credits = false;
					}
				}


				if($time_spent != 0){
					$total_meeting_time = gmdate("H:i:s", $time_spent);
					$final_price = $this->data['row']->final_price;
					$amount_calc = round($final_price * ($time_spent / (60 * 60)),2);
					if($this->data['row']->ccr_status == '5' && $this->data['row']->dispute_id){
						$revised_total = round(($final_price - $this->data['row']->refund_price) * ($time_spent / (60 * 60)),2);
						$amount_refund = $this->data['row']->refund_price;

					}
				}
				$recordingsParams = array('meetingId' => $minfo->meeting_slug);
				$record_data = $this->bbb->getRecordingsWithXmlResponseArray($recordingsParams);
				if($record_data['returncode']['0'] == 'SUCCESS'){
					foreach ($record_data as $key => $records) {
						if(is_array($records)){
							if(isset($records['playbackFormatUrl']))
								array_push($recordings, $records['playbackFormatUrl']);
						}
					}
					$recordings = array_reverse($recordings);
				}
			}
		}

		$disputeData = '';
		if(($this->data['row']->ccr_status == 4 || $this->data['row']->ccr_status == 5) && $this->data['row']->dispute_id != 0){
			$disputeData = $this->model->getDispute($this->data['row']->dispute_id);
		}

		$this->data['id'] 				= $id;
		$this->data['shared_course'] 	= $shared_course;
		$this->data['model'] 			= $this->model;
		$this->data['user_model'] 		= $this->user_model;
		$this->data['access'] 			= $this->access;
		$this->data['loggedid'] 		= \Session::get('uid');
		$this->data['applied'] 			= $this->model->checkApplied($id,\Session::get('uid'));
		$this->data['model'] 			= $this->model;
		$this->data['recordings'] 		= $recordings;
		$this->data['amount_calc'] 		= $amount_calc;
		$this->data['revised_total'] 	= $revised_total;
		$this->data['amount_refund'] 	= $amount_refund;
		$this->data['enough_credits'] 	= $enough_credits;
		$this->data['show_meeting'] 	= $show_meeting;
		$this->data['dispute'] 			= $disputeData;
		$this->data['total_meeting_time'] = $total_meeting_time;
		
		//$applied_id = $this->model->applied_id($id,\Session::get('uid'));
		
		//$userType = $this->bsetec->get_option('user_seperate_settings');
		//$this->data['applied_id'] = $applied_id;
		//$this->data['logged_id']  = \Session::get('uid');
		//$this->data['owner_id']   = $this->data['row']->entry_by;
		//$this->data['user_type']  = $this->bsetec->get_option('user_seperate_settings');
		return view('customcourserequest.view',$this->data);
	}

	function postSave( Request $request, $id =0)
	{
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data 					= $this->validatePost('tb_customcourserequest');
			$data['sub_cat_id'] ='';
			if(!empty($request->input('sub_cat_id')))
				$data['sub_cat_id'] 	= $request->input('sub_cat_id');
		
			$data['createdOn'] 		= date("Y-m-d h:i:s");
			$data['updatedOn'] =date("Y-m-d h:i:s");
			if($data['payment_type'] == "0"){
				$data['payment_type'] = "fixed";
			}
			else{
				$data['payment_type'] = "hour";
			}

			$file_ids = '';
			$file_id_data = $request->input('file_ids');
			if(!empty($file_id_data)){
				$file_id 	= explode(',', $file_id_data);
				$file_ids 	= json_encode($file_id);
			}

			$data['file_ids'] = $file_ids;

			$skills 	= $request->input('skills');
			$skillset 	= explode(',',$skills);
			foreach ($skillset as $key => $skill) {
				$this->user_model->addnewskill($skill);
			}
			$data['skills'] 		= json_encode($skillset);

			$selected_tutors 		= $request->input('selected_tutors');
			$selected_tutorsset 	= explode(',',$selected_tutors);
			$data['selected_tutors']= json_encode($selected_tutorsset);

			$data['type'] = $request->input('type');
			$data['entry_by'] = \Session::get('uid');
			$data['tutor_any'] = $request->input('tutor_any');
			$newID = $this->model->insertRow($data , $request->input('ccr_id'));

			if($request->input('ccr_id')=='' && $newID!=''){
				// send message
				$uid = \Session::get('uid');
				$usrinfo = \bsetecHelpers::getuserinfobyid($uid);
				if(count($usrinfo)>0){
		            //join meeting mail
					$alink      = \URL::to('customcourserequest/show/'.$newID);
					$toMail   	= CNF_EMAIL;
					$fromMail   = $usrinfo->email;
					$subject    = CNF_APPNAME." Custom Course/Tutorial/Question Request Created";
					$data       = array('username'=>$usrinfo->username,'link'=>$alink,'title'=>$request->input('ccr_name'),'desc'=>$request->input('ccr_description'));
					$tempname   = 'customcourserequest.mail.created';
					// return view('customcourserequest.mail.created')->with($data);
					$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
				}
			}

			if(!is_null($request->input('apply')))
			{
				$return = 'customcourserequest/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'customcourserequest/view';
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

		} else {

			return Redirect::to('customcourserequest/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}

	}

	public function getDelete( Request $request)
	{

		if($this->access['is_remove'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows
		if(count($request->input('id')) >=1)
		{
			$qinfo = $this->model->getccrinfo($request->input('id'));
			if(!empty($qinfo->file_ids)){
				$file_ids = json_decode($qinfo->file_ids,true);
				foreach ($file_ids as $key => $file_id) {
					$this->model->postDocdelete($request->input('id'),$file_id);
				}
			}
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "Course Request Has Been Removed Successfull");
			// redirect
			return Redirect::to('customcourserequest\view')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');

		} else {
			return Redirect::to('customcourserequest\view')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');
		}

	}

	public function postDelete( Request $request)
	{
		$type  = $request->input('types');
		// echo '';print_r($type);exit;
		$data  = array();
		if($type=='0'){
			if($this->access['is_remove'] ==0)
				return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

			$id 	= $request->input('id');
			if(count($id)>0){
				foreach ($id as $key => $qvalue) {
					$qinfo = $this->model->getccrinfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					if(count($usrinfo)>0){
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Course Request Deleted ";
						$data       = array('item'=>'Course Request','authorname'=>'Admin','title'=>$qinfo->ccr_name,'description'=>$qinfo->ccr_description,'username'=>$usrinfo->username);
						$tempname   = 'customcourserequest.mail.delete';
						// return view('customcourserequest.mail.delete')->with($data);
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					if(!empty($qinfo->file_ids)){
						$file_ids = json_decode($qinfo->file_ids,true);
						foreach ($file_ids as $key => $file_id) {
							$this->model->postDocdelete($qvalue,$file_id);
						}
					}
		            $this->model->destroy($qvalue);
				}

				\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
				// redirect
				return Redirect::to('customcourserequest')
				->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');
			} else {
				return Redirect::to('customcourserequest')
				->with('messagetext','No Item Deleted')->with('msgstatus','error');
			}
		}elseif ($type=='1') {
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $qvalue)
				{
					$qinfo = $this->model->getccrinfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					$notificaton = $this->notification_settings->where('user_id',$qinfo->entry_by)->first();
					if(count($usrinfo)>0 && (empty($notificaton) || (!empty($notificaton) && $notificaton->notif_ccr_approval == 0 && $notificaton->notif_for_all == 0))){
		                //approve mail
						$alink      = \URL::to('customcourserequest/show/'.$qvalue);
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Course Request Approved";
						$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name);
						$tempname   = 'customcourserequest.mail.approved';
						// return view('customcourserequest.mail.approved')->with($data);
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					// send mails to tutors
					if($qinfo->approved == 0){
						// select suitable tutors by category
						$tutor_any = $qinfo->tutor_any;
						$selected = array();
						if($tutor_any == 'selected_tutors'){
							$selected_tutors = $qinfo->selected_tutors;
							$selected_tutorsset = json_decode($selected_tutors, true);
							foreach ($selected_tutorsset as $item) {
								$itemarray =  explode(' - ', $item);
								if(isset($itemarray['1'])){
									 array_push($selected,$itemarray['1']);
								}
							}
						}
						$tutors = $this->model->gettutorsbycategoryskills($qinfo->entry_by,$qinfo->category_id,$qinfo->skills);
						if(count($tutors)){
							foreach ($tutors as $key => $tutor) {
								if(($tutor_any == 'selected_tutors' && in_array($tutor->username, $selected)) || $tutor_any == 'any_tutors'){
									$alink      = \URL::to('customcourserequest/show/'.$qvalue);
									$fromMail   = CNF_EMAIL;
									$toMail     = $tutor->email;
									$subject    = CNF_APPNAME." Course Request Available";
									$data       = array('item'=>'Course Request','authorname'=>$tutor->username,'link'=>$alink,'title'=>$qinfo->ccr_name);
									$tempname   = 'customcourserequest.mail.request_tutors';
									// return view('customcourserequest.mail.request_tutors')->with($data);
									$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
								}
							}
						}
					}
					$this->model->updateStatus($qvalue,'1');
				}
				return Redirect::to('customcourserequest')
				->with('messagetext', 'Course Request has been approved successfully.')->with('msgstatus','success');
			} else {
				return Redirect::to('customcourserequest')
				->with('messagetext','No Course Request Approved')->with('msgstatus','error');
			}

		}elseif ($type=='2')
		{
			$id = $request->input('id');
			if(count($id)>0)
			{
				foreach ($id as $key => $qvalue)
				{
					$qinfo = $this->model->getccrinfo($qvalue);
					$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
					if(count($usrinfo)>0){
		                //approve mail
						$alink      = \URL::to('customcourserequest/show/'.$qvalue);
						$fromMail   = CNF_EMAIL;
						$toMail     = $usrinfo->email;
						$subject    = CNF_APPNAME." Course Request Unapproved";
						$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name);
						$tempname   = 'customcourserequest.mail.unapproved';
						// return view('customcourserequest.mail.unapproved')->with($data);
						$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
					}
					$this->model->updateStatus($qvalue,'2');
				}
				return Redirect::to('customcourserequest')
				->with('messagetext', 'Course Request has been Unapproved successfully.')->with('msgstatus','success');
			} else {
				return Redirect::to('customcourserequest')
				->with('messagetext','No Course Request Unapproved')->with('msgstatus','error');
			}

		}
		return Redirect::to('customcourserequest');


	}

	function postApply(Request $request)
	{
		$uid = \Session::get('uid');
		if($uid){
			$id = $request->input('ccr_id');
			$price = $request->input('price');
			$createdOn = date("Y-m-d h:i:s");

			$this->model->applyRequest($id,$uid,$price,$createdOn);

			return Redirect::to('customcourserequest/show/'.$id)
				->with('messagetext', \Lang::get('core.applied_success'))->with('msgstatus','success');
		} else {
			return Redirect::to('customcourserequest/show/'.$id)
				->with('messagetext','Application Failed')->with('msgstatus','error');
		}
	}

	function getPayments(Request $request)
	{
		// if(!\Auth::check()) return Redirect::to('user/login');
		$uid = \Session::get('uid');
		$take = 10;
		$page = $request->input('page');
		if(is_null($page)) $page = 1;
		$paymentsResult = $this->model->paymentDetails($uid,$page,$take);
		$payments = array();

		if($paymentsResult['count'] > 0) {
			$payments = $paymentsResult['result'];
			foreach ($payments as $key => $payment) {

				if($payment->ccr_status == '3'){
					$payments[$key]->ccrstatus = '<span class="text-info"><strong> <i class="fa fa-check-circle"></i> '.\Lang::get('core.video_completed').'</strong></span>';
				} else if($payment->ccr_status == '1' || $payment->ccr_status == '2'){
					$payments[$key]->ccrstatus = '<span class="text-warning"><strong> <i class="fa fa-spinner"></i> '.\Lang::get('core.ongoing').'</strong></span>';
				} else if($payment->ccr_status == '4'){
					$payments[$key]->ccrstatus = '<span class="text-danger"><strong> <i class="fa fa-cross-circle"></i> '.\Lang::get('core.dispute').'</strong></span>';
				} else if($payment->ccr_status == '5'){
					$payments[$key]->ccrstatus = '<span class="text-danger"><strong> <i class="fa fa-cross-circle"></i> '.\Lang::get('core.Cancelled').'</strong></span>';
				} else {
					$payments[$key]->ccrstatus = '<span class="text-info"><strong> <i class="fa fa-info-circle"></i> '.\Lang::get('core.not_started').'</strong></span>';
				}

				$payments[$key]->total_meeting_time = '-';
				$payments[$key]->refund = '-';
				if($payment->entry_by == $uid) {

					$payments[$key]->credit = '-';
					if($payment->payment_type == 'hour') {
						if($payment->deducted_hours != '0') {

							$minfo = $this->model->getmeetinginfo($payment->meeting_id);
							$time_spent = 0;
							$meeting_duration = $minfo->meeting_duration;
							if(!empty($meeting_duration)){
								$durations = json_decode($meeting_duration,true);
								foreach ($durations as $duration_value) {
									if(isset($duration_value['end'])){
										$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
									}
								}
								if($time_spent != 0){
									$total_meeting_time = gmdate("H:i:s", $time_spent);
									$payments[$key]->total_meeting_time = $total_meeting_time;
								}
							}

							if($payment->ccr_status == '3'){
								$payments[$key]->debit = round($payment->final_price * ($time_spent / (60 * 60)),2);
							} else if($payment->ccr_status == '5'){
								$payments[$key]->debit = round(($payment->final_price - $payment->refund_price) * ($time_spent / (60 * 60)),2);
								$payments[$key]->refund = round($payment->refund_price * ($time_spent / (60 * 60)),2);
							} else {
								$payments[$key]->debit = number_format($payment->final_price * $payment->deducted_hours,2, '.', '');
							}
						} else {
							$payments[$key]->debit = '-';
						}
					} else {
						if($payment->ccr_status == '5'){
							$payments[$key]->debit = number_format(($payment->final_price - $payment->refund_price),2, '.', '');
							$payments[$key]->refund = number_format($payment->refund_price,2, '.', '');
						} else {
							$payments[$key]->debit = number_format($payment->final_price,2, '.', '');
						}
					}
					if(isset($payments[$key]->debit)){
						if($payments[$key]->debit == '-'){
							$payments[$key]->status = '-';
						} else {
							$payments[$key]->status = 'Debited';
						}
					}

				} else if($payment->tutor_id == $uid){

					$payments[$key]->debit = '-';
					if($payment->ccr_status == '3' || $payment->ccr_status == '5' || $payment->ccr_status == '1'){
						if($payment->payment_type == 'hour') {
							if($payment->deducted_hours != '0') {

								$minfo = $this->model->getmeetinginfo($payment->meeting_id);
								$time_spent = 0;
								$meeting_duration = $minfo->meeting_duration;
								if(!empty($meeting_duration)){
									$durations = json_decode($meeting_duration,true);
									foreach ($durations as $duration_value) {
										if(isset($duration_value['end'])){
											$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
										}
									}
									if($time_spent != 0){
										$total_meeting_time = gmdate("H:i:s", $time_spent);
										$payments[$key]->total_meeting_time = $total_meeting_time;
									}
								}

								if($payment->ccr_status == '3' || $payment->ccr_status == '1'){
									$payments[$key]->credit = round($payment->final_price * ($time_spent / (60 * 60)),2);
								} else if($payment->ccr_status == '5'){
									$payments[$key]->credit = round(($payment->final_price - $payment->refund_price) * ($time_spent / (60 * 60)),2);
									$payments[$key]->refund = '-'.round($payment->refund_price * ($time_spent / (60 * 60)),2);
								} else {
									$payments[$key]->credit = number_format($payment->final_price * $payment->deducted_hours,2, '.', '');
								}

							}
						} else {
							if($payment->ccr_status == '5'){
								$payments[$key]->credit = number_format(($payment->final_price - $payment->refund_price),2, '.', '');
								$payments[$key]->refund = '-'.number_format($payment->refund_price,2, '.', '');
							} else {
								$payments[$key]->credit = number_format($payment->final_price,2, '.', '');
							}
						}

						$applData = $this->model->getAwarded($payment->ccr_id,$payment->tutor_id);
						if(!empty($applData)){
							if($applData->payment_status){
								$payments[$key]->status = 'Credited';
							} else {
								$payments[$key]->status = 'Outstanding';
							}
						}
					}

				}
			}
		}

		$this->data['take'] = $take;
		$this->data['page_no'] = $page;
		$this->data['numbe'] = ($page-1) * $take;
		$this->data['payments'] = $payments;
		$this->data['payments_count'] = $paymentsResult['count'];
		return view('customcourserequest.payments',$this->data);
	}

	function getAward(Request $request)
	{
		$id = $request->input('id');
		$appl = $this->model->getapplinfo($id);
		$ccr_id = $appl->ccr_id;
		$tutor_id = $appl->user_id;
		$alink = \URL::to('customcourserequest/logoutbbb/'.$ccr_id);
		$qinfo = $this->model->getccrinfo($ccr_id);
		$usrinfo = \bsetecHelpers::getuserinfobyid($tutor_id);
		$attinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);

		$awardStatus = $this->model->getAwardStatus($id,$qinfo->entry_by,$tutor_id);

		if($awardStatus){

			if($qinfo->type == 'online'){
				$meeting_slug = \bsetecHelpers::getGUID();
				$meeting_create = $this->createBBBMeeting($meeting_slug,$qinfo->ccr_name,$alink,'2');
		        // print_r($meeting_create);

				$moderator_join_url = '';
				$attendee_join_url = '';
				if($meeting_create['status']){
					$moderator_join_url = $this->bbb->getJoinMeetingURL(array('meetingId' => $meeting_slug, 'username' => $usrinfo->first_name.' '.$usrinfo->last_name, 'password' => 'mp','createTime' => '', 'userId' => '', 'webVoiceConf' => ''));
					$attendee_join_url = $this->bbb->getJoinMeetingURL(array('meetingId' => $meeting_slug, 'username' => $attinfo->first_name.' '.$attinfo->last_name, 'password' => 'ap','createTime' => '', 'userId' => '', 'webVoiceConf' => ''));
				}
				$now_date = date("Y-m-d H:i:s");
				$meeting_data = array(
					'meeting_slug' => $meeting_slug,
					'meeting_type' => 'course_request',
					'ccr_id' => $ccr_id,
					'tutor_id' => $tutor_id,
					'tutor' => $usrinfo->username,
					'attendee_id' => $qinfo->entry_by,
					'attendees' => $attinfo->username,
					'meeting_name' => $qinfo->ccr_name,
					'moderator_join_url' => $moderator_join_url,
					'attendee_join_url' => $attendee_join_url,
					'created_at' => $now_date,
					'updated_at' => $now_date
				);
				$goodToGo = $this->model->createmeeting($meeting_data,$ccr_id);

			} else {
				$goodToGo = true;
			}

			if($goodToGo){
				$this->model->awardRequest($id,$qinfo->payment_type,$qinfo->entry_by);
	            //Awarded mail
				$fromMail   = CNF_EMAIL;
				$toMail     = $usrinfo->email;
				$subject    = CNF_APPNAME." Course Request Awarded";
				$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name);
				$tempname   = 'customcourserequest.mail.awarded';
				// return view('customcourserequest.mail.awarded')->with($data);
				// $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);

				return Redirect::to('customcourserequest/show/'.$ccr_id)
				->with('messagetext', \Lang::get('core.awarded_success'))->with('msgstatus','success');

			} else {

				return Redirect::to('customcourserequest/show/'.$ccr_id)
				->with('messagetext', \Lang::get('core.course_failure'))->with('msgstatus','error')->with('msgstatus','error');

			}

		} else {
			return Redirect::to('customcourserequest/show/'.$ccr_id)
			->with('messagetext', \Lang::get('core.no_suffcient_funds'))->with('msgstatus','error')->with('msgstatus','error');
		}
	}

	function createBBBMeeting($meeting_slug,$ccr_name,$alink,$maxParticipants = '-1'){
		$creationParams = array(
	        'meetingId' => $meeting_slug,               // REQUIRED
	        'meetingName' => $ccr_name,   				// REQUIRED
	        'attendeePw' => 'ap',                   	// Match this value in getJoinMeetingURL() to join as attendee.
	        'moderatorPw' => 'mp',                  	// Match this value in getJoinMeetingURL() to join as moderator.
	        'welcomeMsg' => '',  						// ''= use default. Change to customize.
	        'dialNumber' => '',                     	// The main number to call into. Optional.
	        'voiceBridge' => '',                    	// PIN to join voice. Optional.
	        'webVoice' => '',                       	// Alphanumeric to join voice. Optional.
	        'logoutUrl' => $alink,                     	// Default in bigbluebutton.properties. Optional.
	        'maxParticipants' => $maxParticipants,      // Optional. -1 = unlimited. Not supported in BBB. [number]
	        'record' => 'true',                    		// New. 'true' will tell BBB to record the meeting.
	        'duration' => '0',                      	// Default = 0 which means no set duration in minutes. [number]
	        //'meta_category' => '',                	// Use to pass additional info to BBB server. See API docs.
	    );
	    $meeting_create = $this->bbb->BBBcreateMeeting($creationParams);
	    return $meeting_create;
	}

	function getStart($id = null)
	{
		$uid = \Session::get('uid');
		$qinfo = $this->model->getccrinfo($id);
		$minfo = $this->model->getmeetinginfo($qinfo->meeting_id);
		$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->tutor_id);
		$attinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
		if(!empty($minfo)){

			$meetingExists = true;
			$gotoMeeting = true;
			$check_credits = false;
			$detect = false;

			if($qinfo->payment_type == 'hour'){
				$time_spent = 0;

				if(!empty($minfo->meeting_duration)){	// if duration is not empty
					$durations = json_decode($minfo->meeting_duration,true);
					foreach ($durations as $key => $duration_value) {
						if(isset($duration_value['end'])){
							$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
						}
					}
					if($time_spent != 0){
						$hours_spent = $time_spent / (60 * 60);
						if($hours_spent >= $qinfo->deducted_hours){
							$check_credits = true;
						}
					}
				} else {
					$check_credits = true;
				}

				if($check_credits){
					$user_credits = $this->model->userCredits($qinfo->entry_by);
					if($user_credits < $qinfo->final_price){
						$gotoMeeting = false;
					}
				}
			}

			if($gotoMeeting){
				// CREATE A NEW BBB MEETING IF NOT FOUND
				$infoParams = array(
					'meetingId' => $minfo->meeting_slug, 		// REQUIRED - We have to know which meeting.
					'password' => 'mp',							// REQUIRED - Must match moderator pass for meeting.
				);
				$meetinginfo = $this->bbb->BBBgetMeetinginfo($infoParams);
				$moderator_join_url = $minfo->moderator_join_url;
				$attendee_join_url = $minfo->attendee_join_url;
				if(!$meetinginfo['status']){
					$meetingExists = false;
					$alink = \URL::to('customcourserequest/logoutbbb/'.$id);
					$meeting_create = $this->createBBBMeeting($minfo->meeting_slug,$qinfo->ccr_name,$alink,'2');
					if($meeting_create['status']){
						$moderator_join_url = $this->bbb->getJoinMeetingURL(array('meetingId' => $minfo->meeting_slug, 'username' => $usrinfo->first_name.' '.$usrinfo->last_name, 'password' => 'mp','createTime' => '', 'userId' => '', 'webVoiceConf' => ''));
						$attendee_join_url = $this->bbb->getJoinMeetingURL(array('meetingId' => $minfo->meeting_slug, 'username' => $attinfo->first_name.' '.$attinfo->last_name, 'password' => 'ap','createTime' => '', 'userId' => '', 'webVoiceConf' => ''));
						$meetingExists = true;
					}
				}

				if($meetingExists){
					$type = '';
					if($minfo->meeting_status == 0) $type = 'join';
					else if($minfo->meeting_status == 1) $type = 'start';

					if($uid == $minfo->tutor_id && !empty($type)){
						$this->model->updatemeeting($uid,$minfo,$qinfo,$type);
						return Redirect::to($moderator_join_url);
					} else if($uid == $minfo->attendee_id && !empty($type)){
						$this->model->updatemeeting($uid,$minfo,$qinfo,$type);
						return Redirect::to($attendee_join_url);
					}
				} else {
					return Redirect::to('customcourserequest/show/'.$id)
					->with('messagetext', \Lang::get('core.course_failure'))->with('msgstatus','error')->with('msgstatus','error');
				}

			} else {
				if($uid == $minfo->tutor_id){
					$msg = 'core.credits_failure_student';
				} else if($uid == $minfo->attendee_id){
					$msg = 'core.credits_failure_you';
				}
				return Redirect::to('customcourserequest/show/'.$id)->with('messagetext', \Lang::get($msg))->with('msgstatus','error');
			}
		}
		return Redirect::to('customcourserequest/show/'.$id)->with('messagetext', \Lang::get('core.course_failure'))->with('msgstatus','error');
	}

	function postProposalprice(Request $request)
	{
		$id = $request->input('ccr_id');
		$this->model->proposalprice($request->input('dispute_id'),$request->input('price'),$request->input('type'));
		return Redirect::to('customcourserequest/show/'.$id)
			->with('messagetext', \Lang::get('core.note_success'))->with('msgstatus','success');
	}

	function postFinishdispute(Request $request)
	{
		$id = $request->input('ccr_id');
		$price = $request->input('price');
		$dispute_id = $request->input('dispute_id');

		$qinfo = $this->model->getccrinfo($id);
		$amount_calc = 0;
		$amount_return = 0;
		if($qinfo->payment_type == 'hour'){
			$minfo = $this->model->getmeetinginfo($qinfo->meeting_id);
			$meeting_duration = $minfo->meeting_duration;
			if(!empty($meeting_duration)){
				$meet_duration = json_decode($meeting_duration,true);
				$time_spent = 0;
				foreach ($meet_duration as $key => $duration_value) {
					if(isset($duration_value['end'])){
						$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
					}
				}
				if($time_spent != 0){
					$amount_ref = $qinfo->final_price - $price;
					$amount_calc = round($amount_ref * ($time_spent / (60 * 60)),2);
					$round_off = ($qinfo->deducted_hours * 3600) - $time_spent;
					if($round_off > 0){
						$amount_return = round($amount_ref * ($round_off / (60 * 60)),2);
					}
				}
			}
		} else {
			$amount_calc = $qinfo->final_price - $price;
			$amount_return = (float)$price;
		}

		$commision_percentage = $this->bsetec->get_option('commision_percentage') + $this->bsetec->get_option('transaction_charges_percentage');
		//calculate the credits
		$admin_credit = ($amount_calc * $commision_percentage)/100;
		$amount_calc = $amount_calc - $admin_credit;

		$this->model->finishdispute($id,$amount_calc,$amount_return,$qinfo->entry_by,$dispute_id,$price);

		if (defined('CNF_CURRENCY'))
			$currency = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
		else
			$currency = 'USD';

		// send to student
		$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
		$authorname = $usrinfo->first_name.' '.$usrinfo->last_name;
		$notificaton = $this->notification_settings->where('user_id',$qinfo->entry_by)->first();
		if(count($usrinfo)>0 && (empty($notificaton) || (!empty($notificaton) && $notificaton->notif_ccr_dispute == 0 && $notificaton->notif_for_all == 0))){
            //join meeting mail
			$alink      = \URL::to('customcourserequest/show/'.$id);
			$fromMail   = CNF_EMAIL;
			$toMail     = $usrinfo->email;
			$subject    = CNF_APPNAME." Course Request Dispute Closed";
			$data       = array('item'=>'Course Request','authorname'=>$authorname,'link'=>$alink,'title'=>$qinfo->ccr_name,'type'=>'created','payment_type'=>$qinfo->payment_type,'price'=>$price,'revised'=>$amount_calc,'return'=>$amount_return,'currency'=>$currency);
			$tempname   = 'customcourserequest.mail.dispute';
			// return view('customcourserequest.mail.dispute')->with($data);
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		}

		// send to tutor
		$tusrinfo = \bsetecHelpers::getuserinfobyid($qinfo->tutor_id);
		$tauthorname = $tusrinfo->first_name.' '.$tusrinfo->last_name;
		$tnotificaton = $this->notification_settings->where('user_id',$qinfo->tutor_id)->first();
		if(count($usrinfo)>0 && (empty($tnotificaton) || (!empty($tnotificaton) && $tnotificaton->notif_ccr_dispute == 0 && $tnotificaton->notif_for_all == 0))){
            //join meeting mail
			$alink      = \URL::to('customcourserequest/show/'.$id);
			$fromMail   = CNF_EMAIL;
			$toMail     = $tusrinfo->email;
			$subject    = CNF_APPNAME." Course Request Dispute Closed";
			$data       = array('item'=>'Course Request','authorname'=>$tauthorname,'link'=>$alink,'title'=>$qinfo->ccr_name,'type'=>'awarded','payment_type'=>$qinfo->payment_type,'price'=>$price,'revised'=>$amount_calc,'return'=>$amount_return,'currency'=>$currency);
			$tempname   = 'customcourserequest.mail.dispute';
			// return view('customcourserequest.mail.dispute')->with($data);
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		}

		return Redirect::to('customcourserequest/show/'.$id)
			->with('messagetext', \Lang::get('core.note_success'))->with('msgstatus','success');
	}

	function postCancel(Request $request)
	{
		$uid = \Session::get('uid');
		$data['ccr_id'] = $request->input('ccr_id');
		$id = $data['ccr_id'];
		$data['reason'] = $request->input('reason');
		$amount_refund = $request->input('amount_refund') ? $request->input('amount_refund'):'' ;

		$qinfo = $this->model->getccrinfo($id);

		if($qinfo->entry_by == $uid){
			$data['price_proposed_by_student'] = $amount_refund;
			$data['cancelled_by'] = 'student';
		} else if($qinfo->tutor_id == $uid){
			$data['price_proposed_by_tutor'] = $amount_refund;
			$data['cancelled_by'] = 'tutor';
		}
		$now_date = date("Y-m-d H:i:s");
		$data['created_at'] = $now_date;
		$data['updated_at'] = $now_date;

		$cancelled = $this->model->cancelCourse($data);

		// send to student
		$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->entry_by);
		$notificaton = $this->notification_settings->where('user_id',$qinfo->entry_by)->first();
		if(count($usrinfo)>0 && (empty($notificaton) || (!empty($notificaton) && $notificaton->notif_ccr_dispute == 0 && $notificaton->notif_for_all == 0))){
            //join meeting mail
			$alink      = \URL::to('customcourserequest/show/'.$id);
			$fromMail   = CNF_EMAIL;
			$toMail     = $usrinfo->email;
			$subject    = CNF_APPNAME." Course Request Cancelled";
			$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name,'type'=>'created');
			$tempname   = 'customcourserequest.mail.cancel';
			// return view('customcourserequest.mail.cancel')->with($data);
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		}

		// send to tutor
		$tusrinfo = \bsetecHelpers::getuserinfobyid($qinfo->tutor_id);
		$tnotificaton = $this->notification_settings->where('user_id',$qinfo->tutor_id)->first();
		if(count($usrinfo)>0 && (empty($tnotificaton) || (!empty($tnotificaton) && $tnotificaton->notif_ccr_dispute == 0 && $tnotificaton->notif_for_all == 0))){
            //join meeting mail
			$alink      = \URL::to('customcourserequest/show/'.$id);
			$fromMail   = CNF_EMAIL;
			$toMail     = $tusrinfo->email;
			$subject    = CNF_APPNAME." Course Request Cancelled";
			$data       = array('item'=>'Course Request','authorname'=>$tusrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name,'type'=>'awarded');
			$tempname   = 'customcourserequest.mail.cancel';
			// return view('customcourserequest.mail.cancel')->with($data);
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		}

		return Redirect::to('customcourserequest/show/'.$id)
			->with('messagetext', \Lang::get('core.cancelled_success'))->with('msgstatus','success');
	}

	function postMessagesend( Request $request, $id =0)
	{

		$user = \Auth::user();
		$uid = $user->id;

		$data['entry_by'] = $uid;
		$data['recipient'] = $request->input('recipient');
		$data['draft'] = $request->input('draft');
		$data['message'] = $request->input('message');
		$data['subject'] = $request->input('subject');
		$now_date = date("Y-m-d H:i:s");
		$data['createdOn'] = $now_date;
		$data['updatedOn'] = $now_date;
		$data['read'] = '1';

		$Message = new Message;
		$newID = $Message->insertRow($data, '');

		$aid = $request->input('applicationid');
		$this->model->messageStore($aid,$newID);

		$usrinfo = \bsetecHelpers::getuserinfobyid($uid);
		$imgpath   = \SiteHelpers::customavatar($usrinfo->email,$uid);
		$user_link = \URL::to('profile/'.$usrinfo->username);
        $display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
		$message = $data['message'];

		$is_admin = false;
		if($usrinfo->group_id == '1'){
			$is_admin = true;
		}

        $data = array('imgpath'=>$imgpath,'user_link'=>$user_link,'display_name'=>$display_name,'message'=>$message,'is_admin'=>$is_admin);
        $message_block = view('customcourserequest.message_block')->with($data);

		// send message
		$usrinfo = \bsetecHelpers::getuserinfobyid($request->input('recipient'));
		if(count($usrinfo)>0){
            //join meeting mail
			$alink      = \URL::to('customcourserequest/show/'.$id);
			$fromMail   = CNF_EMAIL;
			$toMail     = $usrinfo->email;
			$subject    = CNF_APPNAME." Message Recieved";
			$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$request->input('message'),'type'=>'Message');
			$tempname   = 'customcourserequest.mail.message';
			// return view('customcourserequest.mail.message')->with($data);
			// $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
		}

        return $message_block;

	}

	function postMessageslist(Request $request){
		$message_block = ' ';
		$user = \Auth::user();
		$uid = $user->id;
		$aid = $request->input('application_id');
		$messageList = $this->model->messageList($aid);
		if(!empty($messageList->message)){
			$messageIds = explode(',',$messageList->message);
			foreach ($messageIds as $key => $messageId) {

				$messageData = $this->model->messageSingle($messageId);
				if($messageData){
					$usrinfo = \bsetecHelpers::getuserinfobyid($messageData->entry_by);
					$imgpath   = \SiteHelpers::customavatar($usrinfo->email,$messageData->entry_by);
					// echo $imgpath;exit;
					$user_link = \URL::to('profile/'.$usrinfo->username);
					$display_name = $usrinfo->first_name.' '.$usrinfo->last_name;
					$message = $messageData->message;

					$is_admin = false;
					if($usrinfo->group_id == '1'){
						$is_admin = true;
					}

					$data = array('imgpath'=>$imgpath,'user_link'=>$user_link,'display_name'=>$display_name,'message'=>$message,'is_admin'=>$is_admin);
					// echo '';print_r($data);exit;
					if($messageData->entry_by == $uid)
						$message_block .= view('customcourserequest.message_block')->with($data);
					else
						$message_block .= view('customcourserequest.message_block_right')->with($data);
				}
			}
		}
        return $message_block;
	}

	public function postCourseshare(Request $request)
    {
        $courseid   = $request->input('course_id');
        $user_id    = $request->input('user_id');
        $ccr_id    	= $request->input('ccr_id');
		$usrinfo 	= \bsetecHelpers::getuserinfobyid($user_id);
        $emailid    = $usrinfo->email;

        $this->model->update_shared_course($courseid,$ccr_id);

        $checkalready = $this->model->check_invite_userexists($courseid,$emailid);
        if(count($checkalready)=='0'){
            $allusers   = $this->model->insert_inviter_users($courseid,$emailid);
        }

		return Redirect::to('customcourserequest/show/'.$ccr_id)
			->with('messagetext', \Lang::get('core.shared_success'))->with('msgstatus','success');
    }

	function getCompleterequest($id = null)
	{
		$this->model->completerequestccr($id);

		return Redirect::to('customcourserequest/show/'.$id)
			->with('messagetext', \Lang::get('core.complete_request_success'))->with('msgstatus','success');
	}

	function getComplete($id = null)
	{
		$qinfo = $this->model->getccrinfo($id);
		$amount_calc = 0;
		$amount_return = 0;
		if($qinfo->payment_type == 'hour'){
			$minfo = $this->model->getmeetinginfo($qinfo->meeting_id);
			$meeting_duration = $minfo->meeting_duration;
			if(!empty($meeting_duration)){
				$meet_duration = json_decode($meeting_duration,true);
				$time_spent = 0;
				foreach ($meet_duration as $key => $duration_value) {
					if(isset($duration_value['end'])){
						$time_spent += strtotime($duration_value['end']) - strtotime($duration_value['start']);
					}
				}
				if($time_spent != 0){
					$amount_calc = round($qinfo->final_price * ($time_spent / (60 * 60)),2);
					$round_off = ($qinfo->deducted_hours * 3600) - $time_spent;
					if($round_off > 0){
						$amount_return = round($qinfo->final_price * ($round_off / (60 * 60)),2);
					}
				}
			}
		} else {
			$amount_calc = $qinfo->final_price;
		}

		$commision_percentage = $this->bsetec->get_option('commision_percentage') + $this->bsetec->get_option('transaction_charges_percentage');
		//calculate the credits
		$admin_credit 	= ($amount_calc * $commision_percentage)/100;
		$amount_calc 	= $amount_calc - $admin_credit;

		$this->model->completeccr($id,$amount_calc,$amount_return,$qinfo->entry_by);

		if(!empty($qinfo)){
			$usrinfo = \bsetecHelpers::getuserinfobyid($qinfo->tutor_id);
			if(count($usrinfo)>0){
	            //approve mail
				$alink      = \URL::to('customcourserequest/show/'.$id);
				$fromMail   = CNF_EMAIL;
				$toMail     = $usrinfo->email;
				$subject    = CNF_APPNAME." Course Request Completed";
				$data       = array('item'=>'Course Request','authorname'=>$usrinfo->username,'link'=>$alink,'title'=>$qinfo->ccr_name);
				$tempname   = 'customcourserequest.mail.completed';
				// return view('customcourserequest.mail.completed')->with($data);
				$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
			}
		}

		return Redirect::to('customcourserequest/show/'.$id)
			->with('messagetext', \Lang::get('core.completed_success'))->with('msgstatus','success');

	}

	public function postUpload(Request $request)
    {
		$document 	= $request->file('luploaddoc');
        $file 		= array('document' => $document);
        $rules 		= array('document' => 'required|mimes:zip,jpg,jpeg,png,pdf,doc,docx,txt');
        $validator 	= Validator::make($file, $rules);

        if( $validator->fails() )
        {
			$return_data[] = array(
                'status'=>false,
            );
		} else {
            $file_name 	= explode('.',$document->getClientOriginalName());
            $file_name 	= $file_name[0].'_'.time().rand(4,9999);
            $file_type 	= $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size 	= $document->getSize();

			$request->file('luploaddoc')->move('./uploads/files/', $file_name.'.'.$file_type
			);

            $courseFiles 				= new CourseFiles;
            $courseFiles->file_name 	= $file_name;
            $courseFiles->file_title	= $file_title;
            $courseFiles->file_type 	= $file_type;
            $courseFiles->file_extension= $file_type;
            $courseFiles->file_size 	= $file_size;
			$courseFiles->duration 		= '';
            $courseFiles->file_tag 		= 'course_request';
            $courseFiles->uploader_id 	= \Auth::user()->id;
            $courseFiles->created_at 	= time();
            $courseFiles->updated_at 	= time();
            if($courseFiles->save()){
                $return_data = array(
                    'id'=>$courseFiles->id,
                    'status'=>true,
                    'file_title'=> $file_title,
                    'file_ext'=> $file_type
                );
            }else{
                $return_data = array(
                    'status'=>false,
                );
            }
        }
        return json_encode($return_data);
        exit;
	}

	public function postDocdelete(Request $request){
		$this->model->postDocdelete($request->input('lid'),$request->input('rid'));
		echo '1';
	}

    public function getDownloadresource($name)
    {
        $geturls     = ($name == null ? '' : \SiteHelpers::encryptID($name,true));
        $filename    = \DB::table('course_files')->where('id',$geturls)->get();
        $file        = "./uploads/files/".$filename['0']->file_name.'.'.$filename['0']->file_extension;
        return \Response::download($file);

    }

    public function getLogoutbbb($id = null,$guid = null,$uid = null){
    	if(!is_null($id)){
    		$uid = \Session::get('uid');
			$qinfo = $this->model->getccrinfo($id);
			$minfo = $this->model->getmeetinginfo($qinfo->meeting_id);
			if(!empty($minfo)){
				if($minfo->meeting_status == 2){
					$this->model->updatemeeting($uid,$minfo,$qinfo,'end');
				} else if($minfo->meeting_status == 1){
					$this->model->updatemeeting($uid,$minfo,$qinfo,'close');
				}
			}
    	}

		return Redirect::to('customcourserequest/show/'.$id);
    }

    public function getLogbbb($id = null,$guid = null,$uid = null){
    	if(!is_null($id)){
			$qinfo = $this->model->getccrinfo($id);
			$minfo = $this->model->getmeetinginfo($qinfo->meeting_id);
			if(!empty($minfo)){
				if($minfo->meeting_status == 2){
					$uid = \Session::get('uid');
					$status = $this->model->updatemeeting($uid,$minfo,$qinfo,'log',$guid);
					if(!$status){
						$endParams = array(
							'meetingId' => $minfo->meeting_slug,// REQUIRED - We have to know which meeting to end.
							'password' => 'mp',	// REQUIRED - Must match moderator pass for meeting.
						);
						$meeting_end = $this->bbb->BBBendMeeting($endParams);
						if($meeting_end['status']){
    						$result = array('status'=>'0');
							return 'logResult('.json_encode($result).')';
						}
					}
				}
			}
    	}
    	$result = array('status'=>'1');
		return 'logResult('.json_encode($result).')';
    }

    public function postInstructors(Request $request)
    {
        $searchtext = $request->input('q');
        $loggeduser	= \Session::get('uid');
        $selectedtutors['results'] = \DB::table('users')->select('id AS id','username AS text')->where('active', '=' , '1')->where('username', 'like', '%'.$searchtext.'%')->where('id', '!=', $loggeduser)->get();
        return json_encode($selectedtutors);
       
    }
}