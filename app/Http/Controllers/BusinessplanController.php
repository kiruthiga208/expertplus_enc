<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Businessplan;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\User;
use App\Models\Users;
use App\Models\Transaction;
use App\Models\CourseTaken;
use App\Models\bsetec;
class BusinessplanController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'businessplan';
	static $per_page	= '10';

	public function __construct()
	{
		$this->model = new Businessplan();
		$this->users = new Users();

		$this->info = $this->model->makeInfo( $this->module);

		$this->access = $this->model->validAccess($this->info['id']);

		$this->bsetec = new bsetec();
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'businessplan',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'business_plan_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('businessplan');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('businessplan.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['business_plan_statement'] = json_decode($row['business_plan_statement']);
		} else {
			$this->data['row'] = $this->model->getColumnTable('business_plan');
			$this->data['business_plan_statement']['0'] = '';  
		}

		$this->data['id'] = $id;
		return view('businessplan.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('business_plan'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('businessplan.view',$this->data);	
	}	

	function postSave( Request $request, $id ='')
	{
		$rules = $this->validateForm();
		$rules['business_plan_name']			= 'required';
		//if($request->input('business_plan_solicitor_count')!='0'){
			$rules['business_plan_amount'] 		= 'required|numeric';
		//}
		$rules['business_plan_course_count'] = 'required|numeric|not_in:0';
		$rules['business_plan_interval'] 	= 'required';
		$rules['business_plan_interval.*'] 	= 'required';
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_businessplan');
			$requestid 			= \Request::segment(3);
			$now_date 			= date("Y-m-d H:i:s");
			if(isset($requestid)){
				$planid = $requestid;
				$data['modified_at'] 	= $now_date;
			}else{
				$planid = '';
				$data['created_at'] 	= $now_date;
				$data['modified_at'] 	= $now_date;
			}
			$data['business_plan_name']			= $request->input('business_plan_name');
			$data['business_plan_amount'] 		= $request->input('business_plan_amount');
			$data['business_plan_interval'] 	= $request->input('business_plan_interval');
			$data['business_plan_statement'] 	= json_encode($request->input('business_plan_statement'));
			$data['business_plan_periods']		= ($request->input('business_plan_interval')==1) ? '30' : '365';
			$data['business_plan_course_count'] = $request->input('business_plan_course_count');
			$newID = $this->model->insertRow($data , $request->input('business_plan_id'));



			//get values from db and pass it to paypal
			if(empty($planid)){

			$stripe = $this->bsetec->get_options('stripe');
			\Stripe\Stripe::setApiKey($stripe['secret_key']);
			if(!empty($planid)){
				$inserid = $planid;
			}else{
				$inserid = $newID;
			}

			$product_id = \Stripe\Product::create(array(
			  "name" => $request->input('business_plan_name'),
			  "type" => "service",
			));
			 $response = \Stripe\Plan::create([
	                	"id" 	 	=> $inserid,
	                	"product" => array(
						    "name" => $product_id->name
						  ),
	                	"amount" 	=> $request->input('business_plan_amount')*100,
	                	"interval" 	=> ($data['business_plan_interval'] == '2' ? "year":"month"),
	                	"currency" 	=> CNF_CURRENCY
	                	]);
			}
			
			if(!is_null($request->input('apply')))
			{
				$return = 'businessplan/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'businessplan?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('businessplan/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator);
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			foreach ($request->input('id') as $key => $value) {
				$businessplans = \DB::table('business_plan')->where('business_plan_id',$value )->first();

			$stripe = $this->bsetec->get_options('stripe');
			if($stripe['payment'] == 1){
        		\Stripe\Stripe::setApiKey($stripe['secret_key']);
                if($businessplans->business_plan_id){
                   	$monthly_id = $businessplans->business_plan_id;
                    // monthly plan
					$plan_m = \Stripe\Plan::retrieve($monthly_id);
					$plan_m->delete();
                }
     //            if($memberships->stripe_plan_y){
     //                $yearly_id = $memberships->stripe_plan_y;
     //                $plan_y = \Stripe\Plan::retrieve($yearly_id);
					// $plan_y->delete();
     //            }
        	}
        }
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('businessplan')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('businessplan')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

	public function getCourses(Request $request){

		 if(!\Auth::check()) return Redirect::to('user/login');

		$totalcount = \bsetecHelpers::getDashboards();
		if(count($totalcount)==0){
			return Redirect::to('');
		}

		if(count($totalcount)>0)
		{
			$coursecount = 0;
			foreach ($totalcount as $key => $value) {
				$count = $coursecount + $value->no_of_course;
				$coursecount = $count;
			}
			$createdcourse = $this->model->createdcourse(\Auth::user()->id);
			$createdcount = count($createdcourse);

		}

		$data 					= array();
		$data['totalcount'] = $count;
		$data['createdcount'] = $createdcount;
		return view('businessplan.mycourses',$data);
	}

	public function getAllcourses(Request $request){
		if(\Auth::check()){

			$uid 					= \Session::get('uid');
			$getdraw   				= $request->input('sEcho','');
			$start 					= $request->input('iDisplayStart','');
			$length 				= $request->input('iDisplayLength','');
			$getlists 				= $this->model->getAllcourses($uid, $start, $length);
			$getalllists 			= $this->model->getAllcourses($uid, $start, $length,1);
			$outputstart 			= array();
			$sno 					= 1;
			$customKey              = 0;

			$createcourse = $this->model->createdcourse($uid);
			$createdcoursecount = count($createcourse);

			foreach ($getlists as $key => $uservalue) {
					if($uservalue->course_type=='1'){
						$status = '<a href="'.\URL::to('businessplan/assignuserbysubscription/'.$uservalue->taken_id.'/'.$uservalue->course_id).'" class="btn btn-default"><i class="fa fa-users"></i> Assign User</a>';
					}

				$ptype  = '<span class="label label-default">Business</span>';
				$checkedActive = '0';
				if($uservalue->course_type=='1'){
					$ptype  = '<span class="label label-info">Subscription</span>';
					$getcatinfo = $this->model->getPlanbyid($uservalue->course_id);
											
					if(count($getcatinfo)>0)
					{
						$checkedActive = '1';
						$catname 	= $getcatinfo['0']->business_plan_name;
					}

				}

				if($checkedActive == '1'){
					$outputstart[$customKey][] = $sno;
					$outputstart[$customKey][] = $catname;
					$outputstart[$customKey][] = $uservalue->no_of_course;
					//$outputstart[$customKey][] = $createdcoursecount;
					$outputstart[$customKey][] = $ptype;
				//	$outputstart[$customKey][] = $status;
					$sno++;
					$customKey++;
				}
			}

			$filter    				= array_filter($outputstart);
	        $json_data 				= array(
							            "draw"            => intval($getdraw),
							            "recordsTotal"    => count($getalllists),
							            "recordsFiltered" => count($getalllists),
							            "data"            => $filter   // total data array
						            );

	        echo json_encode($json_data);

	    }
	}	

	public function getAssignuserbysubscription(Request $request){
		$data 						= array();
		$uid 						= \Session::get('uid');

		$getcourses = array();
		$courses = array();

		
		$course_ids  = $this->model->getBusinessusercourse($uid);
		$courses = $this->model->getcoursebybusinessuser($uid);
		foreach ($course_ids as $key => $value) {
			$getcourses[] = $this->model->getallcourse($value->course_id);
		}

	
		$userlist = $this->model->getUserslist();


		//$getcourses 				= $this->model->getallcourse();

		$courselist 				= array('0'=> \Lang::get('core.choose_course_name'));
		$totals 						= array();
		if(count($getcourses)>0){
			foreach ($getcourses as $key => $cvalue) {
				$courselist[$cvalue->course_id] = $cvalue->course_title;
				$totals[] = $cvalue->course_id;
			}
		}

		if(count($courses)>0){
			foreach ($courses as $key => $covalue) {
				$courselist[$covalue->course_id] = $covalue->course_title;
				$totals[] = $covalue->course_id;
			}
		}

		// $getsubinfo 			= $this->model->getPlanbyid($id);
		// if(count($getsubinfo)==0 || empty($id)){
		// 	return \Redirect::to('');
		// }


		$userslist 					= array('0'=>'Choose Students');
		if(count($userlist)>0){
			foreach ($userlist as $key => $users) {
				$userslist[$users->id] = $users->username;

			}
		}

		$data['users_lists']		= $userslist;
		$data['course_lists']		= $courselist;
	//	$data['subname']			= $getsubinfo['0']->business_plan_name;
		//$data['subid']				= $id;
		//$data['taken_id']			= $taken_id;
		return view('businessplan.assignusers_by_subscription',$data);
	}

	public function postAssignusersbysubs(Request $request){
		$users_id 		= $request->input('users_id');
		$course_id		= $request->input('course_id');		
		$uid 			= \Session::get('uid');

		// check purchased course quantity
		$getquantity 	= $this->model->checkcoursequantity($uid, $course_id);
		$assigneduser 	= $this->model->getAssignedinfobysub($uid,  $course_id);

		if(count($getquantity)>0){
			$total 			= $getquantity->quantity;
			$users			= count($assigneduser);
			$remaining 		= $total-$users;
			if(count($users_id)>$remaining){

				if($users>0){
					$emessage    = \Lang::get('core.allowed_for').$total.' students. '.\Lang::get('core.alread_for').$users.' students';
				}else{
					$emessage 	= \Lang::get('core.allowed_for').$total.' students.';
				}

				return Redirect::to('businessplan/assignuserbysubscription')->with('message',\SiteHelpers::alert('error',$emessage));
			}
		}
		// end

		if(count($users_id)>0 && !empty($course_id)){
			$i = 0;
			foreach ($users_id as $key => $userdata) {
				$checkexists 				= $this->model->checkTransactions($userdata,$course_id);
				 $Subscriptioncourse =  \DB::table('course_taken')->where('user_id', '=', $uid)->where('course_type', '=', '1')->first();
				 $getSubscription    =  \DB::table('transactions')->where('user_id', '=', $uid)->where('purchase_type', '=', '1')->first();
				 if(!empty($Subscriptioncourse))
				 	$course_taken = $Subscriptioncourse->taken_id;
				 else
				 	$course_taken = "";

				 if(!empty($getSubscription)){
                 	$takenid    = $getSubscription->id;
                 	$created_at = $getSubscription->created_at;
				 }else{
				 	$takenid = 0;
				 	$created_at = 0;
				 }

				$purchase_type = '0';
				if(count($checkexists)=='0'){
					$purchase 					= new Transaction();
			        $purchase->user_id 			= $userdata;
			        $purchase->course_id 		= $course_id;
			        if(!empty($created_at)){
			        	$purchase->created_at 	= $created_at;
			        }
			        $purchase->amount 			= 0;
			        $purchase->status 			= 'completed';
			        $purchase->payment_method 	= "business";
			        $purchase->business_user_id = $uid;
			        $purchase->expire_id 		= $takenid;
			        $purchase->save();
			        // add for taken course by user
			        $courseTaken 				= new CourseTaken();
			        $courseTaken->user_id 		= $userdata;
			        if(!empty($created_at)){
			        	$courseTaken->created_at = $created_at;
			        }
			        $courseTaken->course_id 	= $course_id;
			        $courseTaken->business_user_id = $uid;
			        $courseTaken->course_type = $purchase_type;
			        $courseTaken->expire_id = $course_taken;
			        $courseTaken->save();
			    }
		        $i++;
			}
			$message   = \Lang::get('core.assign_sucess');
			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('success',$message));
		}else{
			$emessage  = \Lang::get('core.business_status_error');
			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('error',$emessage));
		}
	}

	public function postGetuserlistbyid(Request $request){

		if(!\Auth::check()) return Redirect::to('user/login');

		$uid 						= \Session::get('uid');
		$cid 						= $request->input('cid');
		$getuserlists 				= $this->model->getStudentinfos($uid, $cid);
		$userslist 					= array();
		if(count($getuserlists)>0){
			foreach ($getuserlists as $key => $uvalue) {
				$userslist[$uvalue->id] = $uvalue->email;
			}
		}

		$data['userslist'] 		= $userslist;

		return view('businessplan.getlists',$data);
	}


	public function getStudents(){

		if(!\Auth::check()) return Redirect::to('user/login');

		$uid 					= \Session::get('uid');
		$gettotalstudents 		= $this->model->allowstudents($uid);
		$gettransstudents 		= $this->model->allowstudents($uid,1);
		$getcstudents 			= $this->model->allowstudents($uid,2);
		$data 					= array();

		$totals 				= 0;
		$remaining				= 0;
		$transaction			= 0;
		if(count($gettotalstudents)>0){
			foreach ($gettotalstudents as $key => $infovalue) {
				$totals += $infovalue->no_of_course;
			}
			// $totals  			= $gettotalstudents['0']->total_students;
		}

		$checkcreated = count($getcstudents);
		if(count($gettransstudents)=='0'){
			if(!empty($totals)){
				$remaining 	  = $totals-$checkcreated;
			}
		}elseif(count($gettransstudents)>0){
			$transaction 	  = $totals - count($gettransstudents);
			$create 		  = $totals - $checkcreated;
			$remaining  	  = 1;
			if($transaction=='0' || $create=='0'){
				$remaining = 0;
			}
		}

		$data['coursecount']	= $remaining;
		return view('businessplan.studentlists',$data);
	}

	public function getAllstudents(Request $request){

		$uid 					= \Session::get('uid');
		$getdraw   				= $request->input('sEcho','');
		$start 					= $request->input('iDisplayStart','');
		$length 				= $request->input('iDisplayLength','');
		$getlists 				= $this->model->getStudents($uid, $start, $length);
		$getalllists 			= $this->model->getStudents($uid, $start, $length,1);

		$outputstart 			= array();
		$sno 					= 1;
		foreach ($getlists as $key => $uservalue) {
			if($uservalue->active=='1'){
				$message  = '<span class="label label-success">'.\Lang::get('core.fr_mactive').'</span>';
			}else{
				$message  = '<span class="label label-danger">'.\Lang::get('core.fr_minactive').'</span>';
			}
			$outputstart[$key][] = $sno;
			$outputstart[$key][] = '<a href="'.\URL::to('profile/'.$uservalue->username).'" target="_blank">'.\SiteHelpers::customavatar($uservalue->email,$uservalue->id,'small').'</a>';
			$outputstart[$key][] = $uservalue->username;
			$outputstart[$key][] = $uservalue->email;
			$outputstart[$key][] = $message;
			// $outputstart[$key][] = '<a href="'.\URL::to('businessplan/studentform/'.$uservalue->id).'"><i class="fa fa-edit"></i></a> | <a href="javascript:void(0);" class="deleteusers" data-sid="'.$uservalue->id.'" data-uid="'.$uid.'"><i class="fa fa-trash-o"></i></a>';
			$outputstart[$key][] = '<a href="'.\URL::to('businessplan/studentsum/'.$uservalue->id).'" class="btn btn-default"><i class="fa fa-eye"></i> Course Summary</a> 
			<a href="'.\URL::to('businessplan/studentcourselist/'.$uservalue->id).'" class="btn btn-default">Course Status</a>';
			$sno++;
		}

		$filter    				= array_filter($outputstart);
        $json_data 				= array(
						            "draw"            => intval($getdraw),
						            "recordsTotal"    => count($getalllists),
						            "recordsFiltered" => count($getalllists),
						            "data"            => $filter   // total data array
					            );
        echo json_encode($json_data);
	}


	public function getDeletestudents(Request $request, $sid=0){
		$uid 				= \Session::get('uid');
		if(!empty($uid) && !empty($sid)){
			$checkbusiness = $this->model->checkBusinessid($uid, $sid);
			if($checkbusiness==0){
				return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('error',\Lang::get('core.student_delete_error')));
			}
			$user = Users::find($sid);
			$getImage = CourseImages::find($user->avatar);
			if($getImage != null){
				$image_path = $getImage->id.".".$getImage->image_type;
		    	if(file_exists('./uploads/users/'.$image_path)){
	            	File::Delete('./uploads/users/'.$image_path);
	            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
		        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
                    File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
	        		$getImage->delete();
	        	}
	        }

			$this->model->destroy($sid);
			$this->model->removeCoursestudents($sid);

			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('success',\Lang::get('core.student_delete_sucess')));
		}else{
			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('error',\Lang::get('core.student_delete_error')));
		}

	}


	public function getStudentform(Request $request, $id ='') {

		$uid 					= \Session::get('uid');
		$gettotalstudents 		= $this->model->allowstudents($uid);
		$gettransstudents 		= $this->model->allowstudents($uid,1);
		$getcstudents 			= $this->model->allowstudents($uid,2);
		$totals 				= 0;
		$remaining				= 0;
		$transaction 			= 0;
		if(count($gettotalstudents)>0){
			foreach ($gettotalstudents as $key => $infovalue) {
				$totals += $infovalue->no_of_course;
			}
			// $totals  			= $gettotalstudents['0']->total_students;
		}

		$checkcreated = count($getcstudents);
		if(count($gettransstudents)=='0'){
			if(!empty($totals)){
				$remaining 	  = $totals-$checkcreated;
			}
		}elseif(count($gettransstudents)>0){
			$transaction 	  = $totals - count($gettransstudents);
			$create 		  = $totals - $checkcreated;
			$remaining  	  = 1;
			if($transaction=='0' || $create=='0'){
				$remaining = 0;
			}
		}

		if(($remaining=='0' || is_null($remaining)) && empty($id)){
			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('error',\Lang::get('core.assign_review')));
		}

		$data 					=	array();

		if(!empty($id)){
			$row 				= User::find($id);
			$email 				= $row->email;
			$first_name 		= $row->first_name;
			$last_name 			= $row->last_name;
			$username 			= $row->username;
			$status 			= $row->active;
			$id 				= $id;
		}else{
			$email 				= $request->input('email','');
			$first_name 		= $request->input('first_name','');
			$last_name 			= $request->input('last_name','');
			$username 			= $request->input('username','');
			$status 			= 1;
			$id 				= $id;
		}
		$data['email']			= $email;
		$data['first_name'] 	= $first_name;
		$data['last_name']  	= $last_name;
		$data['username'] 		= $username;
		$data['status'] 		= $status;
		$data['id'] 			= $id;

		return view('businessplan.studentform',$data);
	}

	public function postCreatestudent(Request $request, $id=' ') {

		$uid		= \Session::get('uid');
		$rules 		= $this->validateForm();
		if(!empty($id)){
			if($request->input('password')!='' && $request->input('password_confirmation')!=''){
				$rules['password'] 				= 'required|alpha_num|between:6,12|confirmed';
				$rules['password_confirmation'] = 'required|alpha_num|between:6,12';
			}
			$rules['username'] 			= 'required|alpha_num||min:2|unique:users,username,'.$request->input('id');
			$rules['firstname'] 		= 'required|alpha|min:2';
			$rules['lastname'] 			= 'required|alpha|min:2';
			$rules['email'] 			= 'required|email|unique:users,email,'.$request->input('id');

		}else{
			$rules = array(
					'username'	=> 'required|alpha_num|unique:users|min:2',
					'firstname' => 'required|alpha|min:2',
					'lastname'	=> 'required|alpha|min:2',
					'email'		=> 'required|email|unique:users',
					'password'	=> 'required|alpha_num|between:6,12|confirmed',
					'password_confirmation'	=> 'required|alpha_num|between:6,12'
			);
		}

		$gettotalstudents 		= $this->model->allowstudents($uid);
		$gettransstudents 		= $this->model->allowstudents($uid,1);
		$getcstudents 			= $this->model->allowstudents($uid,2);
		$totals 				= 0;
		$remaining				= 0;
		$transaction 			= 0;
		if(count($gettotalstudents)>0){
			foreach ($gettotalstudents as $key => $infovalue) {
				$totals += $infovalue->no_of_employee;
			}
			// $totals  			= $gettotalstudents['0']->total_students;
		}

		$checkcreated = count($getcstudents);
		if(count($gettransstudents)=='0'){
			if(!empty($totals)){
				$remaining 	  = $totals-$checkcreated;
			}
		}elseif(count($gettransstudents)>0){
			$transaction 	  = $totals - count($gettransstudents);
			$create 		  = $totals - $checkcreated;
			$remaining  	  = 1;
			if($transaction=='0' || $create=='0'){
				$remaining = 0;
			}
		}



		if(($remaining==0 || is_null($remaining)) && empty($id)){
			$emessage = \Lang::get('core.assign_review');
			return Redirect::to('businessplan/studentform')->with('message',\SiteHelpers::alert('error',$emessage));
		}

		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {

			if(!empty($request->input('id'))){
				$authen 					= Users::find($request->input('id'));
				$message 					= \Lang::get('core.student_update_success');
			}else{
				$code 						= rand(10000,10000000);
				$authen 					= new User;
				//for business
				$authen->business_id		= \Session::get('uid');
				$authen->activation 		= $code;
				$authen->group_id 			= 3;
				$message = \Lang::get('core.student_success');
			}
			$authen->active 			= $request->input('active');
			$authen->first_name 		= $request->input('firstname');
			$authen->last_name 			= $request->input('lastname');
			$authen->email 				= trim($request->input('email'));
			$authen->username 			= trim($request->input('username'));

			if($request->input('password')!='' && $request->input('password_confirmation')!=''){
				$authen->password 			= \Hash::make($request->input('password'));
			}

			$authen->save();

			$lastinsertid 					= $authen->id;


			return Redirect::to('businessplan/students')->with('message',\SiteHelpers::alert('success',$message));
		} else {
			return Redirect::to('businessplan/studentform')->with('message',\SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
		}
	}	


	public function getStudentsum(Request $request, $Id=''){

		if(!\Auth::check()):
			return Redirect::to('user/login');
		endif;

		$data 		 	= array();
		$uid 			= \Session::get('uid');
		$getcourseinfo  = $this->model->getAllassignedcoursebystd($uid, $Id);

		$courselist 					= array();
		$courses 						= array();
		if(count($getcourseinfo)>0){
			$courselist['']   = 'Choose One';
			foreach ($getcourseinfo as $key => $cvalue) {
				$courselist[$cvalue->course_id] = $cvalue->course_title;
				$courses[] 						= $cvalue->course_id;
			}
		}

		$studentname 	= Users::find($Id);
		$data['sid'] 	= $Id;
		$data['sname']  = ucfirst($studentname['username']);
		$data['lists']  = $courselist;
		$data['courses'] = $courses;
		return view('businessplan.student_sum',$data);
	}

	public function getUsers(){
		$this->data = array(
			'pageTitle'	=> 	'Businessusers',
			'pageNote'	=>  'Businessusers can add thier users here',
			'pageModule'=> 'businessplan',
			'return'	=> self::returnUrl()
		);
		$this->data['access']		= $this->access;
		return view('businessplan.businessusers',$this->data);

	}

	public function getstudentcourselist(Request $request, $Id='')
	{
		if(!\Auth::check()):
			return Redirect::to('user/login');
		endif;

		$data 		 	= array();
		$uid 			= \Session::get('uid');
		$getcourseinfo  = $this->model->getAllassignedcoursebystd($uid, $Id);

		$courselist 					= array();
		$courses 						= array();
		if(count($getcourseinfo)>0){
			$courselist['']   = 'Choose One';
			foreach ($getcourseinfo as $key => $cvalue) {
				$courselist[$cvalue->course_id] = $cvalue->course_title;
				$courses[] 						= $cvalue->course_id;
			}
		}
		$studentname 	= Users::find($Id);
		$data['sid'] 	= $Id;
		$data['sname']  = ucfirst($studentname['username']);
		$data['lists']  = $courselist;
		$data['courses'] = $courses;
		return view('businessplan.studentcourselist',$data);



	}	


}