<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Clientbanner;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect, File ; 

class ClientbannerController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'clientbanner';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Clientbanner();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'clientbanner',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('clientbanner');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('clientbanner.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('client_banner'); 
		}

		$this->data['id'] = $id;
		return view('clientbanner.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('client_banner'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('clientbanner.view',$this->data);	
	}	

	function postSave( Request $request, $id=null)
	{
	
		if($id){
			$rules = array(
				'banner_title'=>'required',
			);
			$client_banner = Clientbanner::find($id);
			$uploadFile = false;
		}else{
			$rules = array(
				'banner_title'=>'required',
				'banner_image'=>'required|mimes:gif,png,jpg,jpeg',
			);
			$client_banner = new Clientbanner;
			$uploadFile = true;
		}

		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$image = Input::file('banner_image');

			if($id){
				$banner = Clientbanner::find($request->input('id'));
				$banner_path = 'uploads/banner/'.$banner->banner_image;
				if(!file_exists($banner_path)){
					$rules = $this->validateForm();
					$validator = Validator::make($request->all(), $rules);
					if($validator->fails()){
						return Redirect::to('clientbanner/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
						->withErrors($validator)->withInput();	
					}
				}
				if($image){
					if(file_exists($banner_path)){
						File::Delete($banner_path);
					}
					$uploadFile = true;
		    	}
			}
		   
		    if($uploadFile){
		    	$file_tmp_name = $image->getPathName();
                $file_ext =  $image->getClientOriginalExtension();
                $file_name = rand(4,9999).time();
                $file_name = $file_name.'.'.$file_ext;
                if(File::move($file_tmp_name, "uploads/banner/".$file_name))
				{
					chmod("uploads/banner/".$file_name, 0755);
					$client_banner->banner_image = $file_name;
				}
			}

			$client_banner->banner_title = $request->get('banner_title');
			$client_banner->banner_status = ($request->get('banner_status') == 1 ? '1' : '0');
			
			if($id){
				$client_banner->updated_at = time();
			}else{
				$client_banner->created_at = time();
				$client_banner->updated_at = time();
			}
			$client_banner->save();
		    $newID = $client_banner->id;

			if(!is_null($request->input('apply')))
			{
				$return = 'clientbanner/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'clientbanner?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			//echo '<pre>';print_r($return);exit;
			return Redirect::to('clientbanner')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {
			return Redirect::to('clientbanner/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			
			// banner image delete
			foreach ($request->input('id') as $banner_id) {
				$banner = Clientbanner::find($banner_id);
				$banner_path = 'uploads/banner/'.$banner->banner_image;
				if(file_exists($banner_path)){
					\File::Delete($banner_path);
				}
			}

			$this->model->destroy($request->input('id'));

			// redirect
			return Redirect::to('clientbanner')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('clientbanner')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	
	public function deleteClientbanner( Request $request ){
		$banner = Clientbanner::find($request->get('id'));
		if($banner != NULL){
			$banner_path = 'uploads/banner/'.$banner->banner_image;
			if(file_exists($banner_path)){
				\File::Delete($banner_path);
			}
		}
	}			


}
