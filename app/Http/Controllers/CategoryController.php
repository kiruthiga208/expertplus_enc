<?php
/**
 * Company : Bsetec
 * Controller : Category Controller
 * Email : support@bsetec.com
 */
namespace App\Http\Controllers;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Course;
use Validator, Input, Redirect ;
class CategoryController extends Controller {

    public function __construct()
    {
        $this->model = new Categories();
        $this->coursemodel = new Course();
    }


    public function getIndex($category='', $subcategory='')
    {
        $catcount = $this->model->getcategroeiscount($category);
        if ($catcount!= 1) {
            return Redirect::to('course');
        }
        if(isset($subcategory) && $subcategory!=''){
            $subcatcount = $this->model->getsubcategroeiscount($subcategory);
            if (count($subcatcount)==0) {
                return Redirect::to('course');
            }
        }
        $course                     = $this->model->getcategories($category,$subcategory);
        $banners                    = $this->coursemodel->getbanners();
        $this->data['bannercount']  = count($banners);
        $this->data['banners']      = $banners;
        $this->data['course']       = $course;
        if(!empty($subcategory))
            $this->data['search']       = $category.' / '.$subcategory;
        else
            $this->data['search']       = $category;
         return view('course.list',$this->data);
    }

}
