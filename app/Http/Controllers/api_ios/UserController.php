<?php 
namespace App\Http\Controllers\api_ios;
use Illuminate\Http\Request;
use Validator, Input, Redirect, Response ; 

use App\Models\Users;
use App\Models\Categories;
use App\Models\Api;
use App\Models\bsetec;
use App\Models\Coupon;
use App\Models\Admincoupon;
use Carbon\Carbon;
use App\User;
use File;
use App\Models\Course;
use App\Models\CourseImages;
use App\Http\Controllers\CommonmailController;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\CourseFiles;
class UserController extends Controller {

	
	public function __construct()
	{

		// $this->middleware('guest');
		$this->bsetec = new Bsetec();
		$this->users = new Users();
		$this->course = new Course();
		$this->categories = new Categories();
		$this->api = new Api();
		$this->sendmail = new CommonmailController();

	}

	public function postRegister(Request $request)
	{
		//validation rules
		$rules = array(
			'username'=>'required|alpha_num|unique:users|min:2',
			'first_name'=>'required|alpha|min:2',
			'last_name'=>'required|alpha|min:2',
			'email'=>'required|email|unique:users',
			'password'=>'required|alpha_num|between:6,12',
			'device_id'=>'required',
			);	
		$validator = Validator::make($request->all(), $rules);
		//if validation passed, save into database
		if ($validator->passes()) 
		{
			$code = rand(10000,10000000);
			$this->users->first_name = $request->input('first_name');
			$this->users->last_name = $request->input('last_name');
			$this->users->email = trim($request->input('email'));
			$this->users->username = trim($request->input('username'));
			$this->users->activation = $code;
			$this->users->group_id = 3;
			$this->users->active=1;
			$this->users->password = \Hash::make($request->input('password'));
			$this->users->device_id = $request->input('device_id');
			$this->users->save();

			$user = $this->users->find($this->users->id);
			$url = url('');
			$current_url = str_replace('api-ios','', $url);
			$activation_url = $current_url.'/user/activation';
			$data = array(
					'firstname'	=> $request->input('first_name') ,
					'lastname'	=> $request->input('last_name') ,
					'email'		=> $request->input('email') ,
					'password'	=> $request->input('password') ,
					'code'		=> $code,
					'url'		=> $activation_url.'?code='.$code,
					);
			$toMail = $request->input('email');
			$subject =CNF_APPNAME." REGISTRATION "; 			
			$message = view('api_ios.registration', $data);
			$fromMail = CNF_EMAIL;
			$tempname = 'api_ios.registration';
			// $this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);

			$result = array(
                'id'=>$user['id'],
                'username'=>$user['first_name'].' '.$user['last_name'],
                'email'=>$user['email'],
                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
            );
			return Response::json(array('status'=>true, 'result'=>$result), 200); 
		}
		else
		{
			return Response::json(array(
                'status' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200); 
		}
	}

	public function postLogin(Request $request)
	{
		
		//validation rules
		$rules = array(
			'email'=>'required|email',
			'password'=>'required|between:6,12',
			'device_type'=>'required',
			);	
		$validator = Validator::make($request->all(), $rules);
		//if validation passed, save into database
		if ($validator->passes()) 
		{
			if (\Auth::attempt(array('email'=>$request->input('email'), 'password'=>$request->input('password')))) 
			{
				if(\Auth::check())
				{
					$user = $this->users->find(\Auth::user()->id); 
					if($user->active =='1')
					{
						$email = Users::where('email',$request->input('email'))->first();
						$email->device_id = empty($request->input('device_id'))? $email->device_id : $request->input('device_id');
						$email->device_type = $request->input('device_type');
						$email->save();
						$result = array(
				            'id'=>$user['id'],
			                'username'=>$user['first_name'].' '.$user['last_name'],
			                'email'=>$user['email'],
			                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
			            	);
			            return Response::json(array('status'=>true, 'result'=>$result), 200);
					}
					else
					{
						return Response::json(array(
						    'status' => false,
						    'errors' => array('Your account is not active'),
						), 200); 
					}
				}
			}
			else
			{
				return Response::json(array(
				    'status' => false,
				    'errors' => array('Username/Password incorrect'),
				), 200); 
			}
		}
		else
		{
			return Response::json(array(
                'status' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200); 

		}
	}

	public function getCategories(Request $request)
	{
		
		$categories = $this->categories
					->select('id','name', 'slug')
					->where('status','enable')
					->get();
					foreach ($categories as $key => $value) {
						$category[]=array(
							'id'=>$value['id'],
							'name'=>trim($value['name']),
							'slug'=>trim($value['slug']),
							);
					}
					// exit();
		return Response::json(array('status'=>true, 'result'=>$category), 200);
	}

	public function getSubcategories(Request $request)
	{		
		$rules = array(
			'category_id'=>'numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$categoryid =$request->input('category_id');
			if(empty($categoryid)){
				return Response::json(array('status'=>false,'errors'=>array('category id is required')),200);
			}else{
				$course = $this->api->subcategories($categoryid);
				return Response::json($course, 200);
			}

		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}


	/*
	* @process -- course
	*/
		public function getCourse(Request $request){
			//validation rules
			$rules = array(
				'category_id'=>'numeric',
				'instruction_level'=>'alpha',
				'sort_by'=>'alpha',
				);
			$validator = Validator::make($request->all(),$rules);
			if($validator->passes()){
				$categoryid=$request->input('category_id');
				$price=$request->input('price');
				$ins_level=$request->input('instruction_level');
				$sort_by=$request->input('sort_by');
				$user_id =$request->input('user_id');
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1':$page_no;
				if(empty($categoryid) && empty($price) && empty($ins_level) && empty($sort_by)){
					return Response::json(array('status'=>false,'errors'=>array('Atleast Fill One Value')),200);
				}else{
					$course = $this->api->course($categoryid, $price,$ins_level,$sort_by,$user_id,$page);
				return Response::json($course, 200);
				}
				
			}else{
				return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
			}
		}

	/*
	*@process - Featured/Latest/Most viewed course
	*/
	public function getCourselist(Request $request){
		// if($request->input('user_id'))
		// 	$courselist = $this->api->courselist();
		// else
			$courselist = $this->api->courselist($request->input('user_id'));
		return Response::json(array('status'=>true,'result'=>$courselist), 200);
	}
	/*
	* @process --- coursedetails
	* @param --- course id and user id
	*/
	public function postCoursedetails(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(), $rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$details = $this->api->usercoursedetails($request->input('course_id'),$request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$details), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course Not available')),200);
			}
		}
		else{
			return Response::json(array(
                'status' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200);
		}
	}
	/*
	* @process --- course curriculum
	*/
	public function getCoursecurriculum( Request $request )
	{
		$rules = array(
			'course_id'=>'required|numeric'
			);
		$validator = Validator::make($request->all(), $rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$page_no = $request->input('page_no');
			    $page = empty($page_no)? '1':$page_no;
				$curriculum = $this->api->getcurriculum($request->input('course_id'),$page);
				return Response::json(array('status'=>true, 'result'=>$curriculum['curriculum']), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course Not available')),200);
			}
			
		}else{
			return Response::json(array(
                'status' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200); 
		}
	}

	public function getCoursereview(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1':$page_no;
				$review = $this->api->getcoursereview($request->input('course_id'),$page);				
				$review_count = $this->pagecount($review['count']);
				return Response::json(array('status'=>true, 'review'=>$review['review'],'total_page'=>$review_count), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course Not available')),200);
			}
		}
		else
		{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- course view discussion
	*/
	public function getViewdiscussion(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			); 
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes())
		{
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1': $page_no;
				$discussion = $this->api->getdiscussion($request->input('course_id'),$request->input('user_id'),$page);
				$lectures = $this->api->getlecturess($request->input('course_id'));
				$result_array = array('discussion_pagecount'=>$this->pagecount($discussion['count']),'discussion'=>$discussion['discussion'],'lectures'=>$lectures);
				return Response::json(array('status'=>true, 'result'=>$result_array), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}

		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	
	/*
	* @process -- about instructor
	* @param -- course_id
	*/
	public function postInstructor(Request $request)
	{
		$rules = array('course_id'=>'required|numeric');
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$instructor = $this->api->getInstructor($request->input('course_id'));
				return Response::json(array('status'=>true, 'result'=>$instructor), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course Not available')),200);
			}
		}
		else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- Add Discussion API
	*/
	public function postAdddiscussion(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'desc_cnt'=>'required',
			'lecture_id'=>'required|numeric',
			'desc_title'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->adddiscussion($request->input('course_id'),$request->input('user_id'),$request->input('desc_cnt'),$request->input('desc_title'),$request->input('lecture_id'));
				if($msg > 0)
					return Response::json(array('status'=>true, 'result'=>$msg), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Lecture ID not Available this course')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- Edit Discussion API
	*/
	public function postUpdatediscussion(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'desc_cnt'=>'required',
			'discussion_id'=>'required|numeric',
			'lecture_id'=>'required|numeric',
			'desc_title'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->editdiscussion($request->input('course_id'),$request->input('user_id'),$request->input('desc_cnt'),$request->input('desc_title'),$request->input('discussion_id'),$request->input('lecture_id'));
				if($msg==1)
					return Response::json(array('status'=>true, 'result'=>'Update Successfully' ), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Invalid Discussion ID')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process -- Delete Discussion API
	*/
	public function postDiscussiondelete(Request $request){
		
		$rules = array(
			'user_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->destorydiscussion($request->input('user_id'),$request->input('discussion_id'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Deleted Successfully' ), 200);
			else
				return Response::json(array('status'=>false,'errors'=>array('Invalid Discussion ID')),200);
				
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process -- Add wishlist
	*/
	public function postWishlist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'status'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->wishlist($request->input('user_id'),$request->input('course_id'),$request->input('status'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Added Successfully' ), 200);
			else if($msg == 0)
				return Response::json(array('status'=>false,'errors'=>array('removed Successfuly')),200);
			else
				return Response::json(array('status'=>true, 'result'=>'Already Added' ), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- course wishlist
	*/
	public function getCoursewishlist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$user = $this->api->userwishlist($request->input('user_id'),$page);			
			return Response::json(array('status'=>true,'total_count'=>$user['count'], 'result'=>$user['wishlist']), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process -- Add Reply
	*/
	public function postAddreply(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			'reply_cmt'=>'required',
			// 'lecture_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->addreply($request->input('course_id'),$request->input('user_id'),$request->input('reply_cmt'),$request->input('discussion_id'));
				if($msg>0)
					return Response::json(array('status'=>true, 'result'=>$msg), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Lecture ID not Available this course')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}

	/*
	* @process -- Edit Reply
	*/
	public function postUpdatereply(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'reply_cmt'=>'required',
			'reply_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$msg = $this->api->updatereply($request->input('user_id'),$request->input('reply_id'),$request->input('reply_cmt'));
				if($msg==1)
					return Response::json(array('status'=>true, 'result'=>'Successfully updated' ), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Invalid Reply ID')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process -- Delete Reply
	*/
	public function postReplydestroy(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'reply_id'=>'required|numeric',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$msg = $this->api->destoryreply($request->input('user_id'),$request->input('reply_id'));
			if($msg==1)
				return Response::json(array('status'=>true, 'result'=>'Deleted Successfully' ), 200);
			else
				return Response::json(array('status'=>false,'errors'=>array('Invalid Reply ID')),200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	*@process -- view all replies
	*/
	public function getViewreplies(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'discussion_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$avail = $this->api->isAvaildiscussion($request->input('course_id'),$request->input('discussion_id'));
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			if($avail>0){
				$replies = $this->api->viewreplies($request->input('discussion_id'),$page);
				$result = array('Replies'=>$replies['replies'],'replies_pagecount'=>$this->pagecount($replies['count']));
				return Response::json(array('status'=>true, 'result'=>$result), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('Invalid Discussion and Course ID')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	* @process -- mycourse list ( Learning)
	*/
	public function getMycourselist(Request $request){
		$rules = array(
			'user_id'=>'required|numeric',
			'type'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$check = $this->api->Ischeckuser($request->input('user_id'));
			if($check>0){
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1':$page_no;
				$learning = $this->api->learning($request->input('user_id'),$request->input('type'),$page);
				return Response::json(array('status'=>true,'total_course_count'=>$learning['count'] ,'result'=>$learning['coursetaken']), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Invalid User ID')),200);	
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	* @process --- announcement
	*/
	public function getAnnouncement(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$announcement = $this->api->getannouncement($request->input('course_id'),$request->input('user_id'),$page);
				$result = array('Announcement_pagecount'=>$this->pagecount($announcement['count']),'Annoucements'=>$announcement['products']);
				return Response::json(array('status'=>true, 'result'=>$result), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course Not available')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}
	/*
	* @process -- forgetrequest
	*/
	public function postForgetrequest(Request $request)
	{
		$rules = array(
			'email_id'=>'required|email'
			);	
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
			$user =  User::where('email','=',$request->input('email_id'));
			if($user->count() >=1)
			{
				$url = url('');
				$current_url = str_replace('api-ios','', $url);
				$reset_url = $current_url.'/user/resetmob/';
				$user = $user->get();
				$user = $user[0];
				$token = uniqid();
				$token1= $reset_url.$token;
				$data = array('token'=>$token1);	
				$toMail = $request->input('email_id');
				$subject = CNF_APPNAME ." REQUEST PASSWORD RESET "; 			
				$message = view('api_ios.reminder', $data);
				$fromMail = CNF_EMAIL;
				$tempname = 'api_ios.reminder';
				$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
				
				$affectedRows = User::where('email', '=',$user->email)
				->update(array('reminder' => $token));
				return Response::json(array('status'=>true, 'result'=>'Reset Link send your email' ), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Cant find email address')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	* @process --- user course curriculum for subscribed user
	*/
	public function getUsercourse(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$cid = $request->input('course_id');
				$uid = $request->input('user_id');
				$url = url('');
      			$current_url = str_replace('api-ios','', $url);
				$curriculum = $this->api->getusercurriculum($request->input('course_id'),$request->input('user_id'),$page);
				$curriculum_count= $curriculum['count'];
				$cum_cnt = $this->pagecount($curriculum_count);
				$percentage = $this->api->coursepercentage($request->input('course_id'),$request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$curriculum['curriculum'],'curriculum_page_count'=>$cum_cnt,'course_percentage'=>$percentage,'course_completed'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id'))) ? '1':'0','certificate_url'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id')))? $current_url.'/course/certificate/'.$cid.'/'.$uid : ''), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	*@process --- Check Facebook Login
	*/
	public function postSignin(Request $request){
		
		$rules = array(
			'email_id'=>'required',
			'social_type'=>'required',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$email = Users::where('email',$request->input('email_id'))->first();
			if(!empty($email))
			{
				if($email->active =='1')
				{
				$email->device_id = empty($request->input('device_id'))? $email->device_id : $request->input('device_id');
				$email->device_type = empty($request->input('device_type'))? $email->device_type : $request->input('device_type') ;
				$email->save();
				$count = $this->api->Ischeckfbuser($request->input('email_id'),$request->input('social_type'));
				if(count($count)>0)
					return Response::json(array('status'=>true, 'result'=>$count), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('email id is not register with '.$request->input('social_type').' / Device ID ' )) ,200);
				}else{
				return Response::json(array(
						    'status' => false,
						    'errors' => array('Your account is not active'),
						), 200); 
				}
			}else
			{
				return Response::json(array('status'=>false, 'errors' => array('Your email is not valid')),200);
			}

		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	public function getProfileinfo(Request $request)
	{
		$rules=array(
				'user_id'=>'required|numeric',
				);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$Info = $this->api->getProfileInfo($request->input('user_id'));
				return Response::json(array('status'=>true, 'result'=>$Info), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	*@process -- update profile information
	*/
	public function postUpdateinfo(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$Info = $this->api->updateProfileInfo($request->input('user_id'),$request->input('first_name'),$request->input('last_name'),$request->input('user_name'),$request->input('announcement'),$request->input('spl_promotion'));
				if($Info==1)
					return Response::json(array('status'=>true, 'result'=>'Success'), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}

	/*
	* @process -- Change Password
	*/
	public function postChangepassword(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'current_password'=>'required',
			'password'=>'required|confirmed|between:6,12|different:current_password',
			'password_confirmation'=>'required|between:6,12|required_with:password',
			);
		
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$user = User::find($request->input('user_id'));
				if(!\Hash::check($request->input('current_password'), $user->password)){
					return Response::json(array('status'=>false,'errors'=>array('Enter valid Current Password')),200);	
				}else{
					$new_password = \Hash::make($request->get('password'));
					$user->password =$new_password;
					$user->save();
					return Response::json(array('status'=>true, 'result'=>'Password Updated' ), 200);
				}
					
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- Account Delete
	*/
	public function postAccountdestory(Request $request){
		$rules=array(
			'user_id'=>'required|numeric'
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$blog_comments 			= $this->api->userData('blogcomments','user_id',$request->input('user_id'));
				$forum_comments 		= $this->api->userData('forum_comments','user_id',$request->input('user_id'));
				$favorite 				= $this->api->userData('favorite','user_id',$request->input('user_id'));
				$quiz_results 			= $this->api->userData('curriculum_quiz_results','user_id',$request->input('user_id'));
				$featured 				= $this->api->userData('featured','user_id',$request->input('user_id'));
				$instructor 			= $this->api->userData('instructor','user_id',$request->input('user_id'));
				$invitation_users 		= $this->api->userData('invitation_users','user_id',$request->input('user_id'));
				$lectures_comments 		= $this->api->userData('lectures_comments','user_id',$request->input('user_id'));
				$lectures_comment_reply = $this->api->userData('lectures_comment_reply','user_id',$request->input('user_id'));
				$logs 					= $this->api->userData('logs','user_id',$request->input('user_id'));
				$notifications 			= $this->api->userData('notifications','user_id',$request->input('user_id'));
				$report_abuse 			= $this->api->userData('report_abuse','user_id',$request->input('user_id'));
				$subscriber_list 		= $this->api->userData('subscriber_list','user_id',$request->input('user_id'));
				$withdraw_requests 		= $this->api->userData('withdraw_requests','user_id',$request->input('user_id'));
				$course_progress 		= $this->api->userData('course_progress','user_id',$request->input('user_id'));
				$courses = Course::where('user_id',$request->input('user_id'))->get();

				if(count($courses)>0){
					foreach ($courses as $course) {
						$this->course->courseunpublish($course->course_id);
					}
				}
				User::find($request->input('user_id'))->delete();
				return Response::json(array('status'=>true, 'result'=>'Account Deleted Successfully' ), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);	
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- lecture details status
	*/
	public function postLecturedetails(Request $request){
		$rules=array(
				'user_id'=>'required|numeric',
				'lecture_id'=>'required|numeric',
				'course_id'=>'required|numeric',
				'status'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			if($count>0){
				$status = $this->api->lecturedetails($request->input('user_id'),$request->input('course_id'),$request->input('lecture_id'),$request->input('status'));
				if($status==1)
					return Response::json(array('status'=>true, 'result'=>'Lecture Status updated Successfully' ), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);	
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	*@process --- search course
	*/
	public function getSearchcourse(Request $request){
			$rules=array(
				'course_search'=>'required'
				);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$search = $this->api->searchcourse($request->input('user_id'),$request->input('course_search'),$page);
			return Response::json($search, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}

	/*
	*@process --- searchdiscussion
	*/
	public function postSearchdiscussion(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'dis_search'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$search = $this->api->searchdiscussion($request->input('user_id'),$request->input('course_id'),$request->input('dis_search'));
				return Response::json(array('status'=>true, 'result'=>$search), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- updateprofile
	*/
	public function postUpdateprofile(Request $request){

		$rules=array(
			'user_id'=>'required|numeric',
			'cimage'=>'required'
			);
		// echo $request->input('cimage');exit();
		$image = Input::file('cimage');
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$what = getimagesize($image);
			
            if($what[0] < 500 || $what[1] < 500){
                $response = array(
                    'status'=>false,
                    'message'=>'Uploaded image size is '.$what[0].'px * '.$what[1].'px. The Minimum allowed size is 500px * 500px.'
                );
                return Response::json(array('status'=>false,'errors'=>$response),200);
             }else
             {
             	switch(strtolower($what['mime']))
		        {
		            case 'image/png':
		                $img_r = imagecreatefrompng($image);
		                $source_image = imagecreatefrompng($image);
		                $type = '.png';
		                break;
		            case 'image/jpeg':
		                $img_r = imagecreatefromjpeg($image);
		                $source_image = imagecreatefromjpeg($image);
		                $type = '.jpg';
		                break;
		            case 'image/gif':
		                $img_r = imagecreatefromgif($image);
		                $source_image = imagecreatefromgif($image);
		                $type = '.gif';
		                break;
		            default: die('image type not supported');
		         }
		         // $base = base_path();
		         // $path = str_replace('api-ios','', $base);
		         // $destination = $path.'/uploads/users/';
		         $destination = './uploads/users/';
		         $filename = $image->getClientOriginalName();

             	 //echo $filename;
         	 	$courseImages = new CourseImages;
	            $courseImages->image_title = $filename;
	            $courseImages->image_type = substr($type, 1);
	            $courseImages->image_tag = "dummy tag";
	            $courseImages->uploader_id = $request->input('user_id');
	            $courseImages->created_at = time();
	            $courseImages->updated_at = time();
	            $courseImages->save();
	            //update image hash
             	 	$courseUpdate = CourseImages::find($courseImages->id);
            		$courseUpdate->image_hash = md5($courseImages->id.$courseUpdate->created_at);
            		$courseUpdate->save();
             	 $image_n = $courseImages->id;
             	 //$filename = $request->input('user_id').$type;
    			 //$extension =$image->getClientOriginalExtension();
    				$image_size[] = array('imgW'=>'500','imgH'=>'500','value'=>'normal');
		            $image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
		            $image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

                $path = $destination.$image_n.$type;
                
                foreach($image_size as $size){
                    $save_path = ($size['value'] =='normal') ? $image_n.$type : $image_n.'_'.$size['value'].$type; 
                    Image::make($image->getRealPath())->resize($size['imgW'],$size['imgH'])->save($destination.$save_path);
                }
                
    			 
             	 $image_id = $courseImages->find($courseImages->id);
             	 
             	 $data = array();
             	 $data['avatar']=$image_id->id;
             	 $data['updated_at']=time();
             	 Users::where('id',$request->input('user_id'))->update($data);
             	 $users = $this->users->find($request->input('user_id'));
             	 
             	 $image_url=array('profile_image_url'=>$this->api->customavatar($users['email'],$users['id']));
             	 return Response::json(array('status'=>true, 'result'=>$image_url), 200);
		            
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- Add Rating
	*/
	public function postAddrating(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'rating'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$search = $this->api->Addrating($request->input('user_id'),$request->input('course_id'),$request->input('review_text'),$request->input('rating'));
				return Response::json(array('status'=>true, 'result'=>'Rating Successfully Added' ), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	
	/*
	*@process -- get Rating and review
	*/
	public function postRating(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$rating = $this->api->getrating($request->input('user_id'),$request->input('course_id'));
			return Response::json(array('status'=>true, 'result'=>$rating), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- Report Abuse
	*/
	public function postReportabuse(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'issue_type'=>'required',
			'details'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$this->api->reportabuse($request->input('user_id'),$request->input('course_id'),$request->input('issue_type'),$request->input('details'));
				return Response::json(array('status'=>true, 'result'=>'Report Successfully Added' ), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- Studentsenrolled
	*/
	public function getEnrolled(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			$enrolled = $this->api->Studentsenrolled($request->input('course_id'),$page);
			$result =array('Students_pagecount'=>$this->pagecount($enrolled['count']),'Studentsenrolled'=>$enrolled['students']);
			return Response::json(array('status'=>true, 'result'=>$result), 200);
			
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- signupwithfb
	*/
	public function postSignup(Request $request){
		$rules = array(
				'username'=>'required|alpha_num|unique:users|min:2',
				'firstname'=>'required|alpha|min:2',
				'lastname'=>'required|alpha|min:2',
				'email'=>'required|email|unique:users',
				'password'=>'required|alpha_num|between:6,12',
				'social_id'=>'required',
				'social_type'=>'required',
				'device_id'=>'required',
				);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$code = rand(10000,10000000);
			$authen = new User;
			$authen->first_name = $request->input('firstname');
			$authen->last_name = $request->input('lastname');
			$authen->email = trim($request->input('email'));
			$authen->username = trim($request->input('username'));
			$authen->social_id = $request->input('social_id');
			$authen->social_type = $request->input('social_type');
			$authen->activation = $code;
			$authen->group_id = 3;
			$authen->password = \Hash::make($request->input('password'));
			$authen->device_id = $request->input('device_id');
			$authen->active = 1;
			$authen->save();

			$user = $authen->find($authen->id);
			$url = url('');
				$current_url = str_replace('api-ios','', $url);
				$activation_url = $current_url.'/user/activation';
				$data = array(
						'firstname'	=> $request->input('firstname') ,
						'lastname'	=> $request->input('lastname') ,
						'email'		=> $request->input('email') ,
						'password'	=> $request->input('password') ,
						'code'		=> $code,
						'url'		=> $activation_url.'?code='.$code,
						);
				$toMail = $request->input('email');
				$subject =CNF_APPNAME." REGISTRATION "; 			
				$message = view('api_ios.registration', $data);
				$fromMail = CNF_EMAIL;
				$tempname = 'api_ios.registration';
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);

			$result = array(
                'id'=>$user['id'],
                'username'=>$user['first_name'].' '.$user['last_name'],
                'email'=>$user['email'],
                'user_image'=>$this->api->customavatar($user['email'],$user['id']),
            );
			return Response::json(array('status'=>true, 'result'=>$result), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}

	/*
	*@process --- Quiz Questions
	*/
	public function postQuizquestions(Request $request){
		$rules=array(
			// 'course_id'=>'required|numeric',
			// 'user_id'=>'required|numeric',
			// 'lecture_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$quiz = $this->api->getquiz($request->input('user_id'),$request->input('course_id'),$request->input('lecture_id'));

				return Response::json(array('status'=>true, 'result'=>$quiz), 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	public function postChangeemail(Request $request){
		$rules=array(
			'email'=>'required|email|unique:users',
			'user_id'=>'required|numeric',
			'password'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->Ischeckuser($request->input('user_id'));
			$users = $this->users->find($request->input('user_id'));
			if($count>0){
				if(!\Hash::check($request->input('password'), $users->password)){
					return Response::json(array('status'=>false,'errors'=>array('Enter valid Current Password')),200);	
				}else{
					$this->api->changeemail($request->input('user_id'),$request->input('email'));
					return Response::json(array('status'=>true, 'result'=>'Successfully Updated Email ID' ), 200);
				}
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid User id')),200);	
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- Quiz questio result 
	*/
	public function postQuizresult(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'question_id'=>'required',
			'answer_id'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$question_id = $request->input('question_id');
			$answer_id = $request->input('answer_id');
			$cid =  $request->input('course_id');
			$uid = $request->input('user_id');
			 $result = $this->api->quizresult($uid,$cid,$question_id,$answer_id);
			 return Response::json(array('status'=>true, 'result'=>$result), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- Delete Review
	*/
	public function postDelreview(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$this->api->reviewdestory($request->input('user_id'),$request->input('course_id'));
			 return Response::json(array('status'=>true, 'result'=>'Review Successfully Deleted' ), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- course list page trending and 
	*/
	public function getTrending(Request $request){
		$trending = $this->api->trending($request->input('user_id'));
		return Response::json(array('status'=>true, 'result'=>$trending), 200);
	}
	/*
	*@process -- view all
	*/
	public function getViewall(Request $request){
		$rules=array(
			'type'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1':$page_no;
			$count = $this->api->viewall_count($request->input('type'));
			$view_all = $this->api->viewall($request->input('type'),$request->input('user_id'),$page);
			return Response::json(array('status'=>true, 'total_count'=>$count,'result'=>$view_all), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);	
		}
	}

	/*
	*@process --- Buy Course
	*/
	public function postBuycourse(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count==0){
				$course = $this->course->find($request->input('course_id'));
				if($course->pricing==0){
					$this->api->buycourse($request->input('course_id'),$request->input('user_id'));
					return Response::json(array('status'=>true, 'result'=>'Successfully Subscribed' ), 200);
				}else{
					return Response::json(array('status'=>false,'errors'=>array('This is Paid Course')),200);
				}
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('Already subscribed')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process --- Twitter email id
	*/
	public function postUsermail(Request $request){
		$rules=array(
			'id'=>'required',
			'social_type'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$email = $this->api->checkuseremail($request->input('id'),$request->input('social_type'));
		    if(count($email)>0)
		    	return Response::json(array('status'=>true, 'email'=>$email->email), 200);	
		    else
		    	return Response::json(array('status'=>false,'errors'=>array('Invalid id and type')),200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- currency code
	*/
	public function getCurrency(){
		//$basepath 	= dirname(__FILE__);
		// $base = str_replace('api_ios', '',  base_path());
		// require_once $base.'setting.php';
		$currency =  CNF_CURRENCY; 
		 $code =  CNF_FAV; 
		// echo $currency;exit;
		return Response::json(array('status'=>true, 'currency_code'=>$currency,'color_code'=>$code), 200);
	}
	
	/*
	*@process --- Payment -- Paypal, Paypal Checkout, Stripe
	*/
	public function getPaymentsucess(Request $request){
		echo 'success';
	}

	//Push Notification
	public function getNotification($device_id='',$type=''){

		if($type=='android'){
			define( 'API_ACCESS_KEY', 'AIzaSyCGjSZSpXdAyau_fk_ZCueT1IHJ0jwxZ1M' );
			$registrationIds = array($device_id);
			// prep the bundle
			$msg = array
			(
				'message' 	=> 'here is a message. message',
				'title'		=> 'This is a title. title',
				'subtitle'	=> 'This is a subtitle. subtitle',
				'tickerText'	=> 'Ticker text here...Ticker text here...Ticker text here',
				'vibrate'	=> 1,
				'sound'		=> 1,
				'largeIcon'	=> 'large_icon',
				'smallIcon'	=> 'small_icon'
			);
			$fields = array
			(
				'registration_ids' 	=> $registrationIds,
				'data'			=> $msg
			);
			 
			$headers = array
			(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);
			 
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
			$result = curl_exec($ch );
			curl_close( $ch );
			echo $result;
		}
	}

	public function postFeatures(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'status' => 'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){				
		$updatefeatures = $this->api->getupdatefeatures($request->all()); 		
			return Response::json(array('status'=>true, 'result'=>$updatefeatures), 200);	
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- Course guest user
	*/
	public function getNotsubscriber(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'device_id'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$course_cnt =  $this->api->isAvailcourse($request->input('course_id'));
			if($course_cnt>0){
				$details = $this->api->usercoursedetails1($request->input('course_id'),$request->input('user_id'));		
				$curriculum = $this->api->getcurriculum($request->input('course_id'));
				$c_page_cnt = $this->pagecount($curriculum['count']);
				$instructor = $this->api->getInstructor($request->input('course_id'));
				$review = $this->api->getcoursereview($request->input('course_id'));				
				$r_page_cnt = $this->pagecount($review['count']);
				$recent = $this->api->getviewedcoures($request->input('device_id'),$request->input('user_id'),$request->input('course_id'));
				return Response::json(array('status'=>true, 'course_details'=>$details,'curriculum_page_count'=>$c_page_cnt,'curriculum'=>$curriculum['curriculum'],'about_instructor'=>$instructor,'review_page_count'=>$r_page_cnt,'course_review'=>$review['review'],'recently_view_course'=>$recent), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Enter Valid Course id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	// page count
	public function pagecount($count){
		$remain =  $count % 10;
		if($remain!=0){
			$co_count = $count / 10;
			if($remain >= 5)
			{
				$page_count = round($co_count);
			}else{
				$page_count = round($co_count+1);
			}
			
		}else{
			$co_count = $count / 10;
			$page_count = round($co_count);
		}
		return $page_count;
	}
	
	/*
	*@process --- set Recently course
	*/
	public function postRecently(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'device_id'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			 $recent = $this->api->checkrecently($request->input('course_id'),$request->input('device_id'));
				 if($recent==0){
				 	$this->api->updaterecntview($request->input('course_id'),$request->input('device_id'));
				 	return Response::json(array('status'=>true, 'result'=>'Successfully Added'), 200);
				 }else{
				 	return Response::json(array('status'=>false,'errors'=>array('Already viewed')),200);
				 }
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- Save notes
	*/
	public function postNotes(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',			
			'lecture_id'=>'required|numeric',
			'notes'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$cid = $request->input('course_id');
				$uid = $request->input('user_id');				
				$lid = $request->input('lecture_id');
				$notes = $request->input('notes');
				$id = $this->api->insertlecturenotes($uid,$lid,$notes);
				if($id)
					return Response::json(array('status'=>true, 'result'=>'Successfully Added','note_id'=>$id), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Not updated')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course not subscribe')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*@process -- Download lecture notes
	*/
	public function getDownloadnotes(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'lecture_id'=>'required|numeric',
			'page_no' => 'numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			$current_url = str_replace('api-ios','', url('') );
			$url = $current_url.'/download-notesmob/'.$request->input('lecture_id').'/'.$request->input('user_id');
			$count = $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($count>0){
				$notes = $this->api->getnotes($request->input('user_id'),$request->input('lecture_id'),$page);
				// print_r($notes);exit;
				return Response::json(array('status'=>true,'notes_pagecount'=>$this->pagecount($notes['count']),'note_hint'=>$notes['notes'],'downloadable_notes'=>$url), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Course not subscribe')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	// remove notes
	public function postRemovenotes(Request $request){
		$rules=array(
			'note_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$res = $this->api->removenotes($request->input('user_id'),$request->input('note_id'));
			if($res ==1)
				return Response::json(array('status'=>true, 'result'=>'Note Successfully Deleted'), 200);
			else
				return Response::json(array('status'=>false,'errors'=>array('Invalid note and user id')),200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	// Add Annoucements
	public function postAnnouncements(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'announcement'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$status = $this->api->checkInstructor($request->input('user_id'),$request->input('course_id'));
			if($status==1){
				$ann = $this->api->announcement($request->input('user_id'),$request->input('course_id'),$request->input('announcement'));
				return Response::json(array('status'=>true, 'result'=>$ann), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Invalid Instructor user and course id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	// Update Annoucements
	public function postUpdateannouncement(Request $request){
		$rules=array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'announcement'=>'required',
			'announcement_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$status = $this->api->checkInstructor($request->input('user_id'),$request->input('course_id'));
			if($status==1){
				$id = $this->api->updateannouncement($request->input('user_id'),$request->input('course_id'),$request->input('announcement'),$request->input('announcement_id'));
				if($id==1)
					return Response::json(array('status'=>true, 'result'=>'Update Successfully'), 200);
				else
					return Response::json(array('status'=>false,'errors'=>array('Invalid Announcement id')),200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('Invalid Instructor user and course id')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	// remove annoucement
	public function postRemoveannouncement(Request $request){
		$rules=array(
			'user_id'=>'required|numeric',
			'announcement_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$id = \DB::table('course_announcement')->where('user_id',$request->input('user_id'))->where('announcement_id',$request->input('announcement_id'))->delete();		
			if($id)
				return Response::json(array('status'=>true, 'result'=>'Deleted Successfully'), 200);
			else
				return Response::json(array('status'=>false,'errors'=>array('Invalid user and announcement id')),200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	//subscriber
	public function getSubscriber(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);		
		if($validator->passes()){			
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			$url = url('');
			$current_url = str_replace('api-ios','', $url);
			if($course_cnt>0){
				$cid = $request->input('course_id');
				$uid = $request->input('user_id');	
				$author = $this->api->getInstructor($cid);			
				$curriculum = $this->api->getusercurriculum($cid,$uid);								
				$cum_cnt = $this->pagecount($curriculum['count']);
				$students = $this->api->Studentsenrolled($cid);				
				$stu_cnt = $this->pagecount($students['count']);
				$discussion = $this->api->getdiscussion($cid,$uid);				
				$dis_cnt = $this->pagecount($discussion['count']);
				$announcement = $this->api->getannouncement($cid,$uid);
				$rating = $this->api->getrating($uid,$cid);
				$featured = $this->api->getfeaturedcourse($cid,$uid);				
				$is_featured = ($featured == 0) ? 0 : 1; 
				$percentage = $this->api->coursepercentage($cid,$uid);
				$lectures = $this->api->getlecturess($cid);
				$completed_lecture = $this->api->getCourseCompletedCount($cid,$uid);				
		return Response::json(array('status'=>true,'author'=>$author,'curriculum_page_count'=>$cum_cnt,'curriculum'=>$curriculum['curriculum'],'student_page_count'=>$stu_cnt,'students'=>$students['students'],'discussion_page_count'=>$dis_cnt,'discussion'=>$discussion['discussion'],'announcement'=>$announcement['products'],'rating'=>$rating,'is_featured'=>$is_featured,'course_percentage'=>$percentage,'total_lecture_count'=>count($lectures),'completed_lecture_count'=>$completed_lecture,'course_completed'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id'))) ? '1':'0','certificate_url'=>($this->api->checkCompletion($request->input('course_id'),$request->input('user_id')))? $current_url.'/course/certificate/'.$cid.'/'.$uid : ''), 200);				
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}
		else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	/*
	* @process --- user course lecture for subscribed user
	*/
	public function getViewlecture(Request $request){
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'lecture_id'=> 'required|numeric',
			);
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$page_no = $request->input('page_no');
			$page = empty($page_no)? '1': $page_no;
			$course_cnt =  $this->api->isSubscribecourse($request->input('course_id'),$request->input('user_id'));
			if($course_cnt>0){
				$cid = $request->input('course_id');
				$uid = $request->input('user_id');
				$lid = $request->input('lecture_id');
				$url = url('');
      			$current_url = str_replace('api-ios','', $url);
				$curriculum = $this->api->getuserlecture($cid,$uid,$lid);			
				return Response::json(array('status'=>true, 'result'=>$curriculum), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	* @process --- View discussion by lecture
	*/
	public function getLecturediscussion(Request $request)
	{
		$rules = array(
			'course_id'=>'required|numeric',
			'user_id'=>'required|numeric',
			'lecture_id'=>'required|numeric',
			); 
		$validator = Validator::make($request->all(),$rules);
		if($validator->passes())
		{
			$cid = $request->input('course_id');
			$uid = $request->input('user_id');
			$lid = $request->input('lecture_id');
			$course_cnt =  $this->api->isSubscribecourse($cid,$uid);
			if($course_cnt>0){
				$page_no = $request->input('page_no');
				$page = empty($page_no)? '1': $page_no;
				$discussion = $this->api->getdiscussionby_lid($cid,$uid,$lid,$page);
				$lectures = $this->api->getlecturesby_id($cid,$lid);
				$result_array = array('discussion_pagecount'=>$this->pagecount($discussion['count']),'discussion'=>$discussion['discussion'],'lectures'=>$lectures);
				return Response::json(array('status'=>true, 'result'=>$result_array), 200);
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}

		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	//coursecertificate
	public function getCoursecertificate(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);		
		if($validator->passes()){	
			$uid = $request->input('user_id');
			$cid = $request->input('course_id');	
			$course_cnt =  $this->api->isSubscribecourse($cid,$uid);
			$url = url('');
			$current_url = str_replace('api-ios','', $url);
			if($course_cnt>0){
				$percentage = $this->api->coursepercentage($cid,$uid);
				$certificate = $this->api->checkCompletion($cid,$uid);
				if($certificate)
				{
					$result = array('status'=>true,'coursepercentage'=>$percentage, 'certificate_url'=>$current_url.'/course/certificate/'.$cid.'/'.$uid);
				}else{
					$result = array('status'=>'error','coursepercentage'=>$percentage, 'certificate_url'=>'' );
				}
				return Response::json($result, 200);
			}
			else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	public function getUnreadlecture(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			);
		$validator= Validator::make($request->all(),$rules);		
		if($validator->passes()){
			$uid = $request->input('user_id');
			$cid = $request->input('course_id');
			$course_cnt =  $this->api->isSubscribecourse($cid,$uid);
			if($course_cnt>0){
				$lecture_details = array();
				$lectures = $this->api->getlecturess($cid);
				foreach ($lectures as $value) {
				  $status = $this->api->Coursestatus($value['lecture_id'],$uid);				  
                  if($status !== "Restart")
                  {
                  	$lecture_details[] = $this->api->getunreadlecture($cid,$uid,$value['lecture_id']);	
                  }								
				}
				return Response::json(array('status'=>true, 'result'=>$lecture_details), 200);	
			}else{
				return Response::json(array('status'=>false,'errors'=>array('User not subscribe this course')),200);
			}			
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}	
		
	}

	public function getGetcoupon(Request $request)
	{
		
		$rules=array(
			// 'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'coupon_code'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);		
		if($validator->passes()){
			$coupon = Coupon::where('coupon_code',$request->get('coupon_code'))
                        ->where('course_id',$request->get('course_id'))
                        ->where('coupon_status',1)
                        ->first();   
                        // echo '<pre>';print_r($coupon);exit;                   
            if($coupon == NULL){
                    return Response::json(array(
                    	'status'=>false,
                        'errors' => array('Sorry, the coupon code '.$request->get('coupon_code').' is invalid.')
                    ), 200);
            }else{
                if($coupon->coupon_start_date !='0000-00-00'){
                  $coupon = Coupon::where('coupon_code',$request->get('coupon_code'))
                            ->where('course_id',$request->get('course_id'))
                            ->where('coupon_start_date','<=',Carbon::now()->toDateString())
                            ->where('coupon_end_date','>=',Carbon::now()->toDateString())
                            ->where('coupon_status',1)
                            ->first();
                    if($coupon == NULL){  
                        return Response::json(array(
                        		'status'=>false,
                                'errors' => array('Sorry, the coupon code '.$request->get('coupon_code').' is date invalid.')
                        ), 200);
                   }
                }

                $course = Course::find($request->get('course_id'));
                $course_price = $course->pricing;

               

                $discount_amount = $coupon->coupon_value;

                if($coupon->coupon_type == 1){
                    $discount_amount =  $course_price * ($discount_amount / 100);
                    $amount = $course_price - $discount_amount;
                }else{
                    $amount = $course_price - $discount_amount;
                }

                if($amount < 0)
                       $amount = 0;
                
                $return_msg = array(
                        'discount_amount'=>$discount_amount
                    );
				}

              // return response()->json($return_msg,200);		
              return Response::json(array('status'=>true, 'result'=>$return_msg), 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}	
		
	}

	public function postSavepayment(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			'course_id'=>'required|numeric',
			'payment_method'=>'required',
			'amount'=>'required|numeric',
			'status'=>'required',
			'tr_details'=>'required',
			);
		$validator= Validator::make($request->all(),$rules);		
		if($validator->passes()){
		  $url = url('');
		  $current_url = str_replace('api-ios','', $url);
		  $count = $this->api->isSubscribecourse($request->get('course_id'),$request->get('user_id'));
		  if($count==0){
		  		$post=array(
				'user_id'=>$request->get('user_id'),
				'course_id'=>$request->get('course_id'),
				'payment_method'=>$request->get('payment_method'),
				'amount'=>$request->get('amount'),
				'status'=>$request->get('status'),
				'order_details'=> ($request->get('payment_method')=='stripe') ? $request->get('tr_details') : json_encode(array('token'=>$request->get('tr_details'))),
				);
			  $postvars = '';
			  foreach($post as $key=>$value) {
			  	$postvars .= $key . "=" . $value . "&";
			  }
			  // echo $current_url.'payment/paymentmob?'.$postvars;exit;
			  $ch = curl_init();
			  curl_setopt($ch, CURLOPT_URL, $current_url.'/payment/paymentmob?'.$postvars);
	          //return the transfer as a string
	          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	          //curl_setopt($ch, CURLOPT_POSTFIELDS,$postvars);

	          // $output contains the output string
	          $transaction_id = curl_exec($ch);
	          // close curl resource to free up system resources
	          curl_close($ch); 	
	          if($transaction_id!='false')
	          {
	          	// echo $transaction_id;exit;
	          	return Response::json(array('status'=>true, 'result'=>array('message'=>'Congrats! You have enrolled in this course!','transaction_id'=>$transaction_id)), 200);	
	          }
	          else
	          {
	          	return Response::json(array(
	                        		'status'=>false,
	                                'errors' => array('Sorry the payment failed')
	                        ), 200);
	          }
		  }else{
		  	return Response::json(array(
	                        		'status'=>false,
	                                'errors' => array('Sorry You already subscribed this course ')
	                        ), 200);
		  }
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}	
		
	}

	public function postPurchaserestore(Request $request)
	{
		$rules=array(
			'user_id'=>'required|numeric',
			'product_id'=>'required',
			'tr_details'=>'required'
			);
		$validator= Validator::make($request->all(),$rules);
		
		$user_id = $request->input('user_id');
		$product_id = $request->input('product_id');
		$tr_details = $request->input('tr_details');
		
		if($validator->passes())
		{
			$result = $this->api->purchase_restore($user_id, $product_id, $tr_details);

			if($result['status'])
			{
				return Response::json(array('status'=>true,'result'=>array('message'=>$result['message'])),200);
			}
			else
			{
				return Response::json(array('status'=>false,'errors'=>array($result['message'])),200);
			}
		}
		else
		{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}


	}


	/*
	*process -- get questions list
	*/
	public function getQuestions(Request $request){
		$userid   = $request->input('user_id');
		$search   = $request->input('search_text');
		$page_no  = $request->input('page_no');
		if($page_no==''){
			$page_no = 0;
		}
		$trending = $this->api->questions($userid, $search, $page_no);
		return Response::json(array('status'=>true, 'result'=>$trending), 200);
	}

	/*
	*process -- get questions view
	*/
	public function getQuestionview(Request $request){

		$rules = array(
			'question_id' 		=> 	'required|numeric',
			'type' 				=> 	'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$question_id = $request->input('question_id');
			$type        = $request->input('type');
			$qtnview = $this->api->questionview($question_id, $type);
			return Response::json($qtnview, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- get Answer Lists
	*/
	public function getAnswerslist(Request $request){

		$rules = array(
			'question_id' => 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$question_id = $request->input('question_id');
			$search_text = $request->input('search_text');
			$page_no 	 = $request->input('page_no');
			if(empty($question_id)){
				return Response::json(array('status'=>false,'errors'=>array('question id is required') ),200);
			}else{
				$answers = $this->api->answerslist($question_id, $search_text, $page_no);
				return Response::json($answers, 200);
			}

		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- post create questions
	*/
	public function postUpsertquestion(Request $request){

		$rules = array(
			'user_id' 			=> 'required|numeric',
			'question_title'    => 'required|min:5|max:140',
			'category_id' 		=> 'required|numeric',
			'sub_cat_id'        => 'required|numeric',
			'description' 		=> 'required|min:10|max:1000',
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$questionid =  $request->input('question_id');
			$qtns = $this->api->savequestions($request, $questionid);
			return Response::json($qtns, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- post files to questions
	*/
	public function postUploadqtnfile(Request $request){

		$rules 		 = array(
					 'user_id' => 'required|numeric',
					 'qfiles'  => 'required|mimes:zip,jpg,jpeg,png,pdf,doc,docx,txt'
					 );
		$validator 	 = Validator::make($request->all(),$rules);
		if($validator->passes()){
			$user_id   	= $request->input('user_id');
			$document  	= $request->file('qfiles');
			$file_name 	= explode('.',$document->getClientOriginalName());
            $file_name 	= $file_name[0].'_'.time().rand(4,9999);
            $file_type 	= $document->getClientOriginalExtension();
			$file_title = $document->getClientOriginalName();
			$file_size 	= $document->getSize();

			$request->file('qfiles')->move('./uploads/files/', $file_name.'.'.$file_type
			);
            $courseFiles 					= new CourseFiles;
            $courseFiles->file_name 		= $file_name;
            $courseFiles->file_title 		= $file_title;
            $courseFiles->file_type 	 	= $file_type;
            $courseFiles->file_extension 	= $file_type;
            $courseFiles->file_size 		= $file_size;
			$courseFiles->duration 			= '';
            $courseFiles->file_tag 			= 'questions';
            $courseFiles->uploader_id 		= $user_id;
            $courseFiles->created_at 		= time();
            $courseFiles->updated_at 		= time();
            if($courseFiles->save()){
                return Response::json(array('status'=>true, 'message' => 'File has been uploaded successfully', 'file_id' => $courseFiles->id), 200);
            }else{
                return Response::json(array('status'=>false), 200);
            }
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- post create questions
	*/
	public function postUpsertreply(Request $request){

		$rules = array(
			'user_id' 			=> 'required|numeric',
			'question_id' 		=> 'required|numeric',
			'reply_text' 		=> 'required',
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$answer_id =  $request->input('answer_id');
			$reply = $this->api->savereply($request, $answer_id);
			return Response::json($reply, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- post delete questions
	*/
	public function postRemovequestions(Request $request){

		$rules = array(
			'user_id' 			=> 'required|numeric',
			'question_id' 		=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$deletes = $this->api->deleteqtn($request);
			return Response::json($deletes, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- post delete answers
	*/
	public function postRemoveanswers(Request $request){

		$rules = array(
			'user_id' 			=> 'required|numeric',
			'answer_id' 		=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$deletes = $this->api->deleteanswers($request);
			return Response::json($deletes, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	/*
	*process -- post delete files
	*/
	public function postRemoveqtnfile(Request $request){

		$rules = array(
			'user_id' 			=> 'required|numeric',
			'file_id' 			=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$deletes = $this->api->deletefiles($request);
			return Response::json($deletes, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}

	}

	public function getMembership(Request $request)
	{				
		$course = $this->api->membership();
		return Response::json($course, 200);
	}

	/*
	*process -- business packages
	*/
	public function getBusinesspackage(Request $request){

		$package = $this->api->businesspackagelist($request);
		return Response::json($package, 200);
	}

	
	/*
	*process -- business packages
	*/
	public function getAssignedcourse(Request $request){

		$rules = array(
			'business_id' 			=> 'required|numeric',
			'user_id' 				=> 'required|numeric',
			'page_no' 				=> 'numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$assign = $this->api->assignedinfo($request);
			return Response::json($assign, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	public function getListbusinessusers(Request $request){

		$rules = array(
			'business_id' 			=> 'required|numeric',
			'page_no' 				=> 'numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$busers = $this->api->listbusinessusers($request);
			return Response::json($busers, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- Add business user
	*/
	public function postAddbusinessusers(Request $request){
		
		$rules = array(
			'username'=>'required|alpha_num|unique:users|min:2',
			'first_name'=>'required|alpha|min:2',
			'last_name'=>'required|alpha|min:2',
			'email'=>'required|email|unique:users',
			'password'=>'required|confirmed|between:6,12',
			'password_confirmation'=>'required|between:6,12|required_with:password',
			'business_id' =>'required|numeric',
			'status'=>'required|numeric',
			);	
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) 
		{		

			$data = array(
				'firstname'	=> $request->input('first_name') ,
				'lastname'	=> $request->input('last_name') ,
				'userid'	=> $request->input('user_id') ,
				'email'		=> $request->input('email') ,
				'username'	=> $request->input('username') ,
				'password'	=> \Hash::make($request->input('password')),
				'status'	=> $request->input('status') ,
				'business_id' => $request->input('business_id') ,
				);
			$package = $this->api->addbusinessusers($data);
			return Response::json($package, 200);		

		}
		else
		{
			return Response::json(array(
				'status' => false,
				'errors' => $validator->getMessageBag()->toArray()
				), 200); 
		}
				
	}

	/*
	*process -- Edit business user
	*/
	public function postEditbusinessusers(Request $request){

		$rules = array(
				// 'username'=>'required|alpha_num|min:2',
			'first_name'=>'required|alpha|min:2',
			'last_name'=>'required|alpha|min:2',
				// 'email'=>'required|email|unique:users',
			'password'=>'required|confirmed|between:6,12',
			'password_confirmation'=>'required|between:6,12|required_with:password',
			'business_id' =>'required|numeric',
			'status'=>'required|numeric',
			'user_id'=>'required|numeric',
			);	
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) 
		{		
			$data = array(
				'firstname'	=> $request->input('first_name') ,
				'lastname'	=> $request->input('last_name') ,
				'userid'	=> $request->input('user_id') ,
						// 'email'		=> $request->input('email') ,
						// 'username'	=> $request->input('username') ,
				'password'	=> \Hash::make($request->input('password')),
				'status'	=> $request->input('status') ,
				'business_id' => $request->input('business_id') ,
				);
			$package = $this->api->editbusinessusers($data);
			return Response::json($package, 200);		

		}else
		{
			return Response::json(array(
				'status' => false,
				'errors' => $validator->getMessageBag()->toArray()
				), 200); 
		}

	}

	/*
	*process -- course lists
	*/
	public function getCourselists(Request $request){
		$rules = array(
			'business_id' 			=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$pks = $this->api->courseinfo($request);
			return Response::json($pks, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- Assign course
	*/
	public function postAssigncourse(Request $request){
		$rules = array(
			'business_id' 			=> 'required|numeric',
			'user_id' 				=> 'required|numeric',
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$pks = $this->api->assigncoursetouser($request);
			return Response::json($pks, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- Delete business user
	*/
	public function postRemovebusinessuser(Request $request){

		$rules = array(
			'business_id' 			=> 'required|numeric',
			'user_id' 				=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$deletes = $this->api->deletebusinessuser($request);
			return Response::json($deletes, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}

	/*
	*process -- get menu
	*/
	public function getMenus(Request $request){

		$rules = array(
			'user_id' 				=> 'required|numeric'
			);
		$validator 		= Validator::make($request->all(),$rules);
		if($validator->passes()){
			$userid = $request->input('user_id');
			$plan  	= \bsetecHelpers::getDashboards($userid);
			$planstatus = 0;
			if(count($plan)>0){
				$planstatus = 1;
			}
			$membership = \bsetecHelpers::getMembershipofuserallstatus($userid);
			$membershipstatus 	  = 0;
			if(count($membership)>0){
				$membershipstatus = 1;
			}

			$arrs 	 = array('status'=>true, 'business_plan' => $planstatus, 'membership' => $membershipstatus);
			return Response::json($arrs, 200);
		}else{
			return Response::json(array('status'=>false,'errors'=>$validator->getMessageBag()->toArray()),200);
		}
	}
	
//end of controller class
}


