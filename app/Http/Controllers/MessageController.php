<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect, Session ; 
use Auth;

class MessageController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'message';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Message();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'message',
			'return'	=> self::returnUrl()
		);
		date_default_timezone_set('Asia/Kolkata');

	}

	public function postMessageFilter( Request $request)
	{
		$module = $this->module;
		$sort 	= (!is_null($request->input('sort')) ? $request->input('sort') : '');
		$order 	= (!is_null($request->input('order')) ? $request->input('order') : '');
		$rows 	= (!is_null($request->input('rows')) ? $request->input('rows') : '');
		$md 	= (!is_null($request->input('md')) ? $request->input('md') : '');
		$ty 	= (!is_null($request->input('ty')) ? $request->input('ty') : '');
		$lb 	= (!is_null($request->input('lb')) ? $request->input('lb') : '');
		
		$filter = '?';
		if($sort!='') $filter .= '&sort='.$sort; 
		if($order!='') $filter .= '&order='.$order; 
		if($rows!='') $filter .= '&rows='.$rows; 
		if($md !='') $filter .= '&md='.$md;
		if($ty !='') $filter .= '&ty='.$ty;
		if($lb !='') $filter .= '&lb='.$lb;
		
		return Redirect::to($module . $filter);
	}	
	
	public function getIndex( Request $request)
	{
		if(!Auth::check())
		{	
		 \Session::put('user_redirect','message');
            return Redirect::to('user/login'); 
        } 
        
		$user = \Auth::user();
		$uid = $user->id;
		$composeUser = (!is_null($request->input('ctu')) ? $request->input('ctu') : '0');
		$type = (!is_null($request->input('ty')) ? $request->input('ty') : '0');
		$label = (!is_null($request->input('lb')) ? $request->input('lb') : '0');
		
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getMessageRows( $params, $type, $label, $uid );
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('message');
		
		$this->data['rowData']		= $results['rows'];
		$this->data['totalType']		= $results['totalType'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		
		
		$users_data = $this->model->getMessageUsers( $uid );		
		$users = array(''=>'Select an User');
		$username = array();
		$usersNameList = array();
		$usersEmailList = array();
		$usersAvatar = array();
		foreach($users_data as $user_data){
			$users[$user_data->id] = $user_data->first_name.' '.$user_data->last_name.' ('.$user_data->email.')';
			$usersNameList[$user_data->id] = $user_data->first_name.' '.$user_data->last_name;
			$username[$user_data->id] = $user_data->username;
			$usersEmailList[$user_data->id] = $user_data->email;
			$usersAvatar[$user_data->id] = $user_data->avatar;
		}
		$this->data['username'] = $username;
		$this->data['users'] = $users;
		$this->data['usersNameList'] = $usersNameList;
		$this->data['usersEmailList'] = $usersEmailList;
		$this->data['usersAvatar'] = $usersAvatar;
		
		$labels_data = $this->model->getLabels( $uid );
		$labels = array();
		foreach($labels_data as $label_data){
			$labels[$label_data->id] = $label_data->name;
		}
		$this->data['labels'] = $labels;
		$this->data['type'] = $type;
		$this->data['label'] = $label;
		$this->data['uid'] = $uid;
		$this->data['composeUser'] = $composeUser;
		
		return view('message.inbox',$this->data);
	
	}

	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('message'); 
		}

		$this->data['id'] = $id;
		return view('message.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('message'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('message.view',$this->data);	
	}	

	function postMessageUpdate( Request $request)
	{
		if($request->input('update_function') == 'starred'){
			$this->model->postStar($request->input('msg_id'), $request->input('starred'), $request->input('msg_type'));
			echo $request->input('starred');
		} else if($request->input('update_function') == 'read'){
			$this->model->postRead($request->input('msg_id'));
		}
	}

	function postGroupMessageUpdate( Request $request)
	{
		$msg_ids = $request->input('msg_ids');
		$is_sender = $request->input('is_sender');
		$ids = explode(',', $msg_ids);
		$isSender = explode(',', $is_sender);
		if($request->input('group_update_function') == 'delete'){
			$c = 0;
			foreach($ids as $id){
				$this->model->postTrash($id, $isSender[$c]);
				$c++;
			}
			echo $c;
		} else if($request->input('group_update_function') == 'deleteTrash'){
			$c = 0;
			foreach($ids as $id){
				$this->model->postDeleteTrash($id, $isSender[$c]);
				$c++;
			}
			echo $c;
		} else if($request->input('group_update_function') == 'label'){
			$d = 0;
			foreach($ids as $id){
				$this->model->postApplyLabel($id, $isSender[$d], $request->input('label_id'));
				$d++;
			}
			echo 'label';
		} else if($request->input('group_update_function') == 'removelabel'){
			$e = 0;
			foreach($ids as $id){
				$this->model->postRemoveLabel($id, $isSender[$e]);
				$e++;
			}
			echo 'label';
		}
	}

	function postSave( Request $request, $id =0)
	{
		
		$user = \Auth::user();
		$uid = $user->id;
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('message');
			
			$data['entry_by'] = $uid;
			$data['recipient'] = $request->input('recipient');
			$data['draft'] = $request->input('draft');
			$now_date = date("Y-m-d H:i:s");
			$data['createdOn'] = $now_date;
			$data['updatedOn'] = $now_date;
			//$data['read'] = '1';
			
			if($request->has('messageid')){
				$newID = $this->model->insertRow($data, '');
			} else {
				$newID = $this->model->insertRow($data , $request->input('messageid'));
			}
			if(!is_null($request->input('apply')))
			{
				$return = 'message/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'message?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}
			return json_encode(array('status' => '1'));
			//return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
			
		} else {
			return json_encode(array('status' => '0'));
			//return Redirect::to('message/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
		}	
	
	}	

	function postLabelSave( Request $request, $id =0)
	{
		
		$user = \Auth::user();
		$uid = $user->id;
		$name = $request->input('label');		
		$rules = array(
			'label' => 'unique:message_labels,name');
		$validator = Validator::make($request->all(), $rules);			
		if ($validator->passes()) {
			//$data = $this->validatePost('message_labels');
			
			$data['entry_by'] = $uid;
			$data['name'] = $request->input('label');
			$now_date = date("Y-m-d H:i:s");
			$data['createdOn'] = $now_date;
			$data['updatedOn'] = $now_date;
			
			$newID = $this->model->insertLabelRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'message/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'message?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}
			return json_encode(array('status' => '1','newid' => $newID));
			//return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
			
		} else {
			return json_encode(array('status' => '0'));
			//return Redirect::to('message/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
		}	
	
	}	

	function postLabelDelete( Request $request)
	{
		$this->model->postDeleteLabel($request->input('label_id'));
		echo '1';
	}

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('message')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('message')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}