<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Forum;
use App\Models\Forumcategory;
use App\Models\Forumcomments;
use App\Models\Notifications;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class ForumController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'forum';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Forum();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'forum',
			'return'	=> self::returnUrl()
			
		);
	}
	
	public function getIndex( Request $request )
	{
		 
		 //self::createRouters();
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'forum_id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('forum');
		
		$this->data['rowData']		= $results['rows'];
		//echo '<pre>';
		// $count=$results['rows'];
			// foreach ($count as $key => $value) {
			// 	# code...echo [$key].$value;
			// 	$value->status;
			// }
		//print_r($count);exit;
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		// echo '<pre>';
		// print_r($this->data['tableForm']);exit;
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template

		return view('forum.index',$this->data);
	}
	

	public function getUpdate(Request $request, $id = null)
	{	//echo $id;exit;
		//$row = $this->model->find($id);
		if($id=='')
		{
			$view='Add Forum Management';
			$categories = Forumcategory::get();
			//$category = \DB::('forum_category')->get();
			return view('forum.form',$this->data)->with('category',$categories);	
		}
		elseif($id)
		{	//echo $id;exit;
			$vforum=Forum::where('forum_id','=',$id)->get();
			$categories = Forumcategory::get();
			return view('forum.edit',$this->data)->with('row',$vforum)->with('id',$id)->with('category',$categories);
		}
	}
	// for insert record
	public function postInsert(Request $request ,  $id = null)
	{	
		//echo 'correct';
		 $title = Input::get('title');
		 $description = Input::get('content');
		// echo $title,$description;
		 $cat_id = Input::get('cat_id');
		 $slug = $this->create_slug($title); 
		 $status = Input::get('status');
		 $updated_at=date("Y-m-d h:i:sa");
		 $user_id = Input::get('user_id');
		if($status=='on')
			$status=1;
		else
			$status=0;
		$des='<p>'.$description.'</p>';
		Forum::insert(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'created_at'=>$updated_at]);
		return Redirect::to('user-forum/index')->with('message','Successfully Added ');	
	}
	// for 
	public function postForumsave( Request $request )
	{	
		$title = Input::get('title');
		$id = Input::get('id');
		$description = Input::get('content');
		$cat_id = Input::get('cat_id');
		$slug = $this->create_slug($title); 
		$status = Input::get('status');
		$updated_at=date("Y-m-d h:i:sa");
		// $user_id = Input::get('user_id');
		if($status=='on')
			$status=1;
		else
			$status=0;
		//$des='<p>'.$description.'</p>';
		$forum =Forum::find($id);
		$forum->forum_topic = $title;
		$forum->category_id = $cat_id;
		$forum->slug =$slug;
		$forum->forum_content = $description;
		$forum->status = $status;
		$forum->save();
		//Forum::where('forum_id', $id)->update(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'updated_at'=>$updated_at]);
		return Redirect::to('user-forum/index')->with('message', \SiteHelpers::alert('success',\Lang::get('core.note_success')));
	}
	public function create_slug($string){     
        $replace = '-';         
        $string = strtolower($string);     
		
        //replace / and . with white space     
        $string = preg_replace("/[\/\.]/", " ", $string);     
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);     
		
        //remove multiple dashes or whitespaces     
        $string = preg_replace("/[\s-]+/", " ", $string);     
		
        //convert whitespaces and underscore to $replace     
        $string = preg_replace("/[\s_]/", $replace, $string);     
		
        //limit the slug size     
        $string = substr($string, 0, 100);     
		
        //slug is generated     
        return $string; 
    } 
	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('forum'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('forum.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('forum');
			
			$newID = $this->model->insertRow($data , $request->input('forum_id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'forum/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'forum?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('forum/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('user-forum/index')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('user-forum/index')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}
	}
	// user forum side
	public function index( Request $request )
	{	//echo 'correct';exit;
		$user = \Auth::user();
		$forum= \DB::table('forum')
            ->join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name','forum_category.slug as category_slug' )->where('forum.status','=',1)
            ->orderBy('forum_id','desc')->paginate(5);
        $count= count($forum);
        if($count==0)
        	$message='No Records';
        else
        	$message='';    
        //print_r($forum);
        $anscount= \DB::table('forum_comments')
             ->select('forum_id', \DB::raw('count(*) as total'))
             ->groupBy('forum_id')
             ->get();
        $cforum = Forumcategory::get();
        $category = Forumcategory::get();
        if (\Auth::check()){
		    $disable=Forum::where('status','=',0)->where('created_by','=',$user->id)->get();
		}
		else{
			$disable=Forum::get();
		}
		$this->data['pageTitle'] = 'Forum';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'forum.userforum';	
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('cforum',$cforum)->with('message',$message)->with('disable',$disable)->with('anscount',$anscount);
	}

	/* Show Categories wise Forum */
	public function getTopic($slug)
	{
		$forum=new Forum;
		$forum= Forum::join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name', 'forum_category.slug as category_slug')->where('forum_category.slug','=',$slug)
            ->where('forum.status','=',1)
            ->orderBy('forum_id','desc')->paginate(5);
        $count= count($forum);
        if($count==0)
        	$message='No Records';
        else
        	$message='';
        $anscount= \DB::table('forum_comments')
             ->select('forum_id', \DB::raw('count(*) as total'))
             ->groupBy('forum_id')
             ->get();
        $breadcrumb= Forum::join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name')->where('forum_category.slug','=',$slug)
            ->groupBy('forum_category.forum_cat_id')->get();
       //$breadcrumb =Forum::join('users', 'forum.created_by', '=', 'users.id')->where('forum.forum_cat_id','=',$slug)->where('status','=',1)->get();
        $category = Forumcategory::get();
		$this->data['pageTitle'] = $slug;
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'forum.userforum';	
		$theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		$user = \Auth::user();
		if($user != null)
			$disable = Forum::where('status','=',0)->where('created_by','=',$user->id)->get();
		else
			$disable = array();

		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('cforum',$category)->with('message',$message)->with('disable',$disable)->with('anscount',$anscount)->with('breadcrumb',$breadcrumb);
	}
	/* Get Particular Forum Details */	
	public function getForum($id)
	{	$chk=Forum::find($id);

		$cnt=count($chk);
		if($cnt==1)
		{
		$forum= \DB::table('forum')
            ->join('forum_category', 'forum.category_id', '=', 'forum_category.forum_cat_id')
            ->join('users', 'forum.created_by', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_category.name','forum_category.slug as category_slug')->where('forum.forum_id','=',$id)
            ->get();

        $list= \DB::table('forum_comments')
            ->join('forum', 'forum_comments.forum_id', '=', 'forum.forum_id')
            ->join('users', 'forum_comments.user_id', '=', 'users.id')
            ->select('users.*', 'forum.*', 'forum_comments.*')->where('forum_comments.forum_id','=',$id)
            ->paginate(3);
        $breadcrumb =Forum::join('users', 'forum.created_by', '=', 'users.id')->where('forum.forum_id','=',$id)->where('status','=',1)->get();
       // print_r($breadcrumb);
        //$category = Forumcomment::get();
        $category = Forumcategory::get();
		$this->data['pageTitle'] = $chk->slug;
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;

		$this->data['pages'] = 'forum.forumtopic';	
		$theme=\bsetecHelpers::get_options('theme');
$page = 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data)->with('forum',$forum)->with('category',$category)->with('list',$list)->with('message','')->with('breadcrumb',$breadcrumb);
		}
		else
		{
			return Redirect::to('user-forum')->with('message','');
		}
	}
	/* Insert Forum Comments*/
	public function postInsertt(Request $request)
	{
		$input = array(
			'comments' => strip_tags($request->input('comments')));
		$rules = array(
			'comments'=> 'required');
		$validator = Validator::make($input, $rules);	
		if ($validator->passes()) {
		 $comments 		= trim($request->input('comments'));
		 $user_id 		= $request->input('user_id');
		 $forum_id 		= $request->input('forum_id');
		 $updated_at	= date("Y-m-d H:i:s");
		 $alls = Forum::where('forum.forum_id','=',$forum_id)->get();
 
		 $commentid = Forumcomments::insertGetId(['user_id' => $user_id,'forum_id'=>$forum_id,'comment'=>$comments,'created_at'=>$updated_at]);
		 $fid 	   = $alls['0']->created_by;
		 //insert notificatioins
		 Notifications::insertGetId(['user_id' => $fid,'from_id'=>$user_id,'on_id'=>$commentid,'notification_type'=>1,'created_at'=>$updated_at]);
		  return Redirect::to('user-forum/'.$forum_id)->with('message',\SiteHelpers::alert('success','Your Comments are Successfully Added!!'));	 	
		}else{
		 	return Redirect::to('user-forum/'.$forum_id)->with('message',\SiteHelpers::alert('error','Your Comments please!'));	 	
		 }		 
	}	
	/* Insert new forum */
	public function postForuminsert()
	{
		 $title = Input::get('title');
		 $description = Input::get('content');
		 $cat_id = Input::get('cat_id');
		 $slug = $this->create_slug($title); 
		 $status = Input::get('status');
		 $updated_at=date("Y-m-d H:i:s");
		 $user_id = Input::get('user_id');
		if($status=='on')
			$status=1;
		else
			$status=0;
		$des='<p>'.$description.'</p>';
		Forum::insert(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'created_at'=>$updated_at]);
		return Redirect::to('user-forum')->with('message',\SiteHelpers::alert('success','Forum created Successfully '));
	}
	 /* user Forum Update*/
    public function postForumupdate()
	{	
			$title = Input::get('title');
			$id = Input::get('id');
			$description = Input::get('content');
			$cat_id = Input::get('cat_id');
			$slug = $this->create_slug($title); 
			$status = Input::get('status');
			$updated_at = date("Y-m-d H:i:s");
			$user_id = Input::get('user_id');
			if($status=='on')
				$status=1;
			else
				$status=0;			
			$des='<p>'.$description.'</p>';
			Forum::where('forum_id', $id)->update(['forum_topic' => $title,'category_id'=>$cat_id,'slug'=>$slug,'forum_content'=>$des,'created_by'=>$user_id,'status'=>$status,'created_at'=>$updated_at]);
			return Redirect::to('user-forum')->with('message',\SiteHelpers::alert('success','Successfully Updated'));
	}
	/* Forum Delete */
	public function getForumdelete($forum_id)
	{
		Forum::where('forum_id', '=', $forum_id)->delete();
		$getcomments = Forumcomments::where('forum_id', '=', $forum_id)->get();
		if(count($getcomments)>0){
			foreach ($getcomments as $cvalue) {
				Notifications::where('on_id', '=', $cvalue->comment_id)->where('notification_type', '=', 1)->delete();	
			}
		}
		Forumcomments::where('forum_id', '=', $forum_id)->delete();
		return Redirect::to('user-forum')->with('message',\SiteHelpers::alert('success','Successfully Deleted'));
	}
	/* Update Forum Comments*/
	public function postCommentsupdate(Request $request)
	{
		 $comment_id = Input::get('id');
		 $content = Input::get('content');
		 $forum_id = Input::get('forum_id');
		 $updated_at=date("Y-m-d H:i:s");
		 $input = array(
				'content' => strip_tags($request->get('content')));		
		 	$rules 		= array('content'=>'required');
			$validator = Validator::make($input, $rules);
		 if ($validator->passes()) {	
		 Forumcomments::where('comment_id', $comment_id)->update(['comment'=>$content,'updated_at'=>$updated_at]);
		 return Redirect::to('user-forum/'.$forum_id)->with('message',\SiteHelpers::alert('success','Comments are Updated'));
		 }else{
		 	return Redirect::to('user-forum/'.$forum_id)->with('message',\SiteHelpers::alert('error','Your Comments please!'));
		 }			 
	}
	/* Forum Comment delete */
	public function getCommentdestory($id)
	{
		$userid = \Session::get('uid');
		$forum  = Forumcomments::where('comment_id','=',$id)->first();
		Forumcomments::where('comment_id', '=', $id)->delete();
		Notifications::where('from_id', '=', $userid)->where('on_id', '=', $id)->where('notification_type', '=', 1)->delete();
		return Redirect::to('user-forum/'.$forum->forum_id)->with('message',\SiteHelpers::alert('success','Comments are Successfully Deleted'));
	}
	public function getEnable()
	{
		$title = Input::get('status');
		$count=count($title);
		if($count > 0)
		{
			for($i=0;$i<$count;$i++)
			{
				Forum::where('forum_id', $title[$i])->update(['status' => 1]);
			}
			return Redirect::to('user-forum')->with('message',\SiteHelpers::alert('success','Selected forums are Successfully Enabled'));
		}else{
			return Redirect::to('user-forum')->with('messagetext','Please Select topic.')->with('msgstatus','error');
		}
	}	


}