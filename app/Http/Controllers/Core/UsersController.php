<?php namespace App\Http\Controllers\core;

use App\Http\Controllers\Controller;
use App\Models\Core\Users;
use App\Models\Core\Groups;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect,File ; 
use App\Models\CourseImages;
use Intervention\Image\ImageManagerStatic as Image;
use App\Models\Instructor;


use App\Http\Controllers\CommonmailController;

class UsersController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'users';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->model = new Users();
		$this->sendmail = new CommonmailController();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'users',
			'return'	=> self::returnUrl()
			
		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
						
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		
		if(is_null($request->input('sort'))){
			$sort = 'created_at';
			$order = 'desc';
		}

		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? '': '');
		$filter .= " AND ".\bsetecHelpers::getdbprefix()."users.group_id >= '".\Session::get('gid')."'" ;

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('users');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('core.users.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('users'); 
		}

		$this->data['id'] = $id;
		$this->data['user_groups']  = Groups::pluck('name', 'group_id');

		return view('core.users.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('users'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		//check for instructor
		$this->data['instructor'] = Instructor::where('user_id',$id)->first();
		return view('core.users.view',$this->data);	
	}	

	function postSave( Request $request, $id=null)
	{
		
		$rules = $this->validateForm();

		$messages =['avatar:required'   => 'Profile image field is required.'];

		if($request->input('id') ==null)
		{
			$rules['active'] 				= 'required';
			$rules['password'] 				= 'required|alpha_num|between:6,12|confirmed';
			$rules['password_confirmation'] = 'required|alpha_num|between:6,12';
			$rules['email'] 				= 'required|email|unique:users';
			$rules['first_name'] 			= 'required|alpha|min:2';
			$rules['last_name'] 			= 'required|alpha|min:2';			
			$rules['username'] 				= 'required|alpha_num||min:2|unique:users';
			$rules['avatar'] 				= 'mimes:jpg,jpeg,gif,png';
			$user 							= new Users;
			$uploadProcess 					= true;
		} else {
			$rules['active'] 				= 'required';
			$rules['email'] 				= 'required|email|unique:users,id,'.$request->input('id');
			$rules['first_name'] 			= 'required|alpha|min:2';
			$rules['last_name'] 			= 'required|alpha|min:2';			
			$rules['username'] 				= 'required|alpha_num||min:2|unique:users,username,'.$request->input('id');
			$rules['avatar'] 				= 'mimes:jpg,jpeg,gif,png';
			$user 							= Users::find($request->input('id'));
			$uploadProcess 					= false;
		}

		$avatar 			= $request->file('avatar');

		if($id != null){
				if($avatar){
					$getImage = CourseImages::find($user->avatar);
					if($getImage != null){
        				$image_path = $getImage->id.".".$getImage->image_type;
    			    	if(file_exists('./uploads/users/'.$image_path)){
			            	File::Delete('./uploads/users/'.$image_path);
			            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
				        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
		                    File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
			        		$getImage->delete();
			        	}
			        }
			        $rules['avatar'] 				= 'mimes:jpg,jpeg,gif,png';
			       	$uploadProcess = true;
				}
			}

		$validator = Validator::make($request->all(), $rules,$messages);
		
		if ($validator->passes()) {
			//$data = $this->validatePost('users');
			$user->username 	= $request->input('username');
			$user->group_id 	= $request->input('group_id');
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->email 		= $request->input('email');
			$user->active 		= $request->input('active');
			
			//password process
			if($request->input('password'))
				$user->password 	= \Hash::make($request->input('password'));
				
			$user->save();
			$id 				= ($id=="") ? $user->id : $id;

			if($uploadProcess && $avatar){
				// image process	
				$type 						= $avatar->getClientOriginalExtension();
				$courseImages 				= new CourseImages;
	            $courseImages->image_title 	= $request->input('username');
	            $courseImages->image_type 	= $type;
	            $courseImages->image_tag 	= "dummy tag";
	            $courseImages->uploader_id 	= $id;
	            $courseImages->created_at 	= time();
	            $courseImages->updated_at 	= time();
	            $courseImages->save();
	            $image_id 					= $courseImages->id;
	            $image_update				= CourseImages::find($image_id);
	            $image_update->image_hash 	= md5($image_id.$image_update->created_at);
	            $image_update->save();
		
				// resize image width and height
	            $image_size[] = array('imgW'=>'500','imgH'=>'500','value'=>'normal');
	            $image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
	            $image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

	            foreach($image_size as $size){
	                $save_path = ($size['value'] =='normal') ? $image_id.'.'.$type : $image_id.'_'.$size['value'].'.'.$type; 
	                Image::make($avatar->getRealPath())->resize($size['imgW'],$size['imgH'])->save('./uploads/users/'.$save_path);
	            }

	            Image::make($avatar->getRealPath())->save('./uploads/users/'.$image_id.'_lg.'.$type);
	            $user_update = Users::find($user->id);
	            $user_update->avatar = $image_id;
	            $user_update->save();
        	}

			// mail for user activation	
        	if($request->input('active')==1){
        		$toMail 	= $user->email;
				$subject 	= CNF_APPNAME." Activation "; 			
				if(!empty($user->username)){
					$uname = $user->username;
				}else{
					$uname = $user->first_name.' '.$user->last_name;
				}
				$data 		= array('username'=>$uname);
				$fromMail 	= CNF_EMAIL;
				$tempname 	= 'mailtemplates.email_activation';
		 		$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
        	}

			//$this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'core/users/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'core/users?return='.self::returnUrl();
			}
			
			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('core/users/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			// user image delete
			foreach ($request->input('id') as $user_id) {
				$user = Users::find($user_id);
				$checkforCCR = \bsetecHelpers::isappliedccr($user_id);
				if(count($checkforCCR)){
					return Redirect::to('core/users')
        		->with('messagetext', $user->username.'</br>'.\Lang::get('core.not_delete_user'))->with('msgstatus','error'); 
				}
				$getImage = CourseImages::find($user->avatar);
				if($getImage != null){
    				$image_path = $getImage->id.".".$getImage->image_type;
			    	if(file_exists('./uploads/users/'.$image_path)){
		            	File::Delete('./uploads/users/'.$image_path);
		            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
			        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
	                    File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
		        		$getImage->delete();
		        	}
		        }
			}

			$this->model->destroy($request->input('id'));
			
			// redirect
			return Redirect::to('core/users')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('core/users')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}

	public function deleteUsersimage( Request $request ){
		$getImage = CourseImages::where('uploader_id',$request->get('id'))->first();
		if($getImage != null){
			$image_path = $getImage->id.".".$getImage->image_type;
	    	if(file_exists('./uploads/users/'.$image_path)){
            	File::Delete('./uploads/users/'.$image_path);
            	File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
	        	File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
                File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
        		$getImage->delete();
        		$user = Users::find($request->get('id'));
				$user->avatar = 0;
	            $user->save();
        	}
        }
	}

	function getBlast()
	{
		$this->data = array(
			'groups'	=> Groups::all(),
			'pageTitle'	=> 'Blast Email',
			'pageNote'	=> 'Send email to users'
		);	
		return view('core.users.blast',$this->data);		
	}

	function postDoblast( Request $request)
	{

		$rules = array(
			'subject'		=> 'required',
			'message'		=> 'required|min:10',
			'groups'		=> 'required',
			'status'		=> 'required',				
		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) 
		{	

			if(!is_null($request->input('groups')))
			{
				$groups = $request->input('groups');
				for($i=0; $i<count($groups); $i++)
				{
					if($request->input('status') == '3')
					{
						$users = \DB::table('users')->where('group_id','=',$groups[$i])->get();
					} else {
						$users = \DB::table('users')->where('active','=',$request->input('status'))->where('group_id','=',$groups[$i])->get();
					}
					$count = 0;
					foreach($users as $row)
					{
						//subject content
						$subject = str_replace('[fullname]',$row->first_name.' '. $row->last_name,$request->input('subject'));
						$subject = str_replace('[first_name]',$row->first_name,$subject);
						$subject = str_replace('[last_name]',$row->last_name,$subject);
						$subject = str_replace('[email]',$row->email,$subject);

						//message content
						$message = str_replace('[fullname]',$row->first_name.' '. $row->last_name,$request->input('message'));
						$message = str_replace('[first_name]',$row->first_name,$message);
						$message = str_replace('[last_name]',$row->last_name,$message);
						$message = str_replace('[email]',$row->email,$message);

						$toMail = $row->email;
						$message = view('user.emails.blast-mail', array('content'=>$message));
						$fromMail = CNF_EMAIL;
						$tempname = 'user.emails.blast-mail';
						$this->sendmail->getMail($fromMail,$toMail,$subject,array('content'=>$message),$tempname);
						$count = ++$count;					
					} 
					
				}
				return Redirect::to('core/users/blast')->with('messagetext','Total '.$count.' Message has been sent')->with('msgstatus','success');

			}
			return Redirect::to('core/users/blast')->with('messagetext','No Message has been sent')->with('msgstatus','info');
			

		} else {

			return Redirect::to('core/users/blast')->with('messagetext', 'The following errors occurred')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	

	}



}