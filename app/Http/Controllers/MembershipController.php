<?php namespace App\Http\Controllers;

use Carbon\Carbon;
use Response, Lang;
use App\Models\Course;
use App\Models\bsetec;
use App\Models\Membership;
use App\Models\Admincoupon;
use Illuminate\Http\Request;
use Validator, Input, Redirect;
use App\Http\Controllers\Controller;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\CourselistingController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;


class MembershipController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();
	public $module = 'membership';
	static $per_page	= '10';

	public function __construct()
	{

        $this->bsetec 			= new bsetec();
		$this->model 			= new Membership();
		$this->coursemodel 		= new Course();
        $this->transaction 		= new TransactionController();
        $this->courselisting 	= new CourselistingController();
		$this->info 			= $this->model->makeInfo( $this->module);
		$this->access 			= $this->model->validAccess($this->info['id']);

		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'membership',
			'return'	=> self::returnUrl()

		);
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'level');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');


		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('membership');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any

		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		return view('membership.index',$this->data);
	}



	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
			if(isset($this->data['row']['plan_id']) && !empty($this->data['row']['plan_id'])){
				$plan_id = $this->data['row']['plan_id'];
				$result = \DB::table('course')
		                    ->leftJoin('membership_course', 'course.course_id', '=', 'membership_course.course_id')
		                    ->where('membership_course.plan_id','=', $plan_id)
		                    ->orderBy('membership_course.membership_course_id', 'asc')
		                    ->get();
				$this->data['rowData'] = $result;
				$this->data['readonly'] = 'readonly';
			}

		} else {
			$this->data['row'] = $this->model->getColumnTable('membership_plan');
			$this->data['readonly'] = '';
		}

		$this->data['id'] = $id;
		return view('membership.form',$this->data);
	}

	public function getShow( $id = null)
	{

		if($this->access['is_detail'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');

		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('membership_plan');
		}

		$this->data['id'] 		= $id;
		$this->data['access']	= $this->access;
		return view('membership.view',$this->data);
	}

	function postSave( Request $request, $id =0)
	{

		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$data = $this->validatePost('tb_membership');
			$stripe = $this->bsetec->get_options('stripe');

            if(!$request->input('plan_id')){

                $monthly_id = str_random(12);
                $monthly_name = $request->input('plan_name').' Monthly';
                $monthly_amt = $this->truncate_number($request->input('plan_amount'),2)*100;
			   	$data['stripe_plan'] = $monthly_id;

                $yearly_id 		= str_random(12);
                $yearly_name 	= $request->input('plan_name').' Yearly';
                $yearly_amt 	= $this->truncate_number($request->input('plan_amount_y'),2)*100;
                $data['stripe_plan_y'] = $yearly_id;

			   	if($stripe['payment'] == 1){
				   	\Stripe\Stripe::setApiKey($stripe['secret_key']);
				   	//monthly plan
	                $plan_m_id = \Stripe\Plan::create([
	                    "id" 		=> $monthly_id,
	                    "name" 		=> $monthly_name,
	                    "amount" 	=> $monthly_amt,
	                    "interval" 	=> "month",
	                    "currency" 	=> CNF_CURRENCY
	                ]);

	                //yearly plan
	                $plan_y_id = \Stripe\Plan::create([
	                	"id" 	 	=> $yearly_id,
	                	"name" 		=> $yearly_name,
	                	"amount" 	=> $yearly_amt,
	                	"interval" 	=> "year",
	                	"currency" 	=> CNF_CURRENCY
	                	]);

            	}
                $count = \DB::table('membership_plan')->count();
                $data['level'] = $count++;
            } else {
                $plan_id = $request->input('plan_id');
                $memberships = \DB::table('membership_plan')->where('plan_id',$plan_id )->first();
                if ($memberships){
                    $monthly_name = $request->input('plan_name').' Monthly';
                    $monthly_amt = $this->truncate_number($request->input('plan_amount'),2)*100;
                    $data['stripe_plan'] = $memberships->stripe_plan;

                    $yearly_name = $request->input('plan_name').' Yearly';
                    $yearly_amt = $this->truncate_number($request->input('plan_amount_y'),2)*100;
                    $data['stripe_plan_y'] = $memberships->stripe_plan_y;
                    

                    if($stripe['payment'] == 1){
				   		\Stripe\Stripe::setApiKey($stripe['secret_key']);
                        // monthly plan
                        $planformonth = \Stripe\Plan::retrieve($data['stripe_plan']);
                        $planformonth->name = $monthly_name;
                        $planformonth->save();
                         // yearly plan
                        $planforyear = \Stripe\Plan::retrieve($data['stripe_plan_y']);
                        $planforyear->name = $yearly_name;
                        $planforyear->save(); 

                    }
                    
                }
            }

			$newID = $this->model->insertRow($data , $request->input('plan_id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'membership/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'membership?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');

		} else {

			return Redirect::to('membership/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}

	}

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows

		if(count($request->input('id')) >=1)
		{
            $plan_ids = $request->input('id');
            foreach ($plan_ids as $key => $plan) {
                $memberships = \DB::table('membership_plan')->where('plan_id',$plan )->first();
                //coursedelete
                $this->model->deletecourse($plan);
                if ($memberships){
                    // Delete a Stripe Plan
                	$stripe = $this->bsetec->get_options('stripe');
                	if($stripe['payment'] == 1){
                		\Stripe\Stripe::setApiKey($stripe['secret_key']);
	                    if($memberships->stripe_plan){
	                       	$monthly_id = $memberships->stripe_plan;
	                        // monthly plan
							$plan_m = \Stripe\Plan::retrieve($monthly_id);
							$plan_m->delete();
	                    }
	                    if($memberships->stripe_plan_y){
	                        $yearly_id = $memberships->stripe_plan_y;
	                        $plan_y = \Stripe\Plan::retrieve($yearly_id);
							$plan_y->delete();
	                    }
                	}
                }
            }
			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('membership')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success');

		} else {
			return Redirect::to('membership')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');
		}

	}

	public function postAddacourse( Request $request)
	{

		$course_id = $request->input('course_id');
		$plan_id = $request->input('plan_id');

        // $result = \DB::table('membership_course')->where('plan_id',$plan_id )->where('course_id',$course_id )->first();
		$result = \DB::table('membership_course')->where('course_id',$course_id )->first();

		if(empty($result)){
			$data = array();
			$data['plan_id'] = $plan_id;
			$data['course_id'] = $course_id;
			$newID = \DB::table('membership_course')->insertGetId($data);

			$course = \DB::table('course')->where('course_id',$course_id)->first(); //->whereRaw('FIND_IN_SET(?,curriculum)', [$plan_id])
			if(!empty($course) && isset($course->curriculum)){
				$course_plans = explode(',', $course->curriculum);
				if (!in_array($plan_id, $course_plans)) {
					array_push($course_plans, $plan_id);
					$plan_ids = implode(',', $course_plans);
					\DB::table('course')->where('course_id',$course_id)->update(['curriculum' => $plan_ids]);
				}
			} else {
            	\DB::table('course')->where('course_id',$course_id)->update(['curriculum' => $plan_id]);
			}

			return json_encode(array('status' => '1','id' => $newID));
		} else {
			return json_encode(array('status' => '0'));
		}

	}

	public function postRemcourse( Request $request)
	{
		$course_id = $request->input('course_id');
		$plan_id = $request->input('plan_id');

		$course = \DB::table('course')->where('course_id',$course_id)->first(); //->whereRaw('FIND_IN_SET(?,curriculum)', [$plan_id])
		if(!empty($course) && isset($course->curriculum)){
			$course_plans = explode(',', $course->curriculum);
			if(($key = array_search($plan_id, $course_plans)) !== false) {
                unset($course_plans[$key]);
				$plan_ids = implode(',', $course_plans);
				\DB::table('course')->where('course_id',$course_id)->update(['curriculum' => $plan_ids]);
            }
		}
    	return \DB::table('membership_course')->where('course_id',$course_id)->where('plan_id',$plan_id)->delete();
	}

	public function postUpdatecoursemembership( Request $request)
	{
		$course_id = $request->input('course_id');
		$plan_ids  = $request->input('plan_ids');

		$plans = explode(',', $plan_ids);

    	\DB::table('membership_course')->where('course_id',$course_id)->delete();
		foreach ($plans as $key => $plan) {
            if($plan){
    			$data 				= array();
    			$data['plan_id'] 	= $plan;
    			$data['course_id'] 	= $course_id;
    			$newID = \DB::table('membership_course')->insertGetId($data);
            }
		}

		return \DB::table('course')->where('course_id',$course_id)->update(['curriculum' => $plan_ids]);
	}

	public function getCourselist( $plan_id ){
		$memberships = \DB::table('membership_plan')->where('plan_id',$plan_id )->first();
        if (count($memberships)!= 1) {
            return Redirect::to('course');
        }
        $plan_periods_opt = array( '1' => 'Monthly' ,  '12' => 'Yearly' , );
        $course                     = $this->model->getmemberships($plan_id);
        $banners                    = $this->coursemodel->getbanners();
        $this->data['bannercount']  = count($banners);
        $this->data['banners']      = $banners;
        $this->data['course']       = $course;
        $this->data['search']       = $memberships->plan_name;
         return view('course.list',$this->data);
	}

	public function postCouponcheck( Request $request ){
        if($request->get('code') != NULL){
            $coupon = Admincoupon::where('coupon_code',$request->get('code'))
                        ->where('coupon_status',1)
                        ->first();

            if($coupon == NULL){
                    return Response::json(array(
                        'errors' => 'Sorry, the coupon code "<b>'.$request->get('code').'</b>" is invalid.'
                    ), 400);
            }else{
                if($coupon->coupon_start_date !='0000-00-00'){
                  $coupon = Admincoupon::where('coupon_code',$request->get('code'))
                            ->where('coupon_start_date','<=',Carbon::now()->toDateString())
                            ->where('coupon_end_date','>=',Carbon::now()->toDateString())
                            ->where('coupon_status','1')
                            ->where('usage_limit','>=','1')
                            ->first();
                    if($coupon == NULL){
                        return Response::json(array(
                                'errors' => 'Sorry, the coupon code "<b>'.$request->get('code').'</b>" is expired.'
                        ), 400);
                   }
                }

                $plan_id = $request->get('plan_id');
                $plan_type = $request->get('plan_type');
                $membership_plan = \DB::table('membership_plan')->where('plan_id',$plan_id)->first();
                if($plan_type == 'm'){
                    $plan_price = $membership_plan->plan_amount;
                } else if($plan_type == 'y'){
                    $plan_price = $membership_plan->plan_amount_y;
                }

                $discount_amount = $coupon->coupon_value;

                if($coupon->coupon_type == 1){
                    $discount_amount =  $plan_price * ($discount_amount / 100);
                    $amount 		 = $plan_price - $discount_amount;
                }else{
                    $amount = $plan_price - $discount_amount;
                }

                if($amount < 0)
                       $amount = 0;

                $return_msg = array(
                        'discount_amount'=>$discount_amount,
                        'amount'=>$amount,
                        'success_message'=>'Success! the coupon code "<b>'.$request->get('code').'</b>" is applied.'
                    );

                if(\Session::has('coupon_price'))
                    \Session::forget('coupon_price',$amount);


                    \Session::put('coupon_price',$amount);
                    \Session::save();
                }

              return response()->json($return_msg,200);
            }
    }

    public function getUsers( Request $request)
    {
        if(!\Auth::check()) return Redirect::to('user/login');
        $data 					 = $this->transaction->getIndexusers($request);
        $data['pageTitle'] 		 = $this->info['title'].' Users';
        $data['pageNote'] 		 = 'Manage '.$data['pageTitle'];
        $data['membership_plan'] = \DB::table('membership_plan')->get();
        return view('membership.users',$data);
    }

    public function getCourse( Request $request)
    {
        if(!\Auth::check()) return Redirect::to('user/login');
        $data 				= $this->courselisting->getIndexcourse($request);
        $data['pageTitle'] 	= $this->info['title'].' Courses';
        $data['pageNote'] 	= 'Manage '.$data['pageTitle'];
        return view('membership.course',$data);
    }

    function postMultisearchusers( Request $request)
    {
        $post = $_POST;
        $items ='';
        foreach($post as $item=>$val):
            if($_POST[$item] !='' and $item !='_token' and $item !='md' && $item !='id'):
                $items .= $item.':'.trim($val).'|';
            endif;
        endforeach;
        return Redirect::to($this->module.'/users?search='.substr($items,0,strlen($items)-1).'&md='.Input::get('md'));
    }

    function postMultisearchcourses( Request $request)
    {
        $post = $_POST;
        $items ='';
        foreach($post as $item=>$val):
            if($_POST[$item] !='' and $item !='_token' and $item !='md' && $item !='id' && $item !='membership'):
                $items .= $item.':'.trim($val).'|';
            endif;
        endforeach;
        return Redirect::to($this->module.'/course?search='.substr($items,0,strlen($items)-1).'&md='.Input::get('md'));
    }

    function postUpdatelevel( Request $request)
    {
        $sortlist = $request->input('sortlist');
        foreach($sortlist as $item=>$val):
            \DB::table('membership_plan')->where('plan_id',$item)->update(['level' => $val]);
        endforeach;
    }

    function truncate_number( $number, $precision = 2) {
        // Are we negative?
        $negative = $number / abs($number);
        // Cast the number to a positive to solve rounding
        $number = abs($number);
        // Calculate precision number for dividing / multiplying
        $precision = pow(10, $precision);
        // Run the math, re-applying the negative value to ensure returns correctly negative / positive
        return floor( $number * $precision ) / $precision * $negative;
    }

    public function postIndsummary( Request $request)
    {
       $courseid=$request->get('course_id');
       $coursedata=$this->coursemodel->getcourseinfo($courseid);
       $data="<h2>".$coursedata->course_title."</h2><br>".$coursedata->description;
       return $data;
       
    }

    public function postIndstatus( Request $request)
    {
       $courseid = $request->get('course_id');
       $stuentid = $request->get('studentid');
       $coursedata = $this->coursemodel->getcourseinfo($courseid);
       $courseprogress = $this->coursemodel->getCourseProgress($courseid, $stuentid);

       if(count($courseprogress)>0) {
	       	$status = 'completed';
	       	foreach($courseprogress as $progress) {
	       		if($progress->status==0)
	       			$status = 'Inprogress';
	       	}
       } else {
       		$status = 'Pending';
       }

       $data = "
       <span class='statuscourse_image col-sm-3'>
       <img class='' src=".\bsetecHelpers::getImage($coursedata->image)." alt=".$coursedata->course_title." width='200' height='200'/></span>
       <span class='course_desc col-sm-9'>
       <h2>".$coursedata->course_title."</h2><p>".$coursedata->description."</p><p class='".$status."'>Status : <span>".$status."</span></p>";
       return $data;
       
    }

    function getCourseselect( Request $request)
	{

		if(\Auth::check() == true)
		{
			$param = explode(':',$request->input('filter'));
			$limit = (!is_null($request->input('amp;limit')) ? $request->input('amp;limit') : null);
			$rows = $this->model->getcourseselect($param,$limit);
			$items = array();
		
			$fields = explode("|",$param[2]);
			
			foreach($rows as $row) 
			{
				$value = "";
				foreach($fields as $item=>$val)
				{
					if($val != "") $value .= $row->$val." ";
				}
				$paramValue = $param['1'];
				$items[] = array($row->$paramValue , utf8_encode($value));
	
			}
			
			echo json_encode($items); 	
		} else {
			echo json_encode(array('OMG'=>" Ops .. Cant access the page !"));
		}	
	}
}
