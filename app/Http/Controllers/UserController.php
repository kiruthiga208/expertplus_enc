<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\Users;
use App\Models\Course;
use App\Models\bsetec;
use App\Models\UserCredits;
use App\Models\Withdrawrequests;
use App\Models\Businessplan;

use App\Models\Instructor;
use App\Models\CourseImages;
use App\Models\Notificationsettings;
use Hash;
use File;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\CommonmailController;
use App\Models\Membership;

use Socialize;
class UserController extends Controller {

	
	protected $layout = "layouts.main";

	public function __construct() {

		//$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Users();
		$this->course = new Course();
		$this->bsetec = new bsetec();
		$this->sendmail = new CommonmailController();
		$this->user_credits = new UserCredits();
		$this->withdraw_requests = new withdrawrequests();
		 $this->membershipmodel= new Membership;
		$this->businessplan = new Businessplan();


	} 



	public function getRegister(Request $request) {

		if(CNF_REGIST =='false') :    
			if(\Auth::check()):
				return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));
			else:
				return Redirect::to('user/login');
			endif;

			else :
			$data=array();
			$data['email']=$request->input('email','');
			$data['first_name']=$request->input('first_name','');
			$data['last_name']=$request->input('last_name','');
			$data['username']=$request->input('username','');
			if(Input::exists('email')||Input::exists('username')) {
			$social_id = \Session::get('social_id');
			$social_type = \Session::get('social_type');
			$socialavatar = \Session::get('social_avatar');
			} else {
				$social_id='';
				$social_type='';
				$socialavatar='';
			}
			$data['social_id']=$social_id;
			$data['social_type']=$social_type;
			$data['socialavatar'] = $socialavatar;
			return view('user.register',$data);  
			endif ; 



		}

		public function postCreate( Request $request) {

			$rules = array(
				'username'=>'required|unique:users|min:2',
				'firstname'=>'required|alpha|min:2',
				'lastname'=>'required|alpha|min:2',
				'email'=>'required|email|unique:users',
				'password'=>'required|alpha_num|between:6,12|confirmed',
				'password_confirmation'=>'required|alpha_num|between:6,12'
				);	
			if(CNF_RECAPTCHA =='true'){
				$rules['recaptcha_response'] = 'required|captcha';
				$messages = array('recaptcha_response.required'=>'The captcha field is required','recaptcha_response.captcha'=>'The captcha field is not correct');
				$validator = Validator::make($request->all(), $rules,$messages);
			}else{
				$validator = Validator::make($request->all(), $rules);
			}

			if ($validator->passes()) {
				$code = rand(10000,10000000);

				$authen = new User;
				$authen->first_name = $request->input('firstname');
				$authen->last_name = $request->input('lastname');
				$authen->email = trim($request->input('email'));
				$authen->username = trim($request->input('username'));
				$authen->social_id = $request->input('social_id');
				$authen->social_type = $request->input('social_type');
				$authen->social_avatar = $request->input('social_avatar');
				$authen->activation = $code;
				$authen->group_id = 3;
				$authen->password = \Hash::make($request->input('password'));
				if($authen->social_type!='')
				{
					$authen->active=1;
					$authen->save();
					return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success','Registration Successfully completed'));
				}
				else
				{
				if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
				$authen->save();
				
				if ($request->session()->has('social_id')) {
					$request->session()->forget('social_id');
				}
				if ($request->session()->has('social_type')) {
					$request->session()->forget('social_type');
				}
				if ($request->session()->has('social_avatar')) {
					$request->session()->forget('social_avatar');
				}

				$data = array(
					'firstname'	=> $request->input('firstname') ,
					'lastname'	=> $request->input('lastname') ,
					'email'		=> $request->input('email') ,
					'password'	=> $request->input('password') ,
					'code'		=> $code

					);
				if(CNF_ACTIVATION == 'confirmation')
				{ 

					$toMail = $request->input('email');
					$subject = CNF_APPNAME." REGISTRATION "; 			
					$message = view('user.emails.registration', $data);
					$fromMail = CNF_EMAIL;
					$tempname = 'user.emails.registration';
					// $headers  = 'MIME-Version: 1.0' . "\r\n";
					// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// $headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
					// mail($to, $subject, $message, $headers);
			 		
			 		$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
			 		
					$message = \Lang::get('core.reg_inbox');
				} elseif(CNF_ACTIVATION=='manual') {
					$message = \Lang::get('core.reg_validate');
				} else {
					$message = \Lang::get('core.reg_success');         

				}	


				return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success',$message));
			}
			} else {
				return Redirect::to('user/register')->with('message',\SiteHelpers::alert('error','The following errors occurred')
					)->withErrors($validator)->withInput();
			}
		}

		public function getActivation( Request $request  )
		{
			$num = $request->input('code');
			if($num =='')
				return Redirect::to('user/login')->with('message',\SiteHelpers::alert('error','Invalid Code Activation!'));

			$user =  User::where('activation','=',$num)->get();
			if (count($user) >=1)
			{
				\DB::table('users')->where('activation', $num )->update(array('active' => 1,'activation'=>''));

				$toMail 	= $user['0']->email;
				$subject 	= CNF_APPNAME." Activation "; 			
				if(!empty($user['0']->username)){
					$uname = $user['0']->username;
				}else{
					$uname = $user['0']->first_name.' '.$user['0']->last_name;
				}
				$data 		= array('username'=>$uname);
				$fromMail 	= CNF_EMAIL;
				$tempname 	= 'mailtemplates.email_activation';
		 		
		 		$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);

				return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success',\Lang::get('core.ac_active')));

			} else {
				return Redirect::to('user/login')->with('message',\SiteHelpers::alert('error',\Lang::get('core.ac_error')));
			}



		}

		public function getLogin() {
            
			if(\Auth::check())
			{
				return Redirect::to('')->with('message',\SiteHelpers::alert('success',\Lang::get('core.ac_login')));

			} else {
				$this->data['socialize'] =  config('services');
				return View('user.login',$this->data);

			}	
		}

		public function getForgot() 
		{

			if(\Auth::check())
			{
				return Redirect::to('')->with('message',\SiteHelpers::alert('success',\Lang::get('core.ac_login')));

			} 
			else 
			{
				$this->data['socialize'] =  config('services');
				return View('user.forgot',$this->data);

			}	
		}

		public function postSignin( Request $request) {

			$rules = array(
				'email'=>'required|email',
				'password'=>'required',
				);	
			$messages = array(
			'captcha.required' => \Lang::get('core.verify_captcha'),
			'captcha.captcha' => \Lang::get('core.verify_captcha_correctly'));		
			if(CNF_RECAPTCHA =='true') $rules['captcha'] = 'required|captcha';
			$validator = Validator::make(Input::all(), $rules,$messages);
			if ($validator->passes()) {	
				$remember = $request->input('remember')==null ? false : true;
				if (\Auth::attempt(array('email'=>$request->input('email'), 'password'=>$request->input('password')), $remember)) {
					if(\Auth::check())
					{
						$row = User::find(\Auth::user()->id); 

						if($row->active =='0')
						{
						// inactive 
							\Auth::logout();
							return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error',\Lang::get('core.ac_notactive')));

						} else if($row->active=='2')
						{
						// BLocked users
							\Auth::logout();
							return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error',\Lang::get('core.ac_blocked')));
						} else {
							\DB::table('users')->where('id', '=',$row->id )->update(array('last_login' => date("Y-m-d H:i:s")));
							\Session::put('uid', $row->id);
							\Session::put('gid', $row->group_id);
							\Session::put('eid', $row->email);
							\Session::put('ll', $row->last_login);
							\Session::put('fid', $row->first_name.' '. $row->last_name);
							\Session::put('ut' , $row->user_type);	
							\bsetecHelpers::addonlineusers($row->id);	
							if(!is_null($request->input('language')))
							{
								\Session::put('lang', $request->input('language'));	
							} else {
								\Session::put('lang', 'en');	
							}  
							if(CNF_FRONT =='false'){
								return Redirect::to('dashboard');						
							}else {
								$redirect = \Session::get('user_redirect');
								if( $redirect != null ){
									\Session::forget('user_redirect');
									return Redirect::to($redirect);
								}else{
									return Redirect::to('course');
								}
							}
					
						}			

					}			

				} else {
					return Redirect::to('user/login')
					->with('message', \SiteHelpers::alert('error',\Lang::get('core.ac_mismatch')))
					->withInput();
				}
			} else {

				return Redirect::to('user/login')
				->with('message', \SiteHelpers::alert('error',\Lang::get('core.ac_in_error')))
				->withErrors($validator)->withInput();
			}	
		}

	/*
	* @task - user profile update
	* @param - user details
	* @return - view of profile
	*/
	public function getProfile() {
		
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] = 'Profile';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
		$info =	User::find(\Session::get('uid'));
		$this->data['info'] = $info;
		$this->data['pages'] = 'user.profile'; 
		//check for instructor
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  

        $theme=\bsetecHelpers::get_options('theme');
        $this->data['currency']=\bsetecHelpers::getcurrencymethod();
$page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}

	public function postSaveprofile( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('user/login');

		$rules = array(
			'first_name'=>'required|alpha_num|min:2',
			'last_name'=>'required|alpha_num|min:2',
			'headline'=>'max:60',
		);

		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			$user = User::find(\Session::get('uid'));
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->designation 	= $request->input('designation');
			$user->headline 	= $request->input('headline');
			$user->lang 		= $request->input('lang');
			$user->link_web 	= $request->input('link_web');
			$user->biography	= $request->input('biography');
			$user->link_twitter 	= $request->input('link_twitter');
			$user->link_facebook 	= $request->input('link_facebook');
			$user->link_linkedin 	= $request->input('link_linkedin');
			$user->link_youtube 	= $request->input('link_youtube');
			$user->link_google_plus = $request->input('link_google_plus');
			$user->currency 		= $request->input('currency');
			
			if($user->save())
			\Session::forget('fid');
			\Session::put('fid', $user->first_name.' '. $user->last_name);	
			return Redirect::to('user/profile')->with('message',\SiteHelpers::alert('success','<b>Your Profile is Successfully Updated!!</b>'));
	 }else {
			return Redirect::to('user/profile')->with('message',\SiteHelpers::alert('error','Following errors occured...'))
			->withErrors($validator)->withInput();
		}	
	
	}
	public function getLiveclass() {
		if(!\Auth::check()) return Redirect::to('user/login');
		 $loggeduser = \Session::get('uid');
		$this->data['pageTitle'] = 'Live Class Settings';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
	$this->data['liveclass']=\DB::table('citrix')->where('user_id',$loggeduser)->orderBy('id', 'desc')->first();
	 if($this->data['liveclass']=="")
	 {	 $this->data['liveclass'] = (object)array("email"=>false,"clientid"=>false);	 }
		$this->data['pages'] = 'user.liveclass';   
        $theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data);
	}
	public function postLiveclass(Request $request) {
		if(!\Auth::check()) return Redirect::to('user/login');
		 $loggeduser = \Session::get('uid');
		$user_id=$request->input('user_id');	
		$password=$request->input('password');	
		$clientid=$request->input('citrix_clientid');	
		$url="https://api.citrixonline.com/oauth/access_token?grant_type=password&user_id=".$user_id."&password=".$password."&client_id=".$clientid;
		$headers = array(
        "Content-type: application/x-www-form-urlencoded",
        "Accept: application/json",
        );
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
		$generate=json_decode($result);
       if(isset($generate->error))
        { 	     return Redirect::to('user/liveclass/')
        		    ->with('messagetext',$generate->error)->with('msgstatus','error');
        }
		$rules = array(
			'access_token'=>'required',
			'organizer_key'=>'required',
			'account_key'=>'required'
			);		
	$generate=(array) $generate;
	$validator = Validator::make($generate, $rules);
	if ($validator->passes()) {
	$generate['clientid']=$clientid;
	$generate['user_id']=$loggeduser;
	$success=\DB::table('citrix')->insert($generate);
		if($success==1)
		{
		     return Redirect::to('user/liveclass/')
	 		   ->with('messagetext','Saved')->with('msgstatus','success');
		}
	}
	else{

	}


	}
	public function postSavepassword( Request $request)
	{
		$rules = array(
			'password'=>'required|alpha_num|between:6,12',
			'password_confirmation'=>'required|alpha_num|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$user = User::find(\Session::get('uid'));
			$user->password = \Hash::make($request->input('password'));
			$user->save();

			return Redirect::to('user/profile')->with('message', \SiteHelpers::alert('success','Password has been saved!'));
		} else {
			return Redirect::to('user/profile')->with('message', \SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
		}	

	}	
	
	public function getReminder()
	{

		return view('user.remind');
	}	

	public function postRequest( Request $request)
	{

		$rules = array(
			'credit_email'=>'required|email'
			);	
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
			$user_social = User::where('social_id','!=','')->where('email','=',$request->input('credit_email'));
			$user =  User::where('email','=',$request->input('credit_email'));
			if($user->count() >=1 && $user_social->count()==0)
			{
				$user = $user->get();
				$user = $user[0];
				$data = array('token'=>$request->input('_token'));	
				$toMail = $request->input('credit_email');
				$subject = CNF_APPNAME." REQUEST PASSWORD RESET "; 			
				$message = view('user.emails.auth.reminder', $data);
				$fromMail = CNF_EMAIL;
				$tempname = 'user.emails.auth.reminder';
				// $headers  = 'MIME-Version: 1.0' . "\r\n";
				// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// $headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
				// mail($to, $subject, $message, $headers);				

				$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
				
				$affectedRows = User::where('email', '=',$user->email)
				->update(array('reminder' => $request->input('_token')));

				return Redirect::to('user/forgot')->with('message', \SiteHelpers::alert('success','Please check your email'));	
				
			} else {
				// return Redirect::to('user/forgot')->with('message', \SiteHelpers::alert('error','Cant find email address'));
				if($user_social->count()>0)
				{
					return Redirect::to('user/forgot')->with('message', \SiteHelpers::alert('error','Password reset not allowed for social login'));
				}
				if($user->count()==0)
				{
				return Redirect::to('user/forgot')->with('message', \SiteHelpers::alert('error','Cant find email address'));
				}
			}

		}  else {
			return Redirect::to('user/forgot')->with('message', \SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
		}	 
	}	
	
	public function getReset( $token = '')
	{
		if(\Auth::check()) return Redirect::to('dashboard');

		$user = User::where('reminder','=',$token);
		if($user->count() >=1)
		{
			$data = array('verCode'=>$token);
			return view('user.remind',$data);	
		} else {
			return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Cant find your reset code'));
		}
		
	}	
	
	public function postDoreset( Request $request , $token = '')
	{
		$rules = array(
			'password'=>'required|alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			
			$user =  User::where('reminder','=',$token);
			if($user->count() >=1)
			{
				$data = $user->get();
				$user = User::find($data[0]->id);
				$user->reminder = '';
				$user->password = \Hash::make($request->input('password'));
				$user->save();	

				if(!empty($user->username)){
					$uname  = $user->username;
				}else{
					$uname  = $user->first_name.' '.$user->last_name;
				}
				$data = array('username'=>$uname,'password'=>$request->input('password'));	
				$toMail = $user->email;
				$subject = CNF_APPNAME." New Password "; 			
				$fromMail = CNF_EMAIL;
				$tempname = 'mailtemplates.reset_password';
				$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
			}

			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success','Password has been saved!'));
		} else {
			return Redirect::to('user/reset/'.$token)->with('message', \SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
		}	

	}	

	public function getLogout() {
		if(\Auth::check()) {
			\bsetecHelpers::removeonlineusers(\Auth::user()->id);	
			\Cache::forget('user-is-online-' . \Auth::user()->id);
			\Cache::forget('user-is-away-' . \Auth::user()->id);
		}
		\Auth::logout();
		\Auth::logout();
		\Session::flush();
		return Redirect::to('')->with('message', \SiteHelpers::alert('info','Your are now logged out!'));
	}
	
	public function getSocial($social) {
		try {
			$user = Socialize::with($social)->user();
			if($social=='google' || $social == 'facebook')
			{
			$check = User::where('email',$user->email)->first();
			}
			if($social == 'twitter')
			{
				$check = User::where('username',$user->name)->first();
			}
			$user_details =  User::where('social_id',$user->id)->where('social_type',$social)->first();
			if(is_null($user_details) && is_null($check)){

				switch($social) {
					case "facebook":
						if(count($user->user)>0) {
							if(isset($user->user['first_name']) && isset($user->user['last_name'])){
								$first_name = $user->user['first_name'];
								$last_name  = $user->user['last_name'];
							}else{
								if(isset($user->user['name'])){
									$getnames   = explode(' ', $user->user['name']);
									$first_name = $getnames['0'];
									$last_name  = $getnames['1'];
								}
							}
							$email  = $user->user['email'];
							$avatar = $user->avatar_original;
							$params ='?email='.$user->user['email'].'&first_name='.$first_name.'&last_name='.$last_name;
							$social_id=$user->id;
						}
					break;
					case "google":
						if(count($user->user)>0) {
							$params='?email='.$user->email.'&first_name='.$user->user['name']['givenName'].'&last_name='.$user->user['name']['familyName'];
							$email       = $user->email;
							$first_name  = $user->user['name']['givenName'];
							$last_name   = $user->user['name']['familyName'];
							$avatar=$user->avatar_original;
							$social_id=$user->id;
						}

					break;
					case "twitter":
						if(count($user->user)>0) {
							// $params='?username='.$user->name;
							$params='?username='.$user->name.'&social_id='.$user->id.'&social_type='.$social.'&socialavatar='.$user->avatar_original;
							$email=$user->name.'@twitter.com';
							$first_name=$user->name;
							$last_name='';
							$avatar=$user->avatar_original;
							$social_id=$user->id;
							\Session::put('social_avatar',$avatar);
							return Redirect::to('user/register'.'/'.$params);

						}
					break;
				}
				if(count($user->user)>0) {
				    $explode     = explode('@', $email);
				    $info        = \DB::table('users')->where('social_id',$social_id)->first();
				    if(count($info)==0){
				      $authen                 = new User;
				      $authen->group_id       = 3;
				      $authen->active         = 1;
				      $authen->first_name     = $first_name;
				      $authen->last_name      = $last_name;
				      $authen->email          = trim($email);
				      $authen->username       = trim($explode['0']);
				      $authen->social_id      = $user->id;
				      $authen->social_type    = $social;
				      $authen->social_avatar  = $avatar;
				      $authen->save();
				      $user_id                = $authen->id;
				    }else{
				      $user_id                = $info->id;
				      $authen                 = Users::find($user_id);
				      $authen->first_name     = $first_name.'dd';
				      $authen->last_name      = $last_name;
				      $authen->email          = trim($email);
				      $authen->username       = trim($explode['0']);
				      $authen->social_id      = $user->id;
				      $authen->social_type    = $social;
				      $authen->social_avatar  = $avatar;
				      $authen->save();
				    }
				    \Session::put('social_id', $user->id);
					\Session::put('social_type', $social);

					$row = User::find($user_id);
					return self::autoSignin($row);
				}
				
			} else {

				if(!is_null($user_details))
				{

			      $user_id                = $user_details->id;
			      $authen                 = Users::find($user_id);
			      $authen->social_id      = $user->id;
			      $authen->social_type    = $social;
			      $authen->save();
				  return self::autoSignin($user_details);
				}
				if(!is_null($check))
				{
					return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error',  \Lang::get('core.email_already')));
				}

			}
		} catch (\Exception $e) {
			$message = $e->getMessage();
		}

		if($message!=''){
			return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error', $message));
		}

	}

	function getSocialize($slug='')
	{
		//$social = new Socialize();

		return Socialize::with($slug)->redirect();
	}

	function autoSignin($user)
	{
			\Auth::login($user);
			if(\Auth::check())
			{
				$row = User::find(\Auth::user()->id); 

				if($row->active =='0')
				{
					// inactive 
					\Auth::logout();
					return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is not active'));

				} else if($row->active=='2')
				{
					// BLocked users
					\Auth::logout();
					return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is BLocked'));
				} else {
					\Session::put('uid', $row->id);
					\Session::put('gid', $row->group_id);
					\Session::put('eid', $row->group_email);
					\Session::put('fid', $row->first_name.' '. $row->last_name);	
					\Session::put('ut' , $row->user_type);
					\DB::table('users')->where('id', '=',$row->id )->update(array('last_login' => date("Y-m-d H:i:s")));
					if(CNF_FRONT =='false') :
						return Redirect::to('dashboard');						
					else :
						return Redirect::to('');
					endif;					
				}
			}
	}

	public function getLearning()
	{
		if(!\Auth::check()) return Redirect::to('user/login');

		$loggeduser 				= \Session::get('uid');

		if(\Request::get('q') != null)
			$mycourse				= $this->model->getmycourse($loggeduser,\Request::get('q'));
		else
			$mycourse				= $this->model->getmycourse($loggeduser);
		$user 						= $this->model->getmyusers($loggeduser);
		$this->data['mycourse']     = $mycourse;
		$this->data['user']     	= $user;
		return view('user.learning',$this->data);
	}

	public function getMycourse()
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		$loggeduser 				= \Session::get('uid');
		$course                     = $this->model->getcourse($loggeduser);
		$favourites 				= $this->model->getmyfavorites($loggeduser);
		$user 						= $this->model->getmyusers($loggeduser);
		$this->data['course']       = $course;
		$this->data['favourites']   = $favourites;
		$this->data['user']     	= $user;
		//$this->data['live_course_admin'] = $this->model->getlivecourse($loggeduser); 
 		return view('user.mycourse',$this->data);
	}

	public function getWishlist()
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		$loggeduser 				= \Session::get('uid');
		$course                     = $this->model->getcourse($loggeduser);
		$favourites 				= $this->model->getmyfavorites($loggeduser);
		$user 						= $this->model->getmyusers($loggeduser);
		$this->data['course']       = $course;
		$this->data['favourites']   = $favourites;
		$this->data['user']     	= $user;
		return view('user.wishlist',$this->data);
	}
	
	public function getInstructorinfo($edit = ''){
		$this->data['pageTitle'] = 'Instructor Info';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
        $info =	User::find(\Session::get('uid'));
        $instructor = new Instructor;
        $page_check = $instructor->where('user_id',\Session::get('uid'))->first();
        $this->data['page_edit'] = '';
        $this->data['image_info'] = array();
        $this->data['checked'] = NULL;
        if($page_check == NULL){
        	$this->data['request_page'] = "personal_info";
        }else if($page_check->image_id == 0){
        	$this->data['request_page'] = "profile_picture";
		}else if($page_check->is_terms == NULL){
			$this->data['request_page'] = "terms_service";
		}else if($page_check->p_email == NULL){
			$this->data['request_page'] = "paypal_info";
		}else{
			if($edit == 'personal_info' ){
				$this->data['request_page'] = "personal_info";
				$this->data['page_edit'] = "instructor_profile";
			}
			else if($edit == 'profile_picture'){
				$this->data['request_page'] = "profile_picture";
				$this->data['page_edit'] = "instructor_picture";
				$this->data['image_info'] = CourseImages::select('image_title','image_type','image_hash')->where('id','=',$page_check->image_id)->first();
			}
			else if($edit == 'terms_service'){
				$this->data['request_page'] = "terms_service";
				$this->data['page_edit'] = "instructor_terms";
				 $this->data['checked'] = ($page_check->is_terms == 1 ? 'checked': '' );
			}
			else if($edit == 'paypal_info'){
				$this->data['request_page'] = "paypal_info";
				$this->data['page_edit'] = "instructor_paypal";
			}else{
				return Redirect::to('/')->with('messagetext',\Lang::get('core.pre_instructor'))->with('msgstatus',\Lang::get('core.msg_success'));
			}
		}
		//check for instructor
		if(count($page_check)){
			$this->data['instructor'] = $page_check;  
		}
		else{
			$this->data['instructor'] =(object) $this->model->getColumnTable('instructor');
		}

		$this->data['info'] =  $info;
		$this->data['pages'] = 'user.instructorinfo';   
        $theme=\bsetecHelpers::get_options('theme');
		$page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}

	public function postInstructorinfo($edit = '',Request $request){
		if(!\Auth::check()) return Redirect::to('user/login');
		$instructor = new Instructor;
		$user =	User::find(\Session::get('uid'));
		$next = '';
		$page_check = $instructor->where('user_id',\Session::get('uid'))->first();
		if($page_check == NULL || $edit == 'instructor_profile'){
			$input = array(
				'first_name'=> $request->get('first_name'),
				'last_name' => $request->get('last_name'),
				'email'		=> $request->get('email'),
				'headline'  => $request->get('headline'),
				'biography' => strip_tags($request->get('biography')),
				'b_address' => strip_tags($request->get('b_address')),
				'b_city' 	=> $request->get('b_city'),
				'b_zip' 	=> $request->get('b_zip'),
				'b_country' 	=> $request->get('b_country'),
				'b_phone'	=> $request->get('b_phone')
			);
			$rules = array(
				'first_name'=>'required|min:2',
				'last_name' =>'required|min:2',
				'email'		=>'required|email',
				'headline'  =>'required|min:10',
				'biography' =>'required|min:200',
				'b_address' =>'required',
				'b_city' 	=>'required',
				'b_zip' 	=>'required|numeric',
				'b_country' 	=>'required',
				'b_phone'	=>'required|numeric',
			);
			 $messages =array(
               'b_address.required' => 'Billing Information field is Required',
               'b_city.required' => 'City Required',
               'b_zip.required' => 'Zip code Required',
               'b_phone.required'=>'Phone number Required',
               'b_zip.numeric'=>'Zip code as numeric ',
               'b_country.required'=>'Country Required',
               'b_phone.numeric'=>'Phone number as numeric',
             );

		}else if($page_check->image_id == 0 || $edit == 'instructor_picture'){
			$input = array('image_id' => $request->get('image_id'));
			$rules = array('image_id' =>'required',);
			$messages=array('image_id.required'=>'profile image required');
			$instructor = Instructor::find($page_check->instructor_id);
		}else if($page_check->is_terms == NULL || $edit == 'instructor_terms' ){
			$input = array('is_terms' => $request->get('is_terms'));
			$rules = array('is_terms' =>'required',);
			$messages=array('is_terms.required'=>'Instructor Terms of service required');
			$instructor = Instructor::find($page_check->instructor_id);
		}else if($page_check->p_email == NULL || $edit == 'instructor_paypal'){
			$input = array(
				'p_email' => $request->get('p_email'),
				'p_address' => strip_tags($request->get('p_address')),
				'p_city' => $request->get('p_city'),
				'p_zip' => $request->get('p_zip'),
				'p_country' => $request->get('p_country'),
				);
			$rules = array(
				'p_email'	=>'required|email',
				'p_address' =>'required',
				'p_city' 	=>'required',
				'p_zip' 	=>'required|numeric',
				'p_country'	=>'required',
			);
			$messages = array
			 (
               'p_address.required' => 'Address Required.',
               'p_city.required' => 'City Required.',
               'p_zip.required' => 'Zip code Required.',
               'p_phone.required'	=>'Phone number Required.',
               'p_email.required'=>'Email Required.',
               'p_country.required'=>'Country Required.',
               'p_email.email' => 'Email format is wrong .',
               'p_zip.numeric'=>'Zip code as numeric ',
               'p_phone.numeric'=>'Phone number as numeric',
             );
			$instructor = Instructor::find($page_check->instructor_id);
		}

		$validator = Validator::make($input, $rules,$messages);

		if ($validator->passes()){
			if($page_check == NULL || $edit == 'instructor_profile'){
				if(($request->input('email') != \Session::get('eid')) || ($request->input('first_name') != $user->first_name) || ($request->input('last_name') != $user->last_name)){
					$user->email = $request->input('email');
					$user->first_name = $request->input('first_name');
					$user->last_name = $request->input('last_name');
					$user->save();
				}

				if($edit == 'instructor_profile'){
					$instructor->where('instructor_id','=',$request->input('instructor_id'))->where('user_id','=',\Session::get('uid'))->update(['headline'=>$request->input('headline'),'biography'=> $request->input('biography'),'b_address'=> $request->input('b_address'),'b_city'=> $request->input('b_city'),'b_zip'=> $request->input('b_zip'),'b_country'=> $request->input('b_country'),'b_phone'=> $request->input('b_phone')]);
					$user->where('id' ,'=' , \Session::get('uid'))->update(['biography' => $request->input('biography')]);
					$return = true;					
					$next = "profile_picture";
				}else{
					$instructor->user_id = \Session::get('uid');
					$instructor->headline = $request->input('headline');
					$instructor->biography = $request->input('biography');
					$instructor->b_address = $request->input('b_address');
					$instructor->b_city = $request->input('b_city');
					$instructor->b_zip = $request->input('b_zip');
					$instructor->b_country = $request->input('b_country');
					$instructor->b_phone = $request->input('b_phone');
					$user->biography = $request->input('biography');

					if($instructor->save() && $user->save()){
						$return = true;
					}
				}
			}else if($page_check->image_id == 0 || $edit == 'instructor_picture'){
				$instructor->image_id = $request->input('image_id');
				$user->avatar = $request->input('image_id');
				if($instructor->save() && $user->save()){
					$return = true;
					$next = ($edit != '' ? "terms_service" : '');
				}
			}elseif($page_check->is_terms == NULL || $edit == 'instructor_terms'){
				$instructor->is_terms = $request->input('is_terms');
				if($instructor->save()){
					$return = true;
					$next = ($edit != '' ? "paypal_info" : '');
				}
			}elseif($page_check->p_email == NULL || $edit == 'instructor_paypal'){
				$instructor->p_email = $request->get('p_email');
				$instructor->p_address = $request->get('p_address');
				$instructor->p_city = $request->get('p_city');
				$instructor->p_zip = $request->get('p_zip');
				$instructor->p_country = $request->get('p_country');
				$instructor->status = 1;
				$instructor->created_at = time();
				$instructor->updated_at = time();
				if($instructor->save()){
					$return = true;
					$next = ($edit != '' ? "personal_info" : '');
				}
			}
			if($return)
				return Redirect::to('user/instructorinfo/'.$next)->with('messagetext',\Lang::get('core.pre_instructor'))->with('msgstatus',\Lang::get('core.msg_success'));
		} else {
			return Redirect::to('user/instructorinfo')->with('messagetext',\Lang::get('core.ac_in_error'))->with('msgstatus',\Lang::get('core.msg_error'))
			->withInput()->withErrors($validator);
		}

	}
/*
	* @task - upload  profile picture and add,view and edit
	* @plugins - fileupload and croppic
	* @parm - image_id for upload process via image upload method
	*/
	public function getPhoto() {
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] = 'Photo';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
		$info =	User::find(\Session::get('uid'));
		$image = CourseImages::find($info->avatar);
		if($image != NUll){
			$image_name = $image['id'].".".$image['image_type'];
			$image_title = $image['image_title'].".".$image['image_type'];
			$this->data['image'] = $image_name;
			$this->data['image_title'] = $image_title;
			$this->data['image_id'] = $image['id'];
		}else{
			$this->data['image'] ="";
			$this->data['image_title'] ="";
			$this->data['image_id'] ="";
		}
		$this->data['pages'] = 'user.photo';   
		//check instructor
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  
        $theme=\bsetecHelpers::get_options('theme');
$page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}
	/*
	* @parm - image_id
	* $method - post
	*/
	public function postPhoto( Request $request ){
		$input = array('image_id' => $request->get('image_id'));
		$rules = array('image_id' =>'required');
		$validator = Validator::make($input, $rules);
		if ($validator->passes()){
			$user = User::find(\Session::get('uid'));
			$user->avatar = $request->get('image_id');
			if($user->save())
				return Redirect::to('user/photo')->with('messagetext','Profile picture has been updated!')->with('msgstatus','success');
		}else{
			return Redirect::to('user/photo')->with('messagetext','The following errors occurred')->with('msgstatus','error')
			->withInput()->withErrors($validator);
		}
	}
	/*
	* @parm - input
	* $method - get
	* account details of email and password
	*/
	public function getAccount(){
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] = 'Account';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
		$user =	User::find(\Session::get('uid'));
		$this->data['email'] = $user->email;
		$this->data['pages'] = 'user.account';
		//check instructor
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  
		$theme=\bsetecHelpers::get_options('theme');
        $page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}

	public function postAccount( Request $request ){
		$user = User::find(\Session::get('uid'));
		if($request->get('request_page') == "email"){
			$user_count = User::where('id','!=',\Session::get('uid'))->where('email','=',$request->get('email'))->count();
			if(!filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
				$return['error'] = "Eamil does not valid!";
			}elseif($user_count > 0 || ($request->get('email') == $user->email)){
				$return['error'] = "This email address is already in use.";
			}elseif(!Hash::check($request->get('email_password'), \Auth::user()->password)){
				$return['error'] = "Password does not Match!";
			}
			if(!isset($return['error'])){
				$user->email = $request->get('email');
				if($user->save())
					$return["success"] = true;
			}
			return json_encode($return);
		}else{
			$input = array(
				'current_password'		=> $request->get('current_password'),
				'password' 				=> $request->get('password'),
				'password_confirmation'	=> $request->get('password_confirmation'),
			);
			$rules = array(
				'current_password'		=> 'required',
				'password'              => 'required|confirmed|between:6,12|different:current_password',
				'password_confirmation' => 'required|between:6,12|required_with:password',
			);
			$validator = Validator::make($input, $rules);
			if($validator->fails()){
				return Redirect::back()->withErrors($validator)->withInput();
			}else if(!Hash::check($request->get('current_password'), \Auth::user()->password)){
				return Redirect::back()->withErrors('Password incorrect')->withInput();
			}else{
				$user->password = Hash::make($request->get('password'));
				if($user->save()){
					return Redirect::to('user/account')->with('message', \SiteHelpers::alert('account_success','Success: your password has been updated!'));
				}
			}
		}
	}

	/**
	 * @task - notification on/off settings
	 * @parm - on/off status
	 * @return - data
	*/
	public function getNotification()
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] = 'Notification';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'user.notificaton';
		$notificaton = Notificationsettings::where('user_id',\Session::get('uid'))->first();
		if($notificaton != NULL){
			$this->data['notif_announcement'] = $notificaton->notif_announcement;
			$this->data['notif_special_promotion'] = $notificaton->notif_special_promotion;
			$this->data['notif_course_subscribed'] = $notificaton->notif_course_subscribed;
			$this->data['notif_reviews'] = $notificaton->notif_reviews;
			$this->data['notif_course_approve'] = $notificaton->notif_course_approve;
			$this->data['notif_course_delete'] = $notificaton->notif_course_delete;
			$this->data['notif_for_all'] = $notificaton->notif_for_all;
		}else{
			$this->data['notif_announcement'] = 0;
			$this->data['notif_special_promotion'] = 0;
			$this->data['notif_course_subscribed'] = 0;
			$this->data['notif_reviews'] = 0;
			$this->data['notif_course_approve'] = 0;
			$this->data['notif_course_delete'] = 0;
			$this->data['notif_for_all'] = 0;
		}
		//check instructor
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  
		$theme=\bsetecHelpers::get_options('theme');
        $page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}

	/**
	 * @task - notification on/off settings
	 * @parm - on/off status
	 * @return - data
	*/
	public function postNotification( Request $request )
	{
		$notificaton = Notificationsettings::where('user_id',\Session::get('uid'))->first();
		if($notificaton != NULL){
			$notificaton->updated_at = time();
		}else{
			$notificaton = new Notificationsettings;
			$notificaton->user_id = \Session::get('uid');
			$notificaton->created_at = time();
			$notificaton->updated_at = time();
		}
		$notificaton->notif_announcement = ($request->get('notif_announcement')) ? $request->get('notif_announcement') : 0;
		$notificaton->notif_special_promotion = $request->get('notif_special_promotion') ? $request->get('notif_special_promotion') : 0;
		$notificaton->notif_course_subscribed = $request->get('notif_course_subscribed') ? $request->get('notif_course_subscribed') : 0;
		$notificaton->notif_reviews = $request->get('notif_reviews') ? $request->get('notif_reviews') : 0;
		$notificaton->notif_course_approve = $request->get('notif_course_approve') ? $request->get('notif_course_approve') : 0;
		$notificaton->notif_course_delete = $request->get('notif_course_delete') ? $request->get('notif_course_delete') : 0;
		
		$notificaton->notif_for_all = $request->get('notif_for_all') ? $request->get('notif_for_all') : 0;
		if($notificaton->save()){
			return Redirect::to('user/notification')->with('message',\SiteHelpers::alert('success','<b>Notification Settings has been updated!!<b>'));
		}
	}

	public function getDangerzone(){
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] = 'Danger Zone';
        $this->data['pageNote'] = 'Welcome To Our Site';
        $this->data['breadcrumb'] = 'inactive'; 
        $this->data['pageMetakey'] =  CNF_METAKEY ;
        $this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'user.danger';
		$theme=\bsetecHelpers::get_options('theme');
		//check instructor
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  
		$page = 'layouts.'.$theme['theme'].'.home.index';
        return view($page,$this->data);
	}

	public function postDangerzone( Request $request ){
		if($request->get('danger')){

			// delete user data's
			$blog_comments 			= $this->model->userData('blogcomments','user_id');
			$forum_comments 		= $this->model->userData('forum_comments','user_id');
			$favorite 				= $this->model->userData('favorite','user_id');
			$quiz_results 			= $this->model->userData('curriculum_quiz_results','user_id');
			$featured 				= $this->model->userData('featured','user_id');
			$instructor 			= $this->model->userData('instructor','user_id');
			$invitation_users 		= $this->model->userData('invitation_users','user_id');
			$lectures_comments 		= $this->model->userData('lectures_comments','user_id');
			$lectures_comment_reply = $this->model->userData('lectures_comment_reply','user_id');
			$logs 					= $this->model->userData('logs','user_id');
			$notifications 			= $this->model->userData('notifications','user_id');
			$report_abuse 			= $this->model->userData('report_abuse','user_id');
			$subscriber_list 		= $this->model->userData('subscriber_list','user_id');
			$withdraw_requests 		= $this->model->userData('withdraw_requests','user_id');
			$course_progress 		= $this->model->userData('course_progress','user_id');
			
			//users course unpublish
			$courses = Course::where('user_id',\Auth::user()->id)->get();

			if(count($courses)>0){
				foreach ($courses as $course) {
					$this->course->courseunpublish($course->course_id);
				}
			}


			User::find(\Session::get('uid'))->delete();
			$this->getLogout();
			return Redirect::to('/');
		}else{
			return Redirect::to('user/dangerzone');
		}
	}	


	/*
	* @task - post image and image crop option
	* @param - imageFile and image details via post
	* @retur - json
	* @process - image croping and add to db
	*/

	public function postImage( Request $request ){
		if(!$request->has('imgUrl')){
			$image = Input::file('cImage');
    		$what = getimagesize($image);
    		//echo '<pre>';print_r($what);exit;
            if($what[0] < 500 || $what[1] < 500){
                $response = array(
                    'status'=>false,
                    'message'=>'Uploaded image size is '.$what[0].'px * '.$what[1].'px. The Minimum allowed size is 500px * 500px.'
                );
             }else{
				$file_tmp_name = $image->getPathName();
				$file_ext =	 $image->getClientOriginalExtension();
				$file_name = explode('.',$image->getClientOriginalName());
		        $file_name = rand(4,9999).'.'.$file_ext;
		        $file_type = $image->getClientMimeType();

		        if (!file_exists('./uploads/tmp')) 
		        {
				    mkdir('./uploads/tmp', 0755, true);
				}
		        if(File::move($file_tmp_name,'./uploads/tmp/'.$file_name)){
					chmod('./uploads/tmp/'.$file_name, 0755);
					$response = array(
						'status'=>true,
						'image_path'=> url('uploads/tmp/'.$file_name),
						'mime_type' => $file_type,
						'image_name'=> $file_name
					);
				}else{
					$response = array(
						'status'=>false,
					);
				}
			}
		} else {
			$r_status = false;
			$image_name = $request->get('image_name');
			$imagec_name = $request->get('imagec_name');
			$imgUrl     = $request->get('imgUrl');
	        $imgInitW   = $request->get('imgInitW');
	        $imgInitH   = $request->get('imgInitH');
	        $imgW       = $request->get('imgW');
	        $imgH       = $request->get('imgH');
	        $imgY1      = $request->get('imgY1');
	        $imgX1      = $request->get('imgX1');
	        $cropW      = $request->get('cropW');
	        $cropH      = $request->get('cropH');

	        $jpeg_quality = 100;
			$file_name = explode('.',$image_name);
	        $filename = $file_name[0];

	        $what = getimagesize($imgUrl);
	        switch(strtolower($what['mime']))
	        {
	            case 'image/png':
	                $img_r = imagecreatefrompng($imgUrl);
	                $source_image = imagecreatefrompng($imgUrl);
	                $type = '.png';
	                break;
	            case 'image/jpeg':
	                $img_r = imagecreatefromjpeg($imgUrl);
	                $source_image = imagecreatefromjpeg($imgUrl);
	                $type = '.jpg';
	                break;
	            case 'image/gif':
	                $img_r = imagecreatefromgif($imgUrl);
	                $source_image = imagecreatefromgif($imgUrl);
	                $type = '.gif';
	                break;
	            default: die('image type not supported');
	         }
	        
	        $output_filename = './uploads/users/'.$filename.'_crop';;
	        
	        $check = CourseImages::where('image_title',$filename)->first();

	        if($check != null){
	        	$image_p = $check->id.".".$check->image_type;
	        	if (file_exists('./uploads/users/'.$image_p)) {
		        	$r_status = true;
		        	File::Delete('./uploads/users/'.$image_p);
		        	File::Delete('./uploads/users/'.$check->id."_medium.".$check->image_type);
                    File::Delete('./uploads/users/'.$check->id."_small.".$check->image_type);
		        	$image_n = $check->id;
		        	$output_filename = './uploads/users/'.$image_n.'_crop';
	       		}
	        }

			$courseImages = new CourseImages;
            $courseImages->image_title = $filename;
            $courseImages->image_type = substr($type, 1);
            $courseImages->image_tag = "dummy tag";
            $courseImages->uploader_id = \Session::get('uid');
            $courseImages->created_at = time();
            $courseImages->updated_at = time();

            if(!$r_status){
            	if($courseImages->save()){
            		$user = User::find(\Session::get('uid'));
					$user->avatar = $courseImages->id;
					$user->save();
            		$image_n = $courseImages->id;
            		$courseUpdate = CourseImages::find($courseImages->id);
            		$courseUpdate->image_hash = md5($courseImages->id.$courseUpdate->created_at);
            		$courseUpdate->save();
            		$output_filename = './uploads/users/'.$image_n.'_crop';
            	}
        	}


            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);   

            $dest_image = imagecreatetruecolor($cropW, $cropH);
            imagecopyresampled($dest_image, $resizedImage, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);    
            imagejpeg($dest_image, $output_filename.$type, $jpeg_quality);
            
             // resize image width and height
            $image_size[] = array('imgW'=>'500','imgH'=>'500','value'=>'normal');
            $image_size[] = array('imgW'=>'150','imgH'=>'150','value'=>'medium');
            $image_size[] = array('imgW'=>'64','imgH'=>'64','value'=>'small');

            if(file_exists('./uploads/users/'.$image_n.'_crop'.$type)){
                $path = './uploads/users/'.$image_n.'_crop'.$type;
                foreach($image_size as $size){
                    $save_path = ($size['value'] =='normal') ? $image_n.$type : $image_n.'_'.$size['value'].$type; 
                    Image::make($path)->resize($size['imgW'],$size['imgH'])->save('./uploads/users/'.$save_path);
                }

                if (file_exists($path))
                    File::Delete($path);
            		rename('./uploads/tmp/'.$image_name,'./uploads/users/'.$image_n.'_lg'.$type); 

            }
           
            $response = array(
                    "status" => 'success',
                    "url" => url('user/image/'.$courseUpdate->image_hash),
                    "image_name" =>$image_n.$type,
                    "image_id"=>$image_n,
            );
		}
		print json_encode($response);
	}
	
	/*
	* task - delete upload images
	* @parm - image_file ( input file tag ) and also delete in db
	* @plugin - fileupload ajax
	*/

	public function deleteImage( Request $request ){
		$image_file = explode('.', $request->get("image_file"));
		
		$getImage = CourseImages::where('image_title','=',$image_file[0])
                        ->orWhere('id','=',$image_file[0])->first();

		if(file_exists('./uploads/tmp/'.$request->get("image_file"))){
			$path = './uploads/tmp/'.$request->get("image_file");
		}else{
			$path = './uploads/users/'.$getImage->id.".".$getImage->image_type;
		}



        if(!file_exists($path)){
            $user = User::find(\Session::get('uid'));
			$user->avatar = 0;
            $user->save();
            $getImage->delete();
            echo "success";
            exit();
        }

		if(File::Delete($path)){
			if($getImage != NULL){
				File::Delete('./uploads/users/'.$getImage->id."_lg.".$getImage->image_type);
				File::Delete('./uploads/users/'.$getImage->id."_medium.".$getImage->image_type);
                File::Delete('./uploads/users/'.$getImage->id."_small.".$getImage->image_type);
				$user = User::find(\Session::get('uid'));
				$user->avatar = 0;
	            $user->save();
				$getImage->delete();
			}
			echo "success";
		}else{
			echo "fail";
		}
		exit();
	}

	public function getCredits(Request $request) 
	{

		if(!\Auth::check()) return Redirect::to('user/login');
		$user_id = \Session::get('uid');
		
		$this->data['credits'] = $this->user_credits->get_credits($user_id);
		$this->data['total_credits'] = $this->user_credits->get_total_credits($user_id);
		if($request->input('page')!=0)
		{
		$this->data['serial_no'] = ($request->input('page')-1)*10;
		}
		else
		{	
			$this->data['serial_no'] = 0;
		}
		// echo '<pre>';print_r($this->data);exit;
		return view('user.credits',$this->data);
	}

	public function getRequests() 
	{

		if(!\Auth::check()) return Redirect::to('user/login');
		$user_id = \Session::get('uid');
		
		$this->data['requests'] = $this->withdraw_requests->get_withdraw_requests($user_id);
		// echo '<pre>';print_r($this->data);exit;
		return view('user.requests',$this->data);
	}

	public function postWithdrawRequest()
	{
		if(!\Auth::check()) return Redirect::to('user/login');

		//get user id from session
		$user_id = \Session::get('uid');
		$amount = Input::get('amount');
		//get total credits for the user
		$total_credits = $this->user_credits->get_total_credits($user_id);
		$withdraw_limit = $this->bsetec->get_option('minimum_withdraw');
		if($amount < $withdraw_limit)
		{
			return Redirect::to('user/credits')->with('messagetext','Minimal withdrawal limit is '.$withdraw_limit)->with('msgstatus','error');
		}
		elseif($amount > $total_credits)
		{
			return Redirect::to('user/credits')->with('messagetext','Amount greater than your total credits')->with('msgstatus','error');
		}
		else
		{
			//create an array and insert all the necessary values
			$save['paypal_id'] = Input::get('paypal_id');
			$save['amount'] = $amount;
			$save['user_id'] = \Session::get('uid');
			$save['requested_on'] = date('Y-m-d H:i:s');
			$save['status'] = 'pending';
	
			$data 		= array('amount'=>$amount,'paymentid'=>Input::get('paypal_id'),'username'=>'Admin');	
			$toMail 	= CNF_EMAIL;
			$subject 	= CNF_APPNAME." WITHDRAW REQUEST FROM ".\Session::get('fid'); 			
			$fromMail 	= \Session::get('eid');
			$tempname 	= 'mailtemplates.withdraw_request';
			$this->sendmail->getMail($fromMail,$toMail,$subject,$data,$tempname);
			//save it in DB
			$this->withdraw_requests->save_withdraw_requests($save);
			return Redirect::to('user/credits')->with('messagetext','Request Sent Successfully')->with('msgstatus','success');
		}
		
	}
	
	public function getProfiles($username)
	{	
		$user = User::where('username', '=', $username)->first();
		if(count($user)=='0')
		{
			return Redirect::to('');
		}

		$user_id = $user['id'];

		$this->data['info'] = $this->model->userinfo($user_id);
		$this->data['coursecount'] = $this->model->coursecount($user_id);
		$this->data['studentscount'] = $this->model->studentscount($user_id);
		$this->data['courses'] = $this->model->mycourses($user_id);
		$this->data['ratings'] = $this->model->courseratings($user_id);
		$next_courses = $this->model->mycourses($user_id, 4);
		$this->data['next_courses'] = count($next_courses);

		$this->data['ratings'] = $this->model->courseratings($user_id);
		// echo '<pre>';print_r($this->data);exit;
		return view('user.profiles', $this->data);
	}

	public function postAjaxCourses()
	{
		$user_id = Input::get('user_id');
		$offset = Input::get('offset');
		$this->data['courses'] = $this->model->mycourses($user_id, $offset);
		return view('user.ajax-courses', $this->data);
	}
	/*
	* @process - courese certificate creation
	* @param - get file content
	* @method - get
	* @return - php file contents
	*/
    public function getCertificate(){
    	if(!\Auth::check()) return Redirect::to('user/login'); else if(\Auth::user()->group_id ==3) return Redirect::to('/');
    	$this->data['pageTitle'] = "Course Certificate";
    	$this->data['pageNote'] =  "Course Completion Certificate";

    	$filepath = str_replace("/app",'',base_path())."/resources/views/course/certificate.blade.php";
    	
    	$this->data['content'] = file_get_contents($filepath);

    	$this->data['return'] = self::returnUrl();
       	return view('user.certificate-form',$this->data);
    }

    public function postCertificate( Request $request ){
    	$rules = array(
			'certificate'		=> 'required|min:10',
		);
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) 
		{
			$filepath = str_replace("/app",'',base_path())."/resources/views/course/certificate.blade.php";
			
			$Fopen = fopen($filepath,"w+");				
			fwrite($Fopen, $request->get('certificate')); 
			fclose($Fopen);
			return Redirect::to('user/certificate')->with('messagetext', 'Certificate Template Has Been Updated')->with('msgstatus','success');	
		}else{
		return Redirect::to('user/certificate')->with('messagetext', 'The following errors occurred')->with('msgstatus','success')
			->withErrors($validator)->withInput();
		}
    }

    public function getCertificateview(){
    	$this->data['course_title'] = 'Dummy Course';
    	$this->data['name'] = \Session::get('fid');
       	$this->data['description'] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a aliquam nibh. Fusce ac nisi vel nibh dignissim sodales. Vestibulum dolor urna, pretium eu leo vitae, tristique blandit felis.";
       	
    	return view('course.certificate',$this->data);
    }
    public function postFeatured( Request $request ){
        if($request->get('action') =='Featured') {
            \DB::table('featured')->insert(
            	array(
            		'course_id'	=> $request->get('course_id'),
            		'user_id'	=> \Auth::user()->id
            	)
            );
            return 'Unfeatured';
        } else {
            \DB::table('featured')
            	->where('course_id',$request->get('course_id'))
            	->where('user_id',\Auth::user()->id)->delete();
            return 'Featured';
		}
    }

	public function getImage($image_hash = '', $size = 'normal')
	{
		$size = $size == 'normal' ? '' : '_'.$size;

		$image = \DB::table('course_images')->where('image_hash', '=', $image_hash)->first();
		
		header('content-type: '.$image->image_type);
	    readfile('uploads/users/'.$image->id.$size.'.'.$image->image_type);	
	}
	
	public function getInstructors(){
		$instructor = new Instructor;
		$this->data['instructors'] = $instructor->getInstructors(); 
		return view('user.instructors-list',$this->data);
    }

    public function postGetskills( Request $request )
	{
		$find = strtolower($request->input('q'));
		$skills['results'] = $this->model->getskills($find);
		return json_encode($skills);
	}  

	public function postGetselectedtutors( Request $request )
	{
		$find = strtolower($request->input('q'));
		$selectedtutors['results'] = $this->model->getselectedtutors($find);
		return json_encode($selectedtutors);
	}


	//   added for custom course request starts  \\
    public function getOnlinetutors($type='',$data='',$sort='',$page=1,$take=1000000){
    	$uid =	\Session::get('uid');
    	$results = $this->model->gettutors($uid,$type,$data,$sort,$page,$take);
    	
    	$instructors = array();

    	if(isset($results['rows'])){
    		foreach ($results['rows'] as $key => $instructor) {
    			if(\Cache::has('user-is-online-' . $instructor->id)){
    				$instructors[] = $instructor;
    			}
    			if(count($instructors) > 5){
    				break;
    			}
    		}
    	}
    	if (defined('CNF_CURRENCY')){
    		$currency = \SiteHelpers::getCurrentcurrency(CNF_CURRENCY);
    	} else {
    		$currency = 'USD';
    	}

    	$this->data['instructors'] = $instructors;
    	$this->data['currency'] = $currency;

    	return view('user.onlinetutors',$this->data);
    }  
	
	public function getPointsinfo(){
		$actions = new Gamificationactions;
		$this->data['actions'] = $actions->getRows(array());
		$levels = new Gamificationlevels;
		$this->data['levels'] = $levels->getRows(array());
		$streaks = new Gamificationstreaks;
		$this->data['streaks'] = $streaks->getRows(array());
		return view('user.gamificationinfo',$this->data);
    }

    public function getMembership(Request $request,$t = '')
     {
        if(!\Auth::check()) return Redirect::to('user/login');
		// get payment config options
		$this->data['paypal_express_checkout'] = $this->bsetec->get_options('paypal_express_checkout');
		$this->data['paypal_standard'] 	= $this->bsetec->get_options('paypal_standard');
		$this->data['stripe'] 			= $this->bsetec->get_options('stripe');

		$this->data['email']		= $request->input('email','');
		$this->data['first_name']	= $request->input('first_name','');
		$this->data['last_name']	= $request->input('last_name','');
		$this->data['username']		= $request->input('username','');
		$this->data['membership']	= $request->input('membership','0');
		if(Input::exists('email')||Input::exists('username')) {
			$social_id 		= \Session::get('social_id');
			$social_type 	= \Session::get('social_type');
		} else {
			$social_id		= '';
			$social_type	= '';
		}
		$this->data['social_id']=$social_id;
		$this->data['social_type']=$social_type;

		$getmonthly   = $this->membershipmodel->getMonthlyplan();
		$getyearly 	  = $this->membershipmodel->getYearlyplan();
		$getcountry   = $this->membershipmodel->getCountry();
		$this->data['monthly'] = $getmonthly;
		$this->data['yearly']  = $getyearly;
		$this->data['tab']     = $t;		
		$countrylist 					= array();
		if(count($getcountry)>0){
			$countrylist[] = 'Select Country';
			foreach ($getcountry as $key => $cvalue) {
				$countrylist[$cvalue->country_id] = $cvalue->country_name;
			}
		}
		$this->data['country'] = $countrylist;
		return view('user.signup',$this->data);
	}


	public function getUsermembership(){
		if(!\Auth::check()) return Redirect::to('user/login');
		$this->data['pageTitle'] 	= 'Membership';
		$this->data['pageNote'] 	= 'Welcome To Our Site';
		$this->data['breadcrumb'] 	= 'inactive';
		$this->data['pageMetakey'] 	=  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$user =	User::find(\Session::get('uid'));
		$this->data['membership'] = \bsetecHelpers::getMembershipofuserallstatus();
		// echo '<pre>';print_r($this->data['membership']);exit;
		$this->data['instructor'] = Instructor::where('user_id',\Session::get('uid'))->first();  
		$this->data['pages'] = 'user.membership';
		$theme 		= \bsetecHelpers::get_options('theme');
		$page 		= 'layouts.'.$theme['theme'].'.home.index';
		return view($page,$this->data);
	}

	 public function getMembershipmobile(Request $request, $userid='', $t = '') {
		// get payment config options
		$this->data['paypal_express_checkout'] = $this->bsetec->get_options('paypal_express_checkout');
		$this->data['paypal_standard']  = $this->bsetec->get_options('paypal_standard');
		$this->data['stripe'] 			= $this->bsetec->get_options('stripe');

		$this->data['email'] 		= $request->input('email','');
		$this->data['first_name'] 	= $request->input('first_name','');
		$this->data['last_name'] 	= $request->input('last_name','');
		$this->data['username'] 	= $request->input('username','');
		$this->data['membership']	= $request->input('membership','0');

		$getmonthly   = $this->membershipmodel->getMonthlyplan();
		$getyearly 	  = $this->membershipmodel->getYearlyplan();
		$getcountry   = $this->membershipmodel->getCountry();
		$this->data['monthly'] = $getmonthly;
		$this->data['yearly']  = $getyearly;
		$this->data['tab']  = $t;	
		$this->data['user_id'] = $userid;

		$countrylist 					= array();
		if(count($getcountry)>0){
			$countrylist[] = 'Select Country';
			foreach ($getcountry as $key => $cvalue) {
				$countrylist[$cvalue->country_id] = $cvalue->country_name;
			}
		}
		$this->data['country'] = $countrylist;
		return view('payment-mobile.membership',$this->data);
	}
	public function getPremiumInstructors(){
    	if(!\Auth::check()) 
    		return Redirect::to('user/login'); 
    	else if(\Auth::user()->group_id ==3) 
    		return Redirect::to('/');
    	$this->data['pageTitle'] = "Premium Instructors";
    	$this->data['pageNote'] =  "Premium Instructors List";

    	$this->data['instructors'] = $this->model->getPremiumInstructors();

    	// print_r($this->data['instructors']);die();

       	return view('user.premium_instructor_list',$this->data);
    }

    public function postApproval(Request $request) {
    	$ids = $request->input('id');
		$type  = $request->input('types');
		foreach ($ids as $id) {
			$success = $this->model->premiumInstructorApproval($id, $type);
		}
		if($success) {
			if($type=='1')
				return Redirect::back()->with('messagetext', 'Approved successfully.')->with('msgstatus',\Lang::get('core.msg_success'));
			else
				return Redirect::back()->with('messagetext', 'Unapproved successfully.')->with('msgstatus',\Lang::get('core.msg_success'));
		} else {
			return Redirect::back()->with('messagetext', 'Failed, Something went wrong.')->with('msgstatus',\Lang::get('core.msg_error'));
		}
    }

    public function getWallet() {
    	if(!\Auth::check()) return Redirect::to('user/login');
		$loggeduser	= \Session::get('uid');
		$data['wallet'] = User::where('id', $loggeduser)->value('wallet');
		$data['transactions'] = \DB::table('transactions')->where('user_id', $loggeduser)->orderBy('id', 'desc')->paginate('10');
		return view('user.wallet', $data);
    }

    public function getWalletpoint() {
    	if(!\Auth::check()) 
    		return Redirect::to('user/login'); 
    	else if(\Auth::user()->group_id ==3) 
    		return Redirect::to('/');

    	$this->data['pageTitle'] = "Wallet Users";
    	$this->data['pageNote'] =  "Wallet Users List";

    	$userdatas = Users::Select('id', 'username', 'email', 'active', 'wallet')->where('wallet', '!=', '')->paginate('10');

    	foreach($userdatas as $userdata) {
			$options_wallet_point = \bsetecHelpers::get_options('wallet_point');
			if(count($options_wallet_point)==0){
				$userdatas = array();
			} else {
				$wallet_amount_per_point = $options_wallet_point['amount']/$options_wallet_point['point']; 
				$userdata->walletpoint = $userdata->wallet/$wallet_amount_per_point;
				$userdata->walletpoint = round($userdata->walletpoint, 2);
			}
			$wallet_amount_per_point = $options_wallet_point['amount']/$options_wallet_point['point']; 
			$userdata->walletpoint = $userdata->wallet/$wallet_amount_per_point;
			$userdata->walletpoint = round($userdata->walletpoint, 2);
    	}

		$this->data['walletusers'] = $userdatas;
       	return view('user.wallet_point_list',$this->data);
    }

    function getWalletPointUpdateForm($id = null) {
		if($id =='') {
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}

		$userdata = Users::Select('id', 'username', 'email', 'wallet')->where('wallet', '!=', '')->where('id', '=', $id)->first(); 

		// $admin_wallet_point = \DB::table('admin_wallet_point')->where('user_id', $userdata->id)->first();

		// if($admin_wallet_point){
		// 	$userdata->walletpoint = floatval($admin_wallet_point->wallet_point);
		// 	$userdata->reason = $admin_wallet_point->reason;
		// }else {
		$options_wallet_point = \bsetecHelpers::get_options('wallet_point');
		$wallet_amount_per_point = $options_wallet_point['amount']/$options_wallet_point['point']; 
		$userdata->walletpoint = $userdata->wallet/$wallet_amount_per_point;
		$userdata->walletpoint = round($userdata->walletpoint, 2);
		// }

		$this->data['userdata'] =  $userdata;
		$this->data['pageTitle'] = "Wallet Users";
    	$this->data['pageNote'] =  "Wallet Users List";
		$this->data['id'] = $id;

		return view('user.wallet_point_update_form',$this->data);
	}	

	function postWalletPointUpdate(Request $request){
		$rules = array('type'=>'required');
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {

			$walletpoint = $request->input('walletpoint');
			$point = $request->input('point');
			$type = $request->input('type');
			$amount = $request->input('amount');

			$options_wallet_point = \bsetecHelpers::get_options('wallet_point');
			$wallet_amount_per_point = $options_wallet_point['amount']/$options_wallet_point['point']; 

			if($type){
				$walletpoint = $walletpoint + $point;	
			}else {
				$walletpoint = $walletpoint - $point;
			}
			$data['type'] = $type;
			$data['user_id'] = $request->input('id');
			$data['reason'] = $request->input('reason');
			$data['wallet_point'] = $walletpoint;

			$inserted = \DB::table('admin_wallet_point')->insert($data);
		
			if($inserted){
				$amount = $walletpoint * $wallet_amount_per_point;
				$user = Users::find($data['user_id']);
				$user->wallet = $amount;
				$user->save();
				return Redirect::to('user/walletpoint')->with('message', \SiteHelpers::alert('success',\Lang::get('core.note_success')));
			} else {
				return Redirect::to('user/walletpointupdate/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors('Failed');
			}
		} else {
			return Redirect::to('user/walletpointupdate/'.$request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')->withErrors($validator)->withInput();
		}
	}

	public function checkbusinessusercourse()
	{
		$business_user = \bsetecHelpers::getDashboards();
		if(count($business_user)>0)
		{
			$coursecount = 0;
			foreach ($business_user as $key => $value) {
				$count = $coursecount + $value->no_of_course;
				$coursecount = $count;
			}

			$createdcourse = $this->businessplan->createdcourse(\Auth::user()->id);
			$createdcount = count($createdcourse);
			if($createdcount >= $count)
			{
				return 0;
				//return Redirect::to('/')->with('messagetext','You have utilized all your credits so kindly purchase package to proceed')->with('msgstatus',\Lang::get('core.msg_error'));
			}else{
				return 1;
			}
		}
		if(count($business_user)==0){
			return 1;
		}
	}

}