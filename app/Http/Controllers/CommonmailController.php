<?php
/**
 * Company : Bsetec
 * Controller : Commonmail Controller
 * Email : support@bsetec.com
 */
namespace App\Http\Controllers;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Validator, Input, Redirect ; 
use Auth;
use Response;
use Mail;
use App\Models\bsetec;

class CommonmailController extends Controller {


	public function getMail($fromMail='', $toMail='', $subject='', $data='', $tempname=''){

		$this->bsetec = new bsetec();

		$email_settings = $this->bsetec->get_options('email_settings');

		if($email_settings['smtp_host']!='' && $email_settings['smtp_port']!='' && $email_settings['smtp_username']!='' && $email_settings['smtp_password']!=''){

			$config = array(
				'driver' 		=> 'smtp',
				'host' 		=> $email_settings['smtp_host'],
				'port' 		=> $email_settings['smtp_port'],
				'from' 		=> array('address' => $fromMail,'name'=>null),
				'encryption'  => $email_settings['smtp_secure'],
				'username' 	=> $email_settings['smtp_username'],
				'password' 	=> $email_settings['smtp_password'],
				'sendmail' 	=> '/usr/sbin/sendmail -bs',
				'pretend' 	=> false,
				);
			\Config::set('mail',$config);

			Mail::send($tempname, $data , function($message) use ($toMail, $fromMail, $subject)
			{   
				$message->from(CNF_EMAIL);
				$message->to($toMail)->subject($subject);
                $message->replyTo($fromMail, $fromMail);

			});
		}else{

			$message  = view($tempname, $data);
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: '.$fromMail.' <'.$fromMail.'>' . "\r\n";
			@mail($toMail, $subject, $message, $headers);
		}
	}

}