<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Subscriber;
use App\Models\Usergroup;
use App\Models\Core\Users;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 


class SubscriberController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'subscriber';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Subscriber();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'subscriber',
			'return'	=> self::returnUrl()
			
		);
		$this->searchmodel = new Usergroup();
		//$this->edithmodel = new Users();
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		//echo '<pre>';
		//print_r($results);exit;
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('subscriber');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('subscriber.index',$this->data);
	}	

	// for view edit group user
	function getUpdate(Request $request, $id = null)
	{
		$list = \DB::table('subscriber_list')
                    ->leftjoin('user_group', 'subscriber_list.group_id', '=','user_group.id')
                    ->leftjoin('users','subscriber_list.user_id','=','users.id')
                    ->select('users.first_name','subscriber_list.*','user_group.group_name')->where('subscriber_list.group_id','=',$id)
                    ->paginate(12);
		$this->data['pageTitle'] = 'Edit Group Users';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		return view('subscriber.group-edit',$this->data)->with('users',$list)->with('group_id',$id);
	}
	// for update group users
	public function postAdd(Request $request, $id = null)
	{
		$ids=Input::get('id');
		$group_id=Input::get('group_id');
		$count=Subscriber::get()->count();
		//echo $count; exit;
		if($count==1)
		{
			if(empty($ids))
			{
				Subscriber::where('group_id','=',$group_id)->delete();	
			}
		}
		if(!empty($ids))
		{
			Subscriber::where('group_id','=',$group_id)->delete();
			foreach($ids as $id)
			{

				$email=\DB::table('users')->select('email')->where('id','=',$id)->first();
				Subscriber::insert(['email' => $email->email,'user_id'=>$id,'group_id'=>$group_id]);
			}
			return Redirect::to('subscriber')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		}
		else
		{
			return Redirect::to('subscriber/update/'.$group_id)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		}
		
	}
	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('subscriber_list'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('subscriber.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		//print_r($request);exit;
		$group=Input::get('group_id');
		$user=Input::get('user_id');
		$email=Users::select('email')->where('id','=',$user)->get();
		foreach($email as $mail)
		{
			$email_id=$mail->email;
		}
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('subscriber');
			// insert query
			//Subscriber::insert[];
			$newID = Subscriber::insert(['email' => $email_id,'user_id'=>$user,'group_id'=>$group]);
			//$newID = $this->model->insertRow($data , $request->input('id'));
			if(!is_null($request->input('apply')))
			{
				$return = 'subscriber/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'subscriber?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('subscriber/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	function postUpdate( Request $request, $id =0)
	{
		//print_r($request);exit;
		//echo $id; exit;
		$group=Input::get('group_id');
		$user=Input::get('user_id');
		$email=Users::select('email')->where('id','=',$user)->get();
		foreach($email as $mail)
		{
			$email_id=$mail->email;
		}
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('subscriber');
			
			$newID = Subscriber::where('id',$id)->update(['email' => $email_id,'user_id'=>$user,'group_id'=>$group]);
			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to('subscriber')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('subscriber/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		$group_id = Input::get('group_id');
		$cnt = count($group_id);			
		if(count($request->input('group_id')) >=1)
		{	
			for($i=0;$i<$cnt;$i++){
				$this->model->group_delete($group_id[$i]);			
			}
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('group_id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('subscriber')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('subscriber')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}
	}

	public function getSearchuser(Request $request, $id = null)			
	{
		//echo 'Id:'.$id;exit;
		$this->data['pageTitle'] = 'Search Users';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['pages'] = 'subscriber.search';	
		$row = $this->model->find($id);
		$this->data['row']=$this->data['row'] =  $row;
		//$this->data['id']=$this->data['id'] = $id;
		$usergroup = Usergroup::get();
		return view('subscriber.search',$this->data)->with('group',$usergroup);
	}
	public function getSearchlist(Request $request, $id = null)
	{
		$country=Input::get('group_id');
		$group_id=Input::get('user_group');
		$user=Input::get('user_id');
		$usertype=Input::get('usertype');
		//echo $group_id,$user;exit;
		$this->data['pageTitle'] = 'Search Users';
		$this->data['pageNote'] = 'Welcome To Our Site';
		$this->data['breadcrumb'] = 'inactive';	
		$this->data['pageMetakey'] =  CNF_METAKEY ;
		$this->data['pageMetadesc'] = CNF_METADESC ;
		$this->data['group_id']=$group_id;

		if(($user)&&($usertype)&&($country)){
			if($usertype=='instructor')
			{	$type='instructor';
				$list= $this->searchmodel->commonsearch($user,$type,$country);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','instructor')->with('search1','country')->with('search2','name');
			}
			elseif($usertype=='student'){
				$type='student';
				$list =$this->searchmodel->commonsearch($user,$type,$country);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','student')->with('search1','country')->with('search2','name');
			}
		}else if(($user)&&($usertype)){
			if($usertype=='instructor')
			{	$type='instructor';
				$list= $this->searchmodel->searchbyusertype($user,$type);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','instructor')->with('search1','')->with('search2','name');
			}
			elseif($usertype=='student'){
				$type='student';
				$list =$this->searchmodel->searchbyusertype($user,$type);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','student')->with('search1','')->with('search2','name');
			}
		}else if(($user)&&($country)){
			$list= $this->searchmodel->searchusercountry($user,$country);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','')->with('search1','country')->with('search2','name');
		}else if(($usertype)&&($country)){
			if($usertype=='instructor')
			{	$type='instructor';
				$list= $this->searchmodel->searchusertypecountry($type,$country);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','instructor')->with('search1','country')->with('search2','');
			}
			elseif($usertype=='student'){
				$type='student';
				$list =$this->searchmodel->searchusertypecountry($type,$country);
				return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','student')->with('search1','country')->with('search2','');
			}
			
		}else if($user){
			//echo $user; exit;
			$list= $this->searchmodel->searchbyuser($user);
			//print_r($list);exit;
			return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','name')->with('search1','')->with('search2','');	
		}elseif($usertype=='instructor'){
			$list= $this->searchmodel->searchbyusertypee($usertype);
			return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','instructor')->with('search1','')->with('search2','');
		}else if($usertype=='student'){
			$list= $this->searchmodel->searchbyusertypee($usertype);
			return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','student')->with('search1','')->with('search2','');
		}elseif($country){
			$list= $this->searchmodel->searchbycountry($country);
			return view('subscriber.search-list',$this->data)->with('list',$list)->with('group_id',$group_id)->with('search','country')->with('search1','')->with('search2','');
		}

	}			


}