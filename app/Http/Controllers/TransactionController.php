<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\Models\CourseTaken;
use App\Models\bsetec;
use App\Http\Controllers\BankController;
use App\Http\Controllers\PaymentController;
class TransactionController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'transaction';
	static $per_page	= '10';

	public function __construct()
	{
		$this->bsetec = new bsetec();
		$this->model = new Transaction();
		$this->taken = new CourseTaken();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'transaction',
			'return'	=> self::returnUrl()
			
		);
		$this->bank=new BankController();
		$this->payment=new PaymentController();
	}

	public function Index( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'created_at'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = (!is_null($request->input('search')) ? $this->buildSearch() : '');

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('transaction');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());

		$pay = $this->bsetec->get_payment_method_enable();
		$_pay[''] = \Lang::get('core.select');
		$_pay['free']= \Lang::get('core.free');
		foreach ($pay as $key => $value) {
			$_pay[$value->code] = $value->code;
		}		
		$this->data['method'] = $_pay;

		// Render into template
		return view('transaction.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}

		$this->data['id'] = $id;
		$pay = $this->bsetec->get_payment_method_enable();
		$_pay[''] = \Lang::get('core.select');
		$_pay['free']= \Lang::get('core.free');
		foreach ($pay as $key => $value) {
			$_pay[$value->code] = $value->code;
		}		
		$this->data['method'] = $_pay;
		return view('transaction.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('transactions'); 
		}
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('transaction.view',$this->data);	
	}	

	function postSave( Request $request, $id =0)
	{
		$rules = array(
				'user_id' => 'required',	
				'course_id' => 'required',	
				'amount' => 'required|numeric',	
				'created_at' => 'required',								
				'payment_method' => 'required',	
				'status' => 'required|alpha'				
			);

		$type = $request->input('type');

		if($type == 'wallet')
			$rules['course_id'] = '';
		
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('transaction');
			
			$newID = $this->model->insertRow($data , $request->input('id'));

			if($newID){
				if($type == 'course' && $data['status'] == 'completed'){
					$taken = $this->taken->where('course_id',$data['course_id'])->where('user_id',$data['user_id'])->first();
					if(!empty($taken)){
						$takenUpdate = $this->taken->find($taken->taken_id);
					}else{
						$takenUpdate = $this->taken;
					}

					//update for course taken				
					$takenUpdate->user_id = $data['user_id'];
		            $takenUpdate->course_id = $data['course_id'];
		            $takenUpdate->completed = $data['status'];
		            $takenUpdate->notes = $data['order_details'];
		            $takenUpdate->updated_at = $data['created_at'];
		            $takenUpdate->created_at = $data['created_at'];
		            $takenUpdate->deleted_at = '';
		            $takenUpdate->save();
		        } else if($type == 'wallet' && $data['status'] == 'completed') {
					$walletBalance = \DB::table('users')->where('id', $data['user_id'])->value('wallet');
					$total = $data['amount'] + $walletBalance;
					$updateWallet = \DB::table('users')->where('id', $data['user_id'])->update(['wallet'=> $total]);
				}
				
	            $transid = $request->get('id');
		        if($data['payment_method']=='bank' && $data['status']=='completed' && $type!='wallet') {
	            	$transaction = \DB::table('user_credits')->select('transaction_id')->where('transaction_id',$transid)->get();
	            	if(count($transaction)==0) {
	            		$this->bank->save_credits($request->get('id'));
 						$this->bank->generate_invoice($request->get('id'));
			        }
			    }
	            if($data['payment_method']=='cod' && $data['status']=='completed' && $type!='wallet') {
	            	$transaction = \DB::table('user_credits')->select('transaction_id')->where('transaction_id',$transid)->get();
	            	if(count($transaction)==0){
		            	$this->payment->save_credits($request->get('id'));
     					$this->bank->generate_invoice($request->get('id'));
	            	}
	            }
	        }

			if(!is_null($request->input('apply')))
			{
				$return = 'transaction/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'transaction?return='.self::returnUrl();
			}

			// Insert logs into database
			if($id ==0)
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$newID.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$newID.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('transaction/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			foreach($request->input('id') as $id){
				$transaction = $this->model->find($id);
				if(!empty($transaction))
					CourseTaken::where('course_id',$transaction->course_id)->where('user_id',$transaction->user_id)->delete();
			}

			$this->model->destroy($request->input('id'));
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			
			// redirect
			return Redirect::to('transaction')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('transaction')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}		


	public function getIndexusers( Request $request )
	{

		if($this->access['is_view'] ==0)
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'created_at');
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'desc');
		// End Filter sort and order for query
		// Filter Search for query
		$filter = (!is_null($request->input('search')) ? $this->mybuildSearch() : '');
		$filter .= " AND purchase_type = 'membership' ";

		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query
		$results = $this->model->getRows( $params );

		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);
		$pagination->setPath('transaction');

		$this->data['rowData']		= $results['rows'];
		// Build Pagination
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();
		// Row grid Number
		$this->data['i']			= ($page * $params['limit'])- $params['limit'];
		// Grid Configuration
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$pay = $this->bsetec->get_payment_method_enable();
		$_pay[''] = \Lang::get('core.select');
		$_pay['free']= \Lang::get('core.free');
		foreach ($pay as $key => $value) {
			$_pay[$value->code] = $value->code;
		}

		if(isset($_GET['search']) && $_GET['search'] !='')
		{
			$type = explode("|",$_GET['search'] );
			if(count($type) >= 1)
			{
				foreach($type as $t)
				{
					$keys = explode(":",$t);

					foreach ($this->data['tableGrid'] as $key => $value) {

						if($keys['0']==$this->data['tableGrid'][$key]['field']){
							$this->data['tableGrid'][$key]['search_value']  = $keys['1'];
						}
					}
				}
			}
		}


		$this->data['method'] = $_pay;
		// Master detail link if any
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
		// Render into template
		return $this->data;
	}			
	


}