<?php
/**
 * @author Bsetec <support@bsetec.com>
 */
class SiteSettings {

    public static function siteSettings($request)
    {
        $request = DB::table('sitesettings')->where('option', '=', $request)->first();
        return $request->value;
    }

    public static function sitecategories()
    {
        return DB::table('categories')->where('status','=','1')->remember(999, 'siteCategories')->orderBy(DB::raw('name'))->get();

    }
    public static function siteCategories_admin()
    {
        return DB::table('categories')->orderBy(DB::raw('name'))->get();

    }
    public static function forumCategories()
    {
        return DB::table('forum_category')->remember(999, 'forumCategories')->orderBy(DB::raw('name'))->get();
    }

    public static function blogCategories()
    {
        return DB::table('blog_category')->remember(999, 'blogCategories')->orderBy(DB::raw('name'))->get();
    }

    /**
     * Pagination limit per page in course
     * @param int $int
     * @return int
     */
    public static function perPage($int = 20)
    {
        if (siteSettings('numberOfImagesInGallery') == '') {
            return $int;
        }
        return siteSettings('numberOfImagesInGallery');
    }

    /**
     * Number of tags that an image can hold
     * @param int $int
     * @return int
     */

    public static function tagLimit($int = 5)
    {
        $request = DB::table('sitesettings')->where('option', '=', 'tagsLimit')->first();
        return $request->value;
    }

    public static function limitPerDay($int = 100)
    {
        if (siteSettings('limitPerDay') == '') {
            return $int;
        }
        return siteSettings('limitPerDay');
    }

}