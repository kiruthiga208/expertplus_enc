/*ajax setup*/
$.ajaxSetup({
   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});

/*category menu on mobile*/
var _site_name = $('meta[name=_site_name]').attr('content');
var main = function() {
$('.mobile_menu .icon-menu').click(function() {
$('body').addClass('slidemenu');
$('.mobile_menu .menu').animate({left: "0px" }, 200); 
$('body').animate({left: "285px"}, 200); });
/* Then push them back */
$('.mobile_menu .icon-close').click(function() {
   $('body').removeClass('slidemenu'); 
   $('.mobile_menu .menu').animate({
      left: "-285px"
   },200);
$('body').animate({
  left: "0px"
  }, 200);
});
};
$(document).ready(main);

	
$(function() {
	
	// Prepare
    var History = window.History; // Note: We are using a capital H instead of a lower h
    if ( !History.enabled ) {
        return false;
    }
    // Bind to StateChange Event
    History.Adapter.bind(window,'statechange',function(test) { // Note: We are using statechange instead of popstate
        var State = History.getState();
		
		if(typeof State.data.state != 'undefined' &&  State.data.state != null){
			if(State.data.state=='course page') {
				expertstyle(State.url,'courselist');
			} else if(State.data.state=='blog page') {
				expertstyle(State.url,'bloglist');
			}
		} else {
			expertstyle(State.url,'common');
		}
    });

    $('.home_menu a, .logo a, .footer_menu a, .recent_block .view_more').click(function(evt) {
		if($(this).hasClass('ignorelink')) {
		} else {
			evt.preventDefault();
			var title = $(this).text().length>0 ? _site_name + ' | '+$(this).text() : _site_name;
			History.pushState(null, title, $(this).attr('href'));
		}
    });
	$(document).on('click', '.login-b .fgt a,.login-b .signup-l a, .login-b .fgt-pwd a, .tab-block-mycourse a, .credit-history .right-block a, .easier_course .learn_remeber a, .banner_section .learn_block a, .seach_alone_courses ul li a, .blog-post a, .expert_footer_blog .recent_sub a, header .browse_block .mCSB_container a', function(evt) {
		if($(this).hasClass('ignorelink')) {
		} else {
			evt.preventDefault();
			var title = $(this).text() ? _site_name + ' | '+$(this).text() : _site_name;
			History.pushState(null, title, $(this).attr('href'));
		}
	});
	
	/*blog sidebar*/
	$(document).on('click','.blog-view',function(evt){
		evt.preventDefault();
		var title = $(this).text() ? _site_name + ' | '+$(this).text() : _site_name;
        History.pushState({state:'blog page'}, title, $(this).attr('href'));
	});
	$(document).on('click', '.blog-sidebar .nav a', function(evt) {
		evt.preventDefault();
		var title = $(this).text() ? _site_name + ' | '+$(this).text() : _site_name;
        History.pushState({state:'blog page'}, title, $(this).attr('href'));
	});
	
	/*course sidebar*/
	$(document).on('click', '.course_body .sidebar_left .mang_develop a', function(evt) {
		evt.preventDefault();
		var title = $(this).text() ? _site_name + ' | '+$(this).text() : _site_name;
        History.pushState({state:'course page'}, title, $(this).attr('href'));
	});
	/*course pagination*/
	$(document).on('click', '.course_body .pagination a', function(evt) {
		evt.preventDefault();
		var title = _site_name + ' | Courses';
        History.pushState({state:'course page'}, title, $(this).attr('href'));
	});
	/*credits pagination*/
	$(document).on('click', '.credit-history .pagination a', function(evt) {
		evt.preventDefault();
		var title = _site_name;
        History.pushState(null, title, $(this).attr('href'));
	});
});

function expertstyle(url,type) {
   $.ajax({
	  type: 'GET',
	  url: url,
	  beforeSend: function() {
		if ($("#loadingbar").length === 0) {
		  $("body").append("<div id='loadingbar'></div>")
		  $("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
		   $("#loadingbar").width((50 + Math.random() * 30) + "%");
		}
	  }
	}).always(function() {
	   $("#loadingbar").width("101%").delay(200).fadeOut(400, function() {
		   $(this).remove();
	   });
	}).done(function(data) {
		$(".lazy").lazyload({effect:"fadeIn"});
		switch(type) {
			case 'courselist':
		    	$('#course-list-section').html($(data).find('div#course-list-section').html());
				    $('html, body').animate({ scrollTop: $('#course-list-section').offset().top - 115 }, 'slow');
			break;
			case 'bloglist':
		    	$('#blog-list-section').html($(data).find('div#blog-list-section').html());
				    $('html, body').animate({ scrollTop: $('#blog-list-section').offset().top - 115 }, 'slow');
			break;
			case 'common':
			    $('#expert_plus_main').html($(data).filter('div#expert_plus_main'));
				 $('html, body').animate({ scrollTop: $('#expert_plus_main').offset().top - 80 }, 'slow');
			break ;
		}
	});
}

